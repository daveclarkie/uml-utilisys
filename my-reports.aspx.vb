
Partial Class my_reports
    Inherits System.Web.UI.Page


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            If Not IIf(Core.data_select_value("select bitAllowMyReports from tblUsers where intUserPK = " & MySession.UserID) = "True", True, False) Then
                Response.Redirect(Core.data_select_value("select RawHomeURL from vwUsers where ID = " & MySession.UserID), True)
            End If

            Dim description As HtmlMeta = New HtmlMeta()
            description.Name = "description"
            description.Content = "UTILISYS - My Reports"
            Dim keywords As HtmlMeta = New HtmlMeta()
            keywords.Name = "keywords"
            keywords.Content = "UTILISYS - My Reports"
            Page.Header.Controls.Add(description)
            Page.Header.Controls.Add(keywords)

            LoadReports()

        Catch ex As Exception
            '
        End Try

    End Sub

    Private Sub LoadReports()

        Me.ulBox.InnerHtml = ""

        Dim strSQL As String = "EXEC spReports_ListReportTypes " & MySession.UserID

        Using connection As New SqlConnection(Core.ConnectionString)
            connection.Open()
            Dim command As New SqlCommand(strSQL, connection)
            command.CommandType = CommandType.Text
            Dim reader As SqlDataReader = command.ExecuteReader()

            While reader.Read

                Dim strUrl As String
                Dim strCSS As String

                If reader("intCount") > 0 Then
                    strUrl = reader("varURLPath").ToString
                    strCSS = reader("varCSS")
                Else
                    strUrl = ""
                    strCSS = reader("varCSS") & "_disabled"
                End If

                Me.ulBox.InnerHtml &= "<li><a id='lnkReports" & reader("intReportTypePK") & "' class='" & strCSS & "' runat='server' title='" & reader("varReportTypeNiceName") & "' href='" & strUrl & "' ></a></li>"

            End While

        End Using

        'core.data_Reader(strSQL)

    End Sub


End Class
