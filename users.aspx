<%@ Page MasterPageFile="~/landing.master" Language="VB" AutoEventWireup="false" CodeFile="users.aspx.vb" Inherits="users" title="Administration Area | View Users"%>

<asp:content contentplaceholderid="content_right" runat="server">
    <div class="content_back my_admin">
        <div id="admin" class="admin_area" runat="server">
            <form id="frmUser" runat="server" defaultbutton="btnSearch"> 
                <asp:Panel id="pnlStatus" cssclass="status_message" runat="server" visible="false">
                    <div class="status"><asp:label id="lblStatus" runat="server" /></div>
                    <div style="margin-left:5px;" class="hr"><hr /></div>
                </asp:Panel>

                <div class="buttons">
                    <asp:button id="btnNew" runat="server" cssclass="button" text="New User"></asp:button>
                    <asp:textbox id="txtSearch" cssclass="search" runat="server"></asp:textbox>
                    <asp:button id="btnSearch" runat="server" cssclass="button" text="Search"></asp:button>
                    <div class="hr"><hr /></div>
                </div>

                <!-- view USERS -->
                <asp:Panel ID="pnlVIEW" runat="server">
                    <!-- paging -->
                    <div id="filters">
                        <asp:panel runat="server" id="pnlFilters">
                            <div id="paging" class="right">
                                <asp:dropdownlist runat="server" cssclass="left" id="ddlUsersPerPage" autopostback="true" tooltip="Select number of users to display per page" width="50px">
                                    <asp:listitem value="10" text="10" />
                                    <asp:listitem value="20" text="20" selected="True" />
                                    <asp:listitem value="30" text="30" />
                                    <asp:listitem value="40" text="40" />
                                    <asp:listitem value="50" text="50" />
                                    <asp:listitem value="60" text="60" />
                                    <asp:listitem value="70" text="70" />
                                    <asp:listitem value="80" text="80" />
                                    <asp:listitem value="90" text="90" />
                                    <asp:listitem value="100" text="100" />
                                </asp:dropdownlist>
                                <span class="left">&nbsp;users per page</span>
                                <ul class="horizontal" id="pages" runat="server"></ul>
                            </div>
                        </asp:panel>
                    </div>

                    <!-- USERS -->
                    <asp:GridView borderwidth="0px" ID="grdUsers" cssclass="users" runat="server" headerstyle-cssclass="header" AutoGenerateColumns="False">
                        <Columns>
                            <asp:TemplateField HeaderText="">
                                <ItemTemplate>
                                    <asp:hyperlink runat="server" id="lnkEdit2" navigateurl='<%#Bind("EditURL") %>'>edit</asp:hyperlink>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="ID" HeaderText="ID" />
                            <asp:BoundField DataField="Name" HeaderText="Name" />
                            <asp:BoundField DataField="Email" HeaderText="Email" />
                            <asp:BoundField DataField="Homepage URL" HeaderText="Home URL" />
                        </Columns>
                        <AlternatingRowStyle CssClass="tralt" />
                        <headerstyle cssclass="header" />
                    </asp:GridView>
                    <asp:panel runat="server" id="pnlFilters2">
                        <div id="paging2" class="right">
                            <ul class="horizontal" id="pages2" runat="server"></ul>
                        </div>
                    </asp:panel>
                </asp:Panel>

            </form>
        </div>
    </div>
</asp:content>
