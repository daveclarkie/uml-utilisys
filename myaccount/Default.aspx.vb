﻿
Partial Class myaccount_Default
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim intCurrentUser As Integer = MySession.UserID

        If Not Page.IsPostBack Then
            Me.lblDetailsUpdated.text = Nothing
            LoadUserDetails(intCurrentUser)
        End If
    End Sub

    Private Sub LoadUserDetails(ByVal intCurrentUser As Integer)
        Me.pnlUserDetails.Visible = True
        Me.pnlUserMeters.Visible = False


        Me.txtUsername.Text = Core.data_select_value("SELECT varEmail FROM [portal.utilitymasters.co.uk].dbo.tblUsers WHERE intUserPK = " & intCurrentUser)
        Me.lblResendPassword.Text = Nothing
        Me.txtForename.Text = Core.data_select_value("SELECT varForename FROM [portal.utilitymasters.co.uk].dbo.tblUsers WHERE intUserPK = " & intCurrentUser)
        Me.txtSurname.Text = Core.data_select_value("SELECT varSurname FROM [portal.utilitymasters.co.uk].dbo.tblUsers WHERE intUserPK = " & intCurrentUser)
        Me.txtJobTitle.Text = Core.data_select_value("SELECT varJobTitle FROM [portal.utilitymasters.co.uk].dbo.tblUsers WHERE intUserPK = " & intCurrentUser)
        Me.txtContactNumber.Text = Core.data_select_value("SELECT varContactNumber FROM [portal.utilitymasters.co.uk].dbo.tblUsers WHERE intUserPK = " & intCurrentUser)
        Me.txtMobileNumber.Text = Core.data_select_value("SELECT varMobileNumber FROM [portal.utilitymasters.co.uk].dbo.tblUsers WHERE intUserPK = " & intCurrentUser)

        If Me.txtForename.Text = "0" Then Me.txtForename.Text = Nothing
        If Me.txtSurname.Text = "0" Then Me.txtSurname.Text = Nothing
        If Me.txtJobTitle.Text = "0" Then Me.txtJobTitle.Text = Nothing
        If Me.txtContactNumber.Text = "0" Then Me.txtContactNumber.Text = Nothing
        If Me.txtMobileNumber.Text = "0" Then Me.txtMobileNumber.Text = Nothing


        If Core.data_select_value("SELECT Count(*) FROM [portal.utilitymasters.co.uk].dbo.tblUsersPacks WHERE intUserFK = " & intCurrentUser & " AND intPackFK = 1") Then
            Me.lblPacksProcurement.ForeColor = System.Drawing.ColorTranslator.FromHtml("#6EBB1F")
            Me.imgPackProcurment.ImageUrl = "~/assets/icons/answer_yes.png"
        Else
            Me.lblPacksProcurement.ForeColor = Drawing.Color.Red
            Me.imgPackProcurment.ImageUrl = "~/assets/icons/answer_no.png"
        End If

        If Core.data_select_value("SELECT Count(*) FROM [portal.utilitymasters.co.uk].dbo.tblUsersPacks WHERE intUserFK = " & intCurrentUser & " AND intPackFK = 2") Then
            Me.lblPacksEnergyManagement.ForeColor = System.Drawing.ColorTranslator.FromHtml("#6EBB1F")
            Me.imgPackEnergyManagement.ImageUrl = "~/assets/icons/answer_yes.png"
        Else
            Me.lblPacksEnergyManagement.ForeColor = Drawing.Color.Red
            Me.imgPackEnergyManagement.ImageUrl = "~/assets/icons/answer_no.png"
        End If

        If Core.data_select_value("SELECT Count(*) FROM [portal.utilitymasters.co.uk].dbo.tblUsersPacks WHERE intUserFK = " & intCurrentUser & " AND intPackFK = 3") Then
            Me.lblPacksCCA.ForeColor = System.Drawing.ColorTranslator.FromHtml("#6EBB1F")
            Me.imgPackCCA.ImageUrl = "~/assets/icons/answer_yes.png"
        Else
            Me.lblPacksCCA.ForeColor = Drawing.Color.Red
            Me.imgPackCCA.ImageUrl = "~/assets/icons/answer_no.png"
        End If

        If Core.data_select_value("SELECT Count(*) FROM [portal.utilitymasters.co.uk].dbo.tblUsersPacks WHERE intUserFK = " & intCurrentUser & " AND intPackFK = 4") Then
            Me.lblPacksCRC.ForeColor = System.Drawing.ColorTranslator.FromHtml("#6EBB1F")
            Me.imgPackCRC.ImageUrl = "~/assets/icons/answer_yes.png"
        Else
            Me.lblPacksCRC.ForeColor = Drawing.Color.Red
            Me.imgPackCRC.ImageUrl = "~/assets/icons/answer_no.png"
        End If

        Dim intCountCompanies As Integer = Core.data_select_value("SELECT count(*) FROM dbo.vwUsersCompanies WHERE [User ID] = " & intCurrentUser & " ORDER BY [Company Name] ASC")

        If intCountCompanies = 1 Then

            Dim objDs As New DataSet()
            Dim myConnection As New System.Data.SqlClient.SqlConnection(Core.ConnectionString)
            Dim myCommand As System.Data.SqlClient.SqlDataAdapter
            myCommand = New System.Data.SqlClient.SqlDataAdapter("SELECT [Company Name] FROM dbo.vwUsersCompanies WHERE [User ID] = " & intCurrentUser & " ORDER BY [Company Name] ASC", myConnection)
            myCommand.SelectCommand.CommandType = CommandType.Text
            myConnection.Open()
            myCommand.Fill(objDs)

            grdCompany.DataSource = objDs
            grdCompany.DataBind()

        Else

            Dim objDs As New DataSet()
            Dim myConnection As New System.Data.SqlClient.SqlConnection(Core.ConnectionString)
            Dim myCommand As System.Data.SqlClient.SqlDataAdapter
            myCommand = New System.Data.SqlClient.SqlDataAdapter("SELECT 'You are currently assigned ' + CAST(count(*) as varchar(100)) + ' companies.' as [Company Name] FROM dbo.vwUsersCompanies WHERE [User ID] = " & intCurrentUser & " ORDER BY [Company Name] ASC", myConnection)
            myCommand.SelectCommand.CommandType = CommandType.Text
            myConnection.Open()
            myCommand.Fill(objDs)

            grdCompany.DataSource = objDs
            grdCompany.DataBind()

        End If


        Dim strResultImage As String = Core.data_select_value("SELECT CASE WHEN image is null then 0 else 1 END FROM [portal.utilitymasters.co.uk].dbo.tblUsers WHERE intUserPK = " & intCurrentUser)
        If strResultImage = 1 Then
            Me.imgUserPortrait.ImageUrl = "~/handler/image/UserImage.ashx?id=" & intCurrentUser
        Else
            Me.imgUserPortrait.ImageUrl = "~/assets/Avatar/~Avatar-1.png"
        End If

        Me.lblAccountType.Text = Core.data_select_value("SELECT varUserAccessLevelName FROM tlkpUserAccessLevels WHERE intUserAccessLevelPK = (SELECT intUserAccessLevelFK FROM [portal.utilitymasters.co.uk].dbo.tblUsers WHERE intUserPK = " & intCurrentUser & ")")

        If Me.lblAccountType.Text = "Standard User" Then
            Me.btnYourMeters.Enabled = False
        Else
            Me.btnYourMeters.Enabled = True
        End If

        Me.lnkFavouriteReport.Text = Core.data_select_value("SELECT TOP 1 varReportName FROM uvw_Analysis_ReportUsageByUser WHERE intUserPK = " & intCurrentUser & " ORDER BY intCount DESC")
        Me.lnkFavouriteReport.NavigateUrl = Core.data_select_value("SELECT TOP 1 varPagePath FROM uvw_Analysis_ReportUsageByUser WHERE intUserPK = " & intCurrentUser & " ORDER BY intCount DESC")

        Me.lnkLastReport.Text = Core.data_select_value("SELECT TOP 1 varReportName FROM uvw_Analysis_ReportUsageByUser_Overview WHERE intUserPK = " & intCurrentUser & " ORDER BY dtmCreated DESC")
        Me.lnkLastReport.NavigateUrl = Core.data_select_value("SELECT TOP 1 varPagePath FROM uvw_Analysis_ReportUsageByUser_Overview WHERE intUserPK = " & intCurrentUser & " ORDER BY dtmCreated DESC")
        Me.lnkLastReport.ToolTip = Core.data_select_value("SELECT TOP 1 'Report ran on ' + CAST(UML_EXTData.dbo.formatdatetime(dtmCreated, 'dd/mm/yyyy') AS VARCHAR(100)) COLLATE SQL_Latin1_General_CP1_CI_AS + ' at ' + CAST(UML_EXTData.dbo.formatdatetime(dtmCreated, 'HH:MM 24') AS VARCHAR(100)) COLLATE SQL_Latin1_General_CP1_CI_AS FROM uvw_Analysis_ReportUsageByUser_Overview WHERE intUserPK = " & intCurrentUser & " ORDER BY dtmCreated DESC")


        Me.lnkAccountManagerName.Text = Core.data_select_value("SELECT TOP 1 varAccountManagerName FROM uvw_UsersCompaniesAccountManagers WHERE intUserFK = " & intCurrentUser & " ORDER BY varCompanyName ASC")
        Me.lnkAccountManagerEmailDetail.Text = "click here to email" ' Core.data_select_value("SELECT TOP 1 varAccountManagerEmail FROM uvw_UsersCompaniesAccountManagers WHERE intUserFK = " & intCurrentUser & " ORDER BY varCompanyName ASC")
        Me.lnkAccountManagerEmailDetail.NavigateUrl = "MailTo:" & Core.data_select_value("SELECT TOP 1 varAccountManagerEmail FROM uvw_UsersCompaniesAccountManagers WHERE intUserFK = " & intCurrentUser & " ORDER BY varCompanyName ASC") & "?Subject=UTILISYS Enquiry"
        Me.lnkAccountManagerPhoneDetail.Text = Core.data_select_value("SELECT TOP 1 varAccountManagerPhone FROM uvw_UsersCompaniesAccountManagers WHERE intUserFK = " & intCurrentUser & " ORDER BY varCompanyName ASC")
        Me.lnkAccountManagerMobileDetail.Text = Core.data_select_value("SELECT TOP 1 varAccountManagerMobile FROM uvw_UsersCompaniesAccountManagers WHERE intUserFK = " & intCurrentUser & " ORDER BY varCompanyName ASC")

        LoadAccountManagerImage(intCurrentUser)

    End Sub

    Private Sub LoadUserMeters(ByVal intCurrentUser As Integer)
        Me.pnlUserDetails.Visible = False
        Me.pnlUserMeters.Visible = True

        With Me.lbUserMeters
            .DataSource = Core.data_select(" EXEC [portal.utilitymasters.co.uk].dbo.usp_MeterPoints_Display " & MySession.UserID & ", 'All Meters', 0,'" & Me.rblMeterFilter.SelectedItem.Value & "', '', '', 0, 1")
            .DataValueField = "ID"
            .DataTextField = "varNiceName"
            .DataBind()
        End With

        If Me.lbUserMeters.Items.Count > 0 Then
            Me.lbUserMeters.SelectedIndex = 0
        End If

        If Me.lbUserMeters.SelectedIndex > -1 Then

            LoadUserMeterDetails(Me.lbUserMeters.SelectedItem.Value.ToString)

        End If

    End Sub

    Private Sub LoadUserMeterDetails(ByVal strMeterID As String)

        Me.lblMeterID.Text = strMeterID
        Me.lblMeterType.Text = Core.data_select_value("SELECT varType FROM UML_CMS.dbo.uvw_Admin_DisplayAllMeterPoints WHERE ID = '" & strMeterID & "'")
        Me.lblMeterIdentifier.Text = Core.data_select_value("SELECT varMeterID FROM UML_CMS.dbo.uvw_Admin_DisplayAllMeterPoints WHERE ID = '" & strMeterID & "'")
        Me.lblMeterChannel.Text = Core.data_select_value("SELECT intChannelID FROM UML_CMS.dbo.uvw_Admin_DisplayAllMeterPoints WHERE ID = '" & strMeterID & "'")
        Me.lblMeterDescription.Text = Core.data_select_value("SELECT CASE WHEN varMeter = varMeterID THEN 'n/a' ELSE varMeter END FROM UML_CMS.dbo.uvw_Admin_DisplayAllMeterPoints WHERE ID = '" & strMeterID & "'")
        Me.lblMeterHalfHour.Text = Core.data_select_value("SELECT CASE WHEN intHalfHour = 1 THEN 'Yes' ELSE 'No' END FROM UML_CMS.dbo.uvw_Admin_DisplayAllMeterPoints WHERE ID = '" & strMeterID & "'")
        Me.txtCustomerReference.Text = Core.data_select_value("SELECT varMeterNiceName FROM tlkpMeterNiceNames WHERE varMeterFK = '" & strMeterID & "'")

        If Core.data_select_value("SELECT count(*) FROM [portal.utilitymasters.co.uk].dbo.tlkpMetersHidden WHERE varMeterFK = '" & strMeterID & "'") > 0 Then
            Me.rblMeterHidden.SelectedValue = 1
        Else
            Me.rblMeterHidden.SelectedValue = 0
        End If

        If Me.txtCustomerReference.Text.Length = 0 Then
            Me.txtCustomerReference.Width = "245"
            Me.btnCustomerReferenceRemove.Visible = False
        Else
            Me.txtCustomerReference.Width = "215"
            Me.btnCustomerReferenceRemove.Visible = True
        End If

        Me.lblMeterReferenceUpdateOutcome.Text = Nothing

    End Sub

    Private Sub LoadAccountManagerImage(ByVal intCurrentUser As Integer)

        Dim strAccountManagerImage As String = Core.data_select_value("SELECT TOP 1 CASE WHEN imgAccountManager is null then 0 else 1 END FROM uvw_UsersCompaniesAccountManagers WHERE intUserFK = " & intCurrentUser & " ORDER BY varCompanyName ASC")
        If strAccountManagerImage = 1 Then
            Me.imgAccountManager.ImageUrl = "~/handler/image/AccountManagerImage.ashx?id=" & Core.data_select_value("SELECT TOP 1 intAccountManagerFK FROM uvw_UsersCompaniesAccountManagers WHERE intUserFK = " & intCurrentUser & " ORDER BY varCompanyName ASC")
        Else
            Me.imgAccountManager.ImageUrl = "~/assets/Avatar/~Avatar-1.png"
        End If



    End Sub


    Protected Sub btnUploadImage_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUploadImage.Click
        Dim intCurrentUser As Integer = MySession.UserID

        Dim connection As SqlConnection = Nothing
        Try
            Dim img As FileUpload = CType(imgUpload, FileUpload)
            Dim imgByte As Byte() = Nothing
            If img.HasFile AndAlso Not img.PostedFile Is Nothing Then
                'To create a PostedFile
                Dim File As HttpPostedFile = imgUpload.PostedFile
                'Create byte Array with file len
                imgByte = New Byte(File.ContentLength - 1) {}
                'force the control to load data in array
                File.InputStream.Read(imgByte, 0, File.ContentLength)
            End If
            ' Insert the employee name and image into db
            ' Insert the employee name and image into db
            Dim conn As String = ConfigurationManager.ConnectionStrings("portal.utilitymasters.co.ukConnectionString").ConnectionString
            connection = New SqlConnection(conn)

            connection.Open()

            Dim sql As String = "UPDATE [portal.utilitymasters.co.uk].dbo.tblUsers SET Image = @eimg WHERE intUserPK = " & intCurrentUser & "; SELECT " & intCurrentUser & ""
            Dim cmd As SqlCommand = New SqlCommand(sql, connection)
            cmd.Parameters.AddWithValue("@eimg", imgByte)

            Dim id As Integer = Convert.ToInt32(cmd.ExecuteScalar())
            'lblResult.Text = String.Format("image uploaded ok")


            Dim intCheckIsAccountManager As Integer = Core.data_select_value("SELECT COUNT(*) FROM UML_CMS.dbo.tblEmployees WHERE email  COLLATE SQL_Latin1_General_CP1_CI_AS = (SELECT varEmail FROM [portal.utilitymasters.co.uk].dbo.tblUsers WHERE intUserPK = " & intCurrentUser & ")")
            If intCheckIsAccountManager > 0 Then
                Dim sql2 As String = "UPDATE UML_CMS.dbo.tblEmployees SET Image  = @eimg  WHERE email COLLATE SQL_Latin1_General_CP1_CI_AS = (SELECT varEmail FROM [portal.utilitymasters.co.uk].dbo.tblUsers WHERE intUserPK = " & intCurrentUser & ")"
                Dim cmd2 As SqlCommand = New SqlCommand(sql2, connection)
                cmd2.Parameters.AddWithValue("@eimg", imgByte)
                cmd2.ExecuteScalar()
            End If

            ' Display the image from the database
            imgUserPortrait.ImageUrl = "~/ShowImage.ashx?id=" & id

            LoadAccountManagerImage(id)
            'Dim sql As String = "UPDATE [portal.utilitymasters.co.uk].dbo.tblUsers SET Image = '" & imgByte.ToString & "' WHERE intUserPK = " & MySession.UserID & "; SELECT " & MySession.UserID & ""
            LoadUserDetails(intCurrentUser)
            'Dim id As Integer = Core.data_select_value(sql)
            'lblResult.Text = String.Format("Employee ID is {0}", id)
        Catch
            'lblResult.Text = "error"
        Finally
            connection.Close()
        End Try

    End Sub

    Protected Sub btnRemoveImage_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRemoveImage.Click

        Dim intCurrentUser As Integer = MySession.UserID

        Core.data_execute_nonquery("UPDATE [portal.utilitymasters.co.uk].dbo.tblUsers SET Image = Null WHERE intUserPK = " & intCurrentUser)

        Dim intCheckIsAccountManager As Integer = Core.data_select_value("SELECT COUNT(*) FROM UML_CMS.dbo.tblEmployees WHERE email  COLLATE SQL_Latin1_General_CP1_CI_AS = (SELECT varEmail FROM [portal.utilitymasters.co.uk].dbo.tblUsers WHERE intUserPK = " & intCurrentUser & ")")
        If intCheckIsAccountManager > 0 Then
            Core.data_execute_nonquery("UPDATE UML_CMS.dbo.tblEmployees SET Image  = NULL WHERE email COLLATE SQL_Latin1_General_CP1_CI_AS = (SELECT varEmail FROM [portal.utilitymasters.co.uk].dbo.tblUsers WHERE intUserPK = " & intCurrentUser & ")")
        End If

        LoadUserDetails(intCurrentUser)

    End Sub

    Protected Sub btnResendPassword_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnResendPassword.Click

        Dim intCurrentUser As Integer = MySession.UserID
        Dim strPassword As String = Core.data_select_value("SELECT varPassword FROM [portal.utilitymasters.co.uk].dbo.tblUsers WHERE intUserPK = " & intCurrentUser)

        Dim htmlMessageBody As String
        htmlMessageBody = " "
        htmlMessageBody = htmlMessageBody + " <HTML xmlns='http://www.w3.org/1999/xhtml'><HEAD><TITLE>Utility Masters Ltd</TITLE>"
        htmlMessageBody = htmlMessageBody + " <META http-equiv=Content-Type content='text/html; charset=utf-8'>"
        htmlMessageBody = htmlMessageBody + " <STYLE type=text/css>"
        htmlMessageBody = htmlMessageBody + " a {FONT-SIZE: 10pt; COLOR: #082b61; FONT-FAMILY: Tahoma}"
        htmlMessageBody = htmlMessageBody + " body {FONT-SIZE: 10pt; COLOR: #082b61; FONT-FAMILY: Tahoma}"
        htmlMessageBody = htmlMessageBody + " a:link {FONT-SIZE: 10pt; COLOR: #082b61; FONT-FAMILY: Tahoma}"
        htmlMessageBody = htmlMessageBody + " a:visited {FONT-SIZE: 10pt; COLOR: #082b61; FONT-FAMILY: Tahoma}"
        htmlMessageBody = htmlMessageBody + " a:active {FONT-SIZE: 10pt; COLOR: #082b61; FONT-FAMILY: Tahoma}"
        htmlMessageBody = htmlMessageBody + " a:hover {FONT-SIZE: 10pt; COLOR: #082b61; FONT-FAMILY: Tahoma}"
        htmlMessageBody = htmlMessageBody + " td {FONT-SIZE: 10pt; COLOR: #082b61; FONT-FAMILY: Tahoma}"
        htmlMessageBody = htmlMessageBody + " td.smallblue {FONT-SIZE: 7pt; COLOR: #082b61; FONT-FAMILY: Tahoma;}"
        htmlMessageBody = htmlMessageBody + " td.green {FONT-SIZE: 7pt; COLOR: green; FONT-FAMILY: Tahoma;}"
        htmlMessageBody = htmlMessageBody + " </STYLE>"
        htmlMessageBody = htmlMessageBody + " <META content='MSHTML 6.00.6000.16674' name=GENERATOR></HEAD>"
        htmlMessageBody = htmlMessageBody + " <BODY style='FONT-SIZE: 10pt; COLOR: #082b61; FONT-FAMILY: Tahoma'>"
        htmlMessageBody = htmlMessageBody + " <TABLE cellSpacing=0 cellPadding=0>"
        htmlMessageBody = htmlMessageBody + " <TBODY>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD><IMG alt='Utility Masters' src='http://images.utilitymasters.co.uk/system_emails/_default/branding-header.jpg' "
        htmlMessageBody = htmlMessageBody + " > </TD></TR>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD>&nbsp; </TD></TR>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD>"
        htmlMessageBody = htmlMessageBody + " <TABLE>"
        htmlMessageBody = htmlMessageBody + " <TBODY>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD style='WIDTH: 20px'></TD>"
        htmlMessageBody = htmlMessageBody + " <TD style='WIDTH: 550px'>"
        htmlMessageBody = htmlMessageBody + " <P><B>"
        htmlMessageBody = htmlMessageBody + " Dear " & Me.txtForename.Text.ToString & " " & Me.txtSurname.Text.ToString
        htmlMessageBody = htmlMessageBody + ", </B> </P>"
        htmlMessageBody = htmlMessageBody + " <P>Your password as requested for <B>UTILI</B>SYS is: " & strPassword & "</P>"
        htmlMessageBody = htmlMessageBody + " </TD>"
        htmlMessageBody = htmlMessageBody + " <TD style='WIDTH: 20px'></TD></TR>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD>&nbsp; </TD></TR>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD></TD>"
        htmlMessageBody = htmlMessageBody + " <TD>Regards, </TD>"
        htmlMessageBody = htmlMessageBody + " <TD></TD></TR>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD></TD>"
        htmlMessageBody = htmlMessageBody + " <TD><B>UTILI</B>SYS Customer Services</TD>"
        htmlMessageBody = htmlMessageBody + " <TD></TD></TR>"
        htmlMessageBody = htmlMessageBody + " <TR style='Height:10px;'>"
        htmlMessageBody = htmlMessageBody + " <TD></TD>"
        htmlMessageBody = htmlMessageBody + " </TR>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD></TD>"
        htmlMessageBody = htmlMessageBody + " <TD>"
        htmlMessageBody = htmlMessageBody + " <TABLE cellSpacing=0 cellPadding=0>"
        htmlMessageBody = htmlMessageBody + " <TBODY>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD style='WIDTH: 280px'>"
        htmlMessageBody = htmlMessageBody + " <TABLE cellSpacing=0 cellPadding=0>"
        htmlMessageBody = htmlMessageBody + " <TBODY>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD><B>Utility Masters Ltd</B> </TD></TR>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD>UML House </TD></TR>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD>5 Salmon Fields Business Village </TD></TR>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD>Royton </TD></TR>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD>Oldham </TD></TR>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD>OL2 6HT </TD></TR></TBODY></TABLE></TD>"
        htmlMessageBody = htmlMessageBody + " <TD style='FONT-SIZE: 12pt; WIDTH: 280px'>"
        htmlMessageBody = htmlMessageBody + " <TABLE cellSpacing=0 cellPadding=0>"
        htmlMessageBody = htmlMessageBody + " <TBODY>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD style='WIDTH: 20px'><B>T</B> </TD>"
        htmlMessageBody = htmlMessageBody + " <TD>+44 (0)161 785 0404 </TD></TR>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD><B>F</B> </TD>"
        htmlMessageBody = htmlMessageBody + " <TD>+44 (0)161 785 7969 </TD></TR>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD><B>W</B> </TD>"
        htmlMessageBody = htmlMessageBody + " <TD><A href='http://www.utilitymasters.co.uk' "
        htmlMessageBody = htmlMessageBody + " target=_blank>www.utilitymasters.co.uk</A> "
        htmlMessageBody = htmlMessageBody + " </TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></TD></TR>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD>&nbsp; </TD></TR>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD></TD>"
        htmlMessageBody = htmlMessageBody + " <TD class=smallblue colSpan=2>Help cut carbon. Please dont print "
        htmlMessageBody = htmlMessageBody + " this email unless you really need to. </TD></TR>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD>&nbsp; </TD></TR>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD class=green style='TEXT-ALIGN: center' colSpan=3>Energy "
        htmlMessageBody = htmlMessageBody + " Management | Climate Change Management | Energy Surveys | Utility "
        htmlMessageBody = htmlMessageBody + " Procurement | Monitoring &amp; Targeting | Bureau Services </TD></TR>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD style='TEXT-ALIGN: right' colSpan=3><IMG "
        htmlMessageBody = htmlMessageBody + " alt='Your Outsourced Energy Manager' src='http://images.utilitymasters.co.uk/system_emails/_utilitymasters_branding/footer_crop_slides.jpg'> "
        htmlMessageBody = htmlMessageBody + " </TD></TR></TBODY></TABLE></TD></TR>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD style='TEXT-ALIGN: right'></TD></TR></TBODY></TABLE></BODY>"

        clsMail.SendEmail("customerservices@utilitymasters.co.uk", Me.txtUsername.Text, "UTILISYS Password Request", htmlMessageBody)

        Me.lblResendPassword.Text = "sent"
    End Sub

    Protected Sub btnMyFavouriteReports_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnMyFavouriteReports.Click
        'Server.Transfer("~/reports/MyReports/Default.aspx")
        Response.Redirect("~/reports/MyReports/Default.aspx")
    End Sub


    Protected Sub btnUpdateDetails_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdateDetails.Click


        Dim intCurrentUser As Integer = MySession.UserID
        Me.txtForename.Text = Core.fn_NoSpecialCharacters(Me.txtForename.Text)
        Me.txtSurname.Text = Core.fn_NoSpecialCharacters(Me.txtSurname.Text)
        Me.txtJobTitle.Text = Core.fn_NoSpecialCharacters(Me.txtJobTitle.Text)
        Me.txtContactNumber.Text = Core.fn_NoSpecialCharacters(Me.txtContactNumber.Text)
        Me.txtMobileNumber.Text = Core.fn_NoSpecialCharacters(Me.txtMobileNumber.Text)

        Dim strForename As String = Me.txtForename.Text.ToString
        Dim strSurname As String = Me.txtSurname.Text.ToString
        Dim strJobTitle As String = Me.txtJobTitle.Text.ToString
        Dim strContactNumber As String = Me.txtContactNumber.Text.ToString
        Dim strMobileNumber As String = Me.txtMobileNumber.Text.ToString

        Core.data_execute_nonquery("UPDATE tblUsers SET varForename = '" & strForename & "', varSurname = '" & strSurname & "', varJobTitle = '" & strJobTitle & "', varContactNumber = '" & strContactNumber & "', varMobileNumber = '" & strMobileNumber & "', dtmModified = getdate(), intUserModifiedBy = " & intCurrentUser & " WHERE intUserPK = " & intCurrentUser)

        Me.lblDetailsUpdated.Text = "Your Details Have Been Updated"

        LoadUserDetails(intCurrentUser)

    End Sub



    Protected Sub btnYourDetails_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnYourDetails.Click
        Me.btnYourDetails.CssClass = "myaccount_selectedmenu"
        Me.btnYourMeters.CssClass = "myaccount_unselectedmenu"

        LoadUserDetails(MySession.UserID)
    End Sub

    Protected Sub btnYourMeters_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnYourMeters.Click
        Me.btnYourDetails.CssClass = "myaccount_unselectedmenu"
        Me.btnYourMeters.CssClass = "myaccount_selectedmenu"

        LoadUserMeters(MySession.UserID)
    End Sub

    Protected Sub rblMeterFilter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rblMeterFilter.SelectedIndexChanged
        LoadUserMeters(MySession.UserID)
    End Sub

    Protected Sub lbUserMeters_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbUserMeters.SelectedIndexChanged
        LoadUserMeterDetails(Me.lbUserMeters.SelectedItem.Value.ToString)
    End Sub

    Protected Sub btnUpdateCustomerMeterRefence_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdateCustomerMeterRefence.Click
        Dim strUpdateValue As String = Me.txtCustomerReference.Text.ToString

        strUpdateValue = Core.fn_NoSpecialCharacters(strUpdateValue)

        If strUpdateValue.Length > 0 Then

            If Core.data_select_value("SELECT COUNT(*) FROM [portal.utilitymasters.co.uk].dbo.tlkpMeterNiceNames WHERE varMeterFK = '" & Me.lblMeterID.Text.ToString & "'") > 0 Then
                'update
                Core.data_execute_nonquery("UPDATE [portal.utilitymasters.co.uk].dbo.tlkpMeterNiceNames SET varMeterNiceName = '" & strUpdateValue & "', dtmModified = getdate(), intUserModifiedFK = " & MySession.UserID & " WHERE varMeterFK = '" & Me.lblMeterID.Text.ToString & "'")
            Else
                'insert
                Core.data_execute_nonquery("INSERT INTO [portal.utilitymasters.co.uk].dbo.tlkpMeterNiceNames (varMeterFK, varMeterNiceName) VALUES ('" & Me.lblMeterID.Text.ToString & "','" & strUpdateValue & "')")
            End If

            Me.lblMeterReferenceUpdateOutcome.Text = "updated."
            Me.lblMeterReferenceUpdateOutcome.ForeColor = Drawing.Color.LimeGreen
        Else
            If Core.data_select_value("SELECT COUNT(*) FROM [portal.utilitymasters.co.uk].dbo.tlkpMeterNiceNames WHERE varMeterFK = '" & Me.lblMeterID.Text.ToString & "'") > 0 Then
                'delete
                Core.data_execute_nonquery("DELETE FROM [portal.utilitymasters.co.uk].dbo.tlkpMeterNiceNames WHERE varMeterFK = '" & Me.lblMeterID.Text.ToString & "'")

                Me.txtCustomerReference.Text = Nothing
                Me.lblMeterReferenceUpdateOutcome.Text = "Your Ref has been removed."
                Me.lblMeterReferenceUpdateOutcome.ForeColor = Drawing.Color.LimeGreen
            End If


        End If

        Dim strType As String = Me.lblMeterType.Text.ToString
        Dim strCustomerRef As String = Core.data_select_value("SELECT CustomerRef FROM UML_CMS.dbo.uvw_Admin_DisplayAllMeterPoints WHERE ID = '" & Me.lblMeterID.Text.ToString & "'")
        Dim strInternalRef As String = Core.data_select_value("SELECT InternalRef FROM UML_CMS.dbo.uvw_Admin_DisplayAllMeterPoints WHERE ID = '" & Me.lblMeterID.Text.ToString & "'")
        Dim strSiteName As String = Core.data_select_value("SELECT sitename FROM UML_CMS.dbo.uvw_Admin_DisplayAllMeterPoints WHERE ID = '" & Me.lblMeterID.Text.ToString & "'")

        Dim strOutput As String = ""

        If strCustomerRef.Length > 0 Then
            strOutput = strType & "  | " & strCustomerRef & " | " & strSiteName
        Else
            strOutput = strInternalRef & strSiteName
        End If

        Me.lbUserMeters.SelectedItem.Text = strOutput
        LoadUserMeterDetails(Me.lbUserMeters.SelectedItem.Value.ToString)
    End Sub

    Protected Sub btnCustomerReferenceRemove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCustomerReferenceRemove.Click
        If Core.data_select_value("SELECT COUNT(*) FROM [portal.utilitymasters.co.uk].dbo.tlkpMeterNiceNames WHERE varMeterFK = '" & Me.lblMeterID.Text.ToString & "'") > 0 Then
            'delete
            Core.data_execute_nonquery("DELETE FROM [portal.utilitymasters.co.uk].dbo.tlkpMeterNiceNames WHERE varMeterFK = '" & Me.lblMeterID.Text.ToString & "'")
            Me.txtCustomerReference.Text = Nothing
            Me.lblMeterReferenceUpdateOutcome.Text = "Your Ref has been removed."
            Me.lblMeterReferenceUpdateOutcome.ForeColor = Drawing.Color.LimeGreen
        End If



        Dim strType As String = Me.lblMeterType.Text.ToString
        Dim strCustomerRef As String = Core.data_select_value("SELECT CASE WHEN CustomerRef IS NULL THEN '_' ELSE CustomerRef END FROM UML_CMS.dbo.uvw_Admin_DisplayAllMeterPoints WHERE ID = '" & Me.lblMeterID.Text.ToString & "'")
        Dim strInternalRef As String = Core.data_select_value("SELECT InternalRef FROM UML_CMS.dbo.uvw_Admin_DisplayAllMeterPoints WHERE ID = '" & Me.lblMeterID.Text.ToString & "'")
        Dim strSiteName As String = Core.data_select_value("SELECT sitename FROM UML_CMS.dbo.uvw_Admin_DisplayAllMeterPoints WHERE ID = '" & Me.lblMeterID.Text.ToString & "'")

        Dim strOutput As String = ""


        strOutput = strInternalRef & strSiteName

        Me.lbUserMeters.SelectedItem.Text = strOutput
        LoadUserMeterDetails(Me.lbUserMeters.SelectedItem.Value.ToString)

    End Sub

    Protected Sub rblMeterHidden_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rblMeterHidden.SelectedIndexChanged
        If Me.rblMeterHidden.SelectedItem.Value = 0 Then
            Core.data_execute_nonquery("DELETE FROM [portal.utilitymasters.co.uk].dbo.tlkpMetersHidden WHERE varMeterFK = '" & Me.lblMeterID.Text.ToString & "'")
        ElseIf Me.rblMeterHidden.SelectedItem.Value = 1 Then
            Core.data_execute_nonquery("INSERT INTO [portal.utilitymasters.co.uk].dbo.tlkpMetersHidden (varMeterFK, intUserCreatedFK) VALUES('" & Me.lblMeterID.Text.ToString & "', " & MySession.UserID & ")")
        End If
    End Sub
End Class
