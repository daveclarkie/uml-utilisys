﻿<%@ Page Title="UTILISYS - my account" Language="VB" MasterPageFile="~/landing.master" AutoEventWireup="false" CodeFile="Default.aspx.vb" Inherits="myaccount_Default"  %>

<asp:Content ID="Content1" ContentPlaceHolderID="content_right" Runat="Server">

<form id="frmMyAccount" runat="server">

<div style="height:500px;">
 <div class="content_back noleft"  style="left:0; position:absolute; z-index:1;">
     
<asp:Table ID="tblMain" runat="server">
    <asp:TableRow>
        <asp:TableCell Width="100px" VerticalAlign="Top">
        
            <asp:Table ID="tblMenu" runat="server" Width="100%">
                <asp:TableRow>
                    <asp:TableCell> 
                        <asp:Button ID="btnYourDetails" runat="server" Text="Details" CssClass="myaccount_selectedmenu" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell> 
                        <asp:Button ID="btnYourMeters" runat="server" Text="Meters" CssClass="myaccount_unselectedmenu" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
        <asp:TableCell Width="665px">
            <asp:Panel id="pnlUserDetails" runat="server" Visible="false">
            <asp:Table ID="tblUserDetails" runat="server" Width="715px">
                <asp:TableRow>
                    <asp:TableCell VerticalAlign="Top">
                        <asp:Table ID="tblDetails" runat="server" Width="420px">
                            <asp:TableRow>
                                <asp:TableCell Width="150px">
                                    <asp:Label ID="lblUsername" runat="server" Text="Username"></asp:Label>
                                </asp:TableCell>
                                <asp:TableCell Width="270px">
                                    <asp:Label ID="txtUsername" runat="server" Width="250px"></asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>

                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Label ID="lblPassword" runat="server" Text="Password"></asp:Label>
                                </asp:TableCell>
                                <asp:TableCell>
                                    <asp:Button ID="btnResendPassword" runat="server" Width="150px" Height="25px" Text="click here to resend"></asp:Button>
                                    &nbsp;<asp:Label ID="lblResendPassword" runat="server" ForeColor="#6EBB1F" Font-Italic="true"></asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>

                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Label ID="lblForename" runat="server" Text="First Name"></asp:Label>
                                </asp:TableCell>
                                <asp:TableCell>
                                    <asp:TextBox ID="txtForename" runat="server" Width="250px" AutoPostBack="false"></asp:TextBox>
                                </asp:TableCell>
                            </asp:TableRow>
        
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Label ID="lblSurname" runat="server" Text="Surname"></asp:Label>
                                </asp:TableCell>
                                <asp:TableCell>
                                    <asp:TextBox ID="txtSurname" runat="server" Width="250px" AutoPostBack="false"></asp:TextBox>
                                </asp:TableCell>
                            </asp:TableRow>
        
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Label ID="lblJobTitle" runat="server" Text="Job Title"></asp:Label>
                                </asp:TableCell>
                                <asp:TableCell>
                                    <asp:TextBox ID="txtJobTitle" runat="server" Width="250px" AutoPostBack="false"></asp:TextBox>
                                </asp:TableCell>
                            </asp:TableRow>
        
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Label ID="lblContactNumber" runat="server" Text="Contact Number"></asp:Label>
                                </asp:TableCell>
                                <asp:TableCell>
                                    <asp:TextBox ID="txtContactNumber" runat="server" Width="250px" AutoPostBack="false"></asp:TextBox>
                                </asp:TableCell>
                            </asp:TableRow>
        
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Label ID="lblMobileNumber" runat="server" Text="Mobile Number"></asp:Label>
                                </asp:TableCell>
                                <asp:TableCell>
                                    <asp:TextBox ID="txtMobileNumber" runat="server" Width="250px" AutoPostBack="false"></asp:TextBox>
                                </asp:TableCell>
                            </asp:TableRow>
        
                            <asp:TableRow>
                                <asp:TableCell ColumnSpan="2">
                                    <asp:Button ID="btnUpdateDetails" runat="server" Width="400px" Text="update my details" />
                                </asp:TableCell>
                            </asp:TableRow>
                
                            <asp:TableRow>
                                <asp:TableCell ColumnSpan="2">
                                    -----------------------------------------------------------------------------
                                </asp:TableCell>
                            </asp:TableRow>

                    
                            <asp:TableRow VerticalAlign="Top">
                                <asp:TableCell>
                                    <asp:Label ID="lblPacks" runat="server" Text="Your Packs"></asp:Label>
                                </asp:TableCell>
                                <asp:TableCell>
                                    <asp:Image ID="imgPackProcurment" runat="server" Height="20px" Width="20px" />          &nbsp;  <asp:Label ID="lblPacksProcurement" runat="server" Text="Procurement"></asp:Label><br />
                                    <asp:Image ID="imgPackEnergyManagement" runat="server" Height="20px" Width="20px" />    &nbsp;  <asp:Label ID="lblPacksEnergyManagement" runat="server" Text="Energy Management"></asp:Label><br />
                                    <asp:Image ID="imgPackCCA" runat="server" Height="20px" Width="20px" />                 &nbsp;  <asp:Label ID="lblPacksCCA" runat="server" Text="CCA"></asp:Label><br />
                                    <asp:Image ID="imgPackCRC" runat="server" Height="20px" Width="20px" />                 &nbsp;  <asp:Label ID="lblPacksCRC" runat="server" Text="CRC"></asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
        
                            <asp:TableRow VerticalAlign="Top">
                                <asp:TableCell>
                                    <asp:Label ID="lblCompany" runat="server" Text="Your Company"></asp:Label>
                                </asp:TableCell>
                                <asp:TableCell>
                                    <asp:GridView ID="grdCompany" runat="server" DataKeyNames="Company Name" ForeColor="Black" HeaderStyle-Height="0px" AutoGenerateColumns="false" ShowHeader="false">
                                        <Columns>
                                            <asp:BoundField DataField="Company Name" />
                                        </Columns>
                                    </asp:GridView>

                                </asp:TableCell>
                            </asp:TableRow>
                           
                            <asp:TableRow>
                                <asp:TableCell ColumnSpan="2">
                                    -----------------------------------------------------------------------------
                                </asp:TableCell>
                            </asp:TableRow>

                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Label ID="lblLastReport" runat="server" Text="Last Report Viewed"></asp:Label>
                                </asp:TableCell>
                                <asp:TableCell>
                                    <asp:HyperLink ID="lnkLastReport" runat="server" Width="250px"></asp:HyperLink>
                                </asp:TableCell>
                            </asp:TableRow>
                    
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Label ID="lblFavouriteReport" runat="server" Text="Most Viewed Report"></asp:Label>
                                </asp:TableCell>
                                <asp:TableCell>
                                    <asp:HyperLink ID="lnkFavouriteReport" runat="server" Width="250px"></asp:HyperLink>
                                </asp:TableCell>
                            </asp:TableRow>

                            <asp:TableRow>
                                <asp:TableCell>
                            
                                </asp:TableCell>
                                <asp:TableCell>
                                    <asp:Button ID="btnMyFavouriteReports" runat="server" Text="My Favourite Reports" Width="250px" />
                                </asp:TableCell>
                            </asp:TableRow>

                        </asp:Table>
                    </asp:TableCell>

                    <asp:TableCell VerticalAlign="Top">
                        <asp:Table ID="tblUserPortrait" runat="server" Width="200px"  BorderColor="Black" BorderStyle="None" BorderWidth="1px" Height="125px">
        
                            <asp:TableRow Height="22px">
                                <asp:TableCell ColumnSpan="2">
                                    <asp:Label ID="lblDetailsUpdated" runat="server" ForeColor="#6EBB1F" Font-Italic="true"></asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Image ID="imgUserPortrait" style="width:100px; height:125px;" Runat="server" BorderStyle="Solid" />
                                    h: 3.3 cm (125px)<br />
                                    w: 2.6 cm (100px)
                                </asp:TableCell>
                        
                                <asp:TableCell VerticalAlign="Bottom">
                            
                                    <asp:FileUpload ID="imgUpload" runat="server" Width="100px" Height="25px"  />
                                    <asp:Button ID="btnUploadImage" runat="server" Width="100px" Height="25px" Text="Upload"  />
                                    <asp:Button ID="btnRemoveImage" runat="server" Width="100px" Height="25px" Text="Remove"  />
                                </asp:TableCell>
                            </asp:TableRow>

                            
                            <asp:TableRow Height="23px" >
                                <asp:TableCell ColumnSpan="2">
                                    
                                </asp:TableCell>
                            </asp:TableRow>
                                                        
                            <asp:TableRow>
                                <asp:TableCell>
                                    Account Type
                                </asp:TableCell>
                                <asp:TableCell>                                   
                                    <asp:HyperLink ID="lblAccountType" runat="server"></asp:HyperLink>
                                </asp:TableCell>
                            </asp:TableRow>
                    
                            <asp:TableRow Height="23px" VerticalAlign="Bottom">
                                <asp:TableCell ColumnSpan="2">
                                    --------------------------------------
                                </asp:TableCell>
                            </asp:TableRow>
                    
                            <asp:TableRow >
                                <asp:TableCell ColumnSpan="2">
                                     <asp:Label ID="lblAccountManagerTitle" runat="server" Text="Your Account Manager" Font-Bold="true"></asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>

                            <asp:TableRow>
                                <asp:TableCell ColumnSpan="2">
                                    <asp:HyperLink ID="lnkAccountManagerName" runat="server" Text="Forename Surname"></asp:HyperLink>
                                </asp:TableCell>
                            </asp:TableRow>

                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Image ID="imgAccountManager" style="width:100px; height:125px;" Runat="server" BorderStyle="Solid" />
                                </asp:TableCell>
                            </asp:TableRow>
                    
                            <asp:TableRow>
                                <asp:TableCell ColumnSpan="2">
                                    <asp:Table ID="tblAccountManagerDetails" runat="server" >
                                        <asp:TableRow>
                                            <asp:TableCell Width="75px">
                                                <asp:Label ID="lblAccountManagerEmail" runat="server" Text="Email"></asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <asp:HyperLink ID="lnkAccountManagerEmailDetail" runat="server"></asp:HyperLink></asp:TableCell>
                                        </asp:TableRow>
                                
                                        <asp:TableRow>
                                            <asp:TableCell>
                                                <asp:Label ID="lblAccountManagerPhone" runat="server" Text="Phone"></asp:Label></asp:TableCell>
                                            <asp:TableCell>
                                                <asp:HyperLink ID="lnkAccountManagerPhoneDetail" runat="server"></asp:HyperLink></asp:TableCell>
                                        </asp:TableRow>
                                
                                        <asp:TableRow>
                                            <asp:TableCell>
                                                <asp:Label ID="lblAccountManagerMobile" runat="server" Text="Mobile"></asp:Label></asp:TableCell>
                                            <asp:TableCell>
                                                <asp:HyperLink ID="lnkAccountManagerMobileDetail" runat="server"></asp:HyperLink></asp:TableCell>
                                        </asp:TableRow>
                                
                                    </asp:Table>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                        

                </asp:TableRow>

        
                <asp:TableRow>
                    <asp:TableCell Height="10px">
                
                    </asp:TableCell>
                </asp:TableRow>

             </asp:Table>
            </asp:Panel>
            <asp:Panel id="pnlUserMeters" runat="server" Visible="false">
            <asp:Table ID="tblUserMeters" runat="server" Width="715px">
             <asp:TableRow>
                    <asp:TableCell VerticalAlign="Top" Width="360px">
                    Filter: <asp:RadioButtonList Width="350px" ID="rblMeterFilter" runat="server" RepeatLayout="table" RepeatDirection="Horizontal" AutoPostBack="true">
                                        <asp:ListItem Value="All" Text="Show All" Selected="True"></asp:ListItem>
                                        <asp:ListItem Value="A" Text="Elec"></asp:ListItem>
                                        <asp:ListItem Value="B" Text="Gas"></asp:ListItem>
                                        <asp:ListItem Value="C" Text="Sub"></asp:ListItem>
                                        <asp:ListItem Value="D" Text="Virtual"></asp:ListItem>
                                    </asp:RadioButtonList>
                        <asp:ListBox id="lbUserMeters" runat="server" Height="400px" AutoPostBack="true"></asp:ListBox>
                    </asp:TableCell>

                    <asp:TableCell VerticalAlign="Top" Width="355px">
                        <asp:Table ID="tblSelectedMeterDetails" runat="server">
                            <asp:TableRow>
                                <asp:TableCell Height="45px">
                                    
                                </asp:TableCell>
                            </asp:TableRow>

                            <asp:TableRow>
                                <asp:TableCell ColumnSpan="2">
                                    <b>Selected Meter Details</b>
                                </asp:TableCell>
                            </asp:TableRow>

                            <asp:TableRow>
                                <asp:TableCell Width="100px">
                                    ID
                                </asp:TableCell>
                                <asp:TableCell Width="255px">
                                    <asp:Label ID="lblMeterID" runat="server"></asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>

                            <asp:TableRow>
                                <asp:TableCell>
                                    Type
                                </asp:TableCell>
                                <asp:TableCell>
                                    <asp:Label ID="lblMeterType" runat="server"></asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>

                            <asp:TableRow>
                                <asp:TableCell>
                                    Identifier
                                </asp:TableCell>
                                <asp:TableCell>
                                    <asp:Label ID="lblMeterIdentifier" runat="server"></asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>

                            <asp:TableRow>
                                <asp:TableCell>
                                    Channel
                                </asp:TableCell>
                                <asp:TableCell>
                                    <asp:Label ID="lblMeterChannel" runat="server"></asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>

                            <asp:TableRow>
                                <asp:TableCell>
                                    Description
                                </asp:TableCell>
                                <asp:TableCell>
                                    <asp:Label ID="lblMeterDescription" runat="server"></asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>

                            <asp:TableRow>
                                <asp:TableCell>
                                    Half Hour
                                </asp:TableCell>
                                <asp:TableCell>
                                    <asp:Label ID="lblMeterHalfHour" runat="server"></asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>

                            <asp:TableRow>
                                <asp:TableCell>
                                    Your Ref
                                </asp:TableCell>
                                <asp:TableCell>
                                    <asp:TextBox ID="txtCustomerReference" runat="server" Width="245px"></asp:TextBox>
                                    <asp:Button ID="btnCustomerReferenceRemove" runat="server" Width="20px" Text="X"></asp:Button>
                                </asp:TableCell>
                            </asp:TableRow>

                            <asp:TableRow>
                                <asp:TableCell>
                                    Hidden
                                </asp:TableCell>
                                <asp:TableCell>
                                    <asp:RadioButtonList ID="rblMeterHidden" runat="server" RepeatDirection="Horizontal" AutoPostBack="true" >
                                        <asp:ListItem Text="&nbsp;Yes" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="&nbsp;No" Value="0" Selected="True"></asp:ListItem>
                                    </asp:RadioButtonList>
                                    
                                </asp:TableCell>
                            </asp:TableRow>

                            <asp:TableRow>
                                <asp:TableCell>
                                    
                                </asp:TableCell>
                                <asp:TableCell>
                                    <asp:Button ID="btnUpdateCustomerMeterRefence" runat="server" Width="245px" Text="update"></asp:Button>
                                </asp:TableCell>
                            </asp:TableRow>

                            <asp:TableRow>
                                <asp:TableCell ColumnSpan="2" HorizontalAlign="Center">
                                    <asp:Label id="lblMeterReferenceUpdateOutcome" runat="server"></asp:Label>                               
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                        
                    </asp:TableCell>
                        

                </asp:TableRow>
                </asp:Table>
            </asp:Panel>
        </asp:TableCell>
    
        <asp:TableCell Width="50px">
                
        </asp:TableCell>
    </asp:TableRow>

</asp:Table>

     
     
    
     
     
     
     

 </div>
 </div>
</form>
</asp:Content>

