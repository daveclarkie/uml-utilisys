
Partial Class nav_my_help
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        If Not Page.IsPostBack = True Then
            LoadHelp()
        End If

    End Sub

    Private Sub HideMenuItems()
        Me.nav.Visible = False
    End Sub

    Private Sub LoadHelp()

        'HideMenuItems()

        Dim strURL As String = Core.CheckForNullString(Me.Page.Request.Url.PathAndQuery.ToString)

        Me.lnkHelpHome.Text = "My Help Home"
        Me.lnkHelpHome.NavigateUrl = "~/my-help.aspx"
        Me.nav.Visible = False

        Me.lnkHelpHome.Text = "Back to Help Home"


        Dim myConnection As New OleDb.OleDbConnection(Core.CurrentConnectionString)


        Dim strSQL As String = "EXEC spHelp_ListRequests " & MySession.UserID


        Using connection As New SqlConnection(Core.ConnectionString)
            connection.Open()
            Dim command As New SqlCommand(strSQL, connection)
            command.CommandType = CommandType.Text
            Dim reader As SqlDataReader = command.ExecuteReader()

            While reader.Read

                strURL = reader("URL").ToString

                If reader("intDisabled") = 0 Then
                    Me.navOnlineHelp.InnerHtml += "<li class='styleformytreeview_company'><a href='" & strURL & "'>" & Left(reader("varRequestName"), 33) & "</a></li>"
               End If

            End While

        End Using

        myConnection.Close()



    End Sub


End Class
