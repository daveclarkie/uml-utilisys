
Partial Class users
    Inherits System.Web.UI.Page

    Private _mtxtID As TextBox = Nothing
    Private _mstrSearch As String = ""
    Private _mintUsersPerPage As Integer = 20
    Private _mintPage As Integer = 1

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim description As HtmlMeta = New HtmlMeta()
        description.Name = "description"
        description.Content = "Utility Masters customer extranet - View Users"
        Dim keywords As HtmlMeta = New HtmlMeta()
        keywords.Name = "keywords"
        keywords.Content = "admin"
        Page.Header.Controls.Add(description)
        Page.Header.Controls.Add(keywords)

        'for paging
        If Not Request.QueryString("page") Is Nothing Then Me._mintPage = Request.QueryString("page")

        'setup form
        If Not Page.IsPostBack Then
            'get cookie values for paging
            If Not Me.Request.Cookies("um_users_page") Is Nothing Then
                Me._mintUsersPerPage = Me.Request.Cookies("um_users_page").Value
                Me.ddlUsersPerPage.SelectedValue = Me._mintUsersPerPage
            End If
        Else
            Me._mstrSearch = Me.txtSearch.Text
            'reset page as we're going to change Users per page
            Me.Response.Cookies("um_users_page").Value = ddlUsersPerPage.SelectedValue
            Me.Response.Cookies("um_users_page").Expires = Now().AddYears(10)
            Me._mintUsersPerPage = ddlUsersPerPage.SelectedValue
        End If

        'check user level is admin and redirect to their home page if not!
        If Not IIf(Core.data_select_value("select bitAllowAdmin from tblUsers where intUserPK = " & MySession.UserID) = "True", True, False) Then
            Response.Redirect(Core.data_select_value("select varHomepageURL from tblUsers where intUserPK = " & MySession.UserID))
        Else
            SetupForm()
        End If
    End Sub

    Private Sub SetupForm()
        'reset view tabs
        Me.grdUsers.Visible = False
        Me.pnlStatus.Visible = False

        Try
            'fill the gridview that shows all users
            LoadUsers()
        Catch ex As Exception
            Core.SendAdminStatusReport("Error loading User in setup form. Error: " & ex.ToString)
        End Try
    End Sub

    Private Sub LoadUsers()
        Try
            'load all users and Sites
            Dim intTotalRows As Integer
            Dim intPageNumber As Integer = 1
            Me.grdUsers.Visible = True
            Dim objUser As New clsUMUser(0)
            Me.grdUsers.DataSource = objUser.ReadAll("all", "", _mintPage, _mintUsersPerPage, intTotalRows)
            Me.grdUsers.DataBind()
            Me.pages.InnerHtml = ""
            Me.pages2.InnerHtml = ""

            'only show paging controls when more than page size of users returned
            If intTotalRows > _mintUsersPerPage Then
                'previous link
                If _mintPage > 1 Then
                    Me.pages.InnerHtml += "<li><a href='users.aspx?page=" & _mintPage - 1 & "'>previous</a></li>"
                    Me.pages2.InnerHtml = Me.pages.InnerHtml
                End If

                'page links
                Do While intTotalRows > 0
                    'add hyperlinks to results <ul>
                    If intPageNumber = _mintPage Then
                        'different style for current page
                        Me.pages.InnerHtml += "<li><a class='current_page' href='users.aspx?page=" & intPageNumber & "'>" & intPageNumber & "</a></li>"
                    Else
                        Me.pages.InnerHtml += "<li><a href='users.aspx?page=" & intPageNumber & "'>" & intPageNumber & "</a></li>"
                    End If
                    Me.pages2.InnerHtml = Me.pages.InnerHtml
                    intTotalRows = intTotalRows - _mintUsersPerPage
                    intPageNumber += 1
                Loop
                'set last page number
                intPageNumber -= 1

                'next link
                If _mintPage < intPageNumber Then
                    Me.pages.InnerHtml += "<li><a href='users.aspx?page=" & _mintPage + 1 & "'>next</a></li>"
                    Me.pages2.InnerHtml = Me.pages.InnerHtml
                End If
                Me.pages.Visible = True
                Me.pages2.Visible = True
            Else
                'hide paging controls
                Me.pages.Visible = False
                Me.pages2.Visible = False
            End If

            SetStatus(Me.grdUsers.Rows.Count & " users listed.", Core.enmStatus.StatusInfo)
        Catch ex As Exception
            Core.SendAdminStatusReport("Error loading Users and Sites. Error: " & ex.ToString)
        End Try
    End Sub

    Private Sub SetStatus(ByVal Message As String, ByVal StatusType As Core.enmStatus)
        If Len(Message) > 0 Then
            Me.pnlStatus.Visible = True
            If StatusType = Core.enmStatus.StatusError Then
                pnlStatus.GroupingText = "Please correct the following errors:"
                Me.pnlStatus.CssClass = "error"
            Else
                pnlStatus.GroupingText = ""
                Me.pnlStatus.CssClass = "ok"
            End If
            Me.lblStatus.Text = Message
        End If
    End Sub

    Protected Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Response.Redirect("~/user.aspx?action=new&id=0")
    End Sub

    Protected Sub grdUsers_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdUsers.RowDataBound
        'hide id column
        e.Row.Cells(1).Visible = False
    End Sub

    Protected Sub grdUsers_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles grdUsers.RowEditing
        'bug workaround for edit button
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Dim strSearch As String = Me.txtSearch.Text.Trim
        Me.grdUsers.DataSource = Core.data_select("select * from vwUsers where ID = " & IIf(IsNumeric(strSearch), strSearch, "0") & " or Name like('%" & strSearch & "%') or Email like('%" & strSearch & "%')")
        Me.grdUsers.DataBind()
        SetStatus(Me.grdUsers.Rows.Count & " search results.", Core.enmStatus.StatusInfo)
    End Sub
End Class
