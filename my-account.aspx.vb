Partial Class my_account
    Inherits System.Web.UI.Page


    Private _mintCompanyID As Integer = 0

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Request.QueryString("custid") Is Nothing Then

            Dim strSQL As String = "EXEC spPortal_TREEVIEW_TopLevel " & MySession.UserID
            Dim strID As String = Core.data_select_value(strSQL)

            If strID.Length > 0 Then

                Response.Redirect("my-account.aspx?custid=" & strID, True)

            End If


        End If

        Try
            If Not IIf(Core.data_select_value("select bitAllowMyAccount from tblUsers where intUserPK = " & MySession.UserID) = "True", True, False) Then
                Response.Redirect(Core.data_select_value("select RawHomeURL from vwUsers where ID = " & MySession.UserID), True)
            End If

            Dim description As HtmlMeta = New HtmlMeta()
            description.Name = "description"
            description.Content = "Utility Masters customer extranet - My Account"
            Dim keywords As HtmlMeta = New HtmlMeta()
            keywords.Name = "keywords"
            keywords.Content = "my account"
            Page.Header.Controls.Add(description)
            Page.Header.Controls.Add(keywords)

            'load account details
            'SetupForm(Core.CheckForNullInteger(Request.QueryString("custid")))
            If Not Request.QueryString("custid") Is Nothing Then Me._mintCompanyID = Request.QueryString("custid")


            SetupForm(_mintCompanyID)

        Catch ex As Exception
            '
        End Try
    End Sub

    Private Sub SetupForm(ByVal ID As Integer)

        Dim objCompany As New clsUMCompany(ID)

        With objCompany
            Me.txtCompany.Text = .Company
            Me.txtAddress.Text = .AddressLine1
            Me.txtAddress2.Text = .AddressLine2
            Me.txtAddress3.Text = .AddressLine3
            Me.txtAddress4.Text = .AddressLine4
            Me.txtCounty.Text = .County
            Me.txtPostcode.Text = .Postcode
            Me.txtPrimaryContact.Text = .ContactName
            Me.txtSecondaryContact.Text = .ContactName2
            Me.txtPrimaryContactNumber.Text = .ContactPhone
            Me.txtSecondaryContactNumber.Text = .ContactPhone2
            Me.txtPrimaryContactEmail.Text = .ContactEmail
            Me.txtSecondaryContactEmail.Text = .ContactEmail2

            'You get the idea - the last few need adding... also to the class clsUMCompany!
            Me.txtVATReg.Text = .VATReg
            'Me.txtActiveSites.Text = .ActiveSites
            'Me.txtInactiveSites.Text = .InActiveSites
            Me.txtAccountManager.Text = .AccountManager
            Me.txtCompanyNumber.Text = .CompanyNumber
        End With
    End Sub



End Class
