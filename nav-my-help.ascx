<%@ Control Language="VB" AutoEventWireup="false" CodeFile="nav-my-help.ascx.vb" Inherits="nav_my_help" %>
<div id="left_nav_help" class="left_nav">
    <div>
    
        <asp:hyperlink runat="server" id="lnkMyHelp" navigateurl="~/my-help.aspx" cssclass="nav_home" tooltip="Your Help" />
        
        <ul class="nav" runat="server" id="nav"></ul>
        <ul class="styleformytreeview" runat="server" id="navOnlineHelp"></ul>
        <asp:Literal ID="litNewFormatMenu" runat="server"></asp:Literal>
        <h2 class="nav_heading">
            <asp:hyperlink runat="server" id="lnkHelpHome" ForeColor="#009530"  navigateurl="~/my-help.aspx" tooltip="Your Help" Text="My Help Home"></asp:hyperlink>
        </h2>
        
    </div>
    
</div>


 
    