
Partial Class maintenance
    Inherits System.Web.UI.Page

    Private _mtxtID As TextBox = Nothing
    Private _mstrSearch As String = ""
    Private _mstrTab As String = "reports"
    Private _mstrAction As String = "edit"
    Private _mintID As Integer = 0
    Private _mintSitesPerPage As Integer = 20 'default
    Private _mintPage As Integer = 1

    'INITIALISATION

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim description As HtmlMeta = New HtmlMeta()
        description.Name = "description"
        description.Content = "Utility Masters customer extranet - Edit User"
        Dim keywords As HtmlMeta = New HtmlMeta()
        keywords.Name = "keywords"
        keywords.Content = "admin"
        Page.Header.Controls.Add(description)
        Page.Header.Controls.Add(keywords)

        'for paging etc
        If Not Request.QueryString("page") Is Nothing Then Me._mintPage = Request.QueryString("page")
        If Not Request.QueryString("tab") Is Nothing Then Me._mstrTab = Request.QueryString("tab")
        If Not Request.QueryString("id") Is Nothing Then Me._mintID = Request.QueryString("id")
        If Not Request.QueryString("action") Is Nothing Then Me._mstrAction = Request.QueryString("action")

        'setup form
        If Not Page.IsPostBack Then

            'get cookie values for paging
        Else
            Me.Response.Cookies("um_sites_page").Expires = Now().AddYears(10)
        End If

        'check user level is admin and redirect to their home page if not!
        If Not IIf(Core.data_select_value("select bitAllowAdmin from tblUsers where intUserPK = " & MySession.UserID) = "True", True, False) Then
            Response.Redirect(Core.data_select_value("select varHomepageURL from tblUsers where intUserPK = " & MySession.UserID))
        Else

            SetupForm()
        End If
    End Sub

    Private Sub SetupForm()
        'reset view tabs

        Me.pnlStatus.Visible = False
        Me.pnlReports.Visible = False
        Me.lnkReports.CssClass = ""

        Try
            Me.lnkReports.NavigateUrl = "~/maintenance.aspx?tab=reports&action=view&id=" & Me._mintID.ToString

            'display the correct panel
            Dim pnl As Panel
            pnl = Me.Form.FindControl("pnl" & StrConv(_mstrTab, VbStrConv.ProperCase))
            pnl.Visible = True
            'select correct tab
            Dim lnk As HyperLink
            lnk = Me.Form.FindControl("lnk" & StrConv(_mstrTab, VbStrConv.ProperCase))
            lnk.CssClass = "current"

            Select Case _mstrTab
                Case "reports"
                    'ResetNewReportForm()
                    LoadAllReports()
            End Select
        Catch ex As Exception
            Core.SendAdminStatusReport("Error loading User in setup form. Error: " & ex.ToString)
        End Try
    End Sub

    'DATA

    Private Sub LoadAllReports()
        Try
            'load all active reports
            Dim objMaintenance As New clsUMMaintenance()
            Me.grdUsersReports.DataSource = objMaintenance.ReadAllReports()
            Me.grdUsersReports.DataBind()
        Catch ex As Exception
            Core.SendAdminStatusReport("Error loading users Reports. Error: " & ex.ToString)
        End Try

        Try
            'load all inactive reports
            Dim objMaintenance As New clsUMMaintenance()
            Me.grdInactiveReports.DataSource = objMaintenance.ReadAllInactiveReports()
            Me.grdInactiveReports.DataBind()
        Catch ex As Exception
            Core.SendAdminStatusReport("Error loading users Reports. Error: " & ex.ToString)
        End Try

    End Sub

    'ACTIVE REPORTS

    Protected Sub grdUsersReports_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdUsersReports.RowCommand
        Try
            pnlStatus.Visible = False
            'must be deleting a Report from this User
            Try
                If grdUsersReports.Rows.Count > 0 Then
                    Dim objMaintenance As New clsUMMaintenance()
                    objMaintenance.DeleteReport(Integer.Parse(grdUsersReports.Rows(e.CommandArgument).Cells(1).Text))
                    'deleted OK
                    SetStatus("REPORT DELETED", Core.enmStatus.StatusInfo)
                    Me.SetupForm()
                End If
            Catch ex As Exception
                SetStatus("ERROR DELETING REPORTS", Core.enmStatus.StatusError)
            End Try
        Catch ex As Exception
            Core.SendAdminStatusReport("Error in grdUsersReports.RowCommand. Error: " & ex.ToString)
        End Try
    End Sub

    Protected Sub grdUsersReports_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdUsersReports.RowDataBound
        e.Row.Cells(1).Visible = False

    End Sub

    Protected Sub grdUsersReports_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles grdUsersReports.RowDeleting
        'bug workaround for deleting Users Reports
    End Sub

    'INACTIVE REPORTS

    Protected Sub grdInactiveReports_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdInactiveReports.RowCommand
        Try
            pnlStatus.Visible = False
            'must be deleting a Report from this User
            Try
                If grdInactiveReports.Rows.Count > 0 Then
                    Dim objMaintenance As New clsUMMaintenance()
                    objMaintenance.AddReport(Integer.Parse(grdInactiveReports.Rows(e.CommandArgument).Cells(1).Text))
                    'Added OK
                    SetStatus("REPORT ADDED", Core.enmStatus.StatusInfo)
                    Me.SetupForm()
                End If
            Catch ex As Exception
                SetStatus("ERROR ADDING REPORTS", Core.enmStatus.StatusError)
            End Try
        Catch ex As Exception
            Core.SendAdminStatusReport("Error in grdInactiveReports.RowCommand. Error: " & ex.ToString)
        End Try
    End Sub

    Protected Sub grdInactiveReports_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdInactiveReports.RowDataBound
        e.Row.Cells(1).Visible = False

    End Sub

    Protected Sub grdInactiveReports_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles grdInactiveReports.RowDeleting
        'bug workaround for deleting Inactive Reports
    End Sub

    ' NEW REPORT

    Private Sub ResetNewReportForm()

        Me.lblNewReportError.Text = Nothing

        Me.txtNewReportName.Text = Nothing
        Me.txtNewOfflineURL.Text = Nothing
        Me.txtNewOnlineURL.Text = Nothing

        Me.txtNewReportName.BackColor = Nothing
        Me.txtNewOfflineURL.BackColor = Nothing
        Me.txtNewOnlineURL.BackColor = Nothing



    End Sub

    Protected Sub btnNewAddReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNewAddReport.Click

        Me.lblNewReportError.Text = Nothing
        Me.txtNewReportName.BackColor = Nothing
        Me.txtNewOfflineURL.BackColor = Nothing
        Me.txtNewOnlineURL.BackColor = Nothing

        If Me.txtNewReportName.Text.Length = 0 Then
            Me.lblNewReportError.Text = "* please enter a report name"
            Me.txtNewReportName.BackColor = Drawing.Color.Red
            Me.txtNewReportName.Focus()
        End If

        If Me.txtNewOnlineURL.Text.Length = 0 Then
            Me.lblNewReportError.Text = "* the report must have an online url"
            Me.txtNewOnlineURL.BackColor = Drawing.Color.Red
            Me.txtNewOnlineURL.Focus()
        End If

        If Me.ddlNewReportType.SelectedItem.Text = "Offline" Then

            If Me.txtNewOfflineURL.Text.Length = 0 Then
                Me.lblNewReportError.Text = "* you must specify a location for the offline report"
                Me.txtNewOfflineURL.BackColor = Drawing.Color.Red
                Me.txtNewOfflineURL.Focus()
            End If
        End If

        If Core.data_select_value("SELECT count(*) FROM tlkpReportTypes WHERE varReportTypeName = '" & Me.txtNewReportName.Text & "'") > 0 Then
            Me.lblNewReportError.Text = "* report with this name already exists"
            Me.txtNewReportName.BackColor = Drawing.Color.Red
            Me.txtNewReportName.Focus()
        End If

        If Me.lblNewReportError.Text.Length = 0 Then
            Dim strSQL As String = ""

            strSQL &= "INSERT INTO tlkpReportTypes (varReportTypeName, varDefaultURL, varReportType, varOnlineURL, intUserCreatedByFK, intUserModifiedByFK) "
            strSQL &= "VALUES ('" & Me.txtNewReportName.Text & "', '" & Me.txtNewOfflineURL.Text & "', '" & Me.ddlNewReportType.SelectedItem.Text & "', '" & Me.txtNewOnlineURL.Text & "', " & MySession.UserID & ", " & MySession.UserID & ") "

            Core.data_execute_nonquery(strSQL)
            SetupForm()
            ResetNewReportForm()
        End If

    End Sub

    Protected Sub ddlNewReportType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlNewReportType.SelectedIndexChanged

        If Me.ddlNewReportType.SelectedItem.Text = "Offline" Then
            Me.trOfflineURL.Visible = True
        Else
            Me.trOfflineURL.Visible = False
        End If

    End Sub



    'HOUSEKEEPING

    Private Sub SetStatus(ByVal Message As String, ByVal StatusType As Core.enmStatus)
        If Len(Message) > 0 Then
            Me.pnlStatus.Visible = True
            If StatusType = Core.enmStatus.StatusError Then
                Me.pnlStatus.Attributes.Add("class", "error")
            Else
                Me.pnlStatus.Attributes.Add("class", "ok")
            End If
            Me.lblStatus.Text = Message
        End If
    End Sub

End Class
