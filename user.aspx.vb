Imports System
Imports System.Security.Cryptography

Partial Class user
    Inherits System.Web.UI.Page

    Private _mtxtID As TextBox = Nothing
    Private _mstrSearch As String = ""
    Private _mstrTab As String = "master"
    Private _mstrAction As String = "edit"
    Private _mintID As Integer = 0
    Private _mintSitesPerPage As Integer = 20 'default
    Private _mintPage As Integer = 1

    'INITIALISATION

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim description As HtmlMeta = New HtmlMeta()
        description.Name = "description"
        description.Content = "Utility Masters customer extranet - Edit User"
        Dim keywords As HtmlMeta = New HtmlMeta()
        keywords.Name = "keywords"
        keywords.Content = "admin"
        Page.Header.Controls.Add(description)
        Page.Header.Controls.Add(keywords)

        'for paging etc
        If Not Request.QueryString("page") Is Nothing Then Me._mintPage = Request.QueryString("page")
        If Not Request.QueryString("tab") Is Nothing Then Me._mstrTab = Request.QueryString("tab")
        If Not Request.QueryString("id") Is Nothing Then Me._mintID = Request.QueryString("id")
        If Not Request.QueryString("action") Is Nothing Then Me._mstrAction = Request.QueryString("action")

        'setup form
        If Not Page.IsPostBack Then
            LoadDropDowns()
            'get cookie values for paging
            If Not Me.Request.Cookies("um_sites_page") Is Nothing Then
                If Me.Request.Cookies("um_sites_page").Value = "" Then Me.Request.Cookies("um_sites_page").Value = _mintSitesPerPage
                Me._mintSitesPerPage = Me.Request.Cookies("um_sites_page").Value
                Me.ddlSitesPerPage.SelectedValue = Me._mintSitesPerPage
            End If
        Else
            Me._mstrSearch = Me.txtSearch.Text
            'reset page as we're going to change Users per page
            Me.Response.Cookies("um_sites_page").Value = ddlSitesPerPage.SelectedValue
            Me.Response.Cookies("um_sites_page").Expires = Now().AddYears(10)
            Me._mintSitesPerPage = ddlSitesPerPage.SelectedValue
        End If

        'check user level is admin and redirect to their home page if not!
        If Not IIf(Core.data_select_value("select bitAllowAdmin from tblUsers where intUserPK = " & MySession.UserID) = "True", True, False) Then
            Response.Redirect(Core.data_select_value("select varHomepageURL from tblUsers where intUserPK = " & MySession.UserID))
        Else

            If Request.QueryString("tab") = "packs" And Request.QueryString("action") = "edit" Then
                If Request.QueryString("method") = "add" Then
                    'Core.data_execute_nonquery("INSERT INTO [portal.utilitymasters.co.uk].dbo.tblUsersPacks (intUserFK, intPackFK) VALUES (" & Me._mintID & ", " & Request.QueryString("packid") & ")")
                    Core.data_execute_nonquery("EXEC [portal.utilitymasters.co.uk].dbo.usp_Portal_Packs_Add " & Me._mintID & ", " & Request.QueryString("packid"))

                ElseIf Request.QueryString("method") = "remove" Then
                    'Core.data_execute_nonquery("DELETE FROM [portal.utilitymasters.co.uk].dbo.tblUsersPacks WHERE intUserFK = " & Me._mintID & " AND intPackFK = " & Request.QueryString("packid"))
                    Core.data_execute_nonquery("EXEC [portal.utilitymasters.co.uk].dbo.usp_Portal_Packs_Remove " & Me._mintID & ", " & Request.QueryString("packid"))
                End If


            End If


            Me.lnkPacks.NavigateUrl = "~/user.aspx?tab=packs&action=view&id=" & Me._mintID.ToString

            SetupForm()
        End If
    End Sub

    Private Sub SetupForm()
        'reset view tabs
        Me.grdSites.Visible = False
        Me.pnlStatus.Visible = False
        Me.pnlMaster.Visible = False
        Me.pnlCompanies.Visible = False
        Me.pnlGroups.Visible = False
        Me.pnlSites.Visible = False
        Me.pnlSystems.Visible = False
        Me.pnlReports.Visible = False
        Me.pnlPacks.Visible = False



        Me.lnkMaster.CssClass = ""
        Me.lnkCompanies.CssClass = ""
        Me.lnkGroups.CssClass = ""
        Me.lnkSites.CssClass = ""
        Me.lnkReports.CssClass = ""
        Me.lnkPacks.CssClass = ""

        Try
            Me.lnkMaster.NavigateUrl = "~/user.aspx?tab=master&id=" & Me._mintID.ToString
            Me.lnkCompanies.NavigateUrl = "~/user.aspx?tab=companies&action=view&id=" & Me._mintID.ToString
            Me.lnkGroups.NavigateUrl = "~/user.aspx?tab=groups&action=view&id=" & Me._mintID.ToString
            Me.lnkSites.NavigateUrl = "~/user.aspx?tab=sites&action=view&id=" & Me._mintID.ToString
            Me.lnkReports.NavigateUrl = "~/user.aspx?tab=reports&action=view&id=" & Me._mintID.ToString
            Me.lnkPacks.NavigateUrl = "~/user.aspx?tab=packs&action=view&id=" & Me._mintID.ToString

            'display the correct panel
            Dim pnl As Panel
            pnl = Me.Form.FindControl("pnl" & StrConv(_mstrTab, VbStrConv.ProperCase))
            pnl.Visible = True
            'select correct tab
            Dim lnk As HyperLink
            lnk = Me.Form.FindControl("lnk" & StrConv(_mstrTab, VbStrConv.ProperCase))
            lnk.CssClass = "current"

            Select Case _mstrTab
                Case "master"
                    'must be working on user details screen
                    Select Case _mstrAction
                        Case "edit"
                            Me.btnSave.Visible = True
                            Me.btnNew.Visible = True
                            If Not _mtxtID Is Nothing Then
                                Me._mtxtID.ReadOnly = True
                                Me.btnDisable.Visible = True
                            End If
                            'if editing then load the selected user
                            LoadUser()
                        Case "new"
                            'reset controls ready to add new user
                            Me.txtID.Text = 0
                            Me.txtForename.Text = ""
                            Me.txtSurname.Text = ""
                            Me.txtEmail.Text = ""
                            Me.txtPassword.Text = ""
                            Me.txtNotes.Text = ""
                            Me.ddlPageTypes.SelectedValue = 1
                            Me.chkAllowMySites.Checked = False
                            Me.chkAllowMyAccount.Checked = False
                            Me.chkAllowMyReports.Checked = False
                            Me.chkAdmin.Checked = False
                            Me.btnSave.Enabled = True
                            Me.btnSaveClose.Enabled = True
                            Me.btnCancel.Enabled = True
                            Me.btnNew.Enabled = False
                            Me.txtForename.Focus()
                            If Not _mtxtID Is Nothing Then
                                Me._mtxtID.Text = 0
                                Me.btnDisable.Visible = False
                            End If
                            Me.lnkCompanies.Visible = False
                            Me.lnkGroups.Visible = False
                            Me.lnkSites.Visible = False
                            Me.lnkReports.Visible = False
                        Case "save"
                            Me._mstrAction = "edit"

                    End Select
                    _mtxtID = Me.Form.FindControl("txtID")
                    'if adding hide the ID box (not required)
                    Me.UserID.Visible = (_mstrAction <> "edit")
                Case "companies"
                    LoadUsersCompanies()
                    LoadCompanies()
                Case "groups"
                    LoadUsersGroups()
                    LoadGroups()
                Case "sites"
                    LoadUsersSites()
                    LoadSites()
                Case "systems"
                    LoadUsersSystems()
                    LoadSystems()
                Case "reports"
                    LoadUsersReports()
                    LoadReports()
                Case "packs"
                    LoadUsersPacks()

            End Select
        Catch ex As Exception
            Core.SendAdminStatusReport("Error loading User in setup form. Error: " & ex.ToString)
        End Try
    End Sub

    'DATA

    Private Sub LoadUser()
        Try
            'load specific User
            Dim objUser As New clsUMUser(_mintID)
            With objUser
                Me.txtID.Text = .ID
                Me.txtForename.Text = .Forename
                Me.txtSurname.Text = .Surname
                Me.txtEmail.Text = .Email
                Me.txtPassword.Text = .Password
                Me.ddlPageTypes.SelectedValue = .HomepageURL
                Me.txtNotes.Text = .Notes
                Me.chkAdmin.Checked = .Admin
                Me.chkAllowMyAccount.Checked = .AllowMyAccount
                Me.chkAllowMyReports.Checked = .AllowMyReports
                Me.chkAllowMySites.Checked = .AllowMySites

                'if it's any other mode apart from a new User, show current Sites
                If _mstrAction <> "new" Then
                    Me.grdUsersSites.DataSource = objUser.Sites
                    Me.grdUsersSites.DataBind()
                    Me.grdUsersSites.Visible = True
                End If
                Me.txtForename.Focus()
            End With
        Catch ex As Exception
            Core.SendAdminStatusReport("Error loading User. Error: " & ex.ToString)
        End Try
    End Sub

    Private Sub LoadCompanies()
        Try
            'load all companies
            Me.grdCompanies.DataSource = Core.data_select("select * from vwCompanies WHERE [ID] not in (SELECT intCompanyFK as ID FROM tblUsersCompanies WHERE intUserFK = " & Me._mintID & ")  ORDER BY [company name] asc")
            Me.grdCompanies.DataBind()
            Me.grdCompanies.Visible = True
            SetStatus(Me.grdCompanies.Rows.Count & " companies available to add to this user.", Core.enmStatus.StatusInfo)
        Catch ex As Exception
            Core.SendAdminStatusReport("Error loading companies. Error: " & ex.ToString)
        End Try
    End Sub

    Private Sub LoadUsersCompanies()
        Try
            'load all companies available to this user
            Dim objUser As New clsUMUser(_mintID)
            Me.grdUsersCompanies.DataSource = objUser.Companies
            Me.grdUsersCompanies.DataBind()
        Catch ex As Exception
            Core.SendAdminStatusReport("Error loading users companies. Error: " & ex.ToString)
        End Try
    End Sub

    Private Sub LoadGroups()
        Try
            'load all Groups
            Me.grdGroups.DataSource = Core.data_select("select * from vwCompaniesGroups WHERE [Group ID] not in (SELECT intGroupFK as ID FROM tblUsersGroups WHERE intUserFK = " & Me._mintID & ") AND [ID] IN (SELECT intCompanyFK as ID FROM tblUsersCompanies WHERE intUserFK = " & Me._mintID & ")")
            Me.grdGroups.DataBind()
            Me.grdGroups.Visible = True
            SetStatus(Me.grdGroups.Rows.Count & " Groups available to add to this user.", Core.enmStatus.StatusInfo)
        Catch ex As Exception
            Core.SendAdminStatusReport("Error loading Groups. Error: " & ex.ToString)
        End Try
    End Sub

    Private Sub LoadUsersGroups()
        Try
            'load all Groups available to this user
            Dim objUser As New clsUMUser(_mintID)
            Me.grdUsersGroups.DataSource = objUser.Groups
            Me.grdUsersGroups.DataBind()
        Catch ex As Exception
            Core.SendAdminStatusReport("Error loading users Groups. Error: " & ex.ToString)
        End Try
    End Sub

    Private Sub LoadSites()
        Try
            'load all users and Sites
            Dim intTotalRows As Integer
            Dim intPageNumber As Integer = 1
            Me.grdSites.Visible = True
            Dim objUser As New clsUMUser(_mintID)
            Me.grdSites.DataSource = objUser.ReadAllSites("all", "", _mintPage, _mintSitesPerPage, intTotalRows)
            Me.grdSites.DataBind()
            Me.pages.InnerHtml = ""
            Me.pages2.InnerHtml = ""

            'only show paging controls when more than page size of Sites returned
            If intTotalRows > _mintSitesPerPage Then
                'previous link
                If _mintPage > 1 Then
                    Me.pages.InnerHtml += "<li><a href='user.aspx?tab=sites&page=" & _mintPage - 1 & "&id=" & _mintID & "'>previous</a></li>"
                    Me.pages2.InnerHtml = Me.pages.InnerHtml
                End If

                'page links
                Do While intTotalRows > 0
                    'add hyperlinks to results <ul>
                    If intPageNumber = _mintPage Then
                        'different style for current page
                        Me.pages.InnerHtml += "<li><a class='current_page' href='user.aspx?tab=sites&page=" & intPageNumber & "&id=" & _mintID & "'>" & intPageNumber & "</a></li>"
                    Else
                        Me.pages.InnerHtml += "<li><a href='user.aspx?tab=sites&page=" & intPageNumber & "&id=" & _mintID & "'>" & intPageNumber & "</a></li>"
                    End If
                    Me.pages2.InnerHtml = Me.pages.InnerHtml
                    intTotalRows = intTotalRows - _mintSitesPerPage
                    intPageNumber += 1
                Loop
                'set last page number
                intPageNumber -= 1

                'next link
                If _mintPage < intPageNumber Then
                    Me.pages.InnerHtml += "<li><a href='user.aspx?tab=sites&page=" & _mintPage + 1 & "&id=" & _mintID & "'>next</a></li>"
                    Me.pages2.InnerHtml = Me.pages.InnerHtml
                End If
                Me.pages.Visible = True
                Me.pages2.Visible = True
            Else
                'hide paging controls
                Me.pages.Visible = False
                Me.pages2.Visible = False
            End If


            SetStatus(intTotalRows & " sites listed.", Core.enmStatus.StatusInfo)
        Catch ex As Exception
            Core.SendAdminStatusReport("Error loading Sites. Error: " & ex.ToString)
        End Try

        Me.pages.Visible = False

    End Sub

    Private Sub LoadUsersSites()
        Try
            'load all Groups available to this user
            Dim objUser As New clsUMUser(_mintID)
            Me.grdUsersSites.DataSource = objUser.Sites
            Me.grdUsersSites.DataBind()
        Catch ex As Exception
            Core.SendAdminStatusReport("Error loading users Sites. Error: " & ex.ToString)
        End Try
    End Sub


    Private Sub LoadSystems()
        Try
            'load all users and Sites
            Dim intTotalRows As Integer
            Dim intPageNumber As Integer = 1
            Me.grdSystems.Visible = True
            Dim objUser As New clsUMUser(_mintID)
            Me.grdSystems.DataSource = objUser.ReadAllSystems(intTotalRows)
            Me.grdSystems.DataBind()

            SetStatus(intTotalRows & " systems listed.", Core.enmStatus.StatusInfo)

        Catch ex As Exception
            Core.SendAdminStatusReport("Error loading Systems. Error: " & ex.ToString)
        End Try

        Me.pages.Visible = False

    End Sub

    Private Sub LoadUsersSystems()
        Try
            'load all Groups available to this user
            Dim objUser As New clsUMUser(_mintID)
            Me.grdUsersSystems.DataSource = objUser.Systems
            Me.grdUsersSystems.DataBind()
        Catch ex As Exception
            Core.SendAdminStatusReport("Error loading users Systems. Error: " & ex.ToString)
        End Try
    End Sub


    Private Sub LoadReports()
        Try
            'load all users and Sites
            Dim intTotalRows As Integer
            Dim intPageNumber As Integer = 1
            Me.grdReports.Visible = True
            Dim objUser As New clsUMUser(_mintID)
            Me.grdReports.DataSource = objUser.ReadAllReports(intTotalRows)
            Me.grdReports.DataBind()

            SetStatus(intTotalRows & " reports listed.", Core.enmStatus.StatusInfo)

        Catch ex As Exception
            Core.SendAdminStatusReport("Error loading Reports. Error: " & ex.ToString)
        End Try

        Me.pages.Visible = False

    End Sub

    Private Sub LoadUsersReports()
        Try
            'load all Groups available to this user
            Dim objUser As New clsUMUser(_mintID)
            Me.grdUsersReports.DataSource = objUser.Reports
            Me.grdUsersReports.DataBind()
        Catch ex As Exception
            Core.SendAdminStatusReport("Error loading users Reports. Error: " & ex.ToString)
        End Try
    End Sub


    Private Sub LoadUsersPacks()

        Try
            'load all Groups available to this user
            Dim objUser As New clsUMUser(_mintID)

            Dim intProcurementPack As Integer = Core.data_select_value("SELECT Count(*) FROM [portal.utilitymasters.co.uk].dbo.tblUsersPacks WHERE intUserFK = " & _mintID & " AND intPackFK = 1")
            If intProcurementPack > 0 Then
                Me.lnkReports1.Attributes.Add("class", "packs_Procurement_Remove")
                Me.lnkReports1.HRef = "user.aspx?tab=packs&action=edit&id=" & objUser.ID & "&method=remove&packid=1"
            Else
                Me.lnkReports1.Attributes.Add("class", "packs_Procurement")
                Me.lnkReports1.HRef = "user.aspx?tab=packs&action=edit&id=" & objUser.ID & "&method=add&packid=1"
            End If

            Dim intEnergyManagementPack As Integer = Core.data_select_value("SELECT Count(*) FROM [portal.utilitymasters.co.uk].dbo.tblUsersPacks WHERE intUserFK = " & _mintID & " AND intPackFK = 2")
            If intEnergyManagementPack > 0 Then
                Me.lnkReports2.Attributes.Add("class", "packs_EnergyMangement_Remove")
                Me.lnkReports2.HRef = "user.aspx?tab=packs&action=edit&id=" & objUser.ID & "&method=remove&packid=2"
            Else
                Me.lnkReports2.Attributes.Add("class", "packs_EnergyMangement")
                Me.lnkReports2.HRef = "user.aspx?tab=packs&action=edit&id=" & objUser.ID & "&method=add&packid=2"
            End If

            Dim intCCAPack As Integer = Core.data_select_value("SELECT Count(*) FROM [portal.utilitymasters.co.uk].dbo.tblUsersPacks WHERE intUserFK = " & _mintID & " AND intPackFK = 3")
            If intCCAPack > 0 Then
                Me.lnkReports3.Attributes.Add("class", "packs_CCA_Remove")
                Me.lnkReports3.HRef = "user.aspx?tab=packs&action=edit&id=" & objUser.ID & "&method=remove&packid=3"
            Else
                Me.lnkReports3.Attributes.Add("class", "packs_CCA")
                Me.lnkReports3.HRef = "user.aspx?tab=packs&action=edit&id=" & objUser.ID & "&method=add&packid=3"
            End If

            Dim intCRCPack As Integer = Core.data_select_value("SELECT Count(*) FROM [portal.utilitymasters.co.uk].dbo.tblUsersPacks WHERE intUserFK = " & _mintID & " AND intPackFK =4 ")
            If intCRCPack > 0 Then
                Me.lnkReports4.Attributes.Add("class", "packs_CRC_Remove")
                Me.lnkReports4.HRef = "user.aspx?tab=packs&action=edit&id=" & objUser.ID & "&method=remove&packid=4"
            Else
                Me.lnkReports4.Attributes.Add("class", "packs_CRC")
                Me.lnkReports4.HRef = "user.aspx?tab=packs&action=edit&id=" & objUser.ID & "&method=add&packid=4"
            End If








        Catch ex As Exception
            Core.SendAdminStatusReport("Error loading users Packs. Error: " & ex.ToString)
        End Try

    End Sub

    Private Sub SaveMe()
        Try
            'validate form
            If ValidateMe() Then
                'save user details
                Dim objUser As New clsUMUser(Integer.Parse(Me.txtID.Text), _
                Me.txtForename.Text.Trim, _
                Me.txtSurname.Text.Trim, _
                Me.txtEmail.Text.Trim, _
                Me.txtPassword.Text, _
                Me.ddlPageTypes.SelectedValue, _
                Me.chkAllowMySites.Checked, _
                Me.chkAllowMyReports.Checked, _
                Me.chkAllowMyAccount.Checked, _
                Me.chkAdmin.Checked, _
                Me.txtNotes.Text.Trim, _
                MySession.UserID)
                If objUser.Save() Then
                    _mintID = objUser.ID
                    SetupForm()
                    SetStatus("USER DETAILS SAVED", Core.enmStatus.StatusInfo)


                    Me.lnkCompanies.Visible = True
                    Me.lnkGroups.Visible = True
                    Me.lnkSites.Visible = True

                Else
                    SetStatus("ERROR SAVING USER DETAILS", Core.enmStatus.StatusError)
                End If
            End If
        Catch ex As Exception
            SetStatus("ERROR SAVING USER DETAILS", Core.enmStatus.StatusError)
            Core.SendAdminStatusReport("Error saving User. Error: " & ex.ToString)
        End Try
    End Sub

    'BUTTONS

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        SaveMe()
    End Sub

    Protected Sub btnSaveClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveClose.Click
        SaveMe()
        Response.Redirect("users.aspx")
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect("users.aspx")
    End Sub

    Protected Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNew.Click
        _mstrAction = "new"
        SetupForm()
    End Sub

    Protected Sub btnDisable_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDisable.Click
        Dim objUser As New clsUMUser(Integer.Parse(Me.txtID.Text))
        objUser.Disable()
        SetupForm()
    End Sub

    'COMPANIES

    Protected Sub grdCompanies_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdCompanies.RowDataBound
        e.Row.Cells(1).Visible = False
    End Sub

    Protected Sub grdCompanies_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdCompanies.RowCommand
        Try
            'add company to user currently being edited
            Dim objUser As New clsUMUser(_mintID)
            objUser.AddCompany(Integer.Parse(grdCompanies.Rows(e.CommandArgument).Cells(1).Text))
            SetStatus("COMPANY ADDED SUCCESSFULLY", Core.enmStatus.StatusInfo)
            Me.grdUsersCompanies.DataSource = objUser.Companies
            Me.grdUsersCompanies.DataBind()
            LoadCompanies()
        Catch pk_ex As System.Data.SqlClient.SqlException
            SetStatus(grdCompanies.Rows(e.CommandArgument).Cells(2).Text & " is already viewable by this user", Core.enmStatus.StatusError)
        Catch ex As Exception
            Core.SendAdminStatusReport("Error in grdCompanies.RowCommand. Error: " & ex.ToString)
        End Try
    End Sub

    Protected Sub grdUsersCompanies_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdUsersCompanies.RowCommand
        'delete company from user
        Try
            Dim objUser As New clsUMUser(_mintID)
            objUser.DeleteCompany(Integer.Parse(grdUsersCompanies.Rows(e.CommandArgument).Cells(1).Text))
            SetStatus("COMPANY DELETED SUCCESSFULLY", Core.enmStatus.StatusInfo)
            Me.grdUsersCompanies.DataSource = objUser.Companies
            Me.grdUsersCompanies.DataBind()
        Catch pk_ex As System.Data.SqlClient.SqlException
            SetStatus(grdCompanies.Rows(e.CommandArgument).Cells(2).Text & " is already viewable by this user", Core.enmStatus.StatusError)
        Catch ex As Exception
            Core.SendAdminStatusReport("Error in grdUsersCompanies.RowCommand. Error: " & ex.ToString)
        End Try
    End Sub

    Protected Sub grdUsersCompanies_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdUsersCompanies.RowDataBound
        e.Row.Cells(1).Visible = False
        e.Row.Cells(2).Visible = False
    End Sub

    Protected Sub grdUsersCompanies_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles grdUsersCompanies.RowDeleting
        '
    End Sub

    'GROUPS

    Protected Sub grdGroups_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdGroups.RowCommand
        Try
            'add group to user currently being edited
            Dim objUser As New clsUMUser(_mintID)
            objUser.Addgroup(Integer.Parse(grdGroups.Rows(e.CommandArgument).Cells(1).Text))
            SetStatus("GROUP ADDED SUCCESSFULLY", Core.enmStatus.StatusInfo)
            Me.grdUsersGroups.DataSource = objUser.Groups
            Me.grdUsersGroups.DataBind()
            LoadGroups()
        Catch pk_ex As System.Data.SqlClient.SqlException
            SetStatus(grdGroups.Rows(e.CommandArgument).Cells(2).Text & " is already viewable by this user", Core.enmStatus.StatusError)
        Catch ex As Exception
            Core.SendAdminStatusReport("Error in grdGroups.RowCommand. Error: " & ex.ToString)
        End Try
    End Sub

    Protected Sub grdGroups_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdGroups.RowDataBound
        e.Row.Cells(1).Visible = False
    End Sub

    Protected Sub grdUsersGroups_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdUsersGroups.RowCommand
        'delete Group from user
        Try
            Dim objUser As New clsUMUser(_mintID)
            objUser.DeleteGroup(Integer.Parse(grdUsersGroups.Rows(e.CommandArgument).Cells(1).Text))
            SetStatus("GROUP DELETED SUCCESSFULLY", Core.enmStatus.StatusInfo)
            Me.grdUsersGroups.DataSource = objUser.Groups
            Me.grdUsersGroups.DataBind()
        Catch pk_ex As System.Data.SqlClient.SqlException
            SetStatus(grdGroups.Rows(e.CommandArgument).Cells(2).Text & " is already viewable by this user", Core.enmStatus.StatusError)
        Catch ex As Exception
            Core.SendAdminStatusReport("Error in grdUsersGroups.RowCommand. Error: " & ex.ToString)
        End Try
    End Sub

    Protected Sub grdUsersGroups_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdUsersGroups.RowDataBound
        e.Row.Cells(1).Visible = False
        e.Row.Cells(2).Visible = False
    End Sub

    Protected Sub grdUsersGroups_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles grdUsersGroups.RowDeleting
        '
    End Sub

    'SITES

    Protected Sub grdSites_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdSites.RowCommand
        Try
            'add site to user currently being edited
            Dim objUser As New clsUMUser(_mintID)
            objUser.AddSite(Integer.Parse(grdSites.Rows(e.CommandArgument).Cells(1).Text))
            SetStatus("SITE ADDED SUCCESSFULLY", Core.enmStatus.StatusInfo)
            Me.grdUsersSites.DataSource = objUser.Sites
            Me.grdUsersSites.DataBind()
            LoadSites()
        Catch pk_ex As System.Data.SqlClient.SqlException
            SetStatus(grdSites.Rows(e.CommandArgument).Cells(2).Text & " is already viewable by this user", Core.enmStatus.StatusError)
        Catch ex As Exception
            Core.SendAdminStatusReport("Error in grdSites.RowCommand. Error: " & ex.ToString)
        End Try
    End Sub

    Protected Sub grdSites_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdSites.RowDataBound
        e.Row.Cells(1).Visible = False
    End Sub

    Protected Sub grdUsersSites_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdUsersSites.RowCommand
        Try
            pnlStatus.Visible = False
            'must be deleting a Site from this User
            Try
                If grdUsersSites.Rows.Count > 0 Then
                    Dim objUser As New clsUMUser(_mintID)
                    objUser.DeleteSite(Integer.Parse(grdUsersSites.Rows(e.CommandArgument).Cells(1).Text))
                    'deleted OK
                    SetStatus("SITE DELETED", Core.enmStatus.StatusInfo)
                    Me.SetupForm()
                End If
            Catch ex As Exception
                SetStatus("ERROR DELETING SITE", Core.enmStatus.StatusError)
            End Try
        Catch ex As Exception
            Core.SendAdminStatusReport("Error in grdUsersSites.RowCommand. Error: " & ex.ToString)
        End Try
    End Sub

    Protected Sub grdUsersSites_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdUsersSites.RowDataBound
        e.Row.Cells(1).Visible = False
    End Sub

    Protected Sub grdUsersSites_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles grdUsersSites.RowDeleting
        'bug workaround for deleting Users Sites
    End Sub


    'SYSTEMS

    Protected Sub grdSystems_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdSystems.RowCommand
        Try
            'add site to user currently being edited
            Dim objUser As New clsUMUser(_mintID)
            objUser.AddSystem(Integer.Parse(grdSystems.Rows(e.CommandArgument).Cells(1).Text), grdSystems.Rows(e.CommandArgument).Cells(2).Text)

            SetStatus("REPORT ADDED SUCCESSFULLY", Core.enmStatus.StatusInfo)
            Me.grdUsersSystems.DataSource = objUser.Systems
            Me.grdUsersSystems.DataBind()
            LoadSystems()
        Catch pk_ex As System.Data.SqlClient.SqlException
            SetStatus(grdSystems.Rows(e.CommandArgument).Cells(2).Text & " is already viewable by this user", Core.enmStatus.StatusError)
        Catch ex As Exception
            Core.SendAdminStatusReport("Error in grdSystems.RowCommand. Error: " & ex.ToString)
        End Try
    End Sub

    Protected Sub grdSystems_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdSystems.RowDataBound
        e.Row.Cells(1).Visible = False
    End Sub

    Protected Sub grdUsersSystems_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdUsersSystems.RowCommand
        Try
            pnlStatus.Visible = False
            'must be deleting a System from this User
            Try
                If grdUsersSystems.Rows.Count > 0 Then
                    Dim objUser As New clsUMUser(_mintID)
                    objUser.DeleteSystem(Integer.Parse(grdUsersSystems.Rows(e.CommandArgument).Cells(1).Text), grdUsersSystems.Rows(e.CommandArgument).Cells(2).Text)
                    'deleted OK
                    SetStatus("REPORT DELETED", Core.enmStatus.StatusInfo)
                    Me.SetupForm()
                End If
            Catch ex As Exception
                SetStatus("ERROR DELETING SYSTEMS", Core.enmStatus.StatusError)
            End Try
        Catch ex As Exception
            Core.SendAdminStatusReport("Error in grdUsersSystems.RowCommand. Error: " & ex.ToString)
        End Try
    End Sub

    Protected Sub grdUsersSystems_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdUsersSystems.RowDataBound
        e.Row.Cells(1).Visible = False
    End Sub

    Protected Sub grdUsersSystems_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles grdUsersSystems.RowDeleting
        'bug workaround for deleting Users Systems
    End Sub


    'REPORTS

    Protected Sub grdReports_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdReports.RowCommand
        Try
            'add site to user currently being edited
            Dim objUser As New clsUMUser(_mintID)
            objUser.AddReport(Integer.Parse(grdReports.Rows(e.CommandArgument).Cells(1).Text))
            SetStatus("REPORT ADDED SUCCESSFULLY", Core.enmStatus.StatusInfo)
            Me.grdUsersReports.DataSource = objUser.Reports
            Me.grdUsersReports.DataBind()
            LoadReports()
        Catch pk_ex As System.Data.SqlClient.SqlException
            SetStatus(grdReports.Rows(e.CommandArgument).Cells(2).Text & " is already viewable by this user", Core.enmStatus.StatusError)
        Catch ex As Exception
            Core.SendAdminStatusReport("Error in grdReports.RowCommand. Error: " & ex.ToString)
        End Try
    End Sub

    Protected Sub grdReports_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdReports.RowDataBound
        e.Row.Cells(1).Visible = False
    End Sub

    Protected Sub grdUsersReports_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdUsersReports.RowCommand
        Try
            pnlStatus.Visible = False
            'must be deleting a Report from this User
            Try
                If grdUsersReports.Rows.Count > 0 Then
                    Dim objUser As New clsUMUser(_mintID)
                    objUser.DeleteReport(Integer.Parse(grdUsersReports.Rows(e.CommandArgument).Cells(1).Text))
                    'deleted OK
                    SetStatus("REPORT DELETED", Core.enmStatus.StatusInfo)
                    Me.SetupForm()
                End If
            Catch ex As Exception
                SetStatus("ERROR DELETING REPORTS", Core.enmStatus.StatusError)
            End Try
        Catch ex As Exception
            Core.SendAdminStatusReport("Error in grdUsersReports.RowCommand. Error: " & ex.ToString)
        End Try
    End Sub

    Protected Sub grdUsersReports_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdUsersReports.RowDataBound
        e.Row.Cells(1).Visible = False
    End Sub

    Protected Sub grdUsersReports_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles grdUsersReports.RowDeleting
        'bug workaround for deleting Users Reports
    End Sub




    'HOUSEKEEPING

    Private Function ValidateMe() As Boolean
        Try
            Dim strErrors As String = ""
            SetStatus("", Core.enmStatus.StatusInfo)

            'validate 
            If Me.txtForename.Text.Trim.Length = 0 Then strErrors &= "You must enter a user forename<br/>"
            If Me.txtEmail.Text.Trim.Length = 0 Then strErrors &= "You must enter a user email<br/>"
            If Me.txtPassword.Text.Trim.Length = 0 Then strErrors &= "You must enter a user password<br/>"

            'if no errors then return true, otherwise set the error label and return false
            If Len(strErrors) = 0 Then
                Me.pnlStatus.Visible = False
                SetStatus("", Core.enmStatus.StatusInfo)
                Return True
            Else
                Me.pnlStatus.Visible = True
                SetStatus(strErrors, Core.enmStatus.StatusError)
                Return False
            End If
        Catch ex As Exception
            Me.pnlStatus.Visible = True
            SetStatus("Error validating form data", Core.enmStatus.StatusError)
            Return False
        End Try
    End Function

    Private Sub SetStatus(ByVal Message As String, ByVal StatusType As Core.enmStatus)
        If Len(Message) > 0 Then
            Me.pnlStatus.Visible = True
            If StatusType = Core.enmStatus.StatusError Then
                Me.pnlStatus.Attributes.Add("class", "error")
            Else
                Me.pnlStatus.Attributes.Add("class", "ok")
            End If
            Me.lblStatus.Text = Message
        End If
    End Sub

    Private Sub LoadDropDowns()
        With Me.ddlPageTypes
            .DataSource = Core.data_select("select * from tlkpPageTypes")
            .DataValueField = "intPageTypePK"
            .DataTextField = "varPageName"
            .DataBind()
        End With
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Dim strSearch As String = Me.txtSearch.Text.Trim
        Me.grdSites.DataSource = Core.data_select("select * from vwSites where ID = " & IIf(IsNumeric(strSearch), strSearch, "0") & " or [Site Name] like('%" & strSearch & "%') or [Company Name] like('%" & strSearch & "%')")
        Me.grdSites.DataBind()
        SetStatus(Me.grdSites.Rows.Count & " search results.", Core.enmStatus.StatusInfo)
    End Sub

    ' Define default min and max password lengths.
    Private Shared DEFAULT_MIN_PASSWORD_LENGTH As Integer = 6
    Private Shared DEFAULT_MAX_PASSWORD_LENGTH As Integer = 6

    ' Define supported password characters divided into groups.
    ' You can add (or remove) characters to (from) these groups.
    Private Shared PASSWORD_CHARS_LCASE As String = "abcdefgijkm"
    Private Shared PASSWORD_CHARS_UCASE As String = "nopqrstwxyz"
    Private Shared PASSWORD_CHARS_NUMERIC As String = "2345"
    Private Shared PASSWORD_CHARS_SPECIAL As String = "6789"

    ' <summary>
    ' Generates a random password.
    ' </summary>
    ' <returns>
    ' Randomly generated password.
    ' </returns>
    ' <remarks>
    ' The length of the generated password will be determined at
    ' random. It will be no shorter than the minimum default and
    ' no longer than maximum default.
    ' </remarks>
    Public Shared Function Generate() As String
        Generate = Generate(DEFAULT_MIN_PASSWORD_LENGTH, _
                            DEFAULT_MAX_PASSWORD_LENGTH)
    End Function

    ' <summary>
    ' Generates a random password of the exact length.
    ' </summary>
    ' <param name="length">
    ' Exact password length.
    ' </param>
    ' <returns>
    ' Randomly generated password.
    ' </returns>
    Public Shared Function Generate(ByVal length As Integer) As String
        Generate = Generate(length, length)
    End Function

    ' <summary>
    ' Generates a random password.
    ' </summary>
    ' <param name="minLength">
    ' Minimum password length.
    ' </param>
    ' <param name="maxLength">
    ' Maximum password length.
    ' </param>
    ' <returns>
    ' Randomly generated password.
    ' </returns>
    ' <remarks>
    ' The length of the generated password will be determined at
    ' random and it will fall with the range determined by the
    ' function parameters.
    ' </remarks>
    Public Shared Function Generate(ByVal minLength As Integer, _
    ByVal maxLength As Integer) _
        As String

        ' Make sure that input parameters are valid.
        If (minLength <= 0 Or maxLength <= 0 Or minLength > maxLength) Then
            Generate = Nothing
        End If

        ' Create a local array containing supported password characters
        ' grouped by types. You can remove character groups from this
        ' array, but doing so will weaken the password strength.
        Dim charGroups As Char()() = New Char()() _
        { _
            PASSWORD_CHARS_LCASE.ToCharArray(), _
            PASSWORD_CHARS_UCASE.ToCharArray(), _
            PASSWORD_CHARS_NUMERIC.ToCharArray(), _
            PASSWORD_CHARS_SPECIAL.ToCharArray() _
        }

        ' Use this array to track the number of unused characters in each
        ' character group.
        Dim charsLeftInGroup As Integer() = New Integer(charGroups.Length - 1) {}

        ' Initially, all characters in each group are not used.
        Dim I As Integer
        For I = 0 To charsLeftInGroup.Length - 1
            charsLeftInGroup(I) = charGroups(I).Length
        Next

        ' Use this array to track (iterate through) unused character groups.
        Dim leftGroupsOrder As Integer() = New Integer(charGroups.Length - 1) {}

        ' Initially, all character groups are not used.
        For I = 0 To leftGroupsOrder.Length - 1
            leftGroupsOrder(I) = I
        Next

        ' Because we cannot use the default randomizer, which is based on the
        ' current time (it will produce the same "random" number within a
        ' second), we will use a random number generator to seed the
        ' randomizer.

        ' Use a 4-byte array to fill it with random bytes and convert it then
        ' to an integer value.
        Dim randomBytes As Byte() = New Byte(3) {}

        ' Generate 4 random bytes.
        Dim rng As RNGCryptoServiceProvider = New RNGCryptoServiceProvider()

        rng.GetBytes(randomBytes)

        ' Convert 4 bytes into a 32-bit integer value.
        Dim seed As Integer = ((randomBytes(0) And &H7F) << 24 Or _
                                randomBytes(1) << 16 Or _
                                randomBytes(2) << 8 Or _
                                randomBytes(3))

        ' Now, this is real randomization.
        Dim random As Random = New Random(seed)

        ' This array will hold password characters.
        Dim password As Char() = Nothing

        ' Allocate appropriate memory for the password.
        If (minLength < maxLength) Then
            password = New Char(random.Next(minLength - 1, maxLength)) {}
        Else
            password = New Char(minLength - 1) {}
        End If

        ' Index of the next character to be added to password.
        Dim nextCharIdx As Integer

        ' Index of the next character group to be processed.
        Dim nextGroupIdx As Integer

        ' Index which will be used to track not processed character groups.
        Dim nextLeftGroupsOrderIdx As Integer

        ' Index of the last non-processed character in a group.
        Dim lastCharIdx As Integer

        ' Index of the last non-processed group.
        Dim lastLeftGroupsOrderIdx As Integer = leftGroupsOrder.Length - 1

        ' Generate password characters one at a time.
        For I = 0 To password.Length - 1

            ' If only one character group remained unprocessed, process it;
            ' otherwise, pick a random character group from the unprocessed
            ' group list. To allow a special character to appear in the
            ' first position, increment the second parameter of the Next
            ' function call by one, i.e. lastLeftGroupsOrderIdx + 1.
            If (lastLeftGroupsOrderIdx = 0) Then
                nextLeftGroupsOrderIdx = 0
            Else
                nextLeftGroupsOrderIdx = random.Next(0, lastLeftGroupsOrderIdx)
            End If

            ' Get the actual index of the character group, from which we will
            ' pick the next character.
            nextGroupIdx = leftGroupsOrder(nextLeftGroupsOrderIdx)

            ' Get the index of the last unprocessed characters in this group.
            lastCharIdx = charsLeftInGroup(nextGroupIdx) - 1

            ' If only one unprocessed character is left, pick it; otherwise,
            ' get a random character from the unused character list.
            If (lastCharIdx = 0) Then
                nextCharIdx = 0
            Else
                nextCharIdx = random.Next(0, lastCharIdx + 1)
            End If

            ' Add this character to the password.
            password(I) = charGroups(nextGroupIdx)(nextCharIdx)

            ' If we processed the last character in this group, start over.
            If (lastCharIdx = 0) Then
                charsLeftInGroup(nextGroupIdx) = _
                                charGroups(nextGroupIdx).Length
                ' There are more unprocessed characters left.
            Else
                ' Swap processed character with the last unprocessed character
                ' so that we don't pick it until we process all characters in
                ' this group.
                If (lastCharIdx <> nextCharIdx) Then
                    Dim temp As Char = charGroups(nextGroupIdx)(lastCharIdx)
                    charGroups(nextGroupIdx)(lastCharIdx) = _
                                charGroups(nextGroupIdx)(nextCharIdx)
                    charGroups(nextGroupIdx)(nextCharIdx) = temp
                End If

                ' Decrement the number of unprocessed characters in
                ' this group.
                charsLeftInGroup(nextGroupIdx) = _
                           charsLeftInGroup(nextGroupIdx) - 1
            End If

            ' If we processed the last group, start all over.
            If (lastLeftGroupsOrderIdx = 0) Then
                lastLeftGroupsOrderIdx = leftGroupsOrder.Length - 1
                ' There are more unprocessed groups left.
            Else
                ' Swap processed group with the last unprocessed group
                ' so that we don't pick it until we process all groups.
                If (lastLeftGroupsOrderIdx <> nextLeftGroupsOrderIdx) Then
                    Dim temp As Integer = _
                                leftGroupsOrder(lastLeftGroupsOrderIdx)
                    leftGroupsOrder(lastLeftGroupsOrderIdx) = _
                                leftGroupsOrder(nextLeftGroupsOrderIdx)
                    leftGroupsOrder(nextLeftGroupsOrderIdx) = temp
                End If

                ' Decrement the number of unprocessed groups.
                lastLeftGroupsOrderIdx = lastLeftGroupsOrderIdx - 1
            End If
        Next

        ' Convert password characters into a string and return the result.
        Generate = New String(password)
    End Function


    Protected Sub btnGenPwd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenPwd.Click
        Me.txtPassword.Text = Generate()


    End Sub
End Class
