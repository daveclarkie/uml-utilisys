Imports System
Imports System.Collections.Generic
Imports System.Text

''' <summary>
''' Summary description for Base64
''' </summary>
Public NotInheritable Class Base64

    Public Shared Function Encode(ByVal data As String) As String
        Try
            Dim encData_byte As Byte() = New Byte(data.Length - 1) {}
            encData_byte = Encoding.UTF8.GetBytes(data)
            Dim encodedData As String = Convert.ToBase64String(encData_byte)
            Return encodedData
        Catch e As Exception
            Throw New Exception("Error in base64Encode " & e.Message)
        End Try
    End Function

    Public Shared Function Decode(ByVal data As String) As String
        Try
            Dim encoder As New UTF8Encoding()
            Dim utf8Decode As Decoder = encoder.GetDecoder()

            Dim todecode_byte As Byte() = Convert.FromBase64String(data)
            Dim charCount As Integer = utf8Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length)
            Dim decoded_char As Char() = New Char(charCount - 1) {}
            utf8Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0)
            Dim result As String = New [String](decoded_char)
            Return result
        Catch e As Exception
            Throw New Exception("Error in base64Decode " & e.Message)
        End Try
    End Function
End Class