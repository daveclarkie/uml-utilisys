Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Data
Imports System

Public Class clsUMMaintenance

    'read all active reports
    Public Function ReadAllReports() As DataTable
        Dim objConnect As New SqlConnection(Core.ConnectionString())
        Dim objCommand As New SqlCommand("usp_Admin_Maintenance_Reports_Active", objConnect)
        Dim objDataset As DataSet = New DataSet

        Try

            objCommand.CommandType = CommandType.StoredProcedure

            objConnect.Open()
            Dim da As SqlDataAdapter = New SqlDataAdapter(objCommand)
            da.Fill(objDataset)

            'get total rows from OUTPUT parameter
            If Not objDataset Is Nothing Then
                If Not objDataset.Tables(0) Is Nothing Then

                End If
            End If


            'return page of results
            If objDataset.Tables.Count > 0 Then
                Return objDataset.Tables(0)
            Else
                Return Nothing
            End If
        Catch ex As Exception
            Throw
        Finally
            objCommand.Dispose()
            objConnect.Dispose()
        End Try
    End Function

    'read all inactive reports
    Public Function ReadAllInactiveReports() As DataTable
        Dim objConnect As New SqlConnection(Core.ConnectionString())
        Dim objCommand As New SqlCommand("usp_Admin_Maintenance_Reports_InActive", objConnect)
        Dim objDataset As DataSet = New DataSet

        Try
            objCommand.CommandType = CommandType.StoredProcedure

            objConnect.Open()
            Dim da As SqlDataAdapter = New SqlDataAdapter(objCommand)
            da.Fill(objDataset)

            'get total rows from OUTPUT parameter
            If Not objDataset Is Nothing Then
                If Not objDataset.Tables(0) Is Nothing Then

                End If
            End If


            'return page of results
            If objDataset.Tables.Count > 0 Then
                Return objDataset.Tables(0)
            Else
                Return Nothing
            End If
        Catch ex As Exception
            Throw
        Finally
            objCommand.Dispose()
            objConnect.Dispose()
        End Try
    End Function


    'add a report to this user
    Public Sub AddReport(ByVal ReportID As Integer)
        Dim objConnect As New SqlConnection(Core.ConnectionString())

        Try
            'execute insert query
            Dim objCommand As New SqlCommand("UPDATE tlkpReportTypes SET intDisabled = 0, dtmModified = getdate(), intUserModifiedByFK = " & MySession.UserID & " WHERE intReportTypePK = " & ReportID, objConnect)
            objCommand.CommandType = CommandType.Text
            objConnect.Open()
            objCommand.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    'delete a report from this User
    Public Sub DeleteReport(ByVal ReportID As Integer)
        Dim objConnect As New SqlConnection(Core.ConnectionString())

        Try
            'execute insert query
            Dim objCommand As New SqlCommand("UPDATE tlkpReportTypes SET intDisabled = 1, dtmModified = getdate(), intUserModifiedByFK = " & MySession.UserID & " WHERE intReportTypePK = " & ReportID, objConnect)
            objCommand.CommandType = CommandType.Text
            objConnect.Open()
            objCommand.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    'read all reports in this User




End Class
