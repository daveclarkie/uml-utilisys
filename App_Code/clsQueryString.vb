Imports System
Imports System.Data
Imports System.Collections
Imports System.Configuration
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls

''' <summary>
''' Summary description for QueryString
''' </summary>
Public Class QueryString
    Implements IDisposable

    Private _queryStringItems As String() = New String() {}

    Default Public ReadOnly Property Item(ByVal ordinal As String) As String
        Get
            If ordinal = "" Then
                Return Nothing
            End If

            If HttpContext.Current.Request.QueryString("q") Is Nothing Then
                Return Nothing
            End If

            ' decodes to name/value pairs: id=1&something=what&test=true
            Dim decode As String
            Try
                decode = Base64.Decode(HttpContext.Current.Request.QueryString("q"))
            Catch generatedExceptionName As Exception
                Return Nothing
            End Try

            Dim pairs As String() = decode.Split("&"c)

            For Each pair As String In pairs
                Dim item2() As String = pair.Split("="c)

                If item2(0).ToLower() = ordinal.ToLower() Then
                    Return item2(1)
                End If
            Next

            Return Nothing
        End Get
    End Property

    Public Sub New()
    End Sub

    Public Function EncodePairs(ByVal data As String) As String
        If data = "" Then
            Return ""
        End If

        Return Base64.Encode(data)
    End Function

#Region "IDisposable Members"

    Public Sub Dispose()
    End Sub

#End Region

    Public Sub Dispose1() Implements System.IDisposable.Dispose

    End Sub
End Class


