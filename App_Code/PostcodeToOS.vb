Imports System
Imports System.Net
Imports System.IO

Namespace PostcodeToOS
    ''' <summary> 
    ''' Helper class to convert from postcodes to Ordnance Survey coordinates and longitude/latitude, 
    ''' via screen-scraping of the Streetmap.co.uk site 
    ''' </summary> 
    Public Class Postcode
        Private Shared Function MakeRequest(ByVal request As String) As String
            Dim myReq As HttpWebRequest = DirectCast(WebRequest.Create("http://www.streetmap.co.uk/streetmap.dll?GridConvert?" & request), HttpWebRequest)

            ' Sends the HttpWebRequest and waits for the response. 
            Dim myHttpWebResponse As HttpWebResponse = TryCast(myReq.GetResponse(), HttpWebResponse)
            Try
                Dim response As Stream = myHttpWebResponse.GetResponseStream()
                Dim readStream As New StreamReader(response, System.Text.Encoding.GetEncoding("utf-8"))
                Return readStream.ReadToEnd()
            Finally
                ' Releases the resources of the response. 
                myHttpWebResponse.Close()
            End Try
        End Function

        ''' <summary> 
        ''' Converts a postcode to an Ordnance Survey coordinate. 
        ''' </summary> 
        ''' <param name="Postcode">Postcode</param> 
        ''' <param name="XPos">Ordnance Survey X coordinate</param> 
        ''' <param name="YPos">Ordnance Survey Y coordinate</param> 
        Public Shared Sub ToOS(ByVal Postcode As String, ByRef XPos As Integer, ByRef YPos As Integer)
            XPos = 0
            YPos = 0
            Dim HTML As String = MakeRequest("type=Postcode&name=" & Postcode)
            ' find the link 
            Dim linkToFind As String = "http://www.streetmap.co.uk/streetmap.dll?grid2map?"
            Dim pos As Integer = HTML.IndexOf(linkToFind)
            HTML = HTML.Substring(pos + linkToFind.Length, HTML.Length - pos - linkToFind.Length)

            ' split into separate query strings 
            Dim delimiter As Char() = New Char() {"&"c}
            Dim QueryStrings As String() = HTML.Split(delimiter)

            ' see if we can find the x and y query strings 
            For Each qry As String In QueryStrings
                If qry.StartsWith("X=") Then
                    XPos = Integer.Parse(qry.Substring(2))
                End If
                If qry.StartsWith("Y=") Then
                    YPos = Integer.Parse(qry.Substring(2))
                End If
            Next

            If (XPos <= 0) Or (YPos <= 0) Then
                Throw New Exception("Failed to get OS coordinates for postcode " & Postcode)
            End If
        End Sub

        Private Shared Function GetStringAfterText(ByVal html As String, ByVal text As String) As String
            Dim indexOfStart As Integer = html.IndexOf(text)
            Return html.Substring(indexOfStart + text.Length, html.Length - indexOfStart - text.Length)
        End Function

        ''' <summary> 
        ''' Converts an Ordnance Survey coordinate to a postcode. 
        ''' </summary> 
        ''' <param name="XPos">Ordnance Survey X coordinate</param> 
        ''' <param name="YPos">Ordnance Survey Y coordinate</param> 
        ''' <returns>The postcode.</returns> 
        Public Shared Function FromOS(ByVal XPos As Integer, ByVal YPos As Integer) As String
            Dim HTML As String = MakeRequest(("type=OSgrid&name=" & XPos.ToString() & ",") + YPos.ToString())
            Const startOfPostcode As String = "Nearest Post Code</strong> </td> <td width=""50%"" align=""center"" valign=""middle"">"

            Dim startOfPostcodeString As String = GetStringAfterText(HTML, startOfPostcode)
            Dim indexOfEnd As Integer = startOfPostcodeString.IndexOf(" <")
            Return startOfPostcodeString.Substring(0, indexOfEnd)
        End Function

        ''' <summary> 
        ''' Converts latitude/longitude to a postcode. 
        ''' </summary> 
        ''' <param name="latitude">Latitude</param> 
        ''' <param name="longitude">Longitude</param> 
        ''' <returns>The postcode.</returns> 
        Public Shared Function FromLatLong(ByVal latitude As Single, ByVal longitude As Single) As String
            Dim HTML As String = MakeRequest(("type=LatLong&name=" & latitude.ToString() & ",") + longitude.ToString())
            Const startOfPostcode As String = "Nearest Post Code</strong> </td> <td width=""50%"" align=""center"" valign=""middle"">"

            Dim startOfPostcodeString As String = GetStringAfterText(HTML, startOfPostcode)
            Dim indexOfEnd As Integer = startOfPostcodeString.IndexOf(" <")
            Return startOfPostcodeString.Substring(0, indexOfEnd)
        End Function

        ''' <summary> 
        ''' Converts a postcode to a longitude and latitude coordinate. 
        ''' </summary> 
        ''' <param name="Postcode">The postcode.</param> 
        ''' <param name="latitude">The latitude.</param> 
        ''' <param name="longitude">The longitude.</param> 

        Public Shared Function ToLatLong(ByVal Postcode As String, ByRef latitude As Single, ByRef longitude As Single) As String
            latitude = 0
            longitude = 0

            Dim strPoint As String = ""

            Dim HTML As String = MakeRequest("type=Postcode&name=" & Postcode)
            HTML = GetStringAfterText(HTML, "<strong>Lat</strong> (WGS84)")
            HTML = GetStringAfterText(HTML, " ( ")
            Dim indexOfEnd As Integer = HTML.IndexOf(" ) ")
            If indexOfEnd > -1 Then
                latitude = Single.Parse(HTML.Substring(0, indexOfEnd))

                HTML = GetStringAfterText(HTML, " ( ")
                indexOfEnd = HTML.IndexOf(" ) ")
                longitude = Single.Parse(HTML.Substring(0, indexOfEnd))
                strPoint = """" & latitude & """, """ & longitude & """"
                Return strPoint
            Else
                Return "0,0"

                'Throw New Exception("Failed to get longitude and latitude for postcode " & Postcode)
            End If
        End Function


        Public Shared Function ToLat(ByVal Postcode As String, ByRef latitude As Single, ByRef longitude As Single) As String
            latitude = 0
            longitude = 0

            Dim strPoint As String = ""

            Dim HTML As String = MakeRequest("type=Postcode&name=" & Postcode)
            HTML = GetStringAfterText(HTML, "<strong>Lat</strong> (WGS84)")
            HTML = GetStringAfterText(HTML, " ( ")
            Dim indexOfEnd As Integer = HTML.IndexOf(" ) ")
            If indexOfEnd > -1 Then
                latitude = Single.Parse(HTML.Substring(0, indexOfEnd))

                HTML = GetStringAfterText(HTML, " ( ")
                indexOfEnd = HTML.IndexOf(" ) ")
                longitude = Single.Parse(HTML.Substring(0, indexOfEnd))
                strPoint = latitude
                Return strPoint
            Else
                Return 0
                'Throw New Exception("Failed to get longitude and latitude for postcode " & Postcode)
            End If
        End Function


        Public Shared Function ToLong(ByVal Postcode As String, ByRef latitude As Single, ByRef longitude As Single) As String
            latitude = 0
            longitude = 0

            Dim strPoint As String = ""

            Dim HTML As String = MakeRequest("type=Postcode&name=" & Postcode)
            HTML = GetStringAfterText(HTML, "<strong>Lat</strong> (WGS84)")
            HTML = GetStringAfterText(HTML, " ( ")
            Dim indexOfEnd As Integer = HTML.IndexOf(" ) ")
            If indexOfEnd > -1 Then
                latitude = Single.Parse(HTML.Substring(0, indexOfEnd))

                HTML = GetStringAfterText(HTML, " ( ")
                indexOfEnd = HTML.IndexOf(" ) ")
                longitude = Single.Parse(HTML.Substring(0, indexOfEnd))
                strPoint = longitude
                Return strPoint
            Else
                Return 0
                'Throw New Exception("Failed to get longitude and latitude for postcode " & Postcode)
            End If
        End Function









    End Class
End Namespace