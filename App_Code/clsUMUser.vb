Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Data
Imports System

Public Class clsUMUser
    Private _ID As Integer
    Private _strForename As String
    Private _strSurname As String
    Private _strEmail As String
    Private _strPassword As String
    Private _intHomepageURL As Integer
    Private _blnAllowMySites As Boolean
    Private _blnAllowMyReports As Boolean
    Private _blnAllowMyAccount As Boolean
    Private _blnAdmin As Boolean
    Private _strNotes As String
    Private _dtmCreated As DateTime
    Private _intUserCreatedBy As Integer
    Private _dtmModified As DateTime
    Private _intUserModifiedBy As Integer
    Private _blnDisabled As Boolean = False

    'constructor method for reading an existing user from the DB and populating this object
    Public Sub New(ByVal ID As Integer)
        _ID = ID
        If _ID > 0 Then Read(ID)
    End Sub

    'constructor for new or edit functionality
    Public Sub New(ByVal UserID As Integer, _
                    ByVal strForename As String, _
                    ByVal strSurname As String, _
                    ByVal strEmail As String, _
                    ByVal strPassword As String, _
                    ByVal intHomepageURL As Integer, _
                    ByVal blnAllowMySites As Boolean, _
                    ByVal blnAllowMyReports As Boolean, _
                    ByVal blnAllowMyAccount As Boolean, _
                    ByVal blnAdmin As Boolean, _
                    ByVal strNotes As String, _
                    ByVal intUserModifiedBy As Integer)
        'constructor method - set user class properties
        _ID = UserID
        _strForename = strForename
        _strSurname = strSurname
        _strEmail = strEmail
        _strPassword = strPassword
        _intHomepageURL = intHomepageURL
        _blnAllowMySites = blnAllowMySites
        _blnAllowMyReports = blnAllowMyReports
        _blnAllowMyAccount = blnAllowMyAccount
        _blnAdmin = blnAdmin
        _strNotes = strNotes
        _intUserCreatedBy = intUserModifiedBy
        _intUserModifiedBy = intUserModifiedBy
    End Sub

    'save record - sets new ID into local class variable if INSERT
    Public Function Save() As Boolean
        Dim objConnect As New SqlConnection(Core.ConnectionString())
        Dim objCommand As SqlCommand

        Try
            If _ID = 0 Then
                'execute insert as ID is 0
                objCommand = New SqlCommand("spUser_INSERT", objConnect)
                'add parameters to stored procedure (specific to an INSERT)
                objCommand.Parameters.Add("@intUserPK", SqlDbType.Int)
                objCommand.Parameters.Item("@intUserPK").Direction = ParameterDirection.Output
                objCommand.Parameters.Add("@intUserCreatedBy", SqlDbType.Int)
                objCommand.Parameters.Item("@intUserCreatedBy").Value = _intUserModifiedBy
            Else
                'execute update
                objCommand = New SqlCommand("spUser_UPDATE", objConnect)
                'add parameters to stored procedure (specific to an UPDATE)
                objCommand.Parameters.Add("@intUserPK", SqlDbType.Int)
                objCommand.Parameters.Item("@intUserPK").Value = _ID
                objCommand.Parameters.Add("@intUserModifiedBy", SqlDbType.Int)
                objCommand.Parameters.Item("@intUserModifiedBy").Value = _intUserModifiedBy
            End If

            'add parameters to stored procedure (generic to INSERT or UPDATE)
            objCommand.CommandType = CommandType.StoredProcedure
            With objCommand.Parameters
                .Add("@varForename", SqlDbType.VarChar, 100)
                .Item("@varForename").Value = _strForename
                .Add("@varSurname", SqlDbType.VarChar, 100)
                .Item("@varSurname").Value = _strSurname
                .Add("@varEmail", SqlDbType.VarChar, 255)
                .Item("@varEmail").Value = _strEmail
                .Add("@varPassword", SqlDbType.VarChar, 50)
                .Item("@varPassword").Value = _strPassword
                .Add("@intPageTypeFK", SqlDbType.Int)
                .Item("@intPageTypeFK").Value = _intHomepageURL
                .Add("@bitAllowMySites", SqlDbType.Bit)
                .Item("@bitAllowMySites").Value = _blnAllowMySites
                .Add("@bitAllowMyReports", SqlDbType.Bit)
                .Item("@bitAllowMyReports").Value = _blnAllowMyReports
                .Add("@bitAllowMyAccount", SqlDbType.Bit)
                .Item("@bitAllowMyAccount").Value = _blnAllowMyAccount
                .Add("@bitAllowAdmin", SqlDbType.Bit)
                .Item("@bitAllowAdmin").Value = _blnAdmin
                .Add("@varNotes", SqlDbType.VarChar, 500)
                .Item("@varNotes").Value = _strNotes
            End With

            'open the connection
            objConnect.Open()
            'execute the insert or update
            objCommand.ExecuteNonQuery()
            If _ID = 0 Then
                'set this objects relevant property to the ID that was just inserted
                _ID = Integer.Parse(objCommand.Parameters("@intUserPK").Value)
            End If

            Return True
        Catch ex As Exception
            Throw ex
        Finally
            objConnect.Close()
        End Try
    End Function

    'read single record and populate class
    Public Sub Read(ByVal ID As Integer)
        Dim objConnect As New SqlConnection(Core.ConnectionString())
        Dim objReader As SqlDataReader

        Try
            'execute select stored procedure and fill this object
            Dim objCommand As New SqlCommand("spUser_SELECT " & ID, objConnect)
            objCommand.CommandType = CommandType.Text

            'open the connection
            objConnect.Open()
            'execute the query
            objReader = objCommand.ExecuteReader(CommandBehavior.SingleRow)
            objReader.Read()

            If objReader.HasRows Then
                'populate this instance
                With objReader
                    _ID = Core.CheckForNullInteger(.Item("intUserPK"))
                    _strForename = Core.CheckForNullString(.Item("varForename"))
                    _strSurname = Core.CheckForNullString(.Item("varSurname"))
                    _strEmail = Core.CheckForNullString(.Item("varEmail"))
                    _strPassword = Core.CheckForNullString(.Item("varPassword"))
                    _intHomepageURL = Core.CheckForNullString(.Item("intPageTypeFK"))
                    _blnAllowMySites = Core.CheckForNullBoolean(.Item("bitAllowMySites"))
                    _blnAllowMyReports = Core.CheckForNullBoolean(.Item("bitAllowMyReports"))
                    _blnAllowMyAccount = Core.CheckForNullBoolean(.Item("bitAllowMyAccount"))
                    _blnAdmin = Core.CheckForNullBoolean(.Item("bitAllowAdmin"))
                    _strNotes = Core.CheckForNullString(.Item("varNotes"))
                    _dtmCreated = Core.CheckForNullDate(.Item("dtmCreated"))
                    _intUserCreatedBy = Core.CheckForNullInteger(.Item("intUserCreatedBy"))
                    _dtmModified = Core.CheckForNullDate(.Item("dtmModified"))
                    _intUserModifiedBy = Core.CheckForNullInteger(.Item("intUserCreatedBy"))
                    _blnDisabled = Core.CheckForNullBoolean(.Item("bitDisabled"))
                End With
            Else
                'do nothing
            End If
        Catch ex As Exception
            Throw
        Finally
            objConnect.Close()
        End Try
    End Sub

    'return list of companies currently viewable for this user
    Public Function Companies() As DataTable
        Return Core.data_select("select * from vwUsersCompanies where [User ID] = " & Me._ID)
    End Function

    'return list of groups currently viewable for this user
    Public Function Groups() As DataTable
        Return Core.data_select("select * from vwUsersGroups where [User ID] = " & Me._ID)
    End Function

    'read all sites
    Public Function ReadAllSites(ByVal Filter As String, ByVal Search As String, ByVal CurrentPage As Integer, ByVal PageSize As Integer, ByRef TotalRecords As Integer) As DataTable
        Dim objConnect As New SqlConnection(Core.ConnectionString())
        Dim objCommand As New SqlCommand("spSites_SELECT", objConnect)
        Dim objDataset As DataSet = New DataSet
        Dim strSearch As String = ""
        Dim strFilter As String = "all"

        Try
            objCommand.CommandType = CommandType.StoredProcedure
            objCommand.Parameters.Add("@filter", SqlDbType.VarChar).Value = Filter
            objCommand.Parameters.Add("@search", SqlDbType.VarChar).Value = Search
            objCommand.Parameters.Add("@current_page", SqlDbType.Int).Value = CurrentPage
            objCommand.Parameters.Add("@page_size", SqlDbType.Int).Value = PageSize
            objCommand.Parameters.Add("@user", SqlDbType.Int).Value = Me._ID
            Dim total As SqlParameter = objCommand.Parameters.Add("@total", SqlDbType.Int)
            total.Direction = ParameterDirection.Output
            objConnect.Open()
            Dim da As SqlDataAdapter = New SqlDataAdapter(objCommand)
            da.Fill(objDataset)

            'get total rows from OUTPUT parameter
            TotalRecords = Core.CheckForNullInteger(total.Value)

            'return page of results
            If objDataset.Tables.Count > 0 Then
                Return objDataset.Tables(0)
            Else
                Return Nothing
            End If
        Catch ex As Exception
            Throw
        Finally
            objCommand.Dispose()
            objConnect.Dispose()
        End Try
    End Function


    'read all reports
    Public Function ReadAllSystems(ByRef totalcount As Integer) As DataTable
        Dim objConnect As New SqlConnection(Core.ConnectionString())
        Dim objCommand As New SqlCommand("usp_Admin_Systems_Available", objConnect)
        Dim objDataset As DataSet = New DataSet

        Try
            totalcount = 0
            objCommand.CommandType = CommandType.StoredProcedure

            objCommand.Parameters.Add("@intUserFK", SqlDbType.Int).Value = Me._ID

            objConnect.Open()
            Dim da As SqlDataAdapter = New SqlDataAdapter(objCommand)
            da.Fill(objDataset)

            'get total rows from OUTPUT parameter
            If Not objDataset Is Nothing Then
                If Not objDataset.Tables(0) Is Nothing Then
                    totalcount = objDataset.Tables(0).Rows.Count
                End If
            End If


            'return page of results
            If objDataset.Tables.Count > 0 Then
                Return objDataset.Tables(0)
            Else
                Return Nothing
            End If
        Catch ex As Exception
            Throw
        Finally
            objCommand.Dispose()
            objConnect.Dispose()
        End Try
    End Function

    'read all reports
    Public Function ReadAllReports(ByRef totalcount As Integer) As DataTable
        Dim objConnect As New SqlConnection(Core.ConnectionString())
        Dim objCommand As New SqlCommand("usp_Admin_Reports_Available", objConnect)
        Dim objDataset As DataSet = New DataSet

        Try
            totalcount = 0
            objCommand.CommandType = CommandType.StoredProcedure

            objCommand.Parameters.Add("@intUserFK", SqlDbType.Int).Value = Me._ID

            objConnect.Open()
            Dim da As SqlDataAdapter = New SqlDataAdapter(objCommand)
            da.Fill(objDataset)

            'get total rows from OUTPUT parameter
            If Not objDataset Is Nothing Then
                If Not objDataset.Tables(0) Is Nothing Then
                    totalcount = objDataset.Tables(0).Rows.Count
                End If
            End If


            'return page of results
            If objDataset.Tables.Count > 0 Then
                Return objDataset.Tables(0)
            Else
                Return Nothing
            End If
        Catch ex As Exception
            Throw
        Finally
            objCommand.Dispose()
            objConnect.Dispose()
        End Try
    End Function

    'disable record
    Public Sub Disable()
        Dim objConnect As New SqlConnection(Core.ConnectionString())

        Try
            Dim objCommand As New SqlCommand("update tblUsers set bitDisabled = 1, dtmModified = getdate() where intUserPK = " & ID, objConnect)
            objCommand.CommandType = CommandType.Text
            objConnect.Open()
            objCommand.ExecuteNonQuery()
        Catch ex As Exception
            Throw
        Finally
            objConnect.Close()
        End Try
    End Sub

    'restore record
    Public Sub Enable()
        Dim objConnect As New SqlConnection(Core.ConnectionString())

        Try
            Dim objCommand As New SqlCommand("update tblUsers set bitDisabled = 0 where intUserPK = " & ID, objConnect)
            objCommand.CommandType = CommandType.Text
            objConnect.Open()
            objCommand.ExecuteNonQuery()
        Catch ex As Exception
            Throw
        Finally
            objConnect.Close()
        End Try
    End Sub

    'add a company to this user
    Public Sub AddCompany(ByVal CompanyID As Integer)
        Dim objConnect As New SqlConnection(Core.ConnectionString())

        Try
            'execute insert query
            Dim objCommand As New SqlCommand("insert into tblUsersCompanies (intUserFK, intCompanyFK) values (" & _ID & ", " & CompanyID & ")", objConnect)
            objCommand.CommandType = CommandType.Text
            objConnect.Open()
            objCommand.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    'delete a Company from this User
    Public Sub DeleteCompany(ByVal CompanyID As Integer)
        Dim objConnect As New SqlConnection(Core.ConnectionString())

        Try
            'execute insert query
            Dim objCommand As New SqlCommand("delete tblUsersCompanies where intCompanyFK = " & CompanyID & " and intUserFK = " & _ID, objConnect)
            objCommand.CommandType = CommandType.Text
            objConnect.Open()
            objCommand.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    'add a Group to this user
    Public Sub AddGroup(ByVal GroupID As Integer)
        Dim objConnect As New SqlConnection(Core.ConnectionString())

        Try
            'execute insert query
            Dim objCommand As New SqlCommand("insert into tblUsersGroups (intUserFK, intGroupFK) values (" & _ID & ", " & GroupID & ")", objConnect)
            objCommand.CommandType = CommandType.Text
            objConnect.Open()
            objCommand.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    'delete a Group from this User
    Public Sub DeleteGroup(ByVal GroupID As Integer)
        Dim objConnect As New SqlConnection(Core.ConnectionString())

        Try
            'execute insert query
            Dim objCommand As New SqlCommand("delete tblUsersGroups where intGroupFK = " & GroupID & " and intUserFK = " & _ID, objConnect)
            objCommand.CommandType = CommandType.Text
            objConnect.Open()
            objCommand.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    'add a site to this user
    Public Sub AddSite(ByVal SiteID As Integer)
        Dim objConnect As New SqlConnection(Core.ConnectionString())

        Try
            'execute insert query
            Dim objCommand As New SqlCommand("insert into tblUsersSites (intUserFK, intSiteFK) values (" & _ID & ", " & SiteID & ")", objConnect)
            objCommand.CommandType = CommandType.Text
            objConnect.Open()
            objCommand.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    'delete a site from this User
    Public Sub DeleteSite(ByVal SiteID As Integer)
        Dim objConnect As New SqlConnection(Core.ConnectionString())

        Try
            'execute insert query
            Dim objCommand As New SqlCommand("delete tblUsersSites where intSiteFK = " & SiteID & " and intUserFK = " & _ID, objConnect)
            objCommand.CommandType = CommandType.Text
            objConnect.Open()
            objCommand.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub


    'add a system to this user
    Public Sub AddSystem(ByVal SystemID As Integer, ByVal systemtype As String)
        Dim objConnect As New SqlConnection(Core.ConnectionString())

        Try
            Dim strSQL As String = ""
            'execute insert query

            If systemtype = "IMServ" Then
                strSQL = "insert into tblUsersLoginIMServ (intUserFK, intLoginIMServFK) values (" & _ID & ", " & SystemID & ")"
            ElseIf systemtype = "Utilisys" Then
                strSQL = "insert into tblUsersLoginUtilisys (intUserFK, intLoginUtilisysFK) values (" & _ID & ", " & SystemID & ")"
            End If

            Dim objCommand As New SqlCommand(strSQL, objConnect)
            objCommand.CommandType = CommandType.Text
            objConnect.Open()
            objCommand.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    'delete a system from this User
    Public Sub DeleteSystem(ByVal SystemID As Integer, ByVal systemtype As String)
        Dim objConnect As New SqlConnection(Core.ConnectionString())

        Try
            'execute insert query
            Dim strSQL As String = ""
            'execute insert query

            If systemtype = "IMServ" Then
                strSQL = "DELETE FROM tblUsersLoginIMServ WHERE intUserFK = " & _ID & " AND intLoginIMServFK = " & SystemID
            ElseIf systemtype = "Utilisys" Then
                strSQL = "DELETE FROM tblUsersLoginUtilisys WHERE intUserFK = " & _ID & " AND intLoginUtilisysFK = " & SystemID
            End If

            Dim objCommand As New SqlCommand(strSQL, objConnect)

            objCommand.CommandType = CommandType.Text
            objConnect.Open()
            objCommand.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub


    'add a report to this user
    Public Sub AddReport(ByVal ReportID As Integer)
        Dim objConnect As New SqlConnection(Core.ConnectionString())

        Try
            'execute insert query
            Dim objCommand As New SqlCommand("insert into tblUsersReports (intUserFK, intReportFK) values (" & _ID & ", " & ReportID & ")", objConnect)
            objCommand.CommandType = CommandType.Text
            objConnect.Open()
            objCommand.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    'delete a report from this User
    Public Sub DeleteReport(ByVal ReportID As Integer)
        Dim objConnect As New SqlConnection(Core.ConnectionString())

        Try
            'execute insert query
            Dim objCommand As New SqlCommand("delete tblUsersReports where intReportFK = " & ReportID & " and intUserFK = " & _ID, objConnect)
            objCommand.CommandType = CommandType.Text
            objConnect.Open()
            objCommand.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    'read all users data from table and pass back reader
    Public Function ReadAll(ByVal Filter As String, ByVal Search As String, ByVal CurrentPage As Integer, ByVal PageSize As Integer, ByRef TotalRecords As Integer) As DataTable
        Dim objConnect As New SqlConnection(Core.ConnectionString())
        Dim objCommand As New SqlCommand("spUsers_SELECT", objConnect)
        Dim objDataset As DataSet = New DataSet
        Dim strSearch As String = ""
        Dim strFilter As String = "all"

        Try

            objCommand.CommandType = CommandType.StoredProcedure
            objCommand.Parameters.Add("@filter", SqlDbType.VarChar).Value = Filter
            objCommand.Parameters.Add("@search", SqlDbType.VarChar).Value = Search
            objCommand.Parameters.Add("@current_page", SqlDbType.Int).Value = CurrentPage
            objCommand.Parameters.Add("@page_size", SqlDbType.Int).Value = PageSize
            Dim total As SqlParameter = objCommand.Parameters.Add("@total", SqlDbType.Int)
            total.Direction = ParameterDirection.Output
            objConnect.Open()
            Dim da As SqlDataAdapter = New SqlDataAdapter(objCommand)
            da.Fill(objDataset)

            'get total rows from OUTPUT parameter
            TotalRecords = Core.CheckForNullInteger(total.Value)

            'return page of results
            If objDataset.Tables.Count > 0 Then
                Return objDataset.Tables(0)
            Else
                Return Nothing
            End If
        Catch ex As Exception
            Throw
        Finally
            objCommand.Dispose()
            objConnect.Dispose()
        End Try
    End Function

    'read all sites in this User
    Public Function Sites() As SqlDataReader
        Dim objConnect As New SqlConnection(Core.ConnectionString())
        Dim objReader As SqlDataReader

        Try
            'execute select stored procedure and fill this object
            Dim objCommand As New SqlCommand("select [Site ID] as ID, [Site Name], [Company Name] from vwUsersSites where ID = " & Me._ID, objConnect)
            objCommand.CommandType = CommandType.Text
            'open the connection
            objConnect.Open()
            'execute the query
            objReader = objCommand.ExecuteReader()
            Return objReader
        Catch ex As Exception
            Throw
        End Try
    End Function


    'read all reports in this User
    Public Function Systems() As SqlDataReader
        Dim objConnect As New SqlConnection(Core.ConnectionString())
        Dim objReader As SqlDataReader

        Try
            'execute select stored procedure and fill this object
            Dim objCommand As New SqlCommand("EXEC usp_Admin_Systems_Assigned " & Me._ID, objConnect)
            objCommand.CommandType = CommandType.Text
            'open the connection
            objConnect.Open()
            'execute the query
            objReader = objCommand.ExecuteReader()
            Return objReader
        Catch ex As Exception
            Throw
        End Try
    End Function

    'read all reports in this User
    Public Function Reports() As SqlDataReader
        Dim objConnect As New SqlConnection(Core.ConnectionString())
        Dim objReader As SqlDataReader

        Try
            'execute select stored procedure and fill this object
            Dim objCommand As New SqlCommand("EXEC usp_Admin_Reports_Assigned " & Me._ID, objConnect)
            objCommand.CommandType = CommandType.Text
            'open the connection
            objConnect.Open()
            'execute the query
            objReader = objCommand.ExecuteReader()
            Return objReader
        Catch ex As Exception
            Throw
        End Try
    End Function

    'read all data from table and pass back reader
    Public Function Search(ByVal StartRow As Integer, ByVal MaxRows As Integer) As SqlDataReader
        Dim objConnect As New SqlConnection(Core.ConnectionString())
        Dim objReader As SqlDataReader

        Try
            'execute select stored procedure and fill this object
            Dim objCommand As New SqlCommand("exec spUser_SEARCH " & StartRow & ", " & MaxRows, objConnect)
            objCommand.CommandType = CommandType.Text
            'open the connection
            objConnect.Open()
            'execute the query
            objReader = objCommand.ExecuteReader()
            Return objReader
        Catch
            Return Nothing
        End Try
    End Function

    Public Property ID() As Integer
        Get
            Return _ID
        End Get
        Set(ByVal value As Integer)
            _ID = value
        End Set
    End Property

    Public Property Forename() As String
        Get
            Return _strForename
        End Get
        Set(ByVal value As String)
            _strForename = value
        End Set
    End Property

    Public Property Surname() As String
        Get
            Return _strSurname
        End Get
        Set(ByVal value As String)
            _strSurname = value
        End Set
    End Property

    Public Property Email() As String
        Get
            Return _strEmail
        End Get
        Set(ByVal value As String)
            _strEmail = value
        End Set
    End Property

    Public Property Password() As String
        Get
            Return _strPassword
        End Get
        Set(ByVal value As String)
            _strPassword = value
        End Set
    End Property

    Public Property HomepageURL() As Integer
        Get
            Return _intHomepageURL
        End Get
        Set(ByVal value As Integer)
            _intHomepageURL = value
        End Set
    End Property

    Public Property AllowMySites() As Boolean
        Get
            Return _blnAllowMySites
        End Get
        Set(ByVal value As Boolean)
            _blnAllowMySites = value
        End Set
    End Property

    Public Property AllowMyReports() As Boolean
        Get
            Return _blnAllowMyReports
        End Get
        Set(ByVal value As Boolean)
            _blnAllowMyReports = value
        End Set
    End Property

    Public Property AllowMyAccount() As Boolean
        Get
            Return _blnAllowMyAccount
        End Get
        Set(ByVal value As Boolean)
            _blnAllowMyAccount = value
        End Set
    End Property

    Public Property Admin() As Boolean
        Get
            Return _blnAdmin
        End Get
        Set(ByVal value As Boolean)
            _blnAdmin = value
        End Set
    End Property

    Public Property Notes() As String
        Get
            Return _strNotes
        End Get
        Set(ByVal value As String)
            _strNotes = value
        End Set
    End Property

    Public Property Created() As DateTime
        Get
            Return _dtmCreated
        End Get
        Set(ByVal value As DateTime)
            _dtmCreated = value
        End Set
    End Property

    Public Property UserCreatedBy() As Integer
        Get
            Return _intUserCreatedBy
        End Get
        Set(ByVal value As Integer)
            _intUserCreatedBy = value
        End Set
    End Property

    Public Property Modified() As DateTime
        Get
            Return _dtmModified
        End Get
        Set(ByVal value As DateTime)
            _dtmModified = value
        End Set
    End Property

    Public Property UserModifiedBy() As Integer
        Get
            Return _intUserModifiedBy
        End Get
        Set(ByVal value As Integer)
            _intUserModifiedBy = value
        End Set
    End Property

    Public Property Disabled() As Boolean
        Get
            Return _blnDisabled
        End Get
        Set(ByVal value As Boolean)
            _blnDisabled = value
        End Set
    End Property
End Class
