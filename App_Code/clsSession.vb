Imports System.Web.SessionState
Imports System.Web

Public NotInheritable Class MySession

    'encapsulates the details of managing session variables.
    Private Const UserID_Key As String = "UserID"
    Private Const CompanyID_Key As String = "CompanyID"
    Private Const LoggedIn_Key As String = "LoggedIn"

    Private Sub New()
        'constructor - private means it can't be created
    End Sub

    'returns the current Session object, if any.
    Private Shared ReadOnly Property Session() As HttpSessionState
        Get
            Return HttpContext.Current.Session
        End Get
    End Property

    'returns True if a valid Session object exists.
    Public Shared ReadOnly Property Exists() As Boolean
        Get
            Return Not Session Is Nothing
        End Get
    End Property

    'gets the ID of the user who is currently logged onto the system
    Public Shared Property UserID() As Integer
        Get
            If Not Session(UserID_Key) Is Nothing Then
                Return DirectCast(Session(UserID_Key), Integer)
            Else
                Return 1
            End If
        End Get
        Set(ByVal value As Integer)
            Session(UserID_Key) = value
        End Set
    End Property

    'gets the ID of the Company
    Public Shared Property CompanyID() As Integer
        Get
            If Session(CompanyID_Key) Is Nothing Then
                Return 0
            Else
                Return DirectCast(Session(CompanyID_Key), Integer)
            End If
        End Get
        Set(ByVal value As Integer)
            Session(CompanyID_Key) = value
        End Set
    End Property

    'gets the logged in status of the user
    Public Shared Property LoggedIn() As Boolean
        Get
            If Session(LoggedIn_Key) Is Nothing Then
                Return False
            Else
                Return DirectCast(Session(LoggedIn_Key), Boolean)
            End If
        End Get
        Set(ByVal value As Boolean)
            Session(LoggedIn_Key) = value
        End Set
    End Property

End Class
