Imports System.Net.Mail
Imports System.Net

Public Class clsMail

    Public Shared Sub SendEmail(ByVal strFrom As String, ByVal strTo As String, ByVal strSubject As String, ByVal strMessage As String)

        '(1) Create the MailMessage instance
        Dim mm As New MailMessage(strFrom, strTo)

        '(2) Assign the MailMessage's properties
        mm.Subject = strSubject
        mm.Body = strMessage
        mm.IsBodyHtml = True

        '(3) Create the SmtpClient object
        Dim smtp As New SmtpClient
        '(4)Host
        smtp.Host = "10.44.5.204"
        '(5) Send the MailMessage (will use the Web.config settings)
        smtp.Send(mm)
    End Sub


    Public Shared Sub SendEmailAttachment(ByVal strFrom As String, ByVal strTo As String, ByVal strSubject As String, ByVal strMessage As String, ByVal strAttachment As String)

        '(1) Create the MailMessage instance
        Dim mm As New MailMessage(strFrom, strTo)

        '(2) Assign the MailMessage's properties
        mm.Subject = strSubject
        mm.Body = strMessage
        mm.IsBodyHtml = True
        Dim NewAttachment As New Attachment(strAttachment)
        mm.Attachments.Add(NewAttachment)

        '(3) Create the SmtpClient object
        Dim smtp As New SmtpClient
        '(4)Host
        smtp.Host = "10.44.5.204"
        '(5) Send the MailMessage (will use the Web.config settings)
        smtp.Send(mm)
    End Sub

End Class
