'data layer class - takes care of all data operations and also provides some generic helper functions

Imports System
Imports System.Diagnostics
Imports System.ComponentModel
Imports System.IO
Imports System.Text.RegularExpressions
Imports System.Collections.Specialized
Imports System.Web.Configuration
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic

Public NotInheritable Class Core
    Public Enum enmModes
        Add
        Edit
        Save
        SaveClose
        Delete
    End Enum
    Public Enum enmStatus
        StatusError
        StatusInfo
    End Enum
    Private Sub New()
        'constructor - private means it can't be created
    End Sub


    Public Shared Sub RunProgramme(ByVal DocName As String)
        Dim command As New System.Diagnostics.Process()
        command.StartInfo.FileName = DocName
        command.StartInfo.UserName = "administrator"
        command.StartInfo.Domain = "UML"
        Dim pwd As New System.Security.SecureString()

        For Each c As Char In "chicago"
            pwd.AppendChar(c)
        Next

        command.StartInfo.Password = pwd
        command.StartInfo.CreateNoWindow = False
        command.StartInfo.Verb = "open"
        command.StartInfo.UseShellExecute = False

        Try
            command.Start()
        Catch e11 As Win32Exception

            'Response.Write(e11.Message)
        End Try
        command.WaitForExit()
        command.Close()
    End Sub
    Public Shared Function OpenDocument(ByVal DocName As String) As Boolean
        'Opens a document in it's associated application
        'also works with web pages.

        'Returns true if successful, false otherwise
        'Examples:
        'OpenDocument("C:\MyDocument.doc")
        Try
            System.Diagnostics.Process.Start(DocName)
            Return True
        Catch
            Return False
        End Try

    End Function

    ' Connection Strings
    Public Shared Function CurrentConnectionString() As String
        Dim appSettings As NameValueCollection = WebConfigurationManager.AppSettings
        Return appSettings.Item("DBConnect2").ToString
    End Function
    Public Shared Function ConnectionString() As String
        Dim appSettings As NameValueCollection = WebConfigurationManager.AppSettings
        Return ConfigurationManager.ConnectionStrings("portal.utilitymasters.co.ukConnectionString").ConnectionString
        'Return appSettings.Item("DBConnect").ToString
    End Function  ' non provider connection string
    Public Shared Function ConnectionString2() As String
        Dim appSettings As NameValueCollection = WebConfigurationManager.AppSettings
        Return appSettings.Item("DBConnect2").ToString
    End Function ' portal.utilitymasters.co.uk
    Public Shared Function ConnectionString3() As String
        Dim appSettings As NameValueCollection = WebConfigurationManager.AppSettings
        Return appSettings.Item("DBConnect3").ToString
    End Function ' Cube
    Public Shared Function ConnectionString4() As String
        Dim appSettings As NameValueCollection = WebConfigurationManager.AppSettings
        Return appSettings.Item("DBConnect4").ToString
    End Function ' Helpdesk
    Public Shared Function SMTP() As String
        Dim appSettings As NameValueCollection = WebConfigurationManager.AppSettings
        Return appSettings.Item("SMTP").ToString
    End Function
    Public Shared Function CrystalServer() As String
        Dim appSettings As NameValueCollection = WebConfigurationManager.AppSettings
        Return appSettings.Item("CrystalServer").ToString
    End Function

    ' Query Returns
    

    Public Shared Function data_select(ByVal SQL As String, Optional Connection As Integer = 0) As Data.DataTable
        'create a new Connection object using the connection string
        Dim sqlCon As New SqlConnection(ConnectionString())

        Select Case Connection
            Case 0
                sqlCon = New SqlConnection(ConnectionString())
            Case 1
                sqlCon = New SqlConnection(ConnectionString4())
        End Select

        'create a new Command using the CommandText and Connection object
        Dim sqlCmd As New SqlCommand(SQL, sqlCon)
        'create a new data adapter
        Dim adpAdapter As New SqlDataAdapter
        'create a new dataset
        Dim datSet As New System.Data.DataSet

        Try
            sqlCmd.CommandTimeout = 180
            'set the data adapter command object
            adpAdapter.SelectCommand = sqlCmd
            'open the connection
            sqlCon.Open()
            'fill the dataset
            adpAdapter.Fill(datSet, "Main")
            'return just the main datatable (more lightweight object)
            Return datSet.Tables("Main")
        Catch ex As Exception
            Throw
        Finally
            'clean up
            sqlCon.Close()
            sqlCon.Dispose()
        End Try
    End Function

    Public Shared Function data_select_value(ByVal SQL As String) As String
        'create a new Connection object using the connection string
        Dim sqlCon As New SqlConnection(ConnectionString())
        'create a new Command using the CommandText and Connection object
        Dim sqlCmd As New SqlCommand(SQL, sqlCon)

        Try
            'open the connection
            sqlCon.Open()
            'return just the single value
            Return sqlCmd.ExecuteScalar()
        Catch ex As Exception
            Return "0"
        Finally
            'clean up
            sqlCon.Close()
            sqlCon.Dispose()
        End Try
    End Function
    Public Shared Function data_execute_nonquery(ByVal SQL As String) As Integer
        'create a new Connection object using the connection string
        Dim sqlCon As New SqlConnection(ConnectionString())
        'create a new Command using the CommandText and Connection object
        Dim sqlCmd As New SqlCommand(SQL, sqlCon)

        Try
            'open the connection
            sqlCon.Open()
            'execute the SQL, returning the number of rows affected
            data_execute_nonquery = sqlCmd.ExecuteNonQuery()
        Catch ex As Exception
            Throw
        Finally
            'clean up
            sqlCon.Close()
            sqlCon.Dispose()
        End Try
    End Function
    Public Shared Function data_select_image(ByVal SQL As String, ByVal ImageField As String, ByVal path As String) As Byte()
        'create a new Connection object using the connection string
        Dim sqlCon As New SqlConnection(ConnectionString())
        'create a new Command using the CommandText and Connection object
        Dim sqlCmd As New SqlCommand(SQL, sqlCon)

        Try
            'open the connection
            sqlCon.Open()
            'get image data
            Dim datImage As SqlDataReader = sqlCmd.ExecuteReader
            datImage.Read()
            If datImage.HasRows Then
                Try
                    'return byte array of image from db
                    Return CType(datImage.Item(ImageField), Byte())
                Catch ex As Exception
                    Return Nothing
                End Try
            Else
                Return Nothing
            End If
        Catch ex As Exception
            Throw
        Finally
            'clean up
            sqlCon.Close()
            sqlCon.Dispose()
        End Try
    End Function
    Public Shared Sub data_FillList(ByRef ddList As ListControl, ByRef datasource As Object, ByVal textField As String, ByVal valueField As String)
        Try
            ddList.Items.Clear()
            ddList.DataSource = datasource
            ddList.DataTextField = textField
            ddList.DataValueField = valueField
            ddList.DataBind()
            If ddList.Items.Count > 0 Then
                ddList.SelectedIndex = 0
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Public Shared Function TreeviewRedirectURL() As String

        Dim strSQL As String = "EXEC spPortal_TREEVIEW_TopLevel " & MySession.UserID
        Dim myConnection As New OleDb.OleDbConnection(Core.CurrentConnectionString)

        Dim myCommand As New OleDb.OleDbCommand(strSQL, myConnection)
        myCommand.Connection.Open()
        myCommand.CommandTimeout = 180
        Dim Reader As OleDb.OleDbDataReader = myCommand.ExecuteReader(CommandBehavior.CloseConnection)

        If Reader.HasRows Then
            Reader.Read()
            If Reader("Type") = "Company" Then
                Return "default.aspx?custid=" & Reader("id") & "&method=Customer"
            Else
                Return "default.aspx?custid=" & Reader("id") & "&method=" & Reader("Type")
            End If
            Reader.Close()
        Else
            Return "default.aspx"
        End If

    End Function

    Public Shared Function CustomerDirectory() As String
        Dim _return As String = Core.data_select_value("EXEC uml_cms.dbo.rsp_configuration_customerdirectory_latest")
        Return _return
    End Function
    Public Shared Function DataDirectory() As String
        Dim _return As String = Core.data_select_value("EXEC uml_cms.dbo.rsp_configuration_datadirectory_latest")
        Return _return
    End Function


    ' return 0 if object is null, else decimal valueha ha
    Public Shared Function CheckForNullDecimal(ByVal o As Object) As Decimal
        If (o Is Nothing) Or (IsDBNull(o)) Then
            Return 0
        Else
            Return CType(o, Decimal)
        End If
    End Function
    ' return 0 if null, else integer value of object.
    Public Shared Function CheckForNullInteger(ByVal i As Object) As Integer
        If (i Is Nothing) Or (IsDBNull(i)) Then
            Return 0
        Else
            Return CType(i, Integer)
        End If
    End Function
    ' return String if object is not null, else return empty.string
    Public Shared Function CheckForNullString(ByVal s As Object) As String
        If (s Is Nothing) Or (IsDBNull(s)) Then
            Return String.Empty
        Else
            Return CType(s, String)
        End If
    End Function
    ' return empty string if object is null, else decimal value
    Public Shared Function CheckForNullDate(ByVal d As Object) As String
        If (d Is Nothing) Or (IsDBNull(d)) Then
            Return String.Empty
        Else
            Return CType(d, String)
        End If
    End Function
    ' return false if object is null, else boolean value
    Public Shared Function CheckForNullBoolean(ByVal b As Object) As Boolean
        If (b Is Nothing) Or (isDBNull(b)) Then
            Return False
        Else
            Return CType(b, Boolean)
        End If
    End Function

    Public Shared Sub SendAdminStatusReport(ByVal Message As String)
        Try
            ''send email to confirm sale
            'Dim strMessage As String = ""
            ''instantiate a new instance of MailMessage
            'Dim objMailMessage As New MailMessage()
            ''set the sender address of the mail message
            'objMailMessage.From = New MailAddress("neil@scarletpartnership.co.uk")
            ''set the recepient address of the mail message
            'objMailMessage.To.Add(New MailAddress("neil@scarletpartnership.co.uk"))
            ''set the subject of the mail message
            'objMailMessage.Subject = "Admin Status Report"
            ''set the body of the mail message
            'objMailMessage.Body = Message
            ''set the format of the mail message body as HTML
            'objMailMessage.IsBodyHtml = True
            ''set the priority of the mail message to normal
            'objMailMessage.Priority = MailPriority.High
            ''instantiate a new instance of SmtpClient
            'Dim objSmtpClient As New SmtpClient("mail.scarletpartnership.co.uk", 25)
            ''send the mail message
            'objSmtpClient.Credentials = New System.Net.NetworkCredential("neil@scarletpartnership.co.uk", "thirlmere")
            'objSmtpClient.Send(objMailMessage)
            Throw New Exception("An error has occured")
        Catch ex As Exception
            '
        End Try
    End Sub


    Public Shared Function fn_NoSpecialCharacters(ByVal strTest As String) As String

        Dim sb As New System.Text.StringBuilder

        For Each ch As Char In strTest
            If Char.IsLetterOrDigit(ch) OrElse ch = " "c Then
                sb.Append(ch)
            End If
        Next

        strTest = sb.ToString

        Return strTest
    End Function

    Public Shared Function EmailAddressCheck(ByVal emailAddress As String) As Boolean

        Dim pattern As String = "^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"
        Dim emailAddressMatch As Match = Regex.Match(emailAddress, pattern)
        If emailAddressMatch.Success Then
            EmailAddressCheck = True
        Else
            EmailAddressCheck = False
        End If

    End Function


End Class
