﻿Imports System
Imports System.Diagnostics
Imports System.ComponentModel
Imports System.IO
Imports System.Data
Imports System.Data.OleDb
Imports Microsoft.VisualBasic
Imports System.Collections.Specialized
Imports System.Web.Configuration


Public Class ADODBConnection
    Public cComm As New ADODB.Command
    Public dComm As New ADODB.Connection
    Public rComm_Two As New ADODB.Recordset
    Public rComm As New ADODB.Recordset
    Public rrComm As New ADODB.Recordset


    Public Function OpenConnection() As Boolean
        Dim appSettings As NameValueCollection = WebConfigurationManager.AppSettings
        'Return appSettings.Item("DBConnect2").ToString
        dComm.ConnectionString = appSettings.Item("DBConnect2").ToString
        dComm.Open()
        Return Nothing
    End Function


    Public Function OpenRecordSet(ByVal strSQL As String) As String
        On Error Resume Next
        rComm.CursorLocation = ADODB.CursorLocationEnum.adUseClient
        'rComm.CursorType = ADODB.CursorTypeEnum.adOpenStatic
        rComm.CursorType = ADODB.CursorTypeEnum.adOpenDynamic
        rComm.LockType = ADODB.LockTypeEnum.adLockBatchOptimistic
        rComm.Open(strSQL, dComm)

        OpenRecordSet = "ok:"
    End Function

    Public Function OpenRecordSet_Two(ByVal strSQL As String) As String
        rComm_Two.CursorLocation = ADODB.CursorLocationEnum.adUseClient
        'rComm.CursorType = ADODB.CursorTypeEnum.adOpenStatic
        rComm_Two.CursorType = ADODB.CursorTypeEnum.adOpenDynamic
        rComm_Two.LockType = ADODB.LockTypeEnum.adLockBatchOptimistic
        rComm_Two.Open(strSQL, dComm)
        OpenRecordSet_Two = "ok:"
    End Function



    Public Function OpenRecordSet_StoredProcedure(ByVal strSQL As String) As String
        cComm.ActiveConnection = dComm
        cComm.CommandType = ADODB.CommandTypeEnum.adCmdStoredProc
        cComm.CommandText = strSQL
        'rComm.CursorLocation = ADODB.CursorLocationEnum.adUseClient
        'rComm.CursorType = ADODB.CursorTypeEnum.adOpenStatic
        'rComm.CursorType = ADODB.CursorTypeEnum.adOpenDynamic
        'rComm.LockType = ADODB.LockTypeEnum.adLockOptimistic
        '  rComm.Open(strSQL, dComm)
        'cComm.Parameters.Append(cComm.CreateParameter("StartDate", ADODB.DataTypeEnum.adChar, ADODB.ParameterDirectionEnum.adParamInput, 6, "john"))

        cComm.Parameters.Append(cComm.CreateParameter("StartDate", ADODB.DataTypeEnum.adDate, ADODB.ParameterDirectionEnum.adParamInput, 6, "01/07/2010"))

        rComm = cComm.Execute
        OpenRecordSet_StoredProcedure = "ok:"
    End Function

    Function OpenRec(ByVal strSQL As String) As String
        Dim _return As String = ""
        rrComm = CreateObject("ADODB.recordset")
        rrComm.ActiveConnection = dComm
        rrComm.CursorLocation = 3 ' 2
        rrComm.CursorType = 3 '1
        rrComm.LockType = 1 '3
        rrComm.Source = strSQL
        rrComm.Open()
        Return _return

    End Function

    Public Function CloseConnection() As Boolean
        rComm = Nothing
        dComm.Close()
        Return Nothing
    End Function





End Class
