Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Data
Imports System

Public Class clsUMCompany
    Private _ID As Integer
    Private _strCompany As String
    Private _strContactName As String
    Private _strContactName2 As String
    Private _strContactPhone As String
    Private _strContactPhone2 As String
    Private _strContactEmail As String
    Private _strContactEmail2 As String
    Private _strAddressLine1 As String
    Private _strAddressLine2 As String
    Private _strAddressLine3 As String
    Private _strAddressLine4 As String
    Private _strCounty As String
    Private _strPostcode As String
    'Private _dtmCreated As Date
    'Private _intUserCreatedBy As Integer
    'Private _dtmModified As Date
    'Private _intUserModifiedBy As Integer
    Private _blnDisabled As Boolean

    Private _strVATReg As String
    Private _intActiveSites As Integer
    Private _intInActiveSites As Integer
    Private _strAccountManager As String
    Private _strCompanyNumber As String

    'constructor method for reading an existing item from the DB and populate this object
    Public Sub New(ByVal ID As Integer)
        _ID = ID
        If _ID > 0 Then Read()
    End Sub

    'constructor for company details
    Public Sub New(ByVal ID As Integer, _
                         ByVal strCompany As String, _
                         ByVal strContactName As String, _
                         ByVal strContactName2 As String, _
                         ByVal strContactPhone As String, _
                         ByVal strContactPhone2 As String, _
                         ByVal strContactEmail As String, _
                         ByVal strContactEmail2 As String, _
                         ByVal strAddressLine1 As String, _
                         ByVal strAddressLine2 As String, _
                         ByVal strAddressLine3 As String, _
                         ByVal strAddressLine4 As String, _
                         ByVal strCounty As String, _
                         ByVal strPostcode As String, _
                         ByVal intUserModifiedBy As Integer, _
                         ByVal blnDisabled As Boolean, _
                         ByVal strVATReg As String, _
                         ByVal intActiveSites As Integer, _
                         ByVal intInActiveSites As Integer, _
                         ByVal strAccountManager As String, _
                         ByVal strCompanyNumber As String)
        _ID = ID
        _strCompany = strCompany
        _strContactName = strContactName
        _strContactName2 = strContactName2
        _strContactPhone = strContactPhone
        _strContactPhone2 = strContactPhone2
        _strContactEmail = strContactEmail
        _strContactEmail2 = strContactEmail2
        _strAddressLine1 = strAddressLine1
        _strAddressLine2 = strAddressLine2
        _strAddressLine3 = strAddressLine3
        _strAddressLine4 = strAddressLine4
        _strCounty = strCounty
        _strPostcode = strPostcode
        _blnDisabled = blnDisabled
        _strVATReg = strVATReg
        _intActiveSites = intActiveSites
        _intInActiveSites = intInActiveSites
        _strAccountManager = strAccountManager
        _strCompanyNumber = strCompanyNumber
    End Sub

    'save record - sets new ID into local class variable if INSERT
    Public Function Save() As Boolean
        Dim objConnect As New SqlConnection(Core.ConnectionString())
        Dim objCommand As SqlCommand

        Try
            If _ID = 0 Then
                'execute insert as ID is 0
                objCommand = New SqlCommand("spCompany_INSERT", objConnect)
                'add parameters to stored procedure (specific to an INSERT)
                objCommand.Parameters.Add("@intCompanyPK", SqlDbType.Int)
                objCommand.Parameters.Item("@intCompanyPK").Direction = ParameterDirection.Output
                objCommand.Parameters.Add("@intUserCreatedBy", SqlDbType.Int)
                'objCommand.Parameters.Item("@intUserCreatedBy").Value = _intUserModifiedBy
            Else
                'execute update
                objCommand = New SqlCommand("spCompany_UPDATE", objConnect)
                'add parameters to stored procedure (specific to an UPDATE)
                objCommand.Parameters.Add("@intCompanyPK", SqlDbType.Int)
                objCommand.Parameters.Item("@intCompanyPK").Value = _ID
                objCommand.Parameters.Add("@intUserModifiedBy", SqlDbType.Int)
                'objCommand.Parameters.Item("@intUserModifiedBy").Value = _intUserModifiedBy
                objCommand.Parameters.Add("@bitDisabled", SqlDbType.Bit)
                objCommand.Parameters.Item("@bitDisabled").Value = Core.CheckForNullBoolean(_blnDisabled)
            End If

            'add parameters to stored procedure (generic to INSERT or UPDATE)
            objCommand.CommandType = CommandType.StoredProcedure
            With objCommand.Parameters
                .Add("@varCompany", SqlDbType.VarChar, 100)
                .Item("@varCompany").Value = Core.CheckForNullString(_strCompany)
                .Add("@varContactName", SqlDbType.VarChar, 100)
                .Item("@varContactName").Value = Core.CheckForNullString(_strContactName)
                .Add("@varContactName2", SqlDbType.VarChar, 100)
                .Item("@varContactName2").Value = Core.CheckForNullString(_strContactName2)
                .Add("@varContactPhone", SqlDbType.VarChar, 50)
                .Item("@varContactPhone").Value = Core.CheckForNullString(_strContactPhone)
                .Add("@varContactPhone2", SqlDbType.VarChar, 50)
                .Item("@varContactPhone2").Value = Core.CheckForNullString(_strContactPhone2)
                .Add("@varContactEmail", SqlDbType.VarChar, 100)
                .Item("@varContactEmail").Value = Core.CheckForNullString(_strContactEmail)
                .Add("@varContactEmail2", SqlDbType.VarChar, 100)
                .Item("@varContactEmail2").Value = Core.CheckForNullString(_strContactEmail2)
                .Add("@varAddressLine1", SqlDbType.VarChar, 100)
                .Item("@varAddressLine1").Value = Core.CheckForNullString(_strAddressLine1)
                .Add("@varAddressLine2", SqlDbType.VarChar, 100)
                .Item("@varAddressLine2").Value = Core.CheckForNullString(_strAddressLine2)
                .Add("@varAddressLine3", SqlDbType.VarChar, 100)
                .Item("@varAddressLine3").Value = Core.CheckForNullString(_strAddressLine3)
                .Add("@varAddressLine4", SqlDbType.VarChar, 100)
                .Item("@varAddressLine4").Value = Core.CheckForNullString(_strAddressLine4)
                .Add("@varCounty", SqlDbType.VarChar, 100)
                .Item("@varCounty").Value = Core.CheckForNullString(_strCounty)
                .Add("@varPostcode", SqlDbType.VarChar, 10)
                .Item("@varPostcode").Value = Core.CheckForNullString(_strPostcode)
            End With

            'open the connection
            objConnect.Open()
            'execute the insert or update
            objCommand.ExecuteNonQuery()
            'new company = 0
            If _ID = 0 Then
                'set this objects relevant property to the ID that was just inserted
                _ID = objCommand.Parameters.Item("@intCompanyPK").Value

                '----- IMPORTANT -----'
                If MySession.Exists Then MySession.CompanyID = _ID
            Else
                If MySession.Exists Then MySession.CompanyID = 0
            End If
            Return True
        Catch ex As Exception
            Throw
        Finally
            objConnect.Close()
        End Try
    End Function

    'read single company record and populate class
    Public Sub Read()
        Dim objConnect As New SqlConnection(Core.ConnectionString())
        Dim objReader As SqlDataReader

        Try
            'execute select stored procedure and fill this object
            Dim objCommand As New SqlCommand("spAccount_SELECT " & _ID & ", " & MySession.UserID, objConnect)
            objCommand.CommandType = CommandType.Text

            'open the connection
            objConnect.Open()
            'execute the query
            objReader = objCommand.ExecuteReader(CommandBehavior.SingleRow)
            objReader.Read()

            If objReader.HasRows Then
                'populate this instance
                With objReader
                    _ID = Core.CheckForNullInteger(.Item("custid"))
                    _strCompany = Core.CheckForNullString(.Item("CustomerName"))
                    _strContactName = Core.CheckForNullString(.Item("PrimaryContactName"))
                    _strContactName2 = Core.CheckForNullString(.Item("SecondaryContactName"))
                    _strContactPhone = Core.CheckForNullString(.Item("PrimaryContactTelephone"))
                    _strContactPhone2 = Core.CheckForNullString(.Item("SecondaryContactTelephone"))
                    _strContactEmail = Core.CheckForNullString(.Item("PrimaryContactEmail"))
                    _strContactEmail2 = Core.CheckForNullString(.Item("SecondaryContactEmail"))
                    _strAddressLine1 = Core.CheckForNullString(.Item("address_1"))
                    _strAddressLine2 = Core.CheckForNullString(.Item("address_2"))
                    _strAddressLine3 = Core.CheckForNullString(.Item("address_3"))
                    _strAddressLine4 = Core.CheckForNullString(.Item("address_4"))
                    _strCounty = Core.CheckForNullString(.Item("County"))
                    _strPostcode = Core.CheckForNullString(.Item("PCode"))
                    '_dtmCreated = Core.CheckForNullDate(.Item("dtmCreated"))
                    '_intUserCreatedBy = Core.CheckForNullInteger(.Item("intUserCreatedBy"))
                    '_dtmModified = Core.CheckForNullDate(.Item("dtmModified"))
                    '_intUserModifiedBy = Core.CheckForNullInteger(.Item("intUserModifiedBy"))
                    _blnDisabled = Core.CheckForNullBoolean(.Item("bitDisabled"))
                    _strVATReg = Core.CheckForNullString(.Item("vatNum"))
                    _intActiveSites = Core.CheckForNullInteger(.Item("ActiveSites"))
                    _intInActiveSites = Core.CheckForNullInteger(.Item("InActiveSites"))
                    _strAccountManager = Core.CheckForNullString(.Item("AccountMgr"))
                    _strCompanyNumber = Core.CheckForNullString(.Item("coregnum"))
                End With
            Else
                'do nothing
            End If
        Catch ex As Exception
            Throw
        Finally
            objConnect.Close()
        End Try
    End Sub

    'return list of sites for this company
    Public Function Sites() As DataTable
        Return Core.data_select("select * form vwCompaniesSites where ID = " & Me._ID)
    End Function

    'return list of groups for this company
    Public Function Groups() As DataTable
        Return Core.data_select("select * form vwCompaniesGroups where ID = " & Me._ID)
    End Function


    Public ReadOnly Property ID() As Integer
        Get
            Return _ID
        End Get
    End Property

    Public ReadOnly Property Company() As String
        Get
            Return _strCompany
        End Get
    End Property

    Public ReadOnly Property ContactName() As String
        Get
            Return _strContactName
        End Get
    End Property

    Public ReadOnly Property ContactName2() As String
        Get
            Return _strContactName2
        End Get
    End Property

    Public ReadOnly Property ContactPhone() As String
        Get
            Return _strContactPhone
        End Get
    End Property

    Public ReadOnly Property ContactPhone2() As String
        Get
            Return _strContactPhone2
        End Get
    End Property

    Public ReadOnly Property ContactEmail() As String
        Get
            Return _strContactEmail
        End Get
    End Property

    Public ReadOnly Property ContactEmail2() As String
        Get
            Return _strContactEmail2
        End Get
    End Property

    Public ReadOnly Property AddressLine1() As String
        Get
            Return _strAddressLine1
        End Get
    End Property

    Public ReadOnly Property AddressLine2() As String
        Get
            Return _strAddressLine2
        End Get
    End Property

    Public ReadOnly Property AddressLine3() As String
        Get
            Return _strAddressLine3
        End Get
    End Property

    Public ReadOnly Property AddressLine4() As String
        Get
            Return _strAddressLine4
        End Get
    End Property

    Public ReadOnly Property County() As String
        Get
            Return _strCounty
        End Get
    End Property

    Public ReadOnly Property Postcode() As String
        Get
            Return _strPostcode
        End Get
    End Property

    'Public ReadOnly Property Created() As DateTime
    '    Get
    '        Return _dtmCreated
    '    End Get
    'End Property

    'Public ReadOnly Property UserCreatedBy() As Integer
    '    Get
    '        Return _intUserCreatedBy
    '    End Get
    'End Property

    'Public ReadOnly Property Modified() As DateTime
    '    Get
    '        Return _dtmModified
    '    End Get
    'End Property

    'Public ReadOnly Property UserModifiedBy() As Integer
    '    Get
    '        Return _intUserModifiedBy
    '    End Get
    'End Property

    Public ReadOnly Property Disabled() As Boolean
        Get
            Return _blnDisabled
        End Get
    End Property

    Public ReadOnly Property VATReg() As String
        Get
            Return _strVATReg
        End Get
    End Property
    Public ReadOnly Property ActiveSites() As Integer
        Get
            Return _intActiveSites
        End Get
    End Property
    Public ReadOnly Property InActiveSites() As Integer
        Get
            Return _intInActiveSites
        End Get
    End Property
    Public ReadOnly Property AccountManager() As String
        Get
            Return _strAccountManager
        End Get
    End Property
    Public ReadOnly Property CompanyNumber() As String
        Get
            Return _strCompanyNumber
        End Get
    End Property



End Class
