
Partial Class nav_my_apps
    Inherits System.Web.UI.UserControl



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        LoadReports()


    End Sub

    Private Sub HideMenuItems()
        Me.nav.Visible = False
        Me.navOnlineApps.Visible = False
    End Sub

    Private Sub LoadReports()

        'HideMenuItems()

        Dim strURL As String = Core.CheckForNullString(Me.Page.Request.Url.PathAndQuery.ToString)

        Me.lnkAppsHome.Text = "My Apps Home"
        Me.lnkAppsHome.NavigateUrl = "~/my-apps.aspx"

        Me.Nav_MetersLeftPanelTemplate.Visible = False
        Me.nav_activitymanagement.Visible = False
        Me.navOnlineApps.Visible = False
        Me.nav.Visible = False

        If strURL.Contains("/myapps/downloadhhdata") Then
            Me.navOnlineApps.Visible = True
            LoadTreeview_Report()
            Me.lnkAppsHome.Text = "Back to Apps Home"

        ElseIf strURL.Contains("/myapps/metergrouping") Then
            Me.navOnlineApps.Visible = True
            LoadTreeview_Report()
            Me.lnkAppsHome.Text = "Back to Apps Home"

            ElseIf strURL.Contains("/myapps/MyActivityManagement") Then
                Me.nav_activitymanagement.Visible = True
                Me.lnkAppsHome.Text = "Back to Apps Home"


                '=================================
                ' Meter bits for john
                '
                '=================================
            ElseIf strURL.Contains("/myapps/CustomerMeters") Then
                Me.Nav_MetersLeftPanelTemplate.Visible = True
                Me.lnkAppsHome.Text = "Back to Apps Home"
            ElseIf strURL.Contains("/myapps/customermeters") Then
                Me.Nav_MetersLeftPanelTemplate.Visible = True
                Me.lnkAppsHome.Text = "Back to Apps Home"
                '=================================


            ElseIf strURL.Contains("/myapps/myevents") Then
                Me.navOnlineApps.Visible = True
                Me.lnkAppsHome.Text = "Back to Apps Home"
                LoadTreeview_Report()

            ElseIf strURL.Contains("/myapps/VirtualMeterCreation") Then
                Me.navOnlineApps.Visible = True
                Me.lnkAppsHome.Text = "Back to Apps Home"
                LoadTreeview_Report()

                '===================================================================
                ' Make sure that the directory is all in lowe case
            ElseIf strURL.Contains("/myapps/customerproduction") Then
                Me.navOnlineApps.Visible = True
                LoadTreeview_Report()
                Me.lnkAppsHome.Text = "Back to Apps Home"


                '=================================
                ' Meter bits for john
                '
                '=================================
                ' ElseIf strURL.Contains("/myapps/customermeters") Then
                ' Me.navOnlineReports.Visible = True
                ' LoadTreeview_Report()



            ElseIf strURL.Contains("/myapps/CustomerMonitors") Then
                Me.navOnlineApps.Visible = True
                LoadTreeview_Report()
                Me.lnkAppsHome.Text = "Back to Apps Home"

            ElseIf strURL.Contains("/myapps/mymeters/") Then
                Me.navOnlineApps.Visible = True
                Me.lnkAppsHome.Text = "Back to Apps Home"
                LoadTreeview_Report()

                '===================================================================
            Else

                Me.navOnlineApps.Visible = True
                Me.lnkAppsHome.Text = "My Apps Home"
                Me.lnkAppsHome.NavigateUrl = "~/my-apps.aspx"

                Dim myConnection As New OleDb.OleDbConnection(Core.CurrentConnectionString)


                Dim strSQL As String = "EXEC spReports_ListReports " & MySession.UserID & ", 14"


                Using connection As New SqlConnection(Core.ConnectionString)
                    connection.Open()
                    Dim command As New SqlCommand(strSQL, connection)
                    command.CommandType = CommandType.Text
                    Dim reader As SqlDataReader = command.ExecuteReader()

                    While reader.Read

                        strURL = reader("varOnlineURL").ToString

                        If reader("intDisabled") = 0 Then
                            Me.navOnlineApps.InnerHtml += "<li class='styleformytreeview_company'><a href='" & strURL & "'>" & reader("varReportName") & "</a></li>"
                        Else
                            Me.navOnlineApps.InnerHtml += "<li class='styleformytreeview_company'><a style='color:silver;'>" & reader("varReportName") & "</a></li>"
                        End If

                    End While

                End Using

                myConnection.Close()


            End If


    End Sub

    Private Sub LoadTreeview_Report()
        Me.navOnlineApps.InnerHtml = ""
        Dim strSQL As String = "EXEC spPortal_TREEVIEW " & MySession.UserID
        Dim myConnection As New OleDb.OleDbConnection(Core.CurrentConnectionString)

        Dim myCommand As New OleDb.OleDbCommand(strSQL, myConnection)
        myCommand.Connection.Open()
        myCommand.CommandTimeout = 180
        Dim Reader As OleDb.OleDbDataReader = myCommand.ExecuteReader(CommandBehavior.CloseConnection)

        While Reader.Read

            Select Case CStr(Reader(3))
                Case CStr("Company")
                    Me.navOnlineApps.InnerHtml += "<li class='styleformytreeview_company'><a href='default.aspx?custid=" & Reader(0).ToString & "&method=Customer'>" & Reader(1).ToString & "</a></li>"
                Case CStr("Header")
                    Me.navOnlineApps.InnerHtml += "<li class='styleformytreeview_header'>" & Reader(1).ToString & "</li>"
                Case CStr("Site")
                    Me.navOnlineApps.InnerHtml += "<li class='styleformytreeview_site'><a href='default.aspx?custid=" & Reader(0).ToString & "&method=Site'>" & Left(Reader(1).ToString, 23) & "..</a></li>"
                Case CStr("Group")
                    Me.navOnlineApps.InnerHtml += "<li class='styleformytreeview_group'><a href='default.aspx?custid=" & Reader(0).ToString & "&method=Group'>" & Reader(1).ToString & "</a></li>"
            End Select


        End While

        'Me.navOnlineApps.InnerHtml += "<li class='styleformytreeview_company' style='height:10px;'></li>"
        'Me.navOnlineApps.InnerHtml += "<li class='styleformytreeview_company'><a href='../../my-apps.aspx'><< Back</a></li>"

    End Sub

End Class
