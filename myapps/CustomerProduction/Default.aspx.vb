
Partial Class myapps_CustomerProduction_Default
    Inherits System.Web.UI.Page


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim strSQL As String = ""
        Try
            If Request.QueryString("custid") = "" Then
                If Not IIf(Core.data_select_value("select bitAllowMyApps from tblUsers where intUserPK = " & MySession.UserID) = "True", True, False) Then
                    Response.Redirect(Core.data_select_value("select RawHomeURL from vwUsers where ID = " & MySession.UserID), True)
                End If

                If Not Page.IsPostBack = True Then
                    Dim description As HtmlMeta = New HtmlMeta()
                    description.Name = "description"
                    description.Content = "Utility Masters - My Production"
                    Dim keywords As HtmlMeta = New HtmlMeta()
                    keywords.Name = "keywords"
                    keywords.Content = "My Events"
                    Page.Header.Controls.Add(description)
                    Page.Header.Controls.Add(keywords)
                End If

                If Request.QueryString("custid") Is Nothing Then
                    strSQL = "EXEC spPortal_TREEVIEW " & MySession.UserID
                    Dim myConnection As New OleDb.OleDbConnection(Core.CurrentConnectionString)

                    Dim myCommand As New OleDb.OleDbCommand(strSQL, myConnection)
                    myCommand.Connection.Open()
                    myCommand.CommandTimeout = 180
                    Dim Reader As OleDb.OleDbDataReader = myCommand.ExecuteReader(CommandBehavior.CloseConnection)

                    If Reader.HasRows Then
                        Reader.Read()
                        If Reader("Type") = "Company" Then
                            Session("CurrentSelectedName") = Reader("Name")
                            Session("CurrentSelectedCompanyID") = Reader("Id")
                            Response.Redirect("CustomerProduction.aspx?custid=" & Reader("Id") & "&Customer=" & Reader("Name"), True)
                        End If
                        Reader.Close()
                    End If
                End If

            Else

                strSQL = "SELECT CustomerName FROM   [UML_CMS].dbo.tblcustomer WHERE CustID=" & Request.QueryString("custid")
                Dim myConnection As New OleDb.OleDbConnection(Core.CurrentConnectionString)
                Dim myCommand As New OleDb.OleDbCommand(strSQL, myConnection)
                myCommand.Connection.Open()
                myCommand.CommandTimeout = 180
                Dim Reader As OleDb.OleDbDataReader = myCommand.ExecuteReader(CommandBehavior.CloseConnection)

                If Reader.HasRows Then
                    Reader.Read()
                    Session("CurrentSelectedName") = Reader("CustomerName")
                    Session("CurrentSelectedCompanyID") = Request.QueryString("custid")
                    Response.Redirect("CustomerProduction.aspx?custid=" & Request.QueryString("custid") & "&Customer=" & Reader("CustomerName"), True)
                    Reader.Close()
                End If
            End If


        Catch ex As Exception
        End Try

    End Sub
End Class
