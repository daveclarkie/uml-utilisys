<%@ Page Language="VB" MasterPageFile="~/landing.master" AutoEventWireup="false" CodeFile="CustomerProduction.aspx.vb" Inherits="myapps_CustomerProduction_CustomerProduction" title="Customer Production" %>
<%@ Register Assembly="DundasWebChart" Namespace="Dundas.Charting.WebControl" TagPrefix="DCWC" %>
<asp:Content ID="Content1" ContentPlaceHolderID="content_right" Runat="Server">
<script type="text/javascript" language="javascript">
      function RollOverValues(elementRef,elementVal)
            {
                g_imageWidth = elementRef.style.width;
                g_imageHieght = elementRef.style.height;
                elementRef.style.width = '42px';
                elementRef.style.height = '42px';
               document.getElementById("ctl00_ctl00_content_top_content_right_txtRollOver").value = elementVal;
               //document.getElementById("ctl00_ctl00_content_top_content_right_errorlabe").innerText = elementVal;
            }

function RollOutValues(elementRef)
            {
                g_imageWidth = elementRef.style.width;
                g_imageHieght = elementRef.style.height;
                elementRef.style.width = '38px';
                elementRef.style.height = '38px';
               document.getElementById("ctl00_ctl00_content_top_content_right_txtRollOver").value = "";
               //document.getElementById("ctl00_ctl00_content_top_content_right_errorlabe").innerText ="";
            }

   function sRollOverValues(elementRef,elementVal)
            {
                g_imageWidth = elementRef.style.width;
                g_imageHieght = elementRef.style.height;
                elementRef.style.width = '25px';
                elementRef.style.height = '25px';
               document.getElementById("ctl00_ctl00_content_top_content_right_txtRollOver").value = elementVal;
               //document.getElementById("ctl00_ctl00_content_top_content_right_errorlabe").innerText = elementVal;
            }

function sRollOutValues(elementRef)
            {
                g_imageWidth = elementRef.style.width;
                g_imageHieght = elementRef.style.height;
                elementRef.style.width = '20px';
                elementRef.style.height = '20px';
               document.getElementById("ctl00_ctl00_content_top_content_right_txtRollOver").value = "";
               //document.getElementById("ctl00_ctl00_content_top_content_right_errorlabe").innerText ="";
            }

function tRollOverValues(elementRef,elementVal)
            {
               document.getElementById("ctl00_ctl00_content_top_content_right_txtRollOver").value = elementVal;
               //document.getElementById("ctl00_ctl00_content_top_content_right_errorlabe").innerText = elementVal;
            }

function tRollOutValues(elementRef)
            {
               document.getElementById("ctl00_ctl00_content_top_content_right_txtRollOver").value = "";
               //document.getElementById("ctl00_ctl00_content_top_content_right_errorlabe").innerText ="";
            }
</script>

<form id="form1" runat="server">
  <div class="content_back general">
  
<%--  PanelTopHeader start--%>

 <asp:Label ID="SelectCompany" runat="server"   style="position:absolute;top:20px;left:10px;"  Text="Company"></asp:Label>
 
 <asp:Label ID="errorlabel" runat="server"   style="position:absolute;top:45px;left:10px;"  Text="Label"></asp:Label>
 
<asp:TextBox ID="txtRollOver"  
style="z-index:999;position:absolute;top:82px;left:245px;text-align:left;"  
TextMode="SingleLine" 
BorderStyle="None"
backcolor="transparent"
forecolor="DarkBlue"
font-bold="true"
width="700px" 
Text="" 
runat="server">
</asp:TextBox>
 
 

<%-- <asp:Panel ID="PanelTopHeader" runat="server" Height="125px" Width="125px">
        <asp:Label ID="Label28" style="z-index:100;position:absolute;top:100px;left:300px;"  runat="server" Text="Customer"></asp:Label>
        <asp:ImageButton ID="LoadCompanyName" style="z-index:1;position:absolute;top:105px;left:249px;"  Height="38px" Width="38px"  ImageUrl="~/assets/metering/CompanyID.png" runat="server" />
        <asp:DropDownList ID="CompanySelect"  style="z-index:1;position:absolute;top:125px;left:300px;"  runat="server" Enabled="True"> </asp:DropDownList>
 </asp:Panel>--%>


 
<%--  PanelTopHeader end --%>

<%--  PanelTopItems start--%>
  <asp:Panel ID="PanelTopItems" runat="server"  
                style="position:absolute;top:100px;left:240px;"  
                 visible="false"
                 Height="425px"
                 backImageUrl="~/assets/metering/glassfront.png" 
                 Width="625px"   
                 Borderstyle="Solid"
                 BorderWidth="1"
                 BorderColor="AliceBlue"
                  BackColor="white">

            <asp:DropDownList ID="DropDownList1" runat="server" 
            style="position:absolute;top:20px;left:50px;"  
            DataSourceID="SqlDataSource1"  
            DataTextField="ProductType" 
            DataValueField="intID" 
            Font-Names="Verdana"
            Font-Size="10px"
            Enabled="False" 
            Visible="false"
            Width="200px">
            </asp:DropDownList>
            
        <%--     <asp:LinkButton ID="LinkButtonRefresh" 
              Width="20px"  
              runat="server" 
              style="position:absolute;top:20px;left:15px;"  
              Text="...">
              </asp:LinkButton> 
              --%>
              
               <asp:ImageButton  ID="ImageButtonViewProduction"  
               visible="false"  
               style="position:absolute;top:5px;left:10px;"  
               tooltip="View all production figures for a specific category"  
               Height="38px" Width="38px"   
               ImageUrl="~/assets/metering/greentable.png" runat="server" />
          
            <asp:ImageButton  ID="ImageButtonCategories"  
               visible="true"  
               style="position:absolute;top:5px;left:50px;"  
               tooltip="Add a new Production category"  
               Height="38px" Width="38px"   
               ImageUrl="~/assets/metering/details.png" runat="server" />
          
          
<asp:Panel ID="PanelEachLine"  Visible="false"    runat="server"    style="position:absolute;top:1px;left:50px;"  Height="50px" Width="100px">
<asp:Label ID="Label3"  style="position:absolute;top:1px;left:1px;" runat="server"  Font-Names="Verdana"  Font-Size="10px"  width="200px" Text="Edit selected value "> </asp:Label>
<asp:Label ID="CurrentDate"  style="position:absolute;top:21px;left:1px;" runat="server"    Font-Names="Verdana"  Font-Size="10px"  width="75px"   Text="Date ">   </asp:Label>
<asp:TextBox ID="Alteredvalue"  tooltip="Press the Enter key to Update OR Click the Disk icon"  style="position:absolute;top:21px;left:80px;" Font-Names="Verdana"  maxlength="10" width="75px"  Font-Size="10px"   runat="server"> </asp:TextBox>
                
<asp:ImageButton  ID="ImageButtonUpdateAltered"  
style="position:absolute;top:10px;left:170px;"
visible="true"  
tooltip="Update item"  
Height="20px" Width="20px"   
ImageUrl="~/assets/metering/smallsave.png" runat="server" />     
</asp:Panel>

<%--NewPanel start       --%> 
<asp:Panel ID="NewPanel" style="position:absolute;top:-5px;left:50px;" runat="server" Height="100px"   Visible="False"  Width="300px" >
 <asp:Label ID="Label6"  style="position:absolute;top:1px;left:1px;" runat="server"  Font-Names="Verdana"  Font-Size="10px"  width="200px" Text="Insert Date and Figure"> </asp:Label>
 
 <asp:Label ID="Label12" style="position:absolute;top:21px;left:1px;" runat="server" Font-Names="Verdana" Font-Size="10px" Text="Date"   Width="75px"></asp:Label>
 <asp:TextBox ID="txtNewDate"  style="position:absolute;top:21px;left:60px;" runat="server" Font-Size="10px" Width="75px" Font-Names="Verdana"></asp:TextBox>

<asp:ImageButton ID="ImageButton1" 
style="position:absolute;top:21px;left:142px;"
Height="20px" 
Width="20px"
ToolTip="Select a Production Date"
ImageUrl="~/assets/metering/OrangeCalendar.png" 
runat="server" /> 

<asp:Calendar ID="Calendar1"  
style="position:absolute;top:50px;left:60px;background-color:white; z-index :2002;font-size:7pt"
font-names="verdana"
font-size="10px"

 runat="server" Height="59px" Width="92px"></asp:Calendar>

<asp:Label ID="Label11" style="position:absolute;top:35px;left:1px;" runat="server" Font-Names="Verdana" Font-Size="10px" Text="Production"   Width="75px"></asp:Label>
<asp:TextBox ID="txtNewValue"   style="position:absolute;top:35px;left:60px;" runat="server" Font-Names="Verdana" Font-Size="10px" Width="75px"></asp:TextBox>

<asp:ImageButton  ID="ImageButtonInsertNewDateAndValue"  
style="position:absolute;top:21px;left:167px;"
visible="true"  
tooltip="Update item"  
Height="20px" Width="20px"   
ImageUrl="~/assets/metering/smallsave.png" runat="server" /> 

</asp:Panel>

<%--Newschedule here--%>


<%--SchedulePanel start--%>
 <asp:Panel ID="SchedulePanel" style="position:absolute;top:2px;left:50px;z-index:2003;"   
 BackColor="transparent" runat="server" Height="100px" Width="250px">
 
 <asp:Label ID="Label10" runat="server" 
font-names="verdana"
font-size="10px"
Text="Creating a schedule will ERASE all previous matching criteria" 
Width="250px"></asp:Label>


<asp:Panel ID="Panel1" 
style="position:absolute;top:50px;left:1px"
runat="server" backcolor="black" Height="250px" Width="250px">
      
&nbsp;<asp:Label ID="Label9" runat="server" font-names="verdana" font-size="10px" Text="Exclude item(s)" Width="100px"></asp:Label>
<br />
    &nbsp;<asp:CheckBox ID="chkSaturday" runat="server" font-names="verdana"  font-size="10px" Text=" Saturday" />
<br />
    &nbsp;<asp:CheckBox ID="chkSunday" runat="server" font-names="verdana"  font-size="10px" Text=" Sunday" />
<br />
    &nbsp;<asp:CheckBox ID="chkMonday" runat="server" Font-Names="verdana" Font-Size="10px" Text=" Monday" />
<br />
    &nbsp;<asp:CheckBox ID="chkTuesday" runat="server" font-names="verdana"  font-size="10px"  Text=" Tuesday" />
<br />
    &nbsp;<asp:CheckBox ID="chkWednesday" runat="server" font-names="verdana"  font-size="10px"  Text=" Wednesday" />
<br />
    &nbsp;<asp:CheckBox ID="chkThursday" runat="server" font-names="verdana"  font-size="10px" Text=" Thursday" />
<br />
    &nbsp;<asp:CheckBox ID="chkFriday" runat="server" font-names="verdana"  font-size="10px"  Text=" Friday" />        
      
      <asp:Label ID="Label7" 
      style="position:absolute;top:21px;left:110px"
      runat="server" Font-Names="Verdana" Font-Size="10px" 
      Text="Start date"  Width="62px"></asp:Label>
      
<asp:TextBox ID="ScheduleStart" 
style="position:absolute;top:40px;left:110px"
runat="server" AutoPostBack="true"  Columns="7"  
Font-Names="Verdana" Font-Size="10px" Width="67px"></asp:TextBox>

<asp:ImageButton ID="imgStart" 
style="position:absolute;top:40px;left:190px"
Height="20px" Width="20px" ToolTip="Select a Start Date" ImageUrl="~/assets/metering/OrangeCalendar.png"  runat="server" />

<asp:Label ID="Label8" 
   style="position:absolute;top:60px;left:110px"
runat="server" Font-Names="Verdana" Font-Size="10px" Text="Finish date"  Width="62px"></asp:Label>

<asp:TextBox ID="ScheduleFinish" 
  style="position:absolute;top:80px;left:110px"
runat="server" AutoPostBack="true"   Columns="7"  Font-Names="Verdana" Font-Size="10px"  Width="67px"></asp:TextBox>

<asp:ImageButton ID="imgFinish"  
  style="position:absolute;top:80px;left:190px"
Height="20px"  Width="20px" ToolTip="Select a Finish Date" ImageUrl="~/assets/metering/OrangeCalendar.png"   runat="server" />
<br />   
        
            <asp:Calendar ID="Calendar3" 
             style="position:absolute;top:20px;left:20px;background-color:white; z-index :2004;font-size:7pt" 
            runat="server" 
            borderstyle="Solid"
            borderwidth="1"
            bordercolor="black"
            Height="59px" Width="92px" Visible="False"></asp:Calendar>
       

<asp:ImageButton ID="ImageButtonHideProductionSchedule" 
style="position:absolute;top:110px;left:120px"
Height="20px" Width="20px" ToolTip="Hide this window" ImageUrl="~/assets/metering/hide.png"  runat="server" />
      
<asp:ImageButton ID="ImageButtonCreateProductionSchedule" 
style="position:absolute;top:110px;left:150px"
Height="20px" Width="20px" ToolTip="Create the schedule now" ImageUrl="~/assets/metering/newuom.png"  runat="server" />
      
       
    </asp:Panel>
    
    <%--

<asp:LinkButton ID="UpdateCreate" runat="server" Text="Create"></asp:LinkButton>          
&nbsp;
<asp:LinkButton ID="UpdateCreateHide" runat="server" Text="Hide"></asp:LinkButton>   --%>

                 
      
        </asp:Panel>
<%--SchedulePanel end --%>





<%--NewPanel end  --%> 
          
          
          
          
          
          
          
        <%--   This can be deleted later--%>
            <asp:DropDownList ID="lstViewItems" visible="false" runat="server" Style="font-size:9px;font-family:Arial;" Width="45px"></asp:DropDownList>
         
<asp:ImageButton  ID="ImageButtonCustomerChange"  
visible="false"  
style="position:absolute;top:5px;left:10px;"  
tooltip="Return and change to another customer"  
Height="38px" Width="38px"   
ImageUrl="~/assets/metering/navigate.png" runat="server" />
         
            <asp:DropDownList ID="SummaryYears" 
                     style="position:absolute;top:2px;left:385px;z-index:88;"  
                    runat="server" 
                    visible="false"
                    Font-Names="Verdana"
                    Font-Size="10px"
                    width="54px"
                    BorderStyle="Solid"
                    DataSourceID="SqlDataSource5"  
                    DataTextField="Date" 
                    DataValueField="Date"> 
            </asp:DropDownList>
            
            
<asp:ImageButton  ID="ImageButtonViewProductionTable"  
visible="false"  
style="position:absolute;top:12px;left:400px;"  
tooltip="View the production for this period"  
Height="38px" Width="38px"   
ImageUrl="~/assets/metering/viewgrid.png" runat="server" />
            
           <%--             <asp:LinkButton ID="YearView"   visible="false" runat="server" Text="View" ></asp:LinkButton> --%>

              
<asp:ImageButton  ID="ImageButtonSingleInsert"  
visible="false"  
style="position:absolute;top:5px;left:450px;"  
tooltip="Insert a new production value"  
Height="38px" Width="38px"   
ImageUrl="~/assets/metering/newreadings.png" runat="server" />
               
                  <%-- <asp:LinkButton ID="InsertValue"   visible="false" runat="server" Text="Insert a value" ></asp:LinkButton>--%>

              
<asp:ImageButton  ID="ImageButtonCreateSchedule"  
visible="false"  
style="position:absolute;top:5px;left:500px;"  
tooltip="Create a new production schedule"  
Height="38px" Width="38px"   
ImageUrl="~/assets/metering/history.png" runat="server" />              
    
    
<asp:ImageButton  ID="ImageButtonViewChart" 
style="position:absolute;top:5px;left:550px;"  
visible="false"  
tooltip="View production in a graphical format"  
Height="38px" Width="38px"  
ImageUrl="~/assets/metering/graph.png" runat="server" />    
    

    
    
    <%--<asp:ImageButton  ID="ImageButtonEditItem"  
visible="false"  
style="position:absolute;top:5px;left:600px;"  
tooltip="Modify the current production item"  
Height="38px" Width="38px"   
ImageUrl="~/assets/metering/edititem.png" runat="server" />    --%> 
               
                 <%--       <asp:LinkButton ID="InsertMultiple"   visible="false" runat="server" Text="Insert multiples"></asp:LinkButton>--%>
      
      
      
              
            <asp:GridView ID="GridView2" runat="server"  AllowPaging="True"
                                 style="position:absolute;top:55px;left:50px;"  
                                AutoGenerateColumns="False" 
                                DataSourceID="SqlDataSource3" 
                                Font-Names="Verdana"
                                Font-Size="14px"
                                GridLines="None"
                                PageSize="9"
                                Caption="Selected a category:" 
                                BackColor="transparent" 
                                height="300px"
                                Width="500px">
                                <Columns>
                                    <asp:CommandField ShowSelectButton="True" SelectText="...">
                                           <ItemStyle  Width="8px" Height="8px"/>
                                    </asp:CommandField>
                                
                                <asp:BoundField DataField="CustUM"  visible="False" HeaderText="CustUM" SortExpression="CustUM" />
                                <asp:BoundField DataField="ProductType" HeaderText="Production Type"  ItemStyle-HorizontalAlign="left" SortExpression="ProductType" />
                                <asp:BoundField DataField="mycount" HeaderText="Total Items" ItemStyle-HorizontalAlign="Right" SortExpression="Column1" />
                               <asp:BoundField DataField="range" HeaderText="Range"  ItemStyle-HorizontalAlign="Right" SortExpression="Column1" />
                                </Columns>
                                <PagerStyle   ForeColor="blue"  height="12" HorizontalAlign="Left"  />
                                <PagerSettings PageButtonCount="5"    Position="Bottom" Mode="Numeric" />
                    </asp:GridView>  
            
     <%--        Mode="NextPreviousFirstLast"--%>
            
            <asp:GridView ID="GridView1" runat="server" AllowPaging="True"
            AutoGenerateColumns="False" 
            DataKeyNames="intID" 
            DataSourceID="SqlDataSource2" 
            style="position:absolute;top:55px;left:50px;" 
            Font-Names="Verdana"
            Font-Size="14px"
            PageSize="10"
            GridLines="None"
            width="400px"
            EnableModelValidation="True" 
            AllowSorting="false" 
            Caption="Selected category :" 
            BackColor="Transparent">
            
            <Columns>
               <asp:CommandField ShowSelectButton="True" SelectText="..." >
                     <ItemStyle  Width="8px" Height="8px"/>
                </asp:CommandField>
                
                <asp:BoundField  visible="False" DataField="intID"   HeaderText="intID" InsertVisible="False" ReadOnly="True"  SortExpression="intID" />
                <asp:BoundField  visible="False" DataField="CustID"   HeaderText="CustID" SortExpression="CustID" />
                <asp:BoundField  visible="False" DataField="CustUM"    HeaderText="CustUM" SortExpression="CustUM" />
                <asp:BoundField DataField="ProductionDate"  DataFormatString="{0:dd/MM/yyyy}"  HtmlEncode="False"  HeaderText="Date"> 
                    <ControlStyle Width="25px" />
                </asp:BoundField>
              
               <asp:BoundField DataField="CustValue" HeaderText="Value" ItemStyle-HorizontalAlign="right"  >
                        <ControlStyle Width="10px" /> 
                          <ItemStyle  HorizontalAlign="right"/>
               </asp:BoundField>
               
               <asp:BoundField DataField="DayName" HeaderText="Day" >
                  <ControlStyle Width="20px" />
               </asp:BoundField>
            
            </Columns>
              <PagerStyle  ForeColor="blue"  height="12" HorizontalAlign="Left"  />
              <PagerSettings PageButtonCount="5" Position="Bottom" Mode="Numeric" />
        </asp:GridView>
            
  </asp:Panel>
<%--  PanelTopItems end--%>


                  
                    
        
        
        
<

 
<%--      EditForm start--%>
 <asp:Panel ID="EditForm"  runat="server" Height="450px" Width="450px" Visible="False"   backImageUrl="~/assets/metering/editform.png"  >
     <asp:Panel ID="EditInner" runat="server" style="position:relative;top:20px;left:20px;"  Height="400px" Width="400px">
    
           <asp:LinkButton ID="UpdateValues" runat="server" Text="Update"></asp:LinkButton>
            &nbsp; 
            <asp:LinkButton ID="DeleteValues" runat="server" Text="Delete"></asp:LinkButton>
            &nbsp; 
            <asp:LinkButton ID="HideValues" runat="server" Text="Hide"></asp:LinkButton>
        <br/>
        
        <asp:Label ID="Label5" Font-Names="Verdana" Font-Size="10px"  Width="105px" runat="server" Text="Production value"></asp:Label>
            <asp:TextBox ID="TheValue" runat="server" Columns="7"   AutoPostBack = "true" style="font: menu"></asp:TextBox>
    <br /> 

            <asp:Label ID="Label4" Font-Names="Verdana" Font-Size="10px" Width="105px"  runat="server" Text="Production date"></asp:Label>
            <asp:TextBox ID="TheDate" runat="server"  Columns="7" AutoPostBack = "true"   Font-Names="Verdana" Font-Size="10px" Width="67px" ></asp:TextBox>
            <asp:ImageButton ID="ImageButton2" 
            Height="20px" 
            Width="20px"
            ToolTip="Select a new Production Date"
            ImageUrl="~/assets/metering/OrangeCalendar.png" 
            runat="server" />
            <br />
            <asp:Calendar ID="Calendar2"  style="background-color:white; z-index :100;font-size:7pt" runat="server" Height="59px" Width="92px" Visible="False"></asp:Calendar>
          
    
        </asp:Panel>    
   </asp:Panel>
<%--   EditForm end--%>

        


<%--PanelGridView1 start --%>
 <asp:Panel ID="PanelGridView1" BackColor="#ffffff"  runat="server" Height="100%" Width="400px">
   </asp:Panel>
<%--PanelGridView1 end --%>


<%--CategoryADministration  start --%>
      <asp:Panel ID="PanelAdministration"  
      backcolor="transparent"
      bordercolor="white"
      width="300px"
      height="100px"
      visible="false"
      style="position:absolute;top:100px;left:325px;z-index:2009;"   runat="server">
         
          <asp:HiddenField ID="HiddenFieldCurrentCategoryItem" runat="server" />  
         
          <asp:Label ID="Label1" 
          Font-Names="Verdana" Font-Size="10px" 
          style="position:absolute;top:10px;left:10px;"
          runat="server" Text="Edit category"></asp:Label> 
                        
          <asp:TextBox ID="txtCategoryName" 
           Font-Names="Verdana" Font-Size="10px" 
          width="150px"
          maxlength="75"
          style="position:absolute;top:15px;left:100px;"
          runat="server"></asp:TextBox>

            <asp:ImageButton  ID="ImageButtonCategoryUpdate"  
            style="position:absolute;top:10px;left:265px;"
            visible="true"  
            tooltip="Update category"  
            Height="20px" Width="20px"   
            ImageUrl="~/assets/metering/smallsave.png" runat="server" />     
                 
            <asp:ImageButton  ID="ImageButtonCategoryDelete"  
            style="position:absolute;top:10px;left:297px;"
            visible="true"  
            tooltip="Delete category"  
            Height="20px" Width="20px"   
            ImageUrl="~/assets/metering/smalcancel.png" runat="server" />    
      
      </asp:Panel>
<%--CategoryADministration   end --%>


<%--CategoryADministration  start --%>
      <asp:Panel ID="PanelAdministrationTwo"  
      backcolor="transparent"
      bordercolor="white"
      width="300px"
      height="100px"
      visible="false"
      style="position:absolute;top:100px;left:325px;z-index:2009;"   runat="server">
                
          <asp:Label ID="Label2" 
          Font-Names="Verdana" Font-Size="10px" 
          style="position:absolute;top:10px;left:10px;"
          runat="server" Text="Enter category"></asp:Label> 
                        
          <asp:TextBox ID="txtNewCategoryName" 
          Font-Names="Verdana" Font-Size="10px" 
          width="150px"
          maxlength="75"
          style="position:absolute;top:15px;left:100px;"
          runat="server"></asp:TextBox>

            <asp:ImageButton  ID="ImageButtonNewCategoryUpdate"  
            style="position:absolute;top:10px;left:265px;"
            visible="true"  
            tooltip="Update category"  
            Height="20px" Width="20px"   
            ImageUrl="~/assets/metering/smallsave.png" runat="server" />     
                   
      
      </asp:Panel>
<%--CategoryADministration   end --%>





<%--  C H A R T - start--%>
            <asp:Panel ID="PanelChart" runat="server"  
                style="position:absolute;top:100px;left:240px;"  
                 visible="false"
                 Height="425px"
                 backImageUrl="~/assets/metering/glassfront.png" 
                 Width="625px"   
                 Borderstyle="Solid"
                 BorderWidth="1"
                 BorderColor="AliceBlue"
                  BackColor="white">
                  
                
  <asp:ImageButton 
                  ID="ImageButtonJan"  
                 visible="true"  
                 tooltip="View January Production figures" 
                 style="z-index:1;position:absolute;top:5px;left:20px;" 
                 Height="38px" Width="38px"  
                 ImageUrl="~/assets/metering/jan.png" runat="server" />
              

  <asp:ImageButton 
                  ID="ImageButtonFeb"  
                 visible="true"  
                 tooltip="View February Production figures" 
                 style="z-index:1;position:absolute;top:5px;left:60px;" 
                 Height="38px" Width="38px"  
                 ImageUrl="~/assets/metering/feb.png" runat="server" />
                   

  <asp:ImageButton 
                  ID="ImageButtonMar"  
                 visible="true"  
                 tooltip="View March Production figures" 
                 style="z-index:1;position:absolute;top:5px;left:100px;" 
                 Height="38px" Width="38px"  
                 ImageUrl="~/assets/metering/Mar.png" runat="server" />
              
                
  <asp:ImageButton 
                  ID="ImageButtonApr"  
                 visible="true"  
                 tooltip="View April Production figures" 
                 style="z-index:1;position:absolute;top:5px;left:140px;" 
                 Height="38px" Width="38px"  
                 ImageUrl="~/assets/metering/Apr.png" runat="server" />
             

  <asp:ImageButton 
                  ID="ImageButtonMay"  
                 visible="true"  
                 tooltip="View May Production figures" 
                 style="z-index:1;position:absolute;top:5px;left:180px;" 
                 Height="38px" Width="38px"  
                 ImageUrl="~/assets/metering/May.png" runat="server" />
         
                                           
  <asp:ImageButton 
                  ID="ImageButtonJun"  
                 visible="true"  
                 tooltip="View June Production figures" 
                 style="z-index:1;position:absolute;top:5px;left:220px;" 
                 Height="38px" Width="38px"  
                 ImageUrl="~/assets/metering/Jun.png" runat="server" />
                 
                                       
  <asp:ImageButton 
                  ID="ImageButtonJul"  
                 visible="true"  
                 tooltip="View July Production figures" 
                 style="z-index:1;position:absolute;top:5px;left:260px;" 
                 Height="38px" Width="38px"  
                 ImageUrl="~/assets/metering/Jul.png" runat="server" />                                         
                                          
  <asp:ImageButton 
                  ID="ImageButtonAug"  
                 visible="true"  
                 tooltip="View August Production figures" 
                 style="z-index:1;position:absolute;top:5px;left:300px;" 
                 Height="38px" Width="38px"  
                 ImageUrl="~/assets/metering/Aug.png" runat="server" />     

  <asp:ImageButton 
                  ID="ImageButtonSep"  
                 visible="true"  
                 tooltip="View September Production figures" 
                 style="z-index:1;position:absolute;top:5px;left:340px;" 
                 Height="38px" Width="38px"  
                 ImageUrl="~/assets/metering/Sep.png" runat="server" />                                                               

  <asp:ImageButton 
                  ID="ImageButtonOct"  
                 visible="true"  
                 tooltip="View October Production figures" 
                 style="z-index:1;position:absolute;top:5px;left:380px;" 
                 Height="38px" Width="38px"  
                 ImageUrl="~/assets/metering/Oct.png" runat="server" />                                                

  <asp:ImageButton 
                  ID="ImageButtonNov"  
                 visible="true"  
                 tooltip="View November Production figures" 
                 style="z-index:1;position:absolute;top:5px;left:420px;" 
                 Height="38px" Width="38px"  
                 ImageUrl="~/assets/metering/Nov.png" runat="server" />                                             
                                          

  <asp:ImageButton 
                  ID="ImageButtonDec"  
                 visible="true"  
                 tooltip="View December Production figures" 
                 style="z-index:1;position:absolute;top:5px;left:460px;" 
                 Height="38px" Width="38px"  
                 ImageUrl="~/assets/metering/dec.png" runat="server" />                                                    
                                          

  <asp:ImageButton 
                  ID="ImageButtonYearOverlay"  
                 visible="true"  
                 tooltip="View all yearly figures Overlayed " 
                 style="z-index:1;position:absolute;top:5px;left:500px;" 
                 Height="38px" Width="38px"  
                 ImageUrl="~/assets/metering/overlay.png" runat="server" />                                                   
                                          
                                          
                                          
                                          
                                          
                                                               
                
                
                
                 <asp:ImageButton 
                  ID="ImageButtonCloseChart"  
                 visible="true"  
                 tooltip="Metering personnel" 
                 style="z-index:1;position:absolute;top:5px;left:550px;" 
                 Height="25px" Width="25px"  
                 ImageUrl="~/assets/metering/hide.png" runat="server" />
                <br />
                 
            
                <DCWC:Chart ID="Chart1" 
                    style="position:absolute;top:59px;left:20px;"  
                    runat="server" 
                    width="582px"
                    height="340px"
                    BackColor="Transparent" 
                    BackGradientType="DiagonalLeft" 
                    CallBackStateContent="All"
                    BorderLineColor="DarkKhaki" 
                    DataSourceID="SqlDataSource2"
                    Palette="Light">
                    <Legends>
                        <DCWC:Legend BackColor="Transparent" BorderColor="Transparent" Enabled="False" Name="Default">
                        </DCWC:Legend>
                    </Legends>
                    <Titles>
                        <DCWC:Title Name="TopTitle" Text="Production figures">
                        </DCWC:Title>
                    </Titles>
                    <Series>
                        <DCWC:Series BorderColor="64, 64, 64" BorderWidth="2" ChartType="Line" Name="Series1"
                            ShadowOffset="2" XValueType="DateTime">
                        </DCWC:Series>
                        <DCWC:Series BorderColor="64, 64, 64" BorderWidth="2" ChartType="Line" Name="Series2"
                            ShadowOffset="2" XValueType="DateTime">
                        </DCWC:Series>
                    </Series>
                    <ChartAreas>
                        <DCWC:ChartArea BackColor="Transparent" BorderColor="Transparent" Name="Default">
                            <AxisY Title="Reading">
                                <MajorGrid Enabled="False" />
                                <LabelStyle Format="G" />
                            </AxisY>
                            <AxisX Title="Periods">
                                <MajorGrid Enabled="False" />
                                <LabelStyle Format="d" />
                            </AxisX>
                        </DCWC:ChartArea>
                    </ChartAreas>
                    <BorderSkin FrameBackColor="white" />
                </DCWC:Chart>
            </asp:Panel>
 <%--  C H A R T - end--%>      

        
        
        
        
        &nbsp;
        &nbsp;
        <asp:SqlDataSource ID="SqlDataSource2" runat="server"
            ConnectionString="<%$ ConnectionStrings:portal.utilitymasters.co.ukConnectionString %>"
            DeleteCommand="DELETE FROM [tblCustomer_Production_Values] WHERE [intID] = @original_intID AND (([CustID] = @original_CustID) OR ([CustID] IS NULL AND @original_CustID IS NULL)) AND (([CustUM] = @original_CustUM) OR ([CustUM] IS NULL AND @original_CustUM IS NULL)) AND (([CustValue] = @original_CustValue) OR ([CustValue] IS NULL AND @original_CustValue IS NULL)) AND (([ProductionDate] = @original_ProductionDate) OR ([ProductionDate] IS NULL AND @original_ProductionDate IS NULL))"
            InsertCommand="INSERT INTO [tblCustomer_Production_Values] ([CustID], [CustUM], [CustValue], [ProductionDate]) VALUES (@CustID, @CustUM, @CustValue, @ProductionDate)"
            OldValuesParameterFormatString="original_{0}" 
            SelectCommand="SELECT [intID],[CustID] ,[CustUM ],[CustValue] , [ProductionDate] ,  DATENAME(DW,ProductionDate) as [DayName] FROM [tblCustomer_Production_Values] where [CustID]=0 order by ProductionDate DESC"
            UpdateCommand="UPDATE [tblCustomer_Production_Values] SET [CustID] = @CustID, [CustUM] = @CustUM, [CustValue] = @CustValue, [ProductionDate] = @ProductionDate WHERE [intID] = @original_intID AND (([CustID] = @original_CustID) OR ([CustID] IS NULL AND @original_CustID IS NULL)) AND (([CustUM] = @original_CustUM) OR ([CustUM] IS NULL AND @original_CustUM IS NULL)) AND (([CustValue] = @original_CustValue) OR ([CustValue] IS NULL AND @original_CustValue IS NULL)) AND (([ProductionDate] = @original_ProductionDate) OR ([ProductionDate] IS NULL AND @original_ProductionDate IS NULL))">
            
            <DeleteParameters>
                <asp:Parameter Name="original_intID" Type="Int32" />
                <asp:Parameter Name="original_CustID" Type="Int32" />
                <asp:Parameter Name="original_CustUM" Type="String" />
                <asp:Parameter Name="original_CustValue" Type="String" />
                <asp:Parameter DbType="DateTime" Name="original_ProductionDate" />
            </DeleteParameters>
            
            <UpdateParameters>
                <asp:Parameter Name="CustID" Type="Int32" />
                <asp:Parameter Name="CustUM" Type="String" />
                <asp:Parameter Name="CustValue" Type="String" />
                <asp:Parameter DbType="DateTime" Name="ProductionDate" />
                
                <asp:Parameter Name="original_intID" Type="Int32" />
                <asp:Parameter Name="original_CustID" Type="Int32" />
                <asp:Parameter Name="original_CustUM" Type="String" />
                <asp:Parameter Name="original_CustValue" Type="String" />
                <asp:Parameter DbType="DateTime" Name="original_ProductionDate" />
            </UpdateParameters>
            
            <InsertParameters>
                <asp:Parameter Name="CustID" Type="Int32" />
                <asp:Parameter Name="CustUM" Type="String" />
                <asp:Parameter Name="CustValue" Type="String" />
                <asp:Parameter DbType="DateTime" Name="ProductionDate" />
            </InsertParameters>
            
        </asp:SqlDataSource>
    

    <asp:HiddenField ID="CompanyID" value="0" runat="server" />
     <asp:HiddenField ID="UnitOfMeasure" value="0" runat="server" />
  
     <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:portal.utilitymasters.co.ukConnectionString %>"
         DeleteCommand="DELETE FROM [tblCustomer_Product_Type] WHERE [intID] = @intID"
         InsertCommand="INSERT INTO [tblCustomer_Product_Type] ([CustID], [ProductType]) VALUES (@CustID, @ProductType)"
         SelectCommand="SELECT * FROM [tblCustomer_Product_Type] where custid=0 ORDER BY [ProductType]"
         UpdateCommand="UPDATE [tblCustomer_Product_Type] SET [CustID] = @CustID, [ProductType] = @ProductType WHERE [intID] = @intID">
         <DeleteParameters>
             <asp:Parameter Name="intID" Type="Int32" />
         </DeleteParameters>
         <UpdateParameters>
             <asp:Parameter Name="CustID" Type="Int32" />
             <asp:Parameter Name="ProductType" Type="String" />
             <asp:Parameter Name="intID" Type="Int32" />
         </UpdateParameters>
         <InsertParameters>
             <asp:Parameter Name="CustID" Type="Int32" />
             <asp:Parameter Name="ProductType" Type="String" />
         </InsertParameters>
        
         
     </asp:SqlDataSource>
       
     <br />
     <br />
 
   
     <asp:Button ID="Button3" runat="server" Text="Loop" Visible="False" /><br />
     <asp:Button ID="Button2" runat="server" Text="Update" Visible="False" />
     <asp:Button ID="TestMe" runat="server" Text="Button" Visible="False" />&nbsp;<br />
    &nbsp;
    
 
    
     

      <asp:SqlDataSource
                id="SqlDataSource6"
                runat="server"
                ConnectionString="<%$ ConnectionStrings:portal.utilitymasters.co.ukConnectionString %>"
                SelectCommand="SELECT * from dbo.tblCustomer_Production_Values"
                FilterExpression="ProductionDate='{0}'">
                <FilterParameters>
                    <asp:ControlParameter ControlID="SummaryYears" DefaultValue="1900" Name="myDate"
                        PropertyName="SelectedValue" />
                </FilterParameters>
            </asp:SqlDataSource>
    
    
      <asp:SqlDataSource 
       ID="SQLuniversal" 
       runat="server" ConnectionString="<%$ ConnectionStrings:portal.utilitymasters.co.ukConnectionString %>"
       SelectCommand="SELECT  1-1 AS name">
       </asp:SqlDataSource>
     <asp:SqlDataSource ID="SqlDataSource5" runat="server" ConnectionString="<%$ ConnectionStrings:portal.utilitymasters.co.ukConnectionString %>"
         SelectCommand="SELECT * FROM dbo.tblCustomer_Product_Type&#13;&#10;WHERE CustID=872&#13;&#10;ORDER BY ProductType">
     </asp:SqlDataSource>
     <br />
     <br />
    
    
    <asp:SqlDataSource ID="SqlDataSource3" runat="server" 
      ConnectionString="<%$ ConnectionStrings:portal.utilitymasters.co.ukConnectionString %>"
       
       
       SelectCommand="exec spProductType_RangeCombined 0 "></asp:SqlDataSource> 
       <%--
       SelectCommand="SELECT tblCustomer_Production_Values.CustUM &#13;&#10;, COUNT(*) ,  tblCustomer_Product_Type.ProductType&#13;&#10;  , dbo.fnSummary_ReturnedDates(tblCustomer_Production_Values.CustUM,'0')  as 'Range'  FROM dbo.tblCustomer_Production_Values&#13;&#10;INNER JOIN tblCustomer_Product_Type ON tblCustomer_Product_Type.intID&#13;&#10;= tblCustomer_Production_Values.CustUM&#13;&#10;AND tblCustomer_Product_Type.CustID='0'&#13;&#10;GROUP BY tblCustomer_Production_Values.CustUM,tblCustomer_Product_Type.ProductType"></asp:SqlDataSource>
          --%>
            <asp:SqlDataSource ID="SqlDataSource4" runat="server" ConnectionString="<%$ ConnectionStrings:portal.utilitymasters.co.ukConnectionString %>"
                SelectCommand="SELECT TOP(1) * FROM dbo.tblCustomer_Production_Values&#13;&#10; "></asp:SqlDataSource>
          <br />
          
  <%--    <asp:SqlDataSource ID="SqlDeleteCategoryTester" 
      ConnectionString="<%$ ConnectionStrings:portal.utilitymasters.co.ukConnectionString %>"
       SelectCommand=" SELECT   COUNT(*)  from dbo.tblCustomer_Production_Values WHERE custUM=0 "
      runat="server">
      </asp:SqlDataSource>--%>
     
    
      <asp:HiddenField ID="HiddenFieldSelectedCategoryTotal" runat="server" />
          <asp:HiddenField ID="HiddenFieldSelectedCategoryText" runat="server" />
        <asp:HiddenField ID="HiddeniIndex" value="1" runat="server" />
        <asp:HiddenField ID="SelectedYear" runat="server" />
        <asp:HiddenField ID="HiddenField1" runat="server" />
    
    
        </div>
    </form>


</asp:Content>

