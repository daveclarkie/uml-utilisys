Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Drawing
Imports System.Web
Imports System.Web.SessionState
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports Dundas.Charting.WebControl
Imports System.Data
Imports System.Data.OleDb
Imports System.Drawing.Text


Partial Class myapps_CustomerProduction_CustomerProduction
    Inherits System.Web.UI.Page
    Public NewDateClicked As Boolean = False
    Public ModifiedClicked As Boolean = False

    Dim chart11 As Dundas.Charting.WebControl.Chart
    Dim iTheIndex As Int32 = 0
    Dim iTheValue As String = ""
    Dim iTheDate As String = ""

    Protected Sub GridView1_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridView1.PageIndexChanging
        ' john smith
        Dim strSQL As String = ""
        strSQL = " SELECT [intID],[CustID] ,[CustUM ],[CustValue] , [ProductionDate] ,DATENAME(DW,ProductionDate) as [DayName] FROM [tblCustomer_Production_Values] "
        strSQL = strSQL & " WHERE [CustID] = " & Me.CompanyID.Value
        strSQL = strSQL & " AND [CustUM] = " & Me.HiddenFieldCurrentCategoryItem.Value 'Me.UnitOfMeasure.Value
        strSQL = strSQL & " AND [ProductionDate] LIKE '%" & Trim(SummaryYears.Text) & "%'"
        strSQL = strSQL & " Order By [ProductionDate] DESC"
        Me.SqlDataSource2.SelectCommand = strSQL


        'Call ShowGraph()

        'EditForm.Visible = False
    End Sub

    Protected Sub GridView1_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView1.RowCommand
        Me.NewPanel.Visible = False
        Me.SchedulePanel.Visible = False
        'SchedulePanel.Visible = False
        '  Button5.Visible = True 
        '  MultipleDates.Visible = True
        On Error GoTo Leavesafe
        If IsNumeric(e.CommandArgument) = True Then
            PanelEachLine.Visible = True

            Dim index = Convert.ToInt32(e.CommandArgument)
            Dim mySelectedRow As GridViewRow = GridView1.Rows(index)
            Me.TheValue.Text = mySelectedRow.Cells(5).Text
            Me.TheDate.Text = mySelectedRow.Cells(4).Text


            Me.CurrentDate.Text = Trim(mySelectedRow.Cells(4).Text)
            Me.Alteredvalue.Text = Trim(mySelectedRow.Cells(5).Text)
            Me.Alteredvalue.Focus()

            GridView1.Caption = "Selected category ~ " & Trim(DropDownList1.SelectedItem.Text)

            HiddeniIndex.Value = index
            '   EditForm.Height = 100
            '   EditForm.Visible = True
            Exit Sub
        Else
            '  EditForm.Visible = False
            Exit Sub
        End If

Leavesafe:
        If Err.Number = 13 Then
            EditForm.Visible = False
            MsgBox("is 13")
        Else
            MsgBox("GridRow Command" & vbNewLine & Err.Number & vbNewLine & Err.Description)
        End If
        Exit Sub
    End Sub

    Protected Function UpdateGridView(ByVal this1, ByVal this2) As String
        On Error GoTo LeaveSafe
        Dim TheYear As String = Right("0000" & Year(CDate(TheDate.Text)), 4)
        Dim TheMonth As String = Right("00" & Month(CDate(TheDate.Text)), 2)
        Dim TheDay As String = Right("00" & Day(CDate(TheDate.Text)), 2)
        Dim TheStringDate As String = Trim(TheDate.Text)
        Dim strSQL As String
        Dim nReturn As String = "True"
        Dim mySelectedRow As GridViewRow = GridView1.Rows(HiddeniIndex.Value)
        Dim uintId As String = GridView1.DataKeys(HiddeniIndex.Value).Value
        Dim ID As String = Val(mySelectedRow.Cells(1).Text)
        mySelectedRow.Cells(5).Text = Trim(TheValue.Text)
        mySelectedRow.Cells(4).Text = Trim(TheDate.Text)

        strSQL = "UPDATE [tblCustomer_Production_Values] "
        strSQL = strSQL & " SET [CustValue] = " & Trim(TheValue.Text)
        strSQL = strSQL & ", [ProductionDate] = " & "'" & Trim(TheStringDate) & "'"
        strSQL = strSQL & " WHERE [intID] = " & uintId

        SqlDataSource2.UpdateCommand = strSQL
        SqlDataSource2.Update()
        GridView1.DataBind()

        UpdateGridView = nReturn
        Exit Function

LeaveSafe:
        If Err.Number = 5 Then
            MsgBox("is 5")
        Else
            MsgBox("UpdateGridView" & vbNewLine & Err.Number & vbNewLine & Err.Description)
        End If

        Exit Function
        'Resume Next
    End Function

    'Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
    '    Dim strSQL As String = ""
    '    Call UpdateGridView(Me.TheDate.Text, Me.TheValue.Text)
    '    strSQL = " SELECT [intID],[CustID] ,[CustUM ],[CustValue] , [ProductionDate] ,DATENAME(DW,ProductionDate) as [DayName] FROM [tblCustomer_Production_Values] "
    '    strSQL = strSQL & " WHERE [CustID] = " & Me.CompanyID.Value
    '    strSQL = strSQL & " AND [CustUM] = " & Me.UnitOfMeasure.Value
    '    strSQL = strSQL & " AND [ProductionDate] LIKE '%" & Trim(SummaryYears.Text) & "%'"
    '    strSQL = strSQL & " Order By [ProductionDate] DESC"

    '    SqlDataSource2.SelectCommand = strSQL
    '    EditForm.Visible = False
    '    NewPanel.Visible = False
    '    Chart1.Visible = True

    'End Sub

    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound
        On Error Resume Next
        Select Case e.Row.Cells(6).Text
            Case "Sunday" : e.Row.Cells(6).ForeColor = Drawing.Color.Red
            Case "Saturday" : e.Row.Cells(6).ForeColor = Drawing.Color.OrangeRed
                'Case "Monday" : e.Row.Cells(6).ForeColor = System.Drawing.ColorTranslator.FromHtml("#B2F0FF")
                'Case "Tuesday" : e.Row.Cells(6).ForeColor = System.Drawing.ColorTranslator.FromHtml("#B2E0FF")
                'Case "Wednesday" : e.Row.Cells(6).ForeColor = System.Drawing.ColorTranslator.FromHtml("#B2D0FF")
                'Case "Thursday" : e.Row.Cells(6).ForeColor = System.Drawing.ColorTranslator.FromHtml("#B2C0FF")
                'Case "Friday" : e.Row.Cells(6).ForeColor = System.Drawing.ColorTranslator.FromHtml("#B2B0FF")
        End Select
    End Sub

    Protected Sub TestMe_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles TestMe.Click
        SqlDataSource2.UpdateParameters("original_CustValue").DefaultValue = Me.TheValue.Text
        SqlDataSource2.UpdateParameters("CustValue").DefaultValue = Me.TheValue.Text
        SqlDataSource2.Update()
        GridView1.DataBind()
    End Sub

    Protected Sub Button2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button2.Click
        Dim strSQL As String = ""
    End Sub

    Protected Sub Button3_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button3.Click
        ' Grab each element from the current visible collection
        Dim iString As String = ""
        Dim tIntID As String = ""
        Dim tCustID As String = ""
        Dim tCustUM As String = ""
        Dim tCustValue As String = ""
        Dim tProductionDate As String = ""
        Dim row As GridViewRow

        ' & " - " & Eval(Page.GetDataItem.ToString)

        For Each row In Me.GridView1.Rows
            tIntID = row.Cells(1).Text
            tCustID = row.Cells(2).Text
            tCustUM = row.Cells(3).Text
            tCustValue = row.Cells(4).Text
            tProductionDate = row.Cells(5).Text

            iString = ""
            iString = iString & tIntID & vbNewLine
            iString = iString & tCustID & vbNewLine
            iString = iString & tCustUM & vbNewLine
            iString = iString & tCustValue & vbNewLine
            iString = iString & tProductionDate

            MsgBox(iString)
            '  row.Cells(0).Value = DateTime.Now
        Next row

    End Sub

          

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init

        Me.CompanyID.Value = Request.QueryString("custid")
        Me.SelectCompany.Text = Request.QueryString("Customer")

        '    MsgBox("Page Int" & Me.CompanyID.Value & vbNewLine & Me.SelectCompany.Text)

        '   Me.CompanyID.Value = 0
        Me.UnitOfMeasure.Value = 0
        SchedulePanel.Height = 150
        SchedulePanel.Visible = False
        GridView1.Visible = False
        GridView2.Visible = False
        Chart1.Visible = False

        Me.SummaryYears.Visible = False

        NewDateClicked = False
        ModifiedClicked = False
        Calendar1.Visible = False

        lstViewItems.Items.Add("366")
        lstViewItems.Items.Add("60")
        lstViewItems.Items.Add("30")

        GridView1.SelectedRowStyle.BackColor = Color.Transparent ' System.Drawing.ColorTranslator.FromHtml("#A2C7A4")
        GridView1.SelectedRowStyle.BorderStyle = BorderStyle.Solid
        GridView1.SelectedRowStyle.BorderWidth = 1
        GridView1.SelectedRowStyle.BorderColor = Color.LightGray
        GridView1.SelectedRowStyle.ForeColor = Drawing.Color.Black
        GridView1.PagerStyle.HorizontalAlign = HorizontalAlign.Left

        GridView2.SelectedRowStyle.BackColor = Color.Transparent
        GridView2.SelectedRowStyle.BorderStyle = BorderStyle.Solid
        GridView2.SelectedRowStyle.BorderWidth = 1
        GridView2.PageSize = 7
        GridView2.SelectedRowStyle.BorderColor = Color.LightGray
        GridView2.SelectedRowStyle.ForeColor = Drawing.Color.Black
        GridView2.PagerStyle.HorizontalAlign = HorizontalAlign.Left
        GridView2.Visible = False
        GridView1.Visible = False
        PanelTopItems.Visible = False
        Me.DropDownList1.Items.Clear()

        'Me.SqlDataSource1.SelectCommand = strSQL
        'Me.DropDownList1.DataBind()

        'If Me.DropDownList1.Items.Count > 0 Then
        '    GridView2.Visible = True
        '    '     PanelTopItems.Visible = True
        'Else
        '    GridView2.Visible = False
        '    '    PanelTopItems.Visible = False
        'End If

        PanelTopItems.Visible = True
        Me.ImageButtonViewProductionTable.Visible = False
        Me.ImageButtonSingleInsert.Visible = False
        Me.ImageButtonCreateSchedule.Visible = False
        Me.EditForm.Visible = False
        Me.NewPanel.Visible = False
        Me.SchedulePanel.Visible = False

    End Sub

  

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim strSQL As String = ""
        '  Me.SelectCompany.Text = Session("CurrentSelectedCompanyID")
        '  Session("CurrentSelectedName") = .CompanyNumber
        '  Session("CurrentSelectedCompanyID") = .Company

        Me.CompanyID.Value = Request.QueryString("custid")
        Me.SelectCompany.Text = Request.QueryString("Customer")



        strSQL = ""
        strSQL = " spProductType_RangeCombined " & Me.CompanyID.Value
        Me.SqlDataSource3.SelectCommand = strSQL
        GridView2.DataBind()


        ' just keep strSQL = " EXEC dbo.spCustomer_Product_Type " & Me.CompanyID.Value

        'If Me.IsPostBack = True Then
        '    MsgBox("Postback")
        'Else
        '    MsgBox("Not Post back")
        'End If
        '        MsgBox(Session("CurrentSelectedCompanyID"))

        ' try and catch bits here
        Me.ImageButtonCreateSchedule.Attributes.Add("onmouseover", "return RollOverValues(this,'Create a production schedule    ')")
        Me.ImageButtonCreateSchedule.Attributes.Add("onmouseout", "return RollOutValues(this)")

        Me.ImageButtonViewProductionTable.Attributes.Add("onmouseover", "return RollOverValues(this,'View production history values ')")
        Me.ImageButtonViewProductionTable.Attributes.Add("onmouseout", "return RollOutValues(this)")

        Me.ImageButtonSingleInsert.Attributes.Add("onmouseover", "return RollOverValues(this,'Insert a single new entry ')")
        Me.ImageButtonSingleInsert.Attributes.Add("onmouseout", "return RollOutValues(this)")

        Me.ImageButtonViewChart.Attributes.Add("onmouseover", "return RollOverValues(this,'View hsitorical data in a graphical format ')")
        Me.ImageButtonViewChart.Attributes.Add("onmouseout", "return RollOutValues(this)")

        Me.ImageButtonCustomerChange.Attributes.Add("onmouseover", "return RollOverValues(this,'Change the current Category to another one ')")
        Me.ImageButtonCustomerChange.Attributes.Add("onmouseout", "return RollOutValues(this)")

        Me.ImageButtonViewProduction.Attributes.Add("onmouseover", "return RollOverValues(this,'View all production figures for a specific category')")
        Me.ImageButtonViewProduction.Attributes.Add("onmouseout", "return RollOutValues(this)")

        Me.ImageButtonCloseChart.Attributes.Add("onmouseover", "return RollOverValues(this,'Close this window ')")
        Me.ImageButtonCloseChart.Attributes.Add("onmouseout", "return RollOutValues(this)")

        Me.ImageButtonCategories.Attributes.Add("onmouseover", "return RollOverValues(this,'Add a new Production category ')")
        Me.ImageButtonCategories.Attributes.Add("onmouseout", "return RollOutValues(this)")


        ' ==============  M O N T H S ============================
        Me.ImageButtonJan.Attributes.Add("onmouseover", "return RollOverValues(this,'View January production figures ')")
        Me.ImageButtonJan.Attributes.Add("onmouseout", "return RollOutValues(this)")

        Me.ImageButtonFeb.Attributes.Add("onmouseover", "return RollOverValues(this,'View February production figures ')")
        Me.ImageButtonFeb.Attributes.Add("onmouseout", "return RollOutValues(this)")

        Me.ImageButtonMar.Attributes.Add("onmouseover", "return RollOverValues(this,'View March production figures ')")
        Me.ImageButtonMar.Attributes.Add("onmouseout", "return RollOutValues(this)")

        Me.ImageButtonApr.Attributes.Add("onmouseover", "return RollOverValues(this,'View April production figures ')")
        Me.ImageButtonApr.Attributes.Add("onmouseout", "return RollOutValues(this)")

        Me.ImageButtonMay.Attributes.Add("onmouseover", "return RollOverValues(this,'View May production figures ')")
        Me.ImageButtonMay.Attributes.Add("onmouseout", "return RollOutValues(this)")

        Me.ImageButtonJun.Attributes.Add("onmouseover", "return RollOverValues(this,'View June production figures ')")
        Me.ImageButtonJun.Attributes.Add("onmouseout", "return RollOutValues(this)")

        Me.ImageButtonJul.Attributes.Add("onmouseover", "return RollOverValues(this,'View July production figures ')")
        Me.ImageButtonJul.Attributes.Add("onmouseout", "return RollOutValues(this)")

        Me.ImageButtonAug.Attributes.Add("onmouseover", "return RollOverValues(this,'View August production figures ')")
        Me.ImageButtonAug.Attributes.Add("onmouseout", "return RollOutValues(this)")

        Me.ImageButtonSep.Attributes.Add("onmouseover", "return RollOverValues(this,'View September production figures ')")
        Me.ImageButtonSep.Attributes.Add("onmouseout", "return RollOutValues(this)")

        Me.ImageButtonOct.Attributes.Add("onmouseover", "return RollOverValues(this,'View October production figures ')")
        Me.ImageButtonOct.Attributes.Add("onmouseout", "return RollOutValues(this)")

        Me.ImageButtonNov.Attributes.Add("onmouseover", "return RollOverValues(this,'View November production figures ')")
        Me.ImageButtonNov.Attributes.Add("onmouseout", "return RollOutValues(this)")

        Me.ImageButtonDec.Attributes.Add("onmouseover", "return RollOverValues(this,'View December production figures ')")
        Me.ImageButtonDec.Attributes.Add("onmouseout", "return RollOutValues(this)")


        Me.ImageButtonYearOverlay.Attributes.Add("onmouseover", "return RollOverValues(this,'View all yearly production figures overlayed ')")
        Me.ImageButtonYearOverlay.Attributes.Add("onmouseout", "return RollOutValues(this)")
        ' ==============  M O N T H S ============================



        'Me.ImageButtonEditItem.Attributes.Add("onmouseover", "return RollOverValues(this,'Modify the current production entry ')")
        'Me.ImageButtonEditItem.Attributes.Add("onmouseout", "return RollOutValues(this)")

        Me.ImageButtonUpdateAltered.Attributes.Add("onmouseover", "return sRollOverValues(this,'Update selected value ')")
        Me.ImageButtonUpdateAltered.Attributes.Add("onmouseout", "return sRollOutValues(this)")

        Me.imgStart.Attributes.Add("onmouseover", "return sRollOverValues(this,'Select a Start Date')")
        Me.imgStart.Attributes.Add("onmouseout", "return sRollOutValues(this)")

        Me.imgFinish.Attributes.Add("onmouseover", "return sRollOverValues(this,'Select a Finish Date')")
        Me.imgFinish.Attributes.Add("onmouseout", "return sRollOutValues(this)")

        Me.ImageButtonHideProductionSchedule.Attributes.Add("onmouseover", "return sRollOverValues(this,'Hide this window')")
        Me.ImageButtonHideProductionSchedule.Attributes.Add("onmouseout", "return sRollOutValues(this)")

        Me.ImageButtonCreateProductionSchedule.Attributes.Add("onmouseover", "return sRollOverValues(this,'Create the schedule now')")
        Me.ImageButtonCreateProductionSchedule.Attributes.Add("onmouseout", "return sRollOutValues(this)")

        Me.ImageButtonInsertNewDateAndValue.Attributes.Add("onmouseover", "return sRollOverValues(this,'Insert Date and Value now')")
        Me.ImageButtonInsertNewDateAndValue.Attributes.Add("onmouseout", "return sRollOutValues(this)")

        Me.ImageButton1.Attributes.Add("onmouseover", "return sRollOverValues(this,'Select a Production Date')")
        Me.ImageButton1.Attributes.Add("onmouseout", "return sRollOutValues(this)")

        Me.ImageButtonCategoryUpdate.Attributes.Add("onmouseover", "return sRollOverValues(this,'Update category')")
        Me.ImageButtonCategoryUpdate.Attributes.Add("onmouseout", "return sRollOutValues(this)")

        Me.ImageButtonCategoryDelete.Attributes.Add("onmouseover", "return sRollOverValues(this,'Delete category')")
        Me.ImageButtonCategoryDelete.Attributes.Add("onmouseout", "return sRollOutValues(this)")

        Me.ImageButtonNewCategoryUpdate.Attributes.Add("onmouseover", "return sRollOverValues(this,'Update category')")
        Me.ImageButtonNewCategoryUpdate.Attributes.Add("onmouseout", "return sRollOutValues(this)")







        'Me.GridView1.Attributes.Add("onmouseover", "return tRollOver(this,'Select a Production value')")
        ' Me.GridView1.Attributes.Add("onmouseout", "return tRollOver(this)")

        'Me.GridView2.Attributes.Add("onmouseover", "return tRollOver(this,'Select a Production category')")
        'Me.GridView2.Attributes.Add("onmouseout", "return tRollOver(this)")


        ' ImageButtonEditItem


        'Me.ImageButton2.Attributes.Add("onmouseover", "return RollOverValues(this' Button1 ')")
        'Me.ImageButton2.Attributes.Add("onmouseout", "return RollOutValues(this)")

        'Me.ImageButton1.Attributes.Add("onmouseover", "return RollOverValues(this,'button2 ')")
        'Me.ImageButton1.Attributes.Add("onmouseout", "return RollOutValues(this)")



    End Sub

    'Protected Sub Button6_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button6.Click
    '    Dim mySelectedRow As GridViewRow = GridView1.Rows(HiddeniIndex.Value)
    '    Dim uintId As String = GridView1.DataKeys(HiddeniIndex.Value).Value
    '    Dim ID As String = Val(mySelectedRow.Cells(1).Text)
    '    Dim strSQL As String = ""
    '    strSQL = "Delete from [tblCustomer_Production_Values] "
    '    strSQL = strSQL & " WHERE [intID] = " & uintId

    '    SqlDataSource2.UpdateCommand = strSQL
    '    SqlDataSource2.Update()
    '    GridView1.DataBind()

    '    strSQL = ""
    '    strSQL = "Select * from  [tblCustomer_Production_Values] "
    '    strSQL = strSQL & " WHERE [CustID] = " & Me.CompanyID.Value
    '    strSQL = strSQL & " AND [CustUM] = " & "'" & Me.UnitOfMeasure.Value & "'"
    '    strSQL = strSQL & " Order By [ProductionDate] DESC"

    '    SqlDataSource2.SelectCommand = strSQL

    '    strSQL = ""
    '    strSQL = strSQL & "  SELECT tblCustomer_Production_Values.CustUM"
    '    strSQL = strSQL & "  , COUNT(*) , tblCustomer_Product_Type.ProductType ,"
    '    strSQL = strSQL & " dbo.fnSummary_ReturnedDates(tblCustomer_Production_Values.CustUM,'872') AS 'Range' "
    '    strSQL = strSQL & "   FROM dbo.tblCustomer_Production_Values "
    '    strSQL = strSQL & "  INNER JOIN tblCustomer_Product_Type ON tblCustomer_Product_Type.intID= tblCustomer_Production_Values.CustUM  "
    '    strSQL = strSQL & " AND tblCustomer_Product_Type.CustID=" & "'" & Me.CompanyID.Value & "'"
    '    strSQL = strSQL & " GROUP BY tblCustomer_Production_Values.CustUM , tblCustomer_Product_Type.ProductType "
    '    SqlDataSource3.SelectCommand = strSQL

    '    Me.EditForm.Visible = False

    'End Sub

    Protected Sub lstViewItems_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstViewItems.TextChanged
        ' MsgBox(lstViewItems.Text)
        GridView1.PageSize = lstViewItems.Text
    End Sub

    ' '' ''Protected Sub butRefresh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butRefresh.Click
    ' '' ''    Dim strSQL As String = ""
    ' '' ''    Dim YearstrSQL As String = ""
    ' '' ''    Dim iIndex As Integer = 0
    ' '' ''    Dim iItem As String = ""
    ' '' ''    iIndex = DropDownList1.SelectedValue
    ' '' ''    iItem = DropDownList1.SelectedItem.Text
    ' '' ''    Me.UnitOfMeasure.Value = iIndex

    ' '' ''    Me.SummaryYears.Visible = True
    ' '' ''    Me.butFilter.Visible = True

    ' '' ''    YearstrSQL = ""
    ' '' ''    YearstrSQL = YearstrSQL & " SELECT   year(productiondate) AS 'Date' from dbo.tblCustomer_Production_Values  "
    ' '' ''    YearstrSQL = YearstrSQL & " WHERE custUM=" & iIndex
    ' '' ''    YearstrSQL = YearstrSQL & "  AND CustID=" & Me.CompanyID.Value
    ' '' ''    YearstrSQL = YearstrSQL & "  GROUP BY  year(productiondate) "
    ' '' ''    YearstrSQL = YearstrSQL & "  ORDER BY  year(productiondate)  DESC"

    ' '' ''    Me.SummaryYears.Visible = True

    ' '' ''    Me.SqlDataSource5.SelectCommand = YearstrSQL
    ' '' ''    SummaryYears.SelectedIndex = SummaryYears.Items.IndexOf(SummaryYears.Items.FindByText(SelectedYear.Value.ToString))

    ' '' ''    strSQL = ""
    ' '' ''    strSQL = " SELECT [intID],[CustID] ,[CustUM ],[CustValue] , [ProductionDate] ,DATENAME(DW,ProductionDate) as [DayName] FROM [tblCustomer_Production_Values] "
    ' '' ''    strSQL = strSQL & " WHERE [CustID] = " & Me.CompanyID.Value
    ' '' ''    strSQL = strSQL & " AND [CustUM] = " & iIndex
    ' '' ''    'strSQL = strSQL & " AND [ProductionDate] LIKE '%" & Trim(SummaryYears.Text) & "%'"
    ' '' ''    strSQL = strSQL & " AND [ProductionDate] LIKE '%" & Me.SelectedYear.Value & "%'"
    ' '' ''    strSQL = strSQL & " Order By [ProductionDate] DESC"
    ' '' ''    SqlDataSource2.SelectCommand = strSQL

    ' '' ''    GridView1.Caption = Trim(DropDownList1.SelectedItem.Text)

    ' '' ''    Me.NewPanel.Visible = False
    ' '' ''    Me.EditForm.Visible = False
    ' '' ''    Me.SchedulePanel.Visible = False

    ' '' ''    GridView1.DataBind()
    ' '' ''    GridView1.Visible = True

    ' '' ''    Button5.Visible = True
    ' '' ''    MultipleDates.Visible = True


    ' '' ''End Sub

    Protected Function Summary() As String
        Summary = "Ok"
    End Function

    Protected Sub GridView2_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridView2.PageIndexChanging
        Dim StrSQL As String = ""
        StrSQL = ""
        StrSQL = " spProductType_RangeCombined " & Me.CompanyID.Value
        Me.SqlDataSource3.SelectCommand = StrSQL
    End Sub


    Protected Sub GridView2_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView2.RowCommand
        Dim iSelectedText As String
        Dim mySplit() As String
        Me.PanelAdministration.Visible = "true"
        Me.PanelAdministrationTwo.Visible = "false"

        Me.SummaryYears.Items.Clear()
        Me.SummaryYears.Visible = False
        '   Me.butFilter.Visible = False

        If IsNumeric(e.CommandArgument) = True Then
            Dim index = Convert.ToInt32(e.CommandArgument)
            Dim mySelectedRow As GridViewRow = GridView2.Rows(index)
            iSelectedText = Trim(mySelectedRow.Cells(2).Text)
            Me.HiddenFieldSelectedCategoryText.Value = Trim(mySelectedRow.Cells(2).Text)
            Me.HiddenFieldSelectedCategoryTotal.Value = Trim(mySelectedRow.Cells(3).Text)

            MsgBox(Trim(mySelectedRow.Cells(3).Text))

            If Trim(mySelectedRow.Cells(3).Text) = "0" Then
                Me.ImageButtonViewProduction.Visible = False
                Me.ImageButtonSingleInsert.Visible = True
                Me.ImageButtonCreateSchedule.Visible = True
            Else
                Me.ImageButtonViewProduction.Visible = True
            End If


            DropDownList1.SelectedIndex = DropDownList1.Items.IndexOf(DropDownList1.Items.FindByText(iSelectedText))

            mySplit = Trim(mySelectedRow.Cells(4).Text).Split("-")
            SelectedYear.Value = mySplit(1)
            GridView1.Visible = False

            GridView2.Caption = "Selected category : " & Trim(mySelectedRow.Cells(2).Text)

            Me.txtCategoryName.Text = Trim(mySelectedRow.Cells(2).Text)
            Me.txtCategoryName.Focus()

            Dim iIndex As Integer = 0
            Dim iItem As String = ""
            iIndex = DropDownList1.SelectedValue
            iItem = DropDownList1.SelectedItem.Text

            Me.HiddenFieldCurrentCategoryItem.Value = iIndex




            Me.ImageButtonViewProductionTable.Visible = False
            Me.ImageButtonSingleInsert.Visible = False
            Me.ImageButtonCreateSchedule.Visible = False

            Me.NewPanel.Visible = False
            Me.EditForm.Visible = False
            Me.SchedulePanel.Visible = False
            Me.Chart1.Visible = False
        End If
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        Me.txtRollOver.Text = ""

        NewPanel.Height = 300
        NewPanel.Width = 300
        Calendar1.Width = 200
        Calendar1.Visible = True
        NewDateClicked = True
    End Sub

    Protected Sub Calendar1_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Calendar1.SelectionChanged
        Calendar1.Visible = False
        NewPanel.Height = 100
        txtNewDate.Text = Me.Calendar1.SelectedDate.ToShortDateString()
        NewDateClicked = False
    End Sub

    Protected Sub ImageButton2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton2.Click
        'EditForm.Height = 300
        'EditForm.Width = 300
        'Calendar2.Width = 200
        Calendar2.Visible = True
        ModifiedClicked = True
    End Sub

    Protected Sub Calendar2_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Calendar2.SelectionChanged
        Calendar2.Visible = False
        '  EditForm.Height = 100
        TheDate.Text = Me.Calendar2.SelectedDate.ToShortDateString()
        ModifiedClicked = False
    End Sub

    'Protected Sub MultipleDates_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles MultipleDates.Click
    '    'Me.ScheduleStart.Text = ""
    '    'Me.ScheduleFinish.Text = ""
    '    'chkSaturday.Checked = False
    '    'chkSunday.Checked = False
    '    ''   Button5.Visible = False
    '    ''   MultipleDates.Visible = False
    '    'Me.EditForm.Visible = False
    '    'Me.NewPanel.Visible = False
    '    'Me.SchedulePanel.Visible = True
    'End Sub

    'Protected Sub ScheduleCreate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ScheduleCreate.Click
    '    Dim ProgressBarWidth As Integer = SchedulePanel.Width.Value
    '    Dim Incremental As Integer = 0

    '    Dim vSTART As String = DateAdd(DateInterval.Day, -1, CDate(ScheduleStart.Text)).ToString
    '    '   Dim vSTART As String = ScheduleStart.Text
    '    Dim vFINISH As String = ScheduleFinish.Text
    '    Dim i As Integer
    '    If Trim(vSTART) = "" Then
    '        MsgBox("Please enter a valid Start date")
    '        ScheduleStart.Focus()
    '        Exit Sub
    '    End If

    '    If Trim(vFINISH) = "" Then
    '        MsgBox("Please enter a valid Finish date")
    '        ScheduleFinish.Focus()
    '        Exit Sub
    '    End If

    '    If ScheduleFinish.Text < ScheduleStart.Text Then
    '        MsgBox("Start date cannot be greater than finish date")
    '        Exit Sub
    '    End If

    '    Dim iIndex As Integer = 0
    '    Dim iItem As String = ""
    '    iIndex = DropDownList1.SelectedValue
    '    iItem = DropDownList1.SelectedItem.Text

    '    Dim iIGNORE As Boolean = False
    '    Dim iOne As Date = vFINISH
    '    Dim iTwo As Date = vSTART
    '    Dim strSQL = ""
    '    Dim iWeekName As String = ""
    '    Dim Difference As TimeSpan = iTwo.Subtract(iOne)
    '    Dim IntervalDate As String = ""
    '    Dim IDone As String = ""
    '    Dim IDtwo As String = ""
    '    Dim NICone As String = ""
    '    Dim NICtwo As String = ""
    '    Dim strSQLdelete As String = ""


    '    IDone = DateAdd(DateInterval.Day, i, CDate(vSTART))
    '    NICone = Right("0000" & Year(IDone), 4) & Right("00" & Month(IDone), 2) & Right("00" & Day(IDone), 2)

    '    IDtwo = DateAdd(DateInterval.Day, i, CDate(vFINISH))
    '    NICtwo = Right("0000" & Year(IDtwo), 4) & Right("00" & Month(IDtwo), 2) & Right("00" & Day(IDtwo), 2)

    '    strSQLdelete = " DELETE FROM tblCustomer_Production_Values WHERE ProductionDate between "
    '    strSQLdelete = strSQLdelete & "(" & "'" & NICone & "'" & ") AND (" & "'" & NICtwo & "'" & ")"
    '    strSQLdelete = strSQLdelete & " AND CustUM=" & "'" & iIndex & "'" & " AND CustID=" & Me.CompanyID.Value
    '    SqlDataSource4.DeleteCommand = strSQLdelete
    '    SqlDataSource4.Delete()

    '    Incremental = (SchedulePanel.Width.Value / Math.Abs(Difference.Days))
    '    For i = 1 To Math.Abs(Difference.Days)
    '        iIGNORE = False
    '        strSQL = "  INSERT INTO tblCustomer_Production_Values (CustID,CustUM,CustValue,ProductionDate) "
    '        strSQL = strSQL & " Values("
    '        strSQL = strSQL & "'" & Me.CompanyID.Value & "'" & ","
    '        strSQL = strSQL & "'" & iIndex & "'" & ","
    '        strSQL = strSQL & "0" & ","

    '        IntervalDate = DateAdd(DateInterval.Day, i, CDate(vSTART))
    '        iWeekName = Trim(WeekdayName(Weekday(IntervalDate), False, Microsoft.VisualBasic.FirstDayOfWeek.Sunday))
    '        strSQL = strSQL & "'" & IntervalDate & "'"
    '        strSQL = strSQL & ")"

    '        Select Case iWeekName
    '            Case "Saturday"
    '                If Me.chkSaturday.Checked = True Then
    '                    iIGNORE = True
    '                    '  MsgBox("Saturday ignored")
    '                End If
    '            Case "Sunday"
    '                If Me.chkSunday.Checked = True Then
    '                    iIGNORE = True
    '                    ' MsgBox("Sunday ignored")
    '                End If
    '        End Select

    '        If iIGNORE = True Then
    '        Else
    '            '   MsgBox(iWeekName & vbNewLine & strSQL)
    '            SqlDataSource4.InsertCommand = strSQL
    '            SqlDataSource4.Insert()
    '        End If
    '    Next

    '    '=================================================================
    '    strSQL = " SELECT [intID],[CustID] ,[CustUM ],[CustValue] , [ProductionDate] ,DATENAME(DW,ProductionDate) as [DayName] FROM [tblCustomer_Production_Values] "
    '    strSQL = strSQL & " WHERE [CustID] = " & Me.CompanyID.Value
    '    strSQL = strSQL & " AND [CustUM] = " & iIndex
    '    ' strSQL = strSQL & " AND [CustUM] = " & Me.UnitOfMeasure.Value
    '    strSQL = strSQL & " Order By [ProductionDate] DESC"
    '    SqlDataSource2.SelectCommand = strSQL

    '    strSQL = ""
    '    strSQL = strSQL & "  SELECT tblCustomer_Production_Values.CustUM"
    '    strSQL = strSQL & "  , COUNT(*) , tblCustomer_Product_Type.ProductType ,"
    '    strSQL = strSQL & " dbo.fnSummary_ReturnedDates(tblCustomer_Production_Values.CustUM,'872') AS 'Range'"
    '    strSQL = strSQL & "   FROM dbo.tblCustomer_Production_Values "
    '    strSQL = strSQL & "  INNER JOIN tblCustomer_Product_Type ON tblCustomer_Product_Type.intID= tblCustomer_Production_Values.CustUM  "
    '    strSQL = strSQL & " AND tblCustomer_Product_Type.CustID=" & "'" & Me.CompanyID.Value & "'"
    '    strSQL = strSQL & " GROUP BY tblCustomer_Production_Values.CustUM , tblCustomer_Product_Type.ProductType "
    '    SqlDataSource3.SelectCommand = strSQL
    '    '==================================================================
    '    'Button5.Visible = True
    '    'MultipleDates.Visible = True
    '    Me.SchedulePanel.Visible = False
    'End Sub

    'Protected Sub iCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles iCancel.Click
    '    '    Button5.Visible = True
    '    '    MultipleDates.Visible = True
    '    Me.SchedulePanel.Visible = False
    'End Sub

    Protected Sub imgStart_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgStart.Click
        Me.HiddenField1.Value = "imgStart"
        SchedulePanel.Height = 325
        SchedulePanel.Width = 300
        Calendar3.Width = 200
        Calendar3.Visible = True
    End Sub

    Protected Sub imgFinish_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgFinish.Click
        Me.HiddenField1.Value = "imgFinish"
        SchedulePanel.Height = 325
        SchedulePanel.Width = 300
        Calendar3.Visible = True
    End Sub

    Protected Sub Calendar3_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Calendar3.SelectionChanged
        SchedulePanel.Height = 150
        Select Case Me.HiddenField1.Value
            Case "imgStart" : ScheduleStart.Text = Calendar3.SelectedDate.ToShortDateString()
            Case "imgFinish" : ScheduleFinish.Text = Calendar3.SelectedDate.ToShortDateString()
        End Select
        Calendar3.Visible = False
    End Sub

    Protected Function AlreadyOnFile(ByVal this) As String
        Dim iIndex As Integer = 0
        Dim iItem As String = ""
        Dim strSQL As String = ""
        iIndex = DropDownList1.SelectedValue
        iItem = DropDownList1.SelectedItem.Text

        strSQL = " SELECT * FROM dbo.tblCustomer_Production_Values WHERE ProductionDate="
        strSQL = strSQL & "'" & this & "'"
        strSQL = strSQL & " AND CustUM=" & "'" & iIndex & "'" & " AND CustID=" & Me.CompanyID.Value

        SqlDataSource4.SelectCommand = strSQL

        Dim theData As DataView = CType(SqlDataSource4.Select(DataSourceSelectArguments.Empty), DataView)
        If theData.Table.Rows.Count > 0 Then
            AlreadyOnFile = "True"
        Else
            AlreadyOnFile = "False"
        End If
        '   For Each aDataRow As DataRow In theData.Table.Rows
        '      MsgBox(aDataRow("ProductionDate"))
        '   Next
    End Function


    'Protected Sub butFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butFilter.Click
    '    Dim iIndex As Integer = 0
    '    Dim iItem As String = ""
    '    Dim strSQL As String = ""
    '    iIndex = DropDownList1.SelectedValue
    '    iItem = DropDownList1.SelectedItem.Text
    '    strSQL = " SELECT [intID],[CustID] ,[CustUM ],[CustValue] , [ProductionDate] ,DATENAME(DW,ProductionDate) as [DayName] FROM [tblCustomer_Production_Values] "
    '    strSQL = strSQL & " WHERE [CustID] = " & Me.CompanyID.Value
    '    strSQL = strSQL & " AND [CustUM] = " & iIndex
    '    strSQL = strSQL & " AND [ProductionDate] LIKE '%" & Trim(SummaryYears.Text) & "%'"
    '    strSQL = strSQL & " Order By [ProductionDate] DESC"
    '    SqlDataSource2.SelectCommand = strSQL

    '    Call ShowGraph()

    'End Sub

    Protected Sub DropDownList1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DropDownList1.SelectedIndexChanged
        Me.SummaryYears.Visible = False
        ' Me.butFilter.Visible = False
        GridView1.Visible = False
    End Sub

    Protected Sub DropDownList1_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DropDownList1.TextChanged
        Me.SummaryYears.Visible = False
        '   Me.butFilter.Visible = False
        GridView1.Visible = False
    End Sub


  


    'Protected Sub lnkbtnSelectCompany_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkbtnSelectCompany.Click
    '    Dim strSQL As String = ""
    '    Me.CompanyID.Value = Me.CompanySelect.SelectedValue
    '    strSQL = "  SELECT * FROM [tblCustomer_Product_Type] WHERE CustID = "
    '    strSQL = strSQL & Me.CompanyID.Value
    '    strSQL = strSQL & " ORDER BY [ProductType]"
    '    '   DropDownList1
    '    '   Me.SqlDataSource1.SelectCommand = "   SELECT * FROM [tblCustomer_Product_Type] WHERE CustID = " & john
    '    '  "0 ORDER BY [ProductType]"
    '    GridView2.Visible = False
    '    GridView1.Visible = False
    '    PanelTopItems.Visible = False
    '    Me.DropDownList1.Items.Clear()
    '    Me.SqlDataSource1.SelectCommand = strSQL
    '    Me.DropDownList1.DataBind()

    '    If Me.DropDownList1.Items.Count > 0 Then
    '        GridView2.Visible = True
    '        PanelTopItems.Visible = True

    '    Else
    '        GridView2.Visible = False
    '        PanelTopItems.Visible = False
    '    End If


    '    Me.InsertValue.Visible = False
    '    Me.InsertMultiple.Visible = False
    '    Me.YearView.Visible = False


    '    Me.EditForm.Visible = False
    '    Me.NewPanel.Visible = False
    '    Me.SchedulePanel.Visible = False

    'End Sub

    'Protected Sub LinkButtonRefresh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButtonRefresh.Click
    '    Dim strSQL As String = ""
    '    Dim YearstrSQL As String = ""
    '    Dim iIndex As Integer = 0
    '    Dim iItem As String = ""
    '    iIndex = DropDownList1.SelectedValue
    '    iItem = DropDownList1.SelectedItem.Text
    '    Me.UnitOfMeasure.Value = iIndex

    '    Me.SummaryYears.Visible = True
    '    '  Me.butFilter.Visible = True
    '    Me.GridView2.Visible = False

    '    YearstrSQL = ""
    '    YearstrSQL = YearstrSQL & " SELECT   year(productiondate) AS 'Date' from dbo.tblCustomer_Production_Values  "
    '    YearstrSQL = YearstrSQL & " WHERE custUM=" & iIndex
    '    YearstrSQL = YearstrSQL & "  AND CustID=" & Me.CompanyID.Value
    '    YearstrSQL = YearstrSQL & "  GROUP BY  year(productiondate) "
    '    YearstrSQL = YearstrSQL & "  ORDER BY  year(productiondate)  DESC"

    '    Me.SqlDataSource5.SelectCommand = YearstrSQL
    '    SummaryYears.SelectedIndex = SummaryYears.Items.IndexOf(SummaryYears.Items.FindByText(SelectedYear.Value.ToString))

    '    strSQL = ""
    '    strSQL = " SELECT [intID],[CustID] ,[CustUM ],[CustValue] , [ProductionDate] ,DATENAME(DW,ProductionDate) as [DayName] FROM [tblCustomer_Production_Values] "
    '    strSQL = strSQL & " WHERE [CustID] = " & Me.CompanyID.Value
    '    strSQL = strSQL & " AND [CustUM] = " & iIndex
    '    'strSQL = strSQL & " AND [ProductionDate] LIKE '%" & Trim(SummaryYears.Text) & "%'"
    '    strSQL = strSQL & " AND [ProductionDate] LIKE '%" & Me.SelectedYear.Value & "%'"
    '    strSQL = strSQL & " Order By [ProductionDate] DESC"
    '    SqlDataSource2.SelectCommand = strSQL

    '    GridView1.Caption = "Selected category ~ " & Trim(DropDownList1.SelectedItem.Text)

    '    Me.NewPanel.Visible = False
    '    Me.EditForm.Visible = False
    '    Me.SchedulePanel.Visible = False

    '    GridView1.DataBind()
    '    GridView1.Visible = True

    '    Me.GridView1.Visible = True

    '    Me.SummaryYears.Visible = True
    '    Me.InsertValue.Visible = True
    '    Me.InsertMultiple.Visible = True
    '    Me.YearView.Visible = True

    '    '   Button5.Visible = True
    '    '   MultipleDates.Visible = True

    'End Sub

    'Protected Sub InsertValue_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles InsertValue.Click

    '    SchedulePanel.Visible = False
    '    txtNewDate.Text = FormatDateTime(DateTime.Now.ToString, DateFormat.ShortDate)
    '    txtNewValue.Text = 0
    '    EditForm.Visible = False
    '    NewPanel.Visible = True
    'End Sub

    'Protected Sub InsertMultiple_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles InsertMultiple.Click
    '    Me.ScheduleStart.Text = ""
    '    Me.ScheduleFinish.Text = ""
    '    chkSaturday.Checked = False
    '    chkSunday.Checked = False

    '    Me.EditForm.Visible = False
    '    Me.NewPanel.Visible = False
    '    Me.SchedulePanel.Visible = True
    'End Sub

    'Protected Sub YearView_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles YearView.Click
    '    Dim iIndex As Integer = 0
    '    Dim iItem As String = ""
    '    Dim strSQL As String = ""
    '    iIndex = DropDownList1.SelectedValue
    '    iItem = DropDownList1.SelectedItem.Text
    '    strSQL = " SELECT [intID],[CustID] ,[CustUM ],[CustValue] , [ProductionDate] ,DATENAME(DW,ProductionDate) as [DayName] FROM [tblCustomer_Production_Values] "
    '    strSQL = strSQL & " WHERE [CustID] = " & Me.CompanyID.Value
    '    strSQL = strSQL & " AND [CustUM] = " & iIndex
    '    strSQL = strSQL & " AND [ProductionDate] LIKE '%" & Trim(SummaryYears.Text) & "%'"
    '    strSQL = strSQL & " Order By [ProductionDate] DESC"
    '    Me.SqlDataSource2.SelectCommand = strSQL

    '    Me.EditForm.Visible = False
    '    Me.NewPanel.Visible = False
    '    Me.SchedulePanel.Visible = False

    '    Me.GridView1.Visible = True
    '    Call ShowGraph()
    'End Sub

    Protected Sub UpdateValues_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles UpdateValues.Click
        ' billy
        Dim strSQL As String = ""
        Call UpdateGridView(Me.TheDate.Text, Me.TheValue.Text)
        strSQL = " SELECT [intID],[CustID] ,[CustUM ],[CustValue] , [ProductionDate] ,DATENAME(DW,ProductionDate) as [DayName] FROM [tblCustomer_Production_Values] "
        strSQL = strSQL & " WHERE [CustID] = " & Me.CompanyID.Value
        strSQL = strSQL & " AND [CustUM] = " & Me.HiddenFieldCurrentCategoryItem.Value 'Me.UnitOfMeasure.Value
        strSQL = strSQL & " AND [ProductionDate] LIKE '%" & Trim(SummaryYears.Text) & "%'"
        strSQL = strSQL & " Order By [ProductionDate] DESC"

        SqlDataSource2.SelectCommand = strSQL
        GridView1.DataBind()

        Me.PanelTopItems.Visible = True
        Me.EditForm.Style.Item("z-index") = 0
        EditForm.Visible = False


        'NewPanel.Visible = False
        ' refresh and rebind the grid
        ' Call ShowGraph()
        ' Chart1.Visible = True
    End Sub

    Protected Sub DeleteValues_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles DeleteValues.Click
        '
        Dim mySelectedRow As GridViewRow = GridView1.Rows(HiddeniIndex.Value)
        Dim uintId As String = GridView1.DataKeys(HiddeniIndex.Value).Value
        Dim ID As String = Val(mySelectedRow.Cells(1).Text)
        Dim strSQL As String = ""
        strSQL = "Delete from [tblCustomer_Production_Values] "
        strSQL = strSQL & " WHERE [intID] = " & uintId

        SqlDataSource2.UpdateCommand = strSQL
        SqlDataSource2.Update()
        GridView1.DataBind()

        strSQL = ""
        strSQL = "Select * from  [tblCustomer_Production_Values] "
        strSQL = strSQL & " WHERE [CustID] = " & Me.CompanyID.Value
        ' strSQL = strSQL & " AND [CustUM] = " & "'" & Me.UnitOfMeasure.Value & "'"
        strSQL = strSQL & " AND [CustUM] = " & "'" & Me.HiddenFieldCurrentCategoryItem.Value & "'"
        strSQL = strSQL & " Order By [ProductionDate] DESC"

        SqlDataSource2.SelectCommand = strSQL

        strSQL = ""
        'strSQL = strSQL & "  SELECT tblCustomer_Production_Values.CustUM"
        'strSQL = strSQL & "  , COUNT(*) , tblCustomer_Product_Type.ProductType ,"
        'strSQL = strSQL & " dbo.fnSummary_ReturnedDates(tblCustomer_Production_Values.CustUM,'872') AS 'Range' "
        'strSQL = strSQL & "   FROM dbo.tblCustomer_Production_Values "
        'strSQL = strSQL & "  INNER JOIN tblCustomer_Product_Type ON tblCustomer_Product_Type.intID= tblCustomer_Production_Values.CustUM  "
        'strSQL = strSQL & " AND tblCustomer_Product_Type.CustID=" & "'" & Me.CompanyID.Value & "'"
        'strSQL = strSQL & " GROUP BY tblCustomer_Production_Values.CustUM , tblCustomer_Product_Type.ProductType "

        strSQL = " spProductType_RangeCombined " & Me.CompanyID.Value
        SqlDataSource3.SelectCommand = strSQL

        Me.PanelTopItems.Visible = True
        Me.EditForm.Style.Item("z-index") = 0
        EditForm.Visible = False

    End Sub

    Protected Sub HideValues_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles HideValues.Click
        Me.PanelTopItems.Visible = True
        Me.EditForm.Style.Item("z-index") = 0
        EditForm.Visible = False

    End Sub

    '' '' ''Protected Sub UpdateNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles UpdateNew.Click
    '' '' ''    ' validate
    '' '' ''    Dim strSQL As String = ""
    '' '' ''    If IsDate(Me.txtNewDate.Text) = True Then
    '' '' ''    Else
    '' '' ''        MsgBox("Please enter a valid Productiondate", MsgBoxStyle.Critical, "Validation error")
    '' '' ''        txtNewDate.Focus()
    '' '' ''        Exit Sub
    '' '' ''    End If

    '' '' ''    If IsNumeric(Me.txtNewValue.Text) Then
    '' '' ''        If Val(Me.txtNewValue.Text < 0) Then
    '' '' ''            MsgBox("Please enter a valid Producton value Greater than 0", MsgBoxStyle.Critical, "Validation error")
    '' '' ''            txtNewValue.Focus()
    '' '' ''            Exit Sub
    '' '' ''        Else
    '' '' ''        End If
    '' '' ''    Else
    '' '' ''        MsgBox("Please enter a valid Producton value", MsgBoxStyle.Critical, "Validation error")
    '' '' ''        txtNewValue.Focus()
    '' '' ''        Exit Sub
    '' '' ''    End If

    '' '' ''    ' Check for previous productiondate already on database

    '' '' ''    If AlreadyOnFile(txtNewDate.Text) = "True" Then
    '' '' ''        MsgBox("This item is already on File")
    '' '' ''        Exit Sub
    '' '' ''    Else
    '' '' ''    End If

    '' '' ''    strSQL = "INSERT INTO  [tblCustomer_Production_Values] (CustID , CustUM ,CustValue, ProductionDate) "
    '' '' ''    strSQL = strSQL & " Values ("

    '' '' ''    strSQL = strSQL & Me.CompanyID.Value & ","
    '' '' ''    strSQL = strSQL & Me.UnitOfMeasure.Value & ","

    '' '' ''    strSQL = strSQL & "'" & txtNewValue.Text & "'" & ","
    '' '' ''    strSQL = strSQL & "'" & txtNewDate.Text & "'"
    '' '' ''    strSQL = strSQL & " )"

    '' '' ''    SqlDataSource2.UpdateCommand = strSQL
    '' '' ''    SqlDataSource2.Update()
    '' '' ''    GridView1.DataBind()

    '' '' ''    strSQL = " SELECT [intID],[CustID] ,[CustUM ],[CustValue] , [ProductionDate] ,DATENAME(DW,ProductionDate) as [DayName] FROM [tblCustomer_Production_Values] "
    '' '' ''    strSQL = strSQL & " WHERE [CustID] = " & Me.CompanyID.Value
    '' '' ''    strSQL = strSQL & " AND [CustUM] = " & Me.UnitOfMeasure.Value
    '' '' ''    strSQL = strSQL & " Order By [ProductionDate] DESC"

    '' '' ''    SqlDataSource2.SelectCommand = strSQL

    '' '' ''    strSQL = ""
    '' '' ''    strSQL = strSQL & "  SELECT tblCustomer_Production_Values.CustUM"
    '' '' ''    strSQL = strSQL & "  , COUNT(*) , tblCustomer_Product_Type.ProductType  ,"
    '' '' ''    strSQL = strSQL & " dbo.fnSummary_ReturnedDates(tblCustomer_Production_Values.CustUM,'872')  as 'Range' "
    '' '' ''    strSQL = strSQL & "   FROM dbo.tblCustomer_Production_Values "
    '' '' ''    strSQL = strSQL & "  INNER JOIN tblCustomer_Product_Type ON tblCustomer_Product_Type.intID= tblCustomer_Production_Values.CustUM  "
    '' '' ''    strSQL = strSQL & " AND tblCustomer_Product_Type.CustID=" & "'" & Me.CompanyID.Value & "'"
    '' '' ''    strSQL = strSQL & " GROUP BY tblCustomer_Production_Values.CustUM , tblCustomer_Product_Type.ProductType "

    '' '' ''    SqlDataSource3.SelectCommand = strSQL

    '' '' ''    ' Me.PanelTopHeader.Visible = True
    '' '' ''    Me.PanelTopItems.Visible = True
    '' '' ''    Me.NewPanel.Style.Item("z-index") = 0
    '' '' ''    NewPanel.Visible = False


    '' '' ''    'Call ShowGraph()
    '' '' ''    '  Button5.Visible = True
    '' '' ''    '  MultipleDates.Visible = True
    '' '' ''End Sub

    '' '' ''Protected Sub HideInsert_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles HideInsert.Click
    '' '' ''    'Me.PanelTopHeader.Visible = True
    '' '' ''    Me.PanelTopItems.Visible = True
    '' '' ''    Me.NewPanel.Style.Item("z-index") = 0
    '' '' ''    NewPanel.Visible = False
    '' '' ''End Sub

    'Protected Sub UpdateCreate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles UpdateCreate.Click
    '    Dim ProgressBarWidth As Integer = SchedulePanel.Width.Value
    '    Dim Incremental As Integer = 0

    '    Dim vSTART As String = DateAdd(DateInterval.Day, -1, CDate(ScheduleStart.Text)).ToString
    '    '   Dim vSTART As String = ScheduleStart.Text
    '    Dim vFINISH As String = ScheduleFinish.Text
    '    Dim i As Integer
    '    If Trim(vSTART) = "" Then
    '        MsgBox("Please enter a valid Start date")
    '        ScheduleStart.Focus()
    '        Exit Sub
    '    End If

    '    If Trim(vFINISH) = "" Then
    '        MsgBox("Please enter a valid Finish date")
    '        ScheduleFinish.Focus()
    '        Exit Sub
    '    End If

    '    If ScheduleFinish.Text < ScheduleStart.Text Then
    '        MsgBox("Start date cannot be greater than finish date")
    '        Exit Sub
    '    End If

    '    Dim iIndex As Integer = 0
    '    Dim iItem As String = ""
    '    iIndex = DropDownList1.SelectedValue
    '    iItem = DropDownList1.SelectedItem.Text

    '    Dim iIGNORE As Boolean = False
    '    Dim iOne As Date = vFINISH
    '    Dim iTwo As Date = vSTART
    '    Dim strSQL = ""
    '    Dim iWeekName As String = ""
    '    Dim Difference As TimeSpan = iTwo.Subtract(iOne)
    '    Dim IntervalDate As String = ""
    '    Dim IDone As String = ""
    '    Dim IDtwo As String = ""
    '    Dim NICone As String = ""
    '    Dim NICtwo As String = ""
    '    Dim strSQLdelete As String = ""


    '    IDone = DateAdd(DateInterval.Day, i, CDate(vSTART))
    '    NICone = Right("0000" & Year(IDone), 4) & Right("00" & Month(IDone), 2) & Right("00" & Day(IDone), 2)

    '    IDtwo = DateAdd(DateInterval.Day, i, CDate(vFINISH))
    '    NICtwo = Right("0000" & Year(IDtwo), 4) & Right("00" & Month(IDtwo), 2) & Right("00" & Day(IDtwo), 2)

    '    strSQLdelete = " DELETE FROM tblCustomer_Production_Values WHERE ProductionDate between "
    '    strSQLdelete = strSQLdelete & "(" & "'" & NICone & "'" & ") AND (" & "'" & NICtwo & "'" & ")"
    '    strSQLdelete = strSQLdelete & " AND CustUM=" & "'" & iIndex & "'" & " AND CustID=" & Me.CompanyID.Value
    '    SqlDataSource4.DeleteCommand = strSQLdelete
    '    SqlDataSource4.Delete()

    '    Incremental = (SchedulePanel.Width.Value / Math.Abs(Difference.Days))
    '    For i = 1 To Math.Abs(Difference.Days)
    '        iIGNORE = False
    '        strSQL = "  INSERT INTO tblCustomer_Production_Values (CustID,CustUM,CustValue,ProductionDate) "
    '        strSQL = strSQL & " Values("
    '        strSQL = strSQL & "'" & Me.CompanyID.Value & "'" & ","
    '        strSQL = strSQL & "'" & iIndex & "'" & ","
    '        strSQL = strSQL & "0" & ","

    '        IntervalDate = DateAdd(DateInterval.Day, i, CDate(vSTART))
    '        iWeekName = Trim(WeekdayName(Weekday(IntervalDate), False, Microsoft.VisualBasic.FirstDayOfWeek.Sunday))
    '        strSQL = strSQL & "'" & IntervalDate & "'"
    '        strSQL = strSQL & ")"

    '        Select Case iWeekName
    '            Case "Saturday"
    '                If Me.chkSaturday.Checked = True Then
    '                    iIGNORE = True
    '                    '  MsgBox("Saturday ignored")
    '                End If
    '            Case "Sunday"
    '                If Me.chkSunday.Checked = True Then
    '                    iIGNORE = True
    '                    ' MsgBox("Sunday ignored")
    '                End If
    '        End Select

    '        If iIGNORE = True Then
    '        Else
    '            '   MsgBox(iWeekName & vbNewLine & strSQL)
    '            SqlDataSource4.InsertCommand = strSQL
    '            SqlDataSource4.Insert()
    '        End If
    '    Next

    '    '=================================================================
    '    strSQL = " SELECT [intID],[CustID] ,[CustUM ],[CustValue] , [ProductionDate] ,DATENAME(DW,ProductionDate) as [DayName] FROM [tblCustomer_Production_Values] "
    '    strSQL = strSQL & " WHERE [CustID] = " & Me.CompanyID.Value
    '    strSQL = strSQL & " AND [CustUM] = " & iIndex
    '    ' strSQL = strSQL & " AND [CustUM] = " & Me.UnitOfMeasure.Value
    '    strSQL = strSQL & " Order By [ProductionDate] DESC"
    '    SqlDataSource2.SelectCommand = strSQL

    '    strSQL = ""
    '    strSQL = strSQL & "  SELECT tblCustomer_Production_Values.CustUM"
    '    strSQL = strSQL & "  , COUNT(*) , tblCustomer_Product_Type.ProductType ,"
    '    strSQL = strSQL & " dbo.fnSummary_ReturnedDates(tblCustomer_Production_Values.CustUM,'872') AS 'Range'"
    '    strSQL = strSQL & "   FROM dbo.tblCustomer_Production_Values "
    '    strSQL = strSQL & "  INNER JOIN tblCustomer_Product_Type ON tblCustomer_Product_Type.intID= tblCustomer_Production_Values.CustUM  "
    '    strSQL = strSQL & " AND tblCustomer_Product_Type.CustID=" & "'" & Me.CompanyID.Value & "'"
    '    strSQL = strSQL & " GROUP BY tblCustomer_Production_Values.CustUM , tblCustomer_Product_Type.ProductType "
    '    SqlDataSource3.SelectCommand = strSQL
    '    '==================================================================
    '    'Button5.Visible = True
    '    'MultipleDates.Visible = True
    '    Me.SchedulePanel.Visible = False
    'End Sub

    'Protected Sub UpdateCreateHide_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles UpdateCreateHide.Click
    '    Me.SchedulePanel.Visible = False
    '    Chart1.Visible = True
    'End Sub

    Protected Sub ImageButtonCloseChart_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonCloseChart.Click
        Me.txtRollOver.Text = ""
        Me.PanelChart.Visible = False
        Me.SummaryYears.Visible = True
        Chart1.Visible = False
    End Sub

    Protected Sub ImageButtonViewChart_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonViewChart.Click
        Me.PanelAdministration.Visible = False
        Me.PanelAdministrationTwo.Visible = False
        Me.SchedulePanel.Visible = False
        Me.PanelEachLine.Visible = False
        Me.NewPanel.Visible = False
        Me.txtRollOver.Text = ""
        Me.SummaryYears.Visible = False
        Me.PanelChart.Visible = True

        Call ShowGraph()

        Chart1.Visible = True
    End Sub

    '' ''Protected Sub LoadCompanyName_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles LoadCompanyName.Click
    '' ''    Dim strSQL As String = ""
    '' ''    Me.CompanyID.Value = Me.CompanySelect.SelectedValue


    '' ''    strSQL = "  SELECT * FROM [tblCustomer_Product_Type] WHERE CustID = "
    '' ''    strSQL = strSQL & Me.CompanyID.Value
    '' ''    strSQL = strSQL & " ORDER BY [ProductType]"

    '' ''    GridView2.Visible = False
    '' ''    GridView1.Visible = False
    '' ''    PanelTopItems.Visible = False
    '' ''    Me.DropDownList1.Items.Clear()
    '' ''    Me.SqlDataSource1.SelectCommand = strSQL
    '' ''    Me.DropDownList1.DataBind()

    '' ''    If Me.DropDownList1.Items.Count > 0 Then
    '' ''        GridView2.Visible = True
    '' ''        PanelTopItems.Visible = True
    '' ''    Else
    '' ''        GridView2.Visible = False
    '' ''        PanelTopItems.Visible = False
    '' ''    End If

    '' ''    '  EXEC(spProductType_WithOut_Range) '872'

    '' ''    ' EXEC(spProductType_With_Range) '872'


    '' ''    strSQL = ""
    '' ''    strSQL = strSQL & "   SELECT        "
    '' ''    strSQL = strSQL & "      tblCustomer_Production_Values.CustUM , COUNT(*) ,       "
    '' ''    strSQL = strSQL & "     tblCustomer_Product_Type.ProductType  ,     "
    '' ''    strSQL = strSQL & "    dbo.fnSummary_ReturnedDates (tblCustomer_Production_Values.CustUM,'" & Me.CompanySelect.SelectedValue & "')  as 'Range' "
    '' ''    strSQL = strSQL & "      FROM dbo.tblCustomer_Production_Values   "
    '' ''    strSQL = strSQL & "        INNER JOIN tblCustomer_Product_Type ON tblCustomer_Product_Type.intID=tblCustomer_Production_Values.CustUM   "
    '' ''    strSQL = strSQL & "      AND tblCustomer_Product_Type.CustID=" & "'" & Me.CompanySelect.SelectedValue & "'"
    '' ''    strSQL = strSQL & "    GROUP BY tblCustomer_Production_Values.CustUM,tblCustomer_Product_Type.ProductType     "

    '' ''    Me.SqlDataSource3.SelectCommand = " EXEC spProductType_With_Range " & "'" & Me.CompanySelect.SelectedValue & "'"
    '' ''    GridView2.DataBind()


    '' ''    'Me.InsertValue.Visible = False
    '' ''    'Me.InsertMultiple.Visible = False
    '' ''    'Me.YearView.Visible = False

    '' ''    Me.ImageButtonViewProductionTable.Visible = False
    '' ''    Me.ImageButtonSingleInsert.Visible = False
    '' ''    Me.ImageButtonCreateSchedule.Visible = False

    '' ''    Me.EditForm.Visible = False
    '' ''    Me.NewPanel.Visible = False
    '' ''    Me.SchedulePanel.Visible = False


    '' ''    ' Position
    '' ''    Me.PanelTopHeader.Visible = False

    '' ''End Sub

    Protected Sub ImageButtonViewProduction_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonViewProduction.Click
        If Me.HiddenFieldSelectedCategoryTotal.Value = 0 Then

        Else

        End If
        '     repopulate yeasrsummary
        '     MsgBox("view production")

        Me.PanelAdministration.Visible = False
        Me.PanelAdministrationTwo.Visible = False
        Me.txtRollOver.Text = ""
        Dim strSQL As String = ""
        Dim YearstrSQL As String = ""
        Dim iIndex As Integer = 0
        Dim iItem As String = ""
        iIndex = DropDownList1.SelectedValue
        iItem = DropDownList1.SelectedItem.Text
        Me.UnitOfMeasure.Value = iIndex

        '  MsgBox(DropDownList1.SelectedValue & vbNewLine & DropDownList1.SelectedItem.Text)

        Me.SummaryYears.Visible = True
        '  Me.butFilter.Visible = True
        Me.GridView2.Visible = False

        YearstrSQL = ""
        YearstrSQL = YearstrSQL & " SELECT   year(productiondate) AS 'Date' from dbo.tblCustomer_Production_Values  "
        YearstrSQL = YearstrSQL & " WHERE custUM=" & Me.HiddenFieldCurrentCategoryItem.Value
        YearstrSQL = YearstrSQL & "  AND CustID=" & Me.CompanyID.Value
        YearstrSQL = YearstrSQL & "  GROUP BY  year(productiondate) "
        YearstrSQL = YearstrSQL & "  ORDER BY  year(productiondate)  DESC"

        Me.SqlDataSource5.SelectCommand = YearstrSQL

        SummaryYears.SelectedIndex = SummaryYears.Items.IndexOf(SummaryYears.Items.FindByText(SelectedYear.Value.ToString))

        strSQL = ""
        strSQL = " SELECT [intID],[CustID] ,[CustUM ],[CustValue] , [ProductionDate] ,DATENAME(DW,ProductionDate) as [DayName] FROM [tblCustomer_Production_Values] "
        strSQL = strSQL & " WHERE [CustID] = " & Me.CompanyID.Value
        strSQL = strSQL & " AND [CustUM] = " & Me.HiddenFieldCurrentCategoryItem.Value 'iIndex
        'strSQL = strSQL & " AND [ProductionDate] LIKE '%" & Trim(SummaryYears.Text) & "%'"
        strSQL = strSQL & " AND [ProductionDate] LIKE '%" & Me.SelectedYear.Value & "%'"
        strSQL = strSQL & " Order By [ProductionDate] DESC"
        SqlDataSource2.SelectCommand = strSQL

        '    GridView1.Caption = "Selected category ~ " & Trim(DropDownList1.SelectedItem.Text)
        GridView1.Caption = "Selected category ~ " & Trim(Me.HiddenFieldSelectedCategoryText.Value)

        Me.NewPanel.Visible = False
        Me.EditForm.Visible = False
        Me.SchedulePanel.Visible = False

        GridView1.DataBind()
        GridView1.Visible = True

        Me.GridView1.Visible = True

        Me.SummaryYears.Visible = True

        Me.ImageButtonViewProductionTable.Visible = True
        Me.ImageButtonSingleInsert.Visible = True
        Me.ImageButtonCreateSchedule.Visible = True
        Me.ImageButtonCustomerChange.Visible = True
        Me.ImageButtonViewChart.Visible = True
        Me.ImageButtonViewProduction.Visible = False
        Me.ImageButtonCategories.Visible = False
        Me.SchedulePanel.Visible = False

    End Sub

    Protected Sub ImageButtonViewProductionTable_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonViewProductionTable.Click
        Me.PanelAdministration.Visible = False
        Me.PanelAdministrationTwo.Visible = False
        Me.SchedulePanel.Visible = False
        Me.PanelEachLine.Visible = False
        Me.txtRollOver.Text = ""
        Dim iIndex As Integer = 0
        Dim iItem As String = ""
        Dim strSQL As String = ""
        iIndex = DropDownList1.SelectedValue
        iItem = DropDownList1.SelectedItem.Text
        strSQL = " SELECT [intID],[CustID] ,[CustUM ],[CustValue] , [ProductionDate] ,DATENAME(DW,ProductionDate) as [DayName] FROM [tblCustomer_Production_Values] "
        strSQL = strSQL & " WHERE [CustID] = " & Me.CompanyID.Value
        strSQL = strSQL & " AND [CustUM] = " & Me.HiddenFieldCurrentCategoryItem.Value
        strSQL = strSQL & " AND [ProductionDate] LIKE '%" & Trim(SummaryYears.Text) & "%'"
        strSQL = strSQL & " Order By [ProductionDate] DESC"
        Me.SqlDataSource2.SelectCommand = strSQL

        Me.EditForm.Visible = False
        Me.NewPanel.Visible = False
        Me.SchedulePanel.Visible = False

        Me.GridView1.Visible = True
        Call ShowGraph()
    End Sub

    Protected Sub ImageButtonSingleInsert_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonSingleInsert.Click
        Me.PanelAdministration.Visible = False
        Me.PanelAdministrationTwo.Visible = False
        Me.SchedulePanel.Visible = False
        Me.PanelEachLine.Visible = False
        ' john smith
        Me.txtRollOver.Text = ""
        txtNewDate.Text = FormatDateTime(DateTime.Now.ToString, DateFormat.ShortDate)
        txtNewValue.Text = 0
        ' Me.PanelTopHeader.Visible = False
        ' Me.PanelTopItems.Visible = False
        Me.NewPanel.Style.Item("z-index") = 2000
        NewPanel.Visible = True





    End Sub

    Protected Sub ImageButtonCreateSchedule_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonCreateSchedule.Click
        Me.PanelAdministration.Visible = False
        Me.PanelAdministrationTwo.Visible = False
        Me.PanelEachLine.Visible = False
        Me.txtRollOver.Text = ""
        Me.ScheduleStart.Text = ""
        Me.ScheduleFinish.Text = ""
        chkSaturday.Checked = False
        chkSunday.Checked = False
        chkMonday.Checked = False
        chkTuesday.Checked = False
        chkWednesday.Checked = False
        chkThursday.Checked = False
        chkFriday.Checked = False



        Me.EditForm.Visible = False
        Me.NewPanel.Visible = False
        Me.SchedulePanel.Visible = True
    End Sub

    Protected Sub ImageButtonCustomerChange_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonCustomerChange.Click
        Me.NewPanel.Visible = False
        Me.PanelAdministration.Visible = False
        Me.PanelAdministrationTwo.Visible = False
        Me.txtRollOver.Text = ""
        Me.SummaryYears.Visible = False
        Me.ImageButtonViewProductionTable.Visible = False
        Me.ImageButtonSingleInsert.Visible = False
        Me.ImageButtonCreateSchedule.Visible = False
        Me.ImageButtonViewChart.Visible = False

        Me.ImageButtonViewProduction.Visible = True
        Me.ImageButtonCategories.Visible = True

        'Me.ImageButtonEditItem.Visible = False
        Me.PanelEachLine.Visible = False
        Me.SchedulePanel.Visible = False

        GridView1.Visible = False
        GridView2.Visible = True

        Me.ImageButtonCustomerChange.Visible = False
        'Me.PanelTopHeader.Visible = False
    End Sub

  
    'Protected Sub ImageButtonEditItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonEditItem.Click
    '    Me.txtRollOver.Text = ""
    '    Me.PanelTopHeader.Visible = False
    '    Me.PanelTopItems.Visible = False
    '    Me.EditForm.Style.Item("z-index") = 2000
    '    EditForm.Visible = True
    'End Sub

    Protected Sub ImageButtonUpdateAltered_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonUpdateAltered.Click
        Dim strSQL As String = ""
        Dim TheYear As String = Right("0000" & Year(CDate(TheDate.Text)), 4)
        Dim TheMonth As String = Right("00" & Month(CDate(TheDate.Text)), 2)
        Dim TheDay As String = Right("00" & Day(CDate(TheDate.Text)), 2)
        Dim TheStringDate As String = Trim(TheDate.Text)
        Dim mySelectedRow As GridViewRow = GridView1.Rows(HiddeniIndex.Value)
        Dim uintId As String = GridView1.DataKeys(HiddeniIndex.Value).Value
        Dim ID As String = Val(mySelectedRow.Cells(1).Text)

        mySelectedRow.Cells(5).Text = Trim(TheValue.Text)
        mySelectedRow.Cells(4).Text = Trim(TheDate.Text)

        strSQL = "UPDATE [tblCustomer_Production_Values] "
        strSQL = strSQL & " SET [CustValue] = " & Trim(Alteredvalue.Text)
        strSQL = strSQL & ", [ProductionDate] = " & "'" & Trim(CurrentDate.Text) & "'"
        strSQL = strSQL & " WHERE [intID] = " & uintId

        SqlDataSource2.UpdateCommand = strSQL
        SqlDataSource2.Update()

        strSQL = " SELECT [intID],[CustID] ,[CustUM ],[CustValue] , [ProductionDate] ,DATENAME(DW,ProductionDate) as [DayName] FROM [tblCustomer_Production_Values] "
        strSQL = strSQL & " WHERE [CustID] = " & Me.CompanyID.Value
        strSQL = strSQL & " AND [CustUM] = " & Me.HiddenFieldCurrentCategoryItem.Value ' Me.UnitOfMeasure.Value
        strSQL = strSQL & " AND [ProductionDate] LIKE '%" & Trim(SummaryYears.Text) & "%'"
        strSQL = strSQL & " Order By [ProductionDate] DESC"

        SqlDataSource2.SelectCommand = strSQL
        GridView1.DataBind()

    End Sub

    Protected Sub ImageButtonHideProductionSchedule_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonHideProductionSchedule.Click
        Me.SchedulePanel.Visible = False

    End Sub

    Protected Sub ImageButtonCreateProductionSchedule_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonCreateProductionSchedule.Click
        Me.PanelAdministration.Visible = False
        Me.PanelAdministrationTwo.Visible = False
        Dim Incremental As Integer = 0
        Dim vSTART As String = DateAdd(DateInterval.Day, -1, CDate(ScheduleStart.Text)).ToString
        Dim vFINISH As String = ScheduleFinish.Text
        Dim i As Integer
        If Trim(vSTART) = "" Then
            MsgBox("Please enter a valid Start date")
            ScheduleStart.Focus()
            Exit Sub
        End If

        If Trim(vFINISH) = "" Then
            MsgBox("Please enter a valid Finish date")
            ScheduleFinish.Focus()
            Exit Sub
        End If

        If ScheduleFinish.Text < ScheduleStart.Text Then
            MsgBox("Start date cannot be greater than finish date")
            Exit Sub
        End If

        Dim iIndex As Integer = 0
        Dim iItem As String = ""
        iIndex = DropDownList1.SelectedValue
        iItem = DropDownList1.SelectedItem.Text

        Dim iIGNORE As Boolean = False
        Dim iOne As Date = vFINISH
        Dim iTwo As Date = vSTART
        Dim strSQL = ""
        Dim iWeekName As String = ""
        Dim Difference As TimeSpan = iTwo.Subtract(iOne)
        Dim IntervalDate As String = ""
        Dim IDone As String = ""
        Dim IDtwo As String = ""
        Dim NICone As String = ""
        Dim NICtwo As String = ""
        Dim strSQLdelete As String = ""

        MsgBox("This process may take a few minutes - please be paient - Thank You" & vbNewLine & "Press OK to continue" & MsgBoxStyle.OkOnly, "Process warning")

        IDone = DateAdd(DateInterval.Day, i, CDate(vSTART))
        NICone = Right("0000" & Year(IDone), 4) & Right("00" & Month(IDone), 2) & Right("00" & Day(IDone), 2)

        IDtwo = DateAdd(DateInterval.Day, i, CDate(vFINISH))
        NICtwo = Right("0000" & Year(IDtwo), 4) & Right("00" & Month(IDtwo), 2) & Right("00" & Day(IDtwo), 2)

        strSQLdelete = " DELETE FROM tblCustomer_Production_Values WHERE ProductionDate between "
        strSQLdelete = strSQLdelete & "(" & "'" & NICone & "'" & ") AND (" & "'" & NICtwo & "'" & ")"
        strSQLdelete = strSQLdelete & " AND CustUM=" & "'" & iIndex & "'" & " AND CustID=" & Me.CompanyID.Value
        SqlDataSource4.DeleteCommand = strSQLdelete
        SqlDataSource4.Delete()

        '  Incremental = (SchedulePanel.Width.Value / Math.Abs(Difference.Days))
        For i = 1 To Math.Abs(Difference.Days)
            iIGNORE = False
            strSQL = "  INSERT INTO tblCustomer_Production_Values (CustID,CustUM,CustValue,ProductionDate) "
            strSQL = strSQL & " Values("
            strSQL = strSQL & "'" & Me.CompanyID.Value & "'" & ","
            strSQL = strSQL & "'" & iIndex & "'" & ","
            strSQL = strSQL & "0" & ","

            IntervalDate = DateAdd(DateInterval.Day, i, CDate(vSTART))
            iWeekName = Trim(WeekdayName(Weekday(IntervalDate), False, Microsoft.VisualBasic.FirstDayOfWeek.Sunday))
            strSQL = strSQL & "'" & IntervalDate & "'"
            strSQL = strSQL & ")"

            Select Case iWeekName
                Case "Saturday"
                    If Me.chkSaturday.Checked = True Then
                        iIGNORE = True
                    End If
                Case "Sunday"
                    If Me.chkSunday.Checked = True Then
                        iIGNORE = True
                    End If
                Case "Monday"
                    If Me.chkMonday.Checked = True Then
                        iIGNORE = True
                    End If
                Case "Tuesday"
                    If Me.chkTuesday.Checked = True Then
                        iIGNORE = True
                    End If
                Case "Wednesday"
                    If Me.chkWednesday.Checked = True Then
                        iIGNORE = True
                    End If
                Case "Thursday"
                    If Me.chkThursday.Checked = True Then
                        iIGNORE = True
                    End If
                Case "Friday"
                    If Me.chkFriday.Checked = True Then
                        iIGNORE = True
                    End If
            End Select

            If iIGNORE = True Then
            Else
                SqlDataSource4.InsertCommand = strSQL
                SqlDataSource4.Insert()
            End If
        Next

        '=================================================================
        strSQL = " SELECT [intID],[CustID] ,[CustUM ],[CustValue] , [ProductionDate] ,DATENAME(DW,ProductionDate) as [DayName] FROM [tblCustomer_Production_Values] "
        strSQL = strSQL & " WHERE [CustID] = " & Me.CompanyID.Value
        strSQL = strSQL & " AND [CustUM] = " & Me.HiddenFieldCurrentCategoryItem.Value ' iIndex
        strSQL = strSQL & " Order By [ProductionDate] DESC"
        SqlDataSource2.SelectCommand = strSQL

        strSQL = ""
        'strSQL = strSQL & "  SELECT tblCustomer_Production_Values.CustUM"
        'strSQL = strSQL & "  , COUNT(*) , tblCustomer_Product_Type.ProductType ,"
        'strSQL = strSQL & " dbo.fnSummary_ReturnedDates(tblCustomer_Production_Values.CustUM,'872') AS 'Range'"
        'strSQL = strSQL & "   FROM dbo.tblCustomer_Production_Values "
        'strSQL = strSQL & "  INNER JOIN tblCustomer_Product_Type ON tblCustomer_Product_Type.intID= tblCustomer_Production_Values.CustUM  "
        'strSQL = strSQL & " AND tblCustomer_Product_Type.CustID=" & "'" & Me.CompanyID.Value & "'"
        'strSQL = strSQL & " GROUP BY tblCustomer_Production_Values.CustUM , tblCustomer_Product_Type.ProductType "
        strSQL = " spProductType_RangeCombined " & Me.CompanyID.Value
        SqlDataSource3.SelectCommand = strSQL
        Me.SchedulePanel.Visible = False
    End Sub

    Protected Sub Chart1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Chart1.Click
        ' HitTestResult(HitTestResult = this.DemoChart.HitTest(e.X, e.Y))
        ' DateTime.FromOADate(hitTestResult.ChartArea.AxisX.PixelPositionToValue((double)e.X)).ToString()
        Dim hitTestResult
        hitTestResult = Chart1.HitTest(e.X, e.Y)
        ' Chart1.ChartAreas("Default").AxisX.PixelPositionToValue(e.X)



    End Sub



    Protected Function ShowGraph() As String
        On Error Resume Next
        Dim myConnectionString As String = "Provider=SQLNCLI.1;Integrated Security=SSPI;Persist Security Info=False;Initial Catalog=portal.utilitymasters.co.uk;Data Source=uk-ed0-sqlcl-01.MCEG.local"

        '  Dim myConnectionString As String = WebConfigurationManager.AppSettings.Item("DBConnect2").ToString

        'Public Shared Function ConnectionString2() As String
        '    Dim appSettings As NameValueCollection = WebConfigurationManager.AppSettings
        '    Return appSettings.Item("DBConnect2").ToString
        'End Function

        Dim iIndex As Integer = 0
        Dim iItem As String = ""
        Dim strSQL As String = ""
        iIndex = DropDownList1.SelectedValue
        iItem = DropDownList1.SelectedItem.Text

        Dim ItemToShow As String = ""
        ItemToShow = Trim(lstViewItems.Text)

        ' strSQL = " SELECT TOP " & ItemToShow & " ProductionDate as 'One' , CustValue as 'Two' FROM [tblCustomer_Production_Values] "

        strSQL = " SELECT  ProductionDate as 'One' , CustValue as 'Two' FROM [tblCustomer_Production_Values] "

        strSQL = strSQL & " WHERE [CustID] = " & Me.CompanyID.Value
        strSQL = strSQL & " AND [CustUM] = " & Me.HiddenFieldCurrentCategoryItem.Value ' iIndex
        strSQL = strSQL & " AND [ProductionDate] LIKE '%" & Trim(SummaryYears.Text) & "%'"
        strSQL = strSQL & " Order By [ProductionDate] DESC"


        'MsgBox(strSQL)


        Dim mySelectQuery As String = strSQL
        Dim myConnection As New OleDbConnection(myConnectionString)
        Dim myCommand As New OleDbCommand(mySelectQuery, myConnection)
        myCommand.Connection.Open()
        Dim myReader As OleDbDataReader = myCommand.ExecuteReader(CommandBehavior.CloseConnection)


        Chart1.Width = 590
        Chart1.Height = 344

        'Chart1.Titles.Clear()
        'Chart1.Titles.Add("TopTitle")
        Chart1.Titles("TopTitle").Text = "Production figures for period " & Me.SummaryYears.SelectedItem.Text

        Chart1.Series.Clear()
        Chart1.Series.Add("Default")

        Chart1.ChartAreas.Clear()
        Chart1.ChartAreas.Add("Default")
        Chart1.ChartAreas("Default").AxisX.MajorGrid.Enabled = False
        Chart1.ChartAreas("Default").AxisX.MinorGrid.Enabled = False

        Chart1.ChartAreas("Default").AxisY.MajorGrid.Enabled = False
        Chart1.ChartAreas("Default").AxisY.MinorGrid.Enabled = False

        Chart1.ChartAreas("Default").AxisY.Title = "Production figures"
        Chart1.ChartAreas("Default").AxisY.TitleColor = Color.Blue

        Chart1.ChartAreas("Default").AxisX.Title = "Period dates"
        Chart1.ChartAreas("Default").AxisX.TitleColor = Color.Blue

        Me.Chart1.ChartAreas("Default").CursorX.UserEnabled = True

        Me.Chart1.ChartAreas("Default").AxisX.View.MinSize = 15
        If Not Me.IsPostBack Then
            Me.Chart1.ChartAreas("Default").AxisX.View.Position = 20.0
            Me.Chart1.ChartAreas("Default").AxisX.View.Size = 35.0
        End If

        Chart1.ChartAreas("Default").AxisX.ScrollBar.Enabled = True

        Chart1.Legends.Clear()
        '  Chart1.DataBindTable(myReader, "RecordedDate")

        Chart1.Series("Default").XValueIndexed = False

        Chart1.Series("Default").Color = Color.Black
        Chart1.Series("Default").ToolTip = "#VALY" + ControlChars.Lf + "#VALX{dddd}"

        Chart1.Series("Default").Type = SeriesChartType.Line
        '  Chart1.Series("Default").Type = SeriesChartType.Column

        '   Chart1.ChartAreas("Default").AxisY.Interval = 50
        '  Chart1.ChartAreas("Default").Position.Height = 20

        Chart1.ChartAreas("Default").BackColor = Color.Transparent
        Chart1.ChartAreas("Default").AxisX.IntervalType = DateTimeIntervalType.Days
        Chart1.ChartAreas("Default").AxisX.Interval = 1
        Chart1.ChartAreas("Default").AxisX.LabelStyle.Format = "d-MMM"
        Chart1.ChartAreas("Default").AxisX.LabelStyle.FontAngle = 45
        Chart1.ChartAreas("Default").AxisY.LabelStyle.Font = New Font("Arial", 7)
        Chart1.ChartAreas("Default").AxisX.LabelStyle.Font = New Font("Arial", 7)

        Chart1.Series("Default").MarkerStyle = MarkerStyle.Circle
        Chart1.Series("Default").MarkerSize = 5
        Chart1.Series("Default").MarkerColor = Color.Red
        Chart1.Series("Default").BorderWidth = 1
        Chart1.Series("Default").BorderColor = Color.DarkGreen
        Chart1.Series("Default").Points.DataBindXY(myReader, "One", myReader, "Two")

        myReader.Close()
        myConnection.Close()

        Chart1.Visible = True

        ShowGraph = "ok"

    End Function

    Protected Function ViewMonths(ByVal month As Integer) As String
        Dim myConnectionString As String = "Provider=SQLNCLI.1;Integrated Security=SSPI;Persist Security Info=False;Initial Catalog=portal.utilitymasters.co.uk;Data Source=uk-ed0-sqlcl-01.MCEG.local"
        Dim iIndex As Integer = 0
        Dim iItem As String = ""
        Dim strSQL As String = ""
        iIndex = DropDownList1.SelectedValue
        iItem = DropDownList1.SelectedItem.Text

        strSQL = " SELECT  ProductionDate as 'One' , CustValue as 'Two' FROM [tblCustomer_Production_Values] "
        strSQL = strSQL & " WHERE [CustID] = " & Me.CompanyID.Value
        strSQL = strSQL & " AND [CustUM] = " & Me.HiddenFieldCurrentCategoryItem.Value 'iIndex
        strSQL = strSQL & " AND [ProductionDate] LIKE '%" & Trim(SummaryYears.Text) & "%'"
        strSQL = strSQL & " AND month([ProductionDate]) =" & month
        strSQL = strSQL & " Order By [ProductionDate] DESC"

        Dim mySelectQuery As String = strSQL
        Dim myConnection As New OleDbConnection(myConnectionString)
        Dim myCommand As New OleDbCommand(mySelectQuery, myConnection)
        myCommand.Connection.Open()
        Dim myReader As OleDbDataReader = myCommand.ExecuteReader(CommandBehavior.CloseConnection)


        Chart1.Width = 590
        Chart1.Height = 344

        'Chart1.Titles.Clear()
        'Chart1.Titles.Add("TopTitle")
        Chart1.Titles("TopTitle").Text = "Production figures for period " & Me.SummaryYears.SelectedItem.Text

        Chart1.Series.Clear()
        Chart1.Series.Add("Default")

        Chart1.ChartAreas.Clear()
        Chart1.ChartAreas.Add("Default")
        Chart1.ChartAreas("Default").AxisX.MajorGrid.Enabled = False
        Chart1.ChartAreas("Default").AxisX.MinorGrid.Enabled = False

        Chart1.ChartAreas("Default").AxisY.MajorGrid.Enabled = False
        Chart1.ChartAreas("Default").AxisY.MinorGrid.Enabled = False

        Chart1.ChartAreas("Default").AxisY.Title = "Production figures"
        Chart1.ChartAreas("Default").AxisY.TitleColor = Color.Blue

        Chart1.ChartAreas("Default").AxisX.Title = "Period dates"
        Chart1.ChartAreas("Default").AxisX.TitleColor = Color.Blue

        Me.Chart1.ChartAreas("Default").CursorX.UserEnabled = True

        Me.Chart1.ChartAreas("Default").AxisX.View.MinSize = 15
        If Not Me.IsPostBack Then
            Me.Chart1.ChartAreas("Default").AxisX.View.Position = 20.0
            Me.Chart1.ChartAreas("Default").AxisX.View.Size = 35.0
        End If

        Chart1.ChartAreas("Default").AxisX.ScrollBar.Enabled = True

        Chart1.Legends.Clear()
        '  Chart1.DataBindTable(myReader, "RecordedDate")

        Chart1.Series("Default").XValueIndexed = False

        Chart1.Series("Default").Color = Color.Black
        Chart1.Series("Default").ToolTip = "#VALY" + ControlChars.Lf + "#VALX{dddd}"

        Chart1.Series("Default").Type = SeriesChartType.Line
        '  Chart1.Series("Default").Type = SeriesChartType.Column

        '   Chart1.ChartAreas("Default").AxisY.Interval = 50
        '  Chart1.ChartAreas("Default").Position.Height = 20

        Chart1.ChartAreas("Default").BackColor = Color.Transparent
        Chart1.ChartAreas("Default").AxisX.IntervalType = DateTimeIntervalType.Days
        Chart1.ChartAreas("Default").AxisX.Interval = 1
        Chart1.ChartAreas("Default").AxisX.LabelStyle.Format = "d-MMM"
        Chart1.ChartAreas("Default").AxisX.LabelStyle.FontAngle = 45
        Chart1.ChartAreas("Default").AxisY.LabelStyle.Font = New Font("Arial", 7)
        Chart1.ChartAreas("Default").AxisX.LabelStyle.Font = New Font("Arial", 7)

        Chart1.Series("Default").MarkerStyle = MarkerStyle.Circle
        Chart1.Series("Default").MarkerSize = 5
        Chart1.Series("Default").MarkerColor = Color.Red
        Chart1.Series("Default").BorderWidth = 1
        Chart1.Series("Default").BorderColor = Color.DarkGreen
        Chart1.Series("Default").Points.DataBindXY(myReader, "One", myReader, "Two")

        myReader.Close()
        myConnection.Close()

        Chart1.Visible = True

        ViewMonths = "OK"
    End Function


    Protected Sub ImageButtonJan_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonJan.Click
        Call ViewMonths(1)
    End Sub

    Protected Sub ImageButtonFeb_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonFeb.Click
        Call ViewMonths(2)
    End Sub

    Protected Sub ImageButtonMar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonMar.Click
        Call ViewMonths(3)
    End Sub

    Protected Sub ImageButtonApr_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonApr.Click
        Call ViewMonths(4)
    End Sub

    Protected Sub ImageButtonMay_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonMay.Click
        Call ViewMonths(5)
    End Sub

    Protected Sub ImageButtonJun_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonJun.Click
        Call ViewMonths(6)
    End Sub

    Protected Sub ImageButtonJul_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonJul.Click
        Call ViewMonths(7)
    End Sub

    Protected Sub ImageButtonAug_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonAug.Click
        Call ViewMonths(8)
    End Sub

    Protected Sub ImageButtonSep_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonSep.Click
        Call ViewMonths(9)
    End Sub

    Protected Sub ImageButtonOct_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonOct.Click
        Call ViewMonths(10)
    End Sub

    Protected Sub ImageButtonNov_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonNov.Click
        Call ViewMonths(11)
    End Sub

    Protected Sub ImageButtonDec_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonDec.Click
        Call ViewMonths(12)
    End Sub

    Protected Sub ImageButtonCategories_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonCategories.Click
        Me.PanelAdministration.Visible = False
        Me.txtNewCategoryName.Text = ""
        Me.txtNewCategoryName.Focus()
        Me.PanelAdministrationTwo.Visible = True
    End Sub

    Protected Sub ImageButtonCategoryUpdate_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonCategoryUpdate.Click
        Dim strSQL As String = ""
        If Trim(txtCategoryName.Text) = "" Then
            MsgBox("Please enter a valid Production category", MsgBoxStyle.Critical, "validation error")
            Me.txtCategoryName.Focus()
        Else
            ' intID, CustID,ProductType
            strSQL = " UPDATE tblCustomer_Product_Type "
            strSQL = strSQL & "   SET  ProductType=" & "'" & Trim(Me.txtCategoryName.Text) & "'"
            strSQL = strSQL & "  WHERE intID=" & Me.HiddenFieldCurrentCategoryItem.Value
            Me.SqlDataSource3.UpdateCommand = strSQL
            Me.SqlDataSource3.Update()


            strSQL = ""
            'strSQL = strSQL & "   SELECT        "
            'strSQL = strSQL & "      tblCustomer_Production_Values.CustUM , COUNT(*) as mycount' ,       "
            'strSQL = strSQL & "     tblCustomer_Product_Type.ProductType  ,     "
            'strSQL = strSQL & "    dbo.fnSummary_ReturnedDates (tblCustomer_Production_Values.CustUM,'" & Me.CompanyID.Value & "')  as 'Range' "
            'strSQL = strSQL & "      FROM dbo.tblCustomer_Production_Values   "
            'strSQL = strSQL & "        INNER JOIN tblCustomer_Product_Type ON tblCustomer_Product_Type.intID=tblCustomer_Production_Values.CustUM   "
            'strSQL = strSQL & "      AND tblCustomer_Product_Type.CustID=" & "'" & Me.CompanyID.Value & "'"
            'strSQL = strSQL & "    GROUP BY tblCustomer_Production_Values.CustUM,tblCustomer_Product_Type.ProductType     "

            strSQL = " spProductType_RangeCombined " & Me.CompanyID.Value
            Me.SqlDataSource3.SelectCommand = strSQL
            GridView2.DataBind()


        End If
    End Sub

    Protected Sub ImageButtonCategoryDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonCategoryDelete.Click
        Dim nReturn As Integer = 0
        Dim iMessageWarning As String = ""
        Dim strSQL As String = ""
        ' check to see if there are any production figure
        ' on file - if there are , tell user that this
        ' category cannot be deleted - they must first
        ' delete all the production figures first
        ' OR by accepting YES to delete , this option
        ' will ERASE then for then
        strSQL = " SELECT   COUNT(*) as 'Items'  from dbo.tblCustomer_Production_Values WHERE custUM=" & Me.HiddenFieldCurrentCategoryItem.Value
        Dim myConnection As New OleDb.OleDbConnection(Core.CurrentConnectionString)
        Dim myCommand As New OleDb.OleDbCommand(strSQL, myConnection)
        myCommand.Connection.Open()
        myCommand.CommandTimeout = 180
        Dim DeletionOK As Boolean = False
        Dim Reader As OleDb.OleDbDataReader = myCommand.ExecuteReader(CommandBehavior.CloseConnection)
        If Reader.HasRows Then
            Reader.Read()
            If Reader("items") = 0 Then
                DeletionOK = True
            Else
                DeletionOK = False
            End If
            'End While
            Reader.Close()
        End If

        If DeletionOK = False Then
            iMessageWarning = "There are ENTERIES on the systems which relate  " & vbNewLine
            iMessageWarning = iMessageWarning & " to this specific category     " & vbNewLine & vbNewLine
            iMessageWarning = iMessageWarning & "  If you choose to Contiune with the Deletion   " & vbNewLine
            iMessageWarning = iMessageWarning & "  then Production Figures for All Years stored   " & vbNewLine
            iMessageWarning = iMessageWarning & " will be LOST  " & vbNewLine & vbNewLine
            iMessageWarning = iMessageWarning & " Would you like to Contiune Y/N   " & vbNewLine
            nReturn = MsgBox(iMessageWarning, MsgBoxStyle.YesNo, "Deletion Warning")
            If nReturn = 6 Then ' OK user has aggred to delete all records Via this category
                DeletionOK = True
            Else
                Exit Sub
            End If
        Else
        End If

        If DeletionOK = True Then
            strSQL = ""
            strSQL = strSQL & " DELETE from tblCustomer_Product_Type where intID=" & Me.HiddenFieldCurrentCategoryItem.Value
            Me.SqlDataSource3.DeleteCommand = strSQL
            Me.SqlDataSource3.Delete()

            strSQL = ""
            strSQL = strSQL & " DELETE from tblCustomer_Production_Values where CustUM=" & Me.HiddenFieldCurrentCategoryItem.Value
            Me.SqlDataSource2.DeleteCommand = strSQL
            Me.SqlDataSource2.Delete()
        End If

        strSQL = ""
        'strSQL = strSQL & "   SELECT        "
        'strSQL = strSQL & "      tblCustomer_Production_Values.CustUM , COUNT(*) ,       "
        'strSQL = strSQL & "     tblCustomer_Product_Type.ProductType  ,     "
        'strSQL = strSQL & "    dbo.fnSummary_ReturnedDates (tblCustomer_Production_Values.CustUM,'" & Me.CompanyID.Value & "')  as 'Range' "
        'strSQL = strSQL & "      FROM dbo.tblCustomer_Production_Values   "
        'strSQL = strSQL & "        INNER JOIN tblCustomer_Product_Type ON tblCustomer_Product_Type.intID=tblCustomer_Production_Values.CustUM   "
        'strSQL = strSQL & "      AND tblCustomer_Product_Type.CustID=" & "'" & Me.CompanyID.Value & "'"
        'strSQL = strSQL & "    GROUP BY tblCustomer_Production_Values.CustUM,tblCustomer_Product_Type.ProductType     "


        strSQL = " spProductType_RangeCombined " & Me.CompanyID.Value
        Me.SqlDataSource3.SelectCommand = strSQL
        GridView2.DataBind()

        Me.PanelAdministration.Visible = False

    End Sub

    Protected Sub ImageButtonNewCategoryUpdate_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonNewCategoryUpdate.Click
        If Trim(txtNewCategoryName.Text) = "" Then
            MsgBox("Please enter a valid Production category", MsgBoxStyle.Critical, "validation error")
            Me.txtNewCategoryName.Focus()
        Else
            Dim strSQL As String = ""

            '     intId()
            '     CustID()
            '     producttype()

            strSQL = ""
            strSQL = strSQL & "   INSERT INTO    tblCustomer_Product_Type (custID,ProductType)       "
            strSQL = strSQL & " VALUES( "
            strSQL = strSQL & "'" & Me.CompanyID.Value & "'" & ","
            strSQL = strSQL & "'" & Trim(Me.txtNewCategoryName.Text) & "'"
            strSQL = strSQL & " ) "

            ' MsgBox(strSQL)

            Me.SqlDataSource3.InsertCommand = strSQL
            Me.SqlDataSource3.Insert()

            'strSQL = ""
            'strSQL = strSQL & "   SELECT        "
            'strSQL = strSQL & "      tblCustomer_Production_Values.CustUM , COUNT(*) ,       "
            'strSQL = strSQL & "     tblCustomer_Product_Type.ProductType  ,     "
            'strSQL = strSQL & "    dbo.fnSummary_ReturnedDates (tblCustomer_Production_Values.CustUM,'" & Me.CompanySelect.SelectedValue & "')  as 'Range' "
            'strSQL = strSQL & "      FROM dbo.tblCustomer_Production_Values   "
            'strSQL = strSQL & "        INNER JOIN tblCustomer_Product_Type ON tblCustomer_Product_Type.intID=tblCustomer_Production_Values.CustUM   "
            'strSQL = strSQL & "      AND tblCustomer_Product_Type.CustID=" & "'" & Me.CompanySelect.SelectedValue & "'"
            'strSQL = strSQL & "    GROUP BY tblCustomer_Production_Values.CustUM,tblCustomer_Product_Type.ProductType     "
            'Me.SqlDataSource3.SelectCommand = strSQL

            strSQL = " spProductType_RangeCombined " & Me.CompanyID.Value
            Me.SqlDataSource3.SelectCommand = strSQL

            ' Me.SqlDataSource3.SelectCommand = " EXEC spProductType_With_Range " & "'" & Me.CompanyID.Value & "'"
            GridView2.DataBind()
            Me.txtNewCategoryName.Text = ""
            Me.txtNewCategoryName.Focus()

        End If
    End Sub

    Protected Sub ImageButtonInsertNewDateAndValue_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonInsertNewDateAndValue.Click
        Me.errorlabel.Text = "Status: "
        '      Me.PanelAdministration.Visible = False
        '      Me.PanelAdministrationTwo.Visible = False
        '      Me.SchedulePanel.Visible = False
        '      Me.PanelEachLine.Visible = False
        Me.txtRollOver.Text = ""
        If IsDate(txtNewDate.Text) = True Then
        Else
            Me.errorlabel.Text = "Status: Please enter a Vaild date"
            txtNewDate.Focus()
        End If

        If IsNumeric(txtNewValue.Text) = True Then
        Else
            Me.errorlabel.Text = "Status: Please enter a Vaild Figure"
            txtNewValue.Focus()
        End If

        Dim strSQL As String = ""
        strSQL = "  INSERT INTO tblCustomer_Production_Values (CustID,CustUM,CustValue,ProductionDate) "
        strSQL = strSQL & " Values("
        strSQL = strSQL & "'" & Me.CompanyID.Value & "'" & ","
        strSQL = strSQL & "'" & Me.HiddenFieldCurrentCategoryItem.Value & "'" & ","
        strSQL = strSQL & "'" & Me.txtNewValue.Text & "'" & ","
        strSQL = strSQL & "'" & Me.txtNewDate.Text & "'"
        ' here


        strSQL = strSQL & ")"
        SqlDataSource4.InsertCommand = strSQL
        SqlDataSource4.Insert()

        '=================================================================
        strSQL = " SELECT [intID],[CustID] ,[CustUM ],[CustValue] , [ProductionDate] ,DATENAME(DW,ProductionDate) as [DayName] FROM [tblCustomer_Production_Values] "
        strSQL = strSQL & " WHERE [CustID] = " & Me.CompanyID.Value
        strSQL = strSQL & " AND [CustUM] = " & Me.HiddenFieldCurrentCategoryItem.Value
        strSQL = strSQL & " Order By [ProductionDate] DESC"
        SqlDataSource2.SelectCommand = strSQL

        'strSQL = ""
        'strSQL = strSQL & "  SELECT tblCustomer_Production_Values.CustUM"
        'strSQL = strSQL & "  , COUNT(*) , tblCustomer_Product_Type.ProductType ,"
        'strSQL = strSQL & " dbo.fnSummary_ReturnedDates(tblCustomer_Production_Values.CustUM,'872') AS 'Range'"
        'strSQL = strSQL & "   FROM dbo.tblCustomer_Production_Values "
        'strSQL = strSQL & "  INNER JOIN tblCustomer_Product_Type ON tblCustomer_Product_Type.intID= tblCustomer_Production_Values.CustUM  "
        'strSQL = strSQL & " AND tblCustomer_Product_Type.CustID=" & "'" & Me.CompanyID.Value & "'"
        'strSQL = strSQL & " GROUP BY tblCustomer_Production_Values.CustUM , tblCustomer_Product_Type.ProductType "

        strSQL = " spProductType_RangeCombined " & Me.CompanyID.Value
        SqlDataSource3.SelectCommand = strSQL
        NewPanel.Visible = False


    End Sub
End Class
