Imports System.IO
Imports System.IO.Directory
Imports System.Collections
Imports System.Web.UI
Imports System.Net.Mail

Partial Class myapps_MyActivityManagement_ActivityViewer
    Inherits System.Web.UI.Page

    Dim strReference As String
    Dim intNotify As Integer

    Dim CurrentUserID As String
    Dim CurrentUserName As String
    Dim CurrentEmail As String
    Dim strSavedName As String
    Dim strStatus As String
    Dim dtmDateTime As Date = Now()
    Dim strCompanyName As String

    Dim strSubject As String = "Utiliy Masters Ltd: ActivityManagement Notification."
    Dim strFrom As String = "activitymanagement@utilitymasters.co.uk" ' Email Sender.
    Dim strTo As String = "" 'Email Address of Recipient.
    Dim strMessage As String = ""
    Dim strUserName As String
    Dim strPassword As String
    Dim strAttachment As String = ""
    Dim oFileName As System.IO.File
    Dim oWrite As System.IO.StreamWriter


    Dim dmSQL As New System.Data.OleDb.OleDbCommand
    Dim dnSQL As New System.Data.OleDb.OleDbConnection
    Dim daSQL As New System.Data.OleDb.OleDbDataAdapter
    Dim drSQL As System.Data.OleDb.OleDbDataReader
    Dim dtSQL As New System.Data.DataTable
    Dim dsSQL As New System.Data.DataSet
    Dim dsNewRow As System.Data.DataRow
    Dim strSQL As String
    Dim cn As String = Core.CurrentConnectionString

    Dim UMLemployeeCheck As Integer

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        CurrentUserID = Core.data_select_value("SELECT intUserPK FROM [portal.utilitymasters.co.uk].dbo.tblUsers WHERE intUserPK = " & MySession.UserID)
        CurrentUserName = Core.data_select_value("SELECT varForename + ' ' + varSurname FROM [portal.utilitymasters.co.uk].dbo.tblUsers WHERE intUserPK = " & MySession.UserID)
        CurrentEmail = Core.data_select_value("SELECT varEmail FROM [portal.utilitymasters.co.uk].dbo.tblUsers WHERE intUserPK = " & MySession.UserID)

        UMLemployeeCheck = Core.data_select_value("SELECT intUMLEmployee FROM [portal.utilitymasters.co.uk].dbo.tblUsers WHERE intUserPK = '" & MySession.UserID & "'")

        'Issue Identifier
        strReference = Session("ID")

        strReference = Core.data_select_value("SELECT intIssueID FROM dbo.tblAccMan_IssueLogs WHERE intIssueID = '" & strReference & "'") 'ID
        strSavedName = Core.data_select_value("SELECT varUserName FROM dbo.tblAccMan_IssueLogs WHERE intIssueID = '" & strReference & "'") 'Issue Auther
        strStatus = Core.data_select_value("SELECT varStatus FROM dbo.tblAccMan_IssueLogs WHERE intIssueID ='" & strReference & "'") ' Status
        strCompanyName = Core.data_select_value("SELECT varCompanyName FROM dbo.tblAccMan_IssueLogs WHERE intIssueID ='" & strReference & "'")

        'strTo = Core.data_select_value("SELECT varUserEmail FROM dbo.tblAccMan_IssueLogs WHERE intIssueID = '" & strReference & "'") 'Email Address

        'Load Comments
        LoadComments()

        'Load Selected Issue
        Me.lblReference.Text = " [" & Core.data_select_value("SELECT intIssueID FROM dbo.tblAccMan_IssueLogs WHERE intIssueID = '" & strReference & "'") & "] " & _
        Core.data_select_value("SELECT varIssueName FROM dbo.tblAccMan_IssueLogs WHERE intIssueID = '" & strReference & "'") & _
        Core.data_select_value("SELECT varCompanyName FROM dbo.tblAccMan_IssueLogs WHERE intIssueID = '" & strReference & "'") & _
        " > " & Core.data_select_value("SELECT varSiteName FROM dbo.tblAccMan_IssueLogs WHERE intIssueID = '" & strReference & "'")

        Me.lblIssueBody.Text = Core.data_select_value("SELECT varIssueBody FROM dbo.tblAccMan_IssueLogs WHERE intIssueID = '" & strReference & "'")

        If Not Me.Page.IsPostBack Then

            ddlLoad()

            If strStatus = "Closed" Then
                Me.ddlCompleted.SelectedItem.Selected = False
                Me.ddlCompleted.Items.FindByText("Closed").Selected = True
                Me.pnlIssueDetails.Enabled = False
                Me.pnlViewComments.Enabled = False
                Me.pnlSubscribe.Enabled = False
                'pnlViewAttachments.Enabled = False
                ddlLoad()
            Else
                Me.ddlCompleted.SelectedItem.Selected = False
                Me.ddlCompleted.Items.FindByText(strStatus).Selected = True
                Me.pnlIssueDetails.Enabled = True
                Me.pnlViewComments.Enabled = True
                Me.pnlSubscribe.Enabled = True
                'pnlViewAttachments.Enabled = True
                ddlLoad()
            End If

            If UMLemployeeCheck = 1 Then
            ElseIf UMLemployeeCheck = 0 Then
                Me.ddlCompleted.Items.RemoveAt(3)
            End If
        End If

        Dim strScript As String = "<SCRIPT LANGUAGE=""JavaScript"">javascript:Scroll('Tst1',0,10000);</SCRIPT>"

        'Register the JavaScript. 
        If (Not Page.IsStartupScriptRegistered("clientScript")) Then
            Page.RegisterStartupScript("clientScript", strScript)
        End If

    End Sub

    Public Sub LoadComments()
        Me.Panel2.Controls.Clear()
        'Get All Comments. 
        With Me.SqlComments
            .ConnectionString = Core.ConnectionString
            .SelectCommand = "SELECT * FROM tblAccMan_Comments WHERE intIssueFK = '" & strReference & "' ORDER BY dtmDate ASC"
            .SelectCommandType = SqlDataSourceCommandType.Text
        End With

        Dim CommentData As DataView = CType(SqlComments.Select(DataSourceSelectArguments.Empty), DataView)

        Const TextColumnNameA As String = "varUserName"
        Const TextColumnNameB As String = "dtmDate"
        Const TextColumnNameC As String = "varComment"

        For Each row As DataRowView In CommentData

            Dim UMLemployee As Integer
            UMLemployee = Core.data_select_value("SELECT intUMLEmployee FROM [portal.utilitymasters.co.uk].dbo.tblUsers WHERE varForename + ' ' + varSurname =  '" & row(TextColumnNameA).ToString & "'")

            Dim strComment As String
            Dim strCommentDisplay As String

            strComment = row(TextColumnNameC).ToString
            strCommentDisplay = strComment.Substring(0, 3)

            If strCommentDisplay = "XXX" Then
                'MsgBox("Removed Comment")
                'ElseIf strCommentDisplay = "" Then
                'MsgBox("")
            ElseIf strCommentDisplay = "ATT" Or strCommentDisplay = "REM" Then
                Dim dynDivATT As New System.Web.UI.HtmlControls.HtmlGenericControl("DIV")
                Dim strInner As String = ""

                strInner &= "<div style='font-size:12px; line-height:95%; width:350px; height:auto; Margin-Top:5px; margin-right:10px; Padding-Left:10px; position:10px;float:right; color:#000000;' >"
                strInner &= "  <b class='blspiffy2'>"
                strInner &= "  <b class='spiffy1'><b></b></b>"
                strInner &= "  <b class='spiffy2'><b></b></b>"
                strInner &= "  <b class='spiffy3'></b>"
                strInner &= "  <b class='spiffy4'></b>"
                strInner &= "  <b class='spiffy5'></b></b>"

                strInner &= "  <div class='blspiffy2fg'>"
                If strCommentDisplay = "ATT" Then
                    strInner &= row(TextColumnNameA).ToString & " - " & row(TextColumnNameB).ToString & "<br />" & "Attachment Added: " & (row(TextColumnNameC).ToString).Remove(0, 5)
                ElseIf strCommentDisplay = "REM" Then
                    strInner &= row(TextColumnNameA).ToString & " - " & row(TextColumnNameB).ToString & "<br />" & "Attachment Removed: " & (row(TextColumnNameC).ToString).Remove(0, 5)
                End If

                strInner &= "  </div>"

                strInner &= "  <b class='blspiffy2'>"
                strInner &= "  <b class='spiffy5'></b>"
                strInner &= "  <b class='spiffy4'></b>"
                strInner &= "  <b class='spiffy3'></b>"
                strInner &= "  <b class='spiffy2'><b></b></b>"
                strInner &= "  <b class='spiffy1'><b></b></b></b>"
                strInner &= "  <br>"
                strInner &= "</div>"

                dynDivATT.InnerHtml = strInner
                Me.Panel2.Controls.Add(dynDivATT)
            Else
                If UMLemployee = 1 Then
                    Dim dynDiv As New System.Web.UI.HtmlControls.HtmlGenericControl("DIV")
                    Dim strInner As String = ""

                    'strInner &= "<div style='font-size:12px; line-height:95%; width:350px; height:auto; Margin-Top:5px; Padding-Left:15px; Padding-right:5px; Padding-Bottom:5px; position:10px; float:right; color:#FFFFFF;' >"
                    strInner &= "<div style='font-size:12px; line-height:95%; width:350px; height:auto; Margin-Top:5px; margin-left:10px; Padding-Left:5px; position:10px;float:left;color:#FFFFFF;' >"
                    strInner &= "  <b class='blspiffy'>"
                    strInner &= "  <b class='spiffy1'><b></b></b>"
                    strInner &= "  <b class='spiffy2'><b></b></b>"
                    strInner &= "  <b class='spiffy3'></b>"
                    strInner &= "  <b class='spiffy4'></b>"
                    strInner &= "  <b class='spiffy5'></b></b>"

                    strInner &= "  <div class='blspiffyfg'>"
                    strInner &= row(TextColumnNameA).ToString & "<br />" & row(TextColumnNameB).ToString & "<br />" & row(TextColumnNameC).ToString
                    strInner &= "  </div>"

                    strInner &= "  <b class='blspiffy'>"
                    strInner &= "  <b class='spiffy5'></b>"
                    strInner &= "  <b class='spiffy4'></b>"
                    strInner &= "  <b class='spiffy3'></b>"
                    strInner &= "  <b class='spiffy2'><b></b></b>"
                    strInner &= "  <b class='spiffy1'><b></b></b></b>"
                    strInner &= "  <br>"
                    strInner &= "</div>"

                    dynDiv.InnerHtml = strInner
                    Me.Panel2.Controls.Add(dynDiv)

                Else
                    Dim dynDiv2 As New System.Web.UI.HtmlControls.HtmlGenericControl("DIV")
                    Dim strInner As String = ""

                    'strInner &= "<div style='font-size:12px; line-height:95%; width:350px; height:auto; Margin-Top:5px; Padding-Left:5px; Padding-Right:15px; Padding-Bottom:5px; position:10px; float:left; color:#000000;' >"
                    strInner &= "<div style='font-size:12px; line-height:95%; width:350px; height:auto; Margin-Top:5px; margin-right:10px; Padding-Left:10px; position:10px;float:right; color:#000000;' >"
                    strInner &= "  <b class='spiffy'>"
                    strInner &= "  <b class='spiffy1'><b></b></b>"
                    strInner &= "  <b class='spiffy2'><b></b></b>"
                    strInner &= "  <b class='spiffy3'></b>"
                    strInner &= "  <b class='spiffy4'></b>"
                    strInner &= "  <b class='spiffy5'></b></b>"

                    strInner &= "  <div class='spiffyfg'>"
                    strInner &= row(TextColumnNameA).ToString & "<br />" & row(TextColumnNameB).ToString & "<br />" & row(TextColumnNameC).ToString
                    strInner &= "  </div>"

                    strInner &= "  <b class='spiffy'>"
                    strInner &= "  <b class='spiffy5'></b>"
                    strInner &= "  <b class='spiffy4'></b>"
                    strInner &= "  <b class='spiffy3'></b>"
                    strInner &= "  <b class='spiffy2'><b></b></b>"
                    strInner &= "  <b class='spiffy1'><b></b></b></b>"
                    strInner &= "  <br>"
                    strInner &= "</div>"

                    dynDiv2.InnerHtml = strInner
                    Me.Panel2.Controls.Add(dynDiv2)
                End If
            End If
        Next
    End Sub

    Function RemoveCharacters(ByVal strText As String)
        'Check for unwanted Charatcers. 
        Dim illegalChars As Char() = "'_%*^".ToCharArray()
        Dim str As String = strText
        Dim sb As New System.Text.StringBuilder

        For Each ch As Char In str
            If Array.IndexOf(illegalChars, ch) = -1 Then
                sb.Append(ch)
            End If
        Next
        Return sb.ToString()
    End Function

    Protected Sub btnAddComment_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddComment.ServerClick
        If pnlViewComments.Enabled = False Then
        Else
            intNotify = CInt(Core.data_select_value("SELECT [intNotify] FROM tblAccMan_IssueLogs WHERE [intIssueID] = '" & CInt(strReference) & "'"))

            Core.data_execute_nonquery("INSERT INTO tblAccMan_Comments (intIssueFK, intUserID, varUserName, dtmDate, varComment) VALUES('" & CInt(strReference) & "','" & CurrentUserID & "','" & CurrentUserName & "','" & Now() & "','" & RemoveCharacters(Me.txtComment.Text) & "')")
            Core.data_execute_nonquery("UPDATE tblAccMan_IssueLogs SET dtmModified = '" & dtmDateTime & "', intModUserID = '" & CurrentUserID & "', varModUserName = '" & CurrentUserName & "'   WHERE intIssueID = '" & strReference & "'")

            'Notification Value
            intNotify = CInt(Core.data_select_value("SELECT [intNotify] FROM tblAccMan_IssueLogs WHERE [intIssueID] = '" & CInt(strReference) & "'"))

            'UML Employee Auto Add to Subscribe List on Post
            If UMLemployeeCheck = 1 Then
                Dim intSubscriberExists As Integer
                intSubscriberExists = CInt(Core.data_select_value("SELECT DISTINCT intUserID FROM dbo.tblAccMan_SubscribeList WHERE intIssueFK = '" & Me.strReference & "' AND intUserID = '" & Me.CurrentUserID & "'"))

                If intSubscriberExists = Nothing Then
                    'Add New Subscriber
                    Core.data_execute_nonquery("INSERT INTO dbo.tblAccMan_SubscribeList (intIssueFK,intUserID,varUserEmail,intIssueAuthor,intNotification) VALUES ('" & Me.strReference & "','" & CurrentUserID & "','" & CurrentEmail & "',0,1)")
                Else
                    'Already Subscribed...
                End If
            End If

            dnSQL = New System.Data.OleDb.OleDbConnection(cn)
            strSQL = "SELECT varUserEmail, intUserID FROM dbo.tblAccMan_SubscribeList WHERE intIssueFK = '" & strReference & "'"
            dmSQL.CommandText = strSQL
            dmSQL.Connection = dnSQL
            daSQL.SelectCommand = dmSQL
            dsSQL = New System.Data.DataSet
            daSQL.Fill(dsSQL)
            dtSQL = dsSQL.Tables(0)

            'Check if Notification Required
            If UMLemployeeCheck = 1 Then ' UMLEmployee
                'UML > Non UML
                For Each dsNewRow In dtSQL.Rows

                    Dim IsEmployee As Integer
                    IsEmployee = CInt(Core.data_select_value("SELECT intUMLEmployee FROM [portal.utilitymasters.co.uk].dbo.tblUsers INNER JOIN dbo.tblAccMan_SubscribeList ON intUserID = [portal.utilitymasters.co.uk].dbo.tblUsers.intUserPK WHERE [portal.utilitymasters.co.uk].dbo.tblUsers.intUserPK = '" & dsNewRow(1) & "'"))

                    'Email Non UML employees.
                    If IsEmployee = 0 Then
                        strMessage = ""
                        strFrom = "activitymanagement@utilitymasters.co.uk"
                        strTo = dsNewRow(0) 'Email Address
                        EmailNotification()
                        'MsgBox("Email: Non UML User")
                    End If
                Next

                Response.Redirect("ActivityViewer.aspx")
            ElseIf UMLemployeeCheck = 0 Then ' Non UMLemployee
                'Non UML > UML

                For Each dsNewRow In dtSQL.Rows

                    Dim IsEmployee As Integer
                    IsEmployee = CInt(Core.data_select_value("SELECT intUMLEmployee FROM [portal.utilitymasters.co.uk].dbo.tblUsers INNER JOIN dbo.tblAccMan_SubscribeList ON intUserID = [portal.utilitymasters.co.uk].dbo.tblUsers.intUserPK WHERE [portal.utilitymasters.co.uk].dbo.tblUsers.intUserPK = '" & dsNewRow(1) & "'"))

                    'Email Non UML employees.
                    If IsEmployee = 1 Then
                    Else
                        If dsNewRow(1) = CurrentUserID Then
                        Else
                            strMessage = ""
                            strFrom = CurrentEmail
                            strTo = dsNewRow(0) 'Email Address
                            EmailNotification()
                            'MsgBox("Email: Non UML User")
                        End If
                    End If
                Next

                'Email Activity Management Mailing List
                strMessage = ""
                strFrom = CurrentEmail
                strTo = "activitymanagement@utilitymasters.co.uk"
                EmailNotification()
                'MsgBox("Email: Activity Management")

                Response.Redirect("ActivityViewer.aspx")
            End If
        End If
    End Sub

    Public Sub EmailNotification()
        strMessage &= "<html>"
        strMessage &= "<head>"

        strMessage &= "<title>Utiliy Masters Ltd: ActivityManagement Notification</title>"
        strMessage &= "<style type='text/css' media='screen'>"
        strMessage &= "body {font-family: Verdana;}}"
        strMessage &= "h1, h2, h3, h4, h5, h6{font-family: 'Verdana', 'Helvetica', 'Arial', sans-serif}"
        strMessage &= "h1 {font: normal 2.29em; color:#082B61;}"
        strMessage &= "h2 {font: normal 1.89em; color:#082B61;}"
        strMessage &= "h3 {font: normal 1.19em; color:#6ebb1f;}"
        strMessage &= "h4 {font: normal 1em; color:#082B61;}"
        strMessage &= "h5 {font-size:12px; color:#082B61;}"
        strMessage &= "h6 {font-size:12px; color:#6ebb1f;}"

        strMessage &= ".HeaderGap{height: 5px; font-family: Verdana;}"
        strMessage &= ".MainBox{width: 755px; font-family: Verdana; margin: 0 auto; padding: 0px 10px 10px 10px;}"
        strMessage &= ".TableListOne{background-color: #EBEBEB; font-family: Verdana; margin: 0 auto; padding: 0px 10px 10px 10px;}"
        strMessage &= ".style1{font-weight: normal; font-family: Verdana;}"
        strMessage &= ".style2{text-align: center; font-weight: 700; font-family: Verdana;}"
        strMessage &= ".style3{text-align: center; font-family: Verdana;}"
        strMessage &= ".UMLBlue{color: #082B61; font-weight: bold;font-family: Verdana;}"
        strMessage &= "</style>"
        strMessage &= "</head>"

        strMessage &= "<body>"
        strMessage &= "<form id='form1'>"

        strMessage &= "<table>"
        strMessage &= "<tr><td class='style3'>"
        strMessage &= "<a title='UTILISYS Email Communication' href='http://www.utilitymasters.co.uk/page/carbon-reduction-commitment-service.aspx' target='_blank'><img title='Utility Masters Home' alt='Utility Masters Home' src='http://images.utilitymasters.co.uk/broadcasts/CRC_Seminars_19012010/branding-header.jpg' border='0'></a></td></tr>"
        strMessage &= "</td></tr>"
        strMessage &= "<td class='MainBox'>"
        strMessage &= "<table cellpadding='3'>"
        strMessage &= "<tr><td class='style2'><h2>Activity Management Notification</h2><br /></td></tr>"
        strMessage &= "<tr>"
        strMessage &= "<td><p>Hello,</p></td>"
        strMessage &= "<tr><td><br /><br /></td></tr>"
        strMessage &= "<tr><td><p>You have received this notification in response to a Comment/Solution being posted.</p></td></tr>"
        strMessage &= "</tr>"
        strMessage &= "</table>"

        strMessage &= "<table align='center' cellpadding='3'>"
        strMessage &= "<tr><td><br /><br /></td></tr>"
        strMessage &= "<tr><td><p>Reference: " & Me.lblReference.Text & " </p></td></tr>"
        strMessage &= "<tr><td><p>Issue-Body: " & Me.lblIssueBody.Text & " </p></td></tr>"
        strMessage &= "<tr><td><p>Comment: " & Me.txtComment.Text & " </p></td></tr>"
        strMessage &= "<tr><td><br /><br /></td></tr>"
        strMessage &= "</table>"

        strMessage &= "<table>"
        strMessage &= "<tr><td>Visit our main website, <a title='http://www.utilitymasters.co.uk' href='http://www.utilitymasters.co.uk' target='_blank'>http://www.utilitymasters.co.uk</a> and follow the my <span class='UMLBlue'>UTILI</span>SYS link.</td></tr>"
        strMessage &= "<tr><td>Or alternatively, <a title='https://portal.utilitymasters.co.uk' href='https://portal.utilitymasters.co.uk' target='_blank'>https://portal.utilitymasters.co.uk</a></td></tr>"
        strMessage &= "<tr><td><br /></td></tr>"
        strMessage &= "<tr><td><br /></td></tr>"
        strMessage &= "<tr><td><p>Thankyou for choosing <span class='UMLBlue'>Utility Masters Ltd</span>.</p></td></tr>"
        strMessage &= "<tr><td><br /></td></tr>"
        strMessage &= "<tr><td><br /></td></tr>"
        strMessage &= "<tr><td>"
        strMessage &= "<p>With Best Regards,</p>"
        strMessage &= "</td></tr>"
        strMessage &= "<tr><td><br /></td></tr>"
        strMessage &= "</table>"

        strMessage &= "<table>"
        strMessage &= "<tr>"
        strMessage &= "<td>"
        strMessage &= "<p><span class='UMLBlue'>Utility Masters Ltd</span><br />"
        strMessage &= "UML House, Salmon Fields Business Village, Royton, OL2 6HT<br />"
        strMessage &= "t. 0161 785 0404 <br />"
        strMessage &= "f. 0161 785 7969 <br />"
        strMessage &= "e. <a href='mailto:support@utilitymasters.co.uk?subject=UTILISYS Account Enquiry' target='_blank'>support@utilitymasters.co.uk</a></p>"
        strMessage &= "<br />"
        strMessage &= "</td>"
        strMessage &= "</tr>"
        strMessage &= "<tr>"
        strMessage &= "<td>"
        strMessage &= "<p>This is an automated Email Notification.  Please do not reply to this Email.</p>"
        strMessage &= "<br />"
        strMessage &= "</td>"
        strMessage &= "</tr>"
        strMessage &= "</table>"
        strMessage &= "</form>"
        strMessage &= "</body>"
        strMessage &= "</html>"

        Dim Email As New System.Net.Mail.MailMessage(strFrom, strTo)
        Email.Subject = strSubject
        Email.Body = strMessage
        Email.IsBodyHtml = True

        Dim mailClient As New System.Net.Mail.SmtpClient()
        Dim basicAuthenticationInfo As New System.Net.NetworkCredential("uml\administrator", "chicago")
        mailClient.Host = "10.44.5.204"
        mailClient.UseDefaultCredentials = False
        mailClient.Credentials = basicAuthenticationInfo

        mailClient.Send(Email)

        'Response.Redirect("ActivityViewer.aspx")
    End Sub

    Protected Sub ddlCompleted_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCompleted.SelectedIndexChanged
        If Me.ddlCompleted.SelectedItem.Value = 1 Then
            Core.data_execute_nonquery("UPDATE tblAccMan_IssueLogs SET varStatus = 'Closed', intModUserID = '" & CurrentUserID & "', varModUserName = '" & CurrentUserName & "' WHERE intIssueID = '" & strReference & "'")
            Response.Redirect("ActivityList.aspx")
        ElseIf Me.ddlCompleted.SelectedItem.Value = 2 Then
            Core.data_execute_nonquery("UPDATE tblAccMan_IssueLogs SET varStatus = 'Open', intModUserID = '" & CurrentUserID & "', varModUserName = '" & CurrentUserName & "' WHERE intIssueID = '" & strReference & "'")
            Response.Redirect("ActivityList.aspx")
        ElseIf Me.ddlCompleted.SelectedItem.Value = 3 Then
            Core.data_execute_nonquery("UPDATE tblAccMan_IssueLogs SET varStatus = 'Pending', intModUserID = '" & CurrentUserID & "', varModUserName = '" & CurrentUserName & "' WHERE intIssueID = '" & strReference & "'")
            Response.Redirect("ActivityList.aspx")
        Else
        End If
    End Sub

    Protected Sub btnViewAttachment_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewAttachment.ServerClick
        If pnlViewAttachments.Visible = False Then
            Me.pnlViewAttachments.Visible = True
            Me.pnlViewComments.Visible = False
            lblInvalidSubscriber.Text = "*unsubscribe?"
        ElseIf Me.pnlViewAttachments.Visible = True Then
            Me.pnlViewAttachments.Visible = False
            lblInvalidSubscriber.Text = "*unsubscribe?"
        End If
    End Sub

    Protected Sub btnViewComment_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewComment.ServerClick
        If pnlViewComments.Visible = False Then
            Me.pnlViewComments.Visible = True
            Me.pnlViewAttachments.Visible = False
            lblInvalidSubscriber.Text = "*unsubscribe?"
        ElseIf Me.pnlViewComments.Visible = True Then
            Me.pnlViewComments.Visible = False
            lblInvalidSubscriber.Text = "*unsubscribe?"
        End If
    End Sub

    Protected Sub btnAttach_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAttach.ServerClick
        If Not FileUpload1.PostedFile Is Nothing And FileUpload1.PostedFile.ContentLength > 0 Then

            Dim fn As String = System.IO.Path.GetFileName(FileUpload1.PostedFile.FileName)
            Dim strFolderName As String = strCompanyName & " - Activity Management"

            System.IO.Directory.CreateDirectory(Core.CustomerDirectory & strCompanyName & "\" & strFolderName & "\" & strReference & "\")
            Dim SaveLocation As String = Core.CustomerDirectory & strCompanyName & "\" & strFolderName & "\" & strReference & "\"

            'System.IO.Directory.CreateDirectory(Server.MapPath("Attachments") & "\" & strReference & "\")
            'Dim SaveLocation As String = Server.MapPath("Attachments") & "\" & strReference & "\"

            If (System.IO.Directory.Exists(SaveLocation)) Then
                'the path and file name to check for duplicates.
                Dim pathToCheck As String = SaveLocation & fn
                'Create a temporary file name to use for checking duplicates
                Dim tempfileName As String
                'Check for File Dulplicates
                If (System.IO.File.Exists(pathToCheck)) Then
                    Dim counter As Integer = 2
                    While (System.IO.File.Exists(pathToCheck))
                        'If File exists rename with Counter Pre-fix. 
                        tempfileName = counter.ToString() & "-" & fn
                        pathToCheck = SaveLocation + tempfileName
                        counter = counter + 1
                    End While
                    fn = tempfileName
                    'User Update, stating file name was changed.
                Else
                    'User Update, stating file name was Saved.
                End If
                SaveLocation += fn
                FileUpload1.SaveAs(SaveLocation)

                Core.data_execute_nonquery("INSERT INTO tblAccMan_Comments (intIssueFK, intUserID, varUserName, dtmDate, varComment) VALUES('" & CInt(strReference) & "','" & CurrentUserID & "','" & CurrentUserName & "','" & Now() & "','ATT - " & fn & "')")
                ddlLoad()

                LoadComments()
            End If
        Else
        End If
    End Sub

    Protected Sub btnRemove_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRemove.ServerClick
        Dim strFolderName As String = strCompanyName & " - Activity Management"
        Try
            'Find File
            Dim TheFile As FileInfo = New FileInfo(Core.CustomerDirectory & strCompanyName & "\" & strFolderName & "\" & strReference & "\" & Me.ddlAttachments.SelectedItem.Text) 'Me.lstbAttachList.SelectedItem.Text
            'Check if File Exists
            If TheFile.Exists Then
                'Remove file from Folder and Listbox
                File.Delete(Core.CustomerDirectory & strCompanyName & "\" & strFolderName & "\" & strReference & "\" & Me.ddlAttachments.SelectedItem.Text) 'Me.lstbAttachList.SelectedItem.Text
                'Core.data_execute_nonquery("UPDATE tblAccMan_Comments SET varComment = 'REM - " & Me.ddlAttachments.SelectedItem.Text & "' WHERE intIssueFK = '" & strReference & "' AND varComment = 'ATT - " & Me.ddlAttachments.SelectedItem.Text & "'")
                Core.data_execute_nonquery("INSERT INTO tblAccMan_Comments (intIssueFK, intUserID, varUserName, dtmDate, varComment) VALUES('" & CInt(strReference) & "','" & CurrentUserID & "','" & CurrentUserName & "','" & Now() & "','REM - " & Me.ddlAttachments.SelectedItem.Text & "')")
                ddlLoad()

                LoadComments()
            Else
                Throw New FileNotFoundException()
            End If
        Catch ex As FileNotFoundException
            'msgbox(ex.Message)
        Catch ex As Exception
            'msgbox(ex.Message)
        End Try
    End Sub

    Protected Sub btnSelectAttachment_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSelectAttachment.ServerClick
        If pnlViewAttachments.Enabled = False Then
        Else
            Dim strchklist As String = ""
            Dim li As ListItem

            'Check if any items have been selected and assign value
            For Each li In Me.ddlAttachments.Items
                If li.Selected Then
                    strchklist += li.Text
                End If
            Next

            If strchklist.ToString = "" Or strchklist.ToString = "[-- Attachment List --]" Then
            Else

                Dim strFolderName As String = strCompanyName & " - Activity Management"

                'File Path
                'Dim strRequest As String = Server.MapPath("Attachments") & "\" & strReference & "\" & ddlAttachments.SelectedItem.Text
                Dim strRequest As String = Core.CustomerDirectory & strCompanyName & "\" & strFolderName & "\" & strReference & "\" & ddlAttachments.SelectedItem.Text

                If strRequest <> "" Then
                    Dim path As String = strRequest
                    Dim file As System.IO.FileInfo = New System.IO.FileInfo(path)
                    'If file exists
                    If file.Exists Then
                        'Open Selected File
                        Response.Clear()
                        Response.AddHeader("Content-Disposition", "attachment; filename=" & file.Name)
                        Response.AddHeader("Content-Length", file.Length.ToString())
                        Response.ContentType = "application/octet-stream"
                        Response.WriteFile(file.FullName)
                        Response.End()
                    Else
                        'msgbox("This file does not exist.")
                    End If
                Else
                    'msgbox("Please provide a file to download.")
                End If
            End If
        End If
    End Sub

    Protected Sub ddlAttachments_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAttachments.Load
        If Not Page.IsPostBack Then
            ddlLoad()
        End If
    End Sub

    Protected Sub ddlLoad()
        'Loads the Documents saved to this call
        ddlAttachments.Items.Clear()
        ddlAttachments.Items.Add("[-- Attachment List --]")

        Dim strFolderName As String = strCompanyName & " - Activity Management"

        Dim strPath As String
        'strPath = Server.MapPath("Attachments") & "\" & strReference & "\"
        strPath = Core.CustomerDirectory & strCompanyName & "\" & strFolderName & "\" & strReference & "\"

        If (System.IO.Directory.Exists(strPath)) Then
            ' Create a reference to the current directory.
            Dim FilePath As New DirectoryInfo(strPath)
            ' Create an array representing the files in the current directory.
            Dim fileName As FileInfo() = FilePath.GetFiles()
            Dim fileTemp As FileInfo
            For Each fileTemp In fileName
                ddlAttachments.Items.Add(fileTemp.Name)
            Next fileTemp
        End If
    End Sub

    Protected Sub btnSubscribe_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubscribe.ServerClick
        If Me.pnlSubscribe.Enabled = False Then
        Else
            lblInvalidSubscriber.Text = ""

            'Check If Subscriber Exists 
            Dim intSubscriberExists As Integer
            intSubscriberExists = CInt(Core.data_select_value("SELECT DISTINCT intUserID FROM dbo.tblAccMan_SubscribeList WHERE intIssueFK = '" & Me.strReference & "' AND intUserID = '" & Me.CurrentUserID & "'"))

            If intSubscriberExists = Nothing Then
                'Add New Subscriber
                Core.data_execute_nonquery("INSERT INTO dbo.tblAccMan_SubscribeList (intIssueFK,intUserID,varUserEmail,intIssueAuthor,intNotification) VALUES ('" & Me.strReference & "','" & CurrentUserID & "','" & CurrentEmail & "',0,1)")
                lblInvalidSubscriber.Text = "*you have been added to the subscribers list."
            Else
                'Already Subscribed
                lblInvalidSubscriber.Text = "*you are already added to the subscribers list."
            End If
        End If
    End Sub

    Protected Sub btnunsubscribe_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnunsubscribe.ServerClick
        If Me.pnlSubscribe.Enabled = False Then
        Else
            lblInvalidSubscriber.Text = ""

            'Check If Author of Issue. 
            Dim intIsAuthor As Integer
            intIsAuthor = CInt(Core.data_select_value("SELECT intIssueAuthor FROM dbo.tblAccMan_SubscribeList WHERE intIssueFK = '" & Me.strReference & "' AND intUserID = '" & Me.CurrentUserID & "'"))

            If intIsAuthor = 1 Then
                lblInvalidSubscriber.Text = "*you are the author and cannot be removed."
            ElseIf intIsAuthor = 0 Then
                'Remove from subscribers list
                Core.data_execute_nonquery("DELETE FROM dbo.tblAccMan_SubscribeList WHERE intIssueFK = '" & Me.strReference & "' AND intUserID = '" & Me.CurrentUserID & "'")
                lblInvalidSubscriber.Text = "*you have been successfully removed."
            End If
        End If
    End Sub

End Class
