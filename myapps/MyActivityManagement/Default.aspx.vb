Imports System.IO
Imports System.IO.Directory
Imports System.Collections
Imports System.Net

Partial Class myapps_MyActivityManagement_Default
    Inherits System.Web.UI.Page

    Dim CurrentUserID As String
    Dim CurrentUserName As String
    Dim CurrentEmail As String
    Dim intID As Integer
    Dim strStatus As String = "Open"
    Dim dtmDateTime As Date
    Dim strCompanyName As String

    Dim strSubject As String = "Utiliy Masters Ltd: ActivityManagement Notification." ' Title. 
    Dim strFrom As String = "activitymanagement@utilitymasters.co.uk" ' Email Sender.
    Dim strTo As String = "" 'Email Address of Recipient.
    Dim strMessage As String = ""
    Dim strUserName As String
    Dim strPassword As String
    Dim strAttachment As String = ""

    Dim oFileName As System.IO.File
    Dim oWrite As System.IO.StreamWriter

    Dim UMLemployee As Integer

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Page.MaintainScrollPositionOnPostBack = True

        intID = ConsecutiveNumber()
        dtmDateTime = Now()

        lblReference.Text = "[" & intID & "]" & " - " & strStatus & " - " & dtmDateTime

        CurrentUserID = Core.data_select_value("SELECT intUserPK FROM [portal.utilitymasters.co.uk].dbo.tblUsers WHERE intUserPK = " & MySession.UserID)
        CurrentUserName = Core.data_select_value("SELECT varForename + ' ' + varSurname FROM [portal.utilitymasters.co.uk].dbo.tblUsers WHERE intUserPK = " & MySession.UserID)
        CurrentEmail = Core.data_select_value("SELECT varEmail FROM [portal.utilitymasters.co.uk].dbo.tblUsers WHERE intUserPK = " & MySession.UserID)

        UMLemployee = Core.data_select_value("SELECT intUMLEmployee FROM [portal.utilitymasters.co.uk].dbo.tblUsers WHERE intUserPK = '" & CurrentUserID & "'")

        Me.btnSave.CausesValidation = True
        Me.btnAttach.CausesValidation = False
        Me.btnSelectAttachment.CausesValidation = False
        Me.btnClear.CausesValidation = False
        Me.btnRemove.CausesValidation = False
        'Me.lstbAttachList.CausesValidation = False
        Me.btnViewAttachment.CausesValidation = False
        Me.btnSiteSearch.CausesValidation = False
        Me.btnSearch.CausesValidation = False

        If Not Me.Page.IsPostBack Then
            'Load Companies
            Dim strSQL As String = "EXEC spAccMan_CompanyDisplay " & MySession.UserID
            Dim myConnection As New OleDb.OleDbConnection(Core.CurrentConnectionString)

            Dim myCommand As New OleDb.OleDbCommand(strSQL, myConnection)
            myCommand.Connection.Open()
            myCommand.CommandTimeout = 180
            Dim Reader As OleDb.OleDbDataReader = myCommand.ExecuteReader(CommandBehavior.CloseConnection)

            While Reader.Read
                Dim Text As String
                Dim Value As String

                Text = Reader(1)
                Value = Reader(0)

                Me.ddlCompanies.Items.Add(New ListItem(Text, Value))
            End While

            'Load Issues
            With Me.ddlIssue
                .Items.Clear()
                .Items.Add("[--Please Select An Issue--]")
                .AppendDataBoundItems = True
                .DataSource = Core.data_select("SELECT intIssuePK, varIssueName FROM tlkpAccMan_IssueList")
                .DataValueField = "intIssuePK"
                .DataTextField = "varIssueName"
                .DataBind()
            End With

            ddlLoad()
        End If
    End Sub

    Function ConsecutiveNumber() As Integer
        Dim nthNumber As Integer
        nthNumber = CInt(Core.data_select_value("SELECT intIssueID FROM dbo.tblAccMan_IssueLogs WHERE intIssuePk = (SELECT MAX(intIssuePk) FROM dbo.tblAccMan_IssueLogs)"))

        If nthNumber = 0 Then
            nthNumber = 1
        Else
            nthNumber += 1
        End If
        Return nthNumber
    End Function

    Protected Sub ddlCompanies_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCompanies.SelectedIndexChanged
        'Load Sites

        If Me.ddlCompanies.SelectedItem.Text = "[--Please Select A Company--]" Then
        Else
            Me.lblErrorMsg1.Visible = False

            'Get Site List
            With Me.ddlSites
                .Items.Clear()
                .Items.Add("[--Please Select A Site--]")
            End With

            Dim mySP As String = "EXEC spAccMan_SitesDisplay " & MySession.UserID & ", " & Me.ddlCompanies.SelectedValue
            Dim myCN As New OleDb.OleDbConnection(Core.CurrentConnectionString)

            Dim myCMD As New OleDb.OleDbCommand(mySP, myCN)

            myCMD.Connection.Open()
            myCMD.CommandTimeout = 180
            Dim myRDR As OleDb.OleDbDataReader = myCMD.ExecuteReader(CommandBehavior.CloseConnection)

            While myRDR.Read
                Dim Text As String
                Dim Value As Integer

                Text = myRDR(1)
                Value = myRDR(0)

                Me.ddlSites.Items.Add(New ListItem(Text, Value))
            End While

            'Loadd Assignee List
            UMLemployee = Core.data_select_value("SELECT intUMLEmployee FROM [portal.utilitymasters.co.uk].dbo.tblUsers WHERE intUserPK = '" & CurrentUserID & "'")

            If UMLemployee = 1 Then
                'Load EmployeeList
                Dim strSQL As String = "EXEC spAccMan_EmployeeList " & Me.ddlCompanies.SelectedValue
                Dim myConnection As New OleDb.OleDbConnection(Core.CurrentConnectionString)

                Dim myCommand As New OleDb.OleDbCommand(strSQL, myConnection)
                myCommand.Connection.Open()
                myCommand.CommandTimeout = 180
                Dim Reader As OleDb.OleDbDataReader = myCommand.ExecuteReader(CommandBehavior.CloseConnection)

                While Reader.Read
                    Dim Text As String
                    Dim Value As String

                    Text = Reader(1)
                    Value = Reader(0)

                    Me.ddlAssignee.Items.Add(New ListItem(Text, Value))
                End While
            Else
                With Me.ddlAssignee
                    .Items.Clear()
                    .Items.Add("[--Please Select An Assignee--]")
                    .Items.Add(New ListItem("Utility Masters Ltd", "332"))
                    .AppendDataBoundItems = True
                    .DataBind()
                End With
            End If
        End If
    End Sub

    Protected Sub ddlAttachments_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAttachments.Load
        If Not Page.IsPostBack Then
            ddlLoad()
        End If
    End Sub

    Protected Sub ddlLoad()
        'Loads the Documents saved to this call
        ddlAttachments.Items.Clear()
        ddlAttachments.Items.Add("[-- Attachment List --]")

        Dim strFolderName As String = strCompanyName & " - Activity Management"

        Dim strPath As String
        'strPath = Server.MapPath("Attachments") & "\" & strReference & "\"
        strPath = Core.CustomerDirectory & strCompanyName & "\" & strFolderName & "\" & intID & "\"

        If (System.IO.Directory.Exists(strPath)) Then
            ' Create a reference to the current directory.
            Dim FilePath As New DirectoryInfo(strPath)
            ' Create an array representing the files in the current directory.
            Dim fileName As FileInfo() = FilePath.GetFiles()
            Dim fileTemp As FileInfo
            For Each fileTemp In fileName
                ddlAttachments.Items.Add(fileTemp.Name)
            Next fileTemp
        End If
    End Sub

    Protected Sub btnAttach_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAttach.ServerClick

        If Not FileUpload1.PostedFile Is Nothing And FileUpload1.PostedFile.ContentLength > 0 Then

            Dim fn As String = System.IO.Path.GetFileName(FileUpload1.PostedFile.FileName)

            strCompanyName = ddlCompanies.SelectedItem.Text

            Dim strFolderName As String = strCompanyName & " - Activity Management"

            System.IO.Directory.CreateDirectory(Core.CustomerDirectory & strCompanyName & "\" & strFolderName & "\" & intID & "\")
            Dim SaveLocation As String = Core.CustomerDirectory & strCompanyName & "\" & strFolderName & "\" & intID & "\"

            'System.IO.Directory.CreateDirectory(Server.MapPath("Attachments") & "\" & strReference & "\")
            'Dim SaveLocation As String = Server.MapPath("Attachments") & "\" & strReference & "\"

            If (System.IO.Directory.Exists(SaveLocation)) Then
                'the path and file name to check for duplicates.
                Dim pathToCheck As String = SaveLocation & fn
                'Create a temporary file name to use for checking duplicates
                Dim tempfileName As String
                'Check for File Dulplicates
                If (System.IO.File.Exists(pathToCheck)) Then
                    Dim counter As Integer = 2
                    While (System.IO.File.Exists(pathToCheck))
                        'If File exists rename with Counter Pre-fix. 
                        tempfileName = counter.ToString() & "-" & fn
                        pathToCheck = SaveLocation + tempfileName
                        counter = counter + 1
                    End While
                    fn = tempfileName
                    'User Update, stating file name was changed.
                Else
                    'User Update, stating file name was Saved.
                End If
                SaveLocation += fn
                FileUpload1.SaveAs(SaveLocation)

                Core.data_execute_nonquery("INSERT INTO tblAccMan_Comments (intIssueFK, intUserID, varUserName, dtmDate, varComment) VALUES('" & CInt(intID) & "','" & CurrentUserID & "','" & CurrentUserName & "','" & Now() & "','ATT - " & fn & "')")
                ddlLoad()

            End If
        Else
        End If
    End Sub

    Protected Sub btnSelectAttachment_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSelectAttachment.ServerClick
        If pnlViewAttachments.Enabled = False Then
        Else
            Dim strchklist As String = ""
            Dim li As ListItem

            'Check if any items have been selected and assign value
            For Each li In Me.ddlAttachments.Items
                If li.Selected Then
                    strchklist += li.Text
                End If
            Next

            If strchklist.ToString = "" Or strchklist.ToString = "[-- Attachment List --]" Then
            Else

                Dim strFolderName As String = strCompanyName & " - Activity Management"

                'File Path
                'Dim strRequest As String = Server.MapPath("Attachments") & "\" & strReference & "\" & ddlAttachments.SelectedItem.Text
                Dim strRequest As String = Core.CustomerDirectory & strCompanyName & "\" & strFolderName & "\" & intID & "\" & ddlAttachments.SelectedItem.Text

                If strRequest <> "" Then
                    Dim path As String = strRequest
                    Dim file As System.IO.FileInfo = New System.IO.FileInfo(path)
                    'If file exists
                    If file.Exists Then
                        'Open Selected File
                        Response.Clear()
                        Response.AddHeader("Content-Disposition", "attachment; filename=" & file.Name)
                        Response.AddHeader("Content-Length", file.Length.ToString())
                        Response.ContentType = "application/octet-stream"
                        Response.WriteFile(file.FullName)
                        Response.End()
                    Else
                        'msgbox("This file does not exist.")
                    End If
                Else
                    'msgbox("Please provide a file to download.")
                End If
            End If
        End If
    End Sub

    'Protected Sub lstbAttachList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstbAttachList.SelectedIndexChanged
    '    Dim lstItem As String = lstbAttachList.SelectedItem.Text
    'End Sub

    Protected Sub btnRemove_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRemove.ServerClick

        strCompanyName = Me.ddlCompanies.SelectedItem.Text
        Dim strFolderName As String = strCompanyName & " - Activity Management"

        Try
            'Find File
            Dim TheFile As FileInfo = New FileInfo(Core.CustomerDirectory & strCompanyName & "\" & strFolderName & "\" & intID & "\" & Me.ddlAttachments.SelectedItem.Text) 'Me.lstbAttachList.SelectedItem.Text
            'Check if File Exists
            If TheFile.Exists Then
                'Remove file from Folder and Listbox
                File.Delete(Core.CustomerDirectory & strCompanyName & "\" & strFolderName & "\" & intID & "\" & Me.ddlAttachments.SelectedItem.Text) 'Me.lstbAttachList.SelectedItem.Text
                Core.data_execute_nonquery("UPDATE tblAccMan_Comments SET varComment = 'XXX - " & Me.ddlAttachments.SelectedItem.Text & "' WHERE intIssueFK = '" & intID & "' AND varComment = 'ATT - " & Me.ddlAttachments.SelectedItem.Text & "'")
                'Me.lstbAttachList.Items.Remove(Me.lstbAttachList.SelectedItem.Text)
                ddlLoad()

            Else
                Throw New FileNotFoundException()
            End If
        Catch ex As FileNotFoundException
            'msgbox(ex.Message)
        Catch ex As Exception
            'msgbox(ex.Message)
        End Try
    End Sub

    Protected Sub ddlIssue_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlIssue.SelectedIndexChanged
        If ddlIssue.SelectedItem.Text = "Other" Then
            Me.pnlNewIssueName.Visible = True
            Me.txtNoneListedIssue.Text = ""
        Else
            Me.pnlNewIssueName.Visible = False
            Me.txtNoneListedIssue.Text = ""
        End If
    End Sub

    Public Sub RemoveFolder()

        strCompanyName = Me.ddlCompanies.SelectedItem.Text
        Dim strFolderName As String = strCompanyName & " - Activity Management"

        Try
            For Each FileFound As String In Directory.GetFiles(Core.CustomerDirectory & strCompanyName & "\" & strFolderName & "\" & intID)
                File.Delete(FileFound)
            Next
            Directory.Delete(Core.CustomerDirectory & strCompanyName & "\" & strFolderName & "\" & intID)
            'Me.lstbAttachList.Items.Clear()
            ddlLoad()

        Catch ex As Exception
            'MsgBox(ex.Message)
        End Try
    End Sub

    Function RemoveCharacters(ByVal strText As String) As String
        'Check for unwanted Charatcers. 
        Dim illegalChars As Char() = "'_%*^".ToCharArray()
        Dim str As String = strText
        Dim sb As New System.Text.StringBuilder

        For Each ch As Char In str
            If Array.IndexOf(illegalChars, ch) = -1 Then
                sb.Append(ch)
            End If
        Next
        Return sb.ToString()
    End Function

    Public Sub ClearForm()
        RemoveFolder()
        Response.Redirect("ActivityList.aspx")
        'Clear User Input Fields.
    End Sub

    Protected Sub btnClear_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClear.ServerClick
        ClearForm()
    End Sub

    Protected Sub btnSave_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.ServerClick
        'Panel Visability
        If Me.pnlNewIssueName.Visible = True Then
            'Enable/disable required field validators.
            Me.RequiredFieldValidator4.Enabled = True
            Me.RequiredFieldValidator3.Enabled = False
        ElseIf Me.pnlNewIssueName.Visible = False Then
            Me.RequiredFieldValidator3.Enabled = True
            Me.RequiredFieldValidator4.Enabled = False
        End If

        If Page.IsValid Then
            'Check Page is Valid. 
            SaveIssue()
            EmailNotification()
            ''ClearForm()
            Response.Redirect("ActivityList.aspx")
        ElseIf Not Page.IsValid Then
        End If
        'Respnse.Redirect("ActivityList.aspx")
    End Sub

    Public Sub SaveIssue()
        Dim intNotify As Integer
        If chkbNotify.Checked = True Then
            intNotify = 1
            Core.data_execute_nonquery("INSERT INTO dbo.tblAccMan_SubscribeList (intIssueFK,intUserID,varUserEmail,intIssueAuthor,intNotification) VALUES ('" & Me.intID & "','" & CurrentUserID & "','" & CurrentEmail & "',1,'" & intNotify & "')")
        Else
            intNotify = 0
        End If

        If Me.pnlNewIssueName.Visible = True Then
            If chkbNonSiteSpecific.Checked = True Then
                Try
                    'Save: New Issue Name, Non Site Specific
                    Core.data_execute_nonquery("INSERT INTO tblAccMan_IssueLogs (intIssueID, intUserID, varUserName, varUserEmail, varStatus, dtmDate, intCompanyID, varCompanyName, intSiteID, varSiteName, varPriority, intIssueListID, varIssueName, varIssueBody, intAssigneeID, varAssigneeName, intNotify) VALUES('" & Me.intID & "','" & CurrentUserID & "','" & CurrentUserName & "','" & CurrentEmail & "','" & Me.strStatus & "','" & Me.dtmDateTime & "','" & Me.ddlCompanies.SelectedValue & "','" & Me.ddlCompanies.SelectedItem.Text & "',0,'Non Site Specific','" & Me.ddlPriority.SelectedItem.Text & "','" & Me.ddlIssue.SelectedValue & "','" & RemoveCharacters(Me.txtNoneListedIssue.Text) & "','" & RemoveCharacters(Me.txtIssue.Text) & "','" & Me.ddlAssignee.SelectedValue & "','" & Me.ddlAssignee.SelectedItem.Text & "','" & intNotify & "')")
                Catch ex As Exception
                End Try
            Else
                Try
                    'Save: New Issue Name, Site Specific
                    Core.data_execute_nonquery("INSERT INTO tblAccMan_IssueLogs (intIssueID, intUserID, varUserName, varUserEmail, varStatus, dtmDate, intCompanyID, varCompanyName, intSiteID, varSiteName, varPriority, intIssueListID, varIssueName, varIssueBody, intAssigneeID, varAssigneeName, intNotify) VALUES('" & Me.intID & "','" & CurrentUserID & "','" & CurrentUserName & "','" & CurrentEmail & "','" & Me.strStatus & "','" & Me.dtmDateTime & "','" & Me.ddlCompanies.SelectedValue & "','" & Me.ddlCompanies.SelectedItem.Text & "','" & Me.ddlSites.SelectedValue & "','" & Me.ddlSites.SelectedItem.Text & "','" & Me.ddlPriority.SelectedItem.Text & "','" & Me.ddlIssue.SelectedValue & "','" & RemoveCharacters(Me.txtNoneListedIssue.Text) & "','" & RemoveCharacters(Me.txtIssue.Text) & "','" & Me.ddlAssignee.SelectedValue & "','" & Me.ddlAssignee.SelectedItem.Text & "','" & intNotify & "')")
                Catch ex As Exception
                End Try
            End If
        Else
            If chkbNonSiteSpecific.Checked = True Then
                Try
                    'Save: Issue Name, Non Site Specific
                    Core.data_execute_nonquery("INSERT INTO tblAccMan_IssueLogs (intIssueID, intUserID, varUserName, varUserEmail, varStatus, dtmDate, intCompanyID, varCompanyName, intSiteID, varSiteName, varPriority, intIssueListID, varIssueName, varIssueBody, intAssigneeID, varAssigneeName, intNotify) VALUES('" & Me.intID & "','" & CurrentUserID & "','" & CurrentUserName & "','" & CurrentEmail & "','" & Me.strStatus & "','" & Me.dtmDateTime & "','" & Me.ddlCompanies.SelectedValue & "','" & Me.ddlCompanies.SelectedItem.Text & "',0,'Non Site Specific','" & Me.ddlPriority.SelectedItem.Text & "','" & Me.ddlIssue.SelectedValue & "','" & Me.ddlIssue.SelectedItem.Text & "','" & RemoveCharacters(Me.txtIssue.Text) & "','" & Me.ddlAssignee.SelectedValue & "','" & Me.ddlAssignee.SelectedItem.Text & "','" & intNotify & "')")
                Catch ex As Exception
                End Try
            Else
                Try
                    'Save: Issue Name, Site Specific
                    Core.data_execute_nonquery("INSERT INTO tblAccMan_IssueLogs (intIssueID, intUserID, varUserName, varUserEmail, varStatus, dtmDate, intCompanyID, varCompanyName, intSiteID, varSiteName, varPriority, intIssueListID, varIssueName, varIssueBody, intAssigneeID, varAssigneeName, intNotify) VALUES('" & Me.intID & "','" & CurrentUserID & "','" & CurrentUserName & "','" & CurrentEmail & "','" & Me.strStatus & "','" & Me.dtmDateTime & "','" & Me.ddlCompanies.SelectedValue & "','" & Me.ddlCompanies.SelectedItem.Text & "','" & Me.ddlSites.SelectedValue & "','" & Me.ddlSites.SelectedItem.Text & "','" & Me.ddlPriority.SelectedItem.Text & "','" & Me.ddlIssue.SelectedValue & "','" & Me.ddlIssue.SelectedItem.Text & "','" & RemoveCharacters(Me.txtIssue.Text) & "','" & Me.ddlAssignee.SelectedValue & "','" & Me.ddlAssignee.SelectedItem.Text & "','" & intNotify & "')")
                Catch ex As Exception
                End Try
            End If

        End If
    End Sub

    Public Sub EmailNotification()

        strMessage &= "<html>"
        strMessage &= "<head>"

        strMessage &= "<title>Utiliy Masters Ltd: ActivityManagement Notification</title>"
        strMessage &= "<style type='text/css' media='screen'>"
        strMessage &= "body {font-family: Verdana;}}"
        strMessage &= "h1, h2, h3, h4, h5, h6{font-family: 'Verdana', 'Helvetica', 'Arial', sans-serif}"
        strMessage &= "h1 {font: normal 2.29em; color:#082B61;}"
        strMessage &= "h2 {font: normal 1.89em; color:#082B61;}"
        strMessage &= "h3 {font: normal 1.19em; color:#6ebb1f;}"
        strMessage &= "h4 {font: normal 1em; color:#082B61;}"
        strMessage &= "h5 {font-size:12px; color:#082B61;}"
        strMessage &= "h6 {font-size:12px; color:#6ebb1f;}"

        strMessage &= ".HeaderGap{height: 5px; font-family: Verdana;}"
        strMessage &= ".MainBox{width: 755px; font-family: Verdana; margin: 0 auto; padding: 0px 10px 10px 10px;}"
        strMessage &= ".TableListOne{background-color: #EBEBEB; font-family: Verdana; margin: 0 auto; padding: 0px 10px 10px 10px;}"
        strMessage &= ".style1{font-weight: normal; font-family: Verdana;}"
        strMessage &= ".style2{text-align: center; font-weight: 700; font-family: Verdana;}"
        strMessage &= ".style3{text-align: center; font-family: Verdana;}"
        strMessage &= ".UMLBlue{color: #082B61; font-weight: bold;font-family: Verdana;}"
        strMessage &= "</style>"
        strMessage &= "</head>"

        strMessage &= "<body>"
        strMessage &= "<form id='form1'>"

        strMessage &= "<table>"
        strMessage &= "<tr><td class='style3'>"
        strMessage &= "<a title='UTILISYS Email Communication' href='http://www.utilitymasters.co.uk/page/carbon-reduction-commitment-service.aspx' target='_blank'><img title='Utility Masters Home' alt='Utility Masters Home' src='http://images.utilitymasters.co.uk/broadcasts/CRC_Seminars_19012010/branding-header.jpg' border='0'></a></td></tr>"
        strMessage &= "</td></tr>"
        strMessage &= "<td class='MainBox'>"
        strMessage &= "<table align='center' cellpadding='3'>"
        strMessage &= "<tr><td class='style2'><h2>Activity Management Notification</h2><br /></td></tr>"
        strMessage &= "<tr>"
        strMessage &= "<td><p>Dear '" & Me.ddlAssignee.SelectedItem.Text & "',</p></td>"
        strMessage &= "<tr><td><br /><br /></td></tr>"
        strMessage &= "<tr><td><p>You have received this notification in response to an Issue being assigned to you.</p></td></tr>"
        strMessage &= "</tr>"
        strMessage &= "</table>"

        strMessage &= "<table align='center' cellpadding='3'>"
        strMessage &= "<tr><td><p>Issue raised by: '" & CurrentUserName & "' </p></td></tr>"
        strMessage &= "<tr><td><p>Company: '" & Me.ddlCompanies.SelectedItem.Text & "' </p></td></tr>"
        strMessage &= "<tr><td><p>Site: '" & Me.ddlSites.SelectedItem.Text & "' </p></td></tr>"
        strMessage &= "<tr><td><p>Issue: '" & Me.ddlIssue.SelectedItem.Text & "' </p></td></tr>"
        strMessage &= "<tr><td><p>Issue-Details: '" & Me.txtIssue.Text & "' </p></td></tr>"
        strMessage &= "<tr><td><br /><br /></td></tr>"
        strMessage &= "</table>"

        strMessage &= "<table>"
        strMessage &= "<tr><td>Visit our main website, <a title='http://www.utilitymasters.co.uk' href='http://www.utilitymasters.co.uk' target='_blank'>http://www.utilitymasters.co.uk</a> and follow the my <span class='UMLBlue'>UTILI</span>SYS link.</td></tr>"
        strMessage &= "<tr><td>Or alternatively, <a title='https://portal.utilitymasters.co.uk' href='https://portal.utilitymasters.co.uk' target='_blank'>https://portal.utilitymasters.co.uk</a></td></tr>"
        strMessage &= "<tr><td><br /></td></tr>"
        strMessage &= "<tr><td><br /></td></tr>"
        strMessage &= "<tr><td><p>Thankyou for choosing <span class='UMLBlue'>Utility Masters Ltd</span>.</p></td></tr>"
        strMessage &= "<tr><td><br /></td></tr>"
        strMessage &= "<tr><td><br /></td></tr>"
        strMessage &= "<tr><td>"
        strMessage &= "<p>With Best Regards,</p>"
        strMessage &= "</td></tr>"
        strMessage &= "<tr><td><br /></td></tr>"
        strMessage &= "</table>"

        strMessage &= "<table>"
        strMessage &= "<tr>"
        strMessage &= "<td>"
        strMessage &= "<p><span class='UMLBlue'>Utility Masters Ltd</span><br />"
        strMessage &= "UML House, Salmon Fields Business Village, Royton, OL2 6HT<br />"
        strMessage &= "t. 0161 785 0404 <br />"
        strMessage &= "f. 0161 785 7969 <br />"
        strMessage &= "e. <a href='mailto:support@utilitymasters.co.uk?subject=UTILISYS Account Enquiry' target='_blank'>support@utilitymasters.co.uk</a></p>"
        strMessage &= "<br />"
        strMessage &= "</td>"
        strMessage &= "</tr>"
        strMessage &= "<tr>"
        strMessage &= "<td>"
        strMessage &= "<p>This is an automated Email Notification.  Please do not reply to this Email.</p>"
        strMessage &= "<br />"
        strMessage &= "</td>"
        strMessage &= "</tr>"
        strMessage &= "</table>"
        strMessage &= "</form>"
        strMessage &= "</body>"
        strMessage &= "</html>"

        If Me.UMLemployee = 1 Then
            'UML Employee
            Dim intUMLEmployee As String = Core.data_select_value("SELECT intUMLEmployee FROM tblUsers WHERE intUserPK = '" & Me.ddlAssignee.SelectedValue & "'")

            If intUMLEmployee = "1" Then
                'UML Employee
                strTo = Core.data_select_value("SELECT varEmail FROM tblUsers WHERE intUserPK = '" & Me.ddlAssignee.SelectedValue & "' AND intUMLEmployee =  1")
            ElseIf intUMLEmployee = "0" Then
                'None UML Employee
                strTo = Core.data_select_value("SELECT varEmail FROM tblUsers WHERE intUserPK = '" & Me.ddlAssignee.SelectedValue & "' AND intUMLEmployee =  0")
            ElseIf intUMLEmployee = Nothing Then
                'UML Employee Not in tblUsers. Check tblEmployee. 
                strTo = Core.data_select_value("SELECT Email FROM UML_CMS.dbo.tblemployees WHERE employeeid = '" & Me.ddlAssignee.SelectedValue & "'")
            End If

        ElseIf Me.UMLemployee = 0 Then
            'Not UML Employee
            strTo = "activitymanagement@utilitymasters.co.uk"
        End If

        '!!! UPDATE THIS VALUE TO YOUR EMAIL ADDRESS
        Const strFrom As String = "activitymanagement@utilitymasters.co.uk"

        '(1) Create the MailMessage instance
        Dim mm As New MailMessage(strFrom, strTo)

        '(2) Assign the MailMessage's properties
        mm.Subject = strSubject
        mm.Body = strMessage
        mm.IsBodyHtml = True

        '(3) Create the SmtpClient object
        Dim smtp As New SmtpClient

        '(4) Send the MailMessage (will use the Web.config settings)
        smtp.Send(mm)

        'clsMail.SendEmail(strFrom, strTo, strSubject, strMessage)
    End Sub

    Protected Sub btnViewAttachment_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewAttachment.ServerClick
        If pnlViewAttachments.Visible = False Then

            If Me.ddlCompanies.SelectedItem.Text = "[--Please Select A Company--]" Then
                Me.lblErrorMsg2.Visible = True
                Me.lblErrorMsg2.Text = "*Please Select a Company."
            Else
                Me.pnlViewAttachments.Visible = True
                Me.lblErrorMsg2.Visible = False
            End If
        ElseIf Me.pnlViewAttachments.Visible = True Then
            Me.lblErrorMsg2.Visible = False
            Me.pnlViewAttachments.Visible = False
        End If
    End Sub

    Protected Sub btnSiteSearch_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSiteSearch.ServerClick

        If Me.ddlCompanies.SelectedItem.Text = "[--Please Select A Company--]" Then
            '...
            Me.lblErrorMsg1.Visible = True
            Me.lblErrorMsg1.Text = "*Please Select a Company."
        Else
            Me.lblErrorMsg1.Visible = False
            If Me.pnlViewSearch.Visible = False Then
                Me.pnlViewSearch.Visible = True
            ElseIf Me.pnlViewSearch.Visible = True Then
                Me.ddlCondition.SelectedItem.Selected = False
                Me.ddlCondition.Items.FindByText("[--Select A Condition--]").Selected = True
                Me.txtSearchText.Text = ""
                Me.lstbSiteSearchList.Items.Clear()
                Me.lstbSiteSearchList.DataSource = ""
                Me.lstbSiteSearchList.DataBind()
                Me.pnlViewSearch.Visible = False
            End If
        End If

    End Sub

    Protected Sub btnSearch_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.ServerClick
        Dim strCondition As String = ""

        If Me.ddlCompanies.SelectedItem.Text = "[--Please Select A Company--]" Then
            '...
        Else
            If Me.ddlCondition.SelectedItem.Text = "[--Select A Condition--]" Then
                '...
            ElseIf Me.ddlCondition.SelectedItem.Text = "UML Reference" Then
                strCondition = "SELECT UML_CMS.dbo.tblsites.siteid, UML_CMS.dbo.tblsites.sitename FROM UML_CMS.dbo.tblsites WHERE UML_CMS.dbo.tblsites.siteID = '" & Me.txtSearchText.Text & "' AND custid = '" & Me.ddlCompanies.SelectedValue & "'"
            ElseIf Me.ddlCondition.SelectedItem.Text = "Customer Reference" Then
                strCondition = "SELECT UML_CMS.dbo.tblsites.siteid, UML_CMS.dbo.tblsites.sitename FROM UML_CMS.dbo.tblsites WHERE UML_CMS.dbo.tblsites.Customersiteref = '" & Me.txtSearchText.Text & "' AND custid = '" & Me.ddlCompanies.SelectedValue & "'"
            ElseIf Me.ddlCondition.SelectedItem.Text = "Site Name" Then
                strCondition = "SELECT UML_CMS.dbo.tblsites.siteid, UML_CMS.dbo.tblsites.sitename FROM UML_CMS.dbo.tblsites WHERE UML_CMS.dbo.tblsites.sitename LIKE '%" & Me.txtSearchText.Text & "%' AND custid = '" & Me.ddlCompanies.SelectedValue & "'"
            ElseIf Me.ddlCondition.SelectedItem.Text = "MPAN" Then
                strCondition = "SELECT UML_CMS.dbo.tblsites.siteid, UML_CMS.dbo.tblsites.sitename FROM UML_CMS.dbo.tblsitempans INNER JOIN UML_CMS.dbo.tblsites ON UML_CMS.dbo.tblsites.siteid = UML_CMS.dbo.tblsitempans.FKsite_ID INNER JOIN UML_CMS.dbo.tblcustomer ON UML_CMS.dbo.tblcustomer.Custid = UML_CMS.dbo.tblsites.custid WHERE tblsitempans.distribution + tblsitempans.supplier = '" & Me.txtSearchText.Text & "' AND UML_CMS.dbo.tblsites.custid = '" & Me.ddlCompanies.SelectedValue & "'"
            ElseIf Me.ddlCondition.SelectedItem.Text = "MNumber" Then
                strCondition = "SELECT UML_CMS.dbo.tblsites.siteid, UML_CMS.dbo.tblsites.sitename FROM UML_CMS.dbo.tblsites INNER JOIN UML_CMS.dbo.tblmnumbers ON UML_CMS.dbo.tblmnumbers.siteID = UML_CMS.dbo.tblsites.siteid WHERE UML_CMS.dbo.tblmnumbers.Mnumber = '" & Me.txtSearchText.Text & "' AND UML_CMS.dbo.tblsites.custid = '" & Me.ddlCompanies.SelectedValue & "'"
            Else
            End If
        End If

        With Me.lstbSiteSearchList
            .Items.Clear()
            .AppendDataBoundItems = True
            .DataSource = Core.data_select(strCondition)
            .DataValueField = "siteid"
            .DataTextField = "sitename"
            .DataBind()
        End With

    End Sub

    Protected Sub lstbSiteSearchList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstbSiteSearchList.SelectedIndexChanged
        Me.ddlSites.SelectedItem.Selected = False
        Me.ddlSites.Items.FindByText(lstbSiteSearchList.SelectedItem.Text).Selected = True
        Me.ddlCondition.SelectedItem.Selected = False
        Me.ddlCondition.Items.FindByText("[--Select A Condition--]").Selected = True
        Me.pnlViewSearch.Visible = False
        Me.txtSearchText.Text = ""
        Me.lstbSiteSearchList.Items.Clear()
        Me.lstbSiteSearchList.DataSource = ""
        Me.lstbSiteSearchList.DataBind()

    End Sub

    Protected Sub chkbNonSiteSpecific_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkbNonSiteSpecific.CheckedChanged
        If Me.chkbNonSiteSpecific.Checked = True Then
            Me.RequiredFieldValidator7.Enabled = False
            Me.btnSiteSearch.Disabled = True

            'Clear Site List - Display purpose Only
            With Me.ddlSites
                .Items.Clear()
                .Items.Add("[--Please Select A Site--]")
            End With

            Me.ddlSites.Enabled = False
        Else
            Me.RequiredFieldValidator7.Enabled = True
            Me.btnSiteSearch.Disabled = False

            'Get Site List - Repopulate Site List
            With Me.ddlSites
                .Items.Clear()
                .Items.Add("[--Please Select A Site--]")
            End With

            Dim mySP As String = "EXEC spAccMan_SitesDisplay " & MySession.UserID & ", " & Me.ddlCompanies.SelectedValue
            Dim myCN As New OleDb.OleDbConnection(Core.CurrentConnectionString)

            Dim myCMD As New OleDb.OleDbCommand(mySP, myCN)

            myCMD.Connection.Open()
            myCMD.CommandTimeout = 180
            Dim myRDR As OleDb.OleDbDataReader = myCMD.ExecuteReader(CommandBehavior.CloseConnection)

            While myRDR.Read
                Dim Text As String
                Dim Value As Integer

                Text = myRDR(1)
                Value = myRDR(0)

                Me.ddlSites.Items.Add(New ListItem(Text, Value))
            End While

            Me.ddlSites.Enabled = True
        End If
    End Sub
End Class
