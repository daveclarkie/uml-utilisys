<%@ Page Language="VB" MasterPageFile="~/landing.master" AutoEventWireup="false" CodeFile="Default.aspx.vb" Inherits="myapps_MyActivityManagement_Default" title="Activity Management: Add" %>
<asp:Content ID="Content1" ContentPlaceHolderID="content_right" Runat="Server">
    
<form id="Form1" runat="server">
    <div class="content_back general">
    
        <table>
            <tr>
                <td>
                    <asp:Label ID="Label1" runat="server" Text="Details:" Font-Names="Calibri" ForeColor="Highlight" Width="150px"></asp:Label>
                    <asp:Label ID="lblReference" runat="server" Font-Names="Calibri" Width="250px" ToolTip="Issue Reference" Font-Size="14px" BackColor="Transparent" BorderStyle="None" Height="20px"></asp:Label>
                </td>
                <td>    
                    <a id="btnClear" class="btnClear" runat="server" title="Cancel"></a>
                </td>
            </tr>
        </table><!--ID Details-->
                                
        <table>
            <tr>
                <td style="height: 21px">
                    <asp:Label ID="Label11" runat="server" Font-Names="Calibri" ForeColor="Highlight" Width="150px" Text="My Company:"></asp:Label>
                    <asp:DropDownList ID="ddlCompanies" runat="server" Font-Names="Calibri" Width="305px" AutoPostBack="True" > 
                        <asp:ListItem>[--Please Select A Company--]</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*" ControlToValidate="ddlCompanies" EnableTheming="True" InitialValue="[--Please Select A Company--]" SetFocusOnError="True" Font-Bold="True" Font-Size="20px"></asp:RequiredFieldValidator>
                </td>
            </tr>
        </table><!--My Customers-->
        
        <table>
            <tr>
                <td style="height: 21px">
                    <asp:Label ID="Label6" runat="server" Font-Names="Calibri" ForeColor="Highlight" Width="150px" Text="My Sites:"></asp:Label>
                    <asp:DropDownList ID="ddlSites" runat="server" Font-Names="Calibri" Width="305px"> 
                    <asp:ListItem>[--Please Select A Site--]</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="*" ControlToValidate="ddlSites" EnableTheming="True" InitialValue="[--Please Select A Site--]" SetFocusOnError="True" Font-Bold="True" Font-Size="20px"></asp:RequiredFieldValidator>
                </td>
            </tr>
        </table><!--My Sites-->
        
        <table>
            <tr>
                <td>
                    <asp:Label ID="Label14" runat="server" Font-Names="Calibri" ForeColor="Highlight" Width="150px" Text=""></asp:Label>
                    <asp:CheckBox ID="chkbNonSiteSpecific" runat="server" Checked="False" Text=" *Non Site Specific Issue" Font-Names="Calibri" ForeColor="Highlight" Width="305px" AutoPostBack="True"/>
                </td>
            </tr>
        </table><!--Non Site Specific-->
        
        <table>
            <tr>
                <td>
                    <a id="btnSiteSearch" class="btnSiteSearch" runat="server" title="Find A Site"></a>
                </td>
                <td>
                    <asp:Label ID="lblErrorMsg1" runat="server" Text="" Font-Names="Calibri" ForeColor="Red" Font-Bold="true" Visible="False"></asp:Label>
                </td>
            </tr>    
        </table>
        
        <asp:Panel ID="pnlViewSearch" style="margin-bottom: 10px; padding-top: 5px; background-color:#FFFFC0;" Width="650px" runat="server" Height="110px" HorizontalAlign="left" BorderStyle="Double" BorderWidth="1px" BorderColor="lightgray" Visible="False">
             <table style="margin-left: auto; margin-right: auto;">
                <tr>
                    <td>
                        <asp:Label ID="Label4" runat="server" Text="Condition:" Font-Names="Calibri" ForeColor="Highlight" Width="50px" style="padding-right: 22px;"></asp:Label>
                        <asp:DropDownList ID="ddlCondition" runat="server" Font-Names="Calibri" Width="255px" ToolTip="Condition" Font-Size="14px">
                            <asp:ListItem>[--Select A Condition--]</asp:ListItem>
                            <asp:ListItem>UML Reference</asp:ListItem>
                            <asp:ListItem>Customer Reference</asp:ListItem>
                            <asp:ListItem>Site Name</asp:ListItem>
                            <asp:ListItem>MPAN</asp:ListItem>
                            <asp:ListItem>MNumber</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                     <td>
                        <asp:Label ID="Label2" runat="server" Text="Search:" Font-Names="Calibri" ForeColor="Highlight" Width="50px" style="padding-right: 22px;"></asp:Label>
                        <asp:TextBox ID="txtSearchText" runat="server" Font-Names="Calibri" Width="250px" ToolTip="Search Criteria" Font-Size="14px"></asp:TextBox>
                     </td>
                     <td>
                        <a id="btnSearch" class="btnSearch" runat="server" title="Search" style="padding-left: 20px;"></a>
                     </td>
                </tr>
                <tr>
                    <td> 
                        <asp:Label ID="Label3" runat="server" Text="Results:" Font-Names="Calibri" ForeColor="Highlight" Width="50px" style="padding-right: 22px;"></asp:Label>
                        <asp:ListBox ID="lstbSiteSearchList" CssClass="ListBoxMedium" runat="server" Font-Names="Calibri" Width="255px" Height="50px" ToolTip="Search Criteria" Font-Size="14px" AutoPostBack="True"></asp:ListBox>
                    </td>
                </tr>
             </table>
        </asp:Panel>

        <table>
            <tr>
                <td>
                    <asp:Label ID="Label5" runat="server" Text="Priority Level:" Font-Names="Calibri" ForeColor="Highlight" Width="150px"></asp:Label>
                    <asp:DropDownList ID="ddlPriority" runat="server" Font-Names="Calibri" Width="305px" ToolTip="Level of Urgency" Font-Size="14px">
                        <asp:ListItem>[--Select A Priority Level--]</asp:ListItem>
                        <asp:ListItem>High</asp:ListItem>
                        <asp:ListItem>Medium</asp:ListItem>
                        <asp:ListItem>Low</asp:ListItem>
                    </asp:DropDownList>    
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*" ControlToValidate="ddlPriority" EnableTheming="True" InitialValue="[--Select A Priority Level--]" SetFocusOnError="True" Font-Bold="True" Font-Size="18px"></asp:RequiredFieldValidator>
                </td>
            </tr>
        </table><!--Priority-->
        
        <table>
            <tr>
                <td>
                    <asp:Label ID="Label7" runat="server" Font-Names="Calibri" ForeColor="Highlight" Width="150px" Text="My Issue:"></asp:Label>
                    <asp:DropDownList ID="ddlIssue" runat="server" Font-Names="Calibri" Width="305px" AutoPostBack="true">
                        <asp:ListItem>[--Please Select An Issue--]</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="*" ControlToValidate="ddlIssue" EnableTheming="True" InitialValue="[--Please Select An Issue--]" SetFocusOnError="True" Font-Bold="True" Font-Size="18px"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="pnlNewIssueName" runat="server" Visible="False">
                        <asp:Label ID="Label10" runat="server" Text="New Issue Name:" Font-Names="Calibri" ForeColor="Highlight" Width="150px"></asp:Label>
                        <asp:TextBox ID="txtNoneListedIssue" runat="server" Font-Names="Calibri" Width="300px" ToolTip="Enter a New Issue Name" Font-Size="14px"></asp:TextBox>
                    </asp:Panel>                           
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="*" ControlToValidate="txtNoneListedIssue" EnableTheming="True" InitialValue="" SetFocusOnError="True" Font-Bold="True" Font-Size="18px"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="label12" runat="server" Text="Issue-Body:" Font-Names="Calibri" ForeColor="Highlight" Width="150px"></asp:Label>
                    <asp:TextBox ID="txtIssue" runat="server" TextMode="MultiLine" Width="300px" Height="50px" ToolTip="Enter the Issue Body here." Font-Names="Calibri" Font-Size="14px"></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*" ControlToValidate="txtIssue" EnableTheming="True" InitialValue="" SetFocusOnError="True" Font-Bold="True" Font-Size="18px"></asp:RequiredFieldValidator>
                </td>                                                                
            </tr>
        </table><!--Issue Details-->
        
        <table>
            <tr>
                <td>
                    <asp:Label ID="label13" runat="server" Text="Assign Issue:" Font-Names="Calibri" ForeColor="Highlight" Width="150px"></asp:Label>
                    <asp:DropDownList ID="ddlAssignee" runat="server" Font-Names="Calibri" Width="305px" AutoPostBack="true">
                        <asp:ListItem>[--Please Select An Assignee--]</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="*" ControlToValidate="ddlAssignee" EnableTheming="True" InitialValue="[--Please Select An Assignee--]" SetFocusOnError="True" Font-Bold="True" Font-Size="18px"></asp:RequiredFieldValidator>
                </td>  
            </tr>
        </table><!--Assignment-->
        
        <table>
            <tr>
                <td>
                    <a id="btnViewAttachment" class="btnViewAttachment" runat="server" title="View Attachments Panel"></a>
                </td>
                <td>
                    <asp:Label ID="lblErrorMsg2" runat="server" Text="" Font-Names="Calibri" ForeColor="Red" Font-Bold="true" Visible="False"></asp:Label>
                </td>
            </tr>
        </table>
        
        <asp:Panel ID="pnlViewAttachments" style="margin-bottom: 10px; background-color:#FFFFC0;" Width="650px" runat="server" Height="60px" HorizontalAlign="left" BorderStyle="Double" BorderWidth="1px" BorderColor="lightgray" Visible="False">
            <table style="margin-left: auto; margin-right: auto;">
                <tr>
                    <td>
                        <asp:Label ID="label8" runat="server" Text="Attach:" Font-Names="Calibri" ForeColor="Highlight" Width="60px"></asp:Label>
                    </td>
                    <td>  
                        <asp:FileUpload ID="FileUpload1" runat="server" Width="250px" Font-Size="14px" />
                    </td>
                    <td>
                       <a id="btnAttach" class="btnAttach" runat="server" title="Add Attachment"></a>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label16" runat="server" Text="Open:" Font-Names="Calibri" ForeColor="Highlight" Width="60px"></asp:Label> 
                    </td>
                    <td>  
                        <asp:DropDownList ID="ddlAttachments" runat="server" Font-Names="Calibri" Width="250px" ToolTip="Select an Attachment from the list to view." AutoPostBack="True">
                            <asp:ListItem>[-- Attachment List --]</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td>
                        <a id="btnSelectAttachment" class="btnSelectAttachment" runat="server" title="View Selected Attachment"></a>
                     </td>
                    <td>
                        <a id="btnRemove" class="btnRemove" runat="server" title="Remove Attachment"></a>
                    </td>
                </tr>
            </table><!-- Add Attachment -->
        </asp:Panel>                                          
       
                                
        <table>
            <tr>
                <td>
                    <asp:Label ID="Label9" runat="server" Text="Keep me updated:" Font-Names="Calibri" ForeColor="Highlight" Width="150px"></asp:Label>
                    <asp:CheckBox ID="chkbNotify" runat="server" Text="" Font-Names="Calibri" ForeColor="Highlight" Width="10px" ToolTip="Keeping you updated when a solution/comment is made on your question." Checked="True"/>
                </td>
            </tr>
        </table><!-- Email Notification -->

        <table>
            <tr>
                <td>
                    <a id="btnSave" class="btnSave" runat="server" title="Save"></a>
                </td>
            </tr>
        </table><!-- Add to Database -->

       </div>
</form>

</asp:Content>

