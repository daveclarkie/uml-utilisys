<%@ Page Language="VB" MasterPageFile="~/landing.master" AutoEventWireup="false" CodeFile="ActivityList.aspx.vb" Inherits="myapps_MyActivityManagement_ActivityList" title="Activity Management: Home" %>
<asp:Content ID="Content1" ContentPlaceHolderID="content_right" Runat="Server">
<form id="Form1" runat="server">
    <div class="content_back general">
    
        <table style="margin-left: auto; margin-right: auto;">
            <tr>
                <td>
                    <asp:Label ID="Label8" runat="server" Text="Apply A Filter:" Font-Names="Calibri" ForeColor="Highlight" Width="120px"></asp:Label> 
                    <asp:DropDownList ID="ddlFilterList" runat="server" Font-Names="Calibri" Width="250px" ToolTip="Select A Filter" AutoPostBack="true">
                        <asp:ListItem>[--Please Select A Filter--]</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td>
                    <a id="lbtnCalendar" class="lbtnCalendar" runat="server" title="Open Calendar"></a>
                </td>
                <td><a id="lbtnRefresh" class="lbtnRefresh" runat="server" title="Refresh"></a></td>
            </tr>
        </table><!--Filter Selection-->


        <asp:Calendar ID="Calendar1" runat="server" Font-Size="8pt" Width="250px" 
        onselectionchanged="Date_Selected" style="margin:5px auto 5px auto;" Visible="false">
            <TitleStyle BackColor="#009530" Font-Bold="True" ForeColor="#FFFFFF" HorizontalAlign="Center" />
            <DayHeaderStyle BackColor="#009530" ForeColor="#FFFFFF" />
            <SelectedDayStyle BackColor="#009530" ForeColor="#FFFFFF" BorderStyle="Solid" BorderWidth="1px" BorderColor="Black"  />
        </asp:Calendar>
             
        <asp:SqlDataSource ID="SqlDSCalendar1" runat="server"></asp:SqlDataSource>


<hr />
         <asp:Panel ID="Panel1" runat="server" style="overflow:auto; margin-bottom: 10px;" Width="655px" Height="400px" HorizontalAlign="Left" BorderStyle="Double" BorderWidth="1px" BorderColor="#009530">
            <asp:GridView ID="grdvwIssueList" runat="server" AutoGenerateColumns="False" EmptyDataText="There are no data records to display."
                Font-Names="Verdana" Font-Size="10px" CellPadding="5" CellSpacing="5" HeaderStyle-ForeColor="white" HeaderStyle-BackColor="#009530" AllowSorting="True"
                OnSorting="grdvwIssueList_Sorting" Width="635px">
                    <Columns>
                        <asp:BoundField DataField="intIssuePk" HeaderText="IssuePk:" ReadOnly="True" SortExpression="intIssuePk"></asp:BoundField>
                        <asp:BoundField DataField="intIssueID" HeaderText="IssueID:" SortExpression="intIssueID">
                            <ItemStyle Width="55px" ForeColor="Black" />
                        </asp:BoundField>
                        <asp:BoundField DataField="intUserID" HeaderText="UserID:" SortExpression="intUserID"></asp:BoundField>
                        <asp:BoundField DataField="dtmDate" HeaderText="Date:" SortExpression="dtmDate"></asp:BoundField>
                        <asp:BoundField DataField="dtmModified" HeaderText="Date:" SortExpression="dtmModified"></asp:BoundField>
                        <asp:BoundField DataField="varUserName" HeaderText="UserName:" SortExpression="varUserName"></asp:BoundField>
                        <asp:BoundField DataField="varUserEmail" HeaderText="UserEmail:" SortExpression="varUserEmail"></asp:BoundField>
                        <asp:BoundField DataField="varStatus" HeaderText="Status:" SortExpression="varStatus"></asp:BoundField>
                        <asp:BoundField DataField="intCompanyID" HeaderText="CompanyID:" SortExpression="intCompanyID"></asp:BoundField>
                        <asp:BoundField DataField="varCompanyName" HeaderText="CompanyName:" SortExpression="varCompanyName">
                            <ItemStyle Width="160px" ForeColor="Black" />
                        </asp:BoundField>
                        <asp:BoundField DataField="intSiteID" HeaderText="SiteID:" SortExpression="intSiteID"></asp:BoundField>
                        <asp:BoundField DataField="varSiteName" HeaderText="SiteName:" SortExpression="varSiteName">
                            <ItemStyle Width="160px" ForeColor="Black" />
                        </asp:BoundField>
                        <asp:BoundField DataField="varPriority" HeaderText="Priority:" SortExpression="varPriority"></asp:BoundField>
                        <asp:BoundField DataField="intIssueListID" HeaderText="IssueListID:" SortExpression="intIssueListID"></asp:BoundField>
                        <asp:BoundField DataField="varIssueName" HeaderText="IssueName:" SortExpression="varIssueName">
                            <ItemStyle Width="150px" ForeColor="Black" />
                        </asp:BoundField>
                        <asp:BoundField DataField="varIssueBody" HeaderText="IssueBody:" SortExpression="varIssueBody">
                            <ItemStyle Width="100px" ForeColor="Black" />
                        </asp:BoundField>
                        <asp:BoundField DataField="intAssigneeID" HeaderText="AssigneeID:" SortExpression="intAssigneeID"></asp:BoundField>
                        <asp:BoundField DataField="varAssigneeName" HeaderText="AssigneeName:" SortExpression="varAssigneeName"></asp:BoundField>
                        <asp:BoundField DataField="intNotify" HeaderText="Notify:" SortExpression="intNotify"></asp:BoundField>
                        <asp:ButtonField Text="View" CommandName="View" ButtonType="Button">
                            <ItemStyle Width="80px"/>
                            <ControlStyle Font-Bold="True" Font-Names="Verdana" Font-Size="12px" ForeColor="#082B61" Width="80px" />
                        </asp:ButtonField>
                    </Columns>
                </asp:GridView>
                
                 <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
                     SelectCommand="SELECT [intIssuePk], [intIssueID], [intUserID], [dtmDate], [dtmModified], [varUserName], [varUserEmail], [varStatus], [intCompanyID], [varCompanyName], [intSiteID], [varSiteName], [varPriority], [intIssueListID], [varIssueName], [varIssueBody], [intAssigneeID], [varAssigneeName], [intNotify] FROM [tblAccMan_IssueLogs]">
                 </asp:SqlDataSource>                                    
         </asp:Panel>
     </div>
</form>

</asp:Content>

