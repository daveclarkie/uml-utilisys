
Partial Class myapps_MyActivityManagement_ActivityList
    Inherits System.Web.UI.Page

    Dim CurrentUserID As String
    Dim CurrentUserName As String
    Dim CurrentEmail As String

    Dim UMLemployee As Integer


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Page.MaintainScrollPositionOnPostBack = True

        CurrentUserID = Core.data_select_value("SELECT intUserPK FROM [portal.utilitymasters.co.uk].dbo.tblUsers WHERE intUserPK = " & MySession.UserID)
        CurrentUserName = Core.data_select_value("SELECT varForename + ' ' + varSurname FROM [portal.utilitymasters.co.uk].dbo.tblUsers WHERE intUserPK = " & MySession.UserID)
        CurrentEmail = Core.data_select_value("SELECT varEmail FROM [portal.utilitymasters.co.uk].dbo.tblUsers WHERE intUserPK = " & MySession.UserID)

        'Loadd Assignee List
        'UMLemployee = Core.data_select_value("SELECT firstname + ' ' + lastname FROM UML_CMS.dbo.tblemployees WHERE email = '" & CurrentEmail & "'")

        UMLemployee = Core.data_select_value("SELECT intUMLEmployee FROM [portal.utilitymasters.co.uk].dbo.tblUsers WHERE intUserPK = '" & CurrentUserID & "'")

        grdvwIssueList.Columns(0).Visible = False        'intIssuePk()
        'grdvwIssueList.Columns(1).Visible = False       'intIssueID()
        grdvwIssueList.Columns(2).Visible = False        'intUserID()
        grdvwIssueList.Columns(3).Visible = False        'dtmDate()
        grdvwIssueList.Columns(4).Visible = False        'dtmModified
        grdvwIssueList.Columns(5).Visible = False        'varUserName()
        grdvwIssueList.Columns(6).Visible = False        'varUserEmail()
        grdvwIssueList.Columns(7).Visible = False        'varStatus()
        grdvwIssueList.Columns(8).Visible = False        'intCompanyID()
        'grdvwIssueList.Columns(9).Visible = False       'varCompanyName()
        grdvwIssueList.Columns(10).Visible = False       'intSiteID()
        'grdvwIssueList.Columns(11).Visible = False      'varSiteName()
        grdvwIssueList.Columns(12).Visible = False       'varPriority()
        grdvwIssueList.Columns(13).Visible = False       'intIssueListID()
        'grdvwIssueList.Columns(14).Visible = False      'varIssueName()
        grdvwIssueList.Columns(15).Visible = False       'varIssueBody()
        grdvwIssueList.Columns(16).Visible = False       'intAssigneeID()
        grdvwIssueList.Columns(17).Visible = False       'varAssigneeName()
        grdvwIssueList.Columns(18).Visible = False       'intNotify()

        If Not Me.Page.IsPostBack Then
            If UMLemployee = 1 Then 'UML Employee
                grdvwIssueList.DataSourceID = ""
                Dim mySP As String = "SELECT * FROM tblAccMan_IssueLogs ORDER BY intIssuePK"
                Dim myCN As New OleDb.OleDbConnection(Core.CurrentConnectionString)
                Dim myCMD As New OleDb.OleDbCommand(mySP, myCN)
                Dim myDA As New OleDb.OleDbDataAdapter
                Dim myDS As New System.Data.DataSet
                myDA.SelectCommand = myCMD
                myDA.Fill(myDS)
                grdvwIssueList.DataSource = myDS.Tables(0)
                grdvwIssueList.DataBind()
            Else
                Dim mySP As String = "EXEC spAccMan_SitesDisplay2 " & MySession.UserID
                Dim myCN As New OleDb.OleDbConnection(Core.CurrentConnectionString)
                Dim myCMD As New OleDb.OleDbCommand(mySP, myCN)
                Dim myDA As New OleDb.OleDbDataAdapter
                Dim myDS As New System.Data.DataSet
                myDA.SelectCommand = myCMD
                myDA.Fill(myDS)
                grdvwIssueList.DataSource = myDS.Tables(0)
                grdvwIssueList.DataBind()
            End If

            'Load Filter List
            With Me.ddlFilterList
                .Items.Clear()
                .Items.Add("[--Please Select A Filter--]")
                .AppendDataBoundItems = True
                .DataSource = Core.data_select("SELECT * FROM tlkpAccMan_FilterList")
                .DataValueField = "varSQL"
                .DataTextField = "varFiltername"
                .DataBind()
            End With
        End If

    End Sub

    'Protected Sub grdvwIssueList_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdvwIssueList.PageIndexChanging
    '    'Gridview Paging 
    '    grdvwIssueList.PageIndex = e.NewPageIndex
    '    grdvwIssueList.DataBind()
    'End Sub

    Protected Sub grdvwIssueList_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdvwIssueList.RowCommand
        'Gridview Commands
        If e.CommandName = "View" Then
            Dim rowIndex As Integer = Integer.Parse(e.CommandArgument.ToString())
            Session("ID") = grdvwIssueList.Rows(rowIndex).Cells(1).Text
            Response.Redirect("ActivityViewer.aspx")
            'Server.Transfer("", True)
        End If
    End Sub

    Protected Sub grdvwIssueList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdvwIssueList.RowDataBound
        'Adding Footer, with Page Numbers
        'If e.Row.RowType = DataControlRowType.Footer Then
        '    e.Row.Cells(1).Text = "Page " & (grdvwIssueList.PageIndex + 1) & " of " & grdvwIssueList.PageCount
        'End If

        If e.Row.RowType = DataControlRowType.DataRow Then
            If (e.Row.DataItem("varStatus") = "Open") Then
                e.Row.BackColor = Drawing.Color.Red
            ElseIf (e.Row.DataItem("varStatus") = "Pending") Then
                e.Row.BackColor = Drawing.Color.LightYellow
            ElseIf (e.Row.DataItem("varStatus") = "Closed") Then
                e.Row.BackColor = Drawing.Color.LightBlue
            End If
        End If

    End Sub

    Protected Sub grdvwIssueList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdvwIssueList.Sorting
        'Sort Direction of GridView Prt 1 
        Dim dataTable As DataTable = TryCast(grdvwIssueList.DataSource, DataTable)

        If Not dataTable Is Nothing Then
            Dim dataView As DataView = New DataView(dataTable)
            dataView.Sort = e.SortExpression & " " & ConvertSortDirectionToSql(e.SortDirection)

            grdvwIssueList.DataSource = dataView
            grdvwIssueList.DataBind()
        End If
    End Sub

    Private Function ConvertSortDirectionToSql(ByVal Direciton As SortDirection) As String
        'Sort Direction of GridView Prt 2
        Dim newSortDirection As String = String.Empty
        Select Case Direciton
            Case SortDirection.Ascending
                newSortDirection = "ASC"

            Case SortDirection.Descending
                newSortDirection = "DESC"
        End Select

        Return newSortDirection
    End Function

    Protected Sub ddlFilterList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlFilterList.SelectedIndexChanged
        'Filter Solution Base

        Dim strSQLFilter As String
        If Me.ddlFilterList.SelectedItem.Text = "Closed" Then
            'If UMLemployee = CurrentUserName Then
            If UMLemployee = 1 Then
                strSQLFilter = "SELECT * FROM dbo.tblAccMan_IssueLogs WHERE varStatus = 'Closed' ORDER BY intIssuePK"
            Else
                strSQLFilter = "EXEC spAccMan_SitesDisplay_Filter '" & MySession.UserID & "','Closed'"
            End If
        ElseIf Me.ddlFilterList.SelectedItem.Text = "Open" Then
            'If UMLemployee = CurrentUserName Then
            If UMLemployee = 1 Then
                strSQLFilter = "SELECT * FROM dbo.tblAccMan_IssueLogs WHERE varStatus = 'Open' ORDER BY intIssuePK"
            Else
                strSQLFilter = "EXEC spAccMan_SitesDisplay_Filter '" & MySession.UserID & "','Open'"
            End If

        ElseIf Me.ddlFilterList.SelectedItem.Text = "[--Please Select A Filter--]" Then
            'If UMLemployee = CurrentUserName Then
            If UMLemployee = 1 Then
                strSQLFilter = "SELECT * FROM dbo.tblAccMan_IssueLogs ORDER BY intIssuePK"
            Else
                strSQLFilter = "EXEC spAccMan_SitesDisplay2 " & MySession.UserID
            End If
        End If

        Dim mySP As String = strSQLFilter
        Dim myCN As New OleDb.OleDbConnection(Core.CurrentConnectionString)
        Dim myCMD As New OleDb.OleDbCommand(mySP, myCN)
        Dim myDA As New OleDb.OleDbDataAdapter
        Dim myDS As New System.Data.DataSet
        myDA.SelectCommand = myCMD
        myDA.Fill(myDS)
        grdvwIssueList.DataSource = myDS.Tables(0)
        grdvwIssueList.DataBind()

    End Sub

    Sub Date_Selected(ByVal sender As Object, ByVal e As EventArgs)
        Dim dtmDateValue As Date = Calendar1.SelectedDate.ToShortDateString
        Dim DateFormat As String = dtmDateValue.ToString("dd/MM/yyyy")

        Dim mySP As String
        If UMLemployee = 1 Then
            mySP = "SELECT * FROM dbo.tblAccMan_IssueLogs WHERE convert(varchar, dtmDate, 103) ='" & dtmDateValue & "' ORDER BY intIssuePK"
        Else
            mySP = "EXEC spAccMan_Calendar " & MySession.UserID & ", 0 , '" & DateFormat & "'"
        End If

        Dim myCN As New OleDb.OleDbConnection(Core.CurrentConnectionString)
        Dim myCMD As New OleDb.OleDbCommand(mySP, myCN)
        Dim myDA As New OleDb.OleDbDataAdapter
        Dim myDS As New System.Data.DataSet
        myDA.SelectCommand = myCMD
        myDA.Fill(myDS)
        grdvwIssueList.DataSource = myDS.Tables(0)
        grdvwIssueList.DataBind()

        Me.Calendar1.Visible = False
    End Sub

    Protected Sub Calendar1_DayRender(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DayRenderEventArgs) Handles Calendar1.DayRender
        If UMLemployee = 1 Then
            With Me.SqlDSCalendar1
                .Dispose()
                .ConnectionString = Core.CurrentConnectionString 'Core.CurrentConnectionString
                .SelectCommand = "SELECT * FROM tblAccMan_IssueLogs ORDER BY intIssuePK"
                .SelectCommandType = SqlDataSourceCommandType.Text
                .DataBind()
            End With
        Else
            With Me.SqlDSCalendar1
                .Dispose()
                .ConnectionString = Core.CurrentConnectionString 'Core.CurrentConnectionString
                .SelectCommand = "EXEC spAccMan_SitesDisplay2 " & MySession.UserID & ""
                .SelectCommandType = SqlDataSourceCommandType.Text
                .DataBind()
            End With
        End If

        Dim CalendarData As System.Data.DataView = CType(SqlDSCalendar1.Select(DataSourceSelectArguments.Empty), System.Data.DataView)
        Const TextColumnNameA As String = "dtmDate"
        Const TextColumnNameB As String = "varStatus"

        For Each row As System.Data.DataRowView In CalendarData
            Dim strCommentType As String
            strCommentType = row(TextColumnNameB).ToString

            Dim dtmDateValue As Date = row(TextColumnNameA).ToString
            Dim DateFormat As String = dtmDateValue.ToString("dd/MM/yyyy")

            If strCommentType = "Open" Then
                If e.Day.Date = DateFormat Then
                    e.Cell.BackColor = System.Drawing.Color.Red
                    e.Cell.BorderColor = System.Drawing.Color.Black
                    e.Cell.BorderStyle = WebControls.BorderStyle.Solid
                    e.Cell.BorderWidth = 1

                End If
            End If
        Next
    End Sub

    Protected Sub lbtnCalendar_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbtnCalendar.ServerClick
        If Me.Calendar1.Visible = False Then
            Me.Calendar1.Visible = True
        ElseIf Me.Calendar1.Visible = True Then
            Me.Calendar1.Visible = False
        End If
    End Sub

    Protected Sub lbtnRefresh_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbtnRefresh.ServerClick
        Dim mySP As String

        If UMLemployee = 1 Then
            mySP = "SELECT * FROM tblAccMan_IssueLogs ORDER BY intIssuePK"
        Else
            mySP = "EXEC spAccMan_SitesDisplay2 " & MySession.UserID
        End If

        grdvwIssueList.DataSourceID = ""
        Dim myCN As New OleDb.OleDbConnection(Core.CurrentConnectionString)
        Dim myCMD As New OleDb.OleDbCommand(mySP, myCN)
        Dim myDA As New OleDb.OleDbDataAdapter
        Dim myDS As New System.Data.DataSet
        myDA.SelectCommand = myCMD
        myDA.Fill(myDS)
        grdvwIssueList.DataSource = myDS.Tables(0)
        grdvwIssueList.DataBind()

        'If UMLemployee = 1 Then 'UML Employee

        'Else

        '    Dim myCN As New OleDb.OleDbConnection(Core.CurrentConnectionString)
        '    Dim myCMD As New OleDb.OleDbCommand(mySP, myCN)
        '    Dim myDA As New OleDb.OleDbDataAdapter
        '    Dim myDS As New System.Data.DataSet
        '    myDA.SelectCommand = myCMD
        '    myDA.Fill(myDS)
        '    grdvwIssueList.DataSource = myDS.Tables(0)
        '    grdvwIssueList.DataBind()
        'End If
    End Sub
End Class
