<%@ Page Language="VB" MasterPageFile="~/landing.master" AutoEventWireup="false" CodeFile="ActivityViewer.aspx.vb" Inherits="myapps_MyActivityManagement_ActivityViewer" title="Activity Management: Viewer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="content_right" Runat="Server">

<%--<script type="text/javascript"> 
    Panel p = new Panel("Panel1");
    p.setHeight("200px");
    for (int i = 0; i < 100; i++) 
    {    
    p.addComponent(new Label("Label " + i));
    }
    p.setScrollable(true);
    p.setScrollTop(9999);_l.addComponent(p);
</script>--%>

<script language="JavaScript" type="text/javascript">
    var zxcTO;

    function Scroll(id,dis,pos)
    {    
        var obj=document.getElementById(id)
        obj.scrollTop=obj.scrollTop+dis;
        if (pos){obj.scrollTop=pos; }
        else {zxcTO=setTimeout( function(){ Scroll(id,dis); },10); }
    }
              
</script>

<form id="Form1" runat="server">
    <div class="content_back general">
    
        <asp:Panel ID="pnlSubscribe" runat="server">
            <table style=" padding-bottom:10px; margin-left: auto; margin-right: auto; width: 500px; background-color:#FFFFC0; border:Solid 1px darkgrey; ">
                <tr>
                    <td>
                        <asp:Label ID="Label2" runat="server" Text="" style=" padding-left: 10px; color:#082B61; font-size: 14px; ">new subscriber? <span class="UMLGreen">(Notifications via email)</span></asp:Label>
                    </td> 
                    <td>
                        <a id="btnSubscribe" class="btnSubscribe" runat="server" title="Add to Subscribers List"></a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblInvalidSubscriber" runat="server" Text="" ForeColor="red" style=" padding-left: 10px;" >*unsubscribe?</asp:Label>
                    </td>
                    <td>
                        <a id="btnunsubscribe" class="btnunsubscribe" runat="server" title="Remove me from subscribers list"></a>
                    </td>
                </tr>
            </table> <!-- Add to SubscriberList -->
            <br />
        </asp:Panel>
        
        
        <asp:Panel ID="pnlIssueDetails" runat="server">
            <table>
                <tr>
                    <td>
                        <asp:Label ID="Label1" runat="server" Text="Details:" Font-Names="Calibri" ForeColor="Highlight" Width="140px"></asp:Label>
                        <asp:Label ID="lblReference" runat="server" Text="" Font-Names="Calibri" Width="350px"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label12" runat="server" Text="Issue:" Font-Names="Calibri" ForeColor="Highlight" Width="140px"></asp:Label>
                        <asp:Label ID="lblIssueBody" runat="server" Text="" Width="350px" Height="50px" Font-Names="Calibri"></asp:Label>
                    </td>                                                                 
                </tr>
            </table><!--Question ID Details & Fault/Solution Details-->  
            
            <table>
                <tr>
                    <td>
                        <a id="btnViewComment" class="btnViewComment" runat="server" title="View Comments Panel"></a>
                        <a id="btnViewAttachment" class="btnViewAttachment" runat="server" title="View Attachments Panel"></a>
                    </td>    
                </tr>
            </table>
        </asp:Panel>
   
        <asp:Panel ID="pnlViewComments" style="margin-bottom: 10px; padding-top:5px; background-color:#FFFFC0;" Width="650px" runat="server" Height="40px" HorizontalAlign="left" BorderStyle="Double" BorderWidth="1px" BorderColor="lightgray" Visible="False">
             <table style="margin-left: auto; margin-right: auto;">
                <tr>
                    <td>
                        <asp:Label ID="Label9" runat="server" Text="Comment:" Font-Names="Calibri" ForeColor="Highlight" Width="100px"></asp:Label>
                        <asp:TextBox ID="txtComment" runat="server" TextMode="MultiLine" Width="250px" Height="25px" ToolTip="" Font-Names="Calibri"></asp:TextBox>
                    </td>
                    <td>
                        <a id="btnAddComment" class="btnAddComment" runat="server" title="Add New Comment"></a>
                    </td>
                </tr>                                 
            </table> <!-- Add Comments -->
        </asp:Panel>   
        
        <asp:Panel ID="pnlViewAttachments" style="margin-bottom: 10px; background-color:#FFFFC0;" Width="650px" runat="server" Height="55px" HorizontalAlign="left" BorderStyle="Double" BorderWidth="1px" BorderColor="lightgray" Visible="False">
            <table style="margin-left: auto; margin-right: auto;">
                <tr>
                    <td>
                        <asp:Label ID="label10" runat="server" Text="Attach:" Font-Names="Calibri" ForeColor="Highlight" Width="60px"></asp:Label>
                    </td>
                    <td>  
                        <asp:FileUpload ID="FileUpload1" runat="server" Width="250px" Font-Size="14px" />
                    </td>
                    <td>
                       <a id="btnAttach" class="btnAttach" runat="server" title="Add Attachment"></a>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label8" runat="server" Text="Open:" Font-Names="Calibri" ForeColor="Highlight" Width="60px"></asp:Label> 
                    </td>
                    <td>  
                        <asp:DropDownList ID="ddlAttachments" runat="server" Font-Names="Calibri" Width="250px" ToolTip="Select an Attachment from the list to view." AutoPostBack="True">
                            <asp:ListItem>[-- Attachment List --]</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td>
                        <a id="btnSelectAttachment" class="btnSelectAttachment" runat="server" title="View Selected Attachment"></a>
                    </td>
                    <td>    
                        <a id="btnRemove" class="btnRemove" runat="server" title="Remove Attachment"></a>
                    </td>
                </tr> 
            </table><!-- View Attachments--> 
            <br />
        </asp:Panel>

        <asp:Panel ID="Panel1" style="margin-bottom: 10px;" Width="650px" runat="server" Height="200px" HorizontalAlign="Left"> 
            <div id="Tst1" style="overflow:auto; width:100%; height:100%; border:solid DarkGray 1px;" >        
                    <asp:Panel ID="Panel2" runat="server" Height="100%" Width="100%">
                    </asp:Panel> 
            </div>      
            <br />
            
                <asp:SqlDataSource ID="SqlComments" runat="server">
                </asp:SqlDataSource>
        </asp:Panel>    
        
        <table>
            <tr>
                <td>
                    <asp:Label ID="Label14" runat="server" Text="Mark-As:" Font-Names="Calibri" ForeColor="Highlight" Width="140px"></asp:Label> 
                    <asp:DropDownList ID="ddlCompleted" runat="server" Font-Names="Calibri" Width="305px" ToolTip="Select an Attachment from the list to view." AutoPostBack="True">
                        <asp:ListItem Value="0">[-- Select to Complete --]</asp:ListItem>
                        <asp:ListItem Value="1">Closed</asp:ListItem>
                        <asp:ListItem Value="2">Open</asp:ListItem>
                        <asp:ListItem Value="3">Pending</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
        </table><!--Completed-->
        
       
           
    </div>
</form>

</asp:Content>

