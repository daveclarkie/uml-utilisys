Imports Dundas.Charting.WebControl
Imports System.Drawing
Imports System
Imports System.IO


Partial Class myapps_myevents_default



    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            If Not IIf(Core.data_select_value("select bitAllowMyApps from tblUsers where intUserPK = " & MySession.UserID) = "True", True, False) Then
                Response.Redirect(Core.data_select_value("select RawHomeURL from vwUsers where ID = " & MySession.UserID), True)
            End If

            If Not Page.IsPostBack = True Then
                Dim description As HtmlMeta = New HtmlMeta()
                description.Name = "description"
                description.Content = "Utility Masters - My Events"
                Dim keywords As HtmlMeta = New HtmlMeta()
                keywords.Name = "keywords"
                keywords.Content = "My Events"
                Page.Header.Controls.Add(description)
                Page.Header.Controls.Add(keywords)


            End If

            If Request.QueryString("custid") Is Nothing Then
                Dim strSQL As String = "EXEC spPortal_TREEVIEW " & MySession.UserID
                Dim myConnection As New OleDb.OleDbConnection(Core.CurrentConnectionString)

                Dim myCommand As New OleDb.OleDbCommand(strSQL, myConnection)
                myCommand.Connection.Open()
                myCommand.CommandTimeout = 180
                Dim Reader As OleDb.OleDbDataReader = myCommand.ExecuteReader(CommandBehavior.CloseConnection)

                If Reader.HasRows Then
                    Reader.Read()
                    If Reader("Type") = "Company" Then
                        Response.Redirect("default.aspx?custid=" & Reader("id") & "&method=Customer", True)
                    Else
                        Response.Redirect("default.aspx?custid=" & Reader("id") & "&method=" & Reader("Type"), True)
                    End If
                    Reader.Close()
                End If
            End If

            Me.lblDescription.Text = Request.QueryString("method").ToString
            Me.lblName.Text = Core.data_select_value("EXEC spSites_MAP_CurentSelection " & MySession.UserID & ", '" & Request.QueryString("method") & "', " & Request.QueryString("custid"))

            If Not Page.IsPostBack = True Then
                LoadDropDown_AddNew()


                LoadDateDropDown()
            End If

            LoadCustomerEvents()


        Catch ex As Exception
            '
        End Try

    End Sub

    Private Sub LoadCustomerEvents()


        Dim strSQL As String
        strSQL = ""
        strSQL = strSQL & " EXEC UML_EXTData.dbo.usp_Portal_MyApps_MyEvents_ViewAll " & MySession.UserID & ", '" & Request.QueryString("method") & "', " & Request.QueryString("custid") & ", '" & Me.ddlMyEventsDate.SelectedItem.Value & "'"
        
        Me.dgrdCustomerEvents.DataSource = Core.data_select(strSQL)
        Me.dgrdCustomerEvents.DataBind()

    End Sub

    Private Sub LoadDateDropDown()

        With Me.ddlMyEventsDate
            .Items.Clear()
            .DataSource = Core.data_select(" EXEC UML_EXTData.dbo.usp_Portal_MyApps_MyEvents_ViewFilterDates " & MySession.UserID & ", '" & Request.QueryString("method") & "', " & Request.QueryString("custid"))
            .DataValueField = "ID"
            .DataTextField = "Value"
            .DataBind()
        End With
    End Sub


    Protected Sub ddlMyEventsDate_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlMyEventsDate.SelectedIndexChanged

    End Sub

    Private Sub LoadDropDown_AddNew()

        With Me.ddlSite
            .Items.Clear()
            .DataSource = Core.data_select(" EXEC UML_EXTData.dbo.usp_Portal_MyApps_MyEvents_ViewSites " & MySession.UserID & ", '" & Request.QueryString("method") & "', " & Request.QueryString("custid"))
            .DataValueField = "ID"
            .DataTextField = "Value"
            .DataBind()
        End With


        With Me.ddlType
            .Items.Clear()
            .DataSource = Core.data_select(" EXEC UML_EXTData.dbo.usp_Portal_MyApps_MyEvents_ViewEventTypes " & MySession.UserID & ", '" & Request.QueryString("method") & "', " & Request.QueryString("custid"))
            .DataValueField = "ID"
            .DataTextField = "Value"
            .DataBind()
        End With


        With Me.ddlDate
            .Items.Clear()
            .DataSource = Core.data_select(" EXEC UML_EXTData.dbo.usp_Portal_MyApps_MyEvents_ViewDates " & MySession.UserID & ", '" & Request.QueryString("method") & "', " & Request.QueryString("custid"))
            .DataValueField = "ID"
            .DataTextField = "Value"
            .DataBind()
        End With


        With Me.ddlFrom
            .Items.Clear()
            .DataSource = Core.data_select(" EXEC UML_EXTData.dbo.usp_Portal_MyApps_MyEvents_ViewTimes " & MySession.UserID & ", '" & Request.QueryString("method") & "', " & Request.QueryString("custid"))
            .DataValueField = "ID"
            .DataTextField = "Value"
            .DataBind()
        End With

        With Me.ddlTo
            .Items.Clear()
            .DataSource = Core.data_select(" EXEC UML_EXTData.dbo.usp_Portal_MyApps_MyEvents_ViewTimes " & MySession.UserID & ", '" & Request.QueryString("method") & "', " & Request.QueryString("custid"))
            .DataValueField = "ID"
            .DataTextField = "Value"
            .DataBind()
        End With

        Me.ddlTo.SelectedIndex = Me.ddlTo.Items.Count - 1



    End Sub





End Class
