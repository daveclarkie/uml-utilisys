 <%@ Page Language="VB" MasterPageFile="~/landing.master" AutoEventWireup="false" CodeFile="Default.aspx.vb" Inherits="myapps_myevents_default" %>

<%@ Register Assembly="DundasWebChart" Namespace="Dundas.Charting.WebControl" TagPrefix="DCWC" %>
<asp:Content ID="Content1" ContentPlaceHolderID="content_right" Runat="Server">

        
        <script type="text/javascript" src="../../../ThickBox/jquery-1.3.2.min.js"></script>
        <script type="text/javascript" src="../../../ThickBox/thickbox.js"></script>
        <link rel="stylesheet" href="../../../css/layout.css" type="text/css" media="screen" />  
        <link rel="stylesheet" href="../../../ThickBox/thickbox.css" type="text/css" media="screen" />   
                
<form id="Form1" runat="server">

    <div class="content_back general">
            <table>
                
                <tr valign="middle">
                    <td width="150px" >
                        <asp:Label ID="lblDescription" Text="Description" runat="server"></asp:Label>
                    </td>
                    <td width="400px" align="left">
                        <asp:Label ID="lblName" Text="Name" runat="server"></asp:Label>
                    </td>
                    <td width="50px">
                        
                    </td>
                </tr>
                
                <tr valign="middle">
                    <td width="150px" >
                        Application
                    </td>
                    <td width="400px" align="left">
                        My Events
                    </td>
                    <td width="50px">
                        
                    </td>
                </tr>
                
                
                <tr style="height:10px;">
                    <td>
                    
                    </td>
                </tr>
                
                <tr>
                    <td colspan="3" style="background-color:#082B61; color:#FFFFFF;">
                        <table>
                            <tr>
                                <td></td>
                                <td>Site</td>
                                <td>Type</td>
                                <td>Date</td>
                                <td>From</td>
                                <td>To</td>
                                <td></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td><asp:DropDownList ID="ddlSite" runat="server" AutoPostBack="true" Width="150px"></asp:DropDownList></td>
                                <td><asp:DropDownList ID="ddlType" runat="server" AutoPostBack="true" Width="100"></asp:DropDownList></td>
                                <td><asp:DropDownList ID="ddlDate" runat="server" AutoPostBack="true" Width="100"></asp:DropDownList></td>
                                <td><asp:DropDownList ID="ddlFrom" runat="server" AutoPostBack="true" Width="100"></asp:DropDownList></td>
                                <td><asp:DropDownList ID="ddlTo" runat="server" AutoPostBack="true" Width="100"></asp:DropDownList></td>
                                <td></td>
                                
                            </tr>
                            
                            <tr>
                                <td></td>
                                <td>Event</td>
                                <td colspan="4"><asp:TextBox ID="txtEvent" runat="server" Width="415"></asp:TextBox></td>
                                <td></td>
                            </tr>
                            
                            <tr>
                                <td colspan="5" align="right">
                                    <asp:LinkButton ID="btnAdd" CssClass="btnAdd" runat="server" Text="Add"></asp:LinkButton>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                
                               
                <tr style="height:10px;">
                    <td>
                    
                    </td>
                </tr>
                
                <tr>
                    <td colspan="3" style="background-color:#6EBB1F; color:#000000;">
                        <table>
                            <tr>
                                <td>
                                    Date Filter
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlMyEventsDate" runat="server" AutoPostBack="true"></asp:DropDownList>
                                </td>
                                <td></td>
                            </tr>
                            
                            <tr>
                                <td colspan="2">
                                    <asp:DataGrid autogeneratecolumns="false" borderwidth="0" ID="dgrdCustomerEvents" cssclass="users" runat="server">
                                        <Columns>
                                            <asp:BoundColumn DataField="intSitesEventPK" HeaderText="ID" />
                                            <asp:BoundColumn DataField="sitename" HeaderText="Site Name" />
                                            <asp:BoundColumn DataField="varEventTypeName" HeaderText="Type" />
                                            <asp:BoundColumn DataField="dtmEventDate" HeaderText="Date" />
                                            <asp:BoundColumn DataField="dtmEventStartTime" HeaderText="From" />
                                            <asp:BoundColumn DataField="dtmEventEndTime" HeaderText="To" />
                                            <asp:BoundColumn DataField="varEventDescription" HeaderText="Event" />
                                        </Columns>
                                    <headerstyle cssclass="header" />
                                    </asp:DataGrid>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                                          
            </table>
        </div>
</form>
</asp:Content>

