Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Drawing
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.SessionState
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports System.Data
Imports System.Data.OleDb


Partial Class myapps_CustomerMonitors_MeterMonitors
    Inherits System.Web.UI.Page

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Me.errorlabel.Text = ""
        Me.HiddenFieldCustomerName.Value = Request.QueryString("customer") '
        Me.hiddenCompanyID.Value = Request.QueryString("custid") ' Session("Companyid")
        Me.SqlMonitors.SelectCommand = "  SELECT * FROM dbo.MeterReadingPersonnel  WHERE companyID=" & Me.hiddenCompanyID.Value
        Me.lblCompanyname.Text = Session("CurrentCompanyName")

        Me.PanelEditorPerson.BorderStyle = BorderStyle.Solid
        Me.PanelEditorPerson.BorderWidth = 1
        Me.PanelEditorPerson.BorderColor = Color.LightGray

        '        Me.PanelActualMeters.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFFFC0")
        Me.GridViewPersonnel.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFFFC0")
        Me.GridViewPersonnel.BorderStyle = BorderStyle.Solid
        Me.GridViewPersonnel.BorderWidth = 1
        Me.GridViewPersonnel.BorderColor = Color.LightGray

        '        Me.GridViewPersonnel.SelectedRowStyle.Font.Bold = False
        '        Me.GridViewPersonnel.SelectedRowStyle.ForeColor = Drawing.Color.Blue

    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.errorlabel.Text = ""
        Me.lblCompanyname.Text = Session("CurrentCompanyName")
    End Sub


    Protected Sub GridViewPersonnel_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridViewPersonnel.RowCommand
        Me.errorlabel.Text = ""
        On Error Resume Next
        If IsNumeric(e.CommandArgument) = True Then
            Dim iSelectedText As String
            Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            Dim mySelectedRow As GridViewRow = GridViewPersonnel.Rows(index)
            Me.hiddenGridSelectID.Value = mySelectedRow.Cells(1).Text()
            Me.txtMonitorName.Text = mySelectedRow.Cells(2).Text()
            Me.txtMonitorTitle.Text = mySelectedRow.Cells(3).Text()

            Me.PanelEditorPerson.Visible = True

            Me.ImageButtonSave.Style("LEFT") = "520px"
            Me.ImageButtonSave.Visible = True

            Me.ImageButtonDelete.Style("LEFT") = "110px"
            Me.ImageButtonDelete.Visible = True


            Me.ImageButtonNew.Visible = True
            Me.ImageButtonSaveNewPerson.Visible = False
        Else
        End If
    End Sub

    Protected Function CheckDuplicates(ByVal TheName As String, ByVal TheTitle As String) As Boolean
        Me.errorlabel.Text = ""
        Dim nReturn As Boolean = False
        Dim strSQL As String = ""
        strSQL = ""
        strSQL = strSQL & "  SELECT * FROM dbo.MeterReadingPersonnel "
        strSQL = strSQL & "  WHERE iName=" & "'" & Trim(TheName) & "'"
        strSQL = strSQL & " AND iTitle=" & "'" & Trim(TheTitle) & "'"

        Me.SqlUniversal.SelectCommand = strSQL
        Dim ivwExpensiveItems As Data.DataView = CType(SqlUniversal.Select(DataSourceSelectArguments.Empty), Data.DataView)
        If ivwExpensiveItems.Count > 0 Then
            nReturn = True
        Else
            nReturn = False
        End If

        CheckDuplicates = nReturn
    End Function

    Protected Sub ImageButtonNavigateBack_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonNavigateBack.Click
        Response.Redirect("~/myapps/customermeters/default.aspx?custid=" & Me.hiddenCompanyID.Value & "&method=Customer", True)
    End Sub

  
    Protected Sub ImageButtonSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonSave.Click
        Me.errorlabel.Text = ""
        Dim strSQL As String = ""
        If Trim(Me.txtMonitorName.Text) = "" Then
            Me.errorlabel.Text = "Status: Please enter a valid Name "
            'MsgBox("Please enter a valid Name", MsgBoxStyle.Critical, "Validation error")
            Me.txtMonitorName.Focus()
            Exit Sub
        End If

        If Trim(Me.txtMonitorTitle.Text) = "" Then
            Me.errorlabel.Text = "Status: Please enter a valid Title "
            'MsgBox("Please enter a valid Title", MsgBoxStyle.Critical, "Validation error")
            Me.txtMonitorTitle.Focus()
            Exit Sub
        End If

        If CheckDuplicates(Trim(Me.txtMonitorName.Text), Trim(Me.txtMonitorTitle.Text)) = True Then
            Me.errorlabel.Text = "Status: This person is already of file "
            'MsgBox("This person is already of file", MsgBoxStyle.Critical, "Validation error")
            Me.txtMonitorName.Focus()
        End If

        strSQL = ""
        strSQL = strSQL & "  Update MeterReadingPersonnel "
        strSQL = strSQL & " SET iName=" & "'" & XY(Trim(txtMonitorName.Text)) & "'"
        strSQL = strSQL & " , iTitle=" & "'" & XY(Trim(txtMonitorTitle.Text)) & "'"
        strSQL = strSQL & " Where ID=" & Me.hiddenGridSelectID.Value

        Me.SqlMonitors.UpdateCommand = strSQL
        Me.SqlMonitors.Update()
        Me.GridViewPersonnel.DataBind()

        Me.ImageButtonNew.Visible = True
        Me.PanelEditorPerson.Visible = False
        Me.ImageButtonSave.Visible = False
        Me.ImageButtonDelete.Visible = False
        Me.ImageButtonSaveNewPerson.Visible = False



    End Sub

    Protected Sub ImageButtonDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonDelete.Click
        Me.errorlabel.Text = ""
        Dim strSQL As String = ""
        strSQL = ""
        strSQL = strSQL & "  DELETE FROM MeterReadingPersonnel "
        strSQL = strSQL & " Where ID=" & Me.hiddenGridSelectID.Value
        Me.SqlMonitors.DeleteCommand = strSQL
        Me.SqlMonitors.Delete()
        Me.GridViewPersonnel.DataBind()

        Me.ImageButtonNew.Visible = True
        Me.PanelEditorPerson.Visible = False
        Me.ImageButtonSave.Visible = False
        Me.ImageButtonDelete.Visible = False
        Me.ImageButtonSaveNewPerson.Visible = False

        Me.txtMonitorName.Text = ""
        Me.txtMonitorTitle.Text = ""
    End Sub

    Protected Sub ImageButtonNew_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonNew.Click
        Me.errorlabel.Text = ""
        Me.txtMonitorName.Text = ""
        Me.txtMonitorTitle.Text = ""

        Me.PanelEditorPerson.Visible = True
        Me.ImageButtonNew.Visible = False
        Me.ImageButtonSave.Visible = False
        Me.ImageButtonDelete.Visible = False

        Me.ImageButtonSaveNewPerson.Style("LEFT") = "520px"
        Me.ImageButtonSaveNewPerson.Visible = True

    End Sub

    Protected Sub ImageButtonSaveNewPerson_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonSaveNewPerson.Click
        Me.errorlabel.Text = ""
        Dim strSQL As String = ""
        If Trim(Me.txtMonitorName.Text) = "" Then
            Me.errorlabel.Text = "Status: Please enter a valid Name"
            'MsgBox("Please enter a valid Name", MsgBoxStyle.Critical, "Validation error")
            Me.txtMonitorName.Focus()
            Exit Sub
        End If

        If Trim(Me.txtMonitorTitle.Text) = "Status: Please enter a valid Title" Then
            Me.errorlabel.Text = ""
            'MsgBox("Please enter a valid Title", MsgBoxStyle.Critical, "Validation error")
            Me.txtMonitorTitle.Focus()
            Exit Sub
        End If

        If CheckDuplicates(Trim(Me.txtMonitorName.Text), Trim(Me.txtMonitorTitle.Text)) = True Then
            Me.errorlabel.Text = "Status: This person is already of file"
            'MsgBox("This person is already of file", MsgBoxStyle.Critical, "Validation error")
            Me.txtMonitorName.Focus()
            Exit Sub
        End If

        strSQL = ""
        strSQL = strSQL & "  INSERT INTO MeterReadingPersonnel(CompanyId,iName,iTitle)  "
        strSQL = strSQL & " Values("
        strSQL = strSQL & "'" & Me.hiddenCompanyID.Value & "'" & ","
        strSQL = strSQL & "'" & XY(Trim(txtMonitorName.Text)) & "'" & ","
        strSQL = strSQL & "'" & XY(Trim(Me.txtMonitorTitle.Text)) & "'"
        strSQL = strSQL & " )"

        Me.SqlMonitors.InsertCommand = strSQL
        Me.SqlMonitors.Insert()
        Me.GridViewPersonnel.DataBind()

        Me.txtMonitorName.Text = ""
        Me.txtMonitorTitle.Text = ""

        Me.PanelEditorPerson.Visible = False
        Me.ImageButtonSave.Visible = False
        Me.ImageButtonDelete.Visible = False
        Me.ImageButtonNew.Visible = True
        Me.ImageButtonSaveNewPerson.Visible = False
    End Sub

    Protected Function XY(ByRef what As String) As String
        XY = Replace(what, "'", "''")
    End Function


End Class
