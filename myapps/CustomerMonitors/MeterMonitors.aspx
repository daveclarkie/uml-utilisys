<%@ Page Language="VB" MasterPageFile="~/landing.master" AutoEventWireup="false" CodeFile="MeterMonitors.aspx.vb" Inherits="myapps_CustomerMonitors_MeterMonitors" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="content_right" Runat="Server">

   <form id="form1" runat="server">
   <div class="content_back general">

<div>
<%--
    <asp:Image ID="Image1"  ImageUrl="~/assets/CompanyHeader.gif" style="z-index:10;position:relative;top:5px;left:5px;") runat="server" />
  --%>
<asp:Label ID="errorlabel" style="position:absolute;top:90px;left:300px;"  Font-Names="Arial"   Font-Size="11px"  ForeColor="Red"   runat="server" Text="Status: " Font-Bold="True"></asp:Label>
<asp:Label ID="Label28" style="z-index:100;position:absolute;top:100px;left:245px;"  Font-Names="Arial"   Font-Size="14px" ForeColor="DarkBlue" Font-Bold="true" runat="server" Text="Meter -  monitoring personnel"></asp:Label>
<asp:ImageButton ID="ImageButtonNavigateBack" tooltip="Return to previous screen" style="z-index:1;position:absolute;top:100px;left:850px;" Height="32px" Width="32px"  ImageUrl="~/assets/metering/remove_48X48.png" runat="server" />
<asp:Label ID="lblCompanyname" visible="false" runat="server" Text=""></asp:Label>
</div> 

    <asp:Panel ID="PanelGrid"   style="position:absolute;top:135px;left:240px;" Width="650px" ScrollBars="Vertical" height="300px" runat="server">
        <asp:GridView ID="GridViewPersonnel" 
        runat="server"
        AllowPaging="false"
        AutoGenerateColumns="False" 
       CellSpacing="0" 
        CellPadding="0" 
        Gridlines="None"
        Font-Names="Verdana"
        Font-Size="12px" 
        Width="624px"
        DataKeyNames="ID" 
        DataSourceID="SqlMonitors" 
         EnableModelValidation="True" 
         BackColor="Transparent">
                <Columns>
                        <asp:CommandField ShowSelectButton="True"   SelectText="Edit" >
                        <ItemStyle  Width="15px" Height="10px"/>
                        </asp:CommandField>
                        
                        <asp:BoundField DataField="ID"          HeaderText="Monitor"  ReadOnly="True"  itemstyle-cssclass="hiddencol"  />
                        <asp:BoundField DataField="iName"  HeaderText="Title" >
                              <ItemStyle  Width="200px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="iTitle"    >
                          <ItemStyle  Width="330px" />
                        </asp:BoundField>
                </Columns>
        </asp:GridView>
    </asp:Panel>

 
<asp:Panel ID="PanelMain"  style="position:absolute;top:450px;left:240px;" runat="server" Height="350px" Width="628px">
        <asp:ImageButton ID="ImageButtonNew" style="position:absolute;top:5px;left:4px;" tooltip="Add a new person"   Height="20px"  Width="100px"   ImageUrl="~/assets/button_new.gif" runat="server" />
      
        <asp:ImageButton ID="ImageButtonSaveNewPerson" style="position:absolute;top:5px;left:217px;" tooltip="Append to database"   Height="20px"  Width="100px"    ImageUrl="~/assets/button_save.gif" runat="server" Visible="False" />
        <asp:ImageButton ID="ImageButtonSave" style="position:absolute;top:5px;left:110px;" tooltip="Save current details"  Height="20px" Width="100px"  ImageUrl="~/assets/button_save.gif" runat="server" Visible="False" />
        <asp:ImageButton ID="ImageButtonDelete" style="position:absolute;top:5px;left:520px;" tooltip="Delete current personnel"   Height="20px"  Width="100px"   ImageUrl="~/assets/button_delete.gif" runat="server" Visible="False" />
     
    <asp:Panel ID="PanelEditorPerson"  style="position:absolute;top:30px;left:0px;" runat="server" BackColor="#FFFFC0" Width="624px" Visible="False">
        <asp:Label ID="Label5" runat="server"   Width="100px" Text="Monitor Name" ></asp:Label>
        <asp:TextBox ID="txtMonitorName" runat="server"   maxlength="50" Width="200px"  ></asp:TextBox>
        <br />
        <asp:Label ID="Label7" runat="server"  width="100px" Text="Monitor Title" ></asp:Label>
        <asp:TextBox ID="txtMonitorTitle" runat="server" maxlength="50"  Width="200px"    ></asp:TextBox>
        </asp:Panel>
        
        
  </asp:Panel>
 

      
<asp:SqlDataSource ID="SqlUniversal" 
runat="server"
ConnectionString="<%$ ConnectionStrings:portal.utilitymasters.co.ukConnectionString %>" 
SelectCommand="SELECT * FROM dbo.MeterReadingPersonnel  WHERE companyID=0"
>
       </asp:SqlDataSource>       
      

<asp:SqlDataSource ID="SqlMonitors" 
runat="server"
ConnectionString="<%$ ConnectionStrings:portal.utilitymasters.co.ukConnectionString %>" 
SelectCommand="SELECT * FROM dbo.MeterReadingPersonnel  WHERE companyID=0">
       </asp:SqlDataSource>

       <asp:HiddenField ID="HiddenFieldCustomerName" runat="server" />
       <asp:HiddenField ID="hiddenGridSelectID" runat="server" />
       <asp:HiddenField ID="hiddenCompanyID" runat="server" />  
</div>
 </form>


</asp:Content>

