<%@ Page Language="VB" MasterPageFile="~/landing.master" AutoEventWireup="false" CodeFile="Default.aspx.vb" Inherits="myapps_helpdesk" title="Helpdesk" %>

<asp:Content ID="Content1" ContentPlaceHolderID="content_right" Runat="Server">

<form action="http://uk-ed0-sdsk-02.mceg.local/servlets/RequestServlet" method="GET" target="_blank" >

    <div class="content_back general">
            
            <table cellpadding="0" cellspacing="0">
               
                <tr>
                    
                    <td colspan="1">
                        <table cellpadding="0" cellspacing="0">

                            <tr>
                                <td style="width:120px;" valign="top">
                                     <asp:Label ID="lblDescription" Text="Description" runat="server" Font-Bold="true" Width="120px" ></asp:Label>
                                </td>
                                <td style="width:400px;" valign="middle">
                                     <asp:Label ID="lblName" Text="Name" runat="server"></asp:Label>
                                </td>
                            </tr>

                            <tr>
                                <td style="height:3px;">
                                    
                                </td>
                            </tr>

                             <tr>
                                <td style="width:120px;" valign="top">
                                     <b>Application</b>
                                </td>
                                <td style="width:400px;" valign="middle">
                                    Functional Team Helpdesk
                                </td>
                            </tr>
                                                        
                            <tr>
                                <td style="height:3px;">
                                    
                                </td>
                            </tr>

                            <tr>
                                <td colspan="2">
                                    
                                </td>
                            </tr>
                            
                        </table>
                    </td>
                </tr> <!-- Meter Selection Table -->
                    <td>
                        
                        
                            <table width="550" cellpadding="5" cellspacing="0" style="border:1px #ccc solid" align="center">
	                            <tr>
		                            <th colspan="3" align="center">New Request Form</th>
	                            </tr>
	
	                            <tr>
		                            <td>Target URL</td>
		                            <td>:</td>
		                            <td><input type="text" class="txt" name="targetURL" style="width:200px;" value="/servlets/RequestServlet" /></td>
	                            </tr>
	                           
	                            <tr>
		                            <td>Title</td>
		                            <td>:</td>
		                            <td><input type="text" class="txt" name="title" style="width:200px;" value="Test" /></td>
	                            </tr>
                                
	                            <tr>
		                            <td>Description</td>
		                            <td>:</td>
		                            <td><input type="text" class="txt" name="description" style="width:200px;" value="This is a description" /></td>
	                            </tr>

	                            <tr>
		                            <td>Template Name</td>
		                            <td>:</td>
		                            <td><input type="text" class="txt" name="reqtemplate" style="width:200px;" value="Include - Sanctuary Housing Association" /></td>
	                            </tr>
                                
                                <tr>
                                    <td>Fuel Type</td>
		                            <td>:</td>
		                            <td><input type="text" class="txt" name="Fuel Type" style="width:200px;" value="Gas" /></td>
                                </tr>

                                <tr>
                                    <td>Technician</td>
		                            <td>:</td>
		                            <td><input type="text" class="txt" name="technician" style="width:200px;" value="Unassigned" /></td>
                                </tr>

                                
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>

                                <tr>
                                    <td colspan="2"><hr /></td>
                                </tr>
                                
	                            <tr>
		                            <td>Username</td>
		                            <td>:</td>
		                            <%--<td><input type="text" class="txt" name="username" value="dave.clarke" style="width:200px;" /></td>--%>
		                            <td><input type="text" class="txt" name="username" value="sanctuaryhousingassociation" style="width:200px;" /></td>
	                            </tr>

	                            <tr>
		                            <td>Password</td>
		                            <td>:</td>
		                            <%--<td><input type="password" class="txt" name="password" value="Se7en7se7en7"  style="width:200px;" /></td>--%>
                                    <td><input type="password" class="txt" name="password" value="Cabbage1"  style="width:200px;" /></td>
	                            </tr>
                                                                
                                <tr>
		                            <td>Domain Name</td>
		                            <td>:</td>
		                            <%--<td><input type="text" class="txt" name="DOMAIN_NAME" value="MCEG" style="width:200px;" /></td>--%>
		                            <td><input type="text" class="txt" name="DOMAIN_NAME" value="" style="width:200px;" /></td>
	                            </tr>
                                                                
                                <tr>
		                            <td>logonDomainName</td>
		                            <td>:</td>
		                            <%--<td><input type="text" class="txt" name="logonDomainName" value="AD_AUTH" style="width:200px;" /></td>--%>
		                            <td><input type="text" class="txt" name="logonDomainName" value="Local Authentication" style="width:200px;" /></td>
	                            </tr>
                                
	                            <tr align="center">
		                            <td colspan="5">
                                        <input type="submit" NAME="operation" align='center' class="btnsubmitact" value="AddRequest" />
                                    </td>
	                            </tr>
                            </table>

                    </td>
                <tr>
                
                </tr>
            </table>
        </div>

     
</form>
</asp:Content>

