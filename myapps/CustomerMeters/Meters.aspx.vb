﻿Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Drawing
Imports System.Web
Imports Dundas.Charting.WebControl
Imports Dundas.Charting.WebControl.Utilities
Imports System.Web.Security
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.SessionState
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports System.Data
Imports System.Data.OleDb

Partial Class myapps_CustomerMeters_Meters
    Inherits System.Web.UI.Page
    Dim iCustomer As String = ""
    Dim iMethod As String = ""
    Dim iMethodType As String = ""

    Dim iCurrentItemNeeded As Integer = 0
    Dim GlobalReadRequired As Boolean = False


    Shared cGreen As Color = System.Drawing.ColorTranslator.FromHtml("#6EBB1F")
    Shared cBlue As Color = System.Drawing.ColorTranslator.FromHtml("#082B61")
    Shared cYellow As Color = System.Drawing.ColorTranslator.FromHtml("#FFFFC0")
    Shared colorSet() As Color = {cBlue, cGreen, cYellow, Color.Purple}

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        ' EXEC UML_EXTData.dbo.usp_Portal_MyApps_MyEvents_ViewSites 62 , 'Customer' , 347
        If Request.QueryString("method") = "Customer" Then
            iCustomer = Trim(Replace(Request.QueryString("custid"), "'", ""))
            iMethod = Trim(Replace(Request.QueryString("method"), "'", ""))
            iMethodType = "Customer"
            Session("TheCompanyID") = Request.QueryString("custid")

            Me.ImageButtonOutstanding.Visible = True


        Else
            'iCustomer = Trim(Replace(Request.QueryString("custid"), "'", ""))
            iCustomer = Trim(Replace(Request.QueryString("SiteId"), "'", ""))
            iMethod = Trim(Replace(Request.QueryString("method"), "'", ""))
            iMethodType = "Site"

            'MsgBox(Session("TheCompanyID"))

            Me.ImageButtonOutstanding.Visible = False


        End If

        Session("WhichOrder") = "Descending"
        Dim strSQL As String = ""
        Me.HiddenFieldCurrentUser.Value = MySession.UserID
        Me.HiddenFieldSiteID.Value = Session("CurrentSelectedSiteID")
        Me.hiddenCompanyID.Value = Request.QueryString("custid")

        If Request.QueryString("method") = "Customer" Then
            Me.LabCompany.Text = Request.QueryString("Customer")
        Else
            Me.LabCompany.Text = Request.QueryString("Site")
        End If
 
        Session("CurrentCompany") = Trim(Request.QueryString("custid")).ToString
        '        strSQL = "EXEC UML_EXTData.dbo.usp_Portal_MyApps_MyEvents_ViewSites " & MySession.UserID & "," & "'" & iMethod & "'" & "," & Me.hiddenCompanyID.Value

        strSQL = "EXEC UML_EXTData.dbo.usp_Portal_MyApps_MyMeters_ViewSites " & MySession.UserID & "," & "'" & iMethod & "'" & "," & Me.hiddenCompanyID.Value

        '  Me.DropDownListSiteFilter.Items.Add(New ListItem(".....", "0"))

        Me.SQL_UserSitesCreateMeter.SelectCommand = strSQL
        Me.SQL_UserSitesCreateMeter.DataBind()

        '    Me.SQL_UserSites.SelectCommand = strSQL
        '    Me.SQL_UserSites.DataBind()

        '    MsgBox(Me.DropDownListSiteFilter.DataSource)

        If Trim(Me.LabCompany.Text) = "" Then
            iCurrentItemNeeded = 0
        Else
            iCurrentItemNeeded = 1 'Me.DropDownListSiteFilter.Items.FindByText(Trim(Me.LabCompany.Text)).Value
        End If

        '  

        ' EXEC UML_EXTData.dbo.usp_Portal_MyApps_MyMeters_ViewSites 62,'Customer',761
        ' EXEC UML_EXTData.dbo.usp_Portal_MyApps_MyMeters_ViewSites 62,'Site',8004

        ' Me.DropDownListSiteFilter.Items.Add(New ListItem(".....", "0"))
        Me.HiddenTemplate.Value = 1

        'MsgBox(Me.DropDownListSiteFilter.Items.FindByText(Trim(Me.LabCompany.Text)).Value)

        ' john

        ' Me.DropDownListSiteFilter.SelectedItem.Text = Me.LabCompany.Text
        'Dim strSQL As String = ""
        'strSQL = " EXEC spCustomer_MeterTemplateUnit_Site  " & Me.hiddenCompanyID.Value & " , " & DropDownListSiteFilter.SelectedValue
        ''   MsgBox(strSQL)
        'Me.SQL_MeterSelect.SelectCommand = strSQL
        'Me.LabCompany.Text = Trim(DropDownListSiteFilter.SelectedItem.Text)

        '  Me.DropDownTypeOf.Items.Add(New ListItem("Consumption", "C"))
        ' Me.DropDownTypeOf.Items.Add(New ListItem("Readings Only", "R"))

        Me.DropDownTypeOf.Items.Add(New ListItem("Consumption", "R"))
        Me.DropDownTypeOf.Items.Add(New ListItem("Readings Only", "C"))

        Me.DropDownListLoging.Items.Add(New ListItem("Daily", "D"))
        Me.DropDownListLoging.Items.Add(New ListItem("Weekly", "W"))
        Me.DropDownListLoging.Items.Add(New ListItem("Week Number", "WK"))
        Me.DropDownListLoging.Items.Add(New ListItem("Monthly", "M"))

        Me.DropDownListUseage.Items.Add(New ListItem("Energy", "E"))
        Me.DropDownListUseage.Items.Add(New ListItem("Production", "P"))

        ' SQLPersonnel.SelectCommand = "Select ID , IName  FROM dbo.MeterReadingPersonnel WHERE CompanyID=" & Me.hiddenCompanyID.Value & " ORDER BY  IName ASC"
        ' SQLPersonnelTwo.SelectCommand = "Select ID , IName  FROM dbo.MeterReadingPersonnel WHERE CompanyID=" & Me.hiddenCompanyID.Value & " ORDER BY  IName ASC"

        SQLPersonnel.SelectCommand = "Select ID , IName  FROM dbo.MeterReadingPersonnel WHERE CompanyID=" & Session("TheCompanyID") & " ORDER BY  IName ASC"
        SQLPersonnelTwo.SelectCommand = "Select ID , IName  FROM dbo.MeterReadingPersonnel WHERE CompanyID=" & Session("TheCompanyID") & " ORDER BY  IName ASC"




        Me.PanelActualMeters.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFFFC0")
        Me.PanelGridView2.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFFFC0")

        Me.PanelActualMeterReadingsEditor.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFFFC0")
        Me.PanelPureMeters.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFFFC0")

        Me.PanelActualMeterReadingsEditor.BorderStyle = BorderStyle.Solid
        Me.PanelPureMeters.BorderStyle = BorderStyle.Solid

        Me.PanelActualMeterReadingsEditor.BorderColor = Color.LightGray
        Me.PanelPureMeters.BorderColor = Color.LightGray

        Me.PanelActualMeterReadingsEditor.BorderWidth = 1
        Me.PanelPureMeters.BorderWidth = 1

        GridView1.BackColor = Color.Transparent
        GridView2.BackColor = Color.Transparent


        ' MsgBox("INIT DONE")


    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        On Error Resume Next
        Dim iIndex As Integer = 0
        Dim iItem As String = ""
        Dim strSQL As String = ""
        Dim ItemToShow As String = ""
        Dim i As Integer = 0

        If Me.IsPostBack Then
        Else
            Dim myConnectionString As String = "Provider=SQLNCLI.1;Integrated Security=SSPI;Persist Security Info=False;Initial Catalog=portal.utilitymasters.co.uk;Data Source=uk-ed0-sqlcl-01.MCEG.local"
            Me.HiddenFieldSiteID.Value = Session("CurrentSelectedSiteID")

            If Request.QueryString("Site") = 0 Then
                strSQL = "SELECT COUNT(id) AS 'Count' FROM dbo.tblCustomer_Meters WHERE companyid=" & Me.hiddenCompanyID.Value
            Else
                strSQL = "SELECT COUNT(id) AS 'Count' FROM dbo.tblCustomer_Meters WHERE companyid=" & Me.hiddenCompanyID.Value
                strSQL = strSQL & " AND SiteID=" & Me.HiddenFieldSiteID.Value
            End If

            Dim mySelectQuery As String = strSQL
            Dim myConnection As New OleDbConnection(myConnectionString)
            Dim myCommand As New OleDbCommand(mySelectQuery, myConnection)
            myCommand.Connection.Open()
            Dim myReader As OleDbDataReader = myCommand.ExecuteReader(CommandBehavior.CloseConnection)
            myReader.Read()

            If Me.HiddenFieldSiteID.Value = "" Then
                Me.SQL_MeterSelect.SelectCommand = " EXEC spCustomer_MeterTemplateUnit_Site  " & Me.hiddenCompanyID.Value & " , " & Me.DropDownListSiteLevel.SelectedValue & ",'N'"
            Else
                Me.SQL_MeterSelect.SelectCommand = " EXEC spCustomer_MeterTemplateUnit_Site  " & Me.hiddenCompanyID.Value & " , " & Me.HiddenFieldSiteID.Value & ",'N'"
            End If

            If Me.GridView1.Rows.Count = 0 Then
            Else
            End If

            myReader.Close()

            '   Me.SQL_MeterReadings.SelectCommand = " EXEC dbo.spCustomer_MeterReadings  " & "'" & Me.DropDownListMeterReadingsYear.SelectedValue & "'" & " , " & Me.hiddenMeterID.Value
            '   MeterID , Minus days = Ording'
            '   EXEC spCustomer_MeterReadings_Flexible  1,-5000,1

            Me.SQL_MeterReadings.SelectCommand = " EXEC dbo.spCustomer_MeterReadings_Flexible, " & Me.hiddenMeterID.Value & ",-365,0"
            Me.GridView2.DataBind()

        End If

    End Sub

    Protected Sub GridView2_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView2.RowCommand
        On Error Resume Next
        Dim strSQL As String = ""
        On Error GoTo Leavesafe
        Me.txtTagNote.Text = ""

        Me.errorlabeldateexceeded.Visible = False
        Me.NextReadingDate.Visible = True

        If IsNumeric(e.CommandArgument) = True Then
            Me.HiddenFieldEnterNewReading.Value = "False"
            Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            Dim mySelectedRow As GridViewRow = GridView2.Rows(index)
            Me.HiddenScheduledMeter.Value = Trim(mySelectedRow.Cells(1).Text)
            Me.entryreading.Text = Trim(mySelectedRow.Cells(2).Text)
            Me.entrytime.Text = Mid(Trim(mySelectedRow.Cells(3).Text), 12, 8)
            Me.entrydate.Text = Mid(Trim(mySelectedRow.Cells(3).Text), 1, 10)
            ' Me.QuickDeleteDate.Text = Trim(mySelectedRow.Cells(3).Text)

            Me.SQL_MeterGeneral.SelectCommand = "  EXEC  spCustomer_ScheduleMeterDetails " & HiddenScheduledMeter.Value
            Dim ivwExpensiveItems As System.Data.DataView = CType(SQL_MeterGeneral.Select(DataSourceSelectArguments.Empty), System.Data.DataView)
            If ivwExpensiveItems.Count > 0 Then
                For Each rowProduct As System.Data.DataRowView In ivwExpensiveItems
                    Me.PersonnelListing.SelectedIndex = Me.PersonnelListing.Items.IndexOf(Me.PersonnelListing.Items.FindByText(Trim(rowProduct("iName"))))
                    If IsDBNull(rowProduct("TagNote")) = True Then
                    Else
                        Me.txtTagNote.Text = Trim(rowProduct("TagNote"))
                    End If
                    Exit For
                Next
            Else
            End If

            Me.PanelActualMeterReadingsNew.Visible = False
            Me.PanelGridView2Graph.Visible = False
            Me.PanelActualMeterReadingsEditor.Visible = True
            Me.PanelActualMeterReadingsEditor.Style("TOP") = "270px"

        Else
        End If
        Exit Sub

Leavesafe:
        Exit Sub
    End Sub

    Protected Sub GridView2_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView2.RowDataBound
        On Error Resume Next
        If Trim(e.Row.Cells(2).Text) = "" Or Trim(e.Row.Cells(2).Text) = " " Then
            e.Row.Cells(1).ForeColor = Drawing.Color.Red
            e.Row.Cells(2).ForeColor = Drawing.Color.Red
            e.Row.Cells(3).ForeColor = Drawing.Color.Red
            e.Row.Cells(4).ForeColor = Drawing.Color.Red
        End If

        Select Case e.Row.Cells(4).Text
            Case "Sunday" : e.Row.Cells(4).ForeColor = Drawing.Color.Red
            Case "Saturday" : e.Row.Cells(4).ForeColor = Drawing.Color.OrangeRed
                'Case "Monday" : e.Row.Cells(6).ForeColor = System.Drawing.ColorTranslator.FromHtml("#B2F0FF")
                'Case "Tuesday" : e.Row.Cells(6).ForeColor = System.Drawing.ColorTranslator.FromHtml("#B2E0FF")
                'Case "Wednesday" : e.Row.Cells(6).ForeColor = System.Drawing.ColorTranslator.FromHtml("#B2D0FF")
                'Case "Thursday" : e.Row.Cells(6).ForeColor = System.Drawing.ColorTranslator.FromHtml("#B2C0FF")
                'Case "Friday" : e.Row.Cells(6).ForeColor = System.Drawing.ColorTranslator.FromHtml("#B2B0FF")
        End Select
    End Sub

    Protected Sub GridView1_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView1.RowCommand
        ' RowCommand
        Dim WhichDays As String = ""
        Dim WhichOrder As String = ""
        Dim WhichDaysNumeric As Integer = -1
        Dim strSQL As String = ""
        'On Error Resume Next

        'Me.errorlabel.Text = ""
        'Me.ImageButtonMeters_Save.Visible = True
        'Me.ImageButtonMeters_Delete.Visible = True
        'Me.ImageButtonMeters_New.Visible = True
        'Me.ImageButtonMeter_ViewChart.Visible = True
        'Me.ImageButtonMeters_ViewHistory.Visible = True
        'Me.ImageButtonMeters_Add.Visible = False

        Me.ImageButtonCreateTheMeters.Visible = False
        Me.ImageButtonReadTheMeters.Visible = False
        Me.ImageButtonMeterReaders.Visible = False
        '=========== Navigation Icons Top Right ===============
        Me.ImageButtonReturnToChoice.Visible = False
        Me.ImageButtonReturnToMeters.Visible = True
        '=========== Navigation Icons Top Right ===============
        If IsNumeric(e.CommandArgument) = True Then
            Me.errorlabel5.Text = ""
            Me.ntxtEachdayStart.Text = ""
            Me.ntxtEveryWeekStart.Text = ""
            Me.nWeeklyYear.Text = ""
            Me.nMonthlyYear.Text = ""
            Me.PanelActualMeterReadingsNew.Visible = False
            Me.PanelActualMeterReadingsEditor.Visible = False
            Me.PanelGridView2Graph.Style("TOP") = "270px"
            Me.PanelGridView2Graph.Visible = True
            Dim iSelectedText As String = ""
            Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            Dim mySelectedRow As GridViewRow = GridView1.Rows(index)
            Me.hiddenMeterID.Value = mySelectedRow.Cells(1).Text
            Call WhichMeter(mySelectedRow.Cells(2).Text)
            '    strSQL = "  select  TOP 1 tblCustomer_ScheduledMeters.ID, tblCustomer_Meters.scheduletype  from dbo.tblCustomer_ScheduledMeters  "
            '    strSQL = strSQL & "  inner join tblCustomer_Meters on tblCustomer_Meters.id = tblCustomer_ScheduledMeters.MeterID "
            '    strSQL = strSQL & " where tblCustomer_ScheduledMeters.meterid =" & Me.hiddenMeterID.Value
            '      Me.SQL_MeterGeneral.SelectCommand = strSQL
            Me.SQL_MeterGeneral.SelectCommand = "SELECT  TOP 1 ID from dbo.tblCustomer_ScheduledMeters where meterid=" & Me.hiddenMeterID.Value

            Dim myItem As System.Data.DataView = CType(SQL_MeterGeneral.Select(DataSourceSelectArguments.Empty), System.Data.DataView)
            If myItem.Count > 0 Then
            Else
                ' SELECT 'Current Day ' + '(' +  DATENAME(dw,GETDATE())   + ' )'  + ' Current Week (' + DATENAME(week,GETDATE())   + ') ' + ' Curent Month (' + CONVERT(VARCHAR(2),MONTH(GETDATE()) ) + ')' AS'Title'
                strSQL = "SELECT  DATENAME(week,GETDATE()) as [WK] , MONTH(GETDATE()) as [M]  ,   'Current Day ' + '(' +  DATENAME(dw,GETDATE())   + ' )'  + ' Current Week (' + DATENAME(week,GETDATE())   + ') ' + ' Current Month (' + CONVERT(VARCHAR(2),MONTH(GETDATE()) ) + ')'  AS  [TITLE]    ,   ScheduleType from dbo.tblCustomer_Meters where ID=" & Me.hiddenMeterID.Value

                Me.SQL_MeterGeneral.SelectCommand = strSQL
                Dim myTwo As System.Data.DataView = CType(SQL_MeterGeneral.Select(DataSourceSelectArguments.Empty), System.Data.DataView)

                Me.nDailyDate.Enabled = "False"
                Me.DropDownListMonthNumbers.Enabled = "False"
                Me.DropDownListWeekDayName.Enabled = "False"
                Me.DropDownListWeekNumbers.Enabled = "False"

                For Each rowProduct As System.Data.DataRowView In myTwo
                    Me.Current.Text = Trim(rowProduct("Title"))
                    Me.DropDownListWeekNumbers.SelectedIndex = (Trim(rowProduct("WK")) - 1)
                    Me.DropDownListMonthNumbers.SelectedIndex = (Trim(rowProduct("M")) - 1)

                    Select Case Trim(rowProduct("ScheduleType"))
                        Case "D"
                            Me.nDailyDate.Enabled = "True"
                            Me.ntxtEachdayStart.Enabled = "True"
                        Case "M"
                            Me.DropDownListMonthNumbers.Enabled = "True"
                            Me.nMonthlyYear.Enabled = "True"
                            ' will be  '01/' + right("00" &  selected month,2)  + '/' + Selected Year 
                            'EXEC dbo.ReturnMonthNumbers  '01/12/2009' , 0
                        Case "W"
                            Me.DropDownListWeekDayName.Enabled = "True"
                            Me.ntxtEveryWeekStart.Enabled = "True"
                            'EXEC dbo.ReturnWeekNumbers_YearAndWeekNumber '2009',52, (1) is not critical
                        Case "WK"
                            Me.DropDownListWeekNumbers.Enabled = "True"
                            Me.nWeeklyYear.Enabled = "True"
                            ' EXEC dbo.ReturnWeekNumbers_YearAndWeekNumber '2009',52,1
                    End Select
                    Exit For
                Next

                Me.PanelActualMeters.Visible = False
                Me.labelFirstmeterRead.Text = Trim(mySelectedRow.Cells(2).Text)
                ' oliver
 
                Me.ImageButtonReturnToChoice.Visible = False
                Me.ImageButtonReturnToMeters.Visible = False
                Me.ImageButtonReturnToTopLevel.Visible = False

                Me.PanelActualFirstReadingsEditor.Visible = True
                Me.ImageButtonReturnToMeters.Visible = False
                Exit Sub
            End If



            Call Me.recalculate(Me.RadioButtonList1.SelectedValue, Me.RadioButtonList2.SelectedValue, Me.RadioOutStanding.SelectedValue)
            Call Me.CreateNextReadingDate()
            Call Me.ProcessChart()

            Me.PanelActualMeters.Visible = False
            Me.PanelActualMeterReadings.Visible = True
            Me.PanelGridView2.Visible = True


        Else

        End If
    End Sub

    Protected Sub ImageButtonCreateTheMeters_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonCreateTheMeters.Click
        Me.ImageButtonCreateTheMeters.Visible = False
        Me.ImageButtonReadTheMeters.Visible = False
        Me.ImageButtonMeterReaders.Visible = False

        Me.PanelActualMeterReadings.Visible = False
        Me.PanelActualMeters.Visible = False

        Me.DropDownTypeOf.SelectedIndex = 0
        'Me.DropDownList3.SelectedIndex = 0
        Me.txtIdentifier.Text = ""
        Me.txtInstalleddate.Text = "01/01/1900"
        Me.txtLastserviced.Text = "01/01/1900"
        Me.txtMeterName.Text = ""
        Me.txtMeterLocation.Text = ""
        Me.PanelPureMeters.Visible = True
        Me.ImageButtonReturnToTopLevel.Visible = True


    End Sub

    Protected Sub ImageButtonReadTheMeters_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonReadTheMeters.Click

        Call PushTheMeterReadButton()


    End Sub


    Protected Function PushTheMeterReadButton() As String
        Me.LabelNoMeters.Visible = False

        If Trim(Session("TheCompanyID")) = "" Then
            MsgBox("Please select a valid Company", MsgBoxStyle.Information, "Validation error")
        Else
            If iMethodType = "Customer" Then
                If GatherNewDetailsForCompany() = 0 Then
                    '  Me.LabelNoMeters.Text = "No Customer meter(s) available"
                    '  Me.LabelNoMeters.Visible = True
                Else
                End If

                Me.ImageButtonCreateTheMeters.Visible = False
                Me.ImageButtonReadTheMeters.Visible = False
                Me.ImageButtonMeterReaders.Visible = False
                Me.PanelActualMeterReadings.Visible = False
                Me.PanelActualMeters.Visible = True
                Me.ImageButtonReturnToChoice.Visible = True
            Else
                If GatherNewDetails() = 0 Then
                    ' Me.LabelNoMeters.Text = "No Site meter(s) available"
                    ' Me.LabelNoMeters.Visible = True
                Else
                End If

                Me.ImageButtonCreateTheMeters.Visible = False
                Me.ImageButtonReadTheMeters.Visible = False
                Me.ImageButtonMeterReaders.Visible = False
                Me.PanelActualMeterReadings.Visible = False
                Me.PanelActualMeters.Visible = True
                Me.ImageButtonReturnToChoice.Visible = True

            End If

        End If
        PushTheMeterReadButton = "OK"
    End Function

    Protected Function ProcessChart() As String
        On Error Resume Next
        Dim myConnectionString As String = "Provider=SQLNCLI.1;Integrated Security=SSPI;Persist Security Info=False;Initial Catalog=portal.utilitymasters.co.uk;Data Source=uk-ed0-sqlcl-01.MCEG.local"
        Dim iIndex As Integer = 0
        Dim iItem As String = ""
        Dim strSQL As String = ""
        Dim ItemToShow As String = ""

        strSQL = " SELECT "
        strSQL = strSQL & " dbo.tblCustomer_Meters.ID , "
        strSQL = strSQL & " dbo.tblCustomer_Meters.TypeOf , "
        strSQL = strSQL & "  dbo.tblCustomer_UnitsOfMeasure.iName ,"
        strSQL = strSQL & "  dbo.tblCustomer_UnitsOfMeasure.iDescription ,"
        strSQL = strSQL & "  dbo.tblCustomer_UnitsOfMeasure.iMultiplier ,"
        strSQL = strSQL & "  dbo.tblCustomer_UnitsOfMeasure.iMultiplierRequired ,"
        strSQL = strSQL & "  dbo.tblCustomer_UnitsOfMeasure.iConversionRequired ,"
        strSQL = strSQL & "   dbo.tblCustomer_UnitsOfMeasure.iFactor ,"
        strSQL = strSQL & "             dbo.tblCustomer_UnitsOfMeasure.iOperator"
        strSQL = strSQL & "             FROM dbo.tblCustomer_Meters "

        strSQL = strSQL & " INNER JOIN dbo.tblCustomer_MeterTypes ON dbo.tblCustomer_MeterTypes.ID= dbo.tblCustomer_Meters.MeterTypeID "
        strSQL = strSQL & " INNER JOIN dbo.tblCustomer_UnitsOfMeasure  ON tblCustomer_UnitsOfMeasure.ID=tblCustomer_MeterTypes.UnitOfMeasureID"

        '   strSQL = strSQL & "  INNER JOIN dbo.tblCustomer_MeterTypes ON dbo.tblCustomer_MeterTypes.UnitOfMeasureID= dbo.tblCustomer_Meters.MeterTypeID"
        '   strSQL = strSQL & "  INNER JOIN dbo.tblCustomer_UnitsOfMeasure ON dbo.tblCustomer_UnitsOfMeasure.Id = dbo.tblCustomer_MeterTypes.UnitOfMeasureID"

        strSQL = strSQL & "     WHERE dbo.tblCustomer_Meters.ID =" & Me.hiddenMeterID.Value

        Dim mySelectQuery As String = strSQL
        Dim myConnection As New OleDbConnection(myConnectionString)
        Dim myCommand As New OleDbCommand(mySelectQuery, myConnection)
        myCommand.Connection.Open()
        Dim myReader As OleDbDataReader = myCommand.ExecuteReader(CommandBehavior.CloseConnection)

        Dim ChartType As String = ""
        myReader.Read()

        Dim istrSQL As String = ""
        istrSQL = "  EXEC dbo.spCustomer_ReadingsToConsumption "
        istrSQL = istrSQL & "'" & Me.hiddenMeterID.Value & "'" & ","
        istrSQL = istrSQL & "'" & Me.HiddenFieldStartDate.Value & "'" & ","
        istrSQL = istrSQL & "'" & Me.HiddenFieldFinishDate.Value & "'" & ","

        istrSQL = istrSQL & "'" & "" & "'" & ","
        istrSQL = istrSQL & "'" & myReader("iMultiplierRequired") & "'" & ","
        istrSQL = istrSQL & "'" & myReader("iMultiplier") & "'" & ","
        istrSQL = istrSQL & "'" & myReader("iOperator") & "'" & ","
        istrSQL = istrSQL & "'" & myReader("iConversionRequired") & "'" & ","
        istrSQL = istrSQL & "'" & myReader("iFactor") & "'"

        '        myReader.Close()

        Dim Output_iMultiplier As String = myReader("iMultiplierRequired")
        Dim Output_iMultiplierRequired As String = myReader("iMultiplierRequired")
        Dim Output_iConversionRequired As String = myReader("iConversionRequired")
        Dim Output_iFactor As String = myReader("iFactor")
        Dim Output_iOperator As String = myReader("iOperator")
        Dim Output_iInputTitle As String = myReader("iName")
        Dim Output_iOutputTitle As String = myReader("iDescription")

        Dim imySelectQuery As String = istrSQL
        Dim imyConnection As New OleDbConnection(myConnectionString)
        Dim imyCommand As New OleDbCommand(imySelectQuery, imyConnection)
        imyCommand.Connection.Open()
        Dim imyReader As OleDbDataReader = imyCommand.ExecuteReader(CommandBehavior.CloseConnection)

        Chart1.Width = 650
        Chart1.Height = 280
        Chart1.Series.Clear()
        Chart1.Series.Add("Default")


        'Chart1.UI.Toolbar.Enabled = True
        'Chart1.UI.Toolbar.Position.Auto = True
        'Chart1.PaletteCustomColors = colorSet
        'Chart1.ChartAreas("Default").BackColor = Color.Silver
        'Chart1.BorderColor = Color.Black
        'Chart1.BorderStyle = ChartDashStyle.Solid
        'Chart1.BorderWidth = 2
        'Chart1.BorderSkin.SkinStyle = BorderSkinStyle.Emboss
        'Chart1.UI.Toolbar.Docking = ToolbarDocking.Top


        Dim commands As CommandCollection = Chart1.UI.Commands
        Dim cmdProperties As Command = commands.FindCommand(ChartCommandType.Properties)
        Dim cmdPalette As Command = commands.FindCommand(ChartCommandType.SelectPaletteGroup)
        Dim cmdChartType As Command = commands.FindCommand(ChartCommandType.SelectChartGroup)
        Dim cmdToggle3D As Command = commands.FindCommand(ChartCommandType.Toggle3D)

        cmdProperties.Visible = False
        cmdPalette.Visible = False
        cmdChartType.Visible = False
        cmdToggle3D.Visible = False

        Chart1.UI.Toolbar.Placement = ToolbarPlacement.InsideChart
        Chart1.UI.Toolbar.BorderSkin.SkinStyle = BorderSkinStyle.Emboss
        Chart1.UI.Toolbar.BorderSkin.FrameBackColor = Color.Black
 
        With Me.Chart1
            .BorderColor = Color.Black
            .BorderStyle = ChartDashStyle.Solid
            .BorderWidth = 2
            .BorderSkin.SkinStyle = BorderSkinStyle.Emboss

            .UI.Toolbar.Enabled = True
            .UI.Toolbar.Docking = ToolbarDocking.Top
            .UI.Toolbar.Placement = ToolbarPlacement.InsideChart
            .UI.Toolbar.BorderSkin.SkinStyle = BorderSkinStyle.Emboss
            .UI.Toolbar.BorderSkin.FrameBackColor = Color.Black
        End With
  
 

        'Chart1.Titles.Clear()
        'Chart1.Titles.Add("TopTitle")

        If myReader("TypeOf") = "C" Then
            Chart1.Titles("TopTitle").Text = "Consumption figures for period "
        Else
            Chart1.Titles("TopTitle").Text = "Meter reading values for period "
        End If

        Chart1.ChartAreas.Clear()
        Chart1.ChartAreas.Add("Default")
        Chart1.ChartAreas("Default").AxisX.MajorGrid.Enabled = False
        Chart1.ChartAreas("Default").AxisX.MinorGrid.Enabled = False

        Chart1.ChartAreas("Default").AxisY.MajorGrid.Enabled = False
        Chart1.ChartAreas("Default").AxisY.MinorGrid.Enabled = False

        If myReader("TypeOf") = "C" Then
            Chart1.ChartAreas("Default").AxisY.Title = "Consumption"
        Else
            Chart1.ChartAreas("Default").AxisY.Title = "Meter readings "
        End If

        Chart1.ChartAreas("Default").AxisY.TitleColor = Color.Blue
        Chart1.ChartAreas("Default").AxisX.Title = "Period dates"
        Chart1.ChartAreas("Default").AxisX.TitleColor = Color.Blue

        Me.Chart1.ChartAreas("Default").CursorX.UserEnabled = True

        Me.Chart1.ChartAreas("Default").AxisX.View.MinSize = 15
        If Not Me.IsPostBack Then
            Me.Chart1.ChartAreas("Default").AxisX.View.Position = 20.0
            Me.Chart1.ChartAreas("Default").AxisX.View.Size = 35.0
        End If

        Chart1.ChartAreas("Default").AxisX.ScrollBar.Enabled = True
        Chart1.AJAXZoomEnabled = True

        Chart1.Legends.Clear()
        '  Chart1.DataBindTable(myReader, "RecordedDate")
        ' Chart1.Series("Default").XValueIndexed = True

        Chart1.Series("Default").Color = Color.Black
        Chart1.Series("Default").XValueType = ChartValueTypes.DateTime

        Chart1.Series("Default").ToolTip = "#VALY" + ControlChars.Lf + "#VALX"

        Chart1.Series("Default").Type = SeriesChartType.Line
        '   Chart1.Series("Default").Type = SeriesChartType.Column
        '   Chart1.ChartAreas("Default").AxisY.Interval = 50
        '   Chart1.ChartAreas("Default").Position.Height = 20
        '    Chart1.ChartAreas("Default").BackColor = Color.Transparent
        Chart1.ChartAreas("Default").AxisX.IntervalType = DateTimeIntervalType.Days
        Chart1.ChartAreas("Default").AxisX.Interval = 1
        '  Chart1.ChartAreas("Default").AxisX.LabelStyle.Format = "ddd"
        '  Chart1.ChartAreas("Default").AxisX.LabelStyle.Format = "d-MM"
        Chart1.ChartAreas("Default").AxisX.LabelStyle.FontAngle = 65
        Chart1.ChartAreas("Default").AxisY.LabelStyle.Font = New Font("Arial", 7)
        Chart1.ChartAreas("Default").AxisX.LabelStyle.Font = New Font("Arial", 7)

        Chart1.Series("Default").MarkerStyle = MarkerStyle.Circle
        Chart1.Series("Default").MarkerSize = 8
        Chart1.Series("Default").MarkerColor = Color.Red
        Chart1.Series("Default").BorderWidth = 1
        Chart1.Series("Default").BorderColor = Color.DarkGreen
        ' Chart1.Series("Default").Points.DataBindXY(myReader, "One", myReader, "Two")

        If myReader("TypeOf") = "C" Then
            Chart1.Series("Default").Points.DataBindXY(imyReader, "Entry", imyReader, "Consumption")
        Else
            Chart1.Series("Default").Points.DataBindXY(imyReader, "Entry", imyReader, "Readings")
        End If

        If Session("WhichOrder") = "Assending" Or Session("WhichOrder") = "" Then
            '            Chart1.Series("Default").Sort(PointsSortOrder.Ascending)
            '  Chart1.DataManipulator.Sort(PointsSortOrder.Ascending, "AxisLabel", "Default")
            Chart1.Series("Default").Sort(PointsSortOrder.Ascending, "AxisLabel")

        Else
            '           Chart1.Series("Default").Sort(PointsSortOrder.Descending)
            ' Chart1.DataManipulator.Sort(PointsSortOrder.Descending, "AxisLabel", "Default")
            Chart1.Series("Default").Sort(PointsSortOrder.Descending, "AxisLabel")

        End If

        Chart1.AlignDataPointsByAxisLabel()





        myReader.Close()
        myConnection.Close()

        ProcessChart = "Ok"
    End Function

    Protected Function recalculate(ByVal what As String, ByVal OrderBy As String, ByVal FilterBy As String) As String
        On Error Resume Next
        Dim WhichDays As String = ""
        Dim WhichOrder As String = ""
        Dim WhichDaysNumeric As Integer = -1
        Dim iRequired As String = "No"

        Select Case what
            Case "12 months"
                WhichDays = "-366"
                WhichDaysNumeric = -366
            Case "6 months"
                WhichDays = "-" & (366 / 2)
                WhichDaysNumeric = -366 / 2
            Case "30 days"
                WhichDays = "-30"
                WhichDaysNumeric = -30
            Case "All"
                WhichDays = "-10000"
                WhichDaysNumeric = -10000
        End Select

        Select Case Me.RadioButtonList2.SelectedValue
            Case "Assending"
                WhichOrder = 0
                Session("WhichOrder") = "Assending"
            Case "Descending"
                WhichOrder = 1
                Session("WhichOrder") = "Descending"

        End Select

        Select Case Me.RadioOutStanding.SelectedValue
            Case "ReadRequired"
                iRequired = "Y"
            Case "AllMeters"
                iRequired = "N"
        End Select


        Me.HiddenFieldStartDate.Value = DateAdd(DateInterval.Day, WhichDaysNumeric, Now.Date)
        Me.HiddenFieldFinishDate.Value = Now.Date

        Me.PanelActualMeterReadingsNew.Visible = False
        Me.PanelActualMeterReadingsEditor.Visible = False
        '  blind the new readings
        Me.PanelGridView2Graph.Visible = True

        Dim strSQl As String = " EXEC dbo.spCustomer_MeterReadings_Flexible " & Me.hiddenMeterID.Value & "," & WhichDays & "," & WhichOrder & "," & "'" & iRequired & "'"
        Me.SQL_MeterReadings.SelectCommand = strSQl
        Me.GridView2.DataBind()

        Call ProcessChart()
        recalculate = "Ok"
    End Function

    Protected Sub ImageButtonReturnToMeters_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonReturnToMeters.Click
        Me.PanelActualMeterReadings.Visible = False
        Me.PanelActualMeters.Visible = True
        Me.ImageButtonReturnToChoice.Visible = True
        Me.ImageButtonReturnToMeters.Visible = False
    End Sub

    Protected Sub ImageButtonQuickUpdate_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonQuickUpdate.Click
        Dim strSQL As String = ""
        Dim iStrSQL As String = ""
        Dim nReturn As Integer = 0
        Dim iTime As String = ""
        Dim NiSH As String = ""
        If Not IsNumeric(Trim(entryreading.Text)) Or Me.entryreading.Text = "" Then
            'MsgBox("Please enter a valid numeric value", MsgBoxStyle.Critical, "Validation error")

            Me.errorlabel.Text = "Status: Please enter a valid numeric value"
            entryreading.Focus()
            Exit Sub
        End If

        If Not IsDate(Me.entrydate.Text) = True Or Me.entrydate.Text = "" Then
            'MsgBox("Please enter a valid date", MsgBoxStyle.Critical, "Validation error")

            Me.errorlabel.Text = "Status: Please enter a valid date"
            entrydate.Focus()
            Exit Sub
        End If

        iTime = Me.entrytime.Text
        If IsDate(iTime) = True Then
        Else
            'MsgBox("Please enter a valid Time", MsgBoxStyle.Critical, "Validation error")

            Me.errorlabel.Text = "Status: Please enter a valid Time"
            entrytime.Focus()
            Exit Sub
        End If

        NiSH = ""
        NiSH = NiSH & Year(Trim(entrydate.Text))
        NiSH = NiSH & "-" & Right("00" & Month(Trim(entrydate.Text)), 2)
        NiSH = NiSH & "-" & Right("00" & Day(Trim(entrydate.Text)), 2)

        iStrSQL = "   Select MeterID,RecordedDate  "
        iStrSQL = iStrSQL & " From tblCustomer_ScheduledMeters Where MeterID=" & Me.hiddenMeterID.Value
        iStrSQL = iStrSQL & "  AND CONVERT(CHAR(10),recordeddate,120) = " & "'" & Trim(NiSH) & "'"
        Me.SQL_MeterGeneral.SelectCommand = iStrSQL

        Dim ivwExpensiveItems As System.Data.DataView = CType(SQL_MeterGeneral.Select(DataSourceSelectArguments.Empty), System.Data.DataView)
        If ivwExpensiveItems.Count = 0 Then  ' not found

            '  nReturn = MsgBox("This entry Does Not Exist !!!" & vbNewLine & " Would you like to Create it Now ?", MsgBoxStyle.YesNo, "Validation warning")
            '    If nReturn = 6 Then

            iStrSQL = "DELETE FROM tblCustomer_ScheduledMeters WHERE MeterID=" & Me.hiddenMeterID.Value
            iStrSQL = iStrSQL & "  AND CONVERT(CHAR(10),recordeddate,120) = " & "'" & Trim(NiSH) & "'"
            Me.SQL_MeterGeneral.SelectCommand = iStrSQL

            strSQL = "  INSERT INTO tblCustomer_ScheduledMeters (CompanyID,MeterID,Figure,RecordedDate,PersonnelID,TagNote) "
            strSQL = strSQL & " Values("
            strSQL = strSQL & Me.hiddenCompanyID.Value & ","
            strSQL = strSQL & Me.hiddenMeterID.Value & ","
            strSQL = strSQL & "'" & Trim(Me.entryreading.Text) & "'" & ","
            strSQL = strSQL & "'" & Trim(Me.entrydate.Text) & " " & Trim(Me.entrytime.Text) & "'" & ","
            strSQL = strSQL & Me.PersonnelListing.SelectedValue & ","
            strSQL = strSQL & "'" & XY(Trim(Me.txtTagNote.Text)) & "'"
            strSQL = strSQL & ")"
            Me.SQL_MeterReadings.InsertCommand = strSQL
            Me.SQL_MeterReadings.Insert()

            Dim NewYear As String = ""
            NewYear = Year(Trim(Me.entrydate.Text))

            'If Me.DropDownListMeterReadingsYear.SelectedValue = NewYear Then
            '    ' No change on Year , so no need to Re-Populate the Year Dropdown
            '    ' But we still need to re-Populate the grid
            '    Me.SQL_MeterReadings.SelectCommand = " EXEC dbo.spCustomer_MeterReadings  " & "'" & NewYear & "'" & " , " & Me.hiddenMeterID.Value
            'Else
            '    Me.SQL_MeterReadingsGroupYEAR.SelectCommand = " EXEC dbo.spCustomer_MeterReadingsYEAR " & Me.hiddenMeterID.Value
            '    Me.DropDownListMeterReadingsYear.Items.Clear()
            '    Me.DropDownListMeterReadingsYear.DataBind()
            'End If

            'Me.SQL_MeterReadings.SelectCommand = " EXEC dbo.spCustomer_MeterReadings  " & "'" & NewYear & "'" & " , " & Me.hiddenMeterID.Value
            Me.PersonnelListing.SelectedIndex = 0

            entryreading.Text = ""
            entrydate.Text = ""
            txtTagNote.Text = ""
            '==================

            'strSQL = " EXEC dbo.spCustomer_MeterReadings_Flexible " & Me.hiddenMeterID.Value & "," & WhichDays & "," & WhichOrder
            'Me.SQL_MeterReadings.SelectCommand = strSQL
            'Me.GridView2.DataBind()
            Call Me.recalculate(Me.RadioButtonList1.SelectedValue, Me.RadioButtonList2.SelectedValue, Me.RadioOutStanding.SelectedValue)

            Call ProcessChart()
            '=================
            Exit Sub

            '  Else
            '      Exit Sub ' just leave well alone
            '  End If

        Else
        End If

        ' Get the Personnel Meter Reader from dropdown
        '  Me.DropDownListPersonnel.SelectedValue
        strSQL = "UPDATE [tblCustomer_ScheduledMeters] "
        strSQL = strSQL & " SET [Figure] = " & "'" & Trim(entryreading.Text) & "'"
        strSQL = strSQL & " , [PersonnelID] = " & Me.PersonnelListing.SelectedValue
        strSQL = strSQL & " , [RecordedDate] = " & "'" & Trim(entrydate.Text) & " " & Trim(Me.entrytime.Text) & "'"
        strSQL = strSQL & " , [TagNote] = " & "'" & XY(Trim(txtTagNote.Text)) & "'"
        strSQL = strSQL & " WHERE ID = " & Me.HiddenScheduledMeter.Value

        Me.SQL_MeterSelect.UpdateCommand = strSQL
        Me.SQL_MeterSelect.Update()
        '  MeterGridView.DataBind()

        'iStrSQL = "   Select ID,Figure,RecordedDate,DATENAME(DW,RecordedDate) as [DayName]  "
        'iStrSQL = iStrSQL & " From tblCustomer_ScheduledMeters Where MeterID=" & Me.HiddeniIndex.Value.ToString
        'iStrSQL = iStrSQL & " AND recordeddate LIKE '%" & Me.DropMeterSelection.SelectedValue & "%'"
        'iStrSQL = iStrSQL & " ORDER BY RecordedDate DESC "
        'Me.SQLScheduleMeters.SelectCommand = iStrSQL



        'strSQL = " EXEC dbo.spCustomer_MeterReadings_Flexible " & Me.hiddenMeterID.Value & "," & WhichDays & "," & WhichOrder
        'Me.SQL_MeterReadings.SelectCommand = strSQl
        'Me.GridView2.DataBind()
        Call Me.recalculate(Me.RadioButtonList1.SelectedValue, Me.RadioButtonList2.SelectedValue, Me.RadioOutStanding.SelectedValue)
        Call ProcessChart()

        '        Me.SQL_MeterReadings.SelectCommand = " EXEC dbo.spCustomer_MeterReadings  " & "'" & Me.DropDownListMeterReadingsYear.SelectedValue & "'" & " , " & Me.hiddenMeterID.Value
        Me.PersonnelListing.SelectedIndex = 0

        entryreading.Text = ""
        entrydate.Text = ""
        entrytime.Text = ""
        txtTagNote.Text = ""

        Me.PanelActualMeterReadingsEditor.Visible = False
        Me.PanelGridView2Graph.Visible = True
        Me.PanelActualMeterReadingsNew.Visible = False


    End Sub

    Protected Function XY(ByVal what As String) As String
        XY = Replace(what, "'", "''")
    End Function

    Protected Sub ImageButtonReturnToChoice_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonReturnToChoice.Click
        Me.PanelActualMeters.Visible = False
        Me.ImageButtonCreateTheMeters.Visible = True
        Me.ImageButtonReadTheMeters.Visible = True
        Me.ImageButtonMeterReaders.Visible = True
        Me.PanelActualMeterReadingsNew.Visible = False
        Me.ImageButtonReturnToChoice.Visible = False
    End Sub

    Protected Sub RadioButtonList1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RadioButtonList1.SelectedIndexChanged
        Call Me.recalculate(Me.RadioButtonList1.SelectedValue, Me.RadioButtonList2.SelectedValue, Me.RadioOutStanding.SelectedValue)
    End Sub

    Protected Sub RadioButtonList2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RadioButtonList2.SelectedIndexChanged
        Call Me.recalculate(Me.RadioButtonList1.SelectedValue, Me.RadioButtonList2.SelectedValue, Me.RadioOutStanding.SelectedValue)

    End Sub

    Protected Sub RadioOutStanding_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RadioOutStanding.SelectedIndexChanged
        Call Me.recalculate(Me.RadioButtonList1.SelectedValue, Me.RadioButtonList2.SelectedValue, Me.RadioOutStanding.SelectedValue)
    End Sub

    Protected Sub ImageButtonCloseEditWindow_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonCloseEditWindow.Click
        Me.PanelActualMeterReadingsEditor.Visible = False
        Me.PanelActualMeterReadingsNew.Visible = False
        Me.PanelGridView2Graph.Visible = True
        Me.PanelGridView2Graph.Style("TOP") = "270px"
    End Sub

    'Protected Sub LinkButtonEnterNewReading_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButtonEnterNewReading.Click
    '    '========================================================================================================================
    '    Dim iPeriod As String = ""
    '    Dim iSPLIT As String()
    '    Dim iSTARTdate As String = ""
    '    iSPLIT = Split(Me.NextReadDate.Text, "-")
    '    iSTARTdate = iSPLIT(1)
    '    Select Case Me.HiddenFieldScheduleType.Value
    '        Case "D"
    '            iPeriod = "until tomorrow - Thank You"
    '        Case "M" ' 
    '            iSPLIT = Split(Me.NextReadDate.Text, "-")
    '            iSTARTdate = iSPLIT(2)
    '            iPeriod = "until next month - Thank You"
    '        Case "W"
    '            iPeriod = "until next week - Thank You"
    '        Case "WK"
    '            iPeriod = "until next week - Thank You"
    '        Case Else
    '    End Select

    '    If CDate(iSTARTdate) > Now Then
    '        ' MsgBox("You may NOT Continue as your ""Next Reading"" is NOT due" & vbNewLine & iPeriod, MsgBoxStyle.Information, "Reading validation notice ")

    '        '  Me.NextReadDate.Text = "Next reading date will be - " & DateAdd(DateInterval.Day, 1, CDate(CurrentDateIs))
    '        '  Me.NextReadingDate.Text
    '        Me.NextReadingDate.Visible = False
    '        Me.errorlabeldateexceeded.Text = "You cannot continue as your ""Next Reading"" is NOT due" & vbNewLine & iPeriod
    '        Me.errorlabeldateexceeded.Visible = True
    '        ' here it is
    '        Exit Sub
    '    Else
    '        Me.errorlabeldateexceeded.Visible = False
    '        Me.NextReadingDate.Visible = True
    '    End If
    '    '========================================================================================================================
    '    Me.PanelActualMeterReadingsEditor.Visible = False
    '    Me.PanelGridView2Graph.Visible = False
    '    Me.sEntryReading.Text = ""
    '    Me.sEntryTime.Text = Mid(Now.TimeOfDay.ToString, 1, 8)
    '    Me.stxtTagNote.Text = ""
    '    '   Me.sPersonnelListing.SelectedIndex = 0
    '    Me.HiddenFieldEnterNewReading.Value = "True"
    '    ' Need to validate the input here jusr check fo rthe actual date
    '    ' if less than today then stop user from entering - end of
    '    ' NextReadDate
    '    ' split and get date
    '    ' D split(1) 
    '    ' M split(1) 
    '    ' W - specific day will be split(2) 
    '    ' WK split(1) 

    '    Me.PanelActualMeterReadingsNew.Visible = True
    'End Sub

    Protected Function CreateNextReadingDate() As String
        On Error Resume Next
        Dim myConnectionString As String = "Provider=SQLNCLI.1;Integrated Security=SSPI;Persist Security Info=False;Initial Catalog=portal.utilitymasters.co.uk;Data Source=uk-ed0-sqlcl-01.MCEG.local"
        Dim strSQL As String = ""
        '   strSQL = strSQL & " SELECT  smith.TheDate FROM  ( SELECT  tblCustomer_Meters.scheduletype AS 'Thedate' FROM dbo.tblCustomer_Meters  WHERE ID=" & Me.hiddenMeterID.Value & "   ) AS smith  "
        Dim CurrentDateIs As String = ""
        Dim TheDayIS As String = ""
        Select Case Me.HiddenFieldScheduleType.Value
            Case "D"
                strSQL = " SELECT john.TheDate  FROM  (  SELECT  CONVERT(VARCHAR(20),MAX(RecordedDate),103)   AS 'TheDate'  FROM dbo.tblCustomer_ScheduledMeters   WHERE MeterID=" & Me.hiddenMeterID.Value & " 	) AS john"
            Case "W"
                '  strSQL = "SELECT TOP 1 *   FROM dbo.tblCustomer_ScheduledMeters   WHERE MeterID= " & Me.hiddenMeterID.Value
                strSQL = " SELECT TOP 1 dbo.tblCustomer_ScheduledMeters.* , tblCustomer_Meters.Readingmethod    FROM dbo.tblCustomer_ScheduledMeters   "
                strSQL = strSQL & "  INNER JOIN dbo.tblCustomer_Meters ON dbo.tblCustomer_Meters.ID = tblCustomer_ScheduledMeters.MeterID "
                strSQL = strSQL & " WHERE MeterID=" & Me.hiddenMeterID.Value & "  ORDER BY dbo.tblCustomer_ScheduledMeters.RecordedDate DESC "
            Case "M"
                strSQL = "SELECT TOP 1 *   FROM dbo.tblCustomer_ScheduledMeters   WHERE MeterID= " & Me.hiddenMeterID.Value & "  ORDER BY dbo.tblCustomer_ScheduledMeters.RecordedDate DESC "
            Case "WK"
                strSQL = "SELECT TOP 1 *   FROM dbo.tblCustomer_ScheduledMeters   WHERE MeterID= " & Me.hiddenMeterID.Value & "  ORDER BY dbo.tblCustomer_ScheduledMeters.RecordedDate DESC "
        End Select

        Dim mySelectQuery As String = strSQL
        Dim myConnection As New OleDbConnection(myConnectionString)
        Dim myCommand As New OleDbCommand(mySelectQuery, myConnection)
        myCommand.Connection.Open()
        Dim myReader As OleDbDataReader = myCommand.ExecuteReader(CommandBehavior.CloseConnection)

        Select Case Me.HiddenFieldScheduleType.Value
            Case "D"
                myReader.Read()
                CurrentDateIs = myReader("TheDate")
                Me.NextReadDate.Text = "Next reading date will be - " & DateAdd(DateInterval.Day, 1, CDate(CurrentDateIs))
                Me.NextReadingDate.Text = "Your next reading will be -  " & DateAdd(DateInterval.Day, 1, CDate(CurrentDateIs))
                Me.HiddenFieldNextReadingDate.Value = DateAdd(DateInterval.Day, 1, CDate(CurrentDateIs))
            Case "W"
                myReader.Read()
                Dim NewDateIs As String = ""
                Dim TheStringIS As String = Right("00" & Day(myReader("RecordedDate")), 2) & "/" & Right("00" & Month(myReader("RecordedDate")), 2) & "/" & Right("0000" & Year(myReader("RecordedDate")), 4)
                CurrentDateIs = myReader("WeekNumber")
                TheDayIS = myReader("ReadingMethod")
                NewDateIs = DateAdd(DateInterval.Day, 7, CDate(TheStringIS))
                ' need to find the Thursday in this week , then display
                Me.NextReadDate.Text = "Next reading will be Week  " & CurrentDateIs + 1 & "  on " & Me.GetTheWeekName(TheDayIS) & " - " & NewDateIs
                Me.NextReadingDate.Text = "Your next reading will be Week " & CurrentDateIs + 1 & "  on " & Me.GetTheWeekName(TheDayIS) & " - " & NewDateIs
                Me.HiddenFieldNextReadingDate.Value = CurrentDateIs + 1 & " - " & Me.GetTheWeekName(TheDayIS) & " - " & NewDateIs
            Case "M"
                myReader.Read()
                CurrentDateIs = myReader("MonthNumber")
                Me.NextReadDate.Text = "Next reading will be - " & Me.GetTheMonthName(CurrentDateIs + 1) & " - " & Me.AddMonthNumber(myReader("RecordedDate"))
                Me.NextReadingDate.Text = "Your next reading will be - " & Me.GetTheMonthName(CurrentDateIs + 1) & " - " & Me.AddMonthNumber(myReader("RecordedDate"))
                Me.HiddenFieldNextReadingDate.Value = Me.GetTheMonthName(CurrentDateIs + 1) & " - " & Me.AddMonthNumber(myReader("RecordedDate"))
            Case "WK"
                myReader.Read()
                CurrentDateIs = myReader("WeekNumber")
                Me.NextReadDate.Text = "Next reading will be Week - " & CurrentDateIs + 1
                Me.NextReadingDate.Text = "Your next reading will be Week - " & CurrentDateIs + 1
                Me.HiddenFieldNextReadingDate.Value = CurrentDateIs + 1
        End Select
        myConnection.Close()

        CreateNextReadingDate = "OK"
    End Function


    Protected Sub ImageButtonCloseNewWindow_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonCloseNewWindow.Click
        Me.PanelActualMeterReadingsNew.Visible = False
        Me.PanelGridView2Graph.Visible = True
    End Sub

    Protected Sub ImageButtonNewQuickUpdate_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonNewQuickUpdate.Click
        ' the big UPdate
        Dim iSPLIT As String()
        Dim strSQL As String = ""
        Dim iTime As String = ""
        '   Dim NumberOfDaysARE As Integer
        Dim TheDateAndTheTime As String = ""
        Dim iSTARTdate As Date
        Dim iFINISHdate As Date
        Dim i As Integer = 0
        Dim DuffDate As String = ""

        If Not IsNumeric(Trim(sEntryReading.Text)) Or Me.sEntryReading.Text = "" Then
            Me.errorlabel1.Text = "Status: Please enter a valid Numeric reading value"
            sEntryReading.Focus()
            Exit Sub
        End If

        iTime = Me.sEntryTime.Text
        If IsDate(iTime) = True Then
        Else
            Me.errorlabel1.Text = "Status: Please enter a valid Time"
            sEntryTime.Focus()
            Exit Sub
        End If

        TheDateAndTheTime = Me.HiddenFieldNextReadingDate.Value & " " & iTime

        If Me.HiddenFieldEnterNewReading.Value = "True" Then
            iSPLIT = Split(Me.NextReadDate.Text, "-")
            iSTARTdate = iSPLIT(1)
            iFINISHdate = CDate(Mid(Now.Date, 1, 10))
            Select Case Me.HiddenFieldScheduleType.Value
                Case "D"
                    DuffDate = DateAdd(DateInterval.Day, 1, iFINISHdate) & " " & "00:00:00"
                    strSQL = "  INSERT INTO tblCustomer_ScheduledMeters (CompanyID,MeterID,Figure,ConfirmRead,RecordedDate,PersonnelID,TagNote) "
                    strSQL = strSQL & " Values("
                    strSQL = strSQL & Me.hiddenCompanyID.Value & ","
                    strSQL = strSQL & Me.hiddenMeterID.Value & ","
                    strSQL = strSQL & "'" & Me.sEntryReading.Text & "'" & ","
                    strSQL = strSQL & "'" & "N" & "'" & ","
                    strSQL = strSQL & "'" & iSTARTdate & " " & Trim(sEntryTime.Text) & "'" & ","
                    strSQL = strSQL & Me.sPersonnelListing.SelectedValue & ","
                    strSQL = strSQL & "'" & XY(Trim(stxtTagNote.Text)) & "'"
                    strSQL = strSQL & ")"
                    Me.SQL_MeterReadings.InsertCommand = strSQL
                    Me.SQL_MeterReadings.Insert()
                Case "M" ' 
                    iSPLIT = Split(Me.NextReadDate.Text, "-")
                    iSTARTdate = iSPLIT(2)
                    Dim iDay As String = "01"
                    Dim iMonth As String = GetTheNameMonth(iFINISHdate)
                    Dim iYear As String = Year(iFINISHdate)
                    Dim tMonth As Integer = MonthNumber(iFINISHdate) ' iFINISHdate.Month
                    strSQL = "  INSERT INTO tblCustomer_ScheduledMeters (CompanyID,MeterID,Figure,ConfirmRead,RecordedDate,PersonnelID,TagNote,MonthNumber) "
                    ' RecordedDate = iSTARTdate
                    ' MonthNumber = iMonth + 1
                    strSQL = strSQL & " Values("
                    strSQL = strSQL & Me.hiddenCompanyID.Value & ","
                    strSQL = strSQL & Me.hiddenMeterID.Value & ","
                    strSQL = strSQL & "'" & Me.sEntryReading.Text & "'" & ","
                    strSQL = strSQL & "'" & "N" & "'" & ","
                    strSQL = strSQL & "'" & iSTARTdate & " " & Trim(sEntryTime.Text) & "'" & ","
                    strSQL = strSQL & Me.sPersonnelListing.SelectedValue & ","
                    strSQL = strSQL & "'" & XY(Trim(stxtTagNote.Text)) & "'" & ","
                    strSQL = strSQL & "'" & iMonth + 1 & "'"
                    strSQL = strSQL & ")"
                    Me.SQL_MeterReadings.InsertCommand = strSQL
                    Me.SQL_MeterReadings.Insert()


                Case "W"
                    Dim iWeek As Integer = WeekNumber(iFINISHdate)
                    'Meters ReadMethod
                    strSQL = "  INSERT INTO tblCustomer_ScheduledMeters (CompanyID,MeterID,Figure,ConfirmRead,RecordedDate,PersonnelID,TagNote,WeekNumber) "
                    '  MsgBox(week(iFINISHdate) & vbNewLine & DateAdd(DateInterval.Weekday, 1, iFINISHdate))
                    'RecordedDate = iSTARTdate
                    'WeekNumber = iWeek + 1
                    strSQL = strSQL & " Values("
                    strSQL = strSQL & Me.hiddenCompanyID.Value & ","
                    strSQL = strSQL & Me.hiddenMeterID.Value & ","
                    strSQL = strSQL & "'" & Me.sEntryReading.Text & "'" & ","
                    strSQL = strSQL & "'" & "N" & "'" & ","
                    strSQL = strSQL & "'" & iSTARTdate & " " & Trim(sEntryTime.Text) & "'" & ","
                    strSQL = strSQL & Me.sPersonnelListing.SelectedValue & ","
                    strSQL = strSQL & "'" & XY(Trim(stxtTagNote.Text)) & "'" & ","
                    strSQL = strSQL & "'" & iWeek + 1 & "'"
                    strSQL = strSQL & ")"
                    Me.SQL_MeterReadings.InsertCommand = strSQL
                    Me.SQL_MeterReadings.Insert()

                Case "WK"
                    strSQL = "  INSERT INTO tblCustomer_ScheduledMeters (CompanyID,MeterID,Figure,ConfirmRead,RecordedDate,PersonnelID,TagNote,WeekNumber) "
                    ' MsgBox(week(iFINISHdate) & vbNewLine & DateAdd(DateInterval.Weekday, 1, iFINISHdate))
                    Dim iWeek As Integer = WeekNumber(iFINISHdate)
                    'RecordedDate = iSTARTdate
                    'WeekNumber = iWeek + 1
                    strSQL = strSQL & " Values("
                    strSQL = strSQL & Me.hiddenCompanyID.Value & ","
                    strSQL = strSQL & Me.hiddenMeterID.Value & ","
                    strSQL = strSQL & "'" & Me.sEntryReading.Text & "'" & ","
                    strSQL = strSQL & "'" & "N" & "'" & ","
                    strSQL = strSQL & "'" & iSTARTdate & " " & Trim(sEntryTime.Text) & "'" & ","
                    strSQL = strSQL & Me.sPersonnelListing.SelectedValue & ","
                    strSQL = strSQL & "'" & XY(Trim(stxtTagNote.Text)) & "'" & ","
                    strSQL = strSQL & "'" & iWeek + 1 & "'"
                    strSQL = strSQL & ")"
                    Me.SQL_MeterReadings.InsertCommand = strSQL
                    Me.SQL_MeterReadings.Insert()


                Case Else
            End Select

        Else
            strSQL = "UPDATE [tblCustomer_ScheduledMeters] "
            strSQL = strSQL & " SET [Figure] = " & "'" & Trim(sEntryReading.Text) & "'"
            strSQL = strSQL & "  , [PersonnelID] = " & Me.sPersonnelListing.SelectedValue
            strSQL = strSQL & "  , [ConfirmRead] =" & "'" & "N" & "'"
            strSQL = strSQL & "  , [TagNote] = " & "'" & XY(Trim(stxtTagNote.Text)) & "'"
            strSQL = strSQL & " WHERE ID = " & Me.HiddenScheduledMeter.Value
            Me.SQL_MeterSelect.UpdateCommand = strSQL
            Me.SQL_MeterSelect.Update()
        End If

        Me.PanelActualMeterReadingsNew.Visible = False
        Me.PanelGridView2Graph.Visible = True

        Me.errorlabel1.Text = ""

        Call Me.recalculate(Me.RadioButtonList1.SelectedValue, Me.RadioButtonList2.SelectedValue, Me.RadioOutStanding.SelectedValue)
        Call CreateNextReadingDate()
        Call ProcessChart()

    End Sub

    Protected Sub ImageButtonReturnToTopLevel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonReturnToTopLevel.Click
        Me.ImageButtonReturnToTopLevel.Visible = False
        Me.PanelPureMeters.Visible = False
        Me.ImageButtonCreateTheMeters.Visible = True
        Me.ImageButtonReadTheMeters.Visible = True
        Me.ImageButtonMeterReaders.Visible = True
    End Sub

    Protected Sub ImageButtonMeters_Add_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonMeters_Add.Click
        Me.errorlabel3.Text = ""
        If Me.DropDownListSiteLevel.SelectedValue > 0 Then
        Else
            Me.errorlabel3.Text = "Status: " & "Please create a Valid Site"
            Exit Sub
        End If

        If Trim(Me.DropDownListSiteLevel.Text) = "" Or Trim(Me.DropDownListSiteLevel.Text) = "..." Then
            Me.errorlabel3.Text = "Status: " & "Please create a Valid Site"
            Exit Sub
        End If

        If Trim(Me.txtMeterName.Text) = "" Then
            Me.errorlabel3.Text = "Status: " & "Please enter a valid Meter name"
            Me.txtMeterName.Focus()
            Exit Sub
        End If

        If Trim(Me.txtMeterLocation.Text) = "" Then
            Me.errorlabel3.Text = "Status: " & "Please enter a valid Meter location"
            Me.txtMeterLocation.Focus()
            Exit Sub
        End If

        If Me.txtInstalleddate.Text <> "" Then
            If Not IsDate(Me.txtInstalleddate.Text) Then
                Me.errorlabel3.Text = "Status: " & "Please enter valid Installed date"
                Me.txtInstalleddate.Focus()
            End If
        Else
            Me.txtInstalleddate.Text = "01/01/1900"
        End If

        If Me.txtLastserviced.Text <> "" Then
            If Not IsDate(Me.txtLastserviced.Text) Then
                Me.errorlabel3.Text = "Status: " & "Please enter valid Last serviced date"
                Me.txtLastserviced.Focus()
            End If
        Else
            Me.txtLastserviced.Text = "01/01/1900"
        End If

        Dim strSQL As String = ""
        strSQL = " INSERT INTO tblCustomer_Meters (CompanyID,MeterTypeID,MeterName,MeterDepartment, MeterIdentifier , Installed , LastServiced,TypeOf ,ScheduleType,UsedFor,SiteID) "
        strSQL = strSQL & " Values("


        '=====================
        Dim myConnectionString As String = "Provider=SQLNCLI.1;Integrated Security=SSPI;Persist Security Info=False;Initial Catalog=portal.utilitymasters.co.uk;Data Source=uk-ed0-sqlcl-01.MCEG.local"
        Dim mySelectQuery As String = " EXEC dbo.spCustomer_FindCompany_WithGiven_SiteID " & Me.DropDownListSiteLevel.SelectedValue
        Dim myConnection As New OleDbConnection(myConnectionString)
        Dim myCommand As New OleDbCommand(mySelectQuery, myConnection)
        myCommand.Connection.Open()
        Dim myReader As OleDbDataReader = myCommand.ExecuteReader(CommandBehavior.CloseConnection)

        myReader.Read()
        'strSQL = strSQL & Me.hiddenCompanyID.Value & ","
        strSQL = strSQL & Trim(myReader("custid")) & ","
        myReader.Close()
        '====================
        strSQL = strSQL & DropDownList3.SelectedValue & ","
        strSQL = strSQL & "'" & XY(Me.txtMeterName.Text) & "'" & ","
        strSQL = strSQL & "'" & XY(Me.txtMeterLocation.Text) & "'" & ","
        strSQL = strSQL & "'" & XY(Me.txtIdentifier.Text) & "'" & ","
        strSQL = strSQL & "'" & Me.txtInstalleddate.Text & "'" & ","
        strSQL = strSQL & "'" & Me.txtLastserviced.Text & "'" & ","
        strSQL = strSQL & "'" & Me.DropDownTypeOf.SelectedValue & "'" & ","
        strSQL = strSQL & "'" & Me.DropDownListLoging.SelectedValue & "'" & ","
        strSQL = strSQL & "'" & Me.DropDownListUseage.SelectedValue & "'" & ","
        strSQL = strSQL & "'" & Me.DropDownListSiteLevel.SelectedValue & "'"
        strSQL = strSQL & ")"

        Me.SQL_MeterSelect.InsertCommand = strSQL
        Me.SQL_MeterSelect.Insert()

        If Me.HiddenFieldSiteID.Value = "" Then
            Me.SQL_MeterSelect.SelectCommand = " EXEC spCustomer_MeterTemplateUnit_Site  " & Me.hiddenCompanyID.Value & " , " & Me.DropDownListSiteLevel.SelectedValue & ",'N'"
        Else
            Me.SQL_MeterSelect.SelectCommand = " EXEC spCustomer_MeterTemplateUnit_Site  " & Me.hiddenCompanyID.Value & " , " & Me.HiddenFieldSiteID.Value & ",'N'"
        End If

        '  Me.SQL_MeterSelect.SelectCommand = "exec spCustomer_MeterTemplateUnit " & Me.hiddenCompanyID.Value
        Me.GridView1.DataBind()

        Me.txtMeterName.Text = ""
        Me.txtMeterLocation.Text = ""
        Me.txtIdentifier.Text = ""
        Me.txtInstalleddate.Text = ""
        Me.txtLastserviced.Text = ""
        strSQL = ""
        Me.DropDownTypeOf.SelectedIndex = 0
        Me.DropDownList3.SelectedIndex = 0

        Me.PanelPureMeters.Visible = False
        Me.ImageButtonCreateTheMeters.Visible = True
        Me.ImageButtonReadTheMeters.Visible = True
        Me.ImageButtonMeterReaders.Visible = True
        Me.LabelNoMeters.Visible = False

    End Sub

    Protected Sub ImageButtonFirstReadingsClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonFirstReadingsClose.Click
        Me.PanelActualFirstReadingsEditor.Visible = False
        Me.ImageButtonReturnToMeters.Visible = True
        Me.PanelActualMeters.Visible = True
    End Sub

    Protected Sub ImageButtonFirstReadingsUpdate_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonFirstReadingsUpdate.Click
        Dim strSQL As String = ""
        Dim istrSQL As String = ""
        Dim iSelection As String = ""
        Dim TheSiteIDis As String = ReturnSiteID(Me.hiddenMeterID.Value)

        If Val(TheSiteIDis) < 1 Then
            ' If Me.HiddenFieldSiteID.Value = " " Then
            Me.errorlabel5.Text = "Status: " & "Please select a Valid Site"
            Exit Sub
        End If

        If Me.nDailyDate.Enabled = True Then
            If Trim(Me.nDailyDate.Text) = "" Or IsDate(Me.nDailyDate.Text) = False Then
                Me.errorlabel5.Text = "Status: " & "Please enter a valid Start date"
                Me.nDailyDate.Focus()
                Exit Sub
            End If
            strSQL = "  EXEC ReturnDateListFromToday " & "'" & Trim(Me.nDailyDate.Text) & "'"
            iSelection = "D"
        End If

        If Me.DropDownListWeekDayName.Enabled = True Then
            If Trim(Me.ntxtEveryWeekStart.Text) = "" Then
                Me.errorlabel5.Text = "Status: " & "Please enter a valid Start Date"
                Me.ntxtEveryWeekStart.Focus()
                Exit Sub
            End If
            strSQL = "  EXEC dbo.ReturnWeekNumbers_YearAndWeekNumber " & Year(Me.ntxtEveryWeekStart.Text) & "," & Right("00" & GetWeekNumber(Me.ntxtEveryWeekStart.Text), 2) & "," & Me.DropDownListWeekDayName.SelectedValue
            istrSQL = ""
            istrSQL = istrSQL & " UPDATE tblCustomer_Meters SET ReadingMethod=" & Me.DropDownListWeekDayName.SelectedValue & "  WHERE ID=" & Me.hiddenMeterID.Value

            Me.SQL_MeterReadings.UpdateCommand = istrSQL
            Me.SQL_MeterReadings.Update()

            iSelection = "W"
        End If

        If Me.DropDownListWeekNumbers.Enabled = True Then
            If Trim(Me.nWeeklyYear.Text) = "" Then
                Me.errorlabel5.Text = "Status: " & "Please enter a valid Start Year"
                Me.nWeeklyYear.Focus()
                Exit Sub
            End If

            strSQL = "  EXEC dbo.ReturnWeekNumbers_YearAndWeekNumber " & Trim(Me.nWeeklyYear.Text) & "," & Right("00" & Me.DropDownListWeekNumbers.SelectedValue, 2) & ",0"
            iSelection = "WK"
        End If

        If Me.DropDownListMonthNumbers.Enabled = True Then
            If Trim(Me.nMonthlyYear.Text) = "" Then
                Me.errorlabel5.Text = "Status: " & "Please enter a valid Start Month"
                Me.nMonthlyYear.Focus()
                Exit Sub
            End If
            iSelection = "M"
            strSQL = " EXEC dbo.ReturnMonthNumbers " & "'01/" & Right("00" & Me.DropDownListMonthNumbers.SelectedValue, 2) & "/" & Trim(Me.nMonthlyYear.Text) & "'" & ",0"
        End If

        ' Works a treat haven't  clue why it gliched here emmmmmmmmmmmmmmm
        ' MsgBox("strsql " & strSQL)

        Me.SQL_MeterGeneral.SelectCommand = strSQL

        Dim ivwExpensiveItems As System.Data.DataView = CType(SQL_MeterGeneral.Select(DataSourceSelectArguments.Empty), System.Data.DataView)
        If ivwExpensiveItems.Count > 0 Then  ' not found
            For Each rowProduct As System.Data.DataRowView In ivwExpensiveItems
                Select Case iSelection
                    Case "D"
                    Case "W"
                    Case "WK"
                    Case "M"
                End Select

                istrSQL = "  INSERT INTO tblCustomer_ScheduledMeters (CompanyID,MeterID,Figure,ConfirmRead,RecordedDate,PersonnelID,TagNote,WeekNumber,MonthNumber,SiteID) "
                istrSQL = istrSQL & " Values("

                ' Have to find the correct company from the given site
                ' oliver
                Dim myConnectionString As String = "Provider=SQLNCLI.1;Integrated Security=SSPI;Persist Security Info=False;Initial Catalog=portal.utilitymasters.co.uk;Data Source=uk-ed0-sqlcl-01.MCEG.local"
                Dim ystrSQL As String = ""
                ystrSQL = "exec spCustomer_FindCompany_WithGiven_SiteID " & TheSiteIDis ' me.DropDownListSiteFilter.SelectedValue


                Dim mySelectQuery As String = ystrSQL
                Dim myConnection As New OleDbConnection(myConnectionString)
                Dim myCommand As New OleDbCommand(mySelectQuery, myConnection)
                myCommand.Connection.Open()
                Dim myReader As OleDbDataReader = myCommand.ExecuteReader(CommandBehavior.CloseConnection)
                myReader.Read()
                '=======================
                istrSQL = istrSQL & myReader("custid") & ","
                myReader.Close()
                '=====================

                istrSQL = istrSQL & Me.hiddenMeterID.Value & ","
                istrSQL = istrSQL & "'" & "" & "'" & ","
                istrSQL = istrSQL & "'" & "Y" & "'" & ","

                If iSelection = "W" Then
                    istrSQL = istrSQL & "'" & rowProduct("TheDate") & "'" & ","
                End If

                If iSelection = "D" Then
                    istrSQL = istrSQL & "'" & rowProduct("StartDate") & "'" & ","
                End If

                If iSelection = "WK" Then
                    istrSQL = istrSQL & "'" & rowProduct("StartDate") & "'" & ","
                End If

                If iSelection = "M" Then
                    istrSQL = istrSQL & "'" & rowProduct("StartDate") & "'" & ","
                End If

                istrSQL = istrSQL & "0" & ","
                istrSQL = istrSQL & "'" & "" & "'" & ","

                If iSelection = "W" Then
                    istrSQL = istrSQL & "'" & rowProduct("WeekNumber") & "'" & ","
                    istrSQL = istrSQL & "'" & "" & "'" & ","
                End If

                If iSelection = "D" Then
                    istrSQL = istrSQL & "'" & "" & "'" & ","
                    istrSQL = istrSQL & "'" & "" & "'" & ","
                End If

                If iSelection = "WK" Then
                    istrSQL = istrSQL & "'" & rowProduct("WeekNumber") & "'" & ","
                    istrSQL = istrSQL & "'" & "" & "'" & ","
                End If

                If iSelection = "M" Then
                    istrSQL = istrSQL & "'" & "" & "'" & ","
                    istrSQL = istrSQL & "'" & rowProduct("MonthNumber") & "'" & ","
                End If

                ' find company with given site
                '  istrSQL = istrSQL & "'" & Me.HiddenFieldSiteID.Value & "'"

                istrSQL = istrSQL & "'" & TheSiteIDis & "'" 'Me.DropDownListSiteFilter.SelectedValue & "'"
                istrSQL = istrSQL & ")"

                '       MsgBox(istrSQL)


                Me.SQL_MeterReadings.InsertCommand = istrSQL
                Me.SQL_MeterReadings.Insert()
            Next
        End If

        Call Me.recalculate(Me.RadioButtonList1.SelectedValue, Me.RadioButtonList2.SelectedValue, Me.RadioOutStanding.SelectedValue)
        Me.PanelActualFirstReadingsEditor.Visible = False
        Me.ImageButtonReturnToMeters.Visible = True
        Me.PanelActualMeters.Visible = True

    End Sub


    Protected Function WhichMeter(ByVal mySelectedRowWas As String) As String
        Dim strSQL As String = ""
        strSQL = ""
        strSQL = strSQL & "  SELECT * FROM dbo.tblCustomer_Meters "
        strSQL = strSQL & "  WHERE ID=" & Me.hiddenMeterID.Value
        Me.SQL_MeterGeneral.SelectCommand = strSQL
        Dim T1 As String = ""
        Dim T2 As String = ""
        Dim T3 As String = ""
        Dim ivwExpensiveItems As System.Data.DataView = CType(SQL_MeterGeneral.Select(DataSourceSelectArguments.Empty), System.Data.DataView)
        If ivwExpensiveItems.Count > 0 Then
            For Each rowProduct As System.Data.DataRowView In ivwExpensiveItems
                'If IsDBNull(rowProduct("MeterIdentifier")) = True Then
                '    Me.txtIdentifier.Text = ""
                'Else
                '    Me.txtIdentifier.Text = rowProduct("MeterIdentifier")
                'End If
                Select Case rowProduct("TypeOf")
                    Case "C" ' Consumption
                        T1 = "Consumption"
                    Case "R" ' Reading ONLY
                        T1 = "Readings"
                    Case "O" ' OTher type
                    Case Else
                End Select

                Select Case rowProduct("ScheduleType")
                    Case "D"
                        T2 = "Daily read"
                        Me.HiddenFieldScheduleType.Value = "D"
                    Case "M" ' 
                        T2 = "Monthly read"
                        Me.HiddenFieldScheduleType.Value = "M"
                    Case "W"
                        T2 = "Weekly read - Specific day"
                        Me.HiddenFieldScheduleType.Value = "W"
                    Case "WK"
                        T2 = "Weekly read - No specific day"
                        Me.HiddenFieldScheduleType.Value = "W"
                    Case Else
                End Select

                Select Case rowProduct("UsedFor")
                    Case "E" ' Consumption
                        T3 = "Energy meter"
                    Case "P" ' Reading ONLY
                        T3 = "Production meter"
                    Case Else
                End Select

                Exit For
            Next

            Me.SelectedMeterName.Text = Trim(mySelectedRowWas) & " - " & "(" & T1 & ")" & " - " & T2 & " (" & T3 & ")"
        End If

        WhichMeter = "Ok"
    End Function


    Protected Function MonthFromWeekNo(ByVal strYear As String, ByVal strWeek As String) As String
        MonthFromWeekNo = "ok"
    End Function

    Protected Sub DropDownListWeekNumbers_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DropDownListWeekNumbers.SelectedIndexChanged
        'MsgBox(MonthFromWeekNo(Me.nWeeklyYear.Text, Me.DropDownListWeekNumbers.SelectedIndex))
    End Sub

    Function GetFiscalWeek(ByVal dateTime As DateTime) As String
        GetFiscalWeek = DatePart("ww", dateTime)
    End Function

    Public Function GetWeekNumber(ByVal inDate As Date) As Integer
        Dim dayOfYear As Integer, wkNumber As Integer
        Dim compensation As Integer = 0
        Dim oneDate As String
        Dim firstDayDate As Date
        dayOfYear = inDate.DayOfYear
        oneDate = "1/1/" & inDate.Year.ToString
        firstDayDate = DateAndTime.DateValue(oneDate)
        Select Case firstDayDate.DayOfWeek
            Case DayOfWeek.Sunday
                compensation = 0
            Case DayOfWeek.Monday
                compensation = 6
            Case DayOfWeek.Tuesday
                compensation = 5
            Case DayOfWeek.Wednesday
                compensation = 4
            Case DayOfWeek.Thursday
                compensation = 3
            Case DayOfWeek.Friday
                compensation = 2
            Case DayOfWeek.Saturday
                compensation = 1
        End Select
        dayOfYear = dayOfYear - compensation
        If dayOfYear Mod 7 = 0 Then
            wkNumber = dayOfYear / 7
        Else
            wkNumber = (dayOfYear \ 7) + 1 'WATCH THE OPERATOR \ !!! IT RETURNS THE INTEGER RESULT
        End If
        Return wkNumber
    End Function


    Protected Function GetTheWeekName(ByVal what As String) As String
        Dim nReturn As String = ""
        Select Case what
            Case "1" : nReturn = "Monday"
            Case "2" : nReturn = "Tuesday"
            Case "3" : nReturn = "Wednesday"
            Case "4" : nReturn = "Thursday"
            Case "5" : nReturn = "Friday"
            Case "6" : nReturn = "Saturday"
            Case "7" : nReturn = "Sunday"
        End Select

        GetTheWeekName = nReturn
    End Function


    Protected Function GetTheMonthName(ByVal what As String) As String
        Dim nReturn As String = ""
        Select Case what
            Case "1" : nReturn = "January"
            Case "2" : nReturn = "February"
            Case "3" : nReturn = "March"
            Case "4" : nReturn = "April"
            Case "5" : nReturn = "May"
            Case "6" : nReturn = "June"
            Case "7" : nReturn = "July"
            Case "8" : nReturn = "August"
            Case "9" : nReturn = "September"
            Case "10" : nReturn = "October"
            Case "11" : nReturn = "November"
            Case "12" : nReturn = "December"
        End Select

        GetTheMonthName = nReturn
    End Function

    Protected Function GetTheNameMonth(ByVal what As String) As String
        Dim nReturn As String = ""
        Select Case what
            Case "January" : nReturn = "01"
            Case "February" : nReturn = "02"
            Case "March" : nReturn = "03"
            Case "April" : nReturn = "04"
            Case "May" : nReturn = "05"
            Case "June" : nReturn = "06"
            Case "July" : nReturn = "07"
            Case "August" : nReturn = "08"
            Case "September" : nReturn = "09"
            Case "October" : nReturn = "10"
            Case "November" : nReturn = "11"
            Case "December" : nReturn = "12"
        End Select

        GetTheNameMonth = nReturn
    End Function

    Public Function WeekOfMonth(ByVal dteInputDate As Date, ByVal intStartWeekday As Integer) As Integer
        Dim intWeekdayOf1st As Integer
        Dim intDaysInWeek1 As Integer
        Dim intDaysPastWeek1 As Integer
        Dim iReturn As Double

        intWeekdayOf1st = Weekday(DateSerial(Year(dteInputDate), Month(dteInputDate), 1))
        intDaysInWeek1 = (6 + intStartWeekday - intWeekdayOf1st) Mod 7 + 1
        If Day(dteInputDate) <= intDaysInWeek1 Then
            WeekOfMonth = 1
        Else
            intDaysPastWeek1 = Day(dteInputDate) - intDaysInWeek1
            ' just me being silly emmmmmmmmm but it works
            '  WeekOfMonth = 1 + (intDaysPastWeek1 \ 7) + Math.Abs(intDaysPastWeek1 Mod 7 <> 0)
            iReturn = intDaysPastWeek1 Mod 7 <> 0
            WeekOfMonth = 1 + (intDaysPastWeek1 \ 7) + Math.Abs(iReturn)
        End If
    End Function


    Protected Function WeekNumber(ByVal dteValue As Date) As Integer
        'Nicked from VB.NET rare as rocking horse *%^$
        Dim cult_info As System.Globalization.CultureInfo = System.Globalization.CultureInfo.CreateSpecificCulture("no")
        Dim cal As System.Globalization.Calendar = cult_info.Calendar
        'Dim weekNO As Integer = cal.GetWeekOfYear(dteValue, cult_info.DateTimeFormat.CalendarWeekRule, cult_info.DateTimeFormat.FirstDayOfWeek)
        WeekNumber = cal.GetWeekOfYear(dteValue, cult_info.DateTimeFormat.CalendarWeekRule, cult_info.DateTimeFormat.FirstDayOfWeek)
    End Function

    Protected Function MonthNumber(ByVal dteValue As Date) As Integer
        'Nicked from VB.NET rare as rocking horse *%^$
        Dim cult_info As System.Globalization.CultureInfo = System.Globalization.CultureInfo.CreateSpecificCulture("no")
        Dim cal As System.Globalization.Calendar = cult_info.Calendar
        'Dim weekNO1 As Integer = cal.GetMonth(dteValue)
        MonthNumber = cal.GetMonth(dteValue)
    End Function

    Protected Function AddWeekNumber(ByVal dteValue As Date) As Date
        'Nicked from VB.NET rare as rocking horse *%^$
        Dim cult_info As System.Globalization.CultureInfo = System.Globalization.CultureInfo.CreateSpecificCulture("no")
        Dim cal As System.Globalization.Calendar = cult_info.Calendar
        'Dim weekNO As Date = cal.AddWeeks(dteValue, 1)
        AddWeekNumber = cal.AddWeeks(dteValue, 1)
    End Function

    Protected Function AddMonthNumber(ByVal dteValue As Date) As Date
        'Nicked from VB.NET rare as rocking horse *%^$
        Dim cult_info As System.Globalization.CultureInfo = System.Globalization.CultureInfo.CreateSpecificCulture("no")
        Dim cal As System.Globalization.Calendar = cult_info.Calendar
        'Dim weekNO3 As Date = cal.AddMonths(dteValue, 1)
        AddMonthNumber = cal.AddMonths(dteValue, 1)
    End Function

    'Protected Sub ImageButtonPersonnelAdministration_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonPersonnelAdministration.Click
    '    Response.Redirect("~/myapps/CustomerMonitors/MeterMonitors.aspx?custid=" & Me.hiddenCompanyID.Value & " &customer=" & Trim(Me.LabCompany.Text))
    'End Sub

    Protected Sub ImageButtonMeterReaders_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonMeterReaders.Click
        Response.Redirect("~/myapps/CustomerMonitors/MeterMonitors.aspx?custid=" & Me.hiddenCompanyID.Value & " &customer=" & Trim(Me.LabCompany.Text))
    End Sub

    'Protected Sub DropDownListSiteFilter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DropDownListSiteFilter.SelectedIndexChanged
    '    'Dim strSQL As String = ""
    '    'strSQL = " EXEC spCustomer_MeterTemplateUnit_Site  " & Me.hiddenCompanyID.Value & " , " & DropDownListSiteFilter.SelectedValue
    '    ''   MsgBox(strSQL)
    '    'Me.SQL_MeterSelect.SelectCommand = strSQL
    '    'Me.LabCompany.Text = Trim(DropDownListSiteFilter.SelectedItem.Text)

    'End Sub

    'Protected Function SiteFilerSelect() As Boolean
    '    DropDownListSiteFilter.SelectedIndex = 0
    '    SiteFilerSelect = True
    'End Function

    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete
        ' june MsgBox("Loaded" & DropDownListSiteFilter.Items.Count)
        '   Me.DropDownListSiteFilter.SelectedItem.Text = Me.LabCompany.Text
        '   MsgBox(DropDownListSiteFilter.SelectedItem.Text & vbNewLine & Me.LabCompany.Text)

    End Sub

    Protected Function GatherNewDetails() As Long
        Dim strSQL As String = ""
        Dim TrueORFalse As String = ""
        strSQL = "SELECT smith.john FROM ( SELECT 1-1 AS 'john')  AS [smith] WHERE [smith].john>3"
        Me.SQL_MeterSelect.SelectCommand = strSQL
        Me.SQL_MeterSelect.DataBind()
        If GlobalReadRequired = True Then
            TrueORFalse = "'Y'"
        Else
            TrueORFalse = "'N'"
        End If

        strSQL = " EXEC spCustomer_MeterTemplateUnit_Site  " & Session("TheCompanyID") & " , " & Session("myid") & "," & TrueORFalse
        Me.SQL_MeterSelect.SelectCommand = strSQL
        Me.SQL_MeterSelect.DataBind()
        GatherNewDetails = Me.GridView1.Rows.Count
    End Function

    Protected Function GatherNewDetailsForCompany() As Long
        Dim strSQL As String = ""
        Dim TrueORFalse As String = ""
        strSQL = "SELECT smith.john FROM ( SELECT 1-1 AS 'john')  AS [smith] WHERE [smith].john>3"
        Me.SQL_MeterSelect.SelectCommand = strSQL
        Me.SQL_MeterSelect.DataBind()

        If GlobalReadRequired = True Then
            TrueORFalse = "'Y'"
        Else
            TrueORFalse = "'N'"
        End If

        strSQL = "EXEC spCustomer_MeterTemplateUnit_Site_Company " & Session("TheCompanyID") & "," & TrueORFalse
        Me.SQL_MeterSelect.SelectCommand = strSQL
        Me.SQL_MeterSelect.DataBind()
        GatherNewDetailsForCompany = Me.GridView1.Rows.Count
    End Function

    Protected Function ReturnSiteID(ByVal ID As Integer) As String
        Dim nReturn As String = ""
        Dim myConnectionString As String = "Provider=SQLNCLI.1;Integrated Security=SSPI;Persist Security Info=False;Initial Catalog=portal.utilitymasters.co.uk;Data Source=uk-ed0-sqlcl-01.MCEG.local"
        Dim ystrSQL As String = ""

        ystrSQL = " SELECT siteId FROM tblCustomer_Meters WHERE ID =" & ID

        Dim mySelectQuery As String = ystrSQL
        Dim myConnection As New OleDbConnection(myConnectionString)
        Dim myCommand As New OleDbCommand(mySelectQuery, myConnection)
        myCommand.Connection.Open()
        Dim myReader As OleDbDataReader = myCommand.ExecuteReader(CommandBehavior.CloseConnection)
        myReader.Read()
        nReturn = myReader("SiteId")
        myReader.Close()

        ReturnSiteID = nReturn
    End Function

    Protected Sub RadioButtonTotalOutstanding_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RadioButtonTotalOutstanding.SelectedIndexChanged
        If Me.RadioButtonTotalOutstanding.SelectedValue = "ReadRequired" Then
            GlobalReadRequired = True
        Else
            GlobalReadRequired = False
        End If

        Call PushTheMeterReadButton()

    End Sub
 
    Protected Sub ImageButtonOutstanding_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonOutstanding.Click
        Dim myItem As String = "~/myapps/CustomerMeters/MeterOutStanding.aspx?custid=" & Me.hiddenCompanyID.Value & " &customer=" & Trim(Me.LabCompany.Text)
        Response.Redirect(myItem)

    End Sub
End Class
