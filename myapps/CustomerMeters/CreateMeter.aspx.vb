﻿Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Drawing
Imports System.Web
'Imports Dundas.Charting.WebControl
'Imports Dundas.Charting.WebControl.Utilities
Imports System.Web.Security
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.SessionState
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports System.Data
Imports System.Data.OleDb

Partial Class myapps_CustomerMeters_CreateMeter
    Inherits System.Web.UI.Page
    Dim TheCustomerID As String = ""
    Dim TheCustomerName As String = ""
    Dim TheSiteID As String = ""
    Dim TheSiteName As String = ""


    Dim iCustomer As String = ""
    Dim iSite As String = ""
    Dim iMethod As String = ""
    Dim iMethodType As String = ""


    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Call LoadTreeview_Report()

        Me.DropDownTypeOf.Items.Add(New ListItem("Consumption", "R"))
        Me.DropDownTypeOf.Items.Add(New ListItem("Readings Only", "C"))

        Me.DropDownListLoging.Items.Add(New ListItem("Daily", "D"))
        Me.DropDownListLoging.Items.Add(New ListItem("Weekly", "W"))
        Me.DropDownListLoging.Items.Add(New ListItem("Week Number", "WK"))
        Me.DropDownListLoging.Items.Add(New ListItem("Monthly", "M"))

        Me.DropDownListUseage.Items.Add(New ListItem("Energy", "E"))
        Me.DropDownListUseage.Items.Add(New ListItem("Production", "P"))


        If Request.QueryString("method") = "Customer" Then
            iCustomer = Trim(Replace(Request.QueryString("custid"), "'", ""))
            iMethod = Trim(Replace(Request.QueryString("method"), "'", ""))
            iMethodType = "Customer"
        Else
            iSite = Trim(Replace(Request.QueryString("SiteId"), "'", ""))
            iMethod = Trim(Replace(Request.QueryString("method"), "'", ""))
            iMethodType = "Site"
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Request.QueryString("custid")) <> "" Then
            Me.PanelPureMeters.Visible = True

            Dim rs As New ADODBConnection
            Dim strSQL As String = ""
            If Request.QueryString("Method") = "Customer" Then
                strSQL = ""
                strSQL = " SELECT custid,customername FROM  [UML_CMS].dbo.tblcustomer where custid=" & Trim(Request.QueryString("Custid"))
            Else
                strSQL = ""
                strSQL = strSQL & " SELECT [UML_CMS].dbo.tblsites.siteid,[UML_CMS].dbo.tblsites.sitename,[UML_CMS].dbo.tblcustomer.customername FROM  [UML_CMS].dbo.tblsites  "
                strSQL = strSQL & " INNER JOIN [UML_CMS].dbo.tblcustomer ON [UML_CMS].dbo.tblsites .custid=[UML_CMS].dbo.tblcustomer.custid where siteid=" & Trim(Request.QueryString("Custid"))
            End If

            rs.OpenConnection()
            rs.OpenRecordSet(strSQL)

            If Request.QueryString("Method") = "Customer" Then
                iCustomer = rs.rComm.Fields("CustId").Value
                iSite = 0
            Else
                '   iCustomer = rs.rComm.Fields("CustId").Value
                iSite = rs.rComm.Fields("SiteID").Value
            End If

            Me.LabCompany.Text = rs.rComm.Fields("CustomerName").Value

            If Request.QueryString("Method") = "Customer" Then
                Me.LabSite.Text = ""
            Else
                Me.LabSite.Text = rs.rComm.Fields("SiteName").Value
            End If
            rs.CloseConnection()

            If Request.QueryString("Method") = "Customer" Then
                Me.Label1.Visible = True
                Me.DropDownListSiteLevel.Visible = True
                'strSQL = "EXEC UML_EXTData.dbo.usp_Portal_MyApps_MyMeters_ViewSites " & MySession.UserID & "," & "'" & iMethod & "'" & "," & iCustomer


                strSQL = " SELECT cast(intSiteFK as varchar) as [ID], [Site Name] as [Value]"
                strSQL = strSQL & " FROM dbo.tblUsersSites us INNER JOIN dbo.vwCompaniesSites cs ON us.intSiteFK = cs.[site id] WHERE intUserFK =" & MySession.UserID & " and cs.id = " & Request.QueryString("CustID")
                strSQL = strSQL & " ORDER BY [Site Name]"

                strSQL = " EXEC uml_extdata.dbo.usp_Portal_Admin_ListSites " & Request.QueryString("CustID") & ", 'Customer', " & MySession.UserID
                Me.SQL_UserSitesCreateMeter.SelectCommand = strSQL
                Me.SQL_UserSitesCreateMeter.DataBind()
            Else
                Me.Label1.Visible = False
                Me.DropDownListSiteLevel.Visible = False
            End If

            'Me.DropDownTypeOf.Items.Add(New ListItem("Consumption", "R"))
            'Me.DropDownTypeOf.Items.Add(New ListItem("Readings Only", "C"))

            'Me.DropDownListLoging.Items.Add(New ListItem("Daily", "D"))
            'Me.DropDownListLoging.Items.Add(New ListItem("Weekly", "W"))
            'Me.DropDownListLoging.Items.Add(New ListItem("Week Number", "WK"))
            'Me.DropDownListLoging.Items.Add(New ListItem("Monthly", "M"))

            'Me.DropDownListUseage.Items.Add(New ListItem("Energy", "E"))
            'Me.DropDownListUseage.Items.Add(New ListItem("Production", "P"))
        Else
            Me.PanelPureMeters.Visible = False
        End If

    End Sub

    Private Sub LoadTreeview_Report()
        Me.navOnlineReports.InnerHtml = ""
        Dim strSQL As String = "EXEC spPortal_TREEVIEW " & MySession.UserID
        Dim myConnection As New OleDb.OleDbConnection(Core.CurrentConnectionString)

        Dim myCommand As New OleDb.OleDbCommand(strSQL, myConnection)
        myCommand.Connection.Open()
        myCommand.CommandTimeout = 180
        Dim Reader As OleDb.OleDbDataReader = myCommand.ExecuteReader(CommandBehavior.CloseConnection)

        'Me.navOnlineReports.InnerHtml += "<br/>"
        'Me.navOnlineReports.InnerHtml += "<br/>"
        'Me.navOnlineReports.InnerHtml += "<br/>"

        While Reader.Read
            Select Case CStr(Reader(3))
                Case CStr("Company")
                    If Len(Reader(1).ToString) > 25 Then
                        Me.navOnlineReports.InnerHtml += "<li class='mnavigation_company'><a onmouseover=" & Chr(34) & "javascript:showmj('" & Reader(1).ToString & "','N')" & Chr(34) & " href='createmeter.aspx?custid=" & Reader(0).ToString & "&method=Customer'>" & Mid$(Reader(1).ToString, 1, 25) & "..." & "</a></li>"
                    Else
                        Me.navOnlineReports.InnerHtml += "<li class='mnavigation_company'><a onmouseover=" & Chr(34) & "javascript:showmj('" & "" & "','Y')" & Chr(34) & " href='createmeter.aspx?custid=" & Reader(0).ToString & "&method=Customer'>" & Reader(1).ToString & "</a></li>"
                    End If
                    ' Me.navOnlineReports.InnerHtml += "<li class='styleformytreeview_company'><a href='createmeter.aspx?custid=" & Reader(0).ToString & "&method=Customer'>" & Reader(1).ToString & "</a></li>"

                Case CStr("Header")
                    Me.navOnlineReports.InnerHtml += "<li class='styleformytreeview_header'>" & Reader(1).ToString & "</li>"


                Case CStr("Site")
                    If Len(Reader(1).ToString) > 25 Then
                        Me.navOnlineReports.InnerHtml += "<li class='mnavigation_company'><a onmouseover=" & Chr(34) & "javascript:showmj('" & Reader(1).ToString & "','N')" & Chr(34) & " href='createmeter.aspx?custid=" & Reader(0).ToString & "&method=Site'>" & Mid$(Reader(1).ToString, 1, 25) & "..." & "</a></li>"
                    Else
                        Me.navOnlineReports.InnerHtml += "<li class='mnavigation_company'><a onmouseover=" & Chr(34) & "javascript:showmj('" & "" & "','Y')" & Chr(34) & " href='createmeter.aspx?custid=" & Reader(0).ToString & "&method=Site'>" & Reader(1).ToString & "</a></li>"
                    End If

                    '   Me.navOnlineReports.InnerHtml += "<li class='styleformytreeview_site'><a href='createmeter.aspx?custid=" & Reader(0).ToString & "&method=Site'>" & Left(Reader(1).ToString, 23) & "..</a></li>"
                Case CStr("Group")
                    Me.navOnlineReports.InnerHtml += "<li class='styleformytreeview_group'><a href='createmeter.aspx?custid=" & Reader(0).ToString & "&method=Group'>" & Reader(1).ToString & "</a></li>"
            End Select

        End While

        Me.navOnlineReports.InnerHtml += "<li class='styleformytreeview_company' style='height:10px;'></li>"
        Me.navOnlineReports.InnerHtml += "<li class='styleformytreeview_company'><a href='Default.aspx'><< Back</a></li>"

    End Sub

    
    Protected Sub ImageButtonReturnToMenu_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonReturnToMenu.Click
        Response.Redirect("~/myapps/customermeters/Default.aspx")
    End Sub

    Protected Sub ImageButtonMeters_Add_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonMeters_Add.Click
        Me.errorlabel3.Text = ""
        If Request.QueryString("Method") = "Customer" Then
            If Me.DropDownListSiteLevel.SelectedValue > 0 Then
            Else
                Me.errorlabel3.Text = "Status: " & "Please create a Valid Site"
                Exit Sub
            End If
        Else
        End If

        If Request.QueryString("Method") = "Customer" Then
            If Trim(Me.DropDownListSiteLevel.SelectedItem.Text) = "" Or Trim(Me.DropDownListSiteLevel.SelectedItem.Text) = "..." Then
                Me.errorlabel3.Text = "Status: " & "Please create a Valid Site"
                Exit Sub
            End If
        Else
        End If

        If Trim(Me.txtMeterName.Text) = "" Then
            Me.errorlabel3.Text = "Status: " & "Please enter a valid Meter name"
            Me.txtMeterName.Focus()
            Exit Sub
        End If

        If Trim(Me.txtMeterLocation.Text) = "" Then
            Me.errorlabel3.Text = "Status: " & "Please enter a valid Meter location"
            Me.txtMeterLocation.Focus()
            Exit Sub
        End If

        If Me.txtInstalleddate.Text <> "" Then
            If Not IsDate(Me.txtInstalleddate.Text) Then
                Me.errorlabel3.Text = "Status: " & "Please enter valid Installed date"
                Me.txtInstalleddate.Focus()
            End If
        Else
            Me.txtInstalleddate.Text = "01/01/1900"
        End If

        If Me.txtLastserviced.Text <> "" Then
            If Not IsDate(Me.txtLastserviced.Text) Then
                Me.errorlabel3.Text = "Status: " & "Please enter valid Last serviced date"
                Me.txtLastserviced.Focus()
            End If
        Else
            Me.txtLastserviced.Text = "01/01/1900"
        End If

        If Me.txtInstalleddate.Text = "" Then Me.txtInstalleddate.Text = "01/01/1900"
        If Me.txtLastserviced.Text = "" Then Me.txtLastserviced.Text = "01/01/1900"

        Dim strSQL As String = ""
        Dim rs As New ADODBConnection
        rs.OpenConnection()

        If Request.QueryString("method") = "Customer" Then
            strSQL = " INSERT INTO tblCustomer_Meters (CompanyID,MeterTypeID,MeterName,MeterDepartment, MeterIdentifier , Installed , LastServiced,TypeOf ,ScheduleType,UsedFor,SiteID) "
            strSQL = strSQL & " Values("
            strSQL = strSQL & iCustomer & ","
            strSQL = strSQL & "'" & Me.DropDownList3.SelectedValue & "'" & ","
            strSQL = strSQL & "'" & XY(Me.txtMeterName.Text) & "'" & ","
            strSQL = strSQL & "'" & XY(Me.txtMeterLocation.Text) & "'" & ","
            strSQL = strSQL & "'" & XY(Me.txtIdentifier.Text) & "'" & ","
            strSQL = strSQL & "'" & Me.txtInstalleddate.Text & "'" & ","
            strSQL = strSQL & "'" & Me.txtLastserviced.Text & "'" & ","
            strSQL = strSQL & "'" & Me.DropDownTypeOf.SelectedValue & "'" & ","
            strSQL = strSQL & "'" & Me.DropDownListLoging.SelectedValue & "'" & ","
            strSQL = strSQL & "'" & Me.DropDownListUseage.SelectedValue & "'" & ","
            strSQL = strSQL & "'" & Me.DropDownListSiteLevel.SelectedValue & "'"
            strSQL = strSQL & ")"
        Else

            Dim mySelectQuery As String = " EXEC dbo.spCustomer_FindCompany_WithGiven_SiteID " & iSite
            rs.OpenRecordSet_Two(mySelectQuery)
            iCustomer = rs.rComm_Two.Fields("CustID").Value

            strSQL = " INSERT INTO tblCustomer_Meters (CompanyID,MeterTypeID,MeterName,MeterDepartment, MeterIdentifier , Installed , LastServiced,TypeOf ,ScheduleType,UsedFor,SiteID) "
            strSQL = strSQL & " Values("
            strSQL = strSQL & iCustomer & ","
            strSQL = strSQL & "'" & Me.DropDownList3.SelectedValue & "'" & ","
            strSQL = strSQL & "'" & XY(Me.txtMeterName.Text) & "'" & ","
            strSQL = strSQL & "'" & XY(Me.txtMeterLocation.Text) & "'" & ","
            strSQL = strSQL & "'" & XY(Me.txtIdentifier.Text) & "'" & ","
            strSQL = strSQL & "'" & Me.txtInstalleddate.Text & "'" & ","
            strSQL = strSQL & "'" & Me.txtLastserviced.Text & "'" & ","
            strSQL = strSQL & "'" & Me.DropDownTypeOf.SelectedValue & "'" & ","
            strSQL = strSQL & "'" & Me.DropDownListLoging.SelectedValue & "'" & ","
            strSQL = strSQL & "'" & Me.DropDownListUseage.SelectedValue & "'" & ","
            strSQL = strSQL & "'" & iSite & "'"
            strSQL = strSQL & ")"
        End If

        rs.OpenRecordSet(strSQL)
        rs.CloseConnection()

        Me.txtMeterName.Text = ""
        Me.txtMeterLocation.Text = ""
        Me.txtIdentifier.Text = ""
        Me.txtInstalleddate.Text = ""
        Me.txtLastserviced.Text = ""
        strSQL = ""
        Me.DropDownTypeOf.SelectedIndex = 0
        Me.DropDownList3.SelectedIndex = 0


    End Sub

    Protected Function XY(ByVal what As String) As String
        XY = Replace(what, "'", "''")
    End Function

End Class
