﻿<%@ Page Language="VB" MasterPageFile="~/landing.master" AutoEventWireup="false" CodeFile="ShowActualFirstReadingSetup.aspx.vb" Inherits="myapps_CustomerMeters_ShowActualFirstReadingSetup" title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="content_right" Runat="Server">
   <form id="form1" runat="server">

<style type="text/css" media="screen">
 .hiddencol  { display:none;  }
.viscol { display:block; }
.general {position:absolute;top:82px;left:233px;height:480px;width:670px;background: transparent url('/assets/content-back-general.jpg') no-repeat; padding-top:15px; padding-left:10px;}
.ImageOne {position:absolute;top:12px;left:13px;height:52px;width:101px;    }
.TopLabel {position:absolute;top:23px;left:12px;height:23px;width:618px; font-family:Arial;font-size:14px;color:Navy;   }
.navgeneral {position:absolute;top:83px;left:0px;width:220px;height:500px;background: white url('/assets/left-nav-account.png') no-repeat; padding-top:0px; padding-left:0px;}

</style>

<div class="navgeneral" >
</div>


    <div class="general" >
        <asp:Label ID="TopLabel" runat="server" CssClass="TopLabel"  Text="First Initial Meter Reading " BorderStyle="None"></asp:Label>
    <asp:ImageButton style=" POSITION: absolute;top:19px; left:628px" 
        id="ImageButtonFirstReadingsClose"  Width="32px" Height="32px" runat="server" 
        ImageUrl="~/assets/metering/remove_48x48.png" tooltip="Close window" 
        visible="true"></asp:ImageButton>
        
        
<asp:Panel ID="PanelActualFirstReadingsEditor" 
            Style="position: absolute; left: 10px; top:57px;"  visible="true"   
            runat="server" Height="267px" Width="650px" BackColor="#FFFFC0"> 
 
 <asp:ImageButton style="LEFT: 536px; POSITION: absolute; TOP: 231px" id="ImageButtonFirstReadingsUpdate" Width="100px" Height="25px" runat="server" ImageUrl="~/assets/button_save.gif" tooltip="Update current details" visible="true"></asp:ImageButton> 

<asp:Label style="LEFT: 556px; POSITION: absolute; TOP: 54px" id="Label3" Width="96px" Height="19px" runat="server" Text="Reading" Font-Size="12px" font-names="arial" Visible="False"></asp:Label>
<asp:Label ID="Label4" runat="server" Font-Names="arial" Font-Size="12px" Height="19px"   Style="left: 13px; position: absolute; top: 173px" Text="Monthly" Width="100px"></asp:Label>
<asp:Label ID="Label8" runat="server" Font-Names="arial" Font-Size="12px" Height="19px"  Style="left: 14px; position: absolute; top: 47px" Text="When would you like this reading to begin"   Width="237px" ForeColor="Green"></asp:Label>
<asp:Label ID="Current" runat="server" Font-Names="arial" Font-Size="12px" Height="19px"    Style="left: 13px; position: absolute; top: 23px" Width="496px" ForeColor="DodgerBlue"></asp:Label>
<asp:TextBox   style="LEFT: 557px; POSITION: absolute; TOP: 77px" id="nentryreading" Width="87px" runat="server" Font-Size="12px" font-names="arial" Visible="False"></asp:TextBox>
<asp:TextBox ID="nWeeklyYear" runat="server" Font-Names="arial" Font-Size="12px"    Style="left: 353px; position: absolute; top: 139px" Width="44px" Enabled="False"></asp:TextBox>
<asp:TextBox ID="ntxtEveryWeekStart" runat="server" Enabled="False" Font-Names="arial" Font-Size="12px"   Style="left: 333px; position: absolute; top: 107px" Width="64px">dd/mm/yyyy</asp:TextBox>

<asp:TextBox ID="nMonthlyYear" runat="server" Font-Names="arial" Font-Size="12px" Style="left: 354px;  position: absolute; top: 169px" Width="44px" Enabled="False"></asp:TextBox>
<asp:Label style="LEFT: 558px; POSITION: absolute; TOP: 103px" id="Label9" runat="server" Text="Time of read" Font-Size="12px" font-names="arial" Width="77px" Visible="False"></asp:Label>
<asp:TextBox  enabled="false" style="LEFT: 211px; POSITION: absolute; TOP: 77px" id="nDailyDate"   runat="server" Text="dd/mm/yyyy" Font-Size="12px" font-names="arial" width="71px"></asp:TextBox> &nbsp;
<asp:Label style="LEFT: 559px; POSITION: absolute; TOP: 151px" id="Label10" Width="100px" runat="server" Text="Read by" Font-Size="12px" font-names="arial" Visible="False"></asp:Label> 
<asp:TextBox style="LEFT: 556px; POSITION: absolute; TOP: 123px" id="nentrytime" runat="server" Text="hh:mm:ss" Font-Size="12px" font-names="arial" width="59px" Visible="False"></asp:TextBox> 
<asp:DropDownList style="LEFT: 479px; POSITION: absolute; TOP: 93px" id="nPersonnelListing" runat="server" Font-Size="12px" font-names="arial" width="112px" DataValueField="ID" DataTextField="iName" DataSourceID="SQLPersonnel" Visible="False">
</asp:DropDownList>

<asp:Label style="LEFT: 560px; POSITION: absolute; TOP: 199px" id="Label11" Width="100px" runat="server" Text="Comments" Font-Size="12px" font-names="arial" Visible="False"></asp:Label>
<asp:TextBox style="LEFT: 475px; POSITION: absolute; TOP: 62px" id="ntxtTagNote" Width="93px" Height="14px" runat="server" Font-Size="12px" font-names="arial" maxlength="100" Visible="False"></asp:TextBox>

<asp:Label ID="errorlabel5" runat="server" style="position:absolute; left: 10px; top: 203px;" Font-Size="12px" font-names="arial" forecolor="Red"  Width="533px"></asp:Label>

<asp:Label ID="labelFirstmeterRead" runat="server" BorderStyle="None" Font-Names="Arial" Font-Size="13px"  ForeColor="darkgreen" Style="left: 142px; position: absolute; top: 5px" Text="Meter" Width="317px"></asp:Label>
<asp:Label ID="Label12" runat="server" BorderStyle="None" Font-Names="Arial" Font-Size="13px" ForeColor="Blue" Style="left: 12px; position: absolute; top: 5px" Text="Your first reading for:"   Width="175px"></asp:Label>

 <asp:DropDownList enabled="false"  AutoPostBack="True"  ID="DropDownListWeekNumbers" style="position:absolute; left: 215px; top: 140px;" runat="server" Font-Names="Arial" Font-Size="12px" Width="45px">
    <asp:ListItem Text="1" Value="1"/>
    <asp:ListItem Text="2" Value="2"/>
    <asp:ListItem Text="3" Value="3"/>
    <asp:ListItem Text="4" Value="4"/>
    <asp:ListItem Text="5" Value="5"/>
    <asp:ListItem Text="6" Value="6"/>
    <asp:ListItem Text="7" Value="7"/>
    <asp:ListItem Text="8" Value="8"/>
    <asp:ListItem Text="9" Value="9"/>
    <asp:ListItem Text="10" Value="10"/>
    <asp:ListItem Text="11" Value="11"/>
    <asp:ListItem Text="12" Value="12"/>
    <asp:ListItem Text="13" Value="13"/>
    <asp:ListItem Text="14" Value="14"/>
    <asp:ListItem Text="15" Value="15"/>
    <asp:ListItem Text="16" Value="16"/>
    <asp:ListItem Text="17" Value="17"/>
    <asp:ListItem Text="18" Value="18"/>
    <asp:ListItem Text="19" Value="19"/>
    <asp:ListItem Text="20" Value="20"/>
    <asp:ListItem Text="21" Value="21"/>
    <asp:ListItem Text="22" Value="22"/>
    <asp:ListItem Text="23" Value="23"/>
    <asp:ListItem Text="24" Value="24"/>
    <asp:ListItem Text="25" Value="25"/>
    <asp:ListItem Text="26" Value="26"/>
    <asp:ListItem Text="27" Value="27"/>
    <asp:ListItem Text="28" Value="28"/>
    <asp:ListItem Text="29" Value="29"/>
    <asp:ListItem Text="30" Value="30"/>
    <asp:ListItem Text="31" Value="31"/>
    <asp:ListItem Text="32" Value="32"/>
    <asp:ListItem Text="33" Value="33"/>
    <asp:ListItem Text="34" Value="34"/>
    <asp:ListItem Text="35" Value="35"/>
    <asp:ListItem Text="36" Value="36"/>
    <asp:ListItem Text="37" Value="37"/>
    <asp:ListItem Text="38" Value="38"/>
    <asp:ListItem Text="39" Value="39"/>
    <asp:ListItem Text="40" Value="40"/>
    <asp:ListItem Text="41" Value="41"/>
    <asp:ListItem Text="42" Value="42"/>
    <asp:ListItem Text="43" Value="43"/>
    <asp:ListItem Text="44" Value="44"/>
    <asp:ListItem Text="45" Value="45"/>
    <asp:ListItem Text="46" Value="46"/>
    <asp:ListItem Text="47" Value="47"/>
    <asp:ListItem Text="48" Value="48"/>
    <asp:ListItem Text="49" Value="49"/>
    <asp:ListItem Text="50" Value="50"/>
    <asp:ListItem Text="51" Value="51"/>
    <asp:ListItem Text="52" Value="52"/>
    </asp:DropDownList>
    
    <asp:DropDownList enabled="false" ID="DropDownListCharactorMonth" style="position:absolute; left: 554px; top: 28px;" runat="server" Font-Names="Arial" Font-Size="12px" Width="88px" Visible="False">
            <asp:ListItem Text="January" Value="1"/>
            <asp:ListItem Text="February" Value="2"/>
            <asp:ListItem Text="March" Value="3"/>
            <asp:ListItem Text="April" Value="4"/>
            <asp:ListItem Text="May" Value="5"/>
            <asp:ListItem Text="June" Value="6"/>
            <asp:ListItem Text="July" Value="7"/>
            <asp:ListItem Text="August" Value="8"/>
            <asp:ListItem Text="September" Value="9"/>
            <asp:ListItem Text="October" Value="10"/>
            <asp:ListItem Text="November" Value="11"/>
            <asp:ListItem Text="December" Value="11"/>
    </asp:DropDownList>
    
    <asp:DropDownList enabled="false" ID="DropDownListMonthNumbers" style="position:absolute; left: 216px; top: 170px;" runat="server" Font-Names="Arial" Font-Size="12px" Width="45px">
        <asp:ListItem Text="1" Value="1"/>
        <asp:ListItem Text="2" Value="2"/>
        <asp:ListItem Text="3" Value="3"/>
        <asp:ListItem Text="4" Value="4"/>
        <asp:ListItem Text="5" Value="5"/>
        <asp:ListItem Text="6" Value="6"/>
        <asp:ListItem Text="7" Value="7"/>
        <asp:ListItem Text="8" Value="8"/>
        <asp:ListItem Text="9" Value="9"/>
        <asp:ListItem Text="10" Value="10"/>
        <asp:ListItem Text="11" Value="11"/>
        <asp:ListItem Text="12" Value="12"/>
 </asp:DropDownList>

 <asp:DropDownList enabled="false" ID="DropDownListWeekDayName" style="position:absolute; left: 125px; top: 109px;" runat="server" Font-Names="Arial" Font-Size="12px" Width="103px">
            <asp:ListItem Text="Monday" Value="1"/>
            <asp:ListItem Text="Tuesday" Value="2"/>
            <asp:ListItem Text="Wednesday" Value="3"/>
            <asp:ListItem Text="Thursday" Value="4"/>
            <asp:ListItem Text="Friday" Value="5"/>
            <asp:ListItem Text="Saturday" Value="6"/>
            <asp:ListItem Text="Sunday" Value="7"/>
 </asp:DropDownList>


<asp:Label ID="Label13" runat="server" Font-Names="arial" Font-Size="12px" Height="19px"  Style="left: 13px; position: absolute; top: 109px" Text="Read every" Width="103px"></asp:Label>
<asp:Label ID="Label14" runat="server" Font-Names="arial" Font-Size="12px" Height="19px"  Style="left: 239px; position: absolute; top: 109px" Text="Starting from" Width="79px"></asp:Label>
<asp:Label ID="Label15" runat="server" Font-Names="arial" Font-Size="12px" Height="19px"  Style="left: 124px; position: absolute; top: 141px" Text="Starting Week"   Width="89px"></asp:Label>
<asp:Label ID="Label16" runat="server" Font-Names="arial" Font-Size="12px" Height="19px"  Style="left: 265px; position: absolute; top: 141px" Text="Starting Year" Width="81px"></asp:Label>
<asp:Label ID="Label17" runat="server" Font-Names="arial" Font-Size="12px" Height="19px"  Style="left: 267px; position: absolute; top: 171px" Text="Starting Year" Width="81px"></asp:Label>
<asp:Label ID="Label18" runat="server" Font-Names="arial" Font-Size="12px" Height="19px"  Style="left: 123px; position: absolute; top: 172px" Text="Starting Month"   Width="87px"></asp:Label>
<asp:Label ID="Label19" runat="server" Font-Names="arial" Font-Size="12px" Height="19px" Style="left: 126px; position: absolute; top: 79px" Text="Starting from" Width="79px"></asp:Label>
<asp:Label ID="Label26" runat="server" Font-Names="arial" Font-Size="12px" Height="19px"  Style="left: 14px; position: absolute; top: 141px" Text="Week number" Width="100px"></asp:Label>
<asp:Label ID="Label28" runat="server" Font-Names="arial" Font-Size="12px" Height="19px"  Style="left: 13px; position: absolute; top: 78px" Text="Each day"  Width="105px"></asp:Label>

    <asp:TextBox ID="ntxtEachdayStart" runat="server" Enabled="False" 
        Font-Names="arial" Font-Size="12px" 
        Style="left: 469px; position: absolute; top: 185px" Visible="false" 
        Width="64px"></asp:TextBox>




<asp:SqlDataSource ID="SQL_MeterReadingsGroupYEAR" runat="server" 
ConnectionString="<%$ ConnectionStrings:portal.utilitymasters.co.ukConnectionString %>" 
SelectCommand="EXEC dbo.spCustomer_MeterReadingsYEAR 0"></asp:SqlDataSource>


        <asp:SqlDataSource ID="SQL_MeterReadings" runat="server" ConnectionString="<%$ ConnectionStrings:portal.utilitymasters.co.ukConnectionString %>" 
        SelectCommand="EXEC spCustomer_MeterReadings '1980',0">
        </asp:SqlDataSource>
            
    
<asp:SqlDataSource ID="SQL_Chart" runat="server" 
ConnectionString="<%$ ConnectionStrings:portal.utilitymasters.co.ukConnectionString %>" 
SelectCommand="select Figure,Recordeddate from tblCustomer_ScheduledMeters where CompanyID=0 and meterId=0">
</asp:SqlDataSource>     


<asp:SqlDataSource ID="SQLPersonnel" runat="server" 
ConnectionString="<%$ ConnectionStrings:portal.utilitymasters.co.ukConnectionString %>" 
SelectCommand="select 1-1">
</asp:SqlDataSource>

<asp:SqlDataSource ID="SQLPersonnelTwo" runat="server" 
ConnectionString="<%$ ConnectionStrings:portal.utilitymasters.co.ukConnectionString %>" 
SelectCommand="select 1-1">
</asp:SqlDataSource>

<asp:SqlDataSource ID="SQL_MeterGeneral" runat="server" ConnectionString="<%$ ConnectionStrings:portal.utilitymasters.co.ukConnectionString %>" 
SelectCommand="Select 1-1">
</asp:SqlDataSource>    

<asp:SqlDataSource ID="SQL_Templates" runat="server" ConnectionString="<%$ ConnectionStrings:portal.utilitymasters.co.ukConnectionString %>" 
SelectCommand=" SELECT * FROM vwCustomer_TemplatesAndMeasurementsCombined">
</asp:SqlDataSource>

<asp:SqlDataSource ID="SQL_UserSitesCreateMeter" runat="server" ConnectionString="<%$ ConnectionStrings:portal.utilitymasters.co.ukConnectionString %>" 
SelectCommand=" SELECT 1-1">
</asp:SqlDataSource>

  </asp:Panel>
  
  
  </div>
  
    </form>


</asp:Content>

