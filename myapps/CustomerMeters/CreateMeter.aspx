﻿<%@ Page Language="VB" MasterPageFile="~/landing.master" AutoEventWireup="false" CodeFile="CreateMeter.aspx.vb" Inherits="myapps_CustomerMeters_CreateMeter" title="Utility Masters | Create a Meter" %>

<asp:Content ID="Content1" ContentPlaceHolderID="content_right" Runat="Server">
<SCRIPT LANGUAGE="javascript" type="text/javascript"  >
function showmj(what, clearme)
 {
   if (clearme == "Y")
     {  document.getElementById("HJ").innerHTML = "";  }
     else 
     { document.getElementById("HJ").innerHTML = what; }
}
</SCRIPT>



<style>
.general {background: transparent url('/assets/content-back-general.jpg') no-repeat; padding-top:15px; padding-left:10px;}
.navgeneral 
{
position:absolute;top:83px;left:0px;width:220px;height:500px;background: white url('/assets/left-nav-account.png') no-repeat; padding-top:0px; padding-left:0px;
 }

.CompanySite
{
direction:ltr;
overflow-x:hidden;
overflow-y:scroll;
scrollbar-arrow-color: #4e658a;
scrollbar-3dlight-color: #777799;
scrollbar-darkshadow-color: #666677;
scrollbar-face-color: #a1b1c3;
scrollbar-highlight-color: #e9e9e9;
scrollbar-shadow-color: #a1b1c3;
scrollbar-track-color: #ffffff;
position:absolute;top:45px;left:0px;width:217px;height:445px;
}

</style>

<form id="form1" Runat="Server">

<div  class="navgeneral">
    <div id="HJ" align="center" style="text-align:center;position:absolute;top:10px;left:5px;font-family:Arial;font-size:12px;color:Blue;"></div>
    <div class="CompanySite" onmouseout="javascript:showmj('','Y')">
      <ul class="styleformytreeview" runat="server" id="navOnlineReports">  </ul> 
    </div>
</div>

<div  style="position:absolute;top:83px;left:230px;width:700px;height:550px" class="general">
<asp:ImageButton ID="ImageButtonCreateTheMeters" 
        style="position:absolute;left:11px; top: 5px; height:52px; width: 101px;" 
        ImageUrl="~/assets/metering/buttons-myapps-dataentry-createmeter.gif"   
        runat="server" ImageAlign="Left"/>

 <asp:Label ID="LabTitle" style="position:absolute;top:2px; left:113px;"  
        Font-Names="Arial"   Font-Size="14px" runat="server" ForeColor="Black"  
        Font-Bold="true" Text="Create Meter Readings"></asp:Label> 

 <asp:Label ID="LabCompany" style="position:absolute;top:20px; left:115px;"  
        Font-Names="Arial"   Font-Size="12px" runat="server" ForeColor="DarkBlue"  
        Font-Bold="False" Text="Company name"></asp:Label> 
        
 <asp:Label ID="LabSite"    style="position:absolute;top:39px; left:116px;"  
        Font-Names="Arial"   Font-Size="11px" runat="server" ForeColor="DarkGreen"  
        Font-Bold="False" Text="Please select a valid Company"></asp:Label> 
 
 <asp:ImageButton  ID="ImageButtonReturnToMenu"   visible="true"  
        style="position:absolute;left:628px; top:6px;"  height="32px"  width="32px"   
        tooltip="Return to Meters menu" runat="server"   ImageAlign="Left"  
        ImageUrl="~/assets/metering/remove_48x48.png">
</asp:ImageButton>
 
<asp:Panel ID="PanelPureMeters" 
runat="server"
visible="true"  
style="position:absolute;left: 12px; top: 60px;"  
Width="650px" height="400px"   
BorderStyle="None"    
ScrollBars="none" BackColor="#FFFFC0">

<asp:Label ID="Label1" runat="server" visible="false" Font-Names="Arial" Font-Size="12px" Style="left: 10px;    position: absolute; top: 43px" Text="Site" Width="96px"></asp:Label>
<asp:DropDownList 
visible="false"
ID="DropDownListSiteLevel"  
DataSourceID="SQL_UserSitesCreateMeter" 
DataTextField="Value" 
DataValueField="ID" 
style="position:absolute;top: 42px; left: 118px;"  
Font-Names="Arial"  Font-Size="12px"  
Width="362px" BackColor="#ffffff" ForeColor="black" runat="server">
</asp:DropDownList>


        
<asp:Label ID="Label5" style="position:absolute;top: 71px; left: 9px;" runat="server"   Font-Names="Arial"  Font-Size="12px"  Width="100px" Text="Meter name" ></asp:Label>
<asp:TextBox ID="txtMeterName" style="position:absolute;top: 70px; left: 118px;"  maxlength="100"  runat="server"    Font-Names="Arial"  Font-Size="12px"  Width="357px" ></asp:TextBox>

<asp:Label ID="Label7" runat="server"  style="position:absolute;top: 100px; left: 8px;"  Font-Names="Arial"  Font-Size="12px"  width="100px" Text="Location" ></asp:Label>
<asp:TextBox ID="txtMeterLocation" style="position:absolute;top: 100px; left: 118px;"  maxlength="100"  Font-Names="Arial"  Font-Size="12px"  runat="server" Width="357px"   ></asp:TextBox>

<asp:Label ID="Label6" runat="server" style="position:absolute;top: 127px; left: 10px;"  Font-Names="Arial"  Font-Size="12px"  width="100px"  Text="Template" ></asp:Label>

<asp:DropDownList
ID="DropDownList3"  
runat="server"
style="position:absolute;top: 129px; left: 119px;"  Font-Names="Arial"  Font-Size="12px"  
Width="361px" BackColor="#ffffff" ForeColor="Black" 
DataSourceID="SQL_Templates" 
DataTextField="TemplateDescription" 
DataValueField="ID">
</asp:DropDownList>

<asp:Label ID="Label20" runat="server"   style="position:absolute;top: 186px; left: 10px;"  Font-Names="Arial"  Font-Size="12px"  Width="100px" Text="Identifier" ></asp:Label>
<asp:TextBox ID="txtIdentifier" runat="server"    style="position:absolute;top: 184px; left: 120px;"   maxlength="25" Font-Names="Arial"  Font-Size="12px"  Width="100px" ></asp:TextBox>

<asp:Label ID="Label27" runat="server"   style="position:absolute;top: 156px; left: 10px;"  Font-Names="Arial"  Font-Size="12px"  Text="Type of meter" Width="96px" ></asp:Label>&nbsp;
<asp:DropDownList ID="DropDownTypeOf"  style="position:absolute;top: 157px; left: 119px;"  Font-Names="Arial"  Font-Size="12px"  Width="165px" BackColor="#ffffff" ForeColor="black" runat="server"></asp:DropDownList>

<asp:DropDownList ID="DropDownListUseage" style="position:absolute;top: 209px; left: 120px;"  Font-Names="Arial"  Font-Size="12px"  Width="111px" BackColor="#ffffff" ForeColor="black" runat="server"></asp:DropDownList>
<asp:DropDownList ID="DropDownListLoging" style="position:absolute;top: 238px; left: 119px;"  Font-Names="Arial"  Font-Size="12px"  Width="111px" BackColor="#ffffff" ForeColor="black" runat="server"></asp:DropDownList>

<asp:Label ID="Label21" runat="server"   style="position:absolute;top: 266px; left: 10px;"  Font-Names="Arial"  Font-Size="12px"  Width="100px" Text="Installed date" ></asp:Label>
<asp:TextBox ID="txtInstalleddate" style="position:absolute;top: 264px; left: 120px;"  runat="server"     Font-Names="Arial"  Font-Size="12px"  Width="74px" ></asp:TextBox>

<asp:Label ID="Label22" runat="server"  style="position:absolute;top: 292px; left: 10px;"  Font-Names="Arial"  Font-Size="12px"  Width="100px" Text="Last serviced" ></asp:Label>
<asp:TextBox ID="txtLastserviced"  style="position:absolute;top: 290px; left: 120px;"  runat="server"    Font-Names="Arial"  Font-Size="12px"  Width="74px" ></asp:TextBox>
 
<asp:ImageButton ID="ImageButtonMeters_Add"  style="position:absolute;top: 349px; left: 537px;"  visible="true"  tooltip="Append details to database"  Height="20px" Width="100px"  ImageUrl="~/assets/button_save.gif" runat="server" />
     <asp:Label ID="Label2" runat="server" Font-Names="Arial" Font-Size="12px" Style="left: 10px;  position: absolute; top: 212px" Text="Useage" Width="100px"></asp:Label>
     <asp:Label ID="Label29" runat="server" Font-Names="Arial" Font-Size="12px" Style="left: 11px;    position: absolute; top: 239px" Text="Logging" Width="100px"></asp:Label>
     <asp:Label ID="errorlabel3" runat="server" 
        style="position:absolute; left: 12px; top: 16px; width: 620px;" 
        BorderStyle="None" Font-Names="Arial" Font-Size="12px" ForeColor="Red"></asp:Label>
</asp:Panel> 

</div>
 
</form> 
 

<asp:SqlDataSource ID="SQL_Templates" runat="server" ConnectionString="<%$ ConnectionStrings:portal.utilitymasters.co.ukConnectionString %>" 
SelectCommand="SELECT * FROM [vwCustomer_TemplatesAndMeasurementsCombined]">
</asp:SqlDataSource>

<asp:SqlDataSource ID="SQL_UserSitesCreateMeter" runat="server" ConnectionString="<%$ ConnectionStrings:portal.utilitymasters.co.ukConnectionString %>" 
SelectCommand=" SELECT 1-1 as 'value', 1-1 as 'id'">
</asp:SqlDataSource>
 
</asp:Content>

