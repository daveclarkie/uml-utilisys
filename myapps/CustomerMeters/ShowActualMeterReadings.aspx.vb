﻿Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Drawing
Imports System.Web
Imports Dundas.Charting.WebControl
Imports Dundas.Charting.WebControl.Utilities
Imports System.Web.Security
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.SessionState
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports System.Data
Imports System.Data.OleDb
Partial Class myapps_CustomerMeters_ShowActualMeterReadings
    Inherits System.Web.UI.Page

    Public Shared StayOnPage As Boolean
    Public Shared tHOLD As New ArrayList

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Me.LabCompany.Text = Session("CustomerName")
        Me.LabSite.Text = Session("MeterName")
        ' Session("MainURL") = Me.Page.Request.Url.PathAndQuery.ToString  'Request.ServerVariables("URL")
        ' Session("MeterID") = mySelectedRow.Cells(1).Text
        ' Session("MeterName") = mySelectedRow.Cells(2).Text
        Call Me.WhichMeter(Session("MeterName"))

        Me.PanelGridView2.BorderStyle = BorderStyle.Solid
        Me.PanelGridView2.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFFFC0")
        Me.PanelGridView2.BorderWidth = 1
        Me.PanelGridView2.BorderColor = Color.LightGray

    End Sub

    Protected Sub ImageButtonReturnToMenu_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonReturnToMenu.Click
        Response.Redirect(Session("MainURL"))
    End Sub

    Protected Sub ButtonLoad_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonLoad.Click
        If Button1.Text = "Hide Chart" Then
            Button1.Text = "Show Chart"
        End If

        Button1.Visible = True

        Me.PanelGridView2Graph.Visible = False
        Me.PanelGridView2.Visible = True
        Call LoadUpDetails()
    End Sub

    Protected Function LoadUpDetails() As Boolean
        On Error Resume Next
        Dim strSQL As String = ""
        Dim myItem As System.Data.DataView = CType(SQL_MeterGeneral.Select(DataSourceSelectArguments.Empty), System.Data.DataView)

        Me.labelerror.Visible = False
        Call Me.recalculate(Me.RadioButtonList1.SelectedValue, Me.RadioButtonList2.SelectedValue, Me.RadioOutStanding.SelectedValue)
        Call Me.CreateNextReadingDate()
        LoadUpDetails = True
    End Function

 
    Protected Function WhichMeter(ByVal mySelectedRowWas As String) As String
        On Error Resume Next
        Dim strSQL As String = ""
        strSQL = ""
        strSQL = strSQL & "  SELECT * FROM dbo.tblCustomer_Meters "
        strSQL = strSQL & "  WHERE ID=" & Session("MeterID")
        Me.SQL_MeterGeneral.SelectCommand = strSQL
        Dim T1 As String = ""
        Dim T2 As String = ""
        Dim T3 As String = ""

        Dim ivwExpensiveItems As System.Data.DataView = CType(SQL_MeterGeneral.Select(DataSourceSelectArguments.Empty), System.Data.DataView)
        If ivwExpensiveItems.Count > 0 Then
            For Each rowProduct As System.Data.DataRowView In ivwExpensiveItems
                Select Case rowProduct("TypeOf")
                    Case "C" ' Consumption
                        T1 = "Consumption"
                    Case "R" ' Reading ONLY
                        T1 = "Readings"
                    Case "O" ' OTher type
                    Case Else
                End Select

                Select Case rowProduct("ScheduleType")
                    Case "D"
                        T2 = "Daily read"
                        Me.HiddenFieldScheduleType.Value = "D"
                    Case "M" ' 
                        T2 = "Monthly read"
                        Me.HiddenFieldScheduleType.Value = "M"
                    Case "W"
                        T2 = "Weekly read - Specific day"
                        Me.HiddenFieldScheduleType.Value = "W"
                    Case "WK"
                        T2 = "Weekly read - No specific day"
                        Me.HiddenFieldScheduleType.Value = "W"
                    Case Else
                End Select

                Select Case rowProduct("UsedFor")
                    Case "E" ' Consumption
                        T3 = "Energy meter"
                    Case "P" ' Reading ONLY
                        T3 = "Production meter"
                    Case Else
                End Select
                Exit For
            Next

            Me.SelectedMeterName.Text = "(" & T1 & ")" & " - " & T2 & " (" & T3 & ")"
        End If

        WhichMeter = "Ok"
    End Function


    Protected Function recalculate(ByVal what As String, ByVal OrderBy As String, ByVal FilterBy As String) As String
        On Error Resume Next
        Dim strSQL As String = ""

        ' Check to see if there any new meters
        Dim myReader As String = ""
        Dim rs As New ADODBConnection
        rs.OpenConnection()
        strSQL = "SELECT  TOP 1 ID from dbo.tblCustomer_ScheduledMeters where meterid=" & Session("MeterID")
        rs.OpenRecordSet(strSQl)
        If rs.rComm.RecordCount > 0 Then
            rs.CloseConnection()
        Else
            rs.CloseConnection()
            ' There is a meter setup but no reading on the database
            ' must be the first reading
            Response.Redirect("ShowActualFirstReadingSetup.aspx?meterID=" & Session("MeterId"))
        End If

        Dim WhichDays As String = ""
        Dim WhichOrder As String = ""
        Dim WhichDaysNumeric As Integer = -1
        Dim iRequired As String = "No"

        Select Case what
            Case "12 months"
                WhichDays = "-366"
                WhichDaysNumeric = -366
            Case "6 months"
                WhichDays = "-" & (366 / 2)
                WhichDaysNumeric = -366 / 2
            Case "30 days"
                WhichDays = "-30"
                WhichDaysNumeric = -30
            Case "All"
                WhichDays = "-10000"
                WhichDaysNumeric = -10000
        End Select

        Select Case Me.RadioButtonList2.SelectedValue
            Case "Assending"
                WhichOrder = 0
                Session("WhichOrder") = "Assending"
            Case "Descending"
                WhichOrder = 1
                Session("WhichOrder") = "Descending"

        End Select

        Select Case Me.RadioOutStanding.SelectedValue
            Case "ReadRequired"
                iRequired = "Y"
            Case "AllMeters"
                iRequired = "N"
        End Select

        Me.HiddenFieldStartDate.Value = DateAdd(DateInterval.Day, WhichDaysNumeric, Now.Date)
        Me.HiddenFieldFinishDate.Value = Now.Date

        strSQL = " EXEC dbo.spCustomer_MeterReadings_Flexible " & Session("MeterID") & "," & WhichDays & "," & WhichOrder & "," & "'" & iRequired & "'"
        Me.SQL_MeterReadings.SelectCommand = strSQl
        Me.GridView2.DataBind()

        If Me.GridView2.Rows.Count = 0 Then
            Me.PanelGridView2.Visible = False
            Me.ButtonUpdate.Visible = False
        Else
            Me.PanelGridView2.Visible = True
            Me.ButtonUpdate.Visible = True
        End If

        '  Call ProcessChart()
        recalculate = "Ok"
    End Function

    Protected Function CreateNextReadingDate() As String
        On Error Resume Next
        Dim strSQL As String = ""
        Dim CurrentDateIs As String = ""
        Dim TheDayIS As String = ""
        Select Case Me.HiddenFieldScheduleType.Value
            Case "D"
                strSQL = " SELECT john.TheDate  FROM  (  SELECT  CONVERT(VARCHAR(20),MAX(RecordedDate),103)   AS 'TheDate'   FROM dbo.tblCustomer_ScheduledMeters   WHERE MeterID=" & Session("MeterID") & " 	) AS john"
            Case "W"
                '  strSQL = "SELECT TOP 1 *   FROM dbo.tblCustomer_ScheduledMeters   WHERE MeterID= " & Me.hiddenMeterID.Value
                strSQL = " SELECT TOP 1 dbo.tblCustomer_ScheduledMeters.* , tblCustomer_Meters.Readingmethod    FROM dbo.tblCustomer_ScheduledMeters   "
                strSQL = strSQL & "  INNER JOIN dbo.tblCustomer_Meters ON dbo.tblCustomer_Meters.ID = tblCustomer_ScheduledMeters.MeterID "
                strSQL = strSQL & " WHERE MeterID=" & Session("MeterID") & "  ORDER BY dbo.tblCustomer_ScheduledMeters.RecordedDate DESC "
            Case "M"
                strSQL = "SELECT TOP 1 *   FROM dbo.tblCustomer_ScheduledMeters   WHERE MeterID= " & Session("MeterID") & "  ORDER BY dbo.tblCustomer_ScheduledMeters.RecordedDate DESC "
            Case "WK"
                strSQL = "SELECT TOP 1 *   FROM dbo.tblCustomer_ScheduledMeters   WHERE MeterID= " & Session("MeterID") & "  ORDER BY dbo.tblCustomer_ScheduledMeters.RecordedDate DESC "
        End Select

        Dim myReader As String = ""
        Dim rs As New ADODBConnection
        rs.OpenConnection()
        rs.OpenRecordSet(strSQL)

        Select Case Me.HiddenFieldScheduleType.Value
            Case "D"
                CurrentDateIs = rs.rComm.Fields("TheDate").Value
                Me.NextReadDate.Text = "Next reading date will be - " & DateAdd(DateInterval.Day, 1, CDate(CurrentDateIs))
                Me.NextReadingDate.Text = "Your next reading will be -  " & DateAdd(DateInterval.Day, 1, CDate(CurrentDateIs))
                Me.HiddenFieldNextReadingDate.Value = DateAdd(DateInterval.Day, 1, CDate(CurrentDateIs))
            Case "W"
                ' myReader = rs.rComm.Fields("TheDate").Value
                Dim NewDateIs As String = ""
                Dim TheStringIS As String = Right("00" & Day(rs.rComm.Fields("RecordedDate").Value), 2) & "/" & Right("00" & Month(rs.rComm.Fields("RecordedDate").Value), 2) & "/" & Right("0000" & Year(rs.rComm.Fields("RecordedDate").Value), 4)
                CurrentDateIs = rs.rComm.Fields("WeekNumber").Value
                TheDayIS = rs.rComm.Fields("ReadingMethod").Value 'myReader("ReadingMethod")
                NewDateIs = DateAdd(DateInterval.Day, 7, CDate(TheStringIS))
                ' need to find the Thursday in this week , then display
                Me.NextReadDate.Text = "Next reading will be Week  " & CurrentDateIs + 1 & "  on " & Me.GetTheWeekName(TheDayIS) & " - " & NewDateIs
                Me.NextReadingDate.Text = "Your next reading will be Week " & CurrentDateIs + 1 & "  on " & Me.GetTheWeekName(TheDayIS) & " - " & NewDateIs
                Me.HiddenFieldNextReadingDate.Value = CurrentDateIs + 1 & " - " & Me.GetTheWeekName(TheDayIS) & " - " & NewDateIs
            Case "M"
                ' myReader.Read()
                CurrentDateIs = rs.rComm.Fields("MonthNumber").Value 'myReader("MonthNumber")
                Me.NextReadDate.Text = "Next reading will be - " & Me.GetTheMonthName(CurrentDateIs + 1) & " - " & Me.AddMonthNumber(rs.rComm.Fields("RecordedDate").Value)
                Me.NextReadingDate.Text = "Your next reading will be - " & Me.GetTheMonthName(CurrentDateIs + 1) & " - " & Me.AddMonthNumber(rs.rComm.Fields("RecordedDate").Value)
                Me.HiddenFieldNextReadingDate.Value = Me.GetTheMonthName(CurrentDateIs + 1) & " - " & Me.AddMonthNumber(rs.rComm.Fields("RecordedDate").Value)
            Case "WK"
                ' myReader.Read()
                CurrentDateIs = rs.rComm.Fields("WeekNumber").Value 'myReader("WeekNumber")
                Me.NextReadDate.Text = "Next reading will be Week - " & CurrentDateIs + 1
                Me.NextReadingDate.Text = "Your next reading will be Week - " & CurrentDateIs + 1
                Me.HiddenFieldNextReadingDate.Value = CurrentDateIs + 1
        End Select
        rs.CloseConnection()
        CreateNextReadingDate = "OK"
    End Function

    Public Function GetWeekNumber(ByVal inDate As Date) As Integer
        Dim dayOfYear As Integer, wkNumber As Integer
        Dim compensation As Integer = 0
        Dim oneDate As String
        Dim firstDayDate As Date
        dayOfYear = inDate.DayOfYear
        oneDate = "1/1/" & inDate.Year.ToString
        firstDayDate = DateAndTime.DateValue(oneDate)
        Select Case firstDayDate.DayOfWeek
            Case DayOfWeek.Sunday
                compensation = 0
            Case DayOfWeek.Monday
                compensation = 6
            Case DayOfWeek.Tuesday
                compensation = 5
            Case DayOfWeek.Wednesday
                compensation = 4
            Case DayOfWeek.Thursday
                compensation = 3
            Case DayOfWeek.Friday
                compensation = 2
            Case DayOfWeek.Saturday
                compensation = 1
        End Select
        dayOfYear = dayOfYear - compensation
        If dayOfYear Mod 7 = 0 Then
            wkNumber = dayOfYear / 7
        Else
            wkNumber = (dayOfYear \ 7) + 1 'WATCH THE OPERATOR \ !!! IT RETURNS THE INTEGER RESULT
        End If
        Return wkNumber
    End Function

    Protected Function GetTheWeekName(ByVal what As String) As String
        Dim nReturn As String = ""
        Select Case what
            Case "1" : nReturn = "Monday"
            Case "2" : nReturn = "Tuesday"
            Case "3" : nReturn = "Wednesday"
            Case "4" : nReturn = "Thursday"
            Case "5" : nReturn = "Friday"
            Case "6" : nReturn = "Saturday"
            Case "7" : nReturn = "Sunday"
        End Select

        GetTheWeekName = nReturn
    End Function

    Protected Function GetTheMonthName(ByVal what As String) As String
        Dim nReturn As String = ""
        Select Case what
            Case "1" : nReturn = "January"
            Case "2" : nReturn = "February"
            Case "3" : nReturn = "March"
            Case "4" : nReturn = "April"
            Case "5" : nReturn = "May"
            Case "6" : nReturn = "June"
            Case "7" : nReturn = "July"
            Case "8" : nReturn = "August"
            Case "9" : nReturn = "September"
            Case "10" : nReturn = "October"
            Case "11" : nReturn = "November"
            Case "12" : nReturn = "December"
        End Select

        GetTheMonthName = nReturn
    End Function

    Protected Function GetTheNameMonth(ByVal what As String) As String
        Dim nReturn As String = ""
        Select Case what
            Case "January" : nReturn = "01"
            Case "February" : nReturn = "02"
            Case "March" : nReturn = "03"
            Case "April" : nReturn = "04"
            Case "May" : nReturn = "05"
            Case "June" : nReturn = "06"
            Case "July" : nReturn = "07"
            Case "August" : nReturn = "08"
            Case "September" : nReturn = "09"
            Case "October" : nReturn = "10"
            Case "November" : nReturn = "11"
            Case "December" : nReturn = "12"
        End Select

        GetTheNameMonth = nReturn
    End Function

    Public Function WeekOfMonth(ByVal dteInputDate As Date, ByVal intStartWeekday As Integer) As Integer
        Dim intWeekdayOf1st As Integer
        Dim intDaysInWeek1 As Integer
        Dim intDaysPastWeek1 As Integer
        Dim iReturn As Double

        intWeekdayOf1st = Weekday(DateSerial(Year(dteInputDate), Month(dteInputDate), 1))
        intDaysInWeek1 = (6 + intStartWeekday - intWeekdayOf1st) Mod 7 + 1
        If Day(dteInputDate) <= intDaysInWeek1 Then
            WeekOfMonth = 1
        Else
            intDaysPastWeek1 = Day(dteInputDate) - intDaysInWeek1
            ' just me being silly emmmmmmmmm but it works
            '  WeekOfMonth = 1 + (intDaysPastWeek1 \ 7) + Math.Abs(intDaysPastWeek1 Mod 7 <> 0)
            iReturn = intDaysPastWeek1 Mod 7 <> 0
            WeekOfMonth = 1 + (intDaysPastWeek1 \ 7) + Math.Abs(iReturn)
        End If
    End Function

    Protected Function WeekNumber(ByVal dteValue As Date) As Integer
        'Nicked from VB.NET rare as rocking horse *%^$
        Dim cult_info As System.Globalization.CultureInfo = System.Globalization.CultureInfo.CreateSpecificCulture("no")
        Dim cal As System.Globalization.Calendar = cult_info.Calendar
        'Dim weekNO As Integer = cal.GetWeekOfYear(dteValue, cult_info.DateTimeFormat.CalendarWeekRule, cult_info.DateTimeFormat.FirstDayOfWeek)
        WeekNumber = cal.GetWeekOfYear(dteValue, cult_info.DateTimeFormat.CalendarWeekRule, cult_info.DateTimeFormat.FirstDayOfWeek)
    End Function

    Protected Function MonthNumber(ByVal dteValue As Date) As Integer
        'Nicked from VB.NET rare as rocking horse *%^$
        Dim cult_info As System.Globalization.CultureInfo = System.Globalization.CultureInfo.CreateSpecificCulture("no")
        Dim cal As System.Globalization.Calendar = cult_info.Calendar
        'Dim weekNO1 As Integer = cal.GetMonth(dteValue)
        MonthNumber = cal.GetMonth(dteValue)
    End Function

    Protected Function AddWeekNumber(ByVal dteValue As Date) As Date
        'Nicked from VB.NET rare as rocking horse *%^$
        Dim cult_info As System.Globalization.CultureInfo = System.Globalization.CultureInfo.CreateSpecificCulture("no")
        Dim cal As System.Globalization.Calendar = cult_info.Calendar
        'Dim weekNO As Date = cal.AddWeeks(dteValue, 1)
        AddWeekNumber = cal.AddWeeks(dteValue, 1)
    End Function

    Protected Function AddMonthNumber(ByVal dteValue As Date) As Date
        'Nicked from VB.NET rare as rocking horse *%^$
        Dim cult_info As System.Globalization.CultureInfo = System.Globalization.CultureInfo.CreateSpecificCulture("no")
        Dim cal As System.Globalization.Calendar = cult_info.Calendar
        'Dim weekNO3 As Date = cal.AddMonths(dteValue, 1)
        AddMonthNumber = cal.AddMonths(dteValue, 1)
    End Function

    Protected Sub ButtonUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonUpdate.Click
        Dim strSQL As String = ""
        labelerror.Visible = False
        Dim nReturn As Integer = 6
        StayOnPage = False
        If iChanges() = True Then
            If iValidate() = True Then
                labelerror.Visible = True
                StayOnPage = True
                Exit Sub
            Else
            End If

            ' physically update the  
            Dim rs As New ADODBConnection
            rs.OpenConnection()

            Dim i As Integer = 0
            For Each gvRow As GridViewRow In GridView2.Rows
                Dim TheID As Integer = Convert.ToInt32(GridView2.DataKeys(gvRow.RowIndex).Value)
                Dim iRead As TextBox = gvRow.FindControl("Reading")
                Dim iComment As TextBox = gvRow.FindControl("Comment")
                Dim m As String = tHOLD(i)

                If Trim(iRead.Text) = Trim(tHOLD(i)) Then
                Else
                    strSQL = ""
                    strSQL = strSQL & "UPDATE [portal.utilitymasters.co.uk].dbo.tblCustomer_ScheduledMeters "
                    strSQL = strSQL & " SET Figure= " & "'" & Trim(iRead.Text) & "'"
                    strSQL = strSQL & "  ,  tagNote= " & "'" & XY(Trim(iComment.Text)) & "'"
                    strSQL = strSQL & "  WHERE ID=" & TheID
                    rs.OpenRecordSet(strSQL)
                End If
            Next
            rs.CloseConnection()
            ' Response.Redirect(Page.Request.Url.PathAndQuery)
        Else
        End If
    End Sub

    Protected Sub GridView2_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView2.RowCommand
        If StayOnPage = True Then
        Else
            tHOLD.Clear()
        End If

        'If IsNumeric(e.CommandArgument) = True Then
        '    Dim xindex As Integer = Convert.ToInt32(e.CommandArgument)
        '    Dim mySelectedRow As GridViewRow = GridView2.Rows(xindex)
        '    MsgBox(mySelectedRow.Cells(1).Text())

        'Else
        'End If

    End Sub

    Protected Sub GridView2_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView2.RowDataBound
        If e.Row.RowIndex = -1 Then
        Else
            tHOLD.Add(e.Row.Cells(0).Text)
        End If
    End Sub

    Protected Function iValidate() As Boolean
        Dim GotValid As Boolean = False
        Dim i As Integer = 0
        For Each gvRow As GridViewRow In GridView2.Rows
            '  Dim TheID As Integer = Convert.ToInt32(GridView2.DataKeys(gvRow.RowIndex).Value)
            Dim iRead As TextBox = gvRow.FindControl("Reading")
            Dim m As String = tHOLD(i)

            If IsNumeric(iRead.Text) = False Then ' Alph- only
                If Trim(iRead.Text) = "" Then ' agian still validate user might not have to current value
                    ' iRead.BackColor = Drawing.Color.Beige
                    ' iRead.ForeColor = Drawing.Color.Black
                    iRead.CssClass = "BeigeBlack"
                Else
                    '  iRead.Text = ""
                    'iRead.BackColor = Drawing.Color.Maroon
                    'Read.ForeColor = Drawing.Color.White
                    iRead.CssClass = "MaroonWhite"
                    GotValid = True
                End If
            Else
                'iRead.BackColor = Drawing.Color.Beige
                'iRead.ForeColor = Drawing.Color.Black
                iRead.CssClass = "BeigeBlack"
            End If
            i = i + 1
        Next

        iValidate = GotValid

    End Function


    Protected Function iChanges() As Boolean
        Dim GotChange As Boolean = False
        Dim i As Integer = 0
        For Each gvRow As GridViewRow In GridView2.Rows
            ' Dim TheID As Integer = Convert.ToInt32(GridView2.DataKeys(gvRow.RowIndex).Value)
            Dim iRead As TextBox = gvRow.FindControl("Reading")
            Dim m As String = tHOLD(i)
            If Trim(iRead.Text) = tHOLD(i) Then
            Else
                GotChange = True
            End If
            i = i + 1
        Next
        iChanges = GotChange
    End Function

    Protected Function XY(ByVal what As String) As String
        Dim nReturn As String = ""
        nReturn = what.Replace("'", "''")
        XY = nReturn
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack = True Then
        Else
            '   Call LoadUpDetails()
        End If
    End Sub

    Protected Function ProcessChart() As String
        On Error Resume Next
        Dim myConnectionString As String = "Provider=SQLNCLI.1;Integrated Security=SSPI;Persist Security Info=False;Initial Catalog=portal.utilitymasters.co.uk;Data Source=uk-ed0-sqlcl-01.MCEG.local"
        Dim iIndex As Integer = 0
        Dim iItem As String = ""
        Dim strSQL As String = ""
        Dim ItemToShow As String = ""

        strSQL = " SELECT "
        strSQL = strSQL & " dbo.tblCustomer_Meters.ID , "
        strSQL = strSQL & " dbo.tblCustomer_Meters.TypeOf , "
        strSQL = strSQL & "  dbo.tblCustomer_UnitsOfMeasure.iName ,"
        strSQL = strSQL & "  dbo.tblCustomer_UnitsOfMeasure.iDescription ,"
        strSQL = strSQL & "  dbo.tblCustomer_UnitsOfMeasure.iMultiplier ,"
        strSQL = strSQL & "  dbo.tblCustomer_UnitsOfMeasure.iMultiplierRequired ,"
        strSQL = strSQL & "  dbo.tblCustomer_UnitsOfMeasure.iConversionRequired ,"
        strSQL = strSQL & "   dbo.tblCustomer_UnitsOfMeasure.iFactor ,"
        strSQL = strSQL & "             dbo.tblCustomer_UnitsOfMeasure.iOperator"
        strSQL = strSQL & "             FROM dbo.tblCustomer_Meters "

        strSQL = strSQL & " INNER JOIN dbo.tblCustomer_MeterTypes ON dbo.tblCustomer_MeterTypes.ID= dbo.tblCustomer_Meters.MeterTypeID "
        strSQL = strSQL & " INNER JOIN dbo.tblCustomer_UnitsOfMeasure  ON tblCustomer_UnitsOfMeasure.ID=tblCustomer_MeterTypes.UnitOfMeasureID"

        '   strSQL = strSQL & "  INNER JOIN dbo.tblCustomer_MeterTypes ON dbo.tblCustomer_MeterTypes.UnitOfMeasureID= dbo.tblCustomer_Meters.MeterTypeID"
        '   strSQL = strSQL & "  INNER JOIN dbo.tblCustomer_UnitsOfMeasure ON dbo.tblCustomer_UnitsOfMeasure.Id = dbo.tblCustomer_MeterTypes.UnitOfMeasureID"

        strSQL = strSQL & "     WHERE dbo.tblCustomer_Meters.ID =" & Session("MeterID")

        Dim mySelectQuery As String = strSQL
        Dim myConnection As New OleDbConnection(myConnectionString)
        Dim myCommand As New OleDbCommand(mySelectQuery, myConnection)
        myCommand.Connection.Open()
        Dim myReader As OleDbDataReader = myCommand.ExecuteReader(CommandBehavior.CloseConnection)

        Dim ChartType As String = ""
        myReader.Read()

        Dim istrSQL As String = ""
        istrSQL = "  EXEC dbo.spCustomer_ReadingsToConsumption "
        istrSQL = istrSQL & "'" & Session("MeterID") & "'" & ","
        ' cap to graph to 30 days from to day

        'Dim currentday As Date = Now
        'Dim enddate As Date = DateAdd(DateInterval.Day, -30, Now)

        istrSQL = istrSQL & "'" & DateAdd(DateInterval.Day, -30, Now) & "'" & ","
        istrSQL = istrSQL & "'" & Now & "'" & ","

        '  istrSQL = istrSQL & "'" & currentday & "'" & ","
        'istrSQL = istrSQL & "'" & enddate & "'" & ","

        'istrSQL = istrSQL & "'" & Me.HiddenFieldStartDate.Value & "'" & ","
        'istrSQL = istrSQL & "'" & Me.HiddenFieldFinishDate.Value & "'" & ","

        istrSQL = istrSQL & "'" & "" & "'" & ","
        istrSQL = istrSQL & "'" & myReader("iMultiplierRequired") & "'" & ","
        istrSQL = istrSQL & "'" & myReader("iMultiplier") & "'" & ","
        istrSQL = istrSQL & "'" & myReader("iOperator") & "'" & ","
        istrSQL = istrSQL & "'" & myReader("iConversionRequired") & "'" & ","
        istrSQL = istrSQL & "'" & myReader("iFactor") & "'" & ","

        istrSQL = istrSQL & "'A'"

        '        myReader.Close()

        Dim Output_iMultiplier As String = myReader("iMultiplierRequired")
        Dim Output_iMultiplierRequired As String = myReader("iMultiplierRequired")
        Dim Output_iConversionRequired As String = myReader("iConversionRequired")
        Dim Output_iFactor As String = myReader("iFactor")
        Dim Output_iOperator As String = myReader("iOperator")
        Dim Output_iInputTitle As String = myReader("iName")
        Dim Output_iOutputTitle As String = myReader("iDescription")

        Dim imySelectQuery As String = istrSQL
        Dim imyConnection As New OleDbConnection(myConnectionString)
        Dim imyCommand As New OleDbCommand(imySelectQuery, imyConnection)
        imyCommand.Connection.Open()
        Dim imyReader As OleDbDataReader = imyCommand.ExecuteReader(CommandBehavior.CloseConnection)

        Chart1.Width = 670
        Chart1.Height = 400
        Chart1.Series.Clear()
        Chart1.Series.Add("Default")

        'Chart1.UI.Toolbar.Enabled = True
        'Chart1.UI.Toolbar.Position.Auto = True
        'Chart1.PaletteCustomColors = colorSet
        'Chart1.ChartAreas("Default").BackColor = Color.Silver
        'Chart1.BorderColor = Color.Black
        'Chart1.BorderStyle = ChartDashStyle.Solid
        'Chart1.BorderWidth = 2
        'Chart1.BorderSkin.SkinStyle = BorderSkinStyle.Emboss
        'Chart1.UI.Toolbar.Docking = ToolbarDocking.Top

        Dim commands As CommandCollection = Chart1.UI.Commands
        Dim cmdProperties As Command = commands.FindCommand(ChartCommandType.Properties)
        Dim cmdPalette As Command = commands.FindCommand(ChartCommandType.SelectPaletteGroup)
        Dim cmdChartType As Command = commands.FindCommand(ChartCommandType.SelectChartGroup)
        Dim cmdToggle3D As Command = commands.FindCommand(ChartCommandType.Toggle3D)

        cmdProperties.Visible = False
        cmdPalette.Visible = False
        cmdChartType.Visible = False
        cmdToggle3D.Visible = False

        Chart1.UI.Toolbar.Placement = ToolbarPlacement.InsideChart
        Chart1.UI.Toolbar.BorderSkin.SkinStyle = BorderSkinStyle.Emboss
        Chart1.UI.Toolbar.BorderSkin.FrameBackColor = Color.Black

        With Me.Chart1
            .BorderColor = Color.Black
            .BorderStyle = ChartDashStyle.Solid
            .BorderWidth = 2
            .BorderSkin.SkinStyle = BorderSkinStyle.Emboss

            .UI.Toolbar.Enabled = True
            .UI.Toolbar.Docking = ToolbarDocking.Top
            .UI.Toolbar.Placement = ToolbarPlacement.InsideChart
            .UI.Toolbar.BorderSkin.SkinStyle = BorderSkinStyle.Emboss
            .UI.Toolbar.BorderSkin.FrameBackColor = Color.Black
        End With

        'Chart1.Titles.Clear()
        'Chart1.Titles.Add("TopTitle")

        If myReader("TypeOf") = "C" Then
            Chart1.Titles("TopTitle").Text = "Consumption figures for period "
        Else
            Chart1.Titles("TopTitle").Text = "Meter reading values for period "
        End If

        Chart1.ChartAreas.Clear()
        Chart1.ChartAreas.Add("Default")
        Chart1.ChartAreas("Default").AxisX.MajorGrid.Enabled = False
        Chart1.ChartAreas("Default").AxisX.MinorGrid.Enabled = False

        Chart1.ChartAreas("Default").AxisY.MajorGrid.Enabled = False
        Chart1.ChartAreas("Default").AxisY.MinorGrid.Enabled = False

        If myReader("TypeOf") = "C" Then
            Chart1.ChartAreas("Default").AxisY.Title = "Consumption"
        Else
            Chart1.ChartAreas("Default").AxisY.Title = "Meter readings "
        End If

        Chart1.ChartAreas("Default").AxisY.TitleColor = Color.Blue
        Chart1.ChartAreas("Default").AxisX.Title = "Period  Last 30 (readings)  " & Mid$(Now, 1, 10) & " - " & Mid$(DateAdd(DateInterval.Day, -30, Now), 1, 10)
        Chart1.ChartAreas("Default").AxisX.TitleColor = Color.Blue

        Me.Chart1.ChartAreas("Default").CursorX.UserEnabled = True

        Me.Chart1.ChartAreas("Default").AxisX.View.MinSize = 15
        If Not Me.IsPostBack Then
            Me.Chart1.ChartAreas("Default").AxisX.View.Position = 20.0
            Me.Chart1.ChartAreas("Default").AxisX.View.Size = 35.0
        End If

        Chart1.ChartAreas("Default").AxisX.ScrollBar.Enabled = True
        Chart1.AJAXZoomEnabled = True

        Chart1.Legends.Clear()
        '  Chart1.DataBindTable(myReader, "RecordedDate")
        ' Chart1.Series("Default").XValueIndexed = True

        Chart1.Series("Default").Color = Color.Black
        Chart1.Series("Default").XValueType = ChartValueTypes.DateTime

        Chart1.Series("Default").ToolTip = "#VALY" + ControlChars.Lf + "#VALX"

        Chart1.Series("Default").Type = SeriesChartType.Line
        '   Chart1.Series("Default").Type = SeriesChartType.Column
        '   Chart1.ChartAreas("Default").AxisY.Interval = 50
        '   Chart1.ChartAreas("Default").Position.Height = 20
        '    Chart1.ChartAreas("Default").BackColor = Color.Transparent
        Chart1.ChartAreas("Default").AxisX.IntervalType = DateTimeIntervalType.Days
        Chart1.ChartAreas("Default").AxisX.Interval = 1
        '  Chart1.ChartAreas("Default").AxisX.LabelStyle.Format = "ddd"
        '  Chart1.ChartAreas("Default").AxisX.LabelStyle.Format = "d-MM"
        Chart1.ChartAreas("Default").AxisX.LabelStyle.FontAngle = 65
        Chart1.ChartAreas("Default").AxisY.LabelStyle.Font = New Font("Arial", 7)
        Chart1.ChartAreas("Default").AxisX.LabelStyle.Font = New Font("Arial", 7)

        Chart1.Series("Default").MarkerStyle = MarkerStyle.Circle
        Chart1.Series("Default").MarkerSize = 8
        Chart1.Series("Default").MarkerColor = Color.Red
        Chart1.Series("Default").BorderWidth = 1
        Chart1.Series("Default").BorderColor = Color.DarkGreen
        ' Chart1.Series("Default").Points.DataBindXY(myReader, "One", myReader, "Two")



        If myReader("TypeOf") = "C" Then
            Chart1.Series("Default").Points.DataBindXY(imyReader, "Entry", imyReader, "Consumption")
        Else
            Chart1.Series("Default").Points.DataBindXY(imyReader, "Entry", imyReader, "Readings")
        End If

        '    If Session("WhichOrder") = "Assending" Or Session("WhichOrder") = "" Then
        '            Chart1.Series("Default").Sort(PointsSortOrder.Ascending)
        '  Chart1.DataManipulator.Sort(PointsSortOrder.Ascending, "AxisLabel", "Default")
        '    Chart1.Series("Default").Sort(PointsSortOrder.Ascending, "AxisLabel")

        '  Else
        '           Chart1.Series("Default").Sort(PointsSortOrder.Descending)
        ' Chart1.DataManipulator.Sort(PointsSortOrder.Descending, "AxisLabel", "Default")
        ' Chart1.Series("Default").Sort(PointsSortOrder.Descending, "AxisLabel")

        '  End If

        Chart1.AlignDataPointsByAxisLabel()


        myReader.Close()
        myConnection.Close()

        ProcessChart = "Ok"
    End Function


    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        If Button1.Text = "Show Chart" Then
            Button1.Text = "Hide Chart"
            Call ProcessChart()
            Me.PanelGridView2.Visible = False
            Me.PanelGridView2Graph.Visible = True
            Me.ButtonLoad.Visible = False
        Else
            Button1.Text = "Show Chart"
            Me.PanelGridView2Graph.Visible = False
            Me.PanelGridView2.Visible = True
            Me.ButtonLoad.Visible = True
        End If

    End Sub
End Class
