﻿<%@ Page Language="VB" MasterPageFile="~/landing.master" AutoEventWireup="false" CodeFile="ShowActualMeterReadings.aspx.vb" Inherits="myapps_CustomerMeters_ShowActualMeterReadings" title="Untitled Page" %>
<%@ Register Assembly="DundasWebChart" Namespace="Dundas.Charting.WebControl" TagPrefix="DCWC" %>

<asp:Content ID="Content1" ContentPlaceHolderID="content_right" Runat="Server">
    <style type="text/css" media="screen">
 .hiddencol  { display:none;  }
.viscol { display:block; }
.general {position:absolute;top:82px;left:233px;height:480px;width:670px;background: transparent url('/assets/content-back-general.jpg') no-repeat; padding-top:15px; padding-left:10px;}
.ImageOne {position:absolute;top:12px;left:13px;height:52px;width:101px;    }
.LabTitle {position:absolute;top:16px; left:120px; font-Size:14px;color:Black;font-weight:bold;font-family:Arial;         height: 17px;         width: 344px;     }
.LabCompany {position:absolute;top:38px; left:120px;font-Size:12px;color:blue;font-weight:bold;font-family:Arial;         height: 14px;         width: 381px;     }
.LabSite {position:absolute;top:57px; left:121px;font-Size:10px;color:green;font-weight:bold;font-family:Arial;         height: 12px;         width: 360px;     }
.ImageButtonReturnToMenu {position:absolute;left:624px; top:6px;height:32px;width:32px;    }
.generalTitle {font-family:Arial;font-size:10px;}
.generalInput {font-family:Arial;font-size:10px;}
.generalButton {font-family:Arial;font-size:10px;background-color:Gray;color:White;border:solid 1px lightgray;cursor:hand;}
.navgeneral {position:absolute;top:83px;left:0px;width:220px;height:500px;background: white url('/assets/left-nav-account.png') no-repeat; padding-top:0px; padding-left:0px;}

.RadioButtonList1{ position:absolute; top: 206px;  left: 12px;  height: 49px;   width: 150px;     }
.RadioOutStanding {position:absolute;   top: 85px; left: 13px; width: 166px; }
.RadioButtonList2 { position:absolute;  top: 145px;left: 13px  }

.TitleOne {position:absolute;top: 68px; left: 13px; font-family:Arial;font-size:12px;Color:Navy; } 
.TitleTwo {position:absolute; top: 128px; left: 13px; font-family:Arial;font-size:12px;Color:Navy; }
.TitleThree {position:absolute; top: 187px; left: 13px; font-family:Arial;font-size:12px;Color:Navy;  }

.ButtonLoad {position:absolute; top: 467px; left: 16px; font-family:Arial;font-size:13px;Color:Navy;background-color:white;cursor:hand;border:solid 1px green;  }
.Button1 {position:absolute; top: 467px; left: 119px; font-family:Arial;font-size:13px;Color:Navy;background-color:white;cursor:hand;border:solid 1px green;  }

.SelectedMeterName  {position:absolute;top: 75px; left: 14px; font-family:Arial;font-size:11px;Color:Navy; } 
.NextReadingDate  {position:absolute;top: 98px; left: 13px; font-family:Arial;font-size:11px;Color:Navy; } 
.errorlabeldateexceeded  {position:absolute;top: 445px; left: 14px; font-family:Arial;font-size:11px;Color:Navy;        width: 303px;    } 
.NextReadDate  {position:absolute;top: 447px; left: 328px; font-family:Arial;font-size:11px;Color:Navy;        height: 14px;        width: 86px;
    } 

.PanelGridView2  {position:absolute;top: 119px; left: 13px;width:639px;height :316px; } 
.ButtonUpdate {position:absolute; top: 468px; left: 17px; font-family:Arial;font-size:13px;Color:Navy;background-color:white;cursor:hand;border:solid 1px green;  }
.labelerror {position:absolute;top: 472px; left: 149px;width:414px;background-color:Maroon;font-family:Arial;font-Size:10px;color:Yellow;     }
.BeigeBlack {background-color:White;color:Black;border:solid 1px lightgray;}
.MaroonWhite {background-color: Maroon;color:white;border:solid 1px lightgray;}

     #form1
     {
         height: 437px;
     }
 
 </style>
<form id="form1" Runat="Server" >
<!-- Left Panel --->
<div class="navgeneral" >
        <asp:Label ID="TitleOne"  BorderStyle="None" CssClass="TitleOne" runat="server" Text="Filter output of required meters"></asp:Label>
        <asp:Label ID="TitleTwo"  BorderStyle="None" CssClass="TitleTwo" runat="server" Text="Direction of output"></asp:Label>
        <asp:Label ID="TitleThree"  BorderStyle="None" CssClass="TitleThree" runat="server" Text="Period contents of meters"></asp:Label>
        <asp:Button ID="ButtonLoad" runat="server" BorderStyle="None" CssClass="ButtonLoad"  Text="Load details"></asp:Button>
        <asp:Button ID="Button1" runat="server" Text="Show Chart" visible="false" cssclass="Button1" BorderStyle="None"/>
                
        <asp:RadioButtonList ID="RadioButtonList1"    runat="server"     CssClass="RadioButtonList1" AutoPostBack="False"   Font-Names="Arial"  Font-Size="10px"   RepeatDirection="Vertical"   RepeatLayout="Table" BorderStyle="None">
        <asp:ListItem Text=" All " Value="All" />    
        <asp:ListItem Text=" 12 months " Value="12 months" />     
        <asp:ListItem Text=" 6 months " Value="6 months" />       
        <asp:ListItem Selected="True" Text=" 30 days " Value="30 days" />
        </asp:RadioButtonList>

        <asp:RadioButtonList ID="RadioOutStanding" runat="server"    CssClass="RadioOutStanding" BorderStyle="None" AutoPostBack="False"  Font-Names="Arial" Font-Size="10px" RepeatDirection="Horizontal" RepeatLayout="Table" TextAlign="Left"  Height="18px" >
        <asp:ListItem Text=" Required read " Value="ReadRequired" />   
        <asp:ListItem Selected="True" Text=" All reads " Value="AllMeters" />
        </asp:RadioButtonList>

        <asp:RadioButtonList ID="RadioButtonList2"  runat="server"  CssClass="RadioButtonList2" AutoPostBack="False" BorderStyle="None" Font-Names="Arial" Font-Size="10px" RepeatDirection="Horizontal" RepeatLayout="Table" TextAlign="Left" Height="11px" Width="171px"  >
        <asp:ListItem Text="Assending " Value="Assending" />     
        <asp:ListItem Selected="True" Text="Descending " Value="Descending" />
        </asp:RadioButtonList>
    
</div>

<!-- Right Panel --->
<div class="general" >
    <asp:Image ID="ImageOne"  CssClass="ImageOne"   runat="server"   ImageUrl="~/assets/metering/buttons-myapps-dataentry-meterreadings.gif"/>
    <asp:Label ID="LabTitle"  CssClass="LabTitle"  runat="server"   Text="Meter Readings"  BorderStyle="None"></asp:Label>
    <asp:Label ID="LabCompany"   CssClass="LabCompany"  runat="server"  Text=""   BorderStyle="None"></asp:Label>
    <asp:Label ID="LabSite" CssClass="LabSite"  runat="server"  Text=""   BorderStyle="None"></asp:Label>
    <asp:ImageButton  ID="ImageButtonReturnToMenu"    CssClass="ImageButtonReturnToMenu"         visible="true"    tooltip="Return to Meters menu" runat="server"   ImageAlign="Left"   ImageUrl="~/assets/metering/remove_48x48.png"></asp:ImageButton>
    <asp:Label ID="NextReadDate" runat="server" CssClass="NextReadDate"   Text="01/01/1900" BorderStyle="None" Visible="False" ></asp:Label>
    <asp:Label ID="NextReadingDate"   runat="server"  CssClass="NextReadingDate"  BorderStyle="None" Text="Next Reading will be on: " visible="true"> </asp:Label>
    <asp:Label ID="SelectedMeterName"        runat="server"  CssClass="SelectedMeterName"  BorderStyle="None" Text="MeterName"></asp:Label>
   <asp:Label ID="errorlabeldateexceeded"    runat="server"  CssClass="errorlabeldateexceeded"  BorderStyle="None" text="Date exceeded" visible="false"> </asp:Label>
        
    
        
   <asp:Button ID="ButtonUpdate" runat="server" BorderStyle="None" visible="false" CssClass="ButtonUpdate"  Text="Global update"></asp:Button>
   <asp:Label ID="labelerror" visible="false" runat="server"  BorderStyle="None"  CssClass="labelerror" Text="Errors have been found - please review your enteries"></asp:Label>      

<asp:Panel ID="PanelGridView2" runat="server"  BorderStyle="None" CssClass="PanelGridView2"  ScrollBars="Auto">
  <asp:GridView ID="GridView2"          runat="server"         BorderStyle="None"         GridLines="None"         AutoGenerateColumns="False"         DataSourceID="SQL_MeterReadings"         HorizontalAlign="Left"         Font-Names="Arial"         Font-Size="10px" DataKeyNames="ID">
       <%--
        <HeaderStyle Font-Names="Arial" Font-Size="12px" HorizontalAlign="Left" VerticalAlign="Top" />
        <SelectedRowStyle BorderStyle="None" HorizontalAlign="Left"   VerticalAlign="Top" />
--%>
        <Columns>
<%--          <asp:CommandField ShowSelectButton="True"   SelectText="Edit" >
               <ItemStyle  Width="15px" Height="10px"/>
         </asp:CommandField>--%>
        
         <asp:BoundField DataField="id" HeaderText="Reading"    InsertVisible="False" ReadOnly="True" >
        <ItemStyle BorderStyle="None" HorizontalAlign="Left" VerticalAlign="Top" CssClass="hiddencol" Width="0px" />
        </asp:BoundField> 

        <asp:TemplateField HeaderText="Comment">
                 <ItemTemplate>
                    <asp:TextBox ID="Reading" runat="server" cssclass="BeigeBlack" Text='<%# Bind("Figure") %>'></asp:TextBox>
                 </ItemTemplate>
             <controlStyle   Width="70px"/>
             <HeaderStyle   Width="70px"      HorizontalAlign="Left"  />           
             <ItemStyle      Width="70px"      HorizontalAlign="Left"  />
        </asp:TemplateField>
         
         
        <asp:TemplateField HeaderText="RecordedDate">
                 <ItemTemplate>
                    <asp:TextBox ID="Comment" runat="server"  cssclass="BeigeBlack" Text='<%# Bind("TagNote") %>'></asp:TextBox>
                 </ItemTemplate>
             <controlStyle   Width="200px"/>
             <HeaderStyle   Width="200px"      HorizontalAlign="Left"  />           
             <ItemStyle        Width="200px"      HorizontalAlign="Left"  />
        </asp:TemplateField>
         
        <asp:BoundField DataField="RecordedDate" HeaderText="Day"    SortExpression="RecordedDate" DataFormatString="{0:d}">
        <ItemStyle BorderStyle="None"     Font-Names="Arial" Font-Size="12px" HorizontalAlign="Left"  VerticalAlign="Top" Width="200px" />
        </asp:BoundField>

        <asp:BoundField DataField="DayName">
        <ItemStyle BorderStyle="None" Font-Names="Arial" Font-Size="12px" HorizontalAlign="Left"
         VerticalAlign="Top"      Width="150px" />
        </asp:BoundField>

        </Columns>
        </asp:GridView>
        
        
</asp:Panel>


<asp:Panel ID="PanelGridView2Graph"  visible="false"  style="position:absolute;left: 0px; top: 119px;"  Width="600px"  Height="450px" runat="server">  
    <DCWC:Chart ID="Chart1" runat="server" >
        <Legends>
            <DCWC:Legend Name="Default"></DCWC:Legend>
        </Legends>
        <Series>
            <DCWC:Series Name="Default">
            </DCWC:Series>
        </Series>
        <ChartAreas>
            <DCWC:ChartArea Name="Default">
            </DCWC:ChartArea>
        </ChartAreas>
    </DCWC:Chart>
 </asp:Panel>


</div>
<!--- HIddenFields -->
<asp:HiddenField ID="HiddenFieldScheduleType" runat="server" />
<asp:HiddenField ID="HiddenFieldStartDate" runat="server" />
<asp:HiddenField ID="HiddenFieldFinishDate" runat="server" />
<asp:HiddenField ID="HiddenFieldNextReadingDate" runat="server" />

<!--- DataBases --->
<asp:SqlDataSource ID="SQL_MeterReadings" runat="server" ConnectionString="<%$ ConnectionStrings:portal.utilitymasters.co.ukConnectionString %>" 
SelectCommand="EXEC spCustomer_MeterReadings '1980',0">
</asp:SqlDataSource>

<asp:SqlDataSource ID="SQL_MeterGeneral" runat="server" ConnectionString="<%$ ConnectionStrings:portal.utilitymasters.co.ukConnectionString %>" 
SelectCommand="Select 1-1">
</asp:SqlDataSource>      


</form>

</asp:Content>

