﻿Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Drawing
Imports System.Web
'Imports Dundas.Charting.WebControl
'Imports Dundas.Charting.WebControl.Utilities
Imports System.Web.Security
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.SessionState
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports System.Data
Imports System.Data.OleDb
Partial Class myapps_CustomerMeters_CustomerOutstandingReads
    Inherits System.Web.UI.Page
    Public Shared StayOnPage As Boolean
    Public Shared tHOLD As New ArrayList
    Dim TheCustomerID As String = ""
    Dim TheCustomerName As String = ""
    Dim TheSiteID As String = ""
    Dim TheSiteName As String = ""

    Dim iCustomer As String = ""
    Dim iMethod As String = ""
    Dim iMethodType As String = ""

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Call LoadTreeview_Report()

        If Request.QueryString("method") = "Customer" Then
            iCustomer = Trim(Replace(Request.QueryString("custid"), "'", ""))
            iMethod = Trim(Replace(Request.QueryString("method"), "'", ""))
            iMethodType = "Customer"
            Session("TheCompanyID") = Request.QueryString("custid")
        Else
            iCustomer = Trim(Replace(Request.QueryString("SiteId"), "'", ""))
            iMethod = Trim(Replace(Request.QueryString("method"), "'", ""))
            iMethodType = "Site"
        End If

        Me.SqlDataSourceMETERS.SelectCommand = "spCustomer_Outstanding_Meternames"
        Me.SqlDataSourceMETERS.SelectParameters.Item("intCustID").DefaultValue = Request.QueryString("custid")

        Me.SqlDataSourceSITES.SelectCommand = "spCustomer_Outstanding_CustomerSites"
        Me.SqlDataSourceSITES.SelectParameters.Item("intCustID").DefaultValue = Request.QueryString("custid")

        Me.AvailableMeter.AppendDataBoundItems = True
        Me.AvailableSite.AppendDataBoundItems = True
        Me.AvailableSite.Items.Add("...")
        Me.AvailableMeter.Items.Add("...")

        Me.Panel1.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFFFC0")
        Me.Panel1.Height = 350
        Me.Panel1.Width = 650
        Me.Panel1.Visible = False

        Me.iNext.Visible = False
        Me.iPrevious.Visible = False
        Me.ClearFields.Visible = False
        Me.UpdateFields.Visible = False

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack = True Then
        Else
            Me.myPage.Text = ""
            Me.HiddenField1.Value = "Site"
        End If

        If Trim(Request.QueryString("custid")) <> "" Then
            Dim rs As New ADODBConnection
            Dim strSQL As String = ""
            If Request.QueryString("Method") = "Customer" Then
                strSQL = ""
                strSQL = " SELECT custid,customername FROM  [UML_CMS].dbo.tblcustomer where custid=" & Trim(Request.QueryString("Custid"))
            Else
                strSQL = ""
                strSQL = strSQL & " SELECT [UML_CMS].dbo.tblsites.siteid,[UML_CMS].dbo.tblsites.sitename,[UML_CMS].dbo.tblcustomer.customername FROM  [UML_CMS].dbo.tblsites  "
                strSQL = strSQL & " INNER JOIN [UML_CMS].dbo.tblcustomer ON [UML_CMS].dbo.tblsites .custid=[UML_CMS].dbo.tblcustomer.custid where siteid=" & Trim(Request.QueryString("Custid"))
            End If

            rs.OpenConnection()
            rs.OpenRecordSet(strSQL)

            Me.LabCompany.Text = rs.rComm.Fields("CustomerName").Value

            If Request.QueryString("Method") = "Customer" Then
                Me.Panelmain.Visible = True
                Me.LabSite.Text = ""

                If Me.AvailableMeter.Items.Count = 0 And Me.AvailableSite.Items.Count = 0 Then
                    Me.Panelmain.Visible = False
                    Me.LabSite.Text = "There are NO Outstanding Readings"
                Else
                    Me.Panelmain.Visible = True
                    Me.LabSite.Text = ""
                End If
            Else
                Me.Panelmain.Visible = False
                Me.LabSite.Text = "Please select a valid Company " 'rs.rComm.Fields("SiteName").Value
            End If
            rs.CloseConnection()

            If Request.QueryString("Method") = "Customer" Then
            Else
            End If
        Else
        End If
    End Sub

    Private Sub LoadTreeview_Report()
        Me.navOnlineReports.InnerHtml = ""
        Dim strSQL As String = "EXEC spPortal_TREEVIEW " & MySession.UserID
        Dim myConnection As New OleDb.OleDbConnection(Core.CurrentConnectionString)

        Dim myCommand As New OleDb.OleDbCommand(strSQL, myConnection)
        myCommand.Connection.Open()
        myCommand.CommandTimeout = 180
        Dim Reader As OleDb.OleDbDataReader = myCommand.ExecuteReader(CommandBehavior.CloseConnection)

        Me.navOnlineReports.InnerHtml += "<br/>"
        Me.navOnlineReports.InnerHtml += "<br/>"
        Me.navOnlineReports.InnerHtml += "<br/>"

        While Reader.Read

            Select Case CStr(Reader(3))
                Case CStr("Company")
                    Me.navOnlineReports.InnerHtml += "<li class='styleformytreeview_company'><a href='CustomerOutStandingReads.aspx?custid=" & Reader(0).ToString & "&method=Customer'>" & Reader(1).ToString & "</a></li>"
                Case CStr("Header")
                    '   Me.navOnlineReports.InnerHtml += "<li class='styleformytreeview_header'>" & Reader(1).ToString & "</li>"
                Case CStr("Site")
                    '  Me.navOnlineReports.InnerHtml += "<li class='styleformytreeview_site'><a href='CustomerOutStandingReads.aspx?custid=" & Reader(0).ToString & "&method=Site'>" & Left(Reader(1).ToString, 23) & "..</a></li>"
                Case CStr("Group")
                    ' Me.navOnlineReports.InnerHtml += "<li class='styleformytreeview_group'><a href='CustomerOutStandingReads.aspx?custid=" & Reader(0).ToString & "&method=Group'>" & Reader(1).ToString & "</a></li>"
            End Select

        End While

        Me.navOnlineReports.InnerHtml += "<li class='styleformytreeview_company' style='height:10px;'></li>"
        Me.navOnlineReports.InnerHtml += "<li class='styleformytreeview_company'><a href='Default.aspx'><< Back</a></li>"

    End Sub


    Protected Sub ImageButtonReturnToMenu_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonReturnToMenu.Click
        Response.Redirect("~/myapps/customermeters/Default.aspx")
    End Sub

    '==========================================================================
    '
    '==========================================================================
    Protected Sub GridView2_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView2.RowDataBound
        If e.Row.RowIndex = -1 Then
        Else
            tHOLD.Add(e.Row.Cells(1).Text)
        End If
    End Sub

    Protected Sub GridView2_PageIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridView2.PageIndexChanged
        Me.myPage.Text = GridView2.PageIndex + 1 & " of " & GridView2.PageCount

    End Sub

    Protected Sub GridView2_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView2.RowCommand
        ' only clear if user accepts a new page
        If StayOnPage = True Then
        Else
            tHOLD.Clear()
        End If
    End Sub


    Protected Function iValidate() As Boolean
        Dim GotValid As Boolean = False
        Dim i As Integer = 0
        For Each gvRow As GridViewRow In GridView2.Rows
            Dim TheID As Integer = Convert.ToInt32(GridView2.DataKeys(gvRow.RowIndex).Value)
            Dim iRead As TextBox = gvRow.FindControl("Reading")
            Dim m As String = tHOLD(i)

            If IsNumeric(iRead.Text) = False Then ' Alph- only
                If Trim(iRead.Text) = "" Then ' agian still validate user might not have to current value
                    iRead.BackColor = Drawing.Color.Beige
                    iRead.ForeColor = Drawing.Color.Black
                Else
                    '  iRead.Text = ""
                    iRead.BackColor = Drawing.Color.Maroon
                    iRead.ForeColor = Drawing.Color.White
                    GotValid = True
                End If
            Else
                iRead.BackColor = Drawing.Color.White
                iRead.ForeColor = Drawing.Color.Navy
            End If
            i = i + 1
        Next

        iValidate = GotValid

    End Function


    Protected Function iChanges() As Boolean
        Dim GotChange As Boolean = False
        Dim i As Integer = 0
        For Each gvRow As GridViewRow In GridView2.Rows
            Dim TheID As Integer = Convert.ToInt32(GridView2.DataKeys(gvRow.RowIndex).Value)
            Dim iRead As TextBox = gvRow.FindControl("Reading")
            Dim m As String = tHOLD(i)

            If Trim(iRead.Text) = tHOLD(i) Then
            Else
                GotChange = True
            End If

            i = i + 1
        Next

        iChanges = GotChange

    End Function

    Protected Sub iNext_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles iNext.Click
        Dim strSQL As String = ""
        labelerror.Visible = False
        Dim nReturn As Integer = 6
        StayOnPage = False
        If iChanges() = True Then
            If iValidate() = True Then
                labelerror.Visible = True
                StayOnPage = True
                Exit Sub
            Else
            End If
            '======================
            ' physically update the  
            Dim rs As New ADODBConnection
            rs.OpenConnection()

            Dim i As Integer = 0
            For Each gvRow As GridViewRow In GridView2.Rows
                Dim TheID As Integer = Convert.ToInt32(GridView2.DataKeys(gvRow.RowIndex).Value)
                Dim iRead As TextBox = gvRow.FindControl("Reading")
                Dim m As String = tHOLD(i)

                If Trim(iRead.Text) = Trim(tHOLD(i)) Then
                Else
                    strSQL = ""
                    strSQL = strSQL & "UPDATE [portal.utilitymasters.co.uk].dbo.tblCustomer_ScheduledMeters "
                    strSQL = strSQL & " SET Figure= " & "'" & Trim(iRead.Text) & "'"
                    strSQL = strSQL & "  WHERE ID=" & TheID
                    rs.OpenRecordSet(strSQL)
                End If
            Next

            rs.CloseConnection()
            Response.Redirect(Page.Request.Url.PathAndQuery)
            '======================
        End If

        GridView2.PageIndex = GridView2.PageIndex + 1
        Me.myPage.Text = "Page " & GridView2.PageIndex & " of " & GridView2.PageCount

        If GridView2.PageIndex > 1 Then
            If GridView2.PageIndex > GridView2.PageCount Then
                GridView2.PageIndex = GridView2.PageCount
            Else
            End If

            Me.iNext.Visible = True
            Me.iPrevious.Visible = True
        Else
            Me.ClearFields.Visible = True
            Me.UpdateFields.Visible = True
        End If

    End Sub

    Protected Sub iPrevious_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles iPrevious.Click
        Dim strSQL As String
        labelerror.Visible = False
        Dim nReturn As Integer = 6
        StayOnPage = False
        If iChanges() = True Then
            If iValidate() = True Then
                labelerror.Visible = True
                StayOnPage = True
                Exit Sub
            Else
            End If
            '=============================
            ' physically update the  
            Dim rs As New ADODBConnection
            rs.OpenConnection()

            Dim i As Integer = 0
            For Each gvRow As GridViewRow In GridView2.Rows
                Dim TheID As Integer = Convert.ToInt32(GridView2.DataKeys(gvRow.RowIndex).Value)
                Dim iRead As TextBox = gvRow.FindControl("Reading")
                Dim m As String = tHOLD(i)

                If Trim(iRead.Text) = Trim(tHOLD(i)) Then
                Else
                    strSQL = ""
                    strSQL = strSQL & "UPDATE [portal.utilitymasters.co.uk].dbo.tblCustomer_ScheduledMeters "
                    strSQL = strSQL & " SET Figure= " & "'" & Trim(iRead.Text) & "'"
                    strSQL = strSQL & "  WHERE ID=" & TheID
                    rs.OpenRecordSet(strSQL)
                End If
            Next

            rs.CloseConnection()
            Response.Redirect(Page.Request.Url.PathAndQuery)
            '=============================
        Else
        End If

        If GridView2.PageIndex - 1 = 0 Then
            GridView2.PageIndex = 1
        Else
            GridView2.PageIndex = GridView2.PageIndex - 1
            Me.myPage.Text = "Page " & GridView2.PageIndex & " of " & GridView2.PageCount
        End If

        If GridView2.PageIndex > 1 Then
            Me.iNext.Visible = True
            Me.iPrevious.Visible = True
        Else
            If GridView2.PageIndex = 1 Then
                Me.iNext.Visible = True
                Me.iPrevious.Visible = False
                Me.ClearFields.Visible = True
                Me.UpdateFields.Visible = True
            Else
            End If
        End If


    End Sub

    Protected Sub UpdateFields_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles UpdateFields.Click
        Dim strSQL As String = ""
        labelerror.Visible = False
        Dim nReturn As Integer = 6
        StayOnPage = False
        If iChanges() = True Then
            If iValidate() = True Then
                labelerror.Visible = True
                StayOnPage = True
                Exit Sub
            Else
            End If

            ' physically update the  
            Dim rs As New ADODBConnection
            rs.OpenConnection()

            Dim i As Integer = 0
            For Each gvRow As GridViewRow In GridView2.Rows
                Dim TheID As Integer = Convert.ToInt32(GridView2.DataKeys(gvRow.RowIndex).Value)
                Dim iRead As TextBox = gvRow.FindControl("Reading")
                Dim m As String = tHOLD(i)

                If Trim(iRead.Text) = Trim(tHOLD(i)) Then
                Else
                    strSQL = ""
                    strSQL = strSQL & "UPDATE [portal.utilitymasters.co.uk].dbo.tblCustomer_ScheduledMeters "
                    strSQL = strSQL & " SET Figure= " & "'" & Trim(iRead.Text) & "'"
                    strSQL = strSQL & "  WHERE ID=" & TheID
                    rs.OpenRecordSet(strSQL)
                End If
            Next

            rs.CloseConnection()
            Response.Redirect(Page.Request.Url.PathAndQuery)
        Else
        End If

    End Sub

    Protected Sub ClearFields_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ClearFields.Click
        labelerror.Visible = False
        Dim i As Integer = 0
        For Each gvRow As GridViewRow In GridView2.Rows
            Dim TheID As Integer = Convert.ToInt32(GridView2.DataKeys(gvRow.RowIndex).Value)
            Dim iRead As TextBox = gvRow.FindControl("Reading")
            Dim m As String = tHOLD(i)

            If iRead.BackColor = Drawing.Color.Maroon Then
                iRead.Text = ""
                iRead.BackColor = Drawing.Color.Beige
                iRead.ForeColor = Drawing.Color.Black
            Else
            End If
        Next
    End Sub

    Protected Sub AvailableMeter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles AvailableMeter.SelectedIndexChanged
        If AvailableMeter.SelectedItem.Text = "..." Then
            Me.Panel1.Visible = False
            Exit Sub
        Else
            Me.Panel1.Visible = True
        End If

        Me.HiddenField1.Value = "Meter"
        Me.SqlDataSource5.SelectCommand = "spCustomer_Outstanding_Meters_BYMeter"
        Me.SqlDataSource5.SelectCommandType = SqlDataSourceCommandType.StoredProcedure
        Me.SqlDataSource5.SelectParameters.Item("intCustID").DefaultValue = Request.QueryString("custid")
        Me.SqlDataSource5.SelectParameters.Item("Order").DefaultValue = Me.OrderSequency.SelectedValue

        Me.SqlDataSource5.SelectParameters.Item("MeterID").DefaultValue = Me.AvailableMeter.SelectedValue
        Me.GridView2.DataSourceID = Nothing
        Me.GridView2.DataBind()

        Me.GridView2.PageSize = PageLister.SelectedItem.Text

        Me.GridView2.DataSourceID = "SqlDataSource5"
        Me.GridView2.DataBind()
        Me.GridView2.PageIndex = 1

        If GridView2.PageCount = 0 Then
            Me.myPage.Text = "Page 0 " & " of  0"
        Else
            Me.myPage.Text = "Page 1 " & " of " & GridView2.PageCount
        End If

        If GridView2.PageCount < 2 Then
            Me.iNext.Visible = False
            Me.iPrevious.Visible = False
            Me.ClearFields.Visible = False
            Me.UpdateFields.Visible = False
        Else
            Me.iNext.Visible = True
            Me.iPrevious.Visible = False
            Me.ClearFields.Visible = True
            Me.UpdateFields.Visible = True
        End If

        If Me.GridView2.PageIndex = 1 Then
            Me.ClearFields.Visible = True
            Me.UpdateFields.Visible = True
        End If

        labelerror.Visible = False

    End Sub

    Protected Sub AvailableSite_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles AvailableSite.SelectedIndexChanged
        If AvailableSite.SelectedItem.Text = "..." Then
            Me.Panel1.Visible = False
            Exit Sub
        Else
            Me.Panel1.Visible = True
        End If


        Me.HiddenField1.Value = "Site"
        Me.SqlDataSource4.SelectCommand = "spCustomer_Outstanding_Meters_BYSite"
        Me.SqlDataSource4.SelectCommandType = SqlDataSourceCommandType.StoredProcedure
        Me.SqlDataSource4.SelectParameters.Item("intCustID").DefaultValue = Request.QueryString("custid")
        Me.SqlDataSource4.SelectParameters.Item("SiteID").DefaultValue = Me.AvailableSite.SelectedValue
        Me.SqlDataSource4.SelectParameters.Item("Order").DefaultValue = Me.OrderSequency.SelectedValue

        Me.GridView2.DataSourceID = Nothing
        Me.GridView2.DataBind()


        Me.GridView2.PageSize = PageLister.SelectedItem.Text

        Me.GridView2.DataSourceID = "SqlDataSource4"
        Me.GridView2.DataBind()
        'Me.GridView2.SelectedIndex = 0
        Me.GridView2.PageIndex = 1

        If GridView2.PageCount < 2 Then
            Me.iNext.Visible = False
            Me.iPrevious.Visible = False
        Else
            Me.iNext.Visible = True
            Me.iPrevious.Visible = False
        End If

        If GridView2.PageCount = 0 Then
            Me.myPage.Text = "Page 0 " & " of  0"
            Me.ClearFields.Visible = False
            Me.UpdateFields.Visible = False
        Else
            Me.myPage.Text = "Page 1 " & " of " & GridView2.PageCount
            Me.ClearFields.Visible = True
            Me.UpdateFields.Visible = True
        End If

        If Me.GridView2.PageIndex = 1 Then
            Me.ClearFields.Visible = True
            Me.UpdateFields.Visible = True
        End If

        labelerror.Visible = False

    End Sub

    Protected Sub PageLister_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles PageLister.SelectedIndexChanged
        labelerror.Visible = False
    End Sub

End Class
