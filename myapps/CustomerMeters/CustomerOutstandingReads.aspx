﻿<%@ Page Language="VB" MasterPageFile="~/landing.master" AutoEventWireup="false" CodeFile="CustomerOutstandingReads.aspx.vb" Inherits="myapps_CustomerMeters_CustomerOutstandingReads" title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="content_right" Runat="Server">

<STYLE> 
.general 
{ background: transparent url('/assets/content-back-general.jpg') no-repeat; padding-top:15px; padding-left:10px;}
          
.CompanyBox
{
background: transparent url('/assets/content-back-general.jpg') no-repeat; padding-top:15px; padding-left:10px;height:500px;
overflow-x: hidden;
overflow-y: scroll;
scrollbar-arrow-color: #4e658a;
scrollbar-3dlight-color: #777799;
scrollbar-darkshadow-color: #666677;
scrollbar-face-color: #a1b1c3;
scrollbar-highlight-color: #e9e9e9;
scrollbar-shadow-color: #a1b1c3;
scrollbar-track-color: #ffffff;          
}              
          
          
/*MeterOutStanding */
.DetailsView {font: font-family:Arial; font-size:9px; background-color: yellow; color: Navy; border:none;}
.ListBox {font-family:Arial; font-size:9px; background-color: white; color: Navy;} 
/*
 scrollbar-arrow-color: #4e658a;
scrollbar-3dlight-color: #777799;
scrollbar-darkshadow-color: #666677;
scrollbar-face-color: #a1b1c3;
scrollbar-highlight-color: #e9e9e9;
scrollbar-shadow-color: #a1b1c3;
scrollbar-track-color: #ffffff;
*/    
.inputbox { font-family:Arial; font-size:9px; background-color: white; color: Navy; border: solid 1px lightgrey;}
.mygrid { font-family:Arial; font-size:9px; background-color: transparent; color: Navy; border:none;}
.mygridinput{ font-family:Arial; font-size:9px; background-color: white; color: Navy; border:solid 1px lightgray;  text-align:left;}
.HiddenColumn{display:none;}
.generalbutton { font-family:Arial; font-size:9px; background-color: white; color: Navy; cursor:hand; border:solid 1px lightgray;}
.pagerstyle {visibility:hidden;}
.mynextprev {font-family:Arial; font-size:9px; background-color: white; color: Navy; cursor:hand; border:solid 1px lightgray;}
.mygriddropdown{font-family:Arial; font-size:9px; background-color: white; color: Navy; cursor:hand; border:solid 1px lightgray;}
.generallabel{ font-family:Arial; font-size:9px; background-color: transparent; color: Navy; border:none;}
.navgeneral {position:absolute;top:83px;left:0px;width:220px;height:500px;background: white url('/assets/left-nav-account.png') no-repeat; padding-top:0px; padding-left:0px;}
.inputtext1 { font-family:Arial; font-size:10px; background-color: white; color: Navy; cursor:hand; border:solid 1px lightgray;}
.BeigeBlack {background-color:White;color:Black;border:solid 1px lightgray;}
.Panel1
{
position:absolute;left: 13px; top: 157px; height: 330px;
overflow:auto;
scrollbar-arrow-color: #4e658a;
scrollbar-3dlight-color: #777799;
scrollbar-darkshadow-color: #666677;
scrollbar-face-color: #a1b1c3;
scrollbar-highlight-color: #e9e9e9;
scrollbar-shadow-color: #a1b1c3;
scrollbar-track-color: #ffffff;
}



/* end  */

</STYLE> 

 
<form id="form1" Runat="Server">

<div  class="navgeneral">
  <ul class="styleformytreeview" runat="server" id="navOnlineReports">  </ul> 
</div>

<div  style="position:absolute;top:83px;left:230px;width:700px;height:550px" class="general">
<asp:ImageButton ID="ImageButtonCreateTheMeters" 
        style="position:absolute;left:11px; top: 5px; height:52px; width: 101px;" 
        ImageUrl="~/assets/metering/buttons-myapps-dataentry-meterreader.gif"   
        runat="server" ImageAlign="Left"/>

 <asp:Label ID="LabTitle" style="position:absolute;top:2px; left:113px;"  
        Font-Names="Arial"   Font-Size="14px" runat="server" ForeColor="Black"  
        Font-Bold="true" Text="Outstanding Meter Readings"></asp:Label> 

 <asp:Label ID="LabCompany" style="position:absolute;top:20px; left:115px;"  
        Font-Names="Arial"   Font-Size="12px" runat="server" ForeColor="DarkBlue"  
        Font-Bold="False" Text="Company name"></asp:Label> 
        
 <asp:Label ID="LabSite"    style="position:absolute;top:39px; left:116px;"  
        Font-Names="Arial"   Font-Size="11px" runat="server" ForeColor="DarkGreen"  
        Font-Bold="False" Text="Please select a valid Company"></asp:Label> 
 
 <asp:ImageButton  ID="ImageButtonReturnToMenu"   visible="true"  
        style="position:absolute;left:628px; top:6px;"  height="32px"  width="32px"   
        tooltip="Return to Meters menu" runat="server"   ImageAlign="Left"  
        ImageUrl="~/assets/metering/remove_48x48.png">
</asp:ImageButton>
 
<%-- P A N E L--%>
<%--==========================--%>
<asp:Panel ID="Panelmain" runat="server"
visible="false"
style="position:absolute;left: 10px; top: 55px; height: 93px;"  
Width="650px" 
BorderStyle="None" 
>
 
<asp:Label ID="Label1" runat="server" Text="Number of elements in list view" Width="150px"  cssclass="generallabel"></asp:Label>
<asp:DropDownList ID="PageLister" AutoPostBack="false" cssclass="mygriddropdown" width="40px" runat="server">
<asp:ListItem value="10" selected="True">10 </asp:ListItem>
<asp:ListItem value="20" >20 </asp:ListItem>
<asp:ListItem value="30" >30 </asp:ListItem>
<asp:ListItem value="40" >40 </asp:ListItem>
<asp:ListItem value="50" >50 </asp:ListItem>
</asp:DropDownList>
<br />
    <asp:Label ID="Label4" runat="server" Text="Order sequencing" cssclass="generallabel" Width="150px" ></asp:Label>
    <asp:DropDownList ID="OrderSequency" AutoPostBack="false" cssclass="mygriddropdown" Width="80px" runat="server">
        <asp:ListItem value="0" selected="True">Ascending </asp:ListItem>
        <asp:ListItem value="1" >Desending </asp:ListItem>
    </asp:DropDownList>
    
    <br />
    
 <asp:LinkButton visible="false" ID="LinkButton2" cssclass="mygrid" runat="server" Font-Names="Arial" Font-Size="10px">Meters for every Site</asp:LinkButton>
<asp:LinkButton  visible="false"  ID="LinkButton1" cssclass="mygrid" runat="server" Font-Names="Arial" Font-Size="10px">Show all meters for this site</asp:LinkButton>
  
<asp:Label ID="Label3" runat="server" Text="Outstanding Meters By Site: " Width="150px"  cssclass="generallabel"></asp:Label>    
<asp:DropDownList ID="AvailableSite"  runat="server" cssclass="mygriddropdown" AutoPostBack="true" DataSourceID="SqlDataSourceSITES" DataTextField="sitename" DataValueField="siteid"></asp:DropDownList>
  <br />      
<asp:Label ID="Label2" runat="server" Text="All Outstanding Meters" Width="150px"  cssclass="generallabel"></asp:Label>    
<asp:DropDownList ID="AvailableMeter" runat="server" cssclass="mygriddropdown" AutoPostBack="true" DataSourceID="SqlDataSourceMETERS" DataTextField="MeterName" DataValueField="meterid"> </asp:DropDownList>
  
</asp:Panel> 
  <br />

<%--==========================--%>
<%--Panel start here--%>
<asp:Panel 
ID="Panel1" CssClass="Panel1"
BorderStyle="None" 
runat="server" ScrollBars="Vertical">
<asp:Label ID="myPage" runat="server"  cssclass="generallabel" Text="Label"></asp:Label>
      <br />
    <asp:Button ID="iNext" cssclass="mynextprev" runat="server" Text="Next" />
    <asp:Button ID="iPrevious" cssclass="mynextprev" runat="server" Text="Previous" />
    
    <asp:Button ID="ClearFields" cssclass="mynextprev" runat="server" Text="Clear all errors" />
    <asp:Button ID="UpdateFields" cssclass="mynextprev" runat="server" Text="Update " />
    <br />
    
    <asp:Label ID="labelerror" visible="false" runat="server" BackColor="Maroon" Font-Names="Arial" Font-Size="10px"
        ForeColor="Yellow" Text="Errors have been found - please review your enteries"
        Width="414px">
     </asp:Label>
     <br />

    <asp:GridView
    ID="GridView2" 
    runat="server" 
    AllowPaging="true" 
    gridlines="None"
    DataKeyNames="scheduledid" 
    AutoGenerateColumns="False" Font-Names="Arial" Font-Size="10px" Height="260px" 
     >
        <Columns>
        
                       <asp:BoundField DataField="scheduledid" HeaderText="scheduledid" InsertVisible="False"  visible="False"  ReadOnly="True" SortExpression="scheduledid" />

                        <asp:TemplateField HeaderText="Figure">
                                       <ItemTemplate>
                                            <asp:TextBox ID="Reading" borderstyle="Solid" cssclass="inputtext1" runat="server" Text='<%# Bind("figure") %>'></asp:TextBox>
                                       </ItemTemplate>
                                 <controlStyle Width="50px"       />
                                 <HeaderStyle   Width="50px"   HorizontalAlign="Left"  />           
                                 <ItemStyle Width="50px"  HorizontalAlign="Left"  />
                        </asp:TemplateField>
                        
                        
                        <asp:BoundField DataField="RecordedDate" HeaderText="Due" SortExpression="RecordedDate" DataFormatString="{0:d}" >
                               <controlStyle Width="50px" cssclass="mygrid"    />
                               <HeaderStyle   Width="50px" cssclass="mygrid"     wrap="False" HorizontalAlign="Left"    VerticalAlign="Top"  />           
                               <ItemStyle Width="50px" cssclass="mygrid"    wrap="False" HorizontalAlign="Left"    VerticalAlign="Top"  /> 
                        </asp:BoundField>
                        
                        <asp:BoundField DataField="MeterName" HeaderText="Meter" SortExpression="MeterName" >
                                <controlStyle Width="150px" cssclass="mygrid"  />
                                <HeaderStyle   Width="150px" cssclass="mygrid"   wrap="False" HorizontalAlign="Left"    VerticalAlign="Top" />           
                                <ItemStyle Width="150px" cssclass="mygrid"  wrap="False" HorizontalAlign="Left"    VerticalAlign="Top"  />
                        </asp:BoundField>
                        
                        <asp:BoundField DataField="sitename" HeaderText="Site" SortExpression="sitename" >
                                <controlStyle Width="150px" cssclass="mygrid"  />
                                <HeaderStyle   Width="150px" cssclass="mygrid" wrap="False" HorizontalAlign="Left"    VerticalAlign="Top"  />           
                                <ItemStyle Width="150px" cssclass="mygrid"  wrap="False" HorizontalAlign="Left"    VerticalAlign="Top" />
                        </asp:BoundField>
            
        </Columns>
        
        <PagerStyle CssClass="pagerstyle" />
    </asp:GridView>

<%--==========================--%>
 <asp:HiddenField ID="HiddenField1" runat="server" />
<br />
  <asp:SqlDataSource ID="SqlDataSource4" 
   runat="server"
   ConnectionString="<%$ ConnectionStrings:portal.utilitymasters.co.ukConnectionString %>" 
   SelectCommand="spCustomer_Outstanding_Meters_BYSite" 
   SelectCommandType="StoredProcedure">
       <SelectParameters>
           <asp:Parameter DefaultValue="0" Name="intCustID" Type="Int32" />
           <asp:Parameter DefaultValue="0" Name="SiteID" Type="Int32" />
           <asp:Parameter DefaultValue="0" Name="Order" Type="Int32" />
       </SelectParameters>
   </asp:SqlDataSource>
 

<asp:SqlDataSource ID="SqlDataSource5" 
runat="server"
ConnectionString="<%$ ConnectionStrings:portal.utilitymasters.co.ukConnectionString %>" 
SelectCommand="spCustomer_Outstanding_Meters_BYMeter" 
SelectCommandType="StoredProcedure">
<SelectParameters>
<asp:Parameter DefaultValue="0" Name="intCustID" Type="Int32" />
<asp:Parameter DefaultValue="0" Name="MeterID" Type="Int32" />
<asp:Parameter DefaultValue="0" Name="Order" Type="Int32" /> 
</SelectParameters>
</asp:SqlDataSource>

<asp:SqlDataSource ID="SqlDataSourceMETERS" 
runat="server"
ConnectionString="<%$ ConnectionStrings:portal.utilitymasters.co.ukConnectionString %>" 
SelectCommand=" spCustomer_Outstanding_Meternames" 
SelectCommandType="StoredProcedure">
<SelectParameters>
<asp:Parameter DefaultValue="0" Name="intCustID" Type="Int32" />
</SelectParameters>
</asp:SqlDataSource>

<asp:SqlDataSource ID="SqlDataSourceSITES" 
runat="server"
ConnectionString="<%$ ConnectionStrings:portal.utilitymasters.co.ukConnectionString %>" 
SelectCommand="spCustomer_Outstanding_CustomerSites" 
SelectCommandType="StoredProcedure">
<SelectParameters>
<asp:Parameter DefaultValue="0" Name="intCustID" Type="Int32" />
</SelectParameters>
</asp:SqlDataSource>


</asp:Panel>

 

</div>
 
</form> 
 

</asp:Content>

