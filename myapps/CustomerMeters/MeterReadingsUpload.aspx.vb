﻿Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Drawing
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.SessionState
Imports System.Web.UI
Imports System.Web.UI.WebControls.TableCellCollection
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports System.Data
Imports System.Data.OleDb
Imports System.IO
Imports System.IO.File
Imports System.Configuration
Imports System.Threading
 
Partial Class myapps_CustomerMeters_MeterReadingsUpload
    Inherits System.Web.UI.Page

    Protected Sub btnImportExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnImportExcel.Click
        Dim bin As Byte() = New Byte(FileUpload1.PostedFile.ContentLength) {}
        Dim n As Int32 = FileUpload1.PostedFile.InputStream.Read(bin, 0, bin.Length)
        Dim s As String = System.Text.Encoding.Unicode.GetString(bin)
        Me.SumbitOne.Visible = False
        Dim fileOK As Boolean = False

        ''If FileUpload1.HasFile Then
        ''    '   Me.btnImportExcel.Visible = True
        ''    Dim fileExtension As String
        ''    fileExtension = System.IO.Path.GetExtension(FileUpload1.FileName).ToLower()
        ''    Dim allowedExtensions As String() = {".xls", ".xlsx"}
        ''    For i As Integer = 0 To allowedExtensions.Length - 1
        ''        If fileExtension = allowedExtensions(i) Then
        ''            fileOK = True
        ''        End If
        ''    Next
        ''    If fileOK Then
        ''        Try
        ''            '  FileUpload1.PostedFile.SaveAs(path & FileUpload1.FileName)
        ''            lblStatus.Text = "File uploaded!"
        ''        Catch ex As Exception
        ''            lblStatus.Text = "File could not be uploaded."
        ''        End Try
        ''    Else
        ''        lblStatus.Text = "Cannot accept files of this type."
        ''    End If
        ''Else
        ''    Dim fileExtension As String
        ''    fileExtension = System.IO.Path.GetExtension(FileUpload1.FileName).ToLower()
        ''    Dim allowedExtensions As String() = {".xls", ".xlsx"}
        ''    For i As Integer = 0 To allowedExtensions.Length - 1
        ''        If fileExtension = allowedExtensions(i) Then
        ''            fileOK = True
        ''        End If
        ''    Next
        ''    If fileOK Then
        ''        Try
        ''            '  FileUpload1.PostedFile.SaveAs(path & FileUpload1.FileName)
        ''            lblStatus.Text = "File uploaded!"
        ''        Catch ex As Exception
        ''            lblStatus.Text = "File could not be uploaded."
        ''        End Try
        ''    Else
        ''        lblStatus.Text = "Cannot accept files of this type."
        ''    End If

        ''End If


        ''If InStr(LCase(FileUpload1.PostedFile.FileName.ToString()), ".xls") > 0 Then
        ''    lblStatus.ForeColor = Color.Green
        ''    lblStatus.Text = ""
        ''Else
        ''    lblStatus.ForeColor = Color.Red
        ''    lblStatus.Text = "Please select a valid Excel file"
        ''    Return
        ''End If

        'If FileUpload1.PostedFile.ContentLength > 0 Then
        '    '            Dim strConn As String = ("Provider=Microsoft.Jet.OLEDB.4.0;" & "Data Source='") + FileUpload1.PostedFile.FileName.ToString() & "';" & "Extended Properties=Excel 8.0;"
        '    '            Dim strConn As String = ("Provider=Microsoft.Jet.OLEDB.4.0;" & "Data Source='") + FileUpload1.PostedFile.FileName.ToString() & "';" & "Extended Properties=Excel 8.0;"

        '    '            Dim strConn As String = ("Provider=Microsoft.Jet.OLEDB.4.0;" & "Data Source='") + FileUpload1.PostedFile.FileName.ToString() & "';" & "Text;HDR=No;FMT=Delimited\;"
        '    'Dim strConn As String = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source='" & FileUpload1.PostedFile.FileName.ToString() & ";Extended Properties=""Text;HDR=Yes;FMT=Delimited\"""

        '    Dim strConn As String = "Provider=Microsoft.Jet.OLEDB.4.0;" & "Data Source=" & FileUpload1.PostedFile.FileName.ToString() & ";Extended Properties='Text;HDR=YES';"

        '    Dim ds As New DataSet()
        '    'Dim da As New OleDbDataAdapter("SELECT * FROM [Sheet1$]", strConn)
        '    Dim da As New OleDbDataAdapter("SELECT * FROM " & FileUpload1.PostedFile.FileName.ToString(), strConn)

        '    da.Fill(ds)
        '    Try
        '        lblStatus.Text = "Problem with Spreadsheet Sheet1"
        '    Catch ex As Exception
        '        lblStatus.Text = "File could not be uploaded."
        '    End Try
        '=================

        '   Try
        Dim fileinfo As New FileInfo(FileUpload1.PostedFile.FileName)
        ' HiddenFieldUserID
        Dim ModifiedFilename As String = fileinfo.Name.Replace(".csv", Me.HiddenFieldUserID.Value & ".csv")
        Dim strCsvFilePath As String = Server.MapPath("Uploads") + "\" + ModifiedFilename
        FileUpload1.SaveAs(strCsvFilePath)

        Dim strFilePath As String = Server.MapPath("Uploads") + "\"
        Dim strSql As String = "SELECT * FROM [" + ModifiedFilename & "]"
        Dim strCSVConnString As String = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & strFilePath & ";" & "Extended Properties='text;HDR=YES;'"
        Dim dtbCSV As New DataTable()
        Dim oleda As New OleDbDataAdapter(strSql, strCSVConnString)

        oleda.Fill(dtbCSV)
        GridView1.DataSource = dtbCSV
        GridView1.DataBind()
        File.Delete(strCsvFilePath)

        '================
        '   For Each row As DataRow In ds.Tables(0).Rows
        '      MsgBox(row("ID").ToString() & row("MeterID"))
        '   Next
        '   MsgBox(GridView1.Columns.Item(0).HeaderText & "  - " & GridView1.Columns.Item(1).HeaderText)
        lblStatus.BackColor = Color.Transparent
        lblStatus.ForeColor = Color.Green
        lblStatus.Text = "File Uploaded Successfully"

        Me.GridView1.Visible = True
        Me.SumbitOne.Visible = True

        'Catch ex As Exception
        '    lblStatus.BackColor = Color.Transparent
        '    lblStatus.ForeColor = Color.Red
        '    lblStatus.Text = "Error when trying to load file - if this persists - Please contact UtilityMasters - Thank You"
        'End Try


    End Sub

    'Protected Sub ButtonSaveToSpreadSheet_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonSaveToSpreadSheet.Click
    '    If Me.GridView1.Rows.Count = 0 Then
    '        Me.lblStatus.ForeColor = Color.Red
    '        Me.lblStatus.Text = "There are no items to process"
    '        Return
    '    End If

    '    If FileUpload2.PostedFile.ContentLength > 0 Then
    '    Else
    '        Me.lblStatus.ForeColor = Color.Red
    '        Me.lblStatus.Text = "Please select a vaild Excel file"
    '        Me.btnImportExcel.Visible = False
    '        Return
    '    End If

    '    Dim BigString As String = ""
    '    Dim sb As New StringBuilder()
    '    sb.Append("ID, MeterId, MeterName,value,date,Comment,Site" + Environment.NewLine)
    '    For Each gvr As GridViewRow In GridView1.Rows
    '        Dim iRead1 As TextBox = gvr.Cells(3).FindControl("Value")
    '        Dim iRead2 As TextBox = gvr.Cells(5).FindControl("Comment")
    '        BigString = ""
    '        BigString = gvr.Cells(0).Text & "," & gvr.Cells(1).Text & "," & gvr.Cells(2).Text & "," & Trim(iRead1.Text) & "," & Mid$(gvr.Cells(4).Text, 1, 10) & "," & Trim(iRead2.Text)
    '        BigString = BigString & "," & gvr.Cells(6).Text
    '        sb.Append(BigString)
    '        sb.Append(Environment.NewLine)
    '    Next

    '    Using sw As New System.IO.StreamWriter(FileUpload2.PostedFile.FileName.ToString(), False)
    '        sw.WriteLine(sb.ToString)
    '        lblStatus.ForeColor = Color.Blue
    '        lblStatus.Text = "File created succsessfully"
    '    End Using

    'End Sub

    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim CurrentValue As String = ""
            e.Row.Cells(0).ForeColor = System.Drawing.Color.Red
            e.Row.Cells(1).ForeColor = System.Drawing.Color.Green
            e.Row.Cells(4).Text = Mid$(e.Row.Cells(4).Text, 1, 10)
        End If
    End Sub

    Private Sub ExportToExcel(ByVal strFileName As String, ByVal dg As GridView)
        Response.ClearContent()
        Response.AddHeader("content-disposition", "attachment; filename=" & strFileName)
        Response.ContentType = "application/excel"
        Dim sw As New System.IO.StringWriter()
        Dim htw As New HtmlTextWriter(sw)
        GridView1.RenderControl(htw)
        Response.Write(sw.ToString())
        Response.End()
    End Sub

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Me.HiddenFieldUserID.Value = MySession.UserID
        Me.Panel1.BorderStyle = BorderStyle.Solid
        Me.Panel1.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFFFC0")
        Me.Panel1.BorderWidth = 1
        Me.Panel1.BorderColor = Color.LightGray

        Me.GridView1.GridLines = GridLines.None
        Me.GridView1.BackColor = Color.Transparent

        Me.btnImportExcel.Visible = True

        Me.GridView1.Visible = False
        Me.SumbitOne.Visible = False

    End Sub

    Protected Sub SumbitOne_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles SumbitOne.Click
        ' Must validate before actual input
        '  lblStatus.Visible = False
        Dim ValidContinue As Boolean = True
        For Each gvr As GridViewRow In GridView1.Rows
            Dim iRead1 As TextBox = gvr.Cells(3).FindControl("Value")
            Dim iRead2 As TextBox = gvr.Cells(5).FindControl("Comment")

            If Trim(iRead1.Text) = "" Or Trim(iRead1.Text) = " " Or IsNumeric(iRead1.Text) = False Then
                ValidContinue = False
                iRead1.BackColor = Color.Maroon
                iRead1.ForeColor = Color.White
            Else
                iRead1.BackColor = Color.White
                iRead1.ForeColor = Color.DarkSeaGreen
            End If
        Next

        If ValidContinue = True Then
            Call UpdateOk()
            Me.lblStatus.BackColor = Color.Transparent
            Me.lblStatus.ForeColor = Color.Green
            Me.lblStatus.Text = "File updated successfully"
            Me.GridView1.Visible = False
            Me.SumbitOne.Visible = False
            '  Response.Redirect("~/myapps/customermeters/Default.aspx")
        Else
            Me.lblStatus.BackColor = Color.Maroon
            Me.lblStatus.ForeColor = Color.White
            Me.lblStatus.Text = "Errors where found - Please review your entries - Thank You"
        End If
    End Sub


    Protected Function UpdateOk() As String
        ' if it get to this stage all items have been validated succesfully
        Dim rs As New ADODBConnection
        Dim strSQL As String = ""
        rs.OpenConnection()
        For Each gvr As GridViewRow In GridView1.Rows
            Dim iRead1 As TextBox = gvr.Cells(3).FindControl("Value")
            Dim iRead2 As TextBox = gvr.Cells(5).FindControl("Comment")
            strSQL = ""
            strSQL = "Update dbo.tblCustomer_ScheduledMeters SET Figure=" & "'" & Trim(Trim(iRead1.Text)) & "'" & " , " & "TagNote=" & "'" & XY(Trim(iRead2.Text)) & "'" & " ,validimport='Y'" & "  WHERE ID=" & Trim(gvr.Cells(0).Text)
            rs.OpenRecordSet(strSQL)
            'MsgBox(strSQL)
        Next
        rs.CloseConnection()
        UpdateOk = "done"
    End Function


    Protected Function XY(ByVal what As String) As String
        Dim nReturn As String = ""
        nReturn = what.Replace("'", "''")
        XY = nReturn
    End Function

    Protected Sub ImageButtonReturnToMenu_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonReturnToMenu.Click
        Response.Redirect("~/myapps/customermeters/Default.aspx")
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Protected Sub JOhnsmith()
        Dim aDataSet As New DataSet()
        '    aDataSet.ReadXml(New StringReader(aXmlDoc.OuterXml))

        ' Bind the DataSet to the grid view
        '   Dim gv As GridView = DirectCast(sender, GridView)
        'gv.DataSource = aDataSet
        'gv.DataBind()


    End Sub

End Class
