﻿<%@ Page Language="VB" MasterPageFile="~/landing.master" EnableEventValidation="false" AutoEventWireup="false" CodeFile="ShowMeterReadings.aspx.vb" Inherits="myapps_CustomerMeters_ShowMeterReadings" title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="content_right" Runat="Server">

<style>
.general {background: transparent url('/assets/content-back-general.jpg') no-repeat; padding-top:15px; padding-left:10px;}
.generalTitle{font-family:Arial;font-size:10px;}
.generalInput {font-family:Arial;font-size:10px;}
.generalButton{font-family:Arial;font-size:10px;background-color:Gray;color:White;border:solid 1px lightgray;cursor:hand;}
.navgeneral {position:absolute;top:83px;left:0px;width:220px;height:500px;background: white url('/assets/left-nav-account.png') no-repeat; padding-top:0px; padding-left:0px;}
.DownLoadOustandingItems {position:absolute;left:438px;top:53px;background-color:transparent;color:Navy;cursor:hand;  width: 221px; height: 22px;  }
.specialInputs{font-family:Arial;font-size:10px;background-color:white;border:solid 1px lightgrey;color:Navy;}
.ButtonSaveToSpreadSheet {position:absolute; font-family:Arial;font-size:13px;Color:Navy;background-color:transparent;cursor:hand;border:solid 1px green; }

.SumbitOne {position:absolute; font-family:Arial;font-size:13px;Color:Navy;background-color:transparent;cursor:hand;border:solid 1px green;  top: 8px;  left: 7px;    width: 177px;    }

    #form1
    {
        height: 385px;
    }

</style>

<form id="form1" Runat="Server">

<div  style="position:absolute;top:83px;left:0px;width:210px;height:480px" class="navgeneral">
  <ul class="styleformytreeview" runat="server" id="navOnlineReports">  </ul> 
</div>

<div  style="position:absolute;top:83px;left:230px;width:700px;height:550px" class="general">
<asp:ImageButton ID="ImageButtonReturn" 
        style="position:absolute;left:11px; top: 5px; height:52px; width: 101px;" 
        ImageUrl="~/assets/metering/buttons-myapps-dataentry-meterreadings.gif"   
        runat="server" ImageAlign="Left"/>

 <asp:Label ID="LabTitle" style="position:absolute;top:2px; left:113px;"  
        Font-Names="Arial"   Font-Size="14px" runat="server" ForeColor="Black"  
        Font-Bold="true" Text="Actual Meter Readings"></asp:Label> 

 <asp:Label ID="LabCompany" style="position:absolute;top:20px; left:115px;"  
        Font-Names="Arial"   Font-Size="12px" runat="server" ForeColor="DarkBlue"  
        Font-Bold="False" Text="Company name">
</asp:Label>         

 <asp:Label ID="LabSite"    style="position:absolute;top:39px; left:116px;"  
        Font-Names="Arial"   Font-Size="11px" runat="server" ForeColor="DarkGreen"  
        Font-Bold="False" Text="Please select a valid Company">
</asp:Label>         

 <asp:ImageButton  ID="ImageButtonReturnToMenu"   visible="true"  
        style="position:absolute;left:628px; top:6px;"  height="32px"  width="32px"   
        tooltip="Return to Meters menu" runat="server"   ImageAlign="Left"  
        ImageUrl="~/assets/metering/remove_48x48.png">
</asp:ImageButton>


<asp:RadioButtonList id="RadioButtonTotalOutstanding" Visible="false" Style="position: absolute; left: 10px; top: 55px;" BorderStyle="None" Width="170px" AutoPostBack="True" RepeatDirection="Horizontal" RepeatLayout="Table"   TextAlign="Left" Font-Names="Arial" Font-Size="10px" runat="server">
<asp:ListItem Text="Required read "   Value="ReadRequired" />
<asp:ListItem Text="All reads "      Value="AllMeters"   Selected="True" />
</asp:RadioButtonList>

<asp:Button ID="DownLoadOustandingItems"  runat="server" visible = "false" BorderStyle="None"  CssClass="DownLoadOustandingItems" text="Download Outstanding Meter Reads" />

 
<%-- Start --%>
<asp:Panel ID="PanelActualMeters" runat="server" 
        Style="left: 14px;  position: absolute; top: 76px; height: 394px;" 
        Visible="true" Width="650px"   BorderStyle="Double" BorderWidth="1px" 
        BorderColor="lightgray" ScrollBars="Vertical">

<br />

<asp:HiddenField ID="HiddenFieldCustomerName" runat="server" />
<asp:HiddenField ID="HiddenGridSelectID" runat="server" />
<asp:HiddenField ID="HiddenCompanyID" runat="server" />       
<asp:HiddenField ID="HiddenSiteID" runat="server" />    

           <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
                 BorderStyle="None" DataKeyNames="id" DataSourceID="SQL_MeterSelect" 
                 Font-Names="Arial" Font-Size="12px" GridLines="None" height="202px" 
                 HorizontalAlign="Left" ShowHeader="False" 
                 style="position:absolute;top:5px;left:2px;" Width="595px" 
        EnableModelValidation="True">
                 <Columns>
                     <asp:CommandField ShowSelectButton="True">
                         <ItemStyle BorderStyle="None" Height="10px" HorizontalAlign="Left" 
                             VerticalAlign="Top" Width="20px" />
                     </asp:CommandField>
                     <asp:BoundField DataField="id" HeaderText="id" InsertVisible="False" 
                         ReadOnly="True" SortExpression="id">
                         <ItemStyle BorderStyle="None" CssClass="hiddencol" HorizontalAlign="Left" 
                             VerticalAlign="Top" Width="0px" />
                     </asp:BoundField>
                    
                     <asp:BoundField DataField="varDisplayMeterName" HeaderText="varDisplayMeterName" 
                         SortExpression="varDisplayMeterName">
                         <ItemStyle BorderStyle="None" Height="10px" HorizontalAlign="Left" 
                             VerticalAlign="Top" Width="200px" />
                     </asp:BoundField>

                     <asp:BoundField DataField="SiteName" HeaderText="SiteName" 
                         SortExpression="SiteName">
                         <ItemStyle BorderStyle="None" Height="10px" HorizontalAlign="Left" 
                             VerticalAlign="Top" Width="400px" />
                     </asp:BoundField>
                     
                     <asp:BoundField DataField="MeterName" HeaderText="MeterName" 
                         SortExpression="MeterName">
                        <ItemStyle BorderStyle="None" CssClass="hiddencol" HorizontalAlign="Left" 
                             VerticalAlign="Top" Width="0px" />
                     </asp:BoundField>
                     <asp:BoundField DataField="MeterDepartment" HeaderText="MeterDepartment" 
                         SortExpression="MeterDepartment">
                         <ItemStyle BorderStyle="None" Height="10px" HorizontalAlign="Left" 
                             VerticalAlign="Top" Width="300px" />
                     </asp:BoundField>
                     <asp:BoundField DataField="CompanyID" HeaderText="CompanyID" 
                         SortExpression="CompanyID">
                         <ItemStyle BorderStyle="None" CssClass="hiddencol" HorizontalAlign="Left" 
                             VerticalAlign="Top" Width="0px" />
                     </asp:BoundField>
                     <asp:BoundField DataField="MeterTypeID" HeaderText="MeterTypeID" 
                         SortExpression="MeterTypeID">
                         <ItemStyle BorderStyle="None" CssClass="hiddencol" HorizontalAlign="Left" 
                             VerticalAlign="Top" Width="0px" />
                     </asp:BoundField>
                     <asp:BoundField DataField="MeterIdentifier" HeaderText="MeterIdentifier" 
                         SortExpression="MeterIdentifier">
                         <ItemStyle BorderStyle="None" CssClass="hiddencol" HorizontalAlign="Left" 
                             VerticalAlign="Top" Width="0px" />
                     </asp:BoundField>
                     <asp:BoundField DataField="Installed" HeaderText="Installed" 
                         SortExpression="Installed">
                         <ItemStyle BorderStyle="None" CssClass="hiddencol" HorizontalAlign="Left" 
                             VerticalAlign="Top" Width="0px" />
                     </asp:BoundField>
                     <asp:BoundField DataField="LastServiced" HeaderText="LastServiced" 
                         SortExpression="LastServiced">
                         <ItemStyle BorderStyle="None" CssClass="hiddencol" HorizontalAlign="Left" 
                             VerticalAlign="Top" Width="0px" />
                     </asp:BoundField>
                     <asp:BoundField DataField="Fitter" HeaderText="Fitter" SortExpression="Fitter">
                         <ItemStyle BorderStyle="None" CssClass="hiddencol" HorizontalAlign="Left" 
                             VerticalAlign="Top" Width="0px" />
                     </asp:BoundField>
                     <asp:BoundField DataField="TypeOf" HeaderText="TypeOf" SortExpression="TypeOf">
                         <ItemStyle BorderStyle="None" CssClass="hiddencol" HorizontalAlign="Left" 
                             VerticalAlign="Top" Width="0px" />
                     </asp:BoundField>
                     <asp:BoundField DataField="TemplateID" HeaderText="TemplateID" ReadOnly="True" 
                         SortExpression="TemplateID">
                         <ItemStyle BorderStyle="None" CssClass="hiddencol" HorizontalAlign="Left" 
                             VerticalAlign="Top" Width="0px" />
                     </asp:BoundField>
                     <asp:BoundField DataField="Template" HeaderText="Template" ReadOnly="True" 
                         SortExpression="Template">
                         <ItemStyle BorderStyle="None" CssClass="hiddencol" HorizontalAlign="Left" 
                             VerticalAlign="Top" Width="0px" />
                     </asp:BoundField>
                     <asp:BoundField DataField="NoDigits" HeaderText="NoDigits" ReadOnly="True" 
                         SortExpression="NoDigits">
                         <ItemStyle BorderStyle="None" CssClass="hiddencol" HorizontalAlign="Left" 
                             VerticalAlign="Top" Width="0px" />
                     </asp:BoundField>
                 </Columns>
                 <RowStyle BorderStyle="None" HorizontalAlign="Left" VerticalAlign="Top" 
                     Wrap="True" />
             </asp:GridView>
  
    <asp:SqlDataSource ID="SQL_MeterSelect" runat="server"   ConnectionString="<%$ ConnectionStrings:portal.utilitymasters.co.ukConnectionString %>" 
               SelectCommand="exec spCustomer_MeterTemplateUnit 0">
</asp:SqlDataSource>

          </asp:Panel>


<asp:Panel ID="PanelDownLoad" runat="server" 
        Style="left: 11px;  position: absolute; top: 4px; height: 500px;" 
        Visible="false" Width="650px"   BorderStyle="Double" BorderWidth="1px" 
        BorderColor="lightgray" ScrollBars="none">

             <asp:ImageButton  ID="ImageButtonHideDownLoad"   visible="true"  
                    style="position:absolute;left:615px; top:6px;"  height="32px"  width="32px"   
                    tooltip="Return to Oustanding Reading" runat="server"   ImageAlign="Left"  
                    ImageUrl="~/assets/metering/remove_48x48.png">
            </asp:ImageButton>        
        
        
      <%--  <asp:FileUpload ID="FileUpload2"  style="position:absolute;left:15px; top:6px; width:300px" visible="true" cssclass="specialInputs" runat="server" />--%>
        <asp:Button ID="ButtonSaveToSpreadSheet"  borderstyle="none" style="position:absolute;left:10px; top:6px;" cssclass="ButtonSaveToSpreadSheet" runat="server"  visible="true"  Text="Export TO Spreadsheet" />
       <%--
       
 <asp:Button ID="Selectall"  style="position:absolute;left:15px; top:25px;" cssclass="generalButton" runat="server"  visible="true"  Text="Select All" />
 <asp:Button ID="UnSelectall"  style="position:absolute;left:85px; top:25px;" cssclass="generalButton" runat="server"  visible="true"  Text="Unselect All" />
                        --%>
<asp:Label ID="LabelError" style="position:absolute;left:15px; top:45px; width:400px" visible="true" runat="server" Text=""></asp:Label>   
      
      
            <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="false" 
        GridLines="None" style="position:absolute;left:15px; top:65px; width:400px">
            <Columns>
                <asp:TemplateField HeaderText="Value">
                    <ItemTemplate>
                        <asp:CheckBox ID="CheckBox1" runat="server" checked="true" />
                    </ItemTemplate>
                    <controlStyle Width="15px" />
                    <HeaderStyle HorizontalAlign="Left" Width="15px" />
                    <ItemStyle HorizontalAlign="Left" Width="15px" />
                </asp:TemplateField>
                <asp:BoundField DataField="ID" HeaderText="ID" visible="false">
                    <controlStyle Width="0px" />
                    <HeaderStyle Width="0px" wrap="False" />
                    <ItemStyle Width="0px" wrap="False" />
                </asp:BoundField>
                <asp:BoundField DataField="MeterName" HeaderText="MeterName">
                    <controlStyle Width="350px" />
                    <HeaderStyle Width="350px" wrap="False" />
                    <ItemStyle Width="350px" wrap="False" />
                </asp:BoundField>
            </Columns>
    </asp:GridView>
      
  
      
</asp:Panel>





 <%-- End--%>
 
 
</div>
 
</form> 

</asp:Content>

