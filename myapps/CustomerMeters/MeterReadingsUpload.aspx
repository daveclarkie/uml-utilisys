﻿<%@ Page Language="VB" MasterPageFile="~/landing.master" AutoEventWireup="false" CodeFile="MeterReadingsUpload.aspx.vb" Inherits="myapps_CustomerMeters_MeterReadingsUpload" title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="content_right" Runat="Server">

<form id="MeterUploadsForm" Runat="Server">


<style>
.general {background: transparent url('/assets/content-back-general.jpg') no-repeat; padding-top:15px; padding-left:10px;}
.generalTitle{font-family:Arial;font-size:10px;}
.generalInput {font-family:Arial;font-size:10px;background-color:#FFFFC0;}
.generalButtons{font-family:Arial;font-size:10px;background-color:white;border:solid 1px lightgray;cursor:hand;color:black;}
.specialInputs{font-family:Arial;font-size:10px;background-color:white;border:solid 1px lightgrey;color:Navy;}
.generalmainTitle {font-family:Arial;font-size:13px;color:blue;}
.ButtonLoad {position:absolute; top: 467px; left: 16px; font-family:Arial;font-size:13px;Color:Navy;background-color:white;cursor:hand;border:solid 1px green;  }

.SumbitOne {position:absolute; font-family:Arial;font-size:13px;Color:Navy;background-color:transparent;cursor:hand;border:solid 1px green;  top: 8px;  left: 7px;    width: 177px;    }
.btnImportExcel {position:absolute; font-family:Arial;font-size:13px;Color:Navy;background-color:transparent;cursor:hand;border:solid 1px green; top: 28px;   left: 563px;   }
.Panel1 
    { 
        position:relative;width:646px; height:412px; 
overflow:scroll; top: 29px; 
left: 2px; 
        scrollbar-arrow-color: #4e658a;
        scrollbar-3dlight-color: #777799;
        scrollbar-darkshadow-color: #666677;
        scrollbar-face-color: #a1b1c3;
        scrollbar-highlight-color: #e9e9e9;
        scrollbar-shadow-color: #a1b1c3;
        scrollbar-track-color: #ffffff;
    }

    #MeterUploadsForm
    {
        height: 506px;
    }

</style>

    
<%-- <div  style="position:absolute;top:83px;left:-5px;width:215px" class="general">
  <ul class="styleformytreeview" runat="server" id="navOnlineReports">  </ul> 
</div>  
--%>

<div  style="position:relative;left:10px;" class="general" >
 
 <asp:ImageButton  ID="ImageButtonReturnToMenu"   visible="true"  
        style="position:absolute;left:628px; top:6px;"  height="32px"  width="32px"   
        tooltip="Return to Meters menu" runat="server"   ImageAlign="Left"  
        ImageUrl="~/assets/metering/remove_48x48.png">
</asp:ImageButton>
 

<asp:Label ID="lblFile" cssclass="generalmainTitle" runat="server" 
        Text="Select file for upload"></asp:Label> 
    <asp:Label ID="lblStatus" runat="server"   borderstyle="None" text="Status"
        style="position:absolute; top: 54px; left: 15px;" Font-Names="Arial" 
        Font-Size="11px"  ForeColor="#FF3300"></asp:Label>
<br />
<asp:FileUpload ID="FileUpload1" runat="server" Width="548px"  cssclass="specialInputs" />
<asp:Button ID="btnImportExcel" runat="server"  Text="Upload"  borderstyle="None"  cssclass="btnImportExcel" />  


<asp:Label ID="Importerror" runat="server"  
        style="position:absolute;font-family :Arial;font-size:11px; top: 7px; left: 548px;"   
        borderstyle="None"      Visible="false" Text="Submit status:"></asp:Label> 


<asp:Panel ID="Panel1" runat="server"   Visible="true" borderstyle="None"  CssClass="Panel1">
<asp:Button ID="SumbitOne" Visible="true"  borderstyle="None" runat="server"  cssclass="SumbitOne" Text="Submit entries for processing" />

<asp:GridView ID="GridView1" Visible="true" style="position:absolute; top: 47px; left: 6px; width: 1143px;"  borderstyle="None" runat="server" AutoGenerateColumns="false">
       <Columns>
                        <asp:BoundField DataField="ID" HeaderText="ID">
                               <controlStyle   Width="50px"    />
                               <HeaderStyle   Width="50px"      wrap="False"    />           
                               <ItemStyle       Width="50px"     wrap="False"    /> 
                        </asp:BoundField>
                        
                           <asp:BoundField DataField="MeterID" HeaderText="MeterID">
                               <controlStyle   Width="100px"    />
                               <HeaderStyle   Width="100px"      wrap="False"    />           
                               <ItemStyle       Width="100px"     wrap="False"    /> 
                        </asp:BoundField>
                        
                      <asp:BoundField DataField="MeterName" HeaderText="MeterName">
                               <controlStyle      Width="150px"    />
                               <HeaderStyle     Width="150px"      wrap="False"    />           
                               <ItemStyle         Width="150px"     wrap="False"    /> 
                        </asp:BoundField>
                  
                           <asp:TemplateField HeaderText="Value">
                                       <ItemTemplate>
                                            <asp:TextBox ID="Value" runat="server" Text='<%# Bind("Value") %>'></asp:TextBox>
                                       </ItemTemplate>
                                 <controlStyle   Width="70px"     cssclass="specialInputs"    />
                                 <HeaderStyle   Width="70px"      HorizontalAlign="Left"  />           
                                 <ItemStyle      Width="70px"      HorizontalAlign="Left"  />
                        </asp:TemplateField>
                    
                       <asp:BoundField DataField="Date" HeaderText="Date"   DataFormatString="{0:d}" >
                               <controlStyle   Width="50px"      />
                               <HeaderStyle   Width="50px"      wrap="False"  />           
                               <ItemStyle       Width="50px"    wrap="False"    /> 
                        </asp:BoundField> 
                    
                        <asp:TemplateField HeaderText="Comment">
                                       <ItemTemplate>
                                            <asp:TextBox ID="Comment" runat="server" Text='<%# Bind("Comment") %>'></asp:TextBox>
                                       </ItemTemplate>
                                 <controlStyle      Width="350px" cssclass="specialInputs"         />
                                 <HeaderStyle    Width="350px"     HorizontalAlign="Left"  />           
                                 <ItemStyle        Width="350px"       HorizontalAlign="Left"  />
                        </asp:TemplateField>
                    
                          <asp:BoundField DataField="Site" HeaderText="Site"  >
                               <controlStyle   Width="350px"      />
                               <HeaderStyle   Width="350px"      wrap="False"  />           
                               <ItemStyle         Width="350px"    wrap="False"    /> 
                        </asp:BoundField> 
        </Columns>
    </asp:GridView>                   
                    
<%--    <asp:Button ID="ButtonSaveToSpreadSheet" runat="server"  visible="false"   Text="Export TO Spreadsheet" />
    <asp:FileUpload ID="FileUpload2"  visible="false"  runat="server" />--%>
    
             
</asp:Panel>      
 
         
    
    
</div>
  <asp:HiddenField ID="HiddenFieldUserID"     runat="server" />
</form>
  

</asp:Content>

