﻿<%@ Page Language="VB" MasterPageFile="~/landing.master" AutoEventWireup="false" CodeFile="ShowActualMeterDetails.aspx.vb" Inherits="myapps_CustomerMeters_ShowActualMeterDetails" title="Untitled Page" %>
<%@ Register Assembly="DundasWebChart" Namespace="Dundas.Charting.WebControl" TagPrefix="DCWC" %>

<asp:Content ID="Content1" ContentPlaceHolderID="content_right" Runat="Server">
<form id="form1" runat="server">

 <div  style="position:absolute;top:83px;left:0px;width:210px;height:480px" class="general">
  <div>
  <asp:Label ID="Label1"    Font-Names="Arial"  Font-Size="12px"  forecolor="darkBlue"  runat="server" Text="Meter Name"></asp:Label>
  <br />
  <asp:Label ID="SelectedmeterNameTwo"    Font-Names="Arial"  Font-Size="11px"  forecolor="Blue"  runat="server" Text="MeterName"></asp:Label>
 </div>
<br /> 
  <ul class="styleformytreeview" runat="server" id="navOnlineReports">  </ul> 
</div>  

 
<div class="content_back general">

<asp:Panel ID="PanelActualMeterReadings"  Visible="true"  Width="657px" 
        runat="server" Height="500px" HorizontalAlign="Left" ScrollBars="none" >  
<%--<asp:ImageButton  ID="ImageButtonreturn"   visible="true"  style="position:relative;left:550px; top:6px;"  height="32px"  width="32px"   tooltip="Return to Meters menu" runat="server"   ImageAlign="Left"  ImageUrl="~/assets/metering/remove_48x48.png"></asp:ImageButton>      

<asp:Label ID="SelectedMeterName"   style="position:relative;left:5px; top:40px;"   Font-Names="Arial"  Font-Size="13px"  forecolor="darkBlue"  runat="server" Text="MeterName"></asp:Label>
<asp:Label ID="NextReadingDate" style="position:relative;left:5px; top:60px;"  runat="server" Font-Names="Arial"    Font-Size="12px" forecolor="Blue"   Text="NextReadingDate" visible="true"> </asp:Label>
<asp:Label ID="errorlabeldateexceeded"  style="position:relative;left:5px; top:80px;"  runat="server" Font-Names="Arial"   Font-Size="12px" forecolor="RED" Text="Date exceeded" visible="false"> </asp:Label>

<div id="myo" style="position:relative;left:5px; top:100px;">
  <asp:RadioButtonList ID="RadioButtonList1"   runat="server"   AutoPostBack="True"   Font-Names="Arial"  Font-Size="10px"   RepeatDirection="Vertical"   RepeatLayout="Table" TextAlign="Left" BorderStyle="None">
            <asp:ListItem Text="All " Value="All" />    
            <asp:ListItem Text="12 months " Value="12 months" />     
            <asp:ListItem Text="6 months " Value="6 months" />       
            <asp:ListItem Selected="True" Text="30 days " Value="30 days" />
        </asp:RadioButtonList>
        
        <asp:RadioButtonList ID="RadioOutStanding"   
            style="position: relative; top: 0px; left: 265px;" runat="server" 
            AutoPostBack="True" BorderStyle="None" Font-Names="Arial" Font-Size="10px" 
            RepeatDirection="Horizontal" RepeatLayout="Table" TextAlign="Left" >
            <asp:ListItem Text="Required read " Value="ReadRequired" />   <asp:ListItem Selected="True" Text="All reads " Value="AllMeters" />
        </asp:RadioButtonList>
        
        <asp:RadioButtonList ID="RadioButtonList2"  
            style="position: relative; top: 0px; left: 462px;" runat="server" 
            AutoPostBack="True" BorderStyle="None" Font-Names="Arial" Font-Size="10px" 
            RepeatDirection="Horizontal" RepeatLayout="Table" TextAlign="Left"  >
            <asp:ListItem Selected="false" Text="Assending " Value="Assending" />     <asp:ListItem Selected="True" Text="Descending " Value="Descending" />
        </asp:RadioButtonList>
</div>   --%>

  
<asp:ImageButton  ID="ImageButtonreturn"   visible="true"  
        style="position:relative;left:534px; top:-4px;"  height="32px"  width="32px"   
        tooltip="Return to Meters menu" runat="server"   ImageAlign="Left"  ImageUrl=""></asp:ImageButton>      


<asp:Label ID="SelectedMeterName"   style="position:relative;left:-26px; top:8px;"   Font-Names="Arial"  Font-Size="13px"  forecolor="darkBlue"  runat="server" Text="MeterName"></asp:Label>
<asp:Label ID="NextReadingDate" style="position:relative;left:-97px; top:27px;"  runat="server" Font-Names="Arial"    Font-Size="12px" forecolor="Blue"   Text="NextReadingDate" visible="true"> </asp:Label>
<asp:Label ID="errorlabeldateexceeded"  style="position:relative;left:-197px; top:45px;"  runat="server" Font-Names="Arial"   Font-Size="12px" forecolor="RED" Text="Date exceeded" visible="false"> </asp:Label>


   <asp:RadioButtonList ID="RadioButtonList1"    
  style="position:relative; left: 6px; top: 58px;" runat="server"  
   AutoPostBack="True"   Font-Names="Arial"  Font-Size="10px"   RepeatDirection="Horizontal"   RepeatLayout="Table" TextAlign="Left" BorderStyle="None" Width="267px">
            <asp:ListItem Text="All " Value="All" />    
            <asp:ListItem Text="12 months " Value="12 months" />     
            <asp:ListItem Text="6 months " Value="6 months" />       
            <asp:ListItem Selected="True" Text="30 days " Value="30 days" />
        </asp:RadioButtonList>
        
        <asp:RadioButtonList ID="RadioOutStanding"   
  style="position: relative; top: 31px; left: 280px;" runat="server" 
            AutoPostBack="True" BorderStyle="None" Font-Names="Arial" Font-Size="10px" 
            RepeatDirection="Horizontal" RepeatLayout="Table" TextAlign="Left" Height="18px" >
            <asp:ListItem Text="Required read " Value="ReadRequired" />   <asp:ListItem Selected="True" Text="All reads " Value="AllMeters" />
        </asp:RadioButtonList>
        
        <asp:RadioButtonList ID="RadioButtonList2"  
 style="position: relative; top: 4px; left: 448px;" runat="server" 
            AutoPostBack="True" BorderStyle="None" Font-Names="Arial" Font-Size="10px" 
            RepeatDirection="Horizontal" RepeatLayout="Table" TextAlign="Left" Height="11px" Width="171px"  >
            <asp:ListItem Text="Assending " Value="Assending" />     <asp:ListItem Selected="True" Text="Descending " Value="Descending" />
        </asp:RadioButtonList>
 
 

   </asp:Panel>
      
    


<asp:Panel ID="PanelGridView2" Style="position: relative; left: 3px; top:65px;" 
        Visible="False" Width="650px" runat="server" Height="228px" 
        HorizontalAlign="Left" ScrollBars="Vertical"  BorderStyle="Double" 
        BorderWidth="1px" BorderColor="lightgray" >


        
<asp:GridView ID="GridView2"   style="position:relative;top:30px;left:0px;"  Width="630px"  Height="10px"  runat="server" 
BorderStyle="None" 
GridLines="None" 
AutoGenerateColumns="False" 
DataSourceID="SQL_MeterReadings" 
HorizontalAlign="Left" 
Font-Names="Arial" 
Font-Size="10px" DataKeyNames="ID">
<HeaderStyle Font-Names="Arial" Font-Size="12px" HorizontalAlign="Left" VerticalAlign="Top" />
<SelectedRowStyle BorderStyle="None" HorizontalAlign="Left"   VerticalAlign="Top" />

<Columns>

<%--<asp:CommandField ShowSelectButton="True" SelectText="Edit">
<ItemStyle BorderStyle="None" Height="10px" HorizontalAlign="Left" VerticalAlign="Top"   Width="20px" Font-Names="Arial" Font-Size="12px" />
</asp:CommandField>
--%>

 <asp:BoundField DataField="id" HeaderText="Figure"    InsertVisible="False" ReadOnly="True" >
<ItemStyle BorderStyle="None" HorizontalAlign="Left" VerticalAlign="Top" CssClass="hiddencol" Width="0px" />
</asp:BoundField> 



<asp:TemplateField HeaderText="Value">
         <ItemTemplate>
            <asp:TextBox ID="Value" runat="server" Text='<%# Bind("Figure") %>'></asp:TextBox>
         </ItemTemplate>
     <controlStyle   Width="70px"/>
     <HeaderStyle   Width="70px"      HorizontalAlign="Left"  />           
     <ItemStyle      Width="70px"      HorizontalAlign="Left"  />
</asp:TemplateField>


<%--<asp:BoundField DataField="Figure" HeaderText="Reading"    SortExpression="Recorded Date">
<ItemStyle BorderStyle="None" Font-Names="Arial" Font-Size="12px" HorizontalAlign="Left"  VerticalAlign="Top" Width="100px" />
</asp:BoundField>--%>

<asp:BoundField DataField="RecordedDate" HeaderText="Day"    SortExpression="RecordedDate" >
<ItemStyle BorderStyle="None" Font-Names="Arial" Font-Size="12px" HorizontalAlign="Left"  VerticalAlign="Top" Width="200px" />
</asp:BoundField>

<asp:BoundField DataField="DayName">
<ItemStyle BorderStyle="None" Font-Names="Arial" Font-Size="12px" HorizontalAlign="Left"
 VerticalAlign="Top" Width="150px" />
</asp:BoundField>

</Columns>
</asp:GridView>
 
 </asp:Panel>

<%--The New Reading--%>
<asp:Panel ID="PanelActualMeterReadingsNew" Style="position: relative; left: 10px; top: 270px;"  visible="false"   runat="server" Height="125px" Width="666px"> 
<asp:ImageButton style=" POSITION: relative;top:0px;left:0px" id="ImageButtonCloseNewWindow"  
runat="server" 
ImageUrl="~/assets/button_cancel.gif" tooltip="View Chart" visible="true">
</asp:ImageButton> 

<asp:ImageButton style="LEFT: 35px; POSITION: relative; TOP: 0px" id="ImageButtonNewQuickUpdate" Width="100px" Height="25px" runat="server" ImageUrl="~/assets/button_save.gif" tooltip="Update current details" visible="true"></asp:ImageButton> 

<asp:Label style="LEFT: 8px; POSITION: relative; TOP: 29px" id="labQ1" Width="96px" Height="19px" runat="server" Text="Reading" Font-Size="12px" font-names="arial"></asp:Label>
<asp:TextBox style="LEFT: 115px; POSITION: relative; TOP: 29px" id="sEntryReading" Width="100px" runat="server" Font-Size="12px" font-names="arial"></asp:TextBox>

<asp:Label style="LEFT: 222px; POSITION: relative; TOP: 29px" id="labQ2" runat="server" Text="Time" Font-Size="12px" font-names="arial" Width="52px"></asp:Label>
&nbsp;

<asp:Label style="LEFT: 8px; POSITION: relative; TOP: 54px" id="labQ3" Width="100px" runat="server" Text="Read by" Font-Size="12px" font-names="arial"></asp:Label> 
<asp:TextBox style="LEFT: 281px; POSITION: relative; TOP: 28px" id="sEntryTime" runat="server" Text="hh:mm:ss" Font-Size="12px" font-names="arial" width="59px"></asp:TextBox> 

<asp:DropDownList style="LEFT: 114px; POSITION: relative; TOP: 55px" id="sPersonnelListing" runat="server" Font-Size="12px" font-names="arial" width="315px" DataValueField="ID" DataTextField="iName" DataSourceID="SQLPersonnel">
</asp:DropDownList>

<asp:Label style="LEFT: 9px; POSITION: relative; TOP: 79px" id="labQ4" Width="100px" runat="server" Text="Comments" Font-Size="12px" font-names="arial"></asp:Label>
<asp:TextBox style="LEFT: 115px; POSITION: relative; TOP: 80px" id="stxtTagNote" Width="537px" Height="13px" runat="server" Font-Size="12px" font-names="arial" maxlength="100"></asp:TextBox>

<asp:Label ID="errorlabel1" runat="server" style="position:relative; left: 8px; top: 105px;" Font-Size="12px" font-names="arial" forecolor="Red"  Width="650px"></asp:Label>

<asp:Label ID="NextReadDate" runat="server" style="  POSITION: relative;  left: 117px; top: 6px;"  Text="01/01/1900" BorderStyle="None" Font-Bold="True" Font-Names="Arial" Font-Size="12px" ForeColor="Green" Width="313px"></asp:Label>
</asp:Panel>
<%--The New Reazding--%>

 

 <asp:Panel ID="PanelActualMeterReadingsEditor" Style="position: relative; left: 0px; top: 234px;"   BorderStyle="Double" BorderColor="lightgray"  BorderWidth="1px"  visible="false"   runat="server" Height="161px" Width="650px"> 
      <asp:ImageButton style=" POSITION: relative;top:113px;left:9px" id="ImageButtonCloseEditWindow"   runat="server" ImageUrl="~/assets/button_cancel.gif" tooltip="Cancel edit" visible="true"></asp:ImageButton> 
      <asp:ImageButton style="LEFT: 534px; POSITION: relative; TOP: 114px" id="ImageButtonQuickUpdate" Width="100px" Height="25px" runat="server" ImageUrl="~/assets/button_save.gif" tooltip="Update current details" visible="true"></asp:ImageButton> 
 
      
            <asp:Label style="LEFT: 8px; POSITION: relative; TOP: 13px" id="Label23" Width="96px" Height="19px" runat="server" Text="Reading" Font-Size="12px" font-names="arial"></asp:Label>
            <asp:TextBox style="LEFT: 115px; POSITION: relative; TOP: 12px" id="entryreading" Width="100px" runat="server" Font-Size="12px" font-names="arial"></asp:TextBox>
      
            <asp:Label style="LEFT: 224px; POSITION: relative; TOP: 12px" id="Label24" runat="server" Text="Date : Time" Font-Size="12px" font-names="arial" Width="65px"></asp:Label>
            <asp:TextBox style="LEFT: 294px; POSITION: relative; TOP: 12px" id="entrydate" enabled="false"   runat="server" Text="dd/mm/yyyy" Font-Size="12px" font-names="arial" width="68px"></asp:TextBox> 
         
             <asp:Label style="LEFT: 8px; POSITION: relative; TOP: 38px" id="Label25" Width="100px" runat="server" Text="Read by" Font-Size="12px" font-names="arial"></asp:Label> 
            <asp:TextBox style="LEFT: 371px; POSITION: relative; TOP: 12px" id="entrytime" runat="server" Text="hh:mm:ss" Font-Size="12px" font-names="arial" width="59px"></asp:TextBox> 
         
            <asp:DropDownList style="LEFT: 114px; POSITION: relative; TOP: 39px" id="PersonnelListing" runat="server" Font-Size="12px" font-names="arial" width="317px" DataValueField="ID" DataTextField="iName" DataSourceID="SQLPersonnel">
            </asp:DropDownList>
            
            
            <asp:Label style="LEFT: 9px; POSITION: relative; TOP: 63px" id="Label30" Width="100px" runat="server" Text="Comments" Font-Size="12px" font-names="arial"></asp:Label>
            <asp:TextBox style="LEFT: 115px; POSITION: relative; TOP: 64px" id="txtTagNote" Width="513px" Height="13px" runat="server" Font-Size="12px" font-names="arial" maxlength="100"></asp:TextBox>
      
         <asp:Label ID="errorlabel" runat="server" style="position:relative; left: 114px; top: 89px;" Font-Size="12px" font-names="arial" forecolor="Red"  Width="339px"></asp:Label>
     </asp:Panel>

<%-- <asp:Panel ID="PanelGridView2Graph"  visible="true"  style="position:relative;left: 0px; top: 270px;"  Width="600px"  Height="300px" runat="server">  
   
                <DCWC:Chart ID="Chart1" runat="server" >
                            <Legends>
                                <DCWC:Legend Name="Default"></DCWC:Legend>
                            </Legends>
                            <Series>
                                <DCWC:Series Name="Default">
                                </DCWC:Series>
                            </Series>
                            <ChartAreas>
                                <DCWC:ChartArea Name="Default">
                                </DCWC:ChartArea>
                            </ChartAreas>
                        </DCWC:Chart>

 </asp:Panel> --%>
 
 
  </asp:Panel>


<!---   S T A R T  S E C O N D --->


   <asp:Panel ID="PanelActualFirstReadingsEditor" Style="position: absolute; left: 240px; top:125px;"  visible="false"   runat="server" Height="267px" Width="650px" BackColor="#FFFFC0"> 
<asp:ImageButton style=" POSITION: absolute;top:0px;left:605px" id="ImageButtonFirstReadingsClose"  Width="32px" Height="32px" runat="server" ImageUrl="~/assets/metering/remove_48x48.png" tooltip="Close window" visible="true"></asp:ImageButton> 
 <asp:ImageButton style="LEFT: 536px; POSITION: absolute; TOP: 231px" id="ImageButtonFirstReadingsUpdate" Width="100px" Height="25px" runat="server" ImageUrl="~/assets/button_save.gif" tooltip="Update current details" visible="true"></asp:ImageButton> 

<asp:Label style="LEFT: 556px; POSITION: absolute; TOP: 54px" id="Label3" Width="96px" Height="19px" runat="server" Text="Reading" Font-Size="12px" font-names="arial" Visible="False"></asp:Label>
<asp:Label ID="Label4" runat="server" Font-Names="arial" Font-Size="12px" Height="19px"   Style="left: 13px; position: absolute; top: 173px" Text="Monthly" Width="100px"></asp:Label>
<asp:Label ID="Label8" runat="server" Font-Names="arial" Font-Size="12px" Height="19px"  Style="left: 14px; position: absolute; top: 47px" Text="When would you like this reading to begin"   Width="237px" ForeColor="Green"></asp:Label>
<asp:Label ID="Current" runat="server" Font-Names="arial" Font-Size="12px" Height="19px"    Style="left: 13px; position: absolute; top: 23px" Width="496px" ForeColor="DodgerBlue"></asp:Label>
<asp:TextBox   style="LEFT: 557px; POSITION: absolute; TOP: 77px" id="nentryreading" Width="87px" runat="server" Font-Size="12px" font-names="arial" Visible="False"></asp:TextBox>
<asp:TextBox ID="nWeeklyYear" runat="server" Font-Names="arial" Font-Size="12px"    Style="left: 353px; position: absolute; top: 139px" Width="44px" Enabled="False"></asp:TextBox>
<asp:TextBox ID="ntxtEveryWeekStart" runat="server" Enabled="False" Font-Names="arial" Font-Size="12px"   Style="left: 333px; position: absolute; top: 107px" Width="64px">dd/mm/yyyy</asp:TextBox>

<asp:TextBox ID="ntxtEachdayStart" runat="server"  Visible="false" Enabled="False" Font-Names="arial" Font-Size="12px"   Style="left: 555px; position: absolute; top: 1px" Width="64px"></asp:TextBox>
<asp:TextBox ID="nMonthlyYear" runat="server" Font-Names="arial" Font-Size="12px" Style="left: 354px;  position: absolute; top: 169px" Width="44px" Enabled="False"></asp:TextBox>
<asp:Label style="LEFT: 558px; POSITION: absolute; TOP: 103px" id="Label9" runat="server" Text="Time of read" Font-Size="12px" font-names="arial" Width="77px" Visible="False"></asp:Label>
<asp:TextBox  enabled="false" style="LEFT: 211px; POSITION: absolute; TOP: 77px" id="nDailyDate"   runat="server" Text="dd/mm/yyyy" Font-Size="12px" font-names="arial" width="71px"></asp:TextBox> &nbsp;
<asp:Label style="LEFT: 559px; POSITION: absolute; TOP: 151px" id="Label10" Width="100px" runat="server" Text="Read by" Font-Size="12px" font-names="arial" Visible="False"></asp:Label> 
<asp:TextBox style="LEFT: 556px; POSITION: absolute; TOP: 123px" id="nentrytime" runat="server" Text="hh:mm:ss" Font-Size="12px" font-names="arial" width="59px" Visible="False"></asp:TextBox> 
<asp:DropDownList style="LEFT: 479px; POSITION: absolute; TOP: 93px" id="nPersonnelListing" runat="server" Font-Size="12px" font-names="arial" width="112px" DataValueField="ID" DataTextField="iName" DataSourceID="SQLPersonnel" Visible="False">
</asp:DropDownList>

<asp:Label style="LEFT: 560px; POSITION: absolute; TOP: 199px" id="Label11" Width="100px" runat="server" Text="Comments" Font-Size="12px" font-names="arial" Visible="False"></asp:Label>
<asp:TextBox style="LEFT: 475px; POSITION: absolute; TOP: 62px" id="ntxtTagNote" Width="93px" Height="14px" runat="server" Font-Size="12px" font-names="arial" maxlength="100" Visible="False"></asp:TextBox>

<asp:Label ID="errorlabel5" runat="server" style="position:absolute; left: 10px; top: 203px;" Font-Size="12px" font-names="arial" forecolor="Red"  Width="533px"></asp:Label>

<asp:Label ID="labelFirstmeterRead" runat="server" BorderStyle="None" Font-Names="Arial" Font-Size="13px"  ForeColor="darkgreen" Style="left: 142px; position: absolute; top: 5px" Text="Meter" Width="317px"></asp:Label>
<asp:Label ID="Label12" runat="server" BorderStyle="None" Font-Names="Arial" Font-Size="13px" ForeColor="Blue" Style="left: 12px; position: absolute; top: 5px" Text="Your first reading for:"   Width="175px"></asp:Label>

 <asp:DropDownList enabled="false"  AutoPostBack="True"  ID="DropDownListWeekNumbers" style="position:absolute; left: 215px; top: 140px;" runat="server" Font-Names="Arial" Font-Size="12px" Width="45px">
    <asp:ListItem Text="1" Value="1"/>
    <asp:ListItem Text="2" Value="2"/>
    <asp:ListItem Text="3" Value="3"/>
    <asp:ListItem Text="4" Value="4"/>
    <asp:ListItem Text="5" Value="5"/>
    <asp:ListItem Text="6" Value="6"/>
    <asp:ListItem Text="7" Value="7"/>
    <asp:ListItem Text="8" Value="8"/>
    <asp:ListItem Text="9" Value="9"/>
    <asp:ListItem Text="10" Value="10"/>
    <asp:ListItem Text="11" Value="11"/>
    <asp:ListItem Text="12" Value="12"/>
    <asp:ListItem Text="13" Value="13"/>
    <asp:ListItem Text="14" Value="14"/>
    <asp:ListItem Text="15" Value="15"/>
    <asp:ListItem Text="16" Value="16"/>
    <asp:ListItem Text="17" Value="17"/>
    <asp:ListItem Text="18" Value="18"/>
    <asp:ListItem Text="19" Value="19"/>
    <asp:ListItem Text="20" Value="20"/>
    <asp:ListItem Text="21" Value="21"/>
    <asp:ListItem Text="22" Value="22"/>
    <asp:ListItem Text="23" Value="23"/>
    <asp:ListItem Text="24" Value="24"/>
    <asp:ListItem Text="25" Value="25"/>
    <asp:ListItem Text="26" Value="26"/>
    <asp:ListItem Text="27" Value="27"/>
    <asp:ListItem Text="28" Value="28"/>
    <asp:ListItem Text="29" Value="29"/>
    <asp:ListItem Text="30" Value="30"/>
    <asp:ListItem Text="31" Value="31"/>
    <asp:ListItem Text="32" Value="32"/>
    <asp:ListItem Text="33" Value="33"/>
    <asp:ListItem Text="34" Value="34"/>
    <asp:ListItem Text="35" Value="35"/>
    <asp:ListItem Text="36" Value="36"/>
    <asp:ListItem Text="37" Value="37"/>
    <asp:ListItem Text="38" Value="38"/>
    <asp:ListItem Text="39" Value="39"/>
    <asp:ListItem Text="40" Value="40"/>
    <asp:ListItem Text="41" Value="41"/>
    <asp:ListItem Text="42" Value="42"/>
    <asp:ListItem Text="43" Value="43"/>
    <asp:ListItem Text="44" Value="44"/>
    <asp:ListItem Text="45" Value="45"/>
    <asp:ListItem Text="46" Value="46"/>
    <asp:ListItem Text="47" Value="47"/>
    <asp:ListItem Text="48" Value="48"/>
    <asp:ListItem Text="49" Value="49"/>
    <asp:ListItem Text="50" Value="50"/>
    <asp:ListItem Text="51" Value="51"/>
    <asp:ListItem Text="52" Value="52"/>
    </asp:DropDownList>
    
    <asp:DropDownList enabled="false" ID="DropDownListCharactorMonth" style="position:absolute; left: 554px; top: 28px;" runat="server" Font-Names="Arial" Font-Size="12px" Width="88px" Visible="False">
            <asp:ListItem Text="January" Value="1"/>
            <asp:ListItem Text="February" Value="2"/>
            <asp:ListItem Text="March" Value="3"/>
            <asp:ListItem Text="April" Value="4"/>
            <asp:ListItem Text="May" Value="5"/>
            <asp:ListItem Text="June" Value="6"/>
            <asp:ListItem Text="July" Value="7"/>
            <asp:ListItem Text="August" Value="8"/>
            <asp:ListItem Text="September" Value="9"/>
            <asp:ListItem Text="October" Value="10"/>
            <asp:ListItem Text="November" Value="11"/>
            <asp:ListItem Text="December" Value="11"/>
    </asp:DropDownList>
    
    <asp:DropDownList enabled="false" ID="DropDownListMonthNumbers" style="position:absolute; left: 216px; top: 170px;" runat="server" Font-Names="Arial" Font-Size="12px" Width="45px">
        <asp:ListItem Text="1" Value="1"/>
        <asp:ListItem Text="2" Value="2"/>
        <asp:ListItem Text="3" Value="3"/>
        <asp:ListItem Text="4" Value="4"/>
        <asp:ListItem Text="5" Value="5"/>
        <asp:ListItem Text="6" Value="6"/>
        <asp:ListItem Text="7" Value="7"/>
        <asp:ListItem Text="8" Value="8"/>
        <asp:ListItem Text="9" Value="9"/>
        <asp:ListItem Text="10" Value="10"/>
        <asp:ListItem Text="11" Value="11"/>
        <asp:ListItem Text="12" Value="12"/>
 </asp:DropDownList>

 <asp:DropDownList enabled="false" ID="DropDownListWeekDayName" style="position:absolute; left: 125px; top: 109px;" runat="server" Font-Names="Arial" Font-Size="12px" Width="103px">
            <asp:ListItem Text="Monday" Value="1"/>
            <asp:ListItem Text="Tuesday" Value="2"/>
            <asp:ListItem Text="Wednesday" Value="3"/>
            <asp:ListItem Text="Thursday" Value="4"/>
            <asp:ListItem Text="Friday" Value="5"/>
            <asp:ListItem Text="Saturday" Value="6"/>
            <asp:ListItem Text="Sunday" Value="7"/>
 </asp:DropDownList>


<asp:Label ID="Label13" runat="server" Font-Names="arial" Font-Size="12px" Height="19px"  Style="left: 13px; position: absolute; top: 109px" Text="Read every" Width="103px"></asp:Label>
<asp:Label ID="Label14" runat="server" Font-Names="arial" Font-Size="12px" Height="19px"  Style="left: 239px; position: absolute; top: 109px" Text="Starting from" Width="79px"></asp:Label>
<asp:Label ID="Label15" runat="server" Font-Names="arial" Font-Size="12px" Height="19px"  Style="left: 124px; position: absolute; top: 141px" Text="Starting Week"   Width="89px"></asp:Label>
<asp:Label ID="Label16" runat="server" Font-Names="arial" Font-Size="12px" Height="19px"  Style="left: 265px; position: absolute; top: 141px" Text="Starting Year" Width="81px"></asp:Label>
<asp:Label ID="Label17" runat="server" Font-Names="arial" Font-Size="12px" Height="19px"  Style="left: 267px; position: absolute; top: 171px" Text="Starting Year" Width="81px"></asp:Label>
<asp:Label ID="Label18" runat="server" Font-Names="arial" Font-Size="12px" Height="19px"  Style="left: 123px; position: absolute; top: 172px" Text="Starting Month"   Width="87px"></asp:Label>
<asp:Label ID="Label19" runat="server" Font-Names="arial" Font-Size="12px" Height="19px" Style="left: 126px; position: absolute; top: 79px" Text="Starting from" Width="79px"></asp:Label>
<asp:Label ID="Label26" runat="server" Font-Names="arial" Font-Size="12px" Height="19px"  Style="left: 14px; position: absolute; top: 141px" Text="Week number" Width="100px"></asp:Label>
<asp:Label ID="Label28" runat="server" Font-Names="arial" Font-Size="12px" Height="19px"  Style="left: 13px; position: absolute; top: 78px" Text="Each day"  Width="105px"></asp:Label>

  </asp:Panel>


    <asp:Panel ID="Preif"  style="position:absolute; left: 1px; top: 600px;" Visible="false" runat="server">
  
<asp:SqlDataSource ID="SQL_MeterReadings" runat="server" ConnectionString="<%$ ConnectionStrings:portal.utilitymasters.co.ukConnectionString %>" 
SelectCommand="EXEC spCustomer_MeterReadings '1980',0">
</asp:SqlDataSource>

<asp:SqlDataSource ID="SQLPersonnel" runat="server" 
ConnectionString="<%$ ConnectionStrings:portal.utilitymasters.co.ukConnectionString %>" 
SelectCommand="select 1-1">
</asp:SqlDataSource>

<asp:SqlDataSource ID="SQLPersonnelTwo" runat="server" 
ConnectionString="<%$ ConnectionStrings:portal.utilitymasters.co.ukConnectionString %>" 
SelectCommand="select 1-1">
</asp:SqlDataSource>          

<asp:SqlDataSource ID="SQL_MeterGeneral" runat="server" ConnectionString="<%$ ConnectionStrings:portal.utilitymasters.co.ukConnectionString %>" 
SelectCommand="Select 1-1">
</asp:SqlDataSource>      
    


<asp:HiddenField ID="HiddenFieldCurrentUser" runat="server" />
<asp:HiddenField ID="HiddenFieldSiteID" runat="server" />
<asp:HiddenField ID="HiddenFieldStartDate" runat="server" />
<asp:HiddenField ID="HiddenFieldFinishDate" runat="server" />
<asp:HiddenField ID="HiddenFieldNextReadingDate" runat="server" />
<asp:HiddenField ID="HiddenFieldScheduleType" runat="server" />
<asp:HiddenField ID="HiddenFieldEnterNewReading" runat="server" />
<asp:HiddenField ID="HiddenFieldCalendar3" runat="server" />
<asp:HiddenField ID="HiddenFieldMeterType" runat="server" />
<asp:HiddenField ID="HiddenTemplate" runat="server" />
<asp:HiddenField ID="hiddenCompanyID" runat="server" />
<asp:HiddenField ID="hiddenMeterID" runat="server" />
<asp:HiddenField ID="HiddenScheduledMeter" runat="server" />
       </asp:Panel>

</div>
</form>


</asp:Content>

