﻿Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Drawing
Imports System.Web
Imports Dundas.Charting.WebControl
Imports Dundas.Charting.WebControl.Utilities
Imports System.Web.Security
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.SessionState
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports System.Data
Imports System.Data.OleDb

Partial Class myapps_CustomerMeters_ShowActualMeterDetails
    Inherits System.Web.UI.Page

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Call LoadTreeview_Report()

        On Error Resume Next
        ' Response.Write(Session("MainURL") & "<br/>")
        ' Response.Write(Request.QueryString("MeterId"))
        'Session("MainURL") = Me.Page.Request.Url.PathAndQuery.ToString  'Request.ServerVariables("URL")
        'Session("MeterID") = mySelectedRow.Cells(1).Text
        'Session("MeterName") = mySelectedRow.Cells(2).Text

        Dim strSQL As String = ""

        Call Me.WhichMeter(Session("MeterName"))
        '================
        Me.SQL_MeterGeneral.SelectCommand = "SELECT  TOP 1 ID from dbo.tblCustomer_ScheduledMeters where meterid=" & Session("MeterID")

        Dim myItem As System.Data.DataView = CType(SQL_MeterGeneral.Select(DataSourceSelectArguments.Empty), System.Data.DataView)
        If myItem.Count > 0 Then
        Else
            strSQL = "SELECT  DATENAME(week,GETDATE()) as [WK] , MONTH(GETDATE()) as [M]  ,   'Current Day ' + '(' +  DATENAME(dw,GETDATE())   + ' )'  + ' Current Week (' + DATENAME(week,GETDATE())   + ') ' + ' Current Month (' + CONVERT(VARCHAR(2),MONTH(GETDATE()) ) + ')'  AS  [TITLE]    ,   ScheduleType from dbo.tblCustomer_Meters where ID=" & Session("MeterID")

            Me.SQL_MeterGeneral.SelectCommand = strSQL
            Dim myTwo As System.Data.DataView = CType(SQL_MeterGeneral.Select(DataSourceSelectArguments.Empty), System.Data.DataView)

            Me.nDailyDate.Enabled = "False"
            Me.DropDownListMonthNumbers.Enabled = "False"
            Me.DropDownListWeekDayName.Enabled = "False"
            Me.DropDownListWeekNumbers.Enabled = "False"

            For Each rowProduct As System.Data.DataRowView In myTwo
                Me.Current.Text = Trim(rowProduct("Title"))
                Me.DropDownListWeekNumbers.SelectedIndex = (Trim(rowProduct("WK")) - 1)
                Me.DropDownListMonthNumbers.SelectedIndex = (Trim(rowProduct("M")) - 1)

                Select Case Trim(rowProduct("ScheduleType"))
                    Case "D"
                        Me.nDailyDate.Enabled = "True"
                        Me.ntxtEachdayStart.Enabled = "True"
                    Case "M"
                        Me.DropDownListMonthNumbers.Enabled = "True"
                        Me.nMonthlyYear.Enabled = "True"
                    Case "W"
                        Me.DropDownListWeekDayName.Enabled = "True"
                        Me.ntxtEveryWeekStart.Enabled = "True"
                    Case "WK"
                        Me.DropDownListWeekNumbers.Enabled = "True"
                        Me.nWeeklyYear.Enabled = "True"
                End Select
                Exit For
            Next
            '========================
            '========================
            Me.labelFirstmeterRead.Text = Trim("need bits here")
            Me.PanelActualFirstReadingsEditor.Visible = True
        End If

        Call Me.recalculate(Me.RadioButtonList1.SelectedValue, Me.RadioButtonList2.SelectedValue, Me.RadioOutStanding.SelectedValue)

        Call Me.CreateNextReadingDate()
        'Call Me.ProcessChart()

        Me.PanelActualMeterReadings.Visible = True
        Me.PanelGridView2.BorderStyle = BorderStyle.Solid
        Me.PanelGridView2.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFFFC0")
        Me.PanelGridView2.BorderWidth = 1
        Me.PanelGridView2.BorderColor = Color.LightGray

        Me.PanelGridView2.Visible = True
        '========================


    End Sub

    Protected Function WhichMeter(ByVal mySelectedRowWas As String) As String
        On Error Resume Next
        Dim strSQL As String = ""
        strSQL = ""
        strSQL = strSQL & "  SELECT * FROM dbo.tblCustomer_Meters "
        strSQL = strSQL & "  WHERE ID=" & Session("MeterID")
        Me.SQL_MeterGeneral.SelectCommand = strSQL
        Dim T1 As String = ""
        Dim T2 As String = ""
        Dim T3 As String = ""

        Dim ivwExpensiveItems As System.Data.DataView = CType(SQL_MeterGeneral.Select(DataSourceSelectArguments.Empty), System.Data.DataView)
        If ivwExpensiveItems.Count > 0 Then
            For Each rowProduct As System.Data.DataRowView In ivwExpensiveItems

                Select Case rowProduct("TypeOf")
                    Case "C" ' Consumption
                        T1 = "Consumption"
                    Case "R" ' Reading ONLY
                        T1 = "Readings"
                    Case "O" ' OTher type
                    Case Else
                End Select

                Select Case rowProduct("ScheduleType")
                    Case "D"
                        T2 = "Daily read"
                        Me.HiddenFieldScheduleType.Value = "D"
                    Case "M" ' 
                        T2 = "Monthly read"
                        Me.HiddenFieldScheduleType.Value = "M"
                    Case "W"
                        T2 = "Weekly read - Specific day"
                        Me.HiddenFieldScheduleType.Value = "W"
                    Case "WK"
                        T2 = "Weekly read - No specific day"
                        Me.HiddenFieldScheduleType.Value = "W"
                    Case Else
                End Select

                Select Case rowProduct("UsedFor")
                    Case "E" ' Consumption
                        T3 = "Energy meter"
                    Case "P" ' Reading ONLY
                        T3 = "Production meter"
                    Case Else
                End Select

                Exit For
            Next

            'Me.SelectedMeterName.Text = Trim(mySelectedRowWas) & " - " & "(" & T1 & ")" & " - " & T2 & " (" & T3 & ")"
            Me.SelectedMeterName.Text = "(" & T1 & ")" & " - " & T2 & " (" & T3 & ")"
            Me.SelectedmeterNameTwo.Text = Session("MeterName")

        End If

        WhichMeter = "Ok"
    End Function


    Protected Function recalculate(ByVal what As String, ByVal OrderBy As String, ByVal FilterBy As String) As String
        On Error Resume Next
        Dim WhichDays As String = ""
        Dim WhichOrder As String = ""
        Dim WhichDaysNumeric As Integer = -1
        Dim iRequired As String = "No"

        Select Case what
            Case "12 months"
                WhichDays = "-366"
                WhichDaysNumeric = -366
            Case "6 months"
                WhichDays = "-" & (366 / 2)
                WhichDaysNumeric = -366 / 2
            Case "30 days"
                WhichDays = "-30"
                WhichDaysNumeric = -30
            Case "All"
                WhichDays = "-10000"
                WhichDaysNumeric = -10000
        End Select

        Select Case Me.RadioButtonList2.SelectedValue
            Case "Assending"
                WhichOrder = 0
                Session("WhichOrder") = "Assending"
            Case "Descending"
                WhichOrder = 1
                Session("WhichOrder") = "Descending"

        End Select

        Select Case Me.RadioOutStanding.SelectedValue
            Case "ReadRequired"
                iRequired = "Y"
            Case "AllMeters"
                iRequired = "N"
        End Select


        Me.HiddenFieldStartDate.Value = DateAdd(DateInterval.Day, WhichDaysNumeric, Now.Date)
        Me.HiddenFieldFinishDate.Value = Now.Date

        Me.PanelActualMeterReadingsNew.Visible = False
        Me.PanelActualMeterReadingsEditor.Visible = False
        '  blind the new readings
        '''Me.PanelGridView2Graph.Visible = True

        Dim strSQl As String = " EXEC dbo.spCustomer_MeterReadings_Flexible " & Session("MeterID") & "," & WhichDays & "," & WhichOrder & "," & "'" & iRequired & "'"
        Me.SQL_MeterReadings.SelectCommand = strSQl
        Me.GridView2.DataBind()

        '  Call ProcessChart()
        recalculate = "Ok"
    End Function

    Protected Function CreateNextReadingDate() As String
        On Error Resume Next
        Dim myConnectionString As String = "Provider=SQLNCLI.1;Integrated Security=SSPI;Persist Security Info=False;Initial Catalog=portal.utilitymasters.co.uk;Data Source=uk-ed0-sqlcl-01.MCEG.local"
        Dim strSQL As String = ""
        Dim CurrentDateIs As String = ""
        Dim TheDayIS As String = ""
        Select Case Me.HiddenFieldScheduleType.Value
            Case "D"
                strSQL = " SELECT john.TheDate  FROM  (  SELECT  CONVERT(VARCHAR(20),MAX(RecordedDate),103)   AS 'TheDate'  FROM dbo.tblCustomer_ScheduledMeters   WHERE MeterID=" & Session("MeterID") & " 	) AS john"
            Case "W"
                '  strSQL = "SELECT TOP 1 *   FROM dbo.tblCustomer_ScheduledMeters   WHERE MeterID= " & Me.hiddenMeterID.Value
                strSQL = " SELECT TOP 1 dbo.tblCustomer_ScheduledMeters.* , tblCustomer_Meters.Readingmethod    FROM dbo.tblCustomer_ScheduledMeters   "
                strSQL = strSQL & "  INNER JOIN dbo.tblCustomer_Meters ON dbo.tblCustomer_Meters.ID = tblCustomer_ScheduledMeters.MeterID "
                strSQL = strSQL & " WHERE MeterID=" & Session("MeterID") & "  ORDER BY dbo.tblCustomer_ScheduledMeters.RecordedDate DESC "
            Case "M"
                strSQL = "SELECT TOP 1 *   FROM dbo.tblCustomer_ScheduledMeters   WHERE MeterID= " & Session("MeterID") & "  ORDER BY dbo.tblCustomer_ScheduledMeters.RecordedDate DESC "
            Case "WK"
                strSQL = "SELECT TOP 1 *   FROM dbo.tblCustomer_ScheduledMeters   WHERE MeterID= " & Session("MeterID") & "  ORDER BY dbo.tblCustomer_ScheduledMeters.RecordedDate DESC "
        End Select

        Dim mySelectQuery As String = strSQL
        Dim myConnection As New OleDbConnection(myConnectionString)
        Dim myCommand As New OleDbCommand(mySelectQuery, myConnection)
        myCommand.Connection.Open()
        Dim myReader As OleDbDataReader = myCommand.ExecuteReader(CommandBehavior.CloseConnection)

        Select Case Me.HiddenFieldScheduleType.Value
            Case "D"
                myReader.Read()
                CurrentDateIs = myReader("TheDate")
                Me.NextReadDate.Text = "Next reading date will be - " & DateAdd(DateInterval.Day, 1, CDate(CurrentDateIs))
                Me.NextReadingDate.Text = "Your next reading will be -  " & DateAdd(DateInterval.Day, 1, CDate(CurrentDateIs))
                Me.HiddenFieldNextReadingDate.Value = DateAdd(DateInterval.Day, 1, CDate(CurrentDateIs))
            Case "W"
                myReader.Read()
                Dim NewDateIs As String = ""
                Dim TheStringIS As String = Right("00" & Day(myReader("RecordedDate")), 2) & "/" & Right("00" & Month(myReader("RecordedDate")), 2) & "/" & Right("0000" & Year(myReader("RecordedDate")), 4)
                CurrentDateIs = myReader("WeekNumber")
                TheDayIS = myReader("ReadingMethod")
                NewDateIs = DateAdd(DateInterval.Day, 7, CDate(TheStringIS))
                ' need to find the Thursday in this week , then display
                Me.NextReadDate.Text = "Next reading will be Week  " & CurrentDateIs + 1 & "  on " & Me.GetTheWeekName(TheDayIS) & " - " & NewDateIs
                Me.NextReadingDate.Text = "Your next reading will be Week " & CurrentDateIs + 1 & "  on " & Me.GetTheWeekName(TheDayIS) & " - " & NewDateIs
                Me.HiddenFieldNextReadingDate.Value = CurrentDateIs + 1 & " - " & Me.GetTheWeekName(TheDayIS) & " - " & NewDateIs
            Case "M"
                myReader.Read()
                CurrentDateIs = myReader("MonthNumber")
                Me.NextReadDate.Text = "Next reading will be - " & Me.GetTheMonthName(CurrentDateIs + 1) & " - " & Me.AddMonthNumber(myReader("RecordedDate"))
                Me.NextReadingDate.Text = "Your next reading will be - " & Me.GetTheMonthName(CurrentDateIs + 1) & " - " & Me.AddMonthNumber(myReader("RecordedDate"))
                Me.HiddenFieldNextReadingDate.Value = Me.GetTheMonthName(CurrentDateIs + 1) & " - " & Me.AddMonthNumber(myReader("RecordedDate"))
            Case "WK"
                myReader.Read()
                CurrentDateIs = myReader("WeekNumber")
                Me.NextReadDate.Text = "Next reading will be Week - " & CurrentDateIs + 1
                Me.NextReadingDate.Text = "Your next reading will be Week - " & CurrentDateIs + 1
                Me.HiddenFieldNextReadingDate.Value = CurrentDateIs + 1
        End Select
        myConnection.Close()

        CreateNextReadingDate = "OK"
    End Function

    Public Function GetWeekNumber(ByVal inDate As Date) As Integer
        Dim dayOfYear As Integer, wkNumber As Integer
        Dim compensation As Integer = 0
        Dim oneDate As String
        Dim firstDayDate As Date
        dayOfYear = inDate.DayOfYear
        oneDate = "1/1/" & inDate.Year.ToString
        firstDayDate = DateAndTime.DateValue(oneDate)
        Select Case firstDayDate.DayOfWeek
            Case DayOfWeek.Sunday
                compensation = 0
            Case DayOfWeek.Monday
                compensation = 6
            Case DayOfWeek.Tuesday
                compensation = 5
            Case DayOfWeek.Wednesday
                compensation = 4
            Case DayOfWeek.Thursday
                compensation = 3
            Case DayOfWeek.Friday
                compensation = 2
            Case DayOfWeek.Saturday
                compensation = 1
        End Select
        dayOfYear = dayOfYear - compensation
        If dayOfYear Mod 7 = 0 Then
            wkNumber = dayOfYear / 7
        Else
            wkNumber = (dayOfYear \ 7) + 1 'WATCH THE OPERATOR \ !!! IT RETURNS THE INTEGER RESULT
        End If
        Return wkNumber
    End Function


    Protected Function GetTheWeekName(ByVal what As String) As String
        Dim nReturn As String = ""
        Select Case what
            Case "1" : nReturn = "Monday"
            Case "2" : nReturn = "Tuesday"
            Case "3" : nReturn = "Wednesday"
            Case "4" : nReturn = "Thursday"
            Case "5" : nReturn = "Friday"
            Case "6" : nReturn = "Saturday"
            Case "7" : nReturn = "Sunday"
        End Select

        GetTheWeekName = nReturn
    End Function


    Protected Function GetTheMonthName(ByVal what As String) As String
        Dim nReturn As String = ""
        Select Case what
            Case "1" : nReturn = "January"
            Case "2" : nReturn = "February"
            Case "3" : nReturn = "March"
            Case "4" : nReturn = "April"
            Case "5" : nReturn = "May"
            Case "6" : nReturn = "June"
            Case "7" : nReturn = "July"
            Case "8" : nReturn = "August"
            Case "9" : nReturn = "September"
            Case "10" : nReturn = "October"
            Case "11" : nReturn = "November"
            Case "12" : nReturn = "December"
        End Select

        GetTheMonthName = nReturn
    End Function

    Protected Function GetTheNameMonth(ByVal what As String) As String
        Dim nReturn As String = ""
        Select Case what
            Case "January" : nReturn = "01"
            Case "February" : nReturn = "02"
            Case "March" : nReturn = "03"
            Case "April" : nReturn = "04"
            Case "May" : nReturn = "05"
            Case "June" : nReturn = "06"
            Case "July" : nReturn = "07"
            Case "August" : nReturn = "08"
            Case "September" : nReturn = "09"
            Case "October" : nReturn = "10"
            Case "November" : nReturn = "11"
            Case "December" : nReturn = "12"
        End Select

        GetTheNameMonth = nReturn
    End Function

    Public Function WeekOfMonth(ByVal dteInputDate As Date, ByVal intStartWeekday As Integer) As Integer
        Dim intWeekdayOf1st As Integer
        Dim intDaysInWeek1 As Integer
        Dim intDaysPastWeek1 As Integer
        Dim iReturn As Double

        intWeekdayOf1st = Weekday(DateSerial(Year(dteInputDate), Month(dteInputDate), 1))
        intDaysInWeek1 = (6 + intStartWeekday - intWeekdayOf1st) Mod 7 + 1
        If Day(dteInputDate) <= intDaysInWeek1 Then
            WeekOfMonth = 1
        Else
            intDaysPastWeek1 = Day(dteInputDate) - intDaysInWeek1
            ' just me being silly emmmmmmmmm but it works
            '  WeekOfMonth = 1 + (intDaysPastWeek1 \ 7) + Math.Abs(intDaysPastWeek1 Mod 7 <> 0)
            iReturn = intDaysPastWeek1 Mod 7 <> 0
            WeekOfMonth = 1 + (intDaysPastWeek1 \ 7) + Math.Abs(iReturn)
        End If
    End Function


    Protected Function WeekNumber(ByVal dteValue As Date) As Integer
        'Nicked from VB.NET rare as rocking horse *%^$
        Dim cult_info As System.Globalization.CultureInfo = System.Globalization.CultureInfo.CreateSpecificCulture("no")
        Dim cal As System.Globalization.Calendar = cult_info.Calendar
        'Dim weekNO As Integer = cal.GetWeekOfYear(dteValue, cult_info.DateTimeFormat.CalendarWeekRule, cult_info.DateTimeFormat.FirstDayOfWeek)
        WeekNumber = cal.GetWeekOfYear(dteValue, cult_info.DateTimeFormat.CalendarWeekRule, cult_info.DateTimeFormat.FirstDayOfWeek)
    End Function

    Protected Function MonthNumber(ByVal dteValue As Date) As Integer
        'Nicked from VB.NET rare as rocking horse *%^$
        Dim cult_info As System.Globalization.CultureInfo = System.Globalization.CultureInfo.CreateSpecificCulture("no")
        Dim cal As System.Globalization.Calendar = cult_info.Calendar
        'Dim weekNO1 As Integer = cal.GetMonth(dteValue)
        MonthNumber = cal.GetMonth(dteValue)
    End Function

    Protected Function AddWeekNumber(ByVal dteValue As Date) As Date
        'Nicked from VB.NET rare as rocking horse *%^$
        Dim cult_info As System.Globalization.CultureInfo = System.Globalization.CultureInfo.CreateSpecificCulture("no")
        Dim cal As System.Globalization.Calendar = cult_info.Calendar
        'Dim weekNO As Date = cal.AddWeeks(dteValue, 1)
        AddWeekNumber = cal.AddWeeks(dteValue, 1)
    End Function

    Protected Function AddMonthNumber(ByVal dteValue As Date) As Date
        'Nicked from VB.NET rare as rocking horse *%^$
        Dim cult_info As System.Globalization.CultureInfo = System.Globalization.CultureInfo.CreateSpecificCulture("no")
        Dim cal As System.Globalization.Calendar = cult_info.Calendar
        'Dim weekNO3 As Date = cal.AddMonths(dteValue, 1)
        AddMonthNumber = cal.AddMonths(dteValue, 1)
    End Function


    Private Sub LoadTreeview_Report()
        Me.navOnlineReports.InnerHtml = ""
        'Dim strSQL As String = "EXEC spPortal_TREEVIEW " & MySession.UserID
        'Dim myConnection As New OleDb.OleDbConnection(Core.CurrentConnectionString)
        'Dim myCommand As New OleDb.OleDbCommand(strSQL, myConnection)
        'myCommand.Connection.Open()
        'myCommand.CommandTimeout = 180
        'Dim Reader As OleDb.OleDbDataReader = myCommand.ExecuteReader(CommandBehavior.CloseConnection)

        'While Reader.Read
        '    Select Case CStr(Reader(3))
        '        Case CStr("Company")
        '            Me.navOnlineReports.InnerHtml += "<li class='styleformytreeview_company'><a href='MeterReader.aspx?custid=" & Reader(0).ToString & "&method=Customer'>" & Reader(1).ToString & "</a></li>"
        '        Case CStr("Header")
        '            Me.navOnlineReports.InnerHtml += "<li class='styleformytreeview_header'>" & Reader(1).ToString & "</li>"
        '        Case CStr("Site")
        '            Me.navOnlineReports.InnerHtml += "<li class='styleformytreeview_site'><a href='MeterReader.aspx?custid=" & Reader(0).ToString & "&method=Site'>" & Left(Reader(1).ToString, 23) & "..</a></li>"
        '        Case CStr("Group")
        '            Me.navOnlineReports.InnerHtml += "<li class='styleformytreeview_group'><a href='MeterReader.aspx?custid=" & Reader(0).ToString & "&method=Group'>" & Reader(1).ToString & "</a></li>"
        '    End Select
        'End While

        Me.navOnlineReports.InnerHtml += "<li class='styleformytreeview_company' style='height:10px;'></li>"
        Me.navOnlineReports.InnerHtml += "<li class='styleformytreeview_company'><a href='MeterReader.aspx?custid=</a>Meter Menu</a></li>"
        Me.navOnlineReports.InnerHtml += "<li class='styleformytreeview_company'><a href='Default.aspx'><< Back</a></li>"

        ' myapps/customermeters/Default.aspx

    End Sub

    Protected Sub ImageButtonreturn_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonreturn.Click
        'Session("MainURL") = Me.Page.Request.Url.PathAndQuery.ToString  'Request.ServerVariables("URL")

        Response.Redirect(Session("SubURL"))

    End Sub
End Class
