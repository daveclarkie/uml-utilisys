﻿<%@ Page Language="VB" MasterPageFile="~/landing.master" AutoEventWireup="false" CodeFile="Meters.aspx.vb" Inherits="myapps_CustomerMeters_Meters" %>
<%@ Register Assembly="DundasWebChart" Namespace="Dundas.Charting.WebControl" TagPrefix="DCWC" %>

 
<asp:Content ID="Content1" ContentPlaceHolderID="content_right" Runat="Server"> 

<script type="text/javascript" language="javascript">
      function RollOverValues(elementRef,elementVal)
            {
   //            alert(elementRef);
                g_imageWidth = elementRef.style.width;
                g_imageHieght = elementRef.style.height;
                elementRef.style.width = '30px';
                elementRef.style.height = '30px';
               document.getElementById("ctl00_ctl00_content_top_content_right_txtRollOver").value = elementVal;
            }

function RollOutValues(elementRef)
            {
                g_imageWidth = elementRef.style.width;
                g_imageHieght = elementRef.style.height;
                elementRef.style.width = '25px';
                elementRef.style.height = '25px';
               document.getElementById("ctl00_ctl00_content_top_content_right_txtRollOver").value = "";
            }
 
</script>
 
<form id="form1" runat="server">
<div class="content_back general">

<asp:Label ID="LabCompany"  style="position:absolute;top:90px;left:245px;"  Font-Names="Arial"   Font-Size="14px" runat="server" ForeColor="DarkBlue" Font-Bold="true" Text="Current selected meter"></asp:Label> 
 
<%--<asp:DropDownList ID="DropDownListSiteFilter" AutoPostBack="true"
        DataSourceID="SQL_UserSites" DataTextField="Value" 
        DataValueField="ID" 
        style="position:absolute;top: 90px; left: 550px;" 
         Font-Names="Arial"  Font-Size="12px"  
         Width="362px" 
         BackColor="#ffffff" 
         ForeColor="black"
        runat="server">
        </asp:DropDownList>
--%>
<asp:Label ID="LabelNoMeters"  Visible="false" style="position:absolute;top:110px;left:245px;"  Font-Names="Arial"   Font-Size="12px" runat="server" ForeColor="DarkRed" Font-Bold="true" Text="There are NO meter(s) Available - Click  the Create Meter Icon - to Setup your Meter(s)"></asp:Label> 
<asp:Label ID="Currentselectedmeter"  visible="false" style="position:absolute;top:115px;left:248px;"  Font-Names="Arial"   Font-Size="10px" runat="server" Text="Current selected meter"></asp:Label> 

<%--The big images--%>

<asp:ImageButton ID="ImageButtonCreateTheMeters" style="position:absolute;left:240px; top: 125px;" ImageUrl="~/assets/metering/buttons-myapps-dataentry-createmeter.gif" runat="server" ImageAlign="Left" />
<asp:ImageButton ID="ImageButtonReadTheMeters" style="position:absolute;left:521px; top: 125px;" runat="server" ImageAlign="Left" ImageUrl="~/assets/metering/buttons-myapps-dataentry-meterreadings.gif" />
<asp:ImageButton ID="ImageButtonMeterReaders" style="position:absolute;left:240px; top: 300px;" ImageUrl="~/assets/metering/buttons-myapps-dataentry-meterreader.gif" runat="server" ImageAlign="Left" />

<asp:ImageButton ID="ImageButtonOutstanding" 
style="position:absolute;left:521px; top: 300px;" 
ImageUrl="~/assets/metering/buttons-myapps-dataentry-meterreader.gif" 
runat="server" ImageAlign="Left" />



<asp:ImageButton ID="ImageButtonReturnToChoice"   visible="false" style="position:absolute;left:850px; top:90px;" height="32px" width="32px" tooltip="Return to Main options" runat="server" ImageAlign="Left" ImageUrl="~/assets/metering/remove_48x48.png" />
<asp:ImageButton ID="ImageButtonReturnToMeters"      visible="false" style="position:absolute;left:850px; top:90px;" height="32px" width="32px"  tooltip="Return to Meters window" runat="server"  ImageAlign="Left" ImageUrl="~/assets/metering/remove_48x48.png" />
<asp:ImageButton  id="ImageButtonReturnToTopLevel" visible="false"  style="position:absolute;left:850px; top:90px;" Width="32px" Height="32px" runat="server" ImageUrl="~/assets/metering/Remove_48x48.png" tooltip="Return to Main option"  >
</asp:ImageButton> &nbsp;

    
<asp:Panel ID="PanelActualMeters" runat="server" Height="550px" 
        Style="left: 903px;  position: absolute; top: 106px" Visible="False" 
        Width="650px"   BorderStyle="Double" BorderWidth="1px" BorderColor="lightgray" 
        ScrollBars="Vertical">
    
<asp:RadioButtonList id="RadioButtonTotalOutstanding" Style="position: absolute; left: 0px; top: 10px;" BorderStyle="None" Width="170px" AutoPostBack="True" RepeatDirection="Horizontal" RepeatLayout="Table"   TextAlign="Left" Font-Names="Arial" Font-Size="10px" runat="server">
<asp:ListItem Text="Required read "   Value="ReadRequired" />
<asp:ListItem Text="All reads "      Value="AllMeters"   Selected="True" />
</asp:RadioButtonList>

     <%-- <asp:Label ID="Sites"  visible="True" style="position:absolute;top:0px;left:10px;"  Font-Names="Arial"  width="100px"  Font-Size="12px" runat="server" Text="Your Site's"></asp:Label> 
  --%>

       <asp:GridView ID="GridView1" style="position:absolute;top:50px;left:2px;"  Width="595px"  runat="server" 
        AutoGenerateColumns="False" 
        DataKeyNames="id" 
        DataSourceID="SQL_MeterSelect" 
        Font-Names="Arial" 
        Font-Size="12px" 
        BorderStyle="None" 
        GridLines="None" 
        ShowHeader="False" 
        height="202px"
        HorizontalAlign="Left">

<Columns>
    <asp:CommandField ShowSelectButton="True">
        <ItemStyle BorderStyle="None" Height="10px" HorizontalAlign="Left" VerticalAlign="Top" Width="20px" />
    </asp:CommandField>
    
    <asp:BoundField DataField="id" HeaderText="id" InsertVisible="False" ReadOnly="True"
        SortExpression="id">
        <ItemStyle BorderStyle="None" HorizontalAlign="Left" VerticalAlign="Top" CssClass="hiddencol" Width="0px" />
    </asp:BoundField>
   
    <asp:BoundField DataField="MeterName" HeaderText="MeterName" SortExpression="MeterName">
        <ItemStyle BorderStyle="None" HorizontalAlign="Left" VerticalAlign="Top" Height="10px" Width="250px" />
    </asp:BoundField>
    <asp:BoundField DataField="MeterDepartment" HeaderText="MeterDepartment" SortExpression="MeterDepartment">
        <ItemStyle BorderStyle="None" HorizontalAlign="Left" VerticalAlign="Top" Height="10px" Width="300px" />
    </asp:BoundField>
    <asp:BoundField DataField="CompanyID" HeaderText="CompanyID" SortExpression="CompanyID">
        <ItemStyle BorderStyle="None" HorizontalAlign="Left" VerticalAlign="Top" CssClass="hiddencol" Width="0px" />
    </asp:BoundField>
    <asp:BoundField DataField="MeterTypeID" HeaderText="MeterTypeID" SortExpression="MeterTypeID">
        <ItemStyle BorderStyle="None" HorizontalAlign="Left" VerticalAlign="Top" CssClass="hiddencol" Width="0px" />
    </asp:BoundField>
    <asp:BoundField DataField="MeterIdentifier" HeaderText="MeterIdentifier" SortExpression="MeterIdentifier">
        <ItemStyle BorderStyle="None" HorizontalAlign="Left" VerticalAlign="Top" CssClass="hiddencol" Width="0px" />
    </asp:BoundField>
    <asp:BoundField DataField="Installed" HeaderText="Installed" SortExpression="Installed">
        <ItemStyle BorderStyle="None" HorizontalAlign="Left" VerticalAlign="Top" CssClass="hiddencol" Width="0px" />
    </asp:BoundField>
    <asp:BoundField DataField="LastServiced" HeaderText="LastServiced" SortExpression="LastServiced">
        <ItemStyle BorderStyle="None" HorizontalAlign="Left" VerticalAlign="Top" CssClass="hiddencol" Width="0px" />
    </asp:BoundField>
    <asp:BoundField DataField="Fitter" HeaderText="Fitter" SortExpression="Fitter">
        <ItemStyle BorderStyle="None" HorizontalAlign="Left" VerticalAlign="Top" CssClass="hiddencol" Width="0px" />
    </asp:BoundField>
    <asp:BoundField DataField="TypeOf" HeaderText="TypeOf" SortExpression="TypeOf">
        <ItemStyle BorderStyle="None" HorizontalAlign="Left" VerticalAlign="Top" CssClass="hiddencol" Width="0px" />
    </asp:BoundField>
    <asp:BoundField DataField="TemplateID" HeaderText="TemplateID" ReadOnly="True" SortExpression="TemplateID">
        <ItemStyle BorderStyle="None" HorizontalAlign="Left" VerticalAlign="Top" CssClass="hiddencol" Width="0px" />
    </asp:BoundField>
    <asp:BoundField DataField="Template" HeaderText="Template" ReadOnly="True" SortExpression="Template">
        <ItemStyle BorderStyle="None" HorizontalAlign="Left" VerticalAlign="Top" CssClass="hiddencol" Width="0px" />
    </asp:BoundField>
    <asp:BoundField DataField="NoDigits" HeaderText="NoDigits" ReadOnly="True" SortExpression="NoDigits">
        <ItemStyle BorderStyle="None" HorizontalAlign="Left" VerticalAlign="Top" CssClass="hiddencol" Width="0px" />
    </asp:BoundField>

</Columns>
    <RowStyle BorderStyle="None" HorizontalAlign="Left" VerticalAlign="Top" Wrap="True" />
</asp:GridView> 
        </asp:Panel>
    
    
    
 <asp:Panel ID="PanelActualMeterReadings" 
        Style="left: 374px;  position: absolute; top: 555px" Visible="False" 
        Width="700px" runat="server" Height="1000px" HorizontalAlign="Left" 
        ScrollBars="none" >    
 <asp:Label ID="SelectedMeterName" style="position:absolute;top:1px;left:1px;" 
Font-Names="Arial" 
Font-Size="13px"
forecolor="darkBlue"
 runat="server" Text="MeterName"></asp:Label>
 
 <asp:Label ID="NextReadingDate"  visible="true" style="position:absolute;top:15px;left:1px;" 
Font-Names="Arial" 
Font-Size="12px"
forecolor="Blue"
 runat="server" Text=""></asp:Label>

<asp:RadioButtonList id="RadioOutStanding" Style="position: absolute; left: 450px; top: 15px;" BorderStyle="None" Width="170px" AutoPostBack="True" RepeatDirection="Horizontal" RepeatLayout="Table"   TextAlign="Left" Font-Names="Arial" Font-Size="10px" runat="server">
<asp:ListItem Text="Required read "   Value="ReadRequired" />
<asp:ListItem Text="All reads "      Value="AllMeters"   Selected="True" />
</asp:RadioButtonList>


 <asp:Label ID="errorlabeldateexceeded" visible="false" style="position:absolute;top:15px;left:1px;" 
Font-Names="Arial" 
Font-Size="12px"
forecolor="RED"
 runat="server" Text="Date exceeded"></asp:Label>

<%-- <asp:LinkButton ID="LinkButtonEnterNewReading"
Font-Size="11px"
Font-Names="Arial" 
forecolor="Green" 
style="position:absolute;top:17px;left:550px;"  runat="server" Text="Enter new reading"></asp:LinkButton>--%>
 
 

<asp:Panel ID="PanelGridView2" Style="position: absolute; left: 0px; top:40px;" Visible="False" Width="650px" runat="server" Height="228px" HorizontalAlign="Left" ScrollBars="Vertical"  BorderStyle="Double" BorderWidth="1px" BorderColor="lightgray" >
<asp:GridView ID="GridView2"   style="position:absolute;top:30px;left:0px;"  Width="630px"  Height="10px"  runat="server" 
BorderStyle="None" 
GridLines="None" 
AutoGenerateColumns="False" 
DataSourceID="SQL_MeterReadings" 
HorizontalAlign="Left" 
Font-Names="Arial" 
Font-Size="10px" DataKeyNames="ID">
<HeaderStyle Font-Names="Arial" Font-Size="12px" HorizontalAlign="Left" VerticalAlign="Top" />
<SelectedRowStyle BorderStyle="None" HorizontalAlign="Left"   VerticalAlign="Top" />

<Columns>
<asp:CommandField ShowSelectButton="True" SelectText="Edit">
<ItemStyle BorderStyle="None" Height="10px" HorizontalAlign="Left" VerticalAlign="Top"   Width="20px" Font-Names="Arial" Font-Size="12px" />
</asp:CommandField>

<asp:BoundField DataField="id" HeaderText="Figure"    InsertVisible="False" ReadOnly="True" >
<ItemStyle BorderStyle="None" HorizontalAlign="Left" VerticalAlign="Top" CssClass="hiddencol" Width="0px" />
</asp:BoundField>

<asp:BoundField DataField="Figure" HeaderText="Reading"    SortExpression="Recorded Date">
<ItemStyle BorderStyle="None" Font-Names="Arial" Font-Size="12px" HorizontalAlign="Left"  VerticalAlign="Top" Width="100px" />
</asp:BoundField>

<asp:BoundField DataField="RecordedDate" HeaderText="Day"    SortExpression="RecordedDate" >
<ItemStyle BorderStyle="None" Font-Names="Arial" Font-Size="12px" HorizontalAlign="Left"  VerticalAlign="Top" Width="200px" />
</asp:BoundField>

<asp:BoundField DataField="DayName">
<ItemStyle BorderStyle="None" Font-Names="Arial" Font-Size="12px" HorizontalAlign="Left"
 VerticalAlign="Top" Width="150px" />
</asp:BoundField>

</Columns>
</asp:GridView>


<%--<asp:RadioButtonList id="RadioOutStanding" Style="position: absolute; left: 0px; top: 0px;" BorderStyle="None" Width="170px" AutoPostBack="True" RepeatDirection="Horizontal" RepeatLayout="Table"   TextAlign="Left" Font-Names="Arial" Font-Size="10px" runat="server">
<asp:ListItem Text="Read required "   Value="ReadRequired" />
<asp:ListItem Text="All meters "      Value="AllMeters"   Selected="True" />
</asp:RadioButtonList>--%>

<asp:RadioButtonList id="RadioButtonList1" Style="position: absolute; left: 20px; top: 0px;" AutoPostBack="True" RepeatDirection="Horizontal"  RepeatLayout="Table"  TextAlign="Left"  runat="server" Font-Names="Arial" Font-Size="10px" Width="256px">
<asp:ListItem Text="All "   Value="All" /> 
<asp:ListItem Text="12 months "   Value="12 months" />
<asp:ListItem Text="6 months "   Value="6 months" />
<asp:ListItem Text="30 days "   Value="30 days"  Selected="True" /> 
</asp:RadioButtonList>

<asp:RadioButtonList id="RadioButtonList2" Style="position: absolute; left: 450px; top: 0px;" BorderStyle="None" Width="170px" AutoPostBack="True" RepeatDirection="Horizontal" RepeatLayout="Table"   TextAlign="Left" Font-Names="Arial" Font-Size="10px" runat="server">
   <asp:ListItem Text="Assending "   Value="Assending"   Selected="false" />
   <asp:ListItem Text="Descending "   Value="Descending"   Selected="True" />
</asp:RadioButtonList>
<%--
<asp:ImageButton ID="ImageButtonReturnToMeters" Style="position: absolute; left: 0px; top: 0px;"   runat="server"  ImageAlign="Left" ImageUrl="~/assets/metering/Refresh_48x48.png" Width="28px"  Height="28px"/>
    &nbsp; --%></asp:Panel>

<%--The New Reading--%>
<asp:Panel ID="PanelActualMeterReadingsNew" Style="position: absolute; left: 10px; top: 270px;"  visible="false"   runat="server" Height="125px" Width="666px"> 
<asp:ImageButton style=" POSITION: absolute;top:0px;left:0px" id="ImageButtonCloseNewWindow"  
runat="server" 
ImageUrl="~/assets/button_cancel.gif" tooltip="View Chart" visible="true">
</asp:ImageButton> 

<asp:ImageButton style="LEFT: 35px; POSITION: absolute; TOP: 0px" id="ImageButtonNewQuickUpdate" Width="100px" Height="25px" runat="server" ImageUrl="~/assets/button_save.gif" tooltip="Update current details" visible="true"></asp:ImageButton> 

<asp:Label style="LEFT: 8px; POSITION: absolute; TOP: 29px" id="labQ1" Width="96px" Height="19px" runat="server" Text="Reading" Font-Size="12px" font-names="arial"></asp:Label>
<asp:TextBox style="LEFT: 115px; POSITION: absolute; TOP: 29px" id="sEntryReading" Width="100px" runat="server" Font-Size="12px" font-names="arial"></asp:TextBox>

<asp:Label style="LEFT: 222px; POSITION: absolute; TOP: 29px" id="labQ2" runat="server" Text="Time" Font-Size="12px" font-names="arial" Width="52px"></asp:Label>
&nbsp;

<asp:Label style="LEFT: 8px; POSITION: absolute; TOP: 54px" id="labQ3" Width="100px" runat="server" Text="Read by" Font-Size="12px" font-names="arial"></asp:Label> 
<asp:TextBox style="LEFT: 281px; POSITION: absolute; TOP: 28px" id="sEntryTime" runat="server" Text="hh:mm:ss" Font-Size="12px" font-names="arial" width="59px"></asp:TextBox> 

<asp:DropDownList style="LEFT: 114px; POSITION: absolute; TOP: 55px" id="sPersonnelListing" runat="server" Font-Size="12px" font-names="arial" width="315px" DataValueField="ID" DataTextField="iName" DataSourceID="SQLPersonnel">
</asp:DropDownList>

<asp:Label style="LEFT: 9px; POSITION: absolute; TOP: 79px" id="labQ4" Width="100px" runat="server" Text="Comments" Font-Size="12px" font-names="arial"></asp:Label>
<asp:TextBox style="LEFT: 115px; POSITION: absolute; TOP: 80px" id="stxtTagNote" Width="537px" Height="13px" runat="server" Font-Size="12px" font-names="arial" maxlength="100"></asp:TextBox>

<asp:Label ID="errorlabel1" runat="server" style="position:absolute; left: 8px; top: 105px;" Font-Size="12px" font-names="arial" forecolor="Red"  Width="650px"></asp:Label>

<asp:Label ID="NextReadDate" runat="server" style="  POSITION: absolute;  left: 117px; top: 6px;"  Text="01/01/1900" BorderStyle="None" Font-Bold="True" Font-Names="Arial" Font-Size="12px" ForeColor="Green" Width="313px"></asp:Label>
</asp:Panel>
<%--The New Reazding--%>

 

 <asp:Panel ID="PanelActualMeterReadingsEditor" Style="position: absolute; left: 0px; top: 234px;"   BorderStyle="Double" BorderColor="lightgray"  BorderWidth="1px"  visible="false"   runat="server" Height="161px" Width="650px"> 
      <asp:ImageButton style=" POSITION: absolute;top:113px;left:9px" id="ImageButtonCloseEditWindow"   runat="server" ImageUrl="~/assets/button_cancel.gif" tooltip="Cancel edit" visible="true"></asp:ImageButton> 
      <asp:ImageButton style="LEFT: 534px; POSITION: absolute; TOP: 114px" id="ImageButtonQuickUpdate" Width="100px" Height="25px" runat="server" ImageUrl="~/assets/button_save.gif" tooltip="Update current details" visible="true"></asp:ImageButton> 
 
      
            <asp:Label style="LEFT: 8px; POSITION: absolute; TOP: 13px" id="Label23" Width="96px" Height="19px" runat="server" Text="Reading" Font-Size="12px" font-names="arial"></asp:Label>
            <asp:TextBox style="LEFT: 115px; POSITION: absolute; TOP: 12px" id="entryreading" Width="100px" runat="server" Font-Size="12px" font-names="arial"></asp:TextBox>
      
            <asp:Label style="LEFT: 224px; POSITION: absolute; TOP: 12px" id="Label24" runat="server" Text="Date : Time" Font-Size="12px" font-names="arial" Width="65px"></asp:Label>
            <asp:TextBox style="LEFT: 294px; POSITION: absolute; TOP: 12px" id="entrydate" enabled="false"   runat="server" Text="dd/mm/yyyy" Font-Size="12px" font-names="arial" width="68px"></asp:TextBox> 
         
             <asp:Label style="LEFT: 8px; POSITION: absolute; TOP: 38px" id="Label25" Width="100px" runat="server" Text="Read by" Font-Size="12px" font-names="arial"></asp:Label> 
            <asp:TextBox style="LEFT: 371px; POSITION: absolute; TOP: 12px" id="entrytime" runat="server" Text="hh:mm:ss" Font-Size="12px" font-names="arial" width="59px"></asp:TextBox> 
         
            <asp:DropDownList style="LEFT: 114px; POSITION: absolute; TOP: 39px" id="PersonnelListing" runat="server" Font-Size="12px" font-names="arial" width="317px" DataValueField="ID" DataTextField="iName" DataSourceID="SQLPersonnel">
            </asp:DropDownList>
            
            
            <asp:Label style="LEFT: 9px; POSITION: absolute; TOP: 63px" id="Label30" Width="100px" runat="server" Text="Comments" Font-Size="12px" font-names="arial"></asp:Label>
            <asp:TextBox style="LEFT: 115px; POSITION: absolute; TOP: 64px" id="txtTagNote" Width="513px" Height="13px" runat="server" Font-Size="12px" font-names="arial" maxlength="100"></asp:TextBox>
      
         <asp:Label ID="errorlabel" runat="server" style="position:absolute; left: 114px; top: 89px;" Font-Size="12px" font-names="arial" forecolor="Red"  Width="339px"></asp:Label>
     </asp:Panel>

<asp:Panel ID="PanelGridView2Graph"  visible="true"  style="position:absolute;left: 0px; top: 270px;"  Width="600px"  Height="300px" runat="server">  
   
                <DCWC:Chart ID="Chart1" runat="server" >
                            <Legends>
                                <DCWC:Legend Name="Default"></DCWC:Legend>
                            </Legends>
                            <Series>
                                <DCWC:Series Name="Default">
                                </DCWC:Series>
                            </Series>
                            <ChartAreas>
                                <DCWC:ChartArea Name="Default">
                                </DCWC:ChartArea>
                            </ChartAreas>
                        </DCWC:Chart>

 </asp:Panel>
  </asp:Panel>
  
  
  
   <asp:Panel ID="PanelActualFirstReadingsEditor" Style="position: absolute; left: 240px; top:125px;"  visible="false"   runat="server" Height="267px" Width="650px" BackColor="#FFFFC0"> 
<asp:ImageButton style=" POSITION: absolute;top:0px;left:605px" id="ImageButtonFirstReadingsClose"  Width="32px" Height="32px" runat="server" ImageUrl="~/assets/metering/remove_48x48.png" tooltip="Close window" visible="true"></asp:ImageButton> 
 <asp:ImageButton style="LEFT: 536px; POSITION: absolute; TOP: 231px" id="ImageButtonFirstReadingsUpdate" Width="100px" Height="25px" runat="server" ImageUrl="~/assets/button_save.gif" tooltip="Update current details" visible="true"></asp:ImageButton> 

<asp:Label style="LEFT: 556px; POSITION: absolute; TOP: 54px" id="Label3" Width="96px" Height="19px" runat="server" Text="Reading" Font-Size="12px" font-names="arial" Visible="False"></asp:Label>
<asp:Label ID="Label4" runat="server" Font-Names="arial" Font-Size="12px" Height="19px"   Style="left: 13px; position: absolute; top: 173px" Text="Monthly" Width="100px"></asp:Label>
<asp:Label ID="Label8" runat="server" Font-Names="arial" Font-Size="12px" Height="19px"  Style="left: 14px; position: absolute; top: 47px" Text="When would you like this reading to begin"   Width="237px" ForeColor="Green"></asp:Label>
<asp:Label ID="Current" runat="server" Font-Names="arial" Font-Size="12px" Height="19px"    Style="left: 13px; position: absolute; top: 23px" Width="496px" ForeColor="DodgerBlue"></asp:Label>
<asp:TextBox   style="LEFT: 557px; POSITION: absolute; TOP: 77px" id="nentryreading" Width="87px" runat="server" Font-Size="12px" font-names="arial" Visible="False"></asp:TextBox>
<asp:TextBox ID="nWeeklyYear" runat="server" Font-Names="arial" Font-Size="12px"    Style="left: 353px; position: absolute; top: 139px" Width="44px" Enabled="False"></asp:TextBox>
<asp:TextBox ID="ntxtEveryWeekStart" runat="server" Enabled="False" Font-Names="arial" Font-Size="12px"   Style="left: 333px; position: absolute; top: 107px" Width="64px">dd/mm/yyyy</asp:TextBox>

<asp:TextBox ID="ntxtEachdayStart" runat="server"  Visible="false" Enabled="False" Font-Names="arial" Font-Size="12px"   Style="left: 555px; position: absolute; top: 1px" Width="64px"></asp:TextBox>
<asp:TextBox ID="nMonthlyYear" runat="server" Font-Names="arial" Font-Size="12px" Style="left: 354px;  position: absolute; top: 169px" Width="44px" Enabled="False"></asp:TextBox>
<asp:Label style="LEFT: 558px; POSITION: absolute; TOP: 103px" id="Label9" runat="server" Text="Time of read" Font-Size="12px" font-names="arial" Width="77px" Visible="False"></asp:Label>
<asp:TextBox  enabled="false" style="LEFT: 211px; POSITION: absolute; TOP: 77px" id="nDailyDate"   runat="server" Text="dd/mm/yyyy" Font-Size="12px" font-names="arial" width="71px"></asp:TextBox> &nbsp;
<asp:Label style="LEFT: 559px; POSITION: absolute; TOP: 151px" id="Label10" Width="100px" runat="server" Text="Read by" Font-Size="12px" font-names="arial" Visible="False"></asp:Label> 
<asp:TextBox style="LEFT: 556px; POSITION: absolute; TOP: 123px" id="nentrytime" runat="server" Text="hh:mm:ss" Font-Size="12px" font-names="arial" width="59px" Visible="False"></asp:TextBox> 
<asp:DropDownList style="LEFT: 479px; POSITION: absolute; TOP: 93px" id="nPersonnelListing" runat="server" Font-Size="12px" font-names="arial" width="112px" DataValueField="ID" DataTextField="iName" DataSourceID="SQLPersonnel" Visible="False">
</asp:DropDownList>

<asp:Label style="LEFT: 560px; POSITION: absolute; TOP: 199px" id="Label11" Width="100px" runat="server" Text="Comments" Font-Size="12px" font-names="arial" Visible="False"></asp:Label>
<asp:TextBox style="LEFT: 475px; POSITION: absolute; TOP: 62px" id="ntxtTagNote" Width="93px" Height="14px" runat="server" Font-Size="12px" font-names="arial" maxlength="100" Visible="False"></asp:TextBox>

<asp:Label ID="errorlabel5" runat="server" style="position:absolute; left: 10px; top: 203px;" Font-Size="12px" font-names="arial" forecolor="Red"  Width="533px"></asp:Label>

<asp:Label ID="labelFirstmeterRead" runat="server" BorderStyle="None" Font-Names="Arial" Font-Size="13px"  ForeColor="darkgreen" Style="left: 142px; position: absolute; top: 5px" Text="Meter" Width="317px"></asp:Label>
<asp:Label ID="Label12" runat="server" BorderStyle="None" Font-Names="Arial" Font-Size="13px" ForeColor="Blue" Style="left: 12px; position: absolute; top: 5px" Text="Your first reading for:"   Width="175px"></asp:Label>

 <asp:DropDownList enabled="false"  AutoPostBack="True"  ID="DropDownListWeekNumbers" style="position:absolute; left: 215px; top: 140px;" runat="server" Font-Names="Arial" Font-Size="12px" Width="45px">
    <asp:ListItem Text="1" Value="1"/>
    <asp:ListItem Text="2" Value="2"/>
    <asp:ListItem Text="3" Value="3"/>
    <asp:ListItem Text="4" Value="4"/>
    <asp:ListItem Text="5" Value="5"/>
    <asp:ListItem Text="6" Value="6"/>
    <asp:ListItem Text="7" Value="7"/>
    <asp:ListItem Text="8" Value="8"/>
    <asp:ListItem Text="9" Value="9"/>
    <asp:ListItem Text="10" Value="10"/>
    <asp:ListItem Text="11" Value="11"/>
    <asp:ListItem Text="12" Value="12"/>
    <asp:ListItem Text="13" Value="13"/>
    <asp:ListItem Text="14" Value="14"/>
    <asp:ListItem Text="15" Value="15"/>
    <asp:ListItem Text="16" Value="16"/>
    <asp:ListItem Text="17" Value="17"/>
    <asp:ListItem Text="18" Value="18"/>
    <asp:ListItem Text="19" Value="19"/>
    <asp:ListItem Text="20" Value="20"/>
    <asp:ListItem Text="21" Value="21"/>
    <asp:ListItem Text="22" Value="22"/>
    <asp:ListItem Text="23" Value="23"/>
    <asp:ListItem Text="24" Value="24"/>
    <asp:ListItem Text="25" Value="25"/>
    <asp:ListItem Text="26" Value="26"/>
    <asp:ListItem Text="27" Value="27"/>
    <asp:ListItem Text="28" Value="28"/>
    <asp:ListItem Text="29" Value="29"/>
    <asp:ListItem Text="30" Value="30"/>
    <asp:ListItem Text="31" Value="31"/>
    <asp:ListItem Text="32" Value="32"/>
    <asp:ListItem Text="33" Value="33"/>
    <asp:ListItem Text="34" Value="34"/>
    <asp:ListItem Text="35" Value="35"/>
    <asp:ListItem Text="36" Value="36"/>
    <asp:ListItem Text="37" Value="37"/>
    <asp:ListItem Text="38" Value="38"/>
    <asp:ListItem Text="39" Value="39"/>
    <asp:ListItem Text="40" Value="40"/>
    <asp:ListItem Text="41" Value="41"/>
    <asp:ListItem Text="42" Value="42"/>
    <asp:ListItem Text="43" Value="43"/>
    <asp:ListItem Text="44" Value="44"/>
    <asp:ListItem Text="45" Value="45"/>
    <asp:ListItem Text="46" Value="46"/>
    <asp:ListItem Text="47" Value="47"/>
    <asp:ListItem Text="48" Value="48"/>
    <asp:ListItem Text="49" Value="49"/>
    <asp:ListItem Text="50" Value="50"/>
    <asp:ListItem Text="51" Value="51"/>
    <asp:ListItem Text="52" Value="52"/>
    </asp:DropDownList>
    
    <asp:DropDownList enabled="false" ID="DropDownListCharactorMonth" style="position:absolute; left: 554px; top: 28px;" runat="server" Font-Names="Arial" Font-Size="12px" Width="88px" Visible="False">
            <asp:ListItem Text="January" Value="1"/>
            <asp:ListItem Text="February" Value="2"/>
            <asp:ListItem Text="March" Value="3"/>
            <asp:ListItem Text="April" Value="4"/>
            <asp:ListItem Text="May" Value="5"/>
            <asp:ListItem Text="June" Value="6"/>
            <asp:ListItem Text="July" Value="7"/>
            <asp:ListItem Text="August" Value="8"/>
            <asp:ListItem Text="September" Value="9"/>
            <asp:ListItem Text="October" Value="10"/>
            <asp:ListItem Text="November" Value="11"/>
            <asp:ListItem Text="December" Value="11"/>
    </asp:DropDownList>
    
    <asp:DropDownList enabled="false" ID="DropDownListMonthNumbers" style="position:absolute; left: 216px; top: 170px;" runat="server" Font-Names="Arial" Font-Size="12px" Width="45px">
        <asp:ListItem Text="1" Value="1"/>
        <asp:ListItem Text="2" Value="2"/>
        <asp:ListItem Text="3" Value="3"/>
        <asp:ListItem Text="4" Value="4"/>
        <asp:ListItem Text="5" Value="5"/>
        <asp:ListItem Text="6" Value="6"/>
        <asp:ListItem Text="7" Value="7"/>
        <asp:ListItem Text="8" Value="8"/>
        <asp:ListItem Text="9" Value="9"/>
        <asp:ListItem Text="10" Value="10"/>
        <asp:ListItem Text="11" Value="11"/>
        <asp:ListItem Text="12" Value="12"/>
 </asp:DropDownList>

 <asp:DropDownList enabled="false" ID="DropDownListWeekDayName" style="position:absolute; left: 125px; top: 109px;" runat="server" Font-Names="Arial" Font-Size="12px" Width="103px">
            <asp:ListItem Text="Monday" Value="1"/>
            <asp:ListItem Text="Tuesday" Value="2"/>
            <asp:ListItem Text="Wednesday" Value="3"/>
            <asp:ListItem Text="Thursday" Value="4"/>
            <asp:ListItem Text="Friday" Value="5"/>
            <asp:ListItem Text="Saturday" Value="6"/>
            <asp:ListItem Text="Sunday" Value="7"/>
 </asp:DropDownList>


<asp:Label ID="Label13" runat="server" Font-Names="arial" Font-Size="12px" Height="19px"  Style="left: 13px; position: absolute; top: 109px" Text="Read every" Width="103px"></asp:Label>
<asp:Label ID="Label14" runat="server" Font-Names="arial" Font-Size="12px" Height="19px"  Style="left: 239px; position: absolute; top: 109px" Text="Starting from" Width="79px"></asp:Label>
<asp:Label ID="Label15" runat="server" Font-Names="arial" Font-Size="12px" Height="19px"  Style="left: 124px; position: absolute; top: 141px" Text="Starting Week"   Width="89px"></asp:Label>
<asp:Label ID="Label16" runat="server" Font-Names="arial" Font-Size="12px" Height="19px"  Style="left: 265px; position: absolute; top: 141px" Text="Starting Year" Width="81px"></asp:Label>
<asp:Label ID="Label17" runat="server" Font-Names="arial" Font-Size="12px" Height="19px"  Style="left: 267px; position: absolute; top: 171px" Text="Starting Year" Width="81px"></asp:Label>
<asp:Label ID="Label18" runat="server" Font-Names="arial" Font-Size="12px" Height="19px"  Style="left: 123px; position: absolute; top: 172px" Text="Starting Month"   Width="87px"></asp:Label>
<asp:Label ID="Label19" runat="server" Font-Names="arial" Font-Size="12px" Height="19px" Style="left: 126px; position: absolute; top: 79px" Text="Starting from" Width="79px"></asp:Label>
<asp:Label ID="Label26" runat="server" Font-Names="arial" Font-Size="12px" Height="19px"  Style="left: 14px; position: absolute; top: 141px" Text="Week number" Width="100px"></asp:Label>
<asp:Label ID="Label28" runat="server" Font-Names="arial" Font-Size="12px" Height="19px"  Style="left: 13px; position: absolute; top: 78px" Text="Each day"  Width="105px"></asp:Label>

  </asp:Panel>
   
  
   
<asp:Panel ID="PanelPureMeters" style="position:absolute;left: 988px; top: 398px;" 
        Width="650px" height="400px" visible="false"   runat="server" 
        BorderStyle="None" ScrollBars="none" BackColor="#FFFFC0">
<asp:Label ID="Label5" style="position:absolute;top: 71px; left: 9px;" runat="server"   Font-Names="Arial"  Font-Size="12px"  Width="100px" Text="Meter name" ></asp:Label>
<asp:TextBox ID="txtMeterName" style="position:absolute;top: 70px; left: 118px;"  maxlength="100"  runat="server"    Font-Names="Arial"  Font-Size="12px"  Width="357px" ></asp:TextBox>

<asp:Label ID="Label7" runat="server"  style="position:absolute;top: 100px; left: 8px;"  Font-Names="Arial"  Font-Size="12px"  width="100px" Text="Location" ></asp:Label>
<asp:TextBox ID="txtMeterLocation" style="position:absolute;top: 100px; left: 118px;"  maxlength="100"  Font-Names="Arial"  Font-Size="12px"  runat="server" Width="357px"   ></asp:TextBox>

<asp:Label ID="Label6" runat="server" style="position:absolute;top: 127px; left: 10px;"  Font-Names="Arial"  Font-Size="12px"  width="100px"  Text="Template" ></asp:Label>
<asp:DropDownList ID="DropDownList3"  style="position:absolute;top: 129px; left: 119px;"  Font-Names="Arial"  Font-Size="12px"  Width="361px" BackColor="#ffffff" ForeColor="Black" runat="server" DataSourceID="SQL_Templates" DataTextField="TemplateDescription" DataValueField="ID">
</asp:DropDownList>

<asp:Label ID="Label20" runat="server"   style="position:absolute;top: 186px; left: 10px;"  Font-Names="Arial"  Font-Size="12px"  Width="100px" Text="Identifier" ></asp:Label>
<asp:TextBox ID="txtIdentifier" runat="server"    style="position:absolute;top: 184px; left: 120px;"   maxlength="25" Font-Names="Arial"  Font-Size="12px"  Width="100px" ></asp:TextBox>

<asp:Label ID="Label27" runat="server"   style="position:absolute;top: 156px; left: 10px;"  Font-Names="Arial"  Font-Size="12px"  Text="Type of meter" Width="96px" ></asp:Label>&nbsp;
<asp:DropDownList ID="DropDownTypeOf"  style="position:absolute;top: 157px; left: 119px;"  Font-Names="Arial"  Font-Size="12px"  Width="165px" BackColor="#ffffff" ForeColor="black" runat="server">
</asp:DropDownList>
    
  <asp:Label ID="Label1" runat="server" Font-Names="Arial" Font-Size="12px" Style="left: 10px;    position: absolute; top: 43px" Text="Site" Width="96px"></asp:Label>
    
  <asp:DropDownList ID="DropDownListSiteLevel"  DataSourceID="SQL_UserSitesCreateMeter" DataTextField="Value" DataValueField="ID" style="position:absolute;top: 42px; left: 118px;"  Font-Names="Arial"  Font-Size="12px"  Width="362px" BackColor="#ffffff" ForeColor="black" runat="server">
  </asp:DropDownList>
      
         
  <asp:DropDownList ID="DropDownListUseage"  style="position:absolute;top: 209px; left: 120px;"  Font-Names="Arial"  Font-Size="12px"  Width="111px" BackColor="#ffffff" ForeColor="black" runat="server">
  </asp:DropDownList>

     <asp:DropDownList ID="DropDownListLoging"  style="position:absolute;top: 238px; left: 119px;"  Font-Names="Arial"  Font-Size="12px"  Width="111px" BackColor="#ffffff" ForeColor="black" runat="server">
     </asp:DropDownList>

 <asp:Label ID="Label21" runat="server"   style="position:absolute;top: 266px; left: 10px;"  Font-Names="Arial"  Font-Size="12px"  Width="100px" Text="Installed date" ></asp:Label>
 <asp:TextBox ID="txtInstalleddate" style="position:absolute;top: 264px; left: 120px;"  runat="server"     Font-Names="Arial"  Font-Size="12px"  Width="74px" ></asp:TextBox>

 <asp:Label ID="Label22" runat="server"  style="position:absolute;top: 292px; left: 10px;"  Font-Names="Arial"  Font-Size="12px"  Width="100px" Text="Last serviced" ></asp:Label>
<asp:TextBox ID="txtLastserviced"  style="position:absolute;top: 290px; left: 120px;"  runat="server"    Font-Names="Arial"  Font-Size="12px"  Width="74px" ></asp:TextBox>
 
<asp:ImageButton ID="ImageButtonMeters_Add"  style="position:absolute;top: 349px; left: 537px;"  visible="true"  tooltip="Append details to database"  Height="20px" Width="100px"  ImageUrl="~/assets/button_save.gif" runat="server" />
     <asp:Label ID="Label2" runat="server" Font-Names="Arial" Font-Size="12px" Style="left: 10px;  position: absolute; top: 212px" Text="Useage" Width="100px"></asp:Label>
     <asp:Label ID="Label29" runat="server" Font-Names="Arial" Font-Size="12px" Style="left: 11px;    position: absolute; top: 239px" Text="Logging" Width="100px"></asp:Label>
 
     <asp:Label ID="errorlabel3" runat="server" style="position:absolute; left: 13px; top: 315px;" BorderStyle="None" Font-Names="Arial" Font-Size="11px" ForeColor="Red" Width="654px"></asp:Label>
  
  </asp:Panel> 
    
    
    <asp:SqlDataSource ID="SQL_MeterSelect" runat="server"   ConnectionString="<%$ ConnectionStrings:portal.utilitymasters.co.ukConnectionString %>" 
               SelectCommand="exec spCustomer_MeterTemplateUnit 0">
        <DeleteParameters>
            <asp:Parameter Name="original_id" Type="Int32" />
            <asp:Parameter Name="original_CompanyID" Type="Int32" />
            <asp:Parameter Name="original_MeterTypeID" Type="Int32" />
            <asp:Parameter Name="original_MeterName" Type="String" />
            <asp:Parameter Name="original_MeterDepartment" Type="String" />
            <asp:Parameter Name="original_MeterIdentifier" Type="String" />
            <asp:Parameter DbType="DateTime" Name="original_Installed" />
            <asp:Parameter DbType="DateTime" Name="original_LastServiced" />
            <asp:Parameter Name="original_Fitter" Type="String" />
            <asp:Parameter Name="original_TypeOf" Type="String" />
        </DeleteParameters>
        <UpdateParameters>
            <asp:Parameter Name="CompanyID" Type="Int32" />
            <asp:Parameter Name="MeterTypeID" Type="Int32" />
            <asp:Parameter Name="MeterName" Type="String" />
            <asp:Parameter Name="MeterDepartment" Type="String" />
            <asp:Parameter Name="MeterIdentifier" Type="String" />
            <asp:Parameter DbType="DateTime" Name="Installed" />
            <asp:Parameter DbType="DateTime" Name="LastServiced" />
            <asp:Parameter Name="Fitter" Type="String" />
            <asp:Parameter Name="TypeOf" Type="String" />
            <asp:Parameter Name="original_id" Type="Int32" />
            <asp:Parameter Name="original_CompanyID" Type="Int32" />
            <asp:Parameter Name="original_MeterTypeID" Type="Int32" />
            <asp:Parameter Name="original_MeterName" Type="String" />
            <asp:Parameter Name="original_MeterDepartment" Type="String" />
            <asp:Parameter Name="original_MeterIdentifier" Type="String" />
            <asp:Parameter DbType="DateTime" Name="original_Installed" />
            <asp:Parameter DbType="DateTime" Name="original_LastServiced" />
            <asp:Parameter Name="original_Fitter" Type="String" />
            <asp:Parameter Name="original_TypeOf" Type="String" />
        </UpdateParameters>
        <InsertParameters>
            <asp:Parameter Name="CompanyID" Type="Int32" />
            <asp:Parameter Name="MeterTypeID" Type="Int32" />
            <asp:Parameter Name="MeterName" Type="String" />
            <asp:Parameter Name="MeterDepartment" Type="String" />
            <asp:Parameter Name="MeterIdentifier" Type="String" />
            <asp:Parameter DbType="DateTime" Name="Installed" />
            <asp:Parameter DbType="DateTime" Name="LastServiced" />
            <asp:Parameter Name="Fitter" Type="String" />
            <asp:Parameter Name="TypeOf" Type="String" />
        </InsertParameters>
</asp:SqlDataSource>
    
    
<asp:SqlDataSource ID="SQL_MeterReadingsGroupYEAR" runat="server" 
ConnectionString="<%$ ConnectionStrings:portal.utilitymasters.co.ukConnectionString %>" 
SelectCommand="EXEC dbo.spCustomer_MeterReadingsYEAR 0"></asp:SqlDataSource>


        <asp:SqlDataSource ID="SQL_MeterReadings" runat="server" ConnectionString="<%$ ConnectionStrings:portal.utilitymasters.co.ukConnectionString %>" 
        SelectCommand="EXEC spCustomer_MeterReadings '1980',0">
        </asp:SqlDataSource>
            
    
<asp:SqlDataSource ID="SQL_Chart" runat="server" 
ConnectionString="<%$ ConnectionStrings:portal.utilitymasters.co.ukConnectionString %>" 
SelectCommand="select Figure,Recordeddate from tblCustomer_ScheduledMeters where CompanyID=0 and meterId=0">
</asp:SqlDataSource>     


<asp:SqlDataSource ID="SQLPersonnel" runat="server" 
ConnectionString="<%$ ConnectionStrings:portal.utilitymasters.co.ukConnectionString %>" 
SelectCommand="select 1-1">
</asp:SqlDataSource>

<asp:SqlDataSource ID="SQLPersonnelTwo" runat="server" 
ConnectionString="<%$ ConnectionStrings:portal.utilitymasters.co.ukConnectionString %>" 
SelectCommand="select 1-1">
</asp:SqlDataSource>

<asp:SqlDataSource ID="SQL_MeterGeneral" runat="server" ConnectionString="<%$ ConnectionStrings:portal.utilitymasters.co.ukConnectionString %>" 
SelectCommand="Select 1-1">
</asp:SqlDataSource>    

<asp:SqlDataSource ID="SQL_Templates" runat="server" ConnectionString="<%$ ConnectionStrings:portal.utilitymasters.co.ukConnectionString %>" 
SelectCommand=" SELECT * FROM vwCustomer_TemplatesAndMeasurementsCombined">
</asp:SqlDataSource>

<%--<asp:SqlDataSource ID="SQL_UserSites" runat="server" ConnectionString="<%$ ConnectionStrings:portal.utilitymasters.co.ukConnectionString %>" 
SelectCommand=" SELECT 1-1">
</asp:SqlDataSource>--%>

<asp:SqlDataSource ID="SQL_UserSitesCreateMeter" runat="server" ConnectionString="<%$ ConnectionStrings:portal.utilitymasters.co.ukConnectionString %>" 
SelectCommand=" SELECT 1-1">
</asp:SqlDataSource>
 
<asp:HiddenField ID="HiddenFieldCurrentUser" runat="server" />
<asp:HiddenField ID="HiddenFieldSiteID" runat="server" />
<asp:HiddenField ID="HiddenFieldStartDate" runat="server" />
<asp:HiddenField ID="HiddenFieldFinishDate" runat="server" />
<asp:HiddenField ID="HiddenFieldNextReadingDate" runat="server" />
<asp:HiddenField ID="HiddenFieldScheduleType" runat="server" />
<asp:HiddenField ID="HiddenFieldEnterNewReading" runat="server" />
<asp:HiddenField ID="HiddenFieldCalendar3" runat="server" />
<asp:HiddenField ID="HiddenFieldMeterType" runat="server" />
<asp:HiddenField ID="HiddenTemplate" runat="server" />
<asp:HiddenField ID="hiddenCompanyID" runat="server" />
<asp:HiddenField ID="hiddenMeterID" runat="server" />
<asp:HiddenField ID="HiddenScheduledMeter" runat="server" />
    
<%--    E N D --%>
    
    </div>
    </form>
 
 
</asp:Content>