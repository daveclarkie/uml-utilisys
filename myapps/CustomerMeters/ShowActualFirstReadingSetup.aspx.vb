﻿Imports System
Imports ADODB
Imports System.Collections
Imports System.ComponentModel
Imports System.Drawing
Imports System.Web
Imports Dundas.Charting.WebControl
Imports Dundas.Charting.WebControl.Utilities
Imports System.Web.Security
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.SessionState
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports System.Data
Imports System.Data.OleDb
Partial Class myapps_CustomerMeters_ShowActualFirstReadingSetup
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.labelFirstmeterRead.Text = Session("MeterName")

        If Request.QueryString("MeterID") <> "" Then
            Dim strSQL As String = ""
            strSQL = "SELECT  DATENAME(week,GETDATE()) as [WK] , MONTH(GETDATE()) as [M]  ,   'Current Day ' + '(' +  DATENAME(dw,GETDATE())   + ' )'  + ' Current Week (' + DATENAME(week,GETDATE())   + ') ' + ' Current Month (' + CONVERT(VARCHAR(2),MONTH(GETDATE()) ) + ')'  AS  [TITLE]    ,   ScheduleType from dbo.tblCustomer_Meters where ID=" & Request.QueryString("MeterID")

            Me.SQL_MeterGeneral.SelectCommand = strSQL
            Dim myTwo As System.Data.DataView = CType(SQL_MeterGeneral.Select(DataSourceSelectArguments.Empty), System.Data.DataView)

            Me.nDailyDate.Enabled = "False"
            Me.DropDownListMonthNumbers.Enabled = "False"
            Me.DropDownListWeekDayName.Enabled = "False"
            Me.DropDownListWeekNumbers.Enabled = "False"

            For Each rowProduct As System.Data.DataRowView In myTwo
                Me.Current.Text = Trim(rowProduct("Title"))
                Me.DropDownListWeekNumbers.SelectedIndex = (Trim(rowProduct("WK")) - 1)
                Me.DropDownListMonthNumbers.SelectedIndex = (Trim(rowProduct("M")) - 1)

                Select Case Trim(rowProduct("ScheduleType"))
                    Case "D"
                        Me.nDailyDate.Enabled = "True"
                        Me.ntxtEachdayStart.Enabled = "True"
                    Case "M"
                        Me.DropDownListMonthNumbers.Enabled = "True"
                        Me.nMonthlyYear.Enabled = "True"
                    Case "W"
                        Me.DropDownListWeekDayName.Enabled = "True"
                        Me.ntxtEveryWeekStart.Enabled = "True"
                    Case "WK"
                        Me.DropDownListWeekNumbers.Enabled = "True"
                        Me.nWeeklyYear.Enabled = "True"
                End Select
                Exit For
            Next
        End If

    End Sub

    Protected Sub ImageButtonFirstReadingsClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonFirstReadingsClose.Click
        Response.Redirect("ShowActualMeterReadings.aspx?meterid=" & Session("MeterID"))

    End Sub

    Protected Sub ImageButtonFirstReadingsUpdate_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonFirstReadingsUpdate.Click
        Dim strSQL As String = ""
        Dim istrSQL As String = ""
        Dim iSelection As String = ""
        Dim TheSiteIDis As String = ReturnSiteID(Session("MeterID"))
        Dim TheCustIDIS As String = Session("CustID")
        Dim TheCount As Integer = 0
        Dim myConnectionString As String = "Provider=SQLNCLI.1;Integrated Security=SSPI;Persist Security Info=False;Initial Catalog=portal.utilitymasters.co.uk;Data Source=uk-ed0-sqlcl-01.MCEG.local"
        Dim ystrSQL As String = ""

        ''SET DATEFORMAT dmy
        'Dim rs As New ADODBConnection
        'Dim cProc As New ADODB.Command
        'Dim rSET As New ADODB.Recordset
        'Dim iSQL As String = ""


        'rs.OpenConnection()
        '' rs.OpenRecordSet_StoredProcedure("EXEC ReturnDateListFromToday '01/07/2010'")

        'With cProc
        '    .ActiveConnection = rs.dComm
        '    .CommandType = ADODB.CommandTypeEnum.adCmdStoredProc
        '    .CommandText = "ReturnDateListFromToday"
        '    .Parameters.Append(.CreateParameter("@StartDate", ADODB.DataTypeEnum.adDBDate, ADODB.ParameterDirectionEnum.adParamInput, , "01/07/2010"))
        '    .Execute()
        'End With

        'With rSET
        '    .ActiveConnection = cProc.ActiveConnection

        'End With



        'rSET.Open(cProc, , ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic)
        'rSET.Open(cProc, cProc.ActiveConnection, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic)
        'MsgBox(cProc.State.ToString)

        '' MsgBox(rSET.RecordCount)
        'Do While Not rSET.EOF
        '    MsgBox(rSET.Fields("StartDate"))
        '    rSET.MoveNext()
        'Loop

        '  intReturn = cmd.Parameters(0).Value
        ' intOutput = cmd.Parameters("@Output").Value


        If Val(TheSiteIDis) < 1 Then
            Me.errorlabel5.Text = "Status: " & "Please select a Valid Site"
            Exit Sub
        End If

        If Me.nDailyDate.Enabled = True Then
            If Trim(Me.nDailyDate.Text) = "" Or IsDate(Me.nDailyDate.Text) = False Then
                Me.errorlabel5.Text = "Status: " & "Please enter a valid Start date"
                Me.nDailyDate.Focus()
                Exit Sub
            End If
            strSQL = "  EXEC ReturnDateListFromToday " & "'" & CDate(Trim(Me.nDailyDate.Text)) & "'"
            iSelection = "D"
        End If

        If Me.DropDownListWeekDayName.Enabled = True Then
            If Trim(Me.ntxtEveryWeekStart.Text) = "" Then
                Me.errorlabel5.Text = "Status: " & "Please enter a valid Start Date"
                Me.ntxtEveryWeekStart.Focus()
                Exit Sub
            End If
            strSQL = "  EXEC dbo.ReturnWeekNumbers_YearAndWeekNumber " & Year(Me.ntxtEveryWeekStart.Text) & "," & Right("00" & GetWeekNumber(Me.ntxtEveryWeekStart.Text), 2) & "," & Me.DropDownListWeekDayName.SelectedValue
            istrSQL = ""
            istrSQL = istrSQL & " UPDATE tblCustomer_Meters SET ReadingMethod=" & Me.DropDownListWeekDayName.SelectedValue & "  WHERE ID=" & Session("MeterID")

            Me.SQL_MeterReadings.UpdateCommand = istrSQL
            Me.SQL_MeterReadings.Update()

            iSelection = "W"
        End If

        If Me.DropDownListWeekNumbers.Enabled = True Then
            If Trim(Me.nWeeklyYear.Text) = "" Then
                Me.errorlabel5.Text = "Status: " & "Please enter a valid Start Year"
                Me.nWeeklyYear.Focus()
                Exit Sub
            End If

            strSQL = "  EXEC dbo.ReturnWeekNumbers_YearAndWeekNumber " & Trim(Me.nWeeklyYear.Text) & "," & Right("00" & Me.DropDownListWeekNumbers.SelectedValue, 2) & ",0"
            iSelection = "WK"
        End If

        If Me.DropDownListMonthNumbers.Enabled = True Then
            If Trim(Me.nMonthlyYear.Text) = "" Then
                Me.errorlabel5.Text = "Status: " & "Please enter a valid Start Month"
                Me.nMonthlyYear.Focus()
                Exit Sub
            End If
            iSelection = "M"
            strSQL = " EXEC dbo.ReturnMonthNumbers " & "'01/" & Right("00" & Me.DropDownListMonthNumbers.SelectedValue, 2) & "/" & Trim(Me.nMonthlyYear.Text) & "'" & ",0"
        End If


        Me.SQL_MeterGeneral.SelectCommand = strSQL
        Dim ivwExpensiveItems As System.Data.DataView = CType(SQL_MeterGeneral.Select(DataSourceSelectArguments.Empty), System.Data.DataView)

        If ivwExpensiveItems.Count > 0 Then  ' not found
            For Each rowProduct As System.Data.DataRowView In ivwExpensiveItems
                Select Case iSelection
                    Case "D"
                    Case "W"
                    Case "WK"
                    Case "M"
                End Select

                istrSQL = "  INSERT INTO tblCustomer_ScheduledMeters (CompanyID,MeterID,Figure,ConfirmRead,RecordedDate,PersonnelID,TagNote,WeekNumber,MonthNumber,SiteID) "
                istrSQL = istrSQL & " Values("

                'ystrSQL = "exec spCustomer_FindCompany_WithGiven_SiteID " & TheSiteIDis ' me.DropDownListSiteFilter.SelectedValue

                'Dim mySelectQuery As String = ystrSQL
                'Dim myConnection As New OleDbConnection(myConnectionString)
                'Dim myCommand As New OleDbCommand(mySelectQuery, myConnection)
                'myCommand.Connection.Open()
                'Dim myReader As OleDbDataReader = myCommand.ExecuteReader(CommandBehavior.CloseConnection)
                'myReader.Read()

                ' Dim nReturn As String = ""
                'Dim rs As New ADODBConnection
                'Dim iSQL As String = ""

                '  iSQL = " 	select ID as 'CustID' from dbo.vwCompaniesSites where [Site ID]=" & TheSiteIDis
                '  rs.OpenConnection()
                ' rs.OpenRecordSet(iSQL)
                istrSQL = istrSQL & Session("CustomerIDis") & ","
                ' rs.CloseConnection()

                istrSQL = istrSQL & Session("MeterID") & ","
                istrSQL = istrSQL & "'" & "" & "'" & ","
                istrSQL = istrSQL & "'" & "Y" & "'" & ","

                If iSelection = "W" Then
                    istrSQL = istrSQL & "'" & rowProduct("TheDate") & "'" & ","
                End If

                If iSelection = "D" Then
                    istrSQL = istrSQL & "'" & rowProduct("StartDate") & "'" & ","
                End If

                If iSelection = "WK" Then
                    istrSQL = istrSQL & "'" & rowProduct("StartDate") & "'" & ","
                End If

                If iSelection = "M" Then
                    istrSQL = istrSQL & "'" & rowProduct("StartDate") & "'" & ","
                End If

                istrSQL = istrSQL & "0" & ","
                istrSQL = istrSQL & "'" & "" & "'" & ","

                If iSelection = "W" Then
                    istrSQL = istrSQL & "'" & rowProduct("WeekNumber") & "'" & ","
                    istrSQL = istrSQL & "'" & "" & "'" & ","
                End If

                If iSelection = "D" Then
                    istrSQL = istrSQL & "'" & "" & "'" & ","
                    istrSQL = istrSQL & "'" & "" & "'" & ","
                End If

                If iSelection = "WK" Then
                    istrSQL = istrSQL & "'" & rowProduct("WeekNumber") & "'" & ","
                    istrSQL = istrSQL & "'" & "" & "'" & ","
                End If

                If iSelection = "M" Then
                    istrSQL = istrSQL & "'" & "" & "'" & ","
                    istrSQL = istrSQL & "'" & rowProduct("MonthNumber") & "'" & ","
                End If

                istrSQL = istrSQL & "'" & TheSiteIDis & "'"
                istrSQL = istrSQL & ")"

                Me.SQL_MeterReadings.InsertCommand = istrSQL
                Me.SQL_MeterReadings.Insert()
            Next

            Response.Redirect("ShowActualMeterReadings.aspx?meterid=" & Session("MeterID"))

        End If

    End Sub


    Protected Function ReturnSiteID(ByVal ID As Integer) As String
        Dim nReturn As String = ""
        Dim rs As New ADODBConnection
        Dim strSQL As String = ""
        strSQL = " SELECT siteId FROM tblCustomer_Meters WHERE ID =" & ID

        rs.OpenConnection()
        rs.OpenRecordSet(strSQL)
        nReturn = rs.rComm("SiteID").Value
        rs.CloseConnection()

        ReturnSiteID = nReturn

    End Function

    '=================
    Public Function GetWeekNumber(ByVal inDate As Date) As Integer
        Dim dayOfYear As Integer, wkNumber As Integer
        Dim compensation As Integer = 0
        Dim oneDate As String
        Dim firstDayDate As Date
        dayOfYear = inDate.DayOfYear
        oneDate = "1/1/" & inDate.Year.ToString
        firstDayDate = DateAndTime.DateValue(oneDate)
        Select Case firstDayDate.DayOfWeek
            Case DayOfWeek.Sunday
                compensation = 0
            Case DayOfWeek.Monday
                compensation = 6
            Case DayOfWeek.Tuesday
                compensation = 5
            Case DayOfWeek.Wednesday
                compensation = 4
            Case DayOfWeek.Thursday
                compensation = 3
            Case DayOfWeek.Friday
                compensation = 2
            Case DayOfWeek.Saturday
                compensation = 1
        End Select
        dayOfYear = dayOfYear - compensation
        If dayOfYear Mod 7 = 0 Then
            wkNumber = dayOfYear / 7
        Else
            wkNumber = (dayOfYear \ 7) + 1 'WATCH THE OPERATOR \ !!! IT RETURNS THE INTEGER RESULT
        End If
        Return wkNumber
    End Function

    Protected Function GetTheWeekName(ByVal what As String) As String
        Dim nReturn As String = ""
        Select Case what
            Case "1" : nReturn = "Monday"
            Case "2" : nReturn = "Tuesday"
            Case "3" : nReturn = "Wednesday"
            Case "4" : nReturn = "Thursday"
            Case "5" : nReturn = "Friday"
            Case "6" : nReturn = "Saturday"
            Case "7" : nReturn = "Sunday"
        End Select

        GetTheWeekName = nReturn
    End Function

    Protected Function GetTheMonthName(ByVal what As String) As String
        Dim nReturn As String = ""
        Select Case what
            Case "1" : nReturn = "January"
            Case "2" : nReturn = "February"
            Case "3" : nReturn = "March"
            Case "4" : nReturn = "April"
            Case "5" : nReturn = "May"
            Case "6" : nReturn = "June"
            Case "7" : nReturn = "July"
            Case "8" : nReturn = "August"
            Case "9" : nReturn = "September"
            Case "10" : nReturn = "October"
            Case "11" : nReturn = "November"
            Case "12" : nReturn = "December"
        End Select

        GetTheMonthName = nReturn
    End Function

    Protected Function GetTheNameMonth(ByVal what As String) As String
        Dim nReturn As String = ""
        Select Case what
            Case "January" : nReturn = "01"
            Case "February" : nReturn = "02"
            Case "March" : nReturn = "03"
            Case "April" : nReturn = "04"
            Case "May" : nReturn = "05"
            Case "June" : nReturn = "06"
            Case "July" : nReturn = "07"
            Case "August" : nReturn = "08"
            Case "September" : nReturn = "09"
            Case "October" : nReturn = "10"
            Case "November" : nReturn = "11"
            Case "December" : nReturn = "12"
        End Select

        GetTheNameMonth = nReturn
    End Function

    Public Function WeekOfMonth(ByVal dteInputDate As Date, ByVal intStartWeekday As Integer) As Integer
        Dim intWeekdayOf1st As Integer
        Dim intDaysInWeek1 As Integer
        Dim intDaysPastWeek1 As Integer
        Dim iReturn As Double

        intWeekdayOf1st = Weekday(DateSerial(Year(dteInputDate), Month(dteInputDate), 1))
        intDaysInWeek1 = (6 + intStartWeekday - intWeekdayOf1st) Mod 7 + 1
        If Day(dteInputDate) <= intDaysInWeek1 Then
            WeekOfMonth = 1
        Else
            intDaysPastWeek1 = Day(dteInputDate) - intDaysInWeek1
            ' just me being silly emmmmmmmmm but it works
            '  WeekOfMonth = 1 + (intDaysPastWeek1 \ 7) + Math.Abs(intDaysPastWeek1 Mod 7 <> 0)
            iReturn = intDaysPastWeek1 Mod 7 <> 0
            WeekOfMonth = 1 + (intDaysPastWeek1 \ 7) + Math.Abs(iReturn)
        End If
    End Function

    Protected Function WeekNumber(ByVal dteValue As Date) As Integer
        'Nicked from VB.NET rare as rocking horse *%^$
        Dim cult_info As System.Globalization.CultureInfo = System.Globalization.CultureInfo.CreateSpecificCulture("no")
        Dim cal As System.Globalization.Calendar = cult_info.Calendar
        'Dim weekNO As Integer = cal.GetWeekOfYear(dteValue, cult_info.DateTimeFormat.CalendarWeekRule, cult_info.DateTimeFormat.FirstDayOfWeek)
        WeekNumber = cal.GetWeekOfYear(dteValue, cult_info.DateTimeFormat.CalendarWeekRule, cult_info.DateTimeFormat.FirstDayOfWeek)
    End Function

    Protected Function MonthNumber(ByVal dteValue As Date) As Integer
        'Nicked from VB.NET rare as rocking horse *%^$
        Dim cult_info As System.Globalization.CultureInfo = System.Globalization.CultureInfo.CreateSpecificCulture("no")
        Dim cal As System.Globalization.Calendar = cult_info.Calendar
        'Dim weekNO1 As Integer = cal.GetMonth(dteValue)
        MonthNumber = cal.GetMonth(dteValue)
    End Function

    Protected Function AddWeekNumber(ByVal dteValue As Date) As Date
        'Nicked from VB.NET rare as rocking horse *%^$
        Dim cult_info As System.Globalization.CultureInfo = System.Globalization.CultureInfo.CreateSpecificCulture("no")
        Dim cal As System.Globalization.Calendar = cult_info.Calendar
        'Dim weekNO As Date = cal.AddWeeks(dteValue, 1)
        AddWeekNumber = cal.AddWeeks(dteValue, 1)
    End Function

    Protected Function AddMonthNumber(ByVal dteValue As Date) As Date
        'Nicked from VB.NET rare as rocking horse *%^$
        Dim cult_info As System.Globalization.CultureInfo = System.Globalization.CultureInfo.CreateSpecificCulture("no")
        Dim cal As System.Globalization.Calendar = cult_info.Calendar
        'Dim weekNO3 As Date = cal.AddMonths(dteValue, 1)
        AddMonthNumber = cal.AddMonths(dteValue, 1)
    End Function

End Class
