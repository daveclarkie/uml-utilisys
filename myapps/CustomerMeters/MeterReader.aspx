﻿<%@ Page Language="VB" MasterPageFile="~/landing.master" AutoEventWireup="false" CodeFile="MeterReader.aspx.vb" Inherits="myapps_CustomerMeters_MeterReader" title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="content_right" Runat="Server">

<style>
.general {background: transparent url('/assets/content-back-general.jpg') no-repeat; padding-top:15px; padding-left:10px;}
.generalTitle{font-family:Arial;font-size:10px;}
.generalInput {font-family:Arial;font-size:10px;}
.navgeneral {position:absolute;top:83px;left:0px;width:220px;height:500px;background: white url('/assets/left-nav-account.png') no-repeat; padding-top:0px; padding-left:0px;}

</style>

<form id="form1" Runat="Server">

<div style="position:absolute;top:83px;left:0px;width:210px;height:480px" class="navgeneral">
  <ul class="styleformytreeview" runat="server" id="navOnlineReports">  </ul> 
</div>

<div  style="position:absolute;top:83px;left:230px;width:700px;height:550px" class="general">
<asp:ImageButton ID="ImageButtonCreateTheMeters" 
        style="position:absolute;left:11px; top: 5px; height:52px; width: 101px;" 
        ImageUrl="~/assets/metering/buttons-myapps-dataentry-meterreader.gif"   
        runat="server" ImageAlign="Left"/>

 <asp:Label ID="LabTitle" style="position:absolute;top:2px; left:113px;"  
        Font-Names="Arial"   Font-Size="14px" runat="server" ForeColor="Black"  
        Font-Bold="true" Text="Meter Reader / Monitor"></asp:Label> 

 <asp:Label ID="LabCompany" style="position:absolute;top:20px; left:115px;"  
        Font-Names="Arial"   Font-Size="12px" runat="server" ForeColor="DarkBlue"  
        Font-Bold="False" Text="Company name">
</asp:Label>         

 <asp:Label ID="LabSite"    style="position:absolute;top:39px; left:116px;"  
        Font-Names="Arial"   Font-Size="11px" runat="server" ForeColor="DarkGreen"  
        Font-Bold="False" Text="Please select a valid Company OR Site">
</asp:Label>         

 <asp:ImageButton  ID="ImageButtonReturnToMenu"   visible="true"  
        style="position:absolute;left:628px; top:6px;"  height="32px"  width="32px"   
        tooltip="Return to Meters menu" runat="server"   ImageAlign="Left"  
        ImageUrl="~/assets/metering/remove_48x48.png">
</asp:ImageButton>
 
 
 
<%-- Start --%>
 <asp:Label ID="errorlabel" style="position:absolute;top:64px; left:117px; height: 12px;"  
        Font-Names="Arial"   Font-Size="11px"  ForeColor="Red"   runat="server" 
        Text="Status: " Font-Bold="False"></asp:Label>

    <asp:Panel ID="PanelGrid"   
        style="position:absolute;top:87px; left:15px; width: 641px;" 
        ScrollBars="Vertical" height="300px" runat="server">
        <asp:GridView ID="GridViewPersonnel" 
        runat="server"
        AllowPaging="false"
        AutoGenerateColumns="False" 
       CellSpacing="0" 
        CellPadding="0" 
        Gridlines="None"
        Font-Names="Arial"
        Font-Size="10px" 
        Width="624px"
        DataKeyNames="ID" 
        DataSourceID="SqlMonitors" 
         EnableModelValidation="True" 
         BackColor="Transparent">
                <Columns>
                        <asp:CommandField ShowSelectButton="True"   SelectText="Edit" >
                        <ItemStyle  Width="15px" Height="10px"/>
                        </asp:CommandField>
                        
                        <asp:BoundField DataField="ID"          HeaderText="Monitor"  ReadOnly="True"  
                            itemstyle-cssclass="hiddencol"  >
                            <ItemStyle CssClass="hiddencol" />
                        </asp:BoundField>
                        <asp:BoundField DataField="iName"  HeaderText="Title" >
                              <ItemStyle  Width="200px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="iTitle"    >
                          <ItemStyle  Width="330px" />
                        </asp:BoundField>
                </Columns>
        </asp:GridView>
    </asp:Panel>

 
<asp:Panel ID="PanelMain"  
        style="position:absolute;top:397px; left:17px; height: 195px;"  runat="server" 
        Width="628px">

<asp:ImageButton ID="ImageButtonNew" 
        style="position:absolute;top:-337px; left:-5px;" tooltip="Add a new person"   
        Height="20px"  Width="100px"   ImageUrl="~/assets/button_new.gif" 
        runat="server" Visible="False" />
<asp:ImageButton ID="ImageButtonSaveNewPerson" 
        style="position:absolute;top:4px; left:107px;" tooltip="Append to database"   
        Height="20px"  Width="100px"    ImageUrl="~/assets/button_save.gif" 
        runat="server" Visible="False" />
<asp:ImageButton ID="ImageButtonSave" style="position:absolute;top:5px;left:2px;" 
        tooltip="Save current details"  Height="20px" Width="100px"  
        ImageUrl="~/assets/button_save.gif" runat="server" Visible="False" />

<asp:ImageButton ID="ImageButtonDelete" 
        style="position:absolute;top:4px; left:409px; height: 20px;" 
        tooltip="Delete current personnel"  Width="100px"   
        ImageUrl="~/assets/button_delete.gif" runat="server" Visible="False" />
     
<asp:Panel ID="PanelEditorPerson"  style="position:absolute;top:30px;left:0px;" runat="server" BackColor="#FFFFC0" Width="624px" Visible="False">
        <asp:Label ID="Label5" runat="server"   Width="100px" Text="Monitor Name" 
            CssClass="generalTitle" ></asp:Label>
        <asp:TextBox ID="txtMonitorName" runat="server"   maxlength="50" Width="200px" 
            CssClass="generalInput"  ></asp:TextBox>
        <br />
        <asp:Label ID="Label7" runat="server"  width="100px" Text="Monitor Title" 
            CssClass="generalTitle" ></asp:Label>
        <asp:TextBox ID="txtMonitorTitle" runat="server" maxlength="50"  Width="200px" 
            CssClass="generalInput"    ></asp:TextBox>
</asp:Panel>
        
 <asp:SqlDataSource ID="SqlUniversal" runat="server"
ConnectionString="<%$ ConnectionStrings:portal.utilitymasters.co.ukConnectionString %>" 
SelectCommand="SELECT * FROM dbo.MeterReadingPersonnel  WHERE companyID=0">
</asp:SqlDataSource>       

<asp:SqlDataSource ID="SqlMonitors" runat="server"
ConnectionString="<%$ ConnectionStrings:portal.utilitymasters.co.ukConnectionString %>" 
SelectCommand="SELECT * FROM dbo.MeterReadingPersonnel  WHERE companyID=0">
</asp:SqlDataSource>

       <asp:HiddenField ID="HiddenFieldCustomerName" runat="server" />
       <asp:HiddenField ID="HiddenGridSelectID" runat="server" />
       <asp:HiddenField ID="HiddenCompanyID" runat="server" />       
       <asp:HiddenField ID="HiddenSiteID" runat="server" />    
  </asp:Panel>
<%-- End--%>

</div>
 
</form> 
</asp:Content>

