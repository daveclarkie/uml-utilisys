
Partial Class myapps_CustomerMeters_Default
    Inherits System.Web.UI.Page


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        '    
    End Sub


    Protected Function GetUserSecurity(ByVal element As Integer) As String
        Dim strSQL As String = ""
        Dim iType As String = ""
        If Not IIf(Core.data_select_value("select bitAllowMyApps from tblUsers where intUserPK = " & MySession.UserID) = "True", True, False) Then
            Response.Redirect(Core.data_select_value("select RawHomeURL from vwUsers where ID = " & MySession.UserID), True)
        End If

        If Not Page.IsPostBack = True Then
            Dim description As HtmlMeta = New HtmlMeta()
            description.Name = "description"
            description.Content = "Utility Masters - My Meters"
            Dim keywords As HtmlMeta = New HtmlMeta()
            keywords.Name = "keywords"
            keywords.Content = "My Meters"
            Page.Header.Controls.Add(description)
            Page.Header.Controls.Add(keywords)

        End If

        If Request.QueryString("custid") Is Nothing Then
            strSQL = "EXEC spPortal_TREEVIEW " & MySession.UserID
            Dim myConnection As New OleDb.OleDbConnection(Core.CurrentConnectionString)
            Dim myCommand As New OleDb.OleDbCommand(strSQL, myConnection)
            myCommand.Connection.Open()
            myCommand.CommandTimeout = 180
            Dim Reader As OleDb.OleDbDataReader = myCommand.ExecuteReader(CommandBehavior.CloseConnection)

            '=========
            If Reader.HasRows Then
                Reader.Read()
                If Reader("Type") = "Company" Then
                    ''''  Response.Redirect("default.aspx?custid=" & Reader("id") & "&method=Customer", True)
                    Session("mytype") = Reader("Type")
                    Session("myid") = Reader("Id")
                    Session("myname") = Reader("Name")
                    Session("TheCompanyID") = Reader("Id")
                    'Response.Redirect("Meters.aspx?method=Customer&custid=" & Reader("Id") & "&Customer=" & Reader("Name"), True)
                Else
                    '' Response.Redirect("default.aspx?custid=" & Reader("id") & "&method=" & Reader("Type"), True)
                    Session("mytype") = Reader("Type")
                    Session("myid") = Reader("Id")
                    Session("myname") = Reader("Name")
                    ' need to get companyid
                    Call GetCompanyID()
                    '   Response.Redirect("Meters.aspx?method=Site&custid=" & Reader("Id") & "&Site=" & Reader("Name"), True)
                End If
                Reader.Close()
            End If
            '===========
        Else
            strSQL = " EXEC spPortal_TREEVIEW " & MySession.UserID
            Dim myConnection As New OleDb.OleDbConnection(Core.CurrentConnectionString)
            Dim myCommand As New OleDb.OleDbCommand(strSQL, myConnection)
            myCommand.Connection.Open()
            myCommand.CommandTimeout = 180
            Dim Reader As OleDb.OleDbDataReader = myCommand.ExecuteReader(CommandBehavior.CloseConnection)

            If Trim(Request.QueryString("method")) = "Customer" Then
                iType = "Company"
            Else
                iType = Trim(Request.QueryString("method"))
            End If

            If Reader.HasRows Then
                While Reader.Read()
                    If Reader("id") = Trim(Request.QueryString("custid")) And Reader("Type") = iType Then
                        Session("mytype") = Reader("Type")
                        Session("myid") = Reader("Id")
                        Session("myname") = Reader("Name")
                        Exit While
                    End If
                End While
            End If

            If Session("mytype") = "Company" Then
                Session("TheCompanyID") = Session("myid")
                '  Response.Redirect("Meters.aspx?method=Customer&custid=" & Session("myid") & "&Customer=" & Session("myname"), True)
            Else
                ' need to get companyid
                Call GetCompanyID()
                '   Response.Redirect("Meters.aspx?method=Site&custid=" & Session("myid") & "&Site=" & Session("myname"), True)
            End If
        End If

        Select Case element
            Case 1
                Session("MeterApplication") = 1
            Case 2
                Session("MeterApplication") = 2
            Case 3
                Session("MeterApplication") = 3
            Case 4
                Session("MeterApplication") = 4
        End Select

        GetUserSecurity = "ok:"

    End Function

    Protected Function GetCompanyID() As String
        Dim strSQL As String = " SELECT custid FROM  [UML_CMS].dbo.tblsites WHERE siteId=" & "'" & Session("myid") & "'"
        Dim myConnection As New OleDb.OleDbConnection(Core.CurrentConnectionString)
        Dim myCommand As New OleDb.OleDbCommand(strSQL, myConnection)
        myCommand.Connection.Open()
        myCommand.CommandTimeout = 180
        Dim Reader As OleDb.OleDbDataReader = myCommand.ExecuteReader(CommandBehavior.CloseConnection)
        Reader.Read()
        Session("TheCompanyID") = Reader("custid")
        Reader.Close()
        GetCompanyID = "john"

    End Function

End Class
