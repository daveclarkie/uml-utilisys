﻿Imports System.Drawing

Partial Class myapps_CustomerMeters_MeterOutStanding
    Inherits System.Web.UI.Page
    Public Shared StayOnPage As Boolean
    Public Shared tHOLD As New ArrayList


    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Me.LabCompany.Text = Request.QueryString("Customer")

        Me.SqlDataSourceMETERS.SelectCommand = "spCustomer_Outstanding_Meternames"
        Me.SqlDataSourceMETERS.SelectParameters.Item("intCustID").DefaultValue = Request.QueryString("custid")

        Me.SqlDataSourceSITES.SelectCommand = "spCustomer_Outstanding_CustomerSites"
        Me.SqlDataSourceSITES.SelectParameters.Item("intCustID").DefaultValue = Request.QueryString("custid")

        Me.AvailableMeter.AppendDataBoundItems = True
        Me.AvailableSite.AppendDataBoundItems = True
        Me.AvailableSite.Items.Add("...")
        Me.AvailableMeter.Items.Add("...")

        Me.Panel1.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFFFC0")
        Me.Panel1.Height = 350
        Me.Panel1.Width = 650
        Me.Panel1.Visible = False

        Me.iNext.Visible = False
        Me.iPrevious.Visible = False
        Me.ClearFields.Visible = False
        Me.UpdateFields.Visible = False

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack = True Then
        Else
            Me.myPage.Text = ""
            Me.HiddenField1.Value = "Site"

        End If
    End Sub

    Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButton1.Click
        'Me.HiddenField1.Value = "Site"
        'Me.SqlDataSource4.SelectCommand = "spCustomer_Outstanding_Meters_BYSite"
        'Me.SqlDataSource4.SelectCommandType = SqlDataSourceCommandType.StoredProcedure
        'Me.SqlDataSource4.SelectParameters.Item("intCustID").DefaultValue = Request.QueryString("custid")
        'Me.SqlDataSource4.SelectParameters.Item("SiteID").DefaultValue = Me.AvailableSite.SelectedValue
        'Me.GridView2.DataSourceID = Nothing
        'Me.GridView2.DataBind()
        'Me.GridView2.PageSize = PageLister.SelectedItem.Text

        'Me.GridView2.DataSourceID = "SqlDataSource4"
        'Me.GridView2.DataBind()
        ''Me.GridView2.SelectedIndex = 0
        'Me.GridView2.PageIndex = 1

        'If GridView2.PageCount < 2 Then
        '    Me.iNext.Visible = False
        '    Me.iPrevious.Visible = False
        'Else
        '    Me.iNext.Visible = True
        '    Me.iPrevious.Visible = False
        'End If

        'If GridView2.PageCount = 0 Then
        '    Me.myPage.Text = "Page 0 " & " of  0"
        '    Me.ClearFields.Visible = False
        '    Me.UpdateFields.Visible = False
        'Else
        '    Me.myPage.Text = "Page 1 " & " of " & GridView2.PageCount
        '    Me.ClearFields.Visible = True
        '    Me.UpdateFields.Visible = True
        'End If

        'If Me.GridView2.PageIndex = 1 Then
        '    Me.ClearFields.Visible = True
        '    Me.UpdateFields.Visible = True
        'End If
    End Sub

    Protected Sub LinkButton2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButton2.Click
        'Me.HiddenField1.Value = "Meter"
        'Me.SqlDataSource5.SelectCommand = "spCustomer_Outstanding_Meters_BYMeter"
        'Me.SqlDataSource5.SelectCommandType = SqlDataSourceCommandType.StoredProcedure
        'Me.SqlDataSource5.SelectParameters.Item("intCustID").DefaultValue = Request.QueryString("custid")
        'Me.SqlDataSource5.SelectParameters.Item("MeterID").DefaultValue = Me.AvailableMeter.SelectedValue
        'Me.GridView2.DataSourceID = Nothing
        'Me.GridView2.DataBind()
        'Me.GridView2.PageSize = PageLister.SelectedItem.Text
        'Me.GridView2.DataSourceID = "SqlDataSource5"
        'Me.GridView2.DataBind()
        'Me.GridView2.PageIndex = 1

        'If GridView2.PageCount = 0 Then
        '    Me.myPage.Text = "Page 0 " & " of  0"
        'Else
        '    Me.myPage.Text = "Page 1 " & " of " & GridView2.PageCount
        'End If

        'If GridView2.PageCount < 2 Then
        '    Me.iNext.Visible = False
        '    Me.iPrevious.Visible = False
        '    Me.ClearFields.Visible = False
        '    Me.UpdateFields.Visible = False
        'Else
        '    Me.iNext.Visible = True
        '    Me.iPrevious.Visible = False
        '    Me.ClearFields.Visible = True
        '    Me.UpdateFields.Visible = True
        'End If

        'If Me.GridView2.PageIndex = 1 Then
        '    Me.ClearFields.Visible = True
        '    Me.UpdateFields.Visible = True
        'End If
    End Sub


    Protected Sub GridView2_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView2.RowDataBound
        If e.Row.RowIndex = -1 Then
        Else
            tHOLD.Add(e.Row.Cells(1).Text)
        End If
    End Sub

    Protected Sub GridView2_PageIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridView2.PageIndexChanged
        Me.myPage.Text = GridView2.PageIndex + 1 & " of " & GridView2.PageCount

    End Sub

    Protected Sub GridView2_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView2.RowCommand
        ' only clear if user accepts a new page
        If StayOnPage = True Then
        Else
            tHOLD.Clear()
        End If
    End Sub


    Protected Function iValidate() As Boolean
        Dim GotValid As Boolean = False
        Dim i As Integer = 0
        For Each gvRow As GridViewRow In GridView2.Rows
            Dim TheID As Integer = Convert.ToInt32(GridView2.DataKeys(gvRow.RowIndex).Value)
            Dim iRead As TextBox = gvRow.FindControl("Reading")
            Dim m As String = tHOLD(i)

            If IsNumeric(iRead.Text) = False Then ' Alph- only
                If Trim(iRead.Text) = "" Then ' agian still validate user might not have to current value
                    iRead.BackColor = Drawing.Color.Beige
                    iRead.ForeColor = Drawing.Color.Black
                Else
                    '  iRead.Text = ""
                    iRead.BackColor = Drawing.Color.Red
                    iRead.ForeColor = Drawing.Color.White
                    GotValid = True
                End If
            Else
                iRead.BackColor = Drawing.Color.Green
                iRead.ForeColor = Drawing.Color.Yellow
            End If
            i = i + 1
        Next

        iValidate = GotValid

    End Function


    Protected Function iChanges() As Boolean
        Dim GotChange As Boolean = False
        Dim i As Integer = 0
        For Each gvRow As GridViewRow In GridView2.Rows
            Dim TheID As Integer = Convert.ToInt32(GridView2.DataKeys(gvRow.RowIndex).Value)
            Dim iRead As TextBox = gvRow.FindControl("Reading")
            Dim m As String = tHOLD(i)

            If Trim(iRead.Text) = tHOLD(i) Then
            Else
                GotChange = True
            End If

            i = i + 1
        Next

        iChanges = GotChange

    End Function

    Protected Sub iNext_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles iNext.Click
        Dim strSQL As String = ""
        labelerror.Visible = False
        Dim nReturn As Integer = 6
        StayOnPage = False
        If iChanges() = True Then
            If iValidate() = True Then
                labelerror.Visible = True
                StayOnPage = True
                Exit Sub
            Else
            End If
            '======================
            ' physically update the  
            Dim rs As New ADODBConnection
            rs.OpenConnection()

            Dim i As Integer = 0
            For Each gvRow As GridViewRow In GridView2.Rows
                Dim TheID As Integer = Convert.ToInt32(GridView2.DataKeys(gvRow.RowIndex).Value)
                Dim iRead As TextBox = gvRow.FindControl("Reading")
                Dim m As String = tHOLD(i)

                If Trim(iRead.Text) = Trim(tHOLD(i)) Then
                Else
                    strSQL = ""
                    strSQL = strSQL & "UPDATE [portal.utilitymasters.co.uk].dbo.tblCustomer_ScheduledMeters "
                    strSQL = strSQL & " SET Figure= " & "'" & Trim(iRead.Text) & "'"
                    strSQL = strSQL & "  WHERE ID=" & TheID
                    rs.OpenRecordSet(strSQL)
                End If
            Next

            rs.CloseConnection()
            Response.Redirect(Page.Request.Url.PathAndQuery)
            '======================
        End If

        GridView2.PageIndex = GridView2.PageIndex + 1
        Me.myPage.Text = "Page " & GridView2.PageIndex & " of " & GridView2.PageCount

        If GridView2.PageIndex > 1 Then
            If GridView2.PageIndex > GridView2.PageCount Then
                GridView2.PageIndex = GridView2.PageCount
            Else
            End If

            Me.iNext.Visible = True
            Me.iPrevious.Visible = True
        Else
            Me.ClearFields.Visible = True
            Me.UpdateFields.Visible = True
        End If

    End Sub

    Protected Sub iPrevious_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles iPrevious.Click
        Dim strSQL As String
        labelerror.Visible = False
        Dim nReturn As Integer = 6
        StayOnPage = False
        If iChanges() = True Then
            If iValidate() = True Then
                labelerror.Visible = True
                StayOnPage = True
                Exit Sub
            Else
            End If
            '=============================
            ' physically update the  
            Dim rs As New ADODBConnection
            rs.OpenConnection()

            Dim i As Integer = 0
            For Each gvRow As GridViewRow In GridView2.Rows
                Dim TheID As Integer = Convert.ToInt32(GridView2.DataKeys(gvRow.RowIndex).Value)
                Dim iRead As TextBox = gvRow.FindControl("Reading")
                Dim m As String = tHOLD(i)

                If Trim(iRead.Text) = Trim(tHOLD(i)) Then
                Else
                    strSQL = ""
                    strSQL = strSQL & "UPDATE [portal.utilitymasters.co.uk].dbo.tblCustomer_ScheduledMeters "
                    strSQL = strSQL & " SET Figure= " & "'" & Trim(iRead.Text) & "'"
                    strSQL = strSQL & "  WHERE ID=" & TheID
                    rs.OpenRecordSet(strSQL)
                End If
            Next

            rs.CloseConnection()
            Response.Redirect(Page.Request.Url.PathAndQuery)
            '=============================
        Else
        End If

        If GridView2.PageIndex - 1 = 0 Then
            GridView2.PageIndex = 1
        Else
            GridView2.PageIndex = GridView2.PageIndex - 1
            Me.myPage.Text = "Page " & GridView2.PageIndex & " of " & GridView2.PageCount
        End If

        If GridView2.PageIndex > 1 Then
            Me.iNext.Visible = True
            Me.iPrevious.Visible = True
        Else
            If GridView2.PageIndex = 1 Then
                Me.iNext.Visible = True
                Me.iPrevious.Visible = False
                Me.ClearFields.Visible = True
                Me.UpdateFields.Visible = True
            Else
            End If
        End If


    End Sub

    Protected Sub UpdateFields_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles UpdateFields.Click
        Dim strSQL As String = ""
        labelerror.Visible = False
        Dim nReturn As Integer = 6
        StayOnPage = False
        If iChanges() = True Then
            If iValidate() = True Then
                labelerror.Visible = True
                StayOnPage = True
                Exit Sub
            Else
            End If

            ' physically update the  
            Dim rs As New ADODBConnection
            rs.OpenConnection()

            Dim i As Integer = 0
            For Each gvRow As GridViewRow In GridView2.Rows
                Dim TheID As Integer = Convert.ToInt32(GridView2.DataKeys(gvRow.RowIndex).Value)
                Dim iRead As TextBox = gvRow.FindControl("Reading")
                Dim m As String = tHOLD(i)

                If Trim(iRead.Text) = Trim(tHOLD(i)) Then
                Else
                    strSQL = ""
                    strSQL = strSQL & "UPDATE [portal.utilitymasters.co.uk].dbo.tblCustomer_ScheduledMeters "
                    strSQL = strSQL & " SET Figure= " & "'" & Trim(iRead.Text) & "'"
                    strSQL = strSQL & "  WHERE ID=" & TheID
                    rs.OpenRecordSet(strSQL)
                End If
            Next

            rs.CloseConnection()
            Response.Redirect(Page.Request.Url.PathAndQuery)
        Else
        End If

    End Sub

    Protected Sub ClearFields_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ClearFields.Click
        labelerror.Visible = False
        Dim i As Integer = 0
        For Each gvRow As GridViewRow In GridView2.Rows
            Dim TheID As Integer = Convert.ToInt32(GridView2.DataKeys(gvRow.RowIndex).Value)
            Dim iRead As TextBox = gvRow.FindControl("Reading")
            Dim m As String = tHOLD(i)

            If iRead.BackColor = Drawing.Color.Red Then
                iRead.Text = ""
                iRead.BackColor = Drawing.Color.White
                iRead.ForeColor = Drawing.Color.Black
            Else
            End If
        Next
    End Sub

    Protected Sub AvailableMeter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles AvailableMeter.SelectedIndexChanged
        If AvailableMeter.SelectedItem.Text = "..." Then
            Me.Panel1.Visible = False
            Exit Sub
        Else
            Me.Panel1.Visible = True
        End If

        Me.HiddenField1.Value = "Meter"
        Me.SqlDataSource5.SelectCommand = "spCustomer_Outstanding_Meters_BYMeter"
        Me.SqlDataSource5.SelectCommandType = SqlDataSourceCommandType.StoredProcedure
        Me.SqlDataSource5.SelectParameters.Item("intCustID").DefaultValue = Request.QueryString("custid")
        Me.SqlDataSource5.SelectParameters.Item("Order").DefaultValue = Me.OrderSequency.SelectedValue

        Me.SqlDataSource5.SelectParameters.Item("MeterID").DefaultValue = Me.AvailableMeter.SelectedValue
        Me.GridView2.DataSourceID = Nothing
        Me.GridView2.DataBind()

        Me.GridView2.PageSize = PageLister.SelectedItem.Text

        Me.GridView2.DataSourceID = "SqlDataSource5"
        Me.GridView2.DataBind()
        Me.GridView2.PageIndex = 1

        If GridView2.PageCount = 0 Then
            Me.myPage.Text = "Page 0 " & " of  0"
        Else
            Me.myPage.Text = "Page 1 " & " of " & GridView2.PageCount
        End If

        If GridView2.PageCount < 2 Then
            Me.iNext.Visible = False
            Me.iPrevious.Visible = False
            Me.ClearFields.Visible = False
            Me.UpdateFields.Visible = False
        Else
            Me.iNext.Visible = True
            Me.iPrevious.Visible = False
            Me.ClearFields.Visible = True
            Me.UpdateFields.Visible = True
        End If

        If Me.GridView2.PageIndex = 1 Then
            Me.ClearFields.Visible = True
            Me.UpdateFields.Visible = True
        End If

        labelerror.Visible = False

    End Sub

    Protected Sub AvailableSite_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles AvailableSite.SelectedIndexChanged
        If AvailableSite.SelectedItem.Text = "..." Then
            Me.Panel1.Visible = False
            Exit Sub
        Else
            Me.Panel1.Visible = True
        End If


        Me.HiddenField1.Value = "Site"
        Me.SqlDataSource4.SelectCommand = "spCustomer_Outstanding_Meters_BYSite"
        Me.SqlDataSource4.SelectCommandType = SqlDataSourceCommandType.StoredProcedure
        Me.SqlDataSource4.SelectParameters.Item("intCustID").DefaultValue = Request.QueryString("custid")
        Me.SqlDataSource4.SelectParameters.Item("SiteID").DefaultValue = Me.AvailableSite.SelectedValue
        Me.SqlDataSource4.SelectParameters.Item("Order").DefaultValue = Me.OrderSequency.SelectedValue

        Me.GridView2.DataSourceID = Nothing
        Me.GridView2.DataBind()


        Me.GridView2.PageSize = PageLister.SelectedItem.Text

        Me.GridView2.DataSourceID = "SqlDataSource4"
        Me.GridView2.DataBind()
        'Me.GridView2.SelectedIndex = 0
        Me.GridView2.PageIndex = 1

        If GridView2.PageCount < 2 Then
            Me.iNext.Visible = False
            Me.iPrevious.Visible = False
        Else
            Me.iNext.Visible = True
            Me.iPrevious.Visible = False
        End If

        If GridView2.PageCount = 0 Then
            Me.myPage.Text = "Page 0 " & " of  0"
            Me.ClearFields.Visible = False
            Me.UpdateFields.Visible = False
        Else
            Me.myPage.Text = "Page 1 " & " of " & GridView2.PageCount
            Me.ClearFields.Visible = True
            Me.UpdateFields.Visible = True
        End If

        If Me.GridView2.PageIndex = 1 Then
            Me.ClearFields.Visible = True
            Me.UpdateFields.Visible = True
        End If

        labelerror.Visible = False

    End Sub

    Protected Sub PageLister_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles PageLister.SelectedIndexChanged
        labelerror.Visible = False
    End Sub

    Protected Sub ImageButtonReturnToMenu_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonReturnToMenu.Click
        Dim nReturnString As String = "~/myapps/customermeters/Meters.aspx"
        nReturnString = nReturnString & "?method=Customer&custid=" & Request.QueryString("custid")
        nReturnString = nReturnString & "&Customer=" & Request.QueryString("customer")
        Response.Redirect(nReturnString)
        ' Response.Redirect("~/myapps/customermeters/Default.aspx")
    End Sub
End Class
