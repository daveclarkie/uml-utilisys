﻿Imports System
Imports System.Diagnostics
Imports System.ComponentModel
Imports System.Collections
Imports System.Drawing
Imports System.Web
Imports System.IO
Imports System.Web.Security
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.SessionState
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports System.Data
Imports System.Data.OleDb
Imports Dundas.Charting.WebControl
Imports Dundas.Charting.WebControl.Utilities
 

Partial Class myapps_CustomerMeters_ShowMeterReadings
    Inherits System.Web.UI.Page
    Dim iCurrentItemNeeded As Integer = 0
    Dim GlobalReadRequired As Boolean = False
    Dim TheCustomerID As String = ""
    Dim TheCustomerName As String = ""
    Dim TheSiteID As String = ""
    Dim TheSiteName As String = ""

    Dim iCustomer As String = ""
    Dim iMethod As String = ""
    Dim iMethodType As String = ""

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Session("SubURL") = Me.Page.Request.Url.PathAndQuery.ToString

        Call LoadTreeview_Report()

        If Request.QueryString("method") = "Site" Then
            Me.LabSite.Text = "Please select a Valid Company"
        Else
            Me.HiddenCompanyID.Value = Request.QueryString("CustID")
            Session("CustomerIDis") = Request.QueryString("custid")
        End If

        Me.PanelDownLoad.BorderStyle = BorderStyle.Solid
        Me.PanelDownLoad.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFFFC0")
        Me.PanelDownLoad.BorderWidth = 1
        Me.PanelDownLoad.BorderColor = Color.LightGray

        Me.PanelActualMeters.BorderStyle = BorderStyle.Solid
        Me.PanelActualMeters.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFFFC0")
        Me.PanelActualMeters.BorderWidth = 1
        Me.PanelActualMeters.BorderColor = Color.LightGray


        'Me.PanelEditorPerson.BorderStyle = BorderStyle.Solid
        'Me.PanelEditorPerson.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFFFC0")
        'Me.PanelEditorPerson.BorderWidth = 1
        'Me.PanelEditorPerson.BorderColor = Color.LightGray

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'If Trim(Session("DownLoadFileName")) = "" Then
        'Else
        '    File.Delete(Session("DownLoadFileName"))
        '    Session("DownLoadFileName") = ""
        'End If


        Dim tString As String = ""
        Dim rs As New ADODBConnection
        Dim strSQL As String = ""

        If Request.QueryString("Method") = "Customer" Then
            Me.HiddenCompanyID.Value = Trim(Request.QueryString("Custid"))
            strSQL = ""
            strSQL = " SELECT custid,customername FROM  [UML_CMS].dbo.tblcustomer where custid=" & Trim(Request.QueryString("Custid"))
            rs.OpenConnection()
            rs.OpenRecordSet(strSQL)
            Me.LabCompany.Text = rs.rComm.Fields("CustomerName").Value
            Session("CustomerName") = Trim(Me.LabCompany.Text)
            Me.LabSite.Text = ""
            rs.CloseConnection()

            If GatherNewDetailsForCompany() = 0 Then
                Me.RadioButtonTotalOutstanding.Visible = False
                Me.PanelActualMeters.Visible = False
            Else
                Me.PanelActualMeters.Visible = True
                Me.RadioButtonTotalOutstanding.Visible = True

            End If
            ' Me.DownLoadOustandingItems.Visible = False
        Else
            Me.LabCompany.Text = "Please select a Valid Company"
            Me.LabSite.Text = ""
            Me.PanelActualMeters.Visible = False
            Me.RadioButtonTotalOutstanding.Visible = False
        End If

    End Sub

    Private Sub LoadTreeview_Report()
        Me.navOnlineReports.InnerHtml = ""
        Dim strSQL As String = "EXEC spPortal_TREEVIEW " & MySession.UserID
        Dim myConnection As New OleDb.OleDbConnection(Core.CurrentConnectionString)
        Dim myCommand As New OleDb.OleDbCommand(strSQL, myConnection)
        myCommand.Connection.Open()
        myCommand.CommandTimeout = 180
        Dim Reader As OleDb.OleDbDataReader = myCommand.ExecuteReader(CommandBehavior.CloseConnection)

        Me.navOnlineReports.InnerHtml += "<br/>"
        Me.navOnlineReports.InnerHtml += "<br/>"
        Me.navOnlineReports.InnerHtml += "<br/>"


        While Reader.Read
            Select Case CStr(Reader(3))
                Case CStr("Company")
                    Me.navOnlineReports.InnerHtml += "<li class='styleformytreeview_company'><a href='ShowMeterReadings.aspx?custid=" & Reader(0).ToString & "&method=Customer'>" & Reader(1).ToString & "</a></li>"
                Case CStr("Header")
                    '  Me.navOnlineReports.InnerHtml += "<li class='styleformytreeview_header'>" & Reader(1).ToString & "</li>"
                Case CStr("Site")
                    '   Me.navOnlineReports.InnerHtml += "<li class='styleformytreeview_site'><a href='ShowMeterReadings.aspx?custid=" & Reader(0).ToString & "&method=Site'>" & Left(Reader(1).ToString, 23) & "..</a></li>"
                Case CStr("Group")
                    ' Me.navOnlineReports.InnerHtml += "<li class='styleformytreeview_group'><a href='ShowMeterReadings.aspx?custid=" & Reader(0).ToString & "&method=Group'>" & Reader(1).ToString & "</a></li>"
            End Select
        End While

        Me.navOnlineReports.InnerHtml += "<li class='styleformytreeview_company' style='height:10px;'></li>"
        'Me.navOnlineReports.InnerHtml += "<li class='styleformytreeview_company'><a href='javascript:history.back()'><< Back</a></li>"

        Me.navOnlineReports.InnerHtml += "<li class='styleformytreeview_company'><a href='Default.aspx'><< Back</a></li>"

    End Sub

    Protected Sub ImageButtonReturnToMenu_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonReturnToMenu.Click
        Response.Redirect("~/myapps/customermeters/Default.aspx")
    End Sub

    Protected Function XY(ByRef what As String) As String
        XY = Replace(what, "'", "''")
    End Function

    Protected Function GetCompanyID(ByVal iSiteId As Integer) As String
        On Error Resume Next
        Dim nReturn As String = ""
        Dim strSQL As String = " SELECT custid FROM  [UML_CMS].dbo.tblsites WHERE siteId=" & iSiteId
        Dim myConnection As New OleDb.OleDbConnection(Core.CurrentConnectionString)
        Dim myCommand As New OleDb.OleDbCommand(strSQL, myConnection)
        myCommand.Connection.Open()
        myCommand.CommandTimeout = 180
        Dim Reader As OleDb.OleDbDataReader = myCommand.ExecuteReader(CommandBehavior.CloseConnection)
        Reader.Read()
        nReturn = Reader("custid")
        Reader.Close()
        GetCompanyID = nReturn
    End Function

    Protected Sub RadioButtonTotalOutstanding_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RadioButtonTotalOutstanding.SelectedIndexChanged
        If Me.RadioButtonTotalOutstanding.SelectedValue = "ReadRequired" Then
            GlobalReadRequired = True
            DownLoadOustandingItems.Visible = True
        Else
            GlobalReadRequired = False
            DownLoadOustandingItems.Visible = False
        End If

        Call PushTheMeterReadButton()
    End Sub

    Protected Function PushTheMeterReadButton() As String
        If Me.HiddenCompanyID.Value = "" Then
            MsgBox("Please select a valid Company", MsgBoxStyle.Information, "Validation error")
        Else
            If Request.QueryString("method") = "Customer" Then
                If GatherNewDetailsForCompany() = 0 Then
                Else
                End If
                Me.PanelActualMeters.Visible = True
            Else
                'If GatherNewDetails() = 0 Then
                'Else
                'End If
                'Me.PanelActualMeters.Visible = True
            End If
        End If
        PushTheMeterReadButton = "OK"
    End Function

    'Protected Function GatherNewDetails() As Long
    '    Dim strSQL As String = ""
    '    Dim TrueORFalse As String = ""
    '    strSQL = "SELECT smith.john FROM ( SELECT 1-1 AS 'john')  AS [smith] WHERE [smith].john>3"
    '    Me.SQL_MeterSelect.SelectCommand = strSQL
    '    Me.SQL_MeterSelect.DataBind()
    '    If GlobalReadRequired = True Then
    '        TrueORFalse = "'Y'"
    '    Else
    '        TrueORFalse = "'N'"
    '    End If

    '    strSQL = " EXEC spCustomer_MeterTemplateUnit_Site  " & Me.HiddenCompanyID.Value & " , " & Session("myid") & "," & TrueORFalse
    '    Me.SQL_MeterSelect.SelectCommand = strSQL
    '    Me.SQL_MeterSelect.DataBind()
    '    GatherNewDetails = Me.GridView1.Rows.Count
    'End Function

    Protected Function GatherNewDetailsForCompany() As Integer
        Dim strSQL As String = ""
        Dim TrueORFalse As String = ""
        strSQL = "SELECT smith.john FROM ( SELECT 1-1 AS 'john')  AS [smith] WHERE [smith].john>3"
        Me.SQL_MeterSelect.SelectCommand = strSQL
        Me.SQL_MeterSelect.DataBind()

        If GlobalReadRequired = True Then
            TrueORFalse = "'Y'"
        Else
            TrueORFalse = "'N'"
        End If

        strSQL = "EXEC spCustomer_MeterTemplateUnit_Site_Company " & Me.HiddenCompanyID.Value & "," & TrueORFalse
        Me.SQL_MeterSelect.SelectCommand = strSQL
        Me.SQL_MeterSelect.DataBind()
        Me.GridView1.DataBind()

        ' MsgBox(Me.GridView1.Rows.Count)

        GatherNewDetailsForCompany = Me.GridView1.Rows.Count
    End Function

    Protected Sub GridView1_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView1.RowCommand
        If IsNumeric(e.CommandArgument) = True Then
            Dim iIndex As Integer = Convert.ToInt32(e.CommandArgument)
            Dim mySelectedRow As GridViewRow = GridView1.Rows(iIndex)
            Session("MainURL") = Me.Page.Request.Url.PathAndQuery.ToString  'Request.ServerVariables("URL")
            Session("MeterID") = mySelectedRow.Cells(1).Text
            Session("MeterName") = mySelectedRow.Cells(2).Text
            'Response.Redirect("ShowActualMeterDetails.aspx?meterid=" & mySelectedRow.Cells(1).Text)

            Response.Redirect("ShowActualMeterReadings.aspx?meterid=" & mySelectedRow.Cells(1).Text)

        Else
        End If
    End Sub

    Protected Sub DownLoadOustandingItems_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles DownLoadOustandingItems.Click
        Me.PanelActualMeters.Visible = False
        Me.PanelDownLoad.Visible = True

        Dim iString As String = ""
        Dim appSettings As NameValueCollection = WebConfigurationManager.AppSettings
        Dim strConn As String = appSettings.Item("DBConnect2").ToString()
        Dim ds As New DataSet()

        iString = " EXEC spCustomer_MeterTemplateUnit_Site_Company " & Trim(Request.QueryString("Custid")) & ",'Y'"
        Dim da As New OleDbDataAdapter(iString, strConn)
        da.Fill(ds)
        GridView2.DataSource = Nothing
        GridView2.DataSource = ds.Tables(0).DataSet
        GridView2.DataBind()
    End Sub

    Protected Sub ImageButtonHideDownLoad_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonHideDownLoad.Click
        Me.PanelActualMeters.Visible = True
        Me.PanelDownLoad.Visible = False
    End Sub

    'Protected Sub Selectall_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Selectall.Click
    '    LabelError.Text = ""
    '    For Each gvr As GridViewRow In GridView2.Rows
    '        Dim iRead1 As CheckBox = gvr.Cells(0).FindControl("CheckBox1")
    '        iRead1.Checked = True
    '    Next
    'End Sub

    'Protected Sub UnSelectall_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles UnSelectall.Click
    '    LabelError.Text = ""
    '    For Each gvr As GridViewRow In GridView2.Rows
    '        Dim iRead1 As CheckBox = gvr.Cells(0).FindControl("CheckBox1")
    '        iRead1.Checked = False
    '    Next
    'End Sub

    Protected Sub ButtonSaveToSpreadSheet_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonSaveToSpreadSheet.Click
        '' check the we have a valid file name
        'If FileUpload2.HasFile = True Then
        '    LabelError.Text = ""
        '    If InStr(LCase(FileUpload2.PostedFile.FileName.ToString()), ".csv") > 0 Then
        '    Else
        '        LabelError.BackColor = Color.Maroon
        '        LabelError.ForeColor = Color.White
        '        LabelError.Text = "Please select a valid CSV file"
        '        Return
        '    End If
        'Else
        '    LabelError.BackColor = Color.Maroon
        '    LabelError.ForeColor = Color.White
        '    LabelError.Text = "Please select a valid file to extract to"
        '    Return
        'End If

        ' check if any UnChecked
        Dim QuickCheck As Boolean = False
        For Each gvr As GridViewRow In GridView2.Rows
            Dim iRead1 As CheckBox = gvr.Cells(0).FindControl("CheckBox1")
            If iRead1.Checked = True Then
                QuickCheck = True
                Exit For
            End If
        Next

        If QuickCheck = False Then
            LabelError.BackColor = Color.Maroon
            LabelError.ForeColor = Color.White
            LabelError.Text = "Please select at least One Meter to Export"
        Else
            LabelError.Text = ""
            '   LabelError.BackColor = Color.Maroon
            '   LabelError.ForeColor = Color.White
        End If


        ' CreateChildControls the file from here
        Dim BigString As String = ""
        '  Dim sb As New StringBuilder()
        '        sb.Append("ID, MeterId, MeterName,value,date,Comment,Site" + Environment.NewLine)
        '   sb.Append("ID, MeterId, MeterName,value,date,Comment,Site" + vbNewLine)

        Dim rs As New ADODBConnection
        Dim strSQL As String = ""
        strSQL = " EXEC spCustomer_Outstanding_CustomerSites_Export " & Trim(Request.QueryString("Custid"))
        rs.OpenConnection()
        rs.OpenRecordSet(strSQL)
        BigString = "ID, MeterId, MeterName,value,date,Comment,Site" & vbNewLine
        Do While Not rs.rComm.EOF
            BigString = BigString & Trim(rs.rComm.Fields("ID").Value) & "," & Trim(rs.rComm.Fields("MeterID").Value) & ","
            BigString = BigString & Trim(rs.rComm.Fields("Metername").Value) & "," & Trim(rs.rComm.Fields("Value").Value) & ","
            BigString = BigString & Trim(rs.rComm.Fields("Date").Value) & "," & Trim(rs.rComm.Fields("Comment").Value) & ","
            BigString = BigString & Trim(rs.rComm.Fields("Site").Value) & vbNewLine
            rs.rComm.MoveNext()
        Loop

        rs.CloseConnection()
        '  Dim strJS As String
        '  strJS = "<script language='javascript'>window.open('printpage.aspx','Print','addressbar=no, scrollbars =no, resizable=yes,status=no, channelmode=yes, toolbar=no,top = 0, left = 0, target=_blank');</script>"
        '  Me.RegisterStartupScript("Print", strJS)
        Dim NameBuild As String = ""
        NameBuild = Server.MapPath("~/myapps/CustomerMeters/Downloads/") & Me.LabCompany.Text & "_" & Right("0000" & Year(Now), 4) & Right("00" & Month(Now), 2) & Right("00" & Day(Now), 2)
        Dim ModiName As String = NameBuild & ".csv"
        Session("DownLoadFileName") = Me.LabCompany.Text & "_" & Right("0000" & Year(Now), 4) & Right("00" & Month(Now), 2) & Right("00" & Day(Now), 2) & ".csv"
        '  Using sw As New System.IO.StreamWriter(ModiName, False)
        'sw.Write(BigString)
        'End Using
        LabelError.BackColor = Color.DarkGreen
        LabelError.ForeColor = Color.White
        LabelError.Text = "File created successfully"

        'Dim TheFIleLength As Long = FileLen(ModiName)
        'Response.Clear()
        'Response.AddHeader("Content-Disposition", "attachment; filename=" & ModiName)
        'Response.AddHeader("Content-Length", TheFIleLength)
        'Response.ContentType = "application/octet-stream"
        'Response.WriteFile(ModiName)
        'Response.End()

        'Dim strfileName As String = Me.LabCompany.Text & "_" & Right("0000" & Year(Now), 4) & Right("00" & Month(Now), 2) & Right("00" & Day(Now), 2) & ".csv"
        'Dim MyStream As New FileStream("c:\" & strfileName, FileMode.Create)
        Dim strfileName As String = ModiName
        'Dim MyStream As New FileStream("c:\" & strfileName, FileMode.Create)
        Dim MyStream As New FileStream(strfileName, FileMode.Create)


        Dim MyWriter As New System.IO.StreamWriter(MyStream)
        MyWriter.Write(BigString)
        MyWriter.Close()
        MyStream.Close()

        Response.Clear()
        Response.ContentType = "application/octet-stream"
        Response.AddHeader("Content-Disposition", "attachment; filename=" & strfileName)
        Response.Flush()
        'Response.WriteFile("c:\" & strfileName)
        Response.WriteFile(strfileName)

        Response.[End]()

    End Sub

End Class
