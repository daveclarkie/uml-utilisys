﻿Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Drawing
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.SessionState
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports System.Data
Imports System.Data.OleDb
Partial Class myapps_CustomerMeters_MeterReader
    Inherits System.Web.UI.Page

    Dim TheCustomerID As String = ""
    Dim TheCustomerName As String = ""
    Dim TheSiteID As String = ""
    Dim TheSiteName As String = ""

    Dim iCustomer As String = ""
    Dim iMethod As String = ""
    Dim iMethodType As String = ""

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Call LoadTreeview_Report()

        If Request.QueryString("method") = "Customer" Then
            Me.LabSite.Text = "Please select a Valid Site"
        Else
            Me.HiddenSiteID.Value = Request.QueryString("CustID")
            Me.HiddenCompanyID.Value = Me.GetCompanyID(Request.QueryString("CustID"))
        End If

        Me.PanelEditorPerson.BorderStyle = BorderStyle.Solid
        Me.PanelEditorPerson.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFFFC0")
        Me.PanelEditorPerson.BorderWidth = 1
        Me.PanelEditorPerson.BorderColor = Color.LightGray

        Me.PanelGrid.BorderStyle = BorderStyle.Solid
        Me.PanelGrid.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFFFC0")
        Me.PanelGrid.BorderWidth = 1
        Me.PanelGrid.BorderColor = Color.LightGray

        Me.GridViewPersonnel.BackColor = Color.Transparent

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim tString As String = ""

        If Trim(Request.QueryString("Method")) = "Site" Then
            Dim rs As New ADODBConnection
            Dim strSQL As String = ""

            Me.HiddenSiteID.Value = Request.QueryString("CustID")
            Me.HiddenCompanyID.Value = Me.GetCompanyID(Request.QueryString("CustID"))
            Me.ImageButtonNew.Visible = True

            strSQL = ""
            strSQL = strSQL & " SELECT [UML_CMS].dbo.tblsites.siteid,[UML_CMS].dbo.tblsites.sitename,[UML_CMS].dbo.tblcustomer.customername FROM  [UML_CMS].dbo.tblsites  "
            strSQL = strSQL & " INNER JOIN [UML_CMS].dbo.tblcustomer ON [UML_CMS].dbo.tblsites.custid=[UML_CMS].dbo.tblcustomer.custid where siteid=" & Me.HiddenSiteID.Value
            Me.ImageButtonNew.Visible = True
            rs.OpenConnection()
            rs.OpenRecordSet(strSQL)

            Me.LabCompany.Text = rs.rComm.Fields("CustomerName").Value
            Me.LabSite.Text = rs.rComm.Fields("SiteName").Value

            If Request.QueryString("Method") = "Site" Then
                tString = ""
                tString = tString & "  SELECT * FROM dbo.MeterReadingPersonnel  WHERE companyID=" & Me.HiddenCompanyID.Value
                tString = tString & " AND SiteId= " & Me.HiddenSiteID.Value & " ORDER BY Iname ASC"
                Me.SqlMonitors.SelectCommand = tString '"  SELECT * FROM dbo.MeterReadingPersonnel  WHERE companyID=" & Me.HiddenCompanyID.Value
            Else
            End If
            rs.CloseConnection()

        Else
            Me.LabSite.Text = "Please select a Valid Site"
        End If

        Me.errorlabel.Text = ""

    End Sub

    Private Sub LoadTreeview_Report()
        Me.navOnlineReports.InnerHtml = ""
        Dim strSQL As String = "EXEC spPortal_TREEVIEW " & MySession.UserID
        Dim myConnection As New OleDb.OleDbConnection(Core.CurrentConnectionString)
        Dim myCommand As New OleDb.OleDbCommand(strSQL, myConnection)
        myCommand.Connection.Open()
        myCommand.CommandTimeout = 180
        Dim Reader As OleDb.OleDbDataReader = myCommand.ExecuteReader(CommandBehavior.CloseConnection)

        Me.navOnlineReports.InnerHtml += "<br/>"
        Me.navOnlineReports.InnerHtml += "<br/>"
        Me.navOnlineReports.InnerHtml += "<br/>"

        While Reader.Read
            Select Case CStr(Reader(3))
                Case CStr("Company")
                    'Me.navOnlineReports.InnerHtml += "<li class='styleformytreeview_company'><a href='MeterReader.aspx?custid=" & Reader(0).ToString & "&method=Customer'>" & Reader(1).ToString & "</a></li>"
                    '  Me.navOnlineReports.InnerHtml += "<li>" & Reader(1).ToString & "</li>"
                Case CStr("Header")
                    Me.navOnlineReports.InnerHtml += "<li class='styleformytreeview_header'>" & Reader(1).ToString & "</li>"
                Case CStr("Site")
                    Me.navOnlineReports.InnerHtml += "<li class='styleformytreeview_site'><a href='MeterReader.aspx?custid=" & Reader(0).ToString & "&method=Site'>" & Left(Reader(1).ToString, 23) & "..</a></li>"
                Case CStr("Group")
                    Me.navOnlineReports.InnerHtml += "<li class='styleformytreeview_group'><a href='MeterReader.aspx?custid=" & Reader(0).ToString & "&method=Group'>" & Reader(1).ToString & "</a></li>"
            End Select
        End While

        Me.navOnlineReports.InnerHtml += "<li class='styleformytreeview_company' style='height:10px;'></li>"
        Me.navOnlineReports.InnerHtml += "<li class='styleformytreeview_company'><a href='Default.aspx'><< Back</a></li>"

    End Sub


    Protected Sub ImageButtonReturnToMenu_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonReturnToMenu.Click
        Response.Redirect("~/myapps/customermeters/Default.aspx")
    End Sub

    Protected Sub GridViewPersonnel_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridViewPersonnel.RowCommand
        Me.errorlabel.Text = ""
        On Error Resume Next
        If IsNumeric(e.CommandArgument) = True Then
            Dim iSelectedText As String
            Dim xindex As Integer = Convert.ToInt32(e.CommandArgument)
            Dim mySelectedRow As GridViewRow = GridViewPersonnel.Rows(xindex)
            Me.hiddenGridSelectID.Value = mySelectedRow.Cells(1).Text()
            Me.txtMonitorName.Text = mySelectedRow.Cells(2).Text()
            Me.txtMonitorTitle.Text = mySelectedRow.Cells(3).Text()

            Me.PanelEditorPerson.Visible = True

            Me.ImageButtonSave.Style("LEFT") = "520px"
            Me.ImageButtonSave.Visible = True

            '   Me.ImageButtonDelete.Style("LEFT") = "110px"
            Me.ImageButtonDelete.Visible = True

            Me.ImageButtonNew.Visible = True
            Me.ImageButtonSaveNewPerson.Visible = False
        Else
        End If
    End Sub

    Protected Sub ImageButtonNew_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonNew.Click
        Me.errorlabel.Text = ""
        Me.txtMonitorName.Text = "Default Name"
        Me.txtMonitorTitle.Text = "Default Title"

        Me.PanelEditorPerson.Visible = True
        Me.ImageButtonNew.Visible = False
        Me.ImageButtonSave.Visible = False
        Me.ImageButtonDelete.Visible = False

        Me.ImageButtonSaveNewPerson.Style("LEFT") = "520px"
        Me.ImageButtonSaveNewPerson.Visible = True
    End Sub

    Protected Sub ImageButtonSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonSave.Click
        Me.errorlabel.Text = ""
        Dim strSQL As String = ""
        If Trim(Me.txtMonitorName.Text) = "" Then
            Me.errorlabel.Text = "Status: Please enter a valid Name "
            'MsgBox("Please enter a valid Name", MsgBoxStyle.Critical, "Validation error")
            Me.txtMonitorName.Focus()
            Exit Sub
        End If

        If Trim(Me.txtMonitorTitle.Text) = "" Then
            Me.errorlabel.Text = "Status: Please enter a valid Title "
            'MsgBox("Please enter a valid Title", MsgBoxStyle.Critical, "Validation error")
            Me.txtMonitorTitle.Focus()
            Exit Sub
        End If

        If CheckDuplicates(Trim(Me.txtMonitorName.Text), Trim(Me.txtMonitorTitle.Text)) = True Then
            Me.errorlabel.Text = "Status: This person is already on file "
            'MsgBox("This person is already of file", MsgBoxStyle.Critical, "Validation error")
            Me.txtMonitorName.Focus()
        End If

        strSQL = ""
        strSQL = strSQL & "  Update MeterReadingPersonnel "
        strSQL = strSQL & " SET iName=" & "'" & XY(Trim(txtMonitorName.Text)) & "'"
        strSQL = strSQL & " , iTitle=" & "'" & XY(Trim(txtMonitorTitle.Text)) & "'"
        strSQL = strSQL & " Where ID=" & Me.hiddenGridSelectID.Value

        Me.SqlMonitors.UpdateCommand = strSQL
        Me.SqlMonitors.Update()
        Me.GridViewPersonnel.DataBind()

        Me.ImageButtonNew.Visible = True
        Me.PanelEditorPerson.Visible = False
        Me.ImageButtonSave.Visible = False
        Me.ImageButtonDelete.Visible = False
        Me.ImageButtonSaveNewPerson.Visible = False

    End Sub

    Protected Sub ImageButtonSaveNewPerson_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonSaveNewPerson.Click
        Me.errorlabel.Text = ""
        Dim strSQL As String = ""
        If Trim(Me.txtMonitorName.Text) = "" Then
            Me.errorlabel.Text = "Status: Please enter a valid Name"
            'MsgBox("Please enter a valid Name", MsgBoxStyle.Critical, "Validation error")
            Me.txtMonitorName.Focus()
            Exit Sub
        End If

        If Trim(Me.txtMonitorTitle.Text) = "Status: Please enter a valid Title" Then
            Me.errorlabel.Text = ""
            'MsgBox("Please enter a valid Title", MsgBoxStyle.Critical, "Validation error")
            Me.txtMonitorTitle.Focus()
            Exit Sub
        End If

        If CheckDuplicates(Trim(Me.txtMonitorName.Text), Trim(Me.txtMonitorTitle.Text)) = True Then
            Me.errorlabel.Text = "Status: This person is already on file"
            Me.txtMonitorName.Focus()
            Exit Sub
        End If

        strSQL = ""
        strSQL = strSQL & "  INSERT INTO MeterReadingPersonnel(CompanyId,iName,iTitle,SIteId)  "
        strSQL = strSQL & " Values("
        strSQL = strSQL & "'" & Me.hiddenCompanyID.Value & "'" & ","
        strSQL = strSQL & "'" & XY(Trim(Me.txtMonitorName.Text)) & "'" & ","
        strSQL = strSQL & "'" & XY(Trim(Me.txtMonitorTitle.Text)) & "'" & ","
        strSQL = strSQL & "'" & Me.HiddenSiteID.Value & "'"
        strSQL = strSQL & " )"

        Me.SqlMonitors.InsertCommand = strSQL
        Me.SqlMonitors.Insert()
        Me.GridViewPersonnel.DataBind()

        Me.txtMonitorName.Text = ""
        Me.txtMonitorTitle.Text = ""

        Me.PanelEditorPerson.Visible = False
        Me.ImageButtonSave.Visible = False
        Me.ImageButtonDelete.Visible = False
        Me.ImageButtonNew.Visible = True
        Me.ImageButtonSaveNewPerson.Visible = False
    End Sub

    Protected Sub ImageButtonDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonDelete.Click
        Me.errorlabel.Text = ""
        Dim strSQL As String = ""
        strSQL = ""
        strSQL = strSQL & "  DELETE FROM MeterReadingPersonnel "
        strSQL = strSQL & " Where ID=" & Me.hiddenGridSelectID.Value
        Me.SqlMonitors.DeleteCommand = strSQL
        Me.SqlMonitors.Delete()
        Me.GridViewPersonnel.DataBind()

        Me.ImageButtonNew.Visible = True
        Me.PanelEditorPerson.Visible = False
        Me.ImageButtonSave.Visible = False
        Me.ImageButtonDelete.Visible = False
        Me.ImageButtonSaveNewPerson.Visible = False

        Me.txtMonitorName.Text = ""
        Me.txtMonitorTitle.Text = ""
    End Sub

    Protected Function CheckDuplicates(ByVal TheName As String, ByVal TheTitle As String) As Boolean
        Me.errorlabel.Text = ""
        Dim nReturn As Boolean = False
        Dim strSQL As String = ""
        strSQL = ""
        strSQL = strSQL & "  SELECT * FROM dbo.MeterReadingPersonnel "
        strSQL = strSQL & "  WHERE iName=" & "'" & Trim(TheName) & "'"
        strSQL = strSQL & "  AND iTitle=" & "'" & Trim(TheTitle) & "'"
        strSQL = strSQL & "  AND CompanyID=" & Me.hiddenCompanyID.Value
        strSQL = strSQL & "  AND SiteID=" & Me.hiddenSiteID.Value

        Me.SqlUniversal.SelectCommand = strSQL
        Dim ivwExpensiveItems As Data.DataView = CType(SqlUniversal.Select(DataSourceSelectArguments.Empty), Data.DataView)
        If ivwExpensiveItems.Count > 0 Then
            nReturn = True
        Else
            nReturn = False
        End If

        CheckDuplicates = nReturn
    End Function

    Protected Function XY(ByRef what As String) As String
        XY = Replace(what, "'", "''")
    End Function

    Protected Function GetCompanyID(ByVal iSiteId As Integer) As String
        On Error Resume Next
        Dim nReturn As String = ""
        Dim strSQL As String = " SELECT custid FROM  [UML_CMS].dbo.tblsites WHERE siteId=" & iSiteId
        Dim myConnection As New OleDb.OleDbConnection(Core.CurrentConnectionString)
        Dim myCommand As New OleDb.OleDbCommand(strSQL, myConnection)
        myCommand.Connection.Open()
        myCommand.CommandTimeout = 180
        Dim Reader As OleDb.OleDbDataReader = myCommand.ExecuteReader(CommandBehavior.CloseConnection)
        Reader.Read()
        nReturn = Reader("custid")
        Reader.Close()
        GetCompanyID = nReturn

    End Function
     
End Class
