<%@ Page Language="VB" MasterPageFile="~/landing.master" AutoEventWireup="false" CodeFile="Default.aspx.vb" Inherits="myapps_CustomerMeters_Default" title="Data Entry Screen" %>
<asp:Content ID="Content1" ContentPlaceHolderID="content_right" Runat="Server">


<div class="content_back general">
        <ul class="box_links_reports">
            <li><a id="lnkMySites1" class="apps_DataEntry_CreateMeter" runat="server" title="Create Meter" href="createmeter.aspx"></a></li>
            <li><a id="lnkMySites2" class="apps_DataEntry_MeterReadings"  runat="server" title="Meter Readings" href="ShowMeterReadings.aspx"></a></li>
            <li><a id="lnkMySites3" class="apps_DataEntry_UploadReadings"  runat="server" title="Upload Readings" href="MeterReadingsUpload.aspx"></a></li>
            <li><a id="lnkMySites4" class="apps_DataEntry_OutstandingReadings"  runat="server" title="Outstanding Readings" href="CustomerOutstandingReads.aspx"></a></li>
            
            
            
        </ul>
    </div>
</asp:Content>

 