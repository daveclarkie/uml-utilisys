Imports System.Drawing
Imports System
Imports System.IO


Partial Class myapps_virtualmetercreation_default

    Inherits System.Web.UI.Page

    Public Class DropDownData

        Public Data As Integer
        Public Description As String

        Public Sub New(ByVal Newdata As Integer, ByVal NewDescription As String)
            Data = Newdata
            Description = NewDescription
        End Sub

        Public Overrides Function ToString() As String
            Return Description
        End Function

    End Class

    Private Sub ShowHidePanel(ByVal strPanelName As String)


        Me.pnlNewVirtualMeter.Visible = False
        Me.pnlAddNewCalculation.Visible = False
        Me.pnlShowCalculations.Visible = False

        If strPanelName = "NewVirtual" Then
            Me.pnlNewVirtualMeter.Visible = True
        ElseIf strPanelName = "NewCalculation" Then
            Me.pnlAddNewCalculation.Visible = True
        ElseIf strPanelName = "ShowCalculation" Then
            Me.pnlShowCalculations.Visible = True
        End If


    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Request.QueryString("custid") Is Nothing Then
            Dim strSQL As String = "EXEC spPortal_TREEVIEW " & MySession.UserID
            Dim myConnection As New OleDb.OleDbConnection(Core.CurrentConnectionString)

            Dim myCommand As New OleDb.OleDbCommand(strSQL, myConnection)
            myCommand.Connection.Open()
            myCommand.CommandTimeout = 180
            Dim Reader As OleDb.OleDbDataReader = myCommand.ExecuteReader(CommandBehavior.CloseConnection)

            If Reader.HasRows Then
                Reader.Read()
                If Reader("Type") = "Company" Then
                    Response.Redirect("default.aspx?custid=" & Reader("id") & "&method=Customer", True)
                Else
                    Response.Redirect("default.aspx?custid=" & Reader("id") & "&method=" & Reader("Type"), True)
                End If
                Reader.Close()
            End If
        End If

        Try
            If Not IIf(Core.data_select_value("select bitAllowMyApps from tblUsers where intUserPK = " & MySession.UserID) = "True", True, False) Then
                Response.Redirect(Core.data_select_value("select RawHomeURL from vwUsers where ID = " & MySession.UserID), True)
            End If

            If Not Page.IsPostBack = True Then
                Dim description As HtmlMeta = New HtmlMeta()
                description.Name = "description"
                description.Content = "Utility Masters - Virtual Meter Creation"
                Dim keywords As HtmlMeta = New HtmlMeta()
                keywords.Name = "keywords"
                keywords.Content = "Virtual Meter Creation"
                Page.Header.Controls.Add(description)
                Page.Header.Controls.Add(keywords)

                ShowHidePanel("None")
                PopulateMultiplier()
                PopulateVirtualMeters()

            End If

            Me.lblDescription.Text = Request.QueryString("method").ToString
            Me.lblName.Text = Core.data_select_value("EXEC spSites_MAP_CurentSelection " & MySession.UserID & ", '" & Request.QueryString("method") & "', " & Request.QueryString("custid"))




        Catch ex As Exception
            '
        End Try

    End Sub

    Private Sub PopulateMultiplier()

        Dim Y As Integer
        Dim X As String

        Y = 1
        X = "1.00"

        Me.ddlMultiplier.Items.Add("-- please select --")

        Do While Y < 202
            Me.ddlMultiplier.Items.Add(X)
            Y = Y + 1
            X = X - "0.01"
        Loop

        Me.ddlMultiplier.SelectedIndex = 0

    End Sub

    Private Sub PopulateSites()

        With Me.ddlVirtualMeterSites
            .Items.Clear()
            .DataSource = Core.data_select(" EXEC UML_ExtData.dbo.usp_Portal_Admin_ListSites " & Request.QueryString("custid") & ", '" & Request.QueryString("method") & "', " & MySession.UserID)
            .DataValueField = "ID"
            .DataTextField = "Value"
            .DataBind()
        End With

    End Sub

    Private Sub ClearAddNewVirtual()

        Me.ddlVirtualMeterSites.Items.Clear()
        Me.txtVirtualMeterName.Text = Nothing
        Me.txtVirtualMeterDescription.Text = Nothing
        Me.ddlPreAggMeter.SelectedIndex = 0
        Me.pnlNewVirtualMeter.Visible = False

        Me.ddlVirtualMeterSites.BackColor = Nothing
        Me.txtVirtualMeterName.BackColor = Nothing
        Me.txtVirtualMeterDescription.BackColor = Nothing
        Me.ddlPreAggMeter.BackColor = Nothing

    End Sub

    Private Sub PopulateVirtualMeters()

        Me.grd2.SelectedIndex = -1

        Dim strSQL As String
        strSQL = " SELECT intVirtualMeterPK , varVirtualMeterName , CASE WHEN intPreCreatedVirtual = 0 THEN 'No' ELSE 'Yes' END AS intPreCreatedVirtual FROM [UML_CMS].[dbo].[tblVirtualMeters] WHERE intCustomerFK = " & Request.QueryString("custid") & " ORDER BY varVirtualMeterName, varVirtualMeterDescription asc "

        Me.grd2.DataSource = Core.data_select(strSQL)
        Me.grd2.DataBind()

    End Sub

    Private Sub PopulateCalculations()

        Me.grd1.SelectedIndex = -1

        Dim strSQL As String
        strSQL = " EXEC uml_extdata.dbo.usp_Portal_MyApps_VirtualMeterCreation_ViewCalculations " & Me.grd2.SelectedValue

        Me.grd1.DataSource = Core.data_select(strSQL)
        Me.grd1.DataBind()
        
    End Sub

    'Protected Sub lbVirtualMeters_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbVirtualMeters.SelectedIndexChanged

    '    If Me.lbVirtualMeters.SelectedIndex > -1 Then
    '        Me.pnlShowVirtualMeters.Visible = False
    '        Me.lnkAddCalcualtion.Visible = True
    '        Me.pnlShowCalculations.Visible = True
    '        Me.pnlNewVirtualMeter.Visible = False
    '        PopulateCalculations()
    '        Me.lblCurrentCalculationSteps.Text = "Current Calculation Steps - " & Me.lbVirtualMeters.SelectedItem.Text
    '        Me.lnkCancelCalculation.Visible = True
    '        Me.lnkNewVirtualMeter.Visible = False
    '    Else
    '        Me.pnlShowVirtualMeters.Visible = True
    '        Me.pnlShowCalculations.Visible = False
    '        Me.pnlNewVirtualMeter.Visible = False
    '        Me.lnkAddCalcualtion.Visible = False
    '        Me.lblCurrentCalculationSteps.Text = "Current Calculation Steps"
    '        Me.lnkCancelCalculation.Visible = False
    '        Me.lnkNewVirtualMeter.Visible = True
    '    End If

    'End Sub

    Protected Sub grd1_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles grd1.RowDeleting

        ' Get the selected row index
        ' MsgBox(Me.grd1.Rows(e.RowIndex).Cells(3).Text)

        Core.data_execute_nonquery(" DELETE FROM [UML_CMS].[dbo].[tblVirtualMeterCalculations] WHERE intVirtualMeterCalculationPK = " & Me.grd1.Rows(e.RowIndex).Cells(3).Text)
        PopulateCalculations()
        ShowHidePanel("ShowCalculation")
    End Sub

    Protected Sub lnkNewVirtualMeter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkNewVirtualMeter.Click

        Me.lnkAddCalcualtion.Visible = False

        PopulateSites()
        Me.pnlShowVirtualMeters.Visible = True

        If Me.pnlNewVirtualMeter.Visible = True Then
            Me.pnlNewVirtualMeter.Visible = False
            Me.pnlAddNewCalculation.Visible = False
        

        Else
            Me.pnlNewVirtualMeter.Visible = True
            Me.pnlAddNewCalculation.Visible = False
            Me.pnlShowCalculations.Visible = False
        End If

    End Sub

    Protected Sub lnkAddCalcualtion_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddCalcualtion.Click

        ResetAddCalculation()

        If Me.pnlAddNewCalculation.Visible = True Then
            Me.pnlNewVirtualMeter.Visible = False
            Me.pnlAddNewCalculation.Visible = False
            Me.pnlShowCalculations.Visible = True
        Else
            Me.pnlNewVirtualMeter.Visible = False
            Me.pnlAddNewCalculation.Visible = True
            Me.pnlShowCalculations.Visible = True
        End If
    End Sub

    Private Sub ResetAddCalculation()

        Me.ddlMeterType.BackColor = Nothing
        Me.ddlMeter.BackColor = Nothing
        Me.ddlMultiplier.BackColor = Nothing

        With Me.ddlMeterType
            .Items.Clear()
            .DataSource = Core.data_select(" SELECT 0 as ID, '-- please select --' as Value UNION SELECT 1, 'Fiscal Meter' UNION SELECT 2, 'Sub Meter' ")
            .DataValueField = "ID"
            .DataTextField = "Value"
            .DataBind()
        End With
        With Me.ddlMeter
            .Items.Clear()
            .DataSource = Core.data_select(" SELECT 0 as ID, '-- please select --' as Value ")
            .DataValueField = "ID"
            .DataTextField = "Value"
            .DataBind()
        End With

        PopulateMultiplier()


        Me.txtNewMeterDescription.Text = Nothing


    End Sub

    Protected Sub ddlMeterType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlMeterType.SelectedIndexChanged

        Me.txtNewMeterDescription.Text = Nothing

        If Me.ddlMeterType.SelectedValue = 0 Then
            With Me.ddlMeter
                .Items.Clear()
                .DataSource = Core.data_select(" SELECT 0 as ID, '-- please select --' as Value ")
                .DataValueField = "ID"
                .DataTextField = "Value"
                .DataBind()
            End With
        Else
            With Me.ddlMeter
                .Items.Clear()
                .DataSource = Core.data_select(" EXEC UML_EXTData.dbo.usp_Portal_MyApps_VirtualMeterCreation_AvailableMeters " & Request.QueryString("custid") & ", '" & Request.QueryString("method") & "', " & Me.ddlMeterType.SelectedValue & ", " & MySession.UserID)
                .DataValueField = "ID"
                .DataTextField = "Value"
                .DataBind()
            End With
        End If

    End Sub

    Protected Sub ddlMeter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlMeter.SelectedIndexChanged
        If Me.ddlMeter.SelectedIndex > -1 Then
            Me.txtNewMeterDescription.Text = Me.ddlMeter.SelectedItem.Text
        End If
    End Sub

    Protected Sub lnkbtnAddCalculation_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkbtnAddCalculation.Click ' add the caculation

        Me.ddlMeterType.BackColor = Nothing
        Me.ddlMeter.BackColor = Nothing
        Me.ddlMultiplier.BackColor = Nothing

        Dim intError As Integer = 0


        If Me.ddlMeterType.SelectedItem.Value = 0 Then
            Me.ddlMeterType.BackColor = Color.Red

        End If

        If Me.ddlMeter.SelectedItem.Value = "0" Then
            Me.ddlMeter.BackColor = Color.Red


        End If

        If Me.ddlMultiplier.SelectedItem.Value = "-- please select --" Then
            Me.ddlMultiplier.BackColor = Color.Red


        End If



        If intError = 0 Then

            Dim par1 As Integer = Me.grd2.SelectedValue
            Dim par2 As Integer = Me.ddlMeterType.SelectedItem.Value
            Dim par3 As String = Me.ddlMeter.SelectedItem.Value
            Dim par4 As String = Me.txtNewMeterDescription.Text
            Dim par5 As Decimal = Me.ddlMultiplier.SelectedItem.Value
            Dim par6 As Integer = MySession.UserID

            Core.data_execute_nonquery(" EXEC UML_EXTData.dbo.usp_Portal_MyApps_VirtualMeterCreation_InsertNewCalculation " & par1 & ", " & par2 & ", '" & par3 & "', '" & par4 & "', '" & par5 & "', " & par6)
            PopulateCalculations()
            ShowHidePanel("ShowCalculation")
        Else

        End If


    End Sub

    Protected Sub lnkbtnCancelCalculation_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkbtnCancelCalculation.Click
        ResetAddCalculation()
        Me.pnlAddNewCalculation.Visible = False
    End Sub

    Protected Sub btnCancelAddVirtual_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelAddVirtual.Click
        PopulateSites()
        Me.pnlShowVirtualMeters.Visible = True
        Me.lblErrorNewVirtual.Text = Nothing

        Me.ddlVirtualMeterSites.SelectedIndex = 0
        Me.txtVirtualMeterName.Text = Nothing
        Me.txtVirtualMeterDescription.Text = Nothing

        If Me.pnlNewVirtualMeter.Visible = True Then
            Me.pnlNewVirtualMeter.Visible = False
            Me.pnlAddNewCalculation.Visible = False
            'If Me.lbVirtualMeters.SelectedIndex > -1 Then
            '    Me.pnlShowCalculations.Visible = True
            'End If

        Else
            Me.pnlNewVirtualMeter.Visible = True
            Me.pnlAddNewCalculation.Visible = False
            Me.pnlShowCalculations.Visible = False
        End If

    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click

        Me.txtVirtualMeterName.Text = RemoveSpecialChars(Me.txtVirtualMeterName.Text)

        Me.txtVirtualMeterName.BackColor = Nothing
        Me.ddlVirtualMeterSites.BackColor = Nothing
        Me.lblErrorNewVirtual.Text = Nothing

        If Me.txtVirtualMeterName.Text.Length > 0 Then
            Dim intCount As Integer = Core.data_select_value(" EXEC UML_EXTData.dbo.usp_Portal_MyApps_VirtualMeterCreation_CheckVirtualMeterName " & Request.QueryString("custid") & ", '" & Request.QueryString("method") & "', " & MySession.UserID & ", '" & Me.txtVirtualMeterName.Text & "'")
            If intCount > 0 Then
                Me.lblErrorNewVirtual.Text = "Name already exists"
                Me.txtVirtualMeterName.BackColor = Color.Red
                Me.txtVirtualMeterName.Focus()

            ElseIf Me.ddlVirtualMeterSites.SelectedItem.Value = 0 Then
                Me.lblErrorNewVirtual.Text = "Please select a site"
                Me.ddlVirtualMeterSites.BackColor = Color.Red
                Me.ddlVirtualMeterSites.Focus()

            ElseIf Me.ddlPreAggMeter.SelectedItem.Value = 0 Then
                Me.lblErrorNewVirtual.Text = "please select pre-aggregated?"
                Me.ddlPreAggMeter.BackColor = Color.Red
                Me.ddlPreAggMeter.Focus()

            Else
                Dim intCustomerFK As Integer = Core.data_select_value(" EXEC UML_EXTData.dbo.usp_Portal_Admin_GetCustomerFK " & Request.QueryString("custid") & ", '" & Request.QueryString("method") & "', " & MySession.UserID)

                Core.data_execute_nonquery(" EXEC UML_EXTData.dbo.usp_Portal_MyApps_VirtualMeterCreation_InsertNewVirtualMeter " & intCustomerFK & ", " & Me.ddlVirtualMeterSites.SelectedItem.Value & ", '" & Me.txtVirtualMeterName.Text & "', '" & Me.txtVirtualMeterDescription.Text & "', " & MySession.UserID & ", " & Me.ddlPreAggMeter.SelectedItem.Value)

                ClearAddNewVirtual()
                PopulateVirtualMeters()
            End If

        End If


    End Sub

    Public Function RemoveSpecialChars(ByVal str As String) As String
        Dim chars As String() = New String() {",", ".", "/", "!", "@", "#", _
         "$", "%", "^", "&", "*", "'", _
         """", ";", "(", ")", _
         ":", "|", "[", "]"}
        For i As Integer = 0 To chars.Length - 1
            If str.Contains(chars(i)) Then
                str = str.Replace(chars(i), "")
            End If
        Next
        Return str
    End Function

    Protected Sub lnkCancelCalculation_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCancelCalculation.Click
        Me.pnlNewVirtualMeter.Visible = False
        Me.pnlAddNewCalculation.Visible = False
        Me.pnlShowCalculations.Visible = False
        Me.pnlShowVirtualMeters.Visible = True
        Me.grd2.SelectedIndex = -1
        Me.lnkCancelCalculation.Visible = False
        Me.lnkAddCalcualtion.Visible = False
        Me.lnkNewVirtualMeter.Visible = True
    End Sub

    Protected Sub grd2_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles grd2.RowDeleting


        'Me.pnlDeleteVirtualConfirmation.Visible = True

        'Me.txtDeleteVirtualConfirmation_VirtualMeterID.Text = Me.grd2.Rows(e.RowIndex).Cells(3).Text
        'Me.txtDeleteVirtualConfirmation_VirtualMeterID.Visible = False

        'Me.lblDeleteVirtualConfirmation_Title.Text = "Are you sure you want to delete : " & Core.data_select_value("SELECT varVirtualMeterName FROM [UML_CMS].[dbo].[tblVirtualMeters] WHERE intVirtualMeterPK = " & Me.grd2.Rows(e.RowIndex).Cells(3).Text)
        Core.data_execute_nonquery(" DELETE FROM [UML_CMS].[dbo].[tblVirtualMeters] WHERE intvirtualmeterpk = " & Me.grd2.Rows(e.RowIndex).Cells(3).Text)
        PopulateVirtualMeters()

    End Sub

    Protected Sub grd2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles grd2.SelectedIndexChanged
        Me.pnlShowVirtualMeters.Visible = False
        Me.lnkAddCalcualtion.Visible = True
        Me.pnlShowCalculations.Visible = True
        Me.pnlNewVirtualMeter.Visible = False
        PopulateCalculations()
        Me.lblCurrentCalculationSteps.Text = "Current Calculation Steps - " & Me.grd2.SelectedRow.Cells(1).Text
        Me.lnkCancelCalculation.Visible = True
        Me.lnkNewVirtualMeter.Visible = False
        Me.pnlDeleteVirtualConfirmation.Visible = False
        Me.txtDeleteVirtualConfirmation_VirtualMeterID.Text = Nothing
    End Sub


    Protected Sub lnkDeleteVirtualConfirmation_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkDeleteVirtualConfirmation_Cancel.Click
        Me.pnlDeleteVirtualConfirmation.Visible = False
        Me.txtDeleteVirtualConfirmation_VirtualMeterID.Text = Nothing
    End Sub

    Protected Sub lnkDeleteVirtualConfirmation_Confirm_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkDeleteVirtualConfirmation_Confirm.Click

    End Sub
End Class
