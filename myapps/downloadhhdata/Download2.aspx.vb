﻿
Partial Class myapps_downloadhhdata_Download2
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Me.label1.Text = Core.data_select_value("SELECT pk_date FROM UML_CMS.dbo.DIM_Dates WHERE intDatePK = " & Request.QueryString("stdate"))
        Me.label2.Text = Core.data_select_value("SELECT pk_date FROM UML_CMS.dbo.DIM_Dates WHERE intDatePK = " & Request.QueryString("enddate"))
        Me.label3.Text = Request.QueryString("meters")
        Dim strSQL As String = ""
        If Request.QueryString("fmt") = "list" Then
            strSQL = "EXEC [portal.utilitymasters.co.uk].dbo.usp_MyApps_DownloadHHData_ListView '" & Me.label3.Text & "', '" & Me.label1.Text & "', '" & Me.label2.Text & "'"
        ElseIf Request.QueryString("fmt") = "tab" Then
            strSQL = "EXEC [portal.utilitymasters.co.uk].dbo.usp_MyApps_DownloadHHData_TabularView '" & Me.label3.Text & "', '" & Me.label1.Text & "', '" & Me.label2.Text & "'"
        End If

        Dim dtDefault As DataTable = Core.data_select(strSQL)

        With Me.HHDataGridView
            .DataSource = dtDefault
            .DataBind()
        End With

        Response.Clear()

        Response.AddHeader("content-disposition", "attachment; filename=Half Hour Data.xls")

        Response.Charset = ""

        ' If you want the option to open the Excel file without saving than

        ' comment out the line below

        ' Response.Cache.SetCacheability(HttpCacheability.NoCache);

        Response.ContentType = "application/vnd.xls"

        Dim stringWrite As New System.IO.StringWriter()

        Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)

        HHDataGridView.RenderControl(htmlWrite)

        Response.Write(stringWrite.ToString())

        Response.[End]()

        Dim strscript As String = "<script language=javascript>window.top.close();</script>"
        If (Not ClientScript.IsStartupScriptRegistered("clientScript")) Then
            ClientScript.RegisterStartupScript(Page.GetType(), "clientScript", strscript)
        End If
    End Sub

    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)

        ' Confirms that an HtmlForm control is rendered for the specified ASP.NET server control at run time.

    End Sub

End Class
