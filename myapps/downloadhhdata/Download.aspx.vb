﻿
Partial Class myapps_downloadhhdata_Download
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Me.label1.Text = Core.data_select_value("SELECT pk_date FROM UML_CMS.dbo.DIM_Dates WHERE intDatePK = " & Request.QueryString("stdate"))
        Me.label2.Text = Core.data_select_value("SELECT pk_date FROM UML_CMS.dbo.DIM_Dates WHERE intDatePK = " & Request.QueryString("enddate"))
        Me.label3.Text = Request.QueryString("meters")
        Dim strSQL As String = ""
        If Request.QueryString("fmt") = "list" Then
            strSQL = "EXEC [portal.utilitymasters.co.uk].dbo.usp_MyApps_DownloadHHData_ListView '" & Me.label3.Text & "', '" & Me.label1.Text & "', '" & Me.label2.Text & "'"
        ElseIf Request.QueryString("fmt") = "tab" Then
            strSQL = "EXEC [portal.utilitymasters.co.uk].dbo.usp_MyApps_DownloadHHData_TabularView '" & Me.label3.Text & "', '" & Me.label1.Text & "', '" & Me.label2.Text & "'"
        End If

        Dim dtDefault As DataTable = Core.data_select(strSQL)


        Dim strscript As String = "<script language=javascript>window.top.close();</script>"
        If (Not ClientScript.IsStartupScriptRegistered("clientScript")) Then
            ClientScript.RegisterStartupScript(Page.GetType(), "clientScript", strscript)
        End If
    End Sub

End Class
