Imports Dundas.Charting.WebControl
Imports System.Drawing
Imports System
Imports System.IO


Partial Class myapps_download_hhdata_Default

    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        Try
            If Not IIf(Core.data_select_value("SELECT COUNT(*) FROM dbo.tblUsersReports WHERE intReportFK = 27 AND intUserFK = " & MySession.UserID) > 0, True, False) Then
                Response.Redirect("~/myaccount/default.aspx", True)
            End If


            If Request.QueryString("custid") Is Nothing Then

                Dim strSQL As String = "EXEC spPortal_TREEVIEW " & MySession.UserID
                Dim myConnection As New OleDb.OleDbConnection(Core.CurrentConnectionString)

                Dim myCommand As New OleDb.OleDbCommand(strSQL, myConnection)
                myCommand.Connection.Open()
                myCommand.CommandTimeout = 180
                Dim Reader As OleDb.OleDbDataReader = myCommand.ExecuteReader(CommandBehavior.CloseConnection)

                If Reader.HasRows Then
                    Reader.Read()
                    If Reader("Type") = "Company" Then
                        Response.Redirect("default.aspx?custid=" & Reader("id") & "&method=Customer", True)
                    Else
                        Response.Redirect("default.aspx?custid=" & Reader("id") & "&method=" & Reader("Type"), True)
                    End If
                    Reader.Close()
                End If
            End If

            If Not Page.IsPostBack = True Then

                Dim description As HtmlMeta = New HtmlMeta()
                description.Name = "description"
                description.Content = "Utilisys - Download Half Hour Data"
                Dim keywords As HtmlMeta = New HtmlMeta()
                keywords.Name = "keywords"
                keywords.Content = "Download Half Hour Data"
                Page.Header.Controls.Add(description)
                Page.Header.Controls.Add(keywords)

                MeterPoints_Show(Me.rblMeterFilter.SelectedItem.Value)
                SetupDateType()
            End If


            Me.lblDescription.Text = Request.QueryString("method").ToString
            Me.lblName.Text = Core.data_select_value("EXEC spSites_MAP_CurentSelection " & MySession.UserID & ", '" & Request.QueryString("method") & "', " & Request.QueryString("custid"))
            CountAssignedMeters()

        Catch ex As Exception
            '
        End Try

    End Sub

    Public Overloads Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)

        ' Confirms that an HtmlForm control is rendered for the specified ASP.NET server control at run time.

    End Sub

    Private Sub DownloadData(ByVal varMeterID As String, ByVal dtmStartDate As DateTime, ByVal dtmEndDate As DateTime, ByVal varMethod As String)

        Dim strSQL As String
        strSQL = ""
        If varMethod = "List" Then
            strSQL = strSQL & " EXEC UML_ExtData.dbo.usp_Portal_MyApps_DownloadHHData_ListView '" & varMeterID & "', '" & dtmStartDate & "',  '" & dtmEndDate & "'; "
        Else
            strSQL = strSQL & " EXEC UML_ExtData.dbo.usp_Portal_MyApps_DownloadHHData_TabularView '" & varMeterID & "', '" & dtmStartDate & "',  '" & dtmEndDate & "'; "
        End If


        Me.gvHHData.DataSource = Core.data_select(strSQL)
        Me.gvHHData.DataBind()
        Me.gvHHData.Visible = False

        GridViewExportUtil.Export("HalfHourData.xls", Me.gvHHData)

    End Sub

    Private Sub MeterPoints_Show(ByVal Type As String)

        With Me.lbAvailableMeters
            .DataSource = Core.data_select(" EXEC [portal.utilitymasters.co.uk].dbo.usp_MeterPoints_Display " & MySession.UserID & ", '" & Request.QueryString("method") & "', " & Request.QueryString("custid") & ",'" & Type & "', '" & ListAssignedMeters() & "', '" & Me.txtCustomFilter.Text.ToString & "', 1")
            .DataValueField = "ID"
            .DataTextField = "varNiceName"
            .DataBind()
        End With


    End Sub

    Private Function ListAssignedMeters() As String
        Dim s As String = ""

        If Me.lbAssignedMeters.Items.Count > 0 Then
            For Each x As ListItem In lbAssignedMeters.Items
                s &= "," & x.Value
            Next
            If s.Length > 0 Then
                s = s.Remove(0, 1)
            End If
        End If

        Return s

    End Function

    Private Sub CountAssignedMeters()
        Dim i As Integer = 0
        For Each x As ListItem In lbAssignedMeters.Items
            i += 1
        Next

        If i = 0 Then
            Me.lnkbtndownloadList.Enabled = False
            Me.lnkbtndownloadList.CssClass = "apps_DownloadData_ExcelList_disabled"
            Me.lnkbtndownloadTab.Enabled = False
            Me.lnkbtndownloadTab.CssClass = "apps_DownloadData_ExcelTab_disabled"
        Else
            Me.lnkbtndownloadList.Enabled = True
            Me.lnkbtndownloadList.CssClass = "apps_DownloadData_ExcelList"
            Me.lnkbtndownloadTab.Enabled = True
            Me.lnkbtndownloadTab.CssClass = "apps_DownloadData_ExcelTab"
        End If

        If i > 4 Then
            Me.lbAvailableMeters.Enabled = False
        Else
            Me.lbAvailableMeters.Enabled = True
        End If

        lnkbtndownloadList.Attributes.Add("onclick", "window.open('download2.aspx?fmt=list&stdate=" & Me.txtStartDate.Text.ToString & "&enddate=" & Me.txtEndDate.Text.ToString & "&meters=" & ListAssignedMeters() & "'); return false;")
        lnkbtndownloadTab.Attributes.Add("onclick", "window.open('download2.aspx?fmt=tab&stdate=" & Me.txtStartDate.Text.ToString & "&enddate=" & Me.txtEndDate.Text.ToString & "&meters=" & ListAssignedMeters() & "'); return false;")

    End Sub

    Protected Sub rblMeterFilter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rblMeterFilter.SelectedIndexChanged
        If Me.rblMeterFilter.SelectedItem.Value = "Custom" Then
            Me.txtCustomFilter.Text = Nothing
            Me.secFilter.Visible = True
        Else
            Me.txtCustomFilter.Text = Nothing
            Me.secFilter.Visible = False
        End If


        MeterPoints_Show(Me.rblMeterFilter.SelectedItem.Value)
    End Sub

    Protected Sub lbAvailableMeters_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbAvailableMeters.SelectedIndexChanged


        Dim iAssigned As Integer = 0
        For Each xAssigned As ListItem In lbAssignedMeters.Items
            iAssigned += 1
        Next
        If iAssigned < 5 Then
            Me.lblAssignedMeterError.Text = ""

            Dim i As Integer = 0
            For Each x As ListItem In lbAvailableMeters.Items
                If x.Selected Then
                    i += 1
                End If
            Next

            If i > 0 Then
repeat:
                If lbAvailableMeters.Items.Count > 0 Then

                    Dim li As ListItem

                    For Each li In lbAvailableMeters.Items
                        If li.Selected = True Then
                            Dim iAssigned2 As Integer = 0
                            For Each xAssigned2 As ListItem In lbAssignedMeters.Items
                                iAssigned2 += 1
                            Next
                            If iAssigned2 = 5 Then
                                GoTo meterlimitexceeded
                            End If
                            lbAssignedMeters.Items.Add(li)
                            lbAvailableMeters.Items.Remove(li)
                            GoTo repeat
                        End If

                    Next

                End If
            End If

            Me.lbAssignedMeters.SelectedIndex = -1
            Me.lbAvailableMeters.SelectedIndex = -1

        Else
            Me.lblAssignedMeterError.Text = "you are currently limited to 5 meter points."
        End If


        MeterPoints_Show(Me.rblMeterFilter.SelectedItem.Value)

meterlimitexceeded:
        Me.lblAssignedMeterError.Text = "you are currently limited to 5 meter points."
        CountAssignedMeters()


    End Sub

    Protected Sub lbAssignedMeters_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbAssignedMeters.SelectedIndexChanged

        Me.lblAssignedMeterError.Text = ""

        Dim i As Integer = 0
        For Each x As ListItem In lbAssignedMeters.Items
            If x.Selected Then
                i += 1
            End If
        Next


        If i > 0 Then
repeat:
            If lbAssignedMeters.Items.Count > 0 Then

                Dim li As ListItem

                For Each li In lbAssignedMeters.Items
                    If li.Selected = True Then
                        lbAvailableMeters.Items.Add(li)
                        lbAssignedMeters.Items.Remove(li)
                        GoTo repeat
                    End If

                Next

            End If
        End If

        Me.lbAssignedMeters.SelectedIndex = -1
        Me.lbAvailableMeters.SelectedIndex = -1

        MeterPoints_Show(Me.rblMeterFilter.SelectedItem.Value)
        CountAssignedMeters()

    End Sub

    Protected Sub btnFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFilter.Click
        Me.txtCustomFilter.Text = Core.fn_NoSpecialCharacters(Me.txtCustomFilter.Text.ToString)
        MeterPoints_Show(Me.rblMeterFilter.SelectedItem.Value)

    End Sub

    Private Sub SetupStartandEndDates()

        Dim strDateType As String = Replace(Me.ddlDateType.SelectedItem.Value, " ", "_")
        Dim strDateSelected As String = Me.ddlDateSelect.SelectedItem.Value

        If strDateType = "Default" Then
            If strDateSelected = "0" Then ' last 1 week

                Dim strDateStart As String = DateAdd(DateInterval.Day, -7, System.DateTime.Now.Date)
                Dim strDateEnd As String = System.DateTime.Now.Date

                Dim strSQL As String = "SELECT MIN(intDatePK) as ID FROM UML_CMS.dbo.DIM_Dates WHERE [pk_date] = UML_EXTData.dbo.formatdatetime('" & strDateStart & "', 'dd/mm/yyyy')"
                Me.txtStartDate.Text = Core.data_select_value(strSQL)
                strSQL = "SELECT MAX(intDatePK) as ID FROM UML_CMS.dbo.DIM_Dates WHERE [pk_date] = UML_EXTData.dbo.formatdatetime('" & strDateEnd & "', 'dd/mm/yyyy')"
                Me.txtEndDate.Text = Core.data_select_value(strSQL)

                Me.lblStartDate.Text = Core.data_select_value("SELECT [Date_Name] FROM UML_CMS.dbo.Dim_Dates WHERE intDatePK = " & Me.txtStartDate.Text)
                Me.lblEndDate.Text = Core.data_select_value("SELECT [Date_Name] FROM UML_CMS.dbo.Dim_Dates WHERE intDatePK = " & Me.txtEndDate.Text)

            ElseIf strDateSelected = "1" Then ' last 1 month

                Dim strDateStart As String = DateAdd(DateInterval.Month, -1, System.DateTime.Now.Date)
                Dim strDateEnd As String = System.DateTime.Now.Date

                Dim strSQL As String = "SELECT MIN(intDatePK) as ID FROM UML_CMS.dbo.DIM_Dates WHERE [pk_date] = UML_EXTData.dbo.formatdatetime('" & strDateStart & "', 'dd/mm/yyyy')"
                Me.txtStartDate.Text = Core.data_select_value(strSQL)
                strSQL = "SELECT MAX(intDatePK) as ID FROM UML_CMS.dbo.DIM_Dates WHERE [pk_date] = UML_EXTData.dbo.formatdatetime('" & strDateEnd & "', 'dd/mm/yyyy')"
                Me.txtEndDate.Text = Core.data_select_value(strSQL)

                Me.lblStartDate.Text = Core.data_select_value("SELECT [Date_Name] FROM UML_CMS.dbo.Dim_Dates WHERE intDatePK = " & Me.txtStartDate.Text)
                Me.lblEndDate.Text = Core.data_select_value("SELECT [Date_Name] FROM UML_CMS.dbo.Dim_Dates WHERE intDatePK = " & Me.txtEndDate.Text)

            ElseIf strDateSelected = "2" Then ' last 1 year

                Dim strDateStart As String = DateAdd(DateInterval.Year, -1, System.DateTime.Now.Date)
                Dim strDateEnd As String = System.DateTime.Now.Date

                Dim strSQL As String = "SELECT MIN(intDatePK) as ID FROM UML_CMS.dbo.DIM_Dates WHERE [pk_date] = UML_EXTData.dbo.formatdatetime('" & strDateStart & "', 'dd/mm/yyyy')"
                Me.txtStartDate.Text = Core.data_select_value(strSQL)
                strSQL = "SELECT MAX(intDatePK) as ID FROM UML_CMS.dbo.DIM_Dates WHERE [pk_date] = UML_EXTData.dbo.formatdatetime('" & strDateEnd & "', 'dd/mm/yyyy')"
                Me.txtEndDate.Text = Core.data_select_value(strSQL)

                Me.lblStartDate.Text = Core.data_select_value("SELECT [Date_Name] FROM UML_CMS.dbo.Dim_Dates WHERE intDatePK = " & Me.txtStartDate.Text)
                Me.lblEndDate.Text = Core.data_select_value("SELECT [Date_Name] FROM UML_CMS.dbo.Dim_Dates WHERE intDatePK = " & Me.txtEndDate.Text)

            End If

        Else

            Dim strSQL As String = "SELECT MIN(intDatePK) as ID FROM UML_CMS.dbo.DIM_Dates WHERE [" & strDateType & "] = UML_EXTData.dbo.formatdatetime('" & strDateSelected & "', 'dd/mm/yyyy')"
            Me.txtStartDate.Text = Core.data_select_value(strSQL)
            strSQL = "SELECT MAX(intDatePK) as ID FROM UML_CMS.dbo.DIM_Dates WHERE [" & strDateType & "] = UML_EXTData.dbo.formatdatetime('" & strDateSelected & "', 'dd/mm/yyyy')"
            Me.txtEndDate.Text = Core.data_select_value(strSQL)

            Me.lblStartDate.Text = Core.data_select_value("SELECT [Date_Name] FROM UML_CMS.dbo.Dim_Dates WHERE intDatePK = " & Me.txtStartDate.Text)
            Me.lblEndDate.Text = Core.data_select_value("SELECT [Date_Name] FROM UML_CMS.dbo.Dim_Dates WHERE intDatePK = " & Me.txtEndDate.Text)

        End If

    End Sub
    Private Sub SetupDateSelect()

        With Me.ddlDateSelect
            .Items.Clear()
            .DataSource = Core.data_select("EXEC [portal.utilitymasters.co.uk].dbo.usp_MyReports_Parameters_CustomDateRanges '" & Me.ddlDateType.SelectedItem.Value & "'")
            .DataValueField = "ID"
            .DataTextField = "Value"
            .DataBind()
        End With

        SetupStartandEndDates()

    End Sub
    Private Sub SetupDateType()

        With Me.ddlDateType
            .Items.Clear()
            .DataSource = Core.data_select("EXEC [portal.utilitymasters.co.uk].dbo.usp_MyReports_Parameters_CustomDateTypes 1")
            .DataValueField = "varValue"
            .DataTextField = "varValue"
            .DataBind()
        End With


        Me.ddlDateType.SelectedItem.Value = "Default"

        SetupDateSelect()

    End Sub
    
    Protected Sub ddlDateType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlDateType.SelectedIndexChanged

        SetupDateSelect()
        'SetupStartandEndDates()
        CountAssignedMeters()
    End Sub
    Protected Sub ddlDateSelect_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlDateSelect.SelectedIndexChanged

        SetupStartandEndDates()
        CountAssignedMeters()
    End Sub

End Class
