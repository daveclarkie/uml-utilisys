<%@ Page Language="VB" MasterPageFile="~/landing.master" AutoEventWireup="false" CodeFile="Default.aspx.vb" Inherits="myapps_download_hhdata_Default" title="Download HH Data" %>

<%@ Register Assembly="DundasWebChart" Namespace="Dundas.Charting.WebControl" TagPrefix="DCWC" %>

<asp:Content ID="Content1" ContentPlaceHolderID="content_right" Runat="Server">

        
        <script type="text/javascript" src="../../../ThickBox/jquery-1.3.2.min.js"></script>
        <script type="text/javascript" src="../../../ThickBox/thickbox.js"></script>
        <link rel="stylesheet" href="../../../ThickBox/thickbox.css" type="text/css" media="screen" />   
                
<form id="Form1" runat="server">

    <div class="content_back general">

    
            
            <table cellpadding="0" cellspacing="0">
                <tr valign="middle">
                    <td style="width:450px;" >
                        
                    </td>
                    <td style="width:150px;">
                    </td>
                </tr> <!-- Company selection -->
                
                <tr>
                    
                    <td colspan="1">
                        <table cellpadding="0" cellspacing="0">

                            <tr>
                                <td style="width:120px;" valign="top">
                                     <asp:Label ID="lblDescription" Text="Description" runat="server" Font-Bold="true" Width="120px" ></asp:Label>
                                </td>
                                <td style="width:400px;" valign="middle">
                                     <asp:Label ID="lblName" Text="Name" runat="server"></asp:Label>
                                </td>
                            </tr>

                            <tr>
                                <td style="height:3px;">
                                    
                                </td>
                            </tr>

                             <tr>
                                <td style="width:120px;" valign="top">
                                     <b>Application</b>
                                </td>
                                <td style="width:400px;" valign="middle">
                                    Download Half Hour Data
                                </td>
                            </tr>
                                                        
                            <tr>
                                <td style="height:3px;">
                                    
                                </td>
                            </tr>

                            <tr>
                                <td style="width:120px;" valign="top">
                                    <asp:label ID="lblFilterNameAvailable" runat="server" Text="Available Meters" Width="120px" Font-Bold="true"></asp:label>
                                </td>
                                <td style="width:400px;" valign="top">
                                    <asp:RadioButtonList Width="450px" ID="rblMeterFilter" runat="server" RepeatLayout="table" RepeatDirection="Horizontal" AutoPostBack="true">
                                        <asp:ListItem Value="All" Text="Show All" Selected="True"></asp:ListItem>
                                        <asp:ListItem Value="A" Text="Elec"></asp:ListItem>
                                        <asp:ListItem Value="B" Text="Gas"></asp:ListItem>
                                        <asp:ListItem Value="C" Text="Sub"></asp:ListItem>
                                        <asp:ListItem Value="D" Text="Virtual"></asp:ListItem>
                                        <asp:ListItem Value="Custom" Text="Custom"></asp:ListItem>
                                    </asp:RadioButtonList>
                                    <div id="secFilter" runat="server" visible="false">
                                        <asp:textbox ID="txtCustomFilter" runat="server" AutoPostBack="true" BackColor="#FFFFC0" style="width:400px;"></asp:textbox>
                                        <asp:Button ID="btnFilter" runat="server" Text="Go" />
                                    </div>
                                </td>
                            </tr>
                            
                            <tr>
                                <td colspan="2">
                                    
                                </td>
                            </tr>
                            
                            <tr>
                                <td colspan="2">
                                    <asp:ListBox ID="lbAvailableMeters" runat="server" style="width:630px; height:100px;" SelectionMode="Multiple" AutoPostBack="true" BackColor="#FFFFC0" >
                                    </asp:ListBox>
                                </td>
                            </tr>
                            
                            <tr>
                                <td>
                                    <asp:label ID="lblFilterNameSelected" runat="server" Text="Available Meters" Width="120px" Font-Bold="true"></asp:label>
                                </td>
                                <td>
                                    <asp:Label ID="lblAssignedMeterError" Text="" runat="server" ForeColor="Red"></asp:Label>
                                </td>
                            </tr>
                            
                            <tr>
                                <td colspan="2">
                                    <asp:ListBox ID="lbAssignedMeters" runat="server" style="width:630px; height:100px;" SelectionMode="Multiple" AutoPostBack="true" BackColor="#FFFFC0" >
                                    </asp:ListBox>                                    
                                </td>
                            </tr>
                            
                        </table>
                    </td>
                </tr> <!-- Meter Selection Table -->
                
                <tr>
                    <td colspan="3">
                        <table>
                            <tr>
                                <td>
                                    <asp:Table ID="tblOtherDates" runat="server" CssClass="background_date" Visible="true">
                    
                                        <asp:TableRow>
                                            <asp:TableCell Height="10px" ColumnSpan="3">
                                                <b>Date Range</b>
                                            </asp:TableCell>
                                        </asp:TableRow>
                    
                                        <asp:TableRow>
                                            <asp:TableCell Width="5px">
                                            
                                            </asp:TableCell>
                                            <asp:TableCell Width="100px">
                                                Date Type
                                            </asp:TableCell>
                                            <asp:TableCell Width="230px">
                                                <asp:DropDownList ID="ddlDateType" runat="server" AutoPostBack="true" Width="100%"></asp:DropDownList>
                                            </asp:TableCell>
                                            <asp:TableCell Width="5px">
                                            
                                            </asp:TableCell>
                                        </asp:TableRow>

                                        <asp:TableRow>
                                            <asp:TableCell></asp:TableCell>
                                            <asp:TableCell>
                                                Date Select
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <asp:DropDownList ID="ddlDateSelect" runat="server" AutoPostBack="true" Width="100%"></asp:DropDownList>
                                            </asp:TableCell>
                                            <asp:TableCell></asp:TableCell>
                                        </asp:TableRow>

                                        <asp:TableRow>
                                            <asp:TableCell></asp:TableCell>
                                            <asp:TableCell>
                                                From
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <asp:TextBox ID="txtStartDate" runat="server" Visible="false"></asp:TextBox>
                                                <asp:Label ID="lblStartDate" runat="server" ></asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell></asp:TableCell>
                                        </asp:TableRow>

                                        <asp:TableRow>
                                            <asp:TableCell></asp:TableCell>
                                            <asp:TableCell>
                                                To
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <asp:TextBox ID="txtEndDate" runat="server" Visible="false"></asp:TextBox>
                                                <asp:Label ID="lblEndDate" runat="server" ></asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell></asp:TableCell>
                                        </asp:TableRow>
                    
                                        <asp:TableRow>
                                            <asp:TableCell Height="10px">
                                            </asp:TableCell>
                                        </asp:TableRow>
                    
                                    </asp:Table>
                                </td>
                                <td>
                                    <asp:linkbutton id="lnkbtndownloadTab" cssclass="apps_DownloadData_ExcelTab" runat="server" visible="true" Height="160px" Width="200px"></asp:linkbutton>
                                </td>
                                <td>
                                    <asp:linkbutton id="lnkbtndownloadList" cssclass="apps_DownloadData_ExcelList" runat="server" visible="true" Height="160px" Width="200px"></asp:linkbutton>
                                </td>
                            </tr>
                        </table>
                         
                    </td>
                </tr>       
                <tr>
                    <td colspan="4">
                        <asp:TextBox ID="txtMeterID" runat="server" Text="" Visible="false" Width="10"></asp:TextBox>
                        <asp:TextBox ID="txtFrequency" runat="server" Text="" Visible="false" Width="10"></asp:TextBox>
                        <asp:TextBox ID="txtStart" runat="server" Text="" Visible="false" Width="100"></asp:TextBox> 
                        <asp:TextBox ID="txtEnd" runat="server" Text="" Visible="false" Width="100"></asp:TextBox>
                        <asp:TextBox ID="txtGraphType" runat="server" Text="" Visible="false" Width="10"></asp:TextBox>
                        <asp:TextBox ID="txtReadingType" runat="server" Text="Cons - kWh" Visible="false" Width="100"></asp:TextBox>
                    </td>
                </tr>  
            </table>
        </div>

           
        <asp:DataGrid ID="grdHHData" runat="server" Visible="false" AutoGenerateColumns="true"></asp:DataGrid>
            
        <asp:GridView ID="gvHHData" runat="server" Visible="false" AutoGenerateColumns="true"></asp:GridView>
     
</form>
</asp:Content>

