Imports Dundas.Charting.WebControl
Imports System.Drawing
Imports System
Imports System.IO


Partial Class myapps_metergrouping_Default

    Inherits System.Web.UI.Page
    Dim intMaxMeters As Integer = 100

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        Try
            If Not IIf(Core.data_select_value("SELECT COUNT(*) FROM dbo.tblUsersReports WHERE intReportFK = 51 AND intUserFK = " & MySession.UserID) > 0, True, False) Then
                Response.Redirect("~/myaccount/default.aspx", True)
            End If


            If Request.QueryString("custid") Is Nothing Then

                Dim strSQL As String = "EXEC spPortal_TREEVIEW " & MySession.UserID
                Dim myConnection As New OleDb.OleDbConnection(Core.CurrentConnectionString)

                Dim myCommand As New OleDb.OleDbCommand(strSQL, myConnection)
                myCommand.Connection.Open()
                myCommand.CommandTimeout = 180
                Dim Reader As OleDb.OleDbDataReader = myCommand.ExecuteReader(CommandBehavior.CloseConnection)

                If Reader.HasRows Then
                    Reader.Read()
                    If Reader("Type") = "Company" Then
                        Response.Redirect("default.aspx?custid=" & Reader("id") & "&method=Customer", True)
                    Else
                        Response.Redirect("default.aspx?custid=" & Reader("id") & "&method=" & Reader("Type"), True)
                    End If
                    Reader.Close()
                End If
            End If

            If Not Page.IsPostBack = True Then

                Dim description As HtmlMeta = New HtmlMeta()
                description.Name = "description"
                description.Content = "Utilisys - Meter Grouping"
                Dim keywords As HtmlMeta = New HtmlMeta()
                keywords.Name = "keywords"
                keywords.Content = "Meter Grouping"
                Page.Header.Controls.Add(description)
                Page.Header.Controls.Add(keywords)

                Me.lblDescription.Text = Request.QueryString("method").ToString
                Me.lblName.Text = Core.data_select_value("EXEC spSites_MAP_CurentSelection " & MySession.UserID & ", '" & Request.QueryString("method") & "', " & Request.QueryString("custid"))

                LoadMeterGroupings()

            End If

        Catch ex As Exception
            '
        End Try

    End Sub

    Public Overloads Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)

        ' Confirms that an HtmlForm control is rendered for the specified ASP.NET server control at run time.

    End Sub

    Private Sub LoadMeterGroupings()

        Me.pnlCurrentGroupings.Visible = True
        Me.pnlMeterGroupingDetails.Visible = False

        With Me.lbMeterGroupings
            .Items.Clear()
            .DataSource = Core.data_select("SELECT DISTINCT intMeterGrouping_NamePK as ID, varMeterGrouping_Name as varName, 1 as intOrder FROM [portal.utilitymasters.co.uk].dbo.tblMeterGrouping_Name WHERE intCustomerFK = " & Request.QueryString("custid") & " ORDER BY intOrder, varName ASC")
            .DataValueField = "ID"
            .DataTextField = "varName"
            .DataBind()
        End With

        If Me.lbMeterGroupings.Items.Count > 0 Then Me.lbMeterGroupings.Enabled = True Else Me.lbMeterGroupings.Enabled = False

    End Sub

    Protected Sub btnCreateNewMeterGrouping_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCreateNewMeterGrouping.Click
        LoadMeterGroupingDetails(0)
    End Sub

    Protected Sub lbMeterGroupings_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbMeterGroupings.SelectedIndexChanged
        If Me.lbMeterGroupings.SelectedItem.Value > 0 Then
            LoadMeterGroupingDetails(Me.lbMeterGroupings.SelectedItem.Value)
        Else
            'un-select the meter grouping selection box
            Me.lbMeterGroupings.SelectedIndex = -1
        End If
    End Sub

    Private Sub LoadMeterGroupingDetails(ByVal intMeterGroupingID As Integer)

        Me.lblMeterGroupNameError.Text = Nothing

        Me.pnlCurrentGroupings.Visible = False
        Me.pnlMeterGroupingDetails.Visible = True

        Me.txtMeterGroupName.Enabled = True
        Me.txtMeterGroupName.Text = Nothing
        Me.imgSmallSaveButton.Visible = True

        Me.lbMeterGroupingAvailable.Visible = True
        Me.lbMeterGroupingAssigned.Visible = True

        Me.lblHalfHourMeterOnly.Visible = True
        Me.rblHalfHourMeterOnly.Visible = True
        Me.lblFilterNameAvailable.Visible = True
        Me.rblMeterFilter.Visible = True
        Me.lblFilterNameSelected.Visible = True
        Me.lnkDeleteMeterGrouping.Visible = False

        Me.lblGroupID.Text = intMeterGroupingID

        GroupingMeterPoints_Show()

        If intMeterGroupingID = 0 Then ' new meter grouping

            Me.lbMeterGroupingAvailable.Visible = False
            Me.lbMeterGroupingAssigned.Visible = False

            Me.lblHalfHourMeterOnly.Visible = False
            Me.rblHalfHourMeterOnly.Visible = False
            Me.lblFilterNameAvailable.Visible = False
            Me.rblMeterFilter.Visible = False
            Me.lblFilterNameSelected.Visible = False

        Else ' load existing meter grouping

            Me.txtMeterGroupName.Enabled = False
            Me.imgSmallSaveButton.Visible = False
            Me.txtMeterGroupName.Text = Core.data_select_value("SELECT varMeterGrouping_Name FROM [portal.utilitymasters.co.uk].dbo.tblMeterGrouping_Name WHERE intMeterGrouping_NamePK = " & intMeterGroupingID)
            Me.lnkDeleteMeterGrouping.Visible = True


        End If

    End Sub

    Private Sub GroupingMeterPoints_Show()

        With Me.lbMeterGroupingAvailable
            .DataSource = Core.data_select(" EXEC [portal.utilitymasters.co.uk].dbo.usp_MeterPoints_Display " & MySession.UserID & ", '" & Request.QueryString("method") & "', " & Request.QueryString("custid") & ",'" & Me.rblMeterFilter.SelectedItem.Value & "', '" & GroupingListAssignedMeters(Me.lblGroupID.Text) & "', '" & Me.txtCustomFilter.Text.ToString & "', " & Me.rblHalfHourMeterOnly.SelectedItem.Value.ToString)
            '
            .DataValueField = "ID"
            .DataTextField = "varNiceName"
            .DataBind()
        End With

    End Sub

    Protected Sub rblMeterFilter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rblMeterFilter.SelectedIndexChanged
        If Me.rblMeterFilter.SelectedItem.Value = "Custom" Then
            Me.txtCustomFilter.Text = Nothing
            Me.secFilter.Visible = True
        Else
            Me.txtCustomFilter.Text = Nothing
            Me.secFilter.Visible = False
        End If
        GroupingMeterPoints_Show()

    End Sub

    Private Function GroupingListAssignedMeters(ByVal intMeterGroupingID As Integer) As String
        Dim s As String = ""

        With Me.lbMeterGroupingAssigned
            .DataSource = Core.data_select("SELECT varMeterViewID, varNiceName FROM [portal.utilitymasters.co.uk].dbo.tblMeterGrouping_Meters INNER JOIN uvw_DisplayAllMeterPoints ON ID = varMeterViewID WHERE intMeterGrouping_NameFK = " & intMeterGroupingID)
            .DataValueField = "varMeterViewID"
            .DataTextField = "varNiceName"
            .DataBind()
        End With

        If Me.lbMeterGroupingAssigned.Items.Count > 0 Then
            For Each x As ListItem In lbMeterGroupingAssigned.Items
                s &= "," & x.Value
            Next
            If s.Length > 0 Then
                s = s.Remove(0, 1)
            End If
        End If

        Return s

    End Function

    Private Function ReturnCountofAssignedMeters() As String
        Dim i As Integer = 0
        For Each x As ListItem In lbMeterGroupingAssigned.Items
            i += 1
        Next
        Return i
    End Function

    Private Sub CountAssignedMeters()
        Dim i As Integer = 0
        For Each x As ListItem In lbMeterGroupingAssigned.Items
            i += 1
        Next

        If i > (intMaxMeters - 1) Then
            Me.lbMeterGroupingAvailable.Enabled = False
        Else
            Me.lbMeterGroupingAvailable.Enabled = True
        End If

    End Sub

    Protected Sub lbMeterGroupingAssigned_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbMeterGroupingAssigned.SelectedIndexChanged

        Me.lblAssignedMeterError.Text = Me.lbMeterGroupingAssigned.SelectedIndex

        Core.data_execute_nonquery("DELETE FROM [portal.utilitymasters.co.uk].dbo.tblMeterGrouping_Meters WHERE intMeterGrouping_NameFK = " & Me.lbMeterGroupings.SelectedItem.Value & " AND varMeterViewID = '" & Me.lbMeterGroupingAssigned.SelectedItem.Value & "'")

        GroupingMeterPoints_Show()
        CountAssignedMeters()

    End Sub

    Protected Sub lbMeterGroupingAvailable_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbMeterGroupingAvailable.SelectedIndexChanged

        If Core.data_select_value("SELECT COUNT(*) FROM [portal.utilitymasters.co.uk].dbo.tblMeterGrouping_Meters WHERE intMeterGrouping_NameFK = " & Me.lblGroupID.Text & " AND varMeterViewID = '" & Me.lbMeterGroupingAvailable.SelectedItem.Value & "'") = 0 Then
            Core.data_execute_nonquery("INSERT INTO [portal.utilitymasters.co.uk].dbo.tblMeterGrouping_Meters (intMeterGrouping_NameFK, varMeterViewID, intUserCreatedFK) VALUES (" & Me.lblGroupID.Text & ", '" & Me.lbMeterGroupingAvailable.SelectedItem.Value & "', " & MySession.UserID & ")")
        End If
        GroupingMeterPoints_Show()
        CountAssignedMeters()
    End Sub

    Protected Sub btnFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFilter.Click
        GroupingMeterPoints_Show()
    End Sub



    Protected Sub imgSmallSaveButton_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgSmallSaveButton.Click

        Dim strGroupName As String = Core.fn_NoSpecialCharacters(Me.txtMeterGroupName.Text)
        Me.txtMeterGroupName.Text = strGroupName

        If strGroupName.Length = 0 Then
            Me.lblMeterGroupNameError.Text = "You need to enter a name for this grouping." ' there is no Group Name entered.
        Else
            If Core.data_select_value("SELECT COUNT(*) FROM [portal.utilitymasters.co.uk].dbo.tblMeterGrouping_Name WHERE varMeterGrouping_Name = '" & strGroupName & "' AND intCustomerFK = " & Request.QueryString("custid")) = 0 Then
                Core.data_execute_nonquery("INSERT INTO [portal.utilitymasters.co.uk].dbo.tblMeterGrouping_Name (varMeterGrouping_Name, intCustomerFK, intUserCreatedFK) VALUES ('" & strGroupName & "', " & Request.QueryString("custid") & ", " & MySession.UserID & ")")
                Dim intNewMeterGroupingPK As Integer = Core.data_select_value("SELECT intMeterGrouping_NamePK FROM [portal.utilitymasters.co.uk].dbo.tblMeterGrouping_Name WHERE varMeterGrouping_Name = '" & strGroupName & "' AND intCustomerFK = " & Request.QueryString("custid"))
                LoadMeterGroupingDetails(intNewMeterGroupingPK)
            Else
                Me.lblMeterGroupNameError.Text = "You already have a meter grouping with this name" 'name already exists
            End If

        End If

    End Sub



    Protected Sub lnkDeleteMeterGrouping_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkDeleteMeterGrouping.Click
        Me.pnlDeleteMeterGroupQuestion.Visible = True
        Me.lblDeleteMeterGroupQuestion.Text = "Are you sure you wish to delete " & Me.txtMeterGroupName.Text & " ?"

    End Sub

    Protected Sub btnDeleteMeterGroupQuestionYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDeleteMeterGroupQuestionYes.Click

        'delete all the meters from the group
        Core.data_execute_nonquery("DELETE FROM [portal.utilitymasters.co.uk].dbo.tblMeterGrouping_Meters WHERE intMeterGrouping_NameFK = " & Me.lblGroupID.Text)

        'delete the group name
        Core.data_execute_nonquery("DELETE FROM [portal.utilitymasters.co.uk].dbo.tblMeterGrouping_Name WHERE intMeterGrouping_NamePK = " & Me.lblGroupID.Text)

        LoadMeterGroupings()
        Me.pnlDeleteMeterGroupQuestion.Visible = False
    End Sub

    Protected Sub btnDeleteMeterGroupQuestionNo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDeleteMeterGroupQuestionNo.Click
        Me.pnlDeleteMeterGroupQuestion.Visible = False
    End Sub

    Protected Sub ImageButtonReturnToReport_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonReturnToReport.Click
        LoadMeterGroupings()
    End Sub
End Class
