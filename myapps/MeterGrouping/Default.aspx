<%@ Page Language="VB" MasterPageFile="~/landing.master" AutoEventWireup="false" CodeFile="Default.aspx.vb" Inherits="myapps_metergrouping_Default" title="Meter Grouping" %>

<%@ Register Assembly="DundasWebChart" Namespace="Dundas.Charting.WebControl" TagPrefix="DCWC" %>

<asp:Content ID="Content1" ContentPlaceHolderID="content_right" Runat="Server">

        
        <script type="text/javascript" src="../../../ThickBox/jquery-1.3.2.min.js"></script>
        <script type="text/javascript" src="../../../ThickBox/thickbox.js"></script>
        <link rel="stylesheet" href="../../../ThickBox/thickbox.css" type="text/css" media="screen" />   
                
<form id="Form1" runat="server">

      <div class="content_back general">
            
            <table cellpadding="0" cellspacing="0">
                <tr valign="middle">
                    <td style="width:450px;" >
                        
                    </td>
                    <td style="width:150px;">
                    </td>
                </tr> <!-- Company selection -->
                
                <tr>
                    
                    <td colspan="1">
                        <table cellpadding="0" cellspacing="0">

                            <tr>
                                <td style="width:140px;" valign="top">
                                     <asp:Label ID="lblDescription" Text="Description" runat="server" Font-Bold="true" Width="120px" ></asp:Label>
                                </td>
                                <td style="width:400px;" valign="middle">
                                     <asp:Label ID="lblName" Text="Name" runat="server"></asp:Label>
                                </td>
                            </tr>
                                                        
                            <tr>
                                <td style="height:3px;">
                                    
                                </td>
                            </tr>

                             <tr>
                                <td style="width:120px;" valign="top">
                                     <b>Application</b>
                                </td>
                                <td style="width:610px;" valign="middle">
                                    Meter Grouping
                                </td>
                            </tr>
                                                        
                            <tr>
                                <td style="height:3px;">
                                    
                                </td>
                            </tr>

                            <tr>
                                <td colspan="2">                                
                                    <asp:Panel ID="pnlCurrentGroupings" runat="server" Visible="false">
                                        <table cellpadding="0" cellspacing="0">
                                     
                                            <tr>
                                                <td colspan="2">
                                                    <asp:Button ID="btnCreateNewMeterGrouping" runat="server" Text="Create New Meter Grouping" Width="500px" />
                                                </td>
                                            </tr>
                                            
                                            <tr>
                                                <td style="height:3px;">
                                    
                                                </td>
                                            </tr>

                                            <tr>
                                                <td>
                                                    <b>Current Meter Groupings</b>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td colspan="2">
                                                    <asp:ListBox ID="lbMeterGroupings" runat="server" AutoPostBack="true" Width="500px" Height="200px" SelectionMode="Single">                                        
                                                    </asp:ListBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                    <asp:Panel ID="pnlMeterGroupingDetails" runat="server" Visible="false">
                                        <table cellpadding="0" cellspacing="0">
                                     <asp:ImageButton ID="ImageButtonReturnToReport"  
                             style="Z-INDEX:1; LEFT: 850px; POSITION: absolute; TOP:100px"
                            height="32px"  
                            width="32px"   
                            runat="server"   
                            ImageAlign="Left"  
                            ImageUrl="~/assets/metering/remove_48x48.png" 
                        />
                                            <tr>
                                                <td style="width:120px;" valign="top">
                                                     <b>Group Name</b><asp:Label ID="lblGroupID" runat="server" Visible="false"></asp:Label>
                                                </td>
                                                <td style="width:400px;" valign="middle">
                                                    <asp:TextBox ID="txtMeterGroupName" runat="server" AutoPostBack="true" Width="300px"></asp:TextBox>
                                                    <div style="Z-INDEX: 125; LEFT:77%; POSITION: absolute; TOP: 168px;">
                                                    <asp:ImageButton ID="imgSmallSaveButton" runat="server" ImageUrl="~/assets/Buttons_Small_Save.png" Visible="True" />
                                                    <asp:LinkButton ID="lnkDeleteMeterGrouping" runat="server" Text="delete" Visible="false"></asp:LinkButton>
                                                </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>
                                                    <asp:Label ID="lblMeterGroupNameError" runat="server" ForeColor="Red"></asp:Label>
                                                </td>
                                            </tr>
                                                   
                                            <tr>
                                                <td style="height:3px;">
                                    
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width:120px;" valign="top">
                                                     <asp:label ID="lblHalfHourMeterOnly" runat="server" Text="Meters" Width="120px" Font-Bold="true"></asp:label>
                                                </td>
                                                <td style="width:400px;" valign="middle">
                                                    <asp:RadioButtonList Width="200px" ID="rblHalfHourMeterOnly" runat="server" RepeatLayout="table" RepeatDirection="Horizontal" AutoPostBack="true">
                                                        <asp:ListItem Value="0" Text="All Meters"></asp:ListItem>
                                                        <asp:ListItem Value="1" Text="Half Hour" Selected="True"></asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td style="width:120px;" valign="top">
                                                    <asp:label ID="lblFilterNameAvailable" runat="server" Text="Available Meters" Width="120px" Font-Bold="true"></asp:label>
                                                </td>
                                                <td style="width:400px;" valign="top">
                                                    <asp:RadioButtonList Width="450px" ID="rblMeterFilter" runat="server" RepeatLayout="table" RepeatDirection="Horizontal" AutoPostBack="true">
                                                        <asp:ListItem Value="All" Text="Show All" Selected="True"></asp:ListItem>
                                                        <asp:ListItem Value="A" Text="Elec"></asp:ListItem>
                                                        <asp:ListItem Value="B" Text="Gas"></asp:ListItem>
                                                        <asp:ListItem Value="C" Text="Sub"></asp:ListItem>
                                                        <asp:ListItem Value="D" Text="Virtual"></asp:ListItem>
                                                        <asp:ListItem Value="Custom" Text="Custom"></asp:ListItem>
                                                    </asp:RadioButtonList>
                                                    <div id="secFilter" runat="server" visible="false">
                                                        <asp:textbox ID="txtCustomFilter" runat="server" AutoPostBack="true" BackColor="#FFFFC0" style="width:400px;"></asp:textbox>
                                                        <asp:Button ID="btnFilter" runat="server" Text="Go" />
                                                    </div>
                                                </td>
                                            </tr>
                            
                                            <tr>
                                                <td colspan="2">
                                    
                                                </td>
                                            </tr>
                            
                                            <tr>
                                                <td colspan="2">
                                                    <asp:ListBox ID="lbMeterGroupingAvailable" runat="server" style="width:630px; height:150px;" SelectionMode="Single" AutoPostBack="true" BackColor="#FFFFC0" >
                                                    </asp:ListBox>
                                                </td>
                                            </tr>
                            
                                            <tr>
                                                <td>
                                                    <asp:label ID="lblFilterNameSelected" runat="server" Text="Assigned Meters" Width="120px" Font-Bold="true"></asp:label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblAssignedMeterError" Text="" runat="server" ForeColor="Red"></asp:Label>
                                                </td>
                                            </tr>
                            
                                            <tr>
                                                <td colspan="2">
                                                    <asp:ListBox ID="lbMeterGroupingAssigned" runat="server" style="width:630px; height:150px;" SelectionMode="Single" AutoPostBack="true" BackColor="#FFFFC0" >
                                                    </asp:ListBox>                                    
                                                </td>
                                            </tr>
                                            
                                        </table>
                                    </asp:Panel>
                                    <asp:Panel ID="pnlDeleteMeterGroupQuestion" runat="server" Visible="false" HorizontalAlign="Center">
                                        <div style="Z-INDEX: 125; LEFT:43%; POSITION: absolute; TOP: 250px; background-color:Silver; height:200px; width: 300px; border: 1px solid black; text-align:center;">
                                            <table style="width:270px;">
                                                <tr style="height:50px;">
                                                
                                                </tr>
                                                <tr>
                                                    <td style="width:20px;"></td>
                                                    <td colspan="2">
                                                        <asp:Label id="lblDeleteMeterGroupQuestion" runat="server" Width="250px" ForeColor="Red"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td>
                                                        <asp:Button ID="btnDeleteMeterGroupQuestionYes" runat="server" Text="Yes" Width="125px" />
                                                    </td>
                                                    <td>
                                                        <asp:Button ID="btnDeleteMeterGroupQuestionNo" runat="server" Text="No" Width="125px" />
                                                    </td>
                                                </tr>
                                            </table>
                                            
                                            
                                            
                                        </div>
                                    </asp:Panel>
                                </td>

                            </tr>

                            
                        </table>
                    </td>
                </tr>             
            </table>
        </div>
     
</form>
</asp:Content>

