Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Drawing
Imports System.Web
Imports System.Web.SessionState
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports Dundas.Charting.WebControl
Imports System.Data
Imports System.Data.OleDb
Imports System.Drawing.Text

Partial Class myapps_CustMeterSetup_CustomerMeterSetup
    Inherits System.Web.UI.Page

    Protected Sub CompanyUnitOfMeasure_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles CompanyUnitOfMeasure.Click
        Me.errorlabel.Text = ""
        Me.PanelMeterTemplates.Visible = False
        Me.PanelUnitOfMeasure.Visible = True
    End Sub

    Protected Sub CompanymeterTemplate_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles CompanymeterTemplate.Click
        Me.errorlabel.Text = " "
        Me.PanelMeterTemplates.Visible = True
        Me.PanelUnitOfMeasure.Visible = False
    End Sub

    Protected Sub GridView2_PageIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridView2.PageIndexChanged
        Me.errorlabel.Text = ""
        Dim strSQL As String = " Select * FROM tblCustomer_UnitsOfMeasure ORDER BY iName "
        Me.SQL_UnitOfMeasure.SelectCommand = strSQL
        ' Me.HiddenSelectedUoM.Value = mySelectedRow.Cells(1).Text
    End Sub

    Protected Sub GridView2_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView2.RowCommand
        Me.errorlabel.Text = " "
        On Error Resume Next
        If IsNumeric(e.CommandArgument) = True Then
            Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            Dim mySelectedRow As GridViewRow = GridView2.Rows(index)
            Me.HiddenSelectedUoM.Value = mySelectedRow.Cells(1).Text

            Me.txtMeasureName.Text = mySelectedRow.Cells(2).Text
            Me.txtMeasureAbbrevation.Text = mySelectedRow.Cells(3).Text

            Me.txtMultiplier.Text = mySelectedRow.Cells(5).Text

            If mySelectedRow.Cells(4).Text = "Y" Then
                Me.chkMultiplierRequired.Checked = True
            Else
                Me.chkMultiplierRequired.Checked = False
            End If

            If mySelectedRow.Cells(6).Text = "Y" Then
                Me.chkConversionRequired.Checked = True
            Else
                Me.chkConversionRequired.Checked = False
            End If

            Me.txtConvserionFactor.Text = mySelectedRow.Cells(7).Text
            Me.OperatorDropDownList.Text = mySelectedRow.Cells(8).Text
            Me.txtConversionDescription.Text = mySelectedRow.Cells(9).Text

            Me.ImageButtonUOM_save.Visible = True
            Me.ImageButtonUOM_Delete.Visible = True
            Me.ImageButtonUOM_New.Visible = True
            Me.ImageButtonUOM_Add.Visible = False

        End If

    End Sub




    Protected Sub ImageButtonUOM_save_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonUOM_save.Click
        Me.errorlabel.Text = " "
        Dim strSQL As String = ""
        If Trim(Me.txtMeasureName.Text) = "" Then
            '  MsgBox("Please enter a valid Measurement name", MsgBoxStyle.Critical, "Validation error")
            Me.errorlabel.Text = "Status: Please enter a valid Measurement name"
            Me.txtMeasureName.Focus()
            Exit Sub
        End If

        If Trim(Me.txtMeasureAbbrevation.Text) = "" Then
            'MsgBox("Please enter a valid Abbrevation name", MsgBoxStyle.Critical, "Validation error")
            Me.errorlabel.Text = "Status: Please enter a valid Abbrevation name"
            Me.txtMeasureAbbrevation.Focus()
            Exit Sub
        End If


        strSQL = ""
        strSQL = strSQL & "  Update tblCustomer_UnitsOfMeasure "
        strSQL = strSQL & "  Set iName=" & "'" & XY(Trim(Me.txtMeasureName.Text)) & "'"
        strSQL = strSQL & " , iAbbrevation=" & "'" & XY(Trim(Me.txtMeasureAbbrevation.Text)) & "'"

        If Me.chkMultiplierRequired.Checked = True Then
            If Not IsNumeric(Me.txtMultiplier.Text) Then
                'MsgBox("Please enter a valid Multipler", MsgBoxStyle.Critical, "Validation error")
                Me.errorlabel.Text = "Status: Please enter a valid Multipler"
                Me.txtMultiplier.Focus()
            Else
                strSQL = strSQL & " , iMultiplierRequired=" & "'" & "Y" & "'"
                strSQL = strSQL & " , iMultiplier=" & "'" & Trim(Me.txtMultiplier.Text) & "'"
            End If
        Else
            strSQL = strSQL & " , iMultiplierRequired=" & "'" & "N" & "'"
            strSQL = strSQL & " , iMultiplier=" & "'" & "0" & "'"
        End If

        If Me.chkConversionRequired.Checked = True Then
            If Not IsNumeric(Me.txtConvserionFactor.Text) Then
                'MsgBox("Please enter a valid Factor", MsgBoxStyle.Critical, "Validation error")
                Me.errorlabel.Text = "Status: Please enter a valid Factor"
                Me.txtConvserionFactor.Focus()
            Else
                strSQL = strSQL & " , iConversionRequired=" & "'" & "Y" & "'"
                strSQL = strSQL & " , iFactor=" & "'" & Trim(Me.txtConvserionFactor.Text) & "'"
            End If

            If Trim(Me.txtConversionDescription.Text) = "" Then
                'MsgBox("Please enter a valid Graph Abbrevation", MsgBoxStyle.Critical, "Validation error")
                Me.errorlabel.Text = "Status: Please enter a valid Graph Abbrevation"
                Me.txtConversionDescription.Focus()
            Else
                strSQL = strSQL & " , iDescription=" & "'" & XY(Trim(Me.txtConversionDescription.Text)) & "'"
            End If

            strSQL = strSQL & "  , iOperator=" & "'" & Me.OperatorDropDownList.Text & "'"

        Else
            strSQL = strSQL & " , iConversionRequired=" & "'" & "N" & "'"
            strSQL = strSQL & " , iFactor=" & "'" & "0" & "'"
            strSQL = strSQL & " , iOperator=" & "'" & "*" & "'"
            strSQL = strSQL & " , iDescription=" & "'" & XY(Trim(Me.txtConversionDescription.Text)) & "'"
        End If

        strSQL = strSQL & " Where ID=" & Me.HiddenSelectedUoM.Value

        Me.SQL_UnitOfMeasure.UpdateCommand = strSQL
        Me.SQL_UnitOfMeasure.Update()
        Me.GridView2.DataBind()
        strSQL = ""
        strSQL = "SELECT ID, rtrim(iName) AS 'iName' ,rtrim(iAbbrevation) AS 'iAbbrevation',iMultiplier,iMultiplierRequired,iConversionRequired,iFactor,iOperator,rtrim(iDescription) AS 'iDescription' FROM [vwCustomer_AllUnitOfMeasures] Order By iName"
        Me.SQL_UnitOfMeasure.SelectCommand = strSQL



    End Sub

    Protected Sub ImageButtonUOM_Add_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonUOM_Add.Click
        Me.errorlabel.Text = " "
        ' save
        ' select id , CompanyID , iName , iAbbrevation from dbo.UnitsOfMeasure
        Dim strSQL As String = ""
        Dim iiMultiplierRequired As String = ""
        Dim iiMultiplier As String = ""
        Dim iiConversionRequired As String = ""
        Dim iiFactor As String = ""
        Dim iiDescription As String = ""
        Dim iiOperator As String = ""

        If Trim(Me.txtMeasureName.Text) = "" Then
            'MsgBox("Please enter a valid Measurement name", MsgBoxStyle.Critical, "Validation error")
            Me.errorlabel.Text = "Status: Please enter a valid Measurement name"
            Me.txtMeasureName.Focus()
            Exit Sub
        End If

        If Trim(Me.txtMeasureAbbrevation.Text) = "" Then
            'MsgBox("Please enter a valid Abbrevation name", MsgBoxStyle.Critical, "Validation error")
            Me.errorlabel.Text = "Status: Please enter a valid Abbrevation name"
            Me.txtMeasureAbbrevation.Focus()
            Exit Sub
        End If

        '========================
        If Me.chkMultiplierRequired.Checked = True Then
            If Not IsNumeric(Me.txtMultiplier.Text) Then
                ' MsgBox("Please enter a valid Multipler", MsgBoxStyle.Critical, "Validation error")
                Me.errorlabel.Text = "Status: Please enter a valid Multipler"
                Me.txtMultiplier.Focus()
            Else
                iiMultiplierRequired = "Y"
                iiMultiplier = Trim(Me.txtMultiplier.Text)
            End If
        Else
            iiMultiplierRequired = "N"
            iiMultiplier = "0"
        End If

        If Me.chkConversionRequired.Checked = True Then
            If Not IsNumeric(Me.txtConvserionFactor.Text) Then
                Me.errorlabel.Text = "Status: Please enter a valid Factor"
                'MsgBox("Please enter a valid Factor", MsgBoxStyle.Critical, "Validation error")
                Me.txtConvserionFactor.Focus()
            Else
                iiConversionRequired = "Y"
                iiFactor = Trim(Me.txtConvserionFactor.Text)
            End If

            If Trim(Me.txtConversionDescription.Text) = "" Then
                'MsgBox("Please enter a valid Graph Abbrevation", MsgBoxStyle.Critical, "Validation error")
                Me.errorlabel.Text = "Status: Please enter a valid Graph Abbrevation"
                Me.txtConversionDescription.Focus()
            Else
                iiDescription = Trim(Me.txtConversionDescription.Text)
            End If

            iiOperator = Me.OperatorDropDownList.Text

        Else
            iiConversionRequired = "N"
            iiFactor = "0"
            iiOperator = "*"
            iiDescription = "N/A"
        End If

        '============================
        strSQL = " INSERT INTO tblCustomer_UnitsOfMeasure (iName,iAbbrevation,"
        strSQL = strSQL & " iMultiplierRequired,iMultiplier, "
        strSQL = strSQL & " iConversionRequired,iFactor,iDescription,iOperator) "
        strSQL = strSQL & " Values("
        strSQL = strSQL & "'" & XY(Trim(Me.txtMeasureName.Text)) & "'" & ","
        strSQL = strSQL & "'" & XY(Trim(Me.txtMeasureAbbrevation.Text)) & "'" & ","
        strSQL = strSQL & "'" & iiMultiplierRequired & "'" & ","
        strSQL = strSQL & "'" & iiMultiplier & "'" & ","
        strSQL = strSQL & "'" & iiConversionRequired & "'" & ","
        strSQL = strSQL & "'" & iiFactor & "'" & ","
        strSQL = strSQL & "'" & XY(iiDescription) & "'" & ","
        strSQL = strSQL & "'" & iiOperator & "'"
        strSQL = strSQL & ")"

        SQL_UnitOfMeasure.InsertCommand = strSQL
        SQL_UnitOfMeasure.Insert()

        strSQL = ""
        strSQL = "SELECT ID, rtrim(iName) AS 'iName' ,rtrim(iAbbrevation) AS 'iAbbrevation',iMultiplier,iMultiplierRequired,iConversionRequired,iFactor,iOperator,rtrim(iDescription) AS 'iDescription' FROM [vwCustomer_AllUnitOfMeasures] Order By iName"
        Me.SQL_UnitOfMeasure.SelectCommand = strSQL
        Me.txtMeasureName.Text = ""
        Me.txtMeasureAbbrevation.Text = ""
        Me.ImageButtonUOM_save.Visible = True
        Me.ImageButtonUOM_Delete.Visible = True
        Me.ImageButtonUOM_New.Visible = True
        Me.ImageButtonUOM_Add.Visible = False

    End Sub

    Protected Sub ImageButtonUOM_New_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonUOM_New.Click
        Me.errorlabel.Text = " "
        Me.txtMeasureName.Text = ""
        Me.txtMeasureAbbrevation.Text = ""
        Me.chkConversionRequired.Checked = False
        Me.chkMultiplierRequired.Checked = False
        Me.txtMultiplier.Text = 0
        Me.txtConvserionFactor.Text = 0
        Me.txtConversionDescription.Text = "N/A"
        Me.OperatorDropDownList.Text = "*"
        Me.ImageButtonUOM_save.Visible = False
        Me.ImageButtonUOM_Delete.Visible = False
        Me.ImageButtonUOM_New.Visible = False
        Me.ImageButtonUOM_Add.Visible = True
        Me.txtMeasureName.Focus()
    End Sub

    Protected Sub ImageButtonUOM_Delete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonUOM_Delete.Click
        Me.errorlabel.Text = ""
        '   MsgBox(Me.HiddenSelectedUoM.Value)
        If Me.HiddenSelectedUoM.Value = "" Then
            'MsgBox("Please select a Valid Unit", MsgBoxStyle.Information, "Validation error")
            Me.errorlabel.Text = "Status: Please select a Valid Unit"
            Exit Sub
        End If

        Dim nReturn As String = ""
        Dim strSQL As String = ""
        strSQL = " SELECT COUNT(UnitOfmeasureID) AS 'total'  FROM dbo.tblCustomer_MeterTypes WHERE UnitOfmeasureID = " & Me.HiddenSelectedUoM.Value
        Dim myConnection As New OleDb.OleDbConnection(Core.CurrentConnectionString)
        Dim myCommand As New OleDb.OleDbCommand(strSQL, myConnection)
        myCommand.Connection.Open()
        myCommand.CommandTimeout = 180
        Dim Reader As OleDb.OleDbDataReader = myCommand.ExecuteReader(CommandBehavior.CloseConnection)
        If Reader.HasRows Then
            Reader.Read()
            If Reader("total") > 0 Then
                ' MsgBox("You may NOTdelete this particular Unit," & vbNewLine & " as there are Templates that require this measurement")
                Me.errorlabel.Text = "You may NOTdelete this particular Unit,Templates that require this measurement"
                Reader.Close()
            Else

                strSQL = ""
                strSQL = strSQL & "  DELETE FROM  tblCustomer_UnitsOfMeasure  WHERE ID = " & Me.HiddenSelectedUoM.Value

                Me.SQL_UnitOfMeasure.DeleteCommand = strSQL
                Me.SQL_UnitOfMeasure.Delete()
                Me.GridView2.DataBind()

                strSQL = ""
                strSQL = "SELECT ID, rtrim(iName) AS 'iName' ,rtrim(iAbbrevation) AS 'iAbbrevation',iMultiplier,iMultiplierRequired,iConversionRequired,iFactor,iOperator,rtrim(iDescription) AS 'iDescription' FROM [vwCustomer_AllUnitOfMeasures] Order By iName"
                Me.SQL_UnitOfMeasure.SelectCommand = strSQL

            End If
        End If

    End Sub

    Protected Sub GridView1_PageIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridView1.PageIndexChanged
        Me.errorlabel.Text = " "
        Dim strSQL As String = "select * from dbo.vwCustomer_Show_MeterTypes Order By MeterName"
        Me.SQL_TemplateTypes.SelectCommand = strSQL
    End Sub


    Protected Sub GridView1_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView1.RowCommand
        Me.errorlabel.Text = ""
        On Error Resume Next
        If IsNumeric(e.CommandArgument) = True Then
            Dim index As Int32 = Convert.ToInt32(e.CommandArgument)
            Dim mySelectedRow As GridViewRow = GridView1.Rows(index)
            Dim iSelectedText As String = ""
            iSelectedText = Trim(mySelectedRow.Cells(3).Text) & " ~ " & Trim(mySelectedRow.Cells(4).Text)
            DropDownList2.SelectedIndex = DropDownList2.Items.IndexOf(DropDownList2.Items.FindByText(iSelectedText))
            Me.txtMeterTypeName.Text = mySelectedRow.Cells(2).Text

            HiddenSelectedMeterType.Value = mySelectedRow.Cells(1).Text

            Me.txtDigits.Text = mySelectedRow.Cells(5).Text

            Me.ImageButtonTemplate_Save.Visible = True
            Me.ImageButtonTemplate_Delete.Visible = True
            Me.ImageButtonTemplate_New.Visible = True
            Me.ImageButtonTemplate_Add.Visible = False
        End If
    End Sub


    Protected Sub ImageButtonTemplate_Save_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonTemplate_Save.Click
        Me.errorlabel.Text = ""
        Dim strSQL As String = ""
        If Trim(Me.txtMeterTypeName.Text) = "" Then
            'MsgBox("Please enter a valid Name Type", MsgBoxStyle.Critical, "Validation error")
            Me.errorlabel.Text = "Status: Please enter a valid Name Type"
            Me.txtMeterTypeName.Focus()
            Exit Sub
        End If

        If Trim(Me.txtDigits.Text) = "" Then
            'MsgBox("Please enter a valid Face Digits", MsgBoxStyle.Critical, "Validation error")
            Me.errorlabel.Text = "Status: Please enter a valid Face Digit"
            Me.txtDigits.Focus()
            Exit Sub
        End If

        strSQL = ""
        strSQL = strSQL & "  Update tblCustomer_MeterTypes "
        strSQL = strSQL & "  set NameType=" & "'" & XY(Trim(txtMeterTypeName.Text)) & "'"
        strSQL = strSQL & " , UnitOfMeasureID=" & DropDownList2.SelectedValue
        strSQL = strSQL & " , NoDigits=" & "'" & Me.txtDigits.Text & "'"
        strSQL = strSQL & "  Where ID=" & HiddenSelectedMeterType.Value

        '<asp:SqlDataSource ID="SQL_TemplateTypes" runat="server" 
        '        ConnectionString = "<%$ ConnectionStrings:portal.utilitymasters.co.ukConnectionString %>"
        'SelectCommand="select * from dbo.vwCustomer_Show_MeterTypes" >
        '</asp:SqlDataSource> 

        Me.SQL_TemplateTypes.UpdateCommand = strSQL
        Me.SQL_TemplateTypes.Update()
        Me.GridView1.DataBind()

        strSQL = "select * from dbo.vwCustomer_Show_MeterTypes Order By MeterName "
        Me.SQL_TemplateTypes.SelectCommand = strSQL
    End Sub


    Protected Sub ImageButtonTemplate_Add_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonTemplate_Add.Click
        Me.errorlabel.Text = " "
        If Trim(Me.txtMeterTypeName.Text) = "" Then
            'MsgBox("Please enter a valid Name Type", MsgBoxStyle.Critical, "Validation error")
            Me.errorlabel.Text = "Status: Please enter a valid Name Type"
            Me.txtMeterTypeName.Focus()
            Exit Sub
        End If

        If Trim(Me.txtDigits.Text) = "" Then
            'MsgBox("Please enter a valid Face Digits", MsgBoxStyle.Critical, "Validation error")
            Me.errorlabel.Text = "Status: Please enter a valid Face Digits"
            Me.txtDigits.Focus()
            Exit Sub
        End If

        Me.ImageButtonTemplate_Save.Visible = True
        Me.ImageButtonTemplate_Delete.Visible = True
        Me.ImageButtonTemplate_New.Visible = True
        Me.ImageButtonTemplate_Add.Visible = False

        Dim strSQL As String = ""
        strSQL = " INSERT INTO tblCustomer_MeterTypes (NameType,UnitOfMeasureID,NoDigits) "
        strSQL = strSQL & " Values("
        strSQL = strSQL & "'" & XY(Me.txtMeterTypeName.Text) & "'" & ","
        strSQL = strSQL & "'" & DropDownList2.SelectedValue & "'" & ","
        strSQL = strSQL & "'" & Me.txtDigits.Text & "'"
        strSQL = strSQL & ")"

        Me.SQL_TemplateTypes.InsertCommand = strSQL
        Me.SQL_TemplateTypes.Insert()

        strSQL = "select * from dbo.vwCustomer_Show_MeterTypes Order By MeterName"
        Me.SQL_TemplateTypes.SelectCommand = strSQL

        Me.txtMeterTypeName.Text = ""
        Me.txtDigits.Text = "5"
        Me.txtMeterTypeName.Focus()
        Me.DropDownList2.SelectedIndex = "0"

        strSQL = "select * from dbo.vwCustomer_Show_MeterTypes Order By MeterName "
        Me.SQL_TemplateTypes.SelectCommand = strSQL
    End Sub

    Protected Sub ImageButtonTemplate_New_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonTemplate_New.Click
        Me.errorlabel.Text = " "
        Me.ImageButtonTemplate_Save.Visible = False
        Me.ImageButtonTemplate_Delete.Visible = False
        Me.ImageButtonTemplate_New.Visible = False
        Me.ImageButtonTemplate_Add.Visible = True

        Me.txtMeterTypeName.Text = ""
        Me.txtDigits.Text = "5"
        Me.txtMeterTypeName.Focus()
        Me.DropDownList2.SelectedIndex = "0"

        '        strSQL = "select * from dbo.vwCustomer_Show_MeterTypes "
        '        Me.SQL_TemplateTypes.SelectCommand = strSQL
    End Sub

    Protected Sub ImageButtonTemplate_Delete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonTemplate_Delete.Click
        Me.errorlabel.Text = ""
        If Me.HiddenSelectedMeterType.Value = "" Then
            'MsgBox("Please select a Valid Template", MsgBoxStyle.Information, "Validation error")
            Me.errorlabel.Text = "Status: Please select a Valid Template"
            Exit Sub
        End If

        Dim nReturn As String = ""
        Dim strSQL As String = ""
        strSQL = "  SELECT * FROM vwCustomer_Templates_In_Use WHERE TypeID= " & Me.HiddenSelectedMeterType.Value
        Dim myConnection As New OleDb.OleDbConnection(Core.CurrentConnectionString)
        Dim myCommand As New OleDb.OleDbCommand(strSQL, myConnection)
        myCommand.Connection.Open()
        myCommand.CommandTimeout = 180
        Dim Reader As OleDb.OleDbDataReader = myCommand.ExecuteReader(CommandBehavior.CloseConnection)
        If Reader.HasRows Then
            ' While Reader.Read()
            'MsgBox("You may NOTdelete this particular Template," & vbNewLine & " as there are still recorded Meter Readings")
            Me.errorlabel.Text = "Status: You may NOTdelete this particular Template, as there are still recorded Meter Readings"
            Reader.Close()
        Else
            Reader.Close()

            strSQL = ""
            strSQL = strSQL & "  DELETE FROM tblCustomer_MeterTypes "
            strSQL = strSQL & " Where ID=" & HiddenSelectedMeterType.Value

            Me.SQL_TemplateTypes.DeleteCommand = strSQL
            Me.SQL_TemplateTypes.Delete()
            Me.GridView1.DataBind()

            strSQL = "select * from dbo.vwCustomer_Show_MeterTypes Order By MeterName "
            Me.SQL_TemplateTypes.SelectCommand = strSQL

        End If

        'MsgBox(Me.HiddenSelectedMeterType.Value)
        'strsql = "  SELECT * FROM vwCustomer_Templates_In_Use WHERE TypeID= " & Me.HiddenSelectedMeterType.Value

    End Sub


    Protected Function XY(ByVal what As String) As String
        ' use this to combat the single quote in SQL  from john's to john''s other wise it won't save
        ' only use for the text part of a field
        XY = Replace(what, "'", "''")
    End Function


End Class
