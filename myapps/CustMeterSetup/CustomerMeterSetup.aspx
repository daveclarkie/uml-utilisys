<%@ Page Language="VB" MasterPageFile="~/landing.master" AutoEventWireup="false" CodeFile="CustomerMeterSetup.aspx.vb" Inherits="myapps_CustMeterSetup_CustomerMeterSetup" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="content_right" Runat="Server">
   <div class="content_back my_admin">
       Customer meter setup
  
<form id="form1" runat="server">

<%--The Header--%>
<asp:Panel ID="PanelHeader"  
     style="position:absolute;top:125px;left:235px;"   
     runat="server" Width="600px" Height="55px">
     
    <asp:Label ID="errorlabel"  ForeColor="red" style="position:absolute;top:-5px;left:0px;"   runat="server" Text=""></asp:Label>
     
            <asp:ImageButton ID="CompanyUnitOfMeasure"    
            style="position:absolute;top:12px;left:0px;"   
             tooltip="Units of measurements administration"  
             Height="38px"  
             Width="38px"  
             ImageUrl="~/assets/metering/NewUOM.png" runat="server" />
             
            <asp:ImageButton ID="CompanymeterTemplate"  
            style="position:absolute;top:12px;left:45px;"  
            tooltip="Meter template  administration" 
            Height="38px" 
            Width="38px"  
            ImageUrl="~/assets/metering/details.png" runat="server" />
</asp:Panel>


<%--The Panels --%>
<asp:Panel ID="PanelUnitOfMeasure" 
              BorderStyle="None"
              style="position:absolute;top:180px;left:235px;"  
              Width="600px" Height="400px" visible="false" runat="server">
                <asp:Label ID="Label11" ForeColor="darkblue"   font-bold="true"  runat="server" Text="Unit of measurement"></asp:Label>
                <br /> 
 <asp:ImageButton ID="ImageButtonUOM_save"  visible="true"  tooltip="Update unit of measurment details"   Height="25px" Width="25px"  ImageUrl="~/assets/metering/smallsave.png" runat="server" />
 <asp:ImageButton ID="ImageButtonUOM_Delete"  visible="true"  tooltip="Delete this unit"  Height="25px" Width="25px"  ImageUrl="~/assets/metering/smalcancel.png" runat="server" />
 <asp:ImageButton ID="ImageButtonUOM_New"  visible="true"  tooltip="Create a new measurement"  Height="25px" Width="25px"  ImageUrl="~/assets/metering/newreadings.png" runat="server" />
 <asp:ImageButton ID="ImageButtonUOM_Add"  visible="false"  tooltip="Append new unit to database"   Height="25px" Width="25px"  ImageUrl="~/assets/metering/smallsave.png" runat="server" />
<br />                
                
                 <asp:Label ID="Label1" runat="server"  font-names="verdana"  Font-Size="12px"   width="150px"  Text="Unit title" ></asp:Label>
                <asp:TextBox ID="txtMeasureName" maxlength="100" runat="server"  font-names="verdana"  Font-Size="12px"   width="300px" BackColor="#B9DDFE" ForeColor="black"></asp:TextBox>
                <br />
                <asp:Label ID="Label2" runat="server" width="150px" font-names="verdana"  Font-Size="12px"   Text="Abbrevation" ></asp:Label>
                <asp:TextBox ID="txtMeasureAbbrevation" maxlength="40" font-names="verdana"  Font-Size="12px"  width="150px" runat="server"  AutoCompleteType="Disabled" BackColor="#B9DDFE"  ForeColor="black"></asp:TextBox>
                 <br />
                 
                 <asp:CheckBox ID="chkMultiplierRequired"  font-names="verdana"  Font-Size="12px"  runat="server" width="150px" Text=" Multiplier Required " />
                <asp:Label ID="Label18" runat="server" font-names="verdana"  Font-Size="12px"  width="50px"  Text="Multiplier" ></asp:Label>  &nbsp;
                 <asp:TextBox ID="txtMultiplier" runat="server"   maxlength="15" font-names="verdana"  Font-Size="12px"  width="40px" BackColor="#B9DDFE" ForeColor="black"></asp:TextBox>
                 <br />
                
                 <asp:CheckBox ID="chkConversionRequired" font-names="verdana"  Font-Size="12px"  runat="server" width="150px" Text=" Conversion Required " />
                 <asp:Label ID="factor" runat="server" font-names="verdana"  Font-Size="12px"  width="50px"  Text="Factor " ></asp:Label>  &nbsp;
                 <asp:TextBox ID="txtConvserionFactor"    maxlength="15"  font-names="verdana"  Font-Size="12px"  runat="server"   width="80px" BackColor="#B9DDFE" ForeColor="black"></asp:TextBox>
                  &nbsp;
                 <asp:Label ID="Label29" runat="server" font-names="verdana"  Font-Size="12px"  width="50px"  Text="Operator " ></asp:Label>
                 &nbsp;
                 <asp:DropDownList ID="OperatorDropDownList"  BackColor="#B9DDFE" font-names="verdana"  Font-Size="12px"  runat="server" Width="45px" >
                <asp:ListItem>*</asp:ListItem>
                <asp:ListItem>/</asp:ListItem>
                <asp:ListItem>+</asp:ListItem>
                <asp:ListItem>-</asp:ListItem>   
                    </asp:DropDownList>
                    <br />
           <asp:Label ID="Convseriondescription" runat="server" font-names="verdana"  Font-Size="12px"   width="150px"  Text="Output abbrevation " ></asp:Label>
            <asp:TextBox ID="txtConversionDescription" maxlength="100" runat="server" width="250px"  font-names="verdana"  Font-Size="12px"   BackColor="#B9DDFE" ForeColor="black"></asp:TextBox>
            <br />
       
 
                   
<asp:GridView ID="GridView2" 
runat="server" 
AutoGenerateColumns="False" 
CellPadding="0"
DataSourceID="SQL_UnitOfMeasure" 
Font-Size="12px" 
Font-Names="Arial" 
GridLines="None" 
AllowPaging="True" 
PageSize="10" 
ShowHeader="True"
HorizontalAlign="Left"  
backcolor="Transparent"
Width="377px" 
Height="83px">

<PagerSettings PageButtonCount="5" />
 
<Columns>
         <asp:CommandField ShowSelectButton="True" SelectText="Select">  
         <ItemStyle  Width="20px" Height="12px"/>
         </asp:CommandField>

          <asp:BoundField DataField="ID" HeaderText="Unit"   itemstyle-cssclass="hiddencol" > 
          <ItemStyle CssClass="hiddencol" />
          </asp:BoundField>

          <asp:BoundField DataField="iName" HeaderText="Abbrevation">
           <ItemStyle  Width="200px" Height="12px" />
          </asp:BoundField>
        
          <asp:BoundField DataField="iAbbrevation" HeaderText="">
          <ItemStyle  Width="75px" Height="12px"/>
           </asp:BoundField>
    
        <asp:BoundField DataField="iMultiplierRequired"   itemstyle-cssclass="hiddencol" >  <ItemStyle CssClass="hiddencol" /></asp:BoundField>
        <asp:BoundField DataField="iMultiplier"   itemstyle-cssclass="hiddencol" >  <ItemStyle CssClass="hiddencol" /></asp:BoundField>
        <asp:BoundField DataField="iConversionRequired"   itemstyle-cssclass="hiddencol" >  <ItemStyle CssClass="hiddencol" /></asp:BoundField>
        <asp:BoundField DataField="iFactor"   itemstyle-cssclass="hiddencol" >  <ItemStyle CssClass="hiddencol" /></asp:BoundField>        
        <asp:BoundField DataField="iOperator"   itemstyle-cssclass="hiddencol" >  <ItemStyle CssClass="hiddencol" /></asp:BoundField>        
        <asp:BoundField DataField="iDescription"   itemstyle-cssclass="hiddencol" >  <ItemStyle CssClass="hiddencol" /></asp:BoundField>        
</Columns>

<SelectedRowStyle BackColor="#B9DDFE" Font-Bold="True" ForeColor="black" />

</asp:GridView>     

</asp:Panel>





<%--MeterTemplates    --%>
<asp:Panel ID="PanelMeterTemplates" 
        BorderStyle="None"
        style="position:absolute;top:180px;left:235px;"  
        Width="600px" 
        Height="400px"
         visible="false" runat="server">

                      <asp:Label ID="Label3" runat="server" ForeColor="darkblue"   font-bold="true" Text="Meter Templates"></asp:Label>
                      <br />         
<asp:ImageButton ID="ImageButtonTemplate_Save"  visible="true"  tooltip="Update template details"   Height="25px" Width="25px"  ImageUrl="~/assets/metering/smallsave.png" runat="server" />
<asp:ImageButton ID="ImageButtonTemplate_Delete"  visible="true"  tooltip="Delete this template"  Height="25px" Width="25px"  ImageUrl="~/assets/metering/smalcancel.png" runat="server" />
<asp:ImageButton ID="ImageButtonTemplate_New"  visible="true"  tooltip="Create a new template"  Height="25px" Width="25px"  ImageUrl="~/assets/metering/newreadings.png" runat="server" />
<asp:ImageButton ID="ImageButtonTemplate_Add"  visible="false"  tooltip="Append this template database"   Height="25px" Width="25px"  ImageUrl="~/assets/metering/smallsave.png" runat="server" />
    <br />  
                        
                            <asp:Label ID="Label4" runat="server"   font-names="verdana"  Font-Size="12px"  width="100px" Text="Template title" ></asp:Label>
                                <asp:TextBox ID="txtMeterTypeName" maxlength="100"  runat="server"   font-names="verdana"  Font-Size="12px"   width="300px" BackColor="#B9DDFE" ForeColor="black"></asp:TextBox>
                                         <br />
                                <asp:Label ID="Label5" runat="server"    font-names="verdana"  Font-Size="12px"  width="100px" Text="UoM" ></asp:Label>
                                <asp:DropDownList ID="DropDownList2" 
                                BackColor="#B9DDFE" 
                                DataValueField="ID"
                                DataTextField="iNames" 
                                DataSourceID="SQL_UOMsummary"
                                Font-Size="12px" 
                                Font-Names="Arial" 
                                runat="server" >
                                </asp:DropDownList>
                                
                                
                                         <br />
                                <asp:Label ID="Label19" runat="server"   font-names="verdana"  Font-Size="12px"  width="100px" Text="Face digits" ></asp:Label>
                                <asp:TextBox ID="txtDigits" runat="server"    maxlength="3" font-names="verdana"  Font-Size="12px"  width="30px" BackColor="#B9DDFE" ForeColor="black"></asp:TextBox>
                                <br />
                       <br />

                <asp:GridView id="GridView1" 
                runat="server" 
                Font-Size="12px" 
                Font-Names="Arial" 
                DataSourceID="SQL_TemplateTypes" 
                ShowHeader="true" 
                DataKeyNames="ID" 
                BorderStyle="None"
                PageSize="10"
                GridLines="None" 
                backcolor="transparent"
                AutoGenerateColumns="False" 
                AllowPaging="True">
                <Columns>

                <asp:CommandField ShowSelectButton="True" SelectText="Select">  
                <ItemStyle  Width="20px" Height="14px"/>
                </asp:CommandField>

                <asp:BoundField DataField="ID" ReadOnly="True" SortExpression="ID" HeaderText="Name" >
                <ItemStyle CssClass="hiddencol"></ItemStyle>
                </asp:BoundField>

                <asp:BoundField DataField="MeterName" HeaderText="Unit" SortExpression="metername">
                <ItemStyle  Width="200px" Height="12px"/>
                </asp:BoundField>

                <asp:BoundField DataField="iname" HeaderText="Abbrevation" SortExpression="iname">
                <ItemStyle  Width="150px" Height="12px"/>
                </asp:BoundField>

                <asp:BoundField DataField="iAbbrevation" HeaderText="Digits" SortExpression="iAbbrevation">
                <ItemStyle  Width="100px" Height="12px"/>
                </asp:BoundField>

                <asp:BoundField DataField="NoDigits" HeaderText="" SortExpression="NoDigits">
                <ItemStyle  Width="40px" Height="12px"/>
                </asp:BoundField>

                <asp:BoundField DataField="UnitOfmeasureID"   ReadOnly="True" SortExpression="UnitOfmeasureID">
                <ItemStyle CssClass="hiddencol"></ItemStyle>
                </asp:BoundField>

                </Columns>


                <PagerSettings PageButtonCount="5" />
<SelectedRowStyle BackColor="#B9DDFE" Font-Bold="True" ForeColor="Black" />            

                </asp:GridView>

</asp:Panel>
  
  
 
  <asp:HiddenField ID="HiddenSelectedMeterType" runat="server" /> 
  <asp:HiddenField ID="HiddenSelectedUoM" runat="server"/> 

  </form>
  
  </div> 
 
<asp:SqlDataSource ID="SQL_UnitOfMeasure" runat="server" 
ConnectionString="<%$ ConnectionStrings:portal.utilitymasters.co.ukConnectionString %>" 
SelectCommand="SELECT ID, rtrim(iName) AS 'iName' ,rtrim(iAbbrevation) AS 'iAbbrevation',iMultiplier,iMultiplierRequired,iConversionRequired,iFactor,iOperator,rtrim(iDescription) AS 'iDescription' FROM [vwCustomer_AllUnitOfMeasures] Order by iName" >
</asp:SqlDataSource> 

 
<asp:SqlDataSource ID="SQL_TemplateTypes" runat="server" 
ConnectionString="<%$ ConnectionStrings:portal.utilitymasters.co.ukConnectionString %>" 
SelectCommand="select * from dbo.vwCustomer_Show_MeterTypes ORDER BY Metername" >
</asp:SqlDataSource> 

<asp:SqlDataSource ID="SQL_UOMsummary" runat="server" 
ConnectionString="<%$ ConnectionStrings:portal.utilitymasters.co.ukConnectionString %>" 
SelectCommand="select id,rtrim(iname) + ' ~ ' + rtrim(iabbrevation) as 'iNames' from dbo.vwCustomer_AllUnitOfMeasures Order by iName" >
</asp:SqlDataSource> 

</asp:Content>


