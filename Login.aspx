<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Login.aspx.vb" Inherits="Login_New" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>UTILISYS Login</title>
    
    <style type="text/css" >
        @import url(/css/reset.css);
        .LoginButton {text-indent:-9999px; display:block; width:119px; height:37px; background: transparent url('/assets/Schneider/login.png') no-repeat;}
        /*.LoginButton:hover {text-indent:-9999px; display:block; width:119px; height:37px; background: transparent url('/assets/Schneider/login-hover.png') no-repeat;cursor:pointer;}*/
        
        .SendEmailButton {text-indent:-9999px; display:block; width:119px; height:37px; background: transparent url('/assets/Schneider/buttons-general-sendemail.png') no-repeat;}
        .SendEmailButton:hover {text-indent:-9999px; display:block; width:119px; height:37px; background: transparent url('/assets/Schneider/buttons-general-sendemail-hover.png') no-repeat;cursor:pointer;}
         
        .CancelEmailButton {text-indent:-9999px; display:block; width:119px; height:37px; background: transparent url('/assets/Schneider/buttons-general-cancel.png') no-repeat;}
        .CancelEmailButton:hover {text-indent:-9999px; display:block; width:119px; height:37px; background: transparent url('/assets/Schneider/buttons-general-cancel-hover.png') no-repeat;cursor:pointer;}

        .ContinueButton {text-indent:-9999px; display:block; width:119px; height:37px; background: transparent url('/assets/Schneider/buttons-general-continue.png') no-repeat;}
        .ContinueButton:hover {text-indent:-9999px; display:block; width:119px; height:37px; background: transparent url('/assets/Schneider/buttons-general-continue-hover.png') no-repeat;cursor:pointer;}
         
        .MarketingEmailButton {text-indent:-9999px; cursor:pointer; display:block; width:450px; height:100px; background: transparent url('/assets/Schneider/button_loginmarketingmessage.png') no-repeat;}
        .MarketingEmailButton:hover {text-indent:-9999px; cursor:pointer; display:block; width:450px; height:100px; background: transparent url('/assets/Schneider/button_loginmarketingmessage_hover.png') no-repeat;}
         
                
         html {font-size: 62.5%; color: #6b6b6b;}
         body 
            {
                background-color: #fff;
	            text-align: center; 
	            width:100%;
	            font: normal 1.19em/1.7em "Verdana", "Helvetica", "Arial", sans-serif;
	            letter-spacing: 0em;
            }
        .style1
        {
            color: #FFFFFF;
        }
    </style>
    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-22664232-1']);
        _gaq.push(['_trackPageview']);

        (function () {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();

</script>
</head>
<body>
    <form id="form1" runat="server">

        <table style="text-align:center; width:100%;">
            <tr>
                <td style="height: 63px">
                    <%--<img src="assets/branding.jpg" alt="Utility Masters" />--%>
                </td>
            </tr>
            <tr>
                <td style="height: 63px" align="center">
                    <%--<a style="font-weight:bold; font-size:24pt; color:#082B61;">UTILI</a><a style="font-size:24pt;color:#082B61;">SYS Portal</a>--%>
                    <img src="assets/Schneider/loginheader.png" alt="Utility Masters" />
                </td>
            </tr>
            <tr>
                <td style="height:30px; ">
                
                </td>
            </tr>
            <tr>
                <td style="height: 63px;" align="center">
                    <asp:Panel runat="server" ID="pnlLogin">
                    <table style="width: 450px; height:250px; background-image:url(assets/Login_BoxBackground2.png);">
                        <tr>
                            <td style="width:20px;"></td>
                            <td>
                                <table style="width: 400px; height:200px;">
                                    <tr>
                                        <td style="height:30px" colspan="3">
                                            <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
                                            <asp:Label ID="Label2" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width:30px;">
                                        </td>
                                        <td align="left">
                                            <asp:Label ID="lblUsername" runat="server" Text="Email Address" ForeColor="#FFFFFF"></asp:Label>
                                        </td>
                                        <td align="left">
                                            <asp:TextBox ID="txtUsername" runat="server" Width="240px"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td align="left">
                                            <asp:Label ID="lblPassword" runat="server" Text="Password" ForeColor="#FFFFFF"></asp:Label>
                                        </td>
                                        <td align="left">
                                            <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" Width="240px"></asp:TextBox>
                                        </td>
                                    </tr>
                                    
                                    <tr>
                                        <td>
                                        </td>
                                        <td align="left">
                                        </td>
                                        <td align="left">
                                            <asp:LinkButton ID="hypForgotPassword" runat="server" Text="forgot your password?" Visible="true"  ForeColor="#FFFFC0"></asp:LinkButton>
                                        </td>
                                    </tr>
                                    
                                    <tr>
                                        <td>
                                        </td>
                                        <td colspan="2">
                                            <asp:Label ID="lblInvalidUsername" runat="server" Text="" ForeColor="red"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="height:10px" colspan="3">
                                            
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right" colspan="3">
                                            <table>
                                                <tr>
                                                    <td align="right">
                                                        <asp:Button ID="btnLogin" runat="server" BorderStyle="None" CssClass="LoginButton" />
                                                    </td>
                                                    <td style="width:25px;">
                                                    
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr> 
                                    <tr>
                                        <td style="height:10px; text-align: left;" colspan="3">
                                            <span class="style1">&nbsp;v 3.00 | <%= GetApplicationVersion() %></span>
                                        </td>
                                    </tr>                       
                                </table>
                            </td>
                        </tr>
                    </table>
                    <table style="width: 450px; height:100px;">
                        <tr>
                            <td>
                                <asp:HyperLink ID="lnkMarketingEmailButton" runat="server" CssClass="MarketingEmailButton" NavigateUrl="mailto:dave.clarke@ems.schneider-electric.com&subject=Tell me more about UTILISYS"></asp:HyperLink>
                            </td>
                        </tr>
                    </table>

                    </asp:Panel>
                    <asp:Panel runat="server" ID="pnlForgotPassword" Visible="false">
                        <table style="width: 400px; background-color:#B7E566; border:solid 1px #000000;">
                        <tr>
                            <td style="height:30px" colspan="3">
                                <asp:label ID="lblForgotPassword" runat="server" Text="Please enter your email address and we will email the password we have on file for you." Width="300" ForeColor="White"></asp:label>
                            </td>
                        </tr>
                        <tr>
                            <td style="height:20px" colspan="3">
                                
                            </td>
                        </tr>
                        <tr>
                            <td style="width:30px;">
                            </td>
                            <td align="left">
                                <asp:Label ID="lblEmailPassword" runat="server" Text="Email Address" ForeColor="White"></asp:Label>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtEmailPassword" runat="server" Width="240px"></asp:TextBox>
                            </td>
                        </tr>
                        
                        <tr>
                            <td>
                            </td>
                            <td colspan="2">
                                <asp:Label ID="lblEmailPasswordError" runat="server" Text="" ForeColor="red"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="height:30px" colspan="3">
                                
                            </td>
                        </tr>
                        <tr>
                            <td align="right" colspan="3">
                                <table>
                                    <tr>
                                        <td align="left" >
                                            <asp:Button ID="btnCancelPassword" runat="server" BorderStyle="None" CssClass="CancelEmailButton" />
                                        </td>
                                        <td style="width:110px;">
                                        
                                        </td>
                                        <td align="right">
                                            <asp:Button ID="btnSendPassword" runat="server" BorderStyle="None" CssClass="SendEmailButton" />
                                        </td>
                                        <td style="width:25px;">
                                        
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr> 
                        <tr>
                            <td style="height:10px" colspan="3">
                                
                            </td>
                        </tr>                       
                    </table>
                    </asp:Panel>
                    <asp:Panel runat="server" ID="pnlConfirmEmailSent" Visible="false">
                        <table style="width: 400px; background-color:#B7E566; border:solid 1px #000000;">
                        <tr>
                            <td style="height:30px">
                                <asp:label ID="lblConfirmEmailSent" runat="server" Text="Your password has been emailed to you, please check your inbox." Width="300"  ForeColor="White"></asp:label>
                            </td>
                        </tr>
                        <tr>
                            <td style="height:30px">
                                
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <table>
                                    <tr>
                                        <td align="left" >
                                            
                                        </td>
                                        <td style="width:110px;">
                                        
                                        </td>
                                        <td align="right">
                                            <asp:Button ID="btnConfirmEmailSentClose" runat="server" BorderStyle="None" CssClass="ContinueButton" />
                                        </td>
                                        <td style="width:25px;">
                                        
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr> 
                        <tr>
                            <td style="height:10px">
                                
                            </td>
                        </tr>                       
                    </table>
                    </asp:Panel>
                </td>
            </tr>
        </table>
        <table style="text-align:center; width:100%;">
            <tr>
                <td style="height: 63px;" align="center">
                    
                    <table style="width: 450px; ">
                        <tr>
                            <td colspan="2">
                                <div id="thawteseal" style="text-align:center;" title="Click to Verify - This site chose Thawte SSL for secure e-commerce and confidential communications.">
                                <div><script type="text/javascript" src="https://seal.thawte.com/getthawteseal?host_name=portal.utilitymasters.co.uk&amp;size=S&amp;lang=en"></script></div>
                                <div><a href="http://www.thawte.com/products/" target="_blank" style="color:#000000; text-decoration:none; font:bold 10px arial,sans-serif; margin:0px; padding:0px;">ABOUT SSL CERTIFICATES</a></div>
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
