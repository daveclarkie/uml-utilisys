<%@ Control Language="VB" AutoEventWireup="false" CodeFile="nav-my-apps.ascx.vb" Inherits="nav_my_apps" %>
<div id="left_nav_apps" class="left_nav">
    <div>

        <asp:hyperlink runat="server" id="lnkMyApps" navigateurl="~/my-apps.aspx" cssclass="nav_home" tooltip="Your Applications" />
        <h2 class="nav_heading"><asp:hyperlink runat="server" id="lnkAppsHome" ForeColor="#009530"  navigateurl="~/my-apps.aspx" tooltip="Your Applications" Text="My Apps Home"></asp:hyperlink></h2>
        <ul class="nav" runat="server" id="nav"></ul>
        <ul class="styleformytreeview" runat="server" id="navOnlineApps"></ul>
        <asp:Literal ID="litNewFormatMenu" runat="server"></asp:Literal>

        
        <ul class="nav" runat="server" id="nav_activitymanagement">
            <li runat="server" id="li1"><asp:hyperlink runat="server" id="Hyperlink1" navigateurl="myapps/myactivitymanagement/activitylist.aspx" tooltip="Home" Text="Home"></asp:hyperlink></li>
            <li runat="server" id="li2"><asp:hyperlink runat="server" id="Hyperlink2" navigateurl="myapps/myactivitymanagement/default.aspx" tooltip="New Issues" Text="New Issues"></asp:hyperlink></li>
           
        </ul>

<%--Start John's New Bits  --%>      
         <ul class="nav" runat="server" id="Nav_MetersLeftPanelTemplate">
            <li runat="server" id="li3"><asp:hyperlink runat="server" id="Hyperlink3" navigateurl="myapps/customermeters/Createmeter.aspx" tooltip="Create Meter" Text="Create Meter"></asp:hyperlink></li>
            <li runat="server" id="li4"><asp:hyperlink runat="server" id="Hyperlink4" navigateurl="myapps/customermeters/ShowMeterReadings.aspx" tooltip="Meter Readings" Text="Meter Readings"></asp:hyperlink></li>
            <%--<li runat="server" id="li5"><asp:hyperlink runat="server" id="Hyperlink5" navigateurl="myapps/customermeters/MeterReader.aspx" tooltip="Meter Reader" Text="Meter Reader"></asp:hyperlink></li>--%>
            <li runat="server" id="li7"><asp:hyperlink runat="server" id="Hyperlink7" navigateurl="myapps/customermeters/MeterReadingsUpload.aspx" tooltip="Upload Bulk Readings" Text="Upload Bulk Readings"></asp:hyperlink></li>
            <li runat="server" id="li6"><asp:hyperlink runat="server" id="Hyperlink6" navigateurl="myapps/customermeters/CustomerOutstandingReads.aspx" tooltip="Outstanding Reading" Text="Outstanding Reading"></asp:hyperlink></li>
            
        
        </ul>
        
<%--End John's New Bits  --%>         
        



    </div>
    
</div>


 
    