Imports Security
Imports System.Reflection

Partial Class Login_New
    Inherits System.Web.UI.Page

    Protected Function GetApplicationVersion() As String
        Return System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString()
    End Function


    Protected Sub btnLogin_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLogin.Click

        Dim intLoginOutcome As Integer = 0

        Dim intUser As Integer = Core.CheckForNullInteger(Core.data_select_value("select intUserPK from tblUsers where LOWER(varEmail) = '" & Me.txtUsername.Text.Trim.ToLower & "' and LOWER(varPassword) = '" & Me.txtPassword.Text.Trim.ToLower & "'"))

        Dim strClientIP As String
        strClientIP = Request.UserHostAddress()

        Dim intEmailAttempts As Integer = Core.data_select_value("SELECT COUNT(*) as COUNT FROM [portal.utilitymasters.co.uk].dbo.tblUsersLoginAttempts where varEmailAddress = '" & Me.txtUsername.Text.Trim.ToLower & "' and varOutcome NOT IN ('Success') and dtmCreated > dateadd(n, -5, getdate())")

        Dim strUserLoginAudit As String = ""
        strUserLoginAudit &= "INSERT INTO [portal.utilitymasters.co.uk].dbo.tblUsersLoginAttempts "
        strUserLoginAudit &= "(intUserFK, varEmailAddress, varPassword, varIPAddress, varOutcome, dtmCreated, intUserCreatedByFK, dtmModified,intUserModifiedByFK, intDisabled)"
        strUserLoginAudit &= " VALUES ("
        strUserLoginAudit &= intUser
        strUserLoginAudit &= ", '" & Me.txtUsername.Text.Trim.ToLower & "'"
        strUserLoginAudit &= ", '" & Me.txtPassword.Text.Trim.ToLower & "'"
        strUserLoginAudit &= ", '" & strClientIP & "'"

        If intEmailAttempts < 5 Then

            If intUser > 0 Then
                If Core.data_select_value("select bitDisabled from tblUsers where intUserPK = " & intUser) = False Then



                    FormsAuthentication.SetAuthCookie(Me.txtUsername.Text.Trim.ToLower, True)
                    MySession.UserID = intUser



                    strUserLoginAudit &= ", 'Success'"
                    intLoginOutcome = 1
                    '1 = approved


                Else

                    strUserLoginAudit &= ", 'Account Disabled'"
                    intLoginOutcome = 2
                    '2 = disabled

                End If
            Else

                strUserLoginAudit &= ", 'Invalid Login'"
                intLoginOutcome = 3
                '3 = invalid

            End If
        Else
            strUserLoginAudit &= ", 'Temporary Lockout'"
            intLoginOutcome = 4
            '4 = Temp Lockout
        End If

        strUserLoginAudit &= ", getdate()"
        strUserLoginAudit &= ", " & intUser
        strUserLoginAudit &= ", getdate()"
        strUserLoginAudit &= ", " & intUser
        strUserLoginAudit &= ",0"
        strUserLoginAudit &= ")"

        If strClientIP = "127.0.0.1" Then
        Else
            Core.data_execute_nonquery(strUserLoginAudit)
        End If





        If intLoginOutcome = 0 Then '0 = unset
            Me.lblInvalidUsername.Text = ""
        ElseIf intLoginOutcome = 1 Then '1 = approved
            Response.Redirect(Core.data_select_value("select RawHomeURL from vwUsers where ID = " & MySession.UserID))
        ElseIf intLoginOutcome = 2 Then '2 = disabled
            Me.lblInvalidUsername.Text = "Account Disabled"
        ElseIf intLoginOutcome = 3 Then '3 = invalid
            Me.lblInvalidUsername.Text = "Invalid Credentials: Please try again"
        ElseIf intLoginOutcome = 4 Then '4 = temp lockout
            Me.lblInvalidUsername.Text = "Your account has been temporaily locked out due to too many attempts.  Try again in 5 minutes."
        Else
            Me.lblInvalidUsername.Text = ""
        End If

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load



        Me.lblInvalidUsername.Text = ""

        If Request.QueryString("action") = "logout" Then
            FormsAuthentication.SignOut()
            Response.Redirect("http://www.mcenergygroup.co.uk", False)
        ElseIf Request.QueryString("action") = "usererror" Then
            FormsAuthentication.SignOut()
            Me.lblInvalidUsername.Text = "an error occured, please re-login"
        End If

    End Sub


    Protected Sub hypForgotPassword_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles hypForgotPassword.Click
        Me.pnlConfirmEmailSent.Visible = False
        Me.pnlForgotPassword.Visible = True
        Me.pnlLogin.Visible = False
        Me.txtEmailPassword.Text = Me.txtUsername.Text

    End Sub

    Protected Sub btnCancelPassword_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelPassword.Click
        Me.pnlConfirmEmailSent.Visible = False
        Me.pnlForgotPassword.Visible = False
        Me.pnlLogin.Visible = True
        txtEmailPassword = Nothing
    End Sub

    Protected Sub btnSendPassword_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSendPassword.Click

        Dim intError As Integer = 0
        Me.lblEmailPasswordError.Text = Nothing

        If Core.data_select_value("SELECT COUNT(*) FROM [portal.utilitymasters.co.uk].dbo.tblUsers WHERE varEmail = '" & Me.txtEmailPassword.Text & "'") = 0 Then
            Me.lblEmailPasswordError.Text &= vbCrLf & "email address does not exist in our system"
            intError += 1
        End If


        If intError = 0 Then
            SendEmail(Me.txtEmailPassword.Text)

            Me.pnlConfirmEmailSent.Visible = True

            Me.pnlForgotPassword.Visible = False
            Me.pnlLogin.Visible = False
            Me.txtEmailPassword.Text = Nothing
        End If



    End Sub

    Protected Sub btnConfirmEmailSentClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnConfirmEmailSentClose.Click
        Me.pnlConfirmEmailSent.Visible = False
        Me.pnlForgotPassword.Visible = False
        Me.pnlLogin.Visible = True
    End Sub

    Private Sub SendEmail(ByVal AddressTo As String)

        Dim strPassword As String = Core.data_select_value("SELECT varPassword FROM [portal.utilitymasters.co.uk].dbo.tblUsers WHERE varEmail = '" & AddressTo & "'")
        Dim strBody As String = ""


        strBody &= "<style type='text/css'>"
        strBody &= "body{"
        strBody &= " font-family:Verdana; font-size:12pt; color:#009530;"
        strBody &= "}"
        strBody &= "</style>"

        strBody &= "<table style='text-align:center; width:100%;'>"
        strBody &= "    <tr>"
        strBody &= "        <td style='height: 63px'>"
        strBody &= "            <img src='https://portal.utilitymasters.co.uk/assets/Schneider/Login_header.png' alt='Schneider Electric' />"
        strBody &= "        </td>"
        strBody &= "    </tr>"

        strBody &= "    <tr>"
        strBody &= "        <td style='height: 63px;' align='center'>"

        strBody &= "<table style='width: 350px; background-color:#a782a8; border:solid 1px #000000;'>"
        strBody &= "<tr><td style='height:30px' colspan='3'></td></tr>"

        strBody &= "<tr>"
        strBody &= "<td style'width:10px;'></td>"
        strBody &= "<td style='width:100px;'>"
        strBody &= "Username"
        strBody &= "</td>"
        strBody &= "<td>"
        strBody &= AddressTo
        strBody &= "</td>"
        strBody &= "</tr>"

        strBody &= "<tr>"
        strBody &= "<td></td>"
        strBody &= "<td>"
        strBody &= "Password"
        strBody &= "</td>"
        strBody &= "<td>"
        strBody &= strPassword
        strBody &= "</td>"
        strBody &= "</tr>"
        strBody &= "</tr>"

        strBody &= "<tr>"
        strBody &= "<td></td>"
        strBody &= "<td>"
        strBody &= "Login"
        strBody &= "</td>"
        strBody &= "<td>"
        strBody &= "<a href='https://portal.utilitymasters.co.uk' target='_blank'>click here</a>"
        strBody &= "</td>"
        strBody &= "</tr>"

        strBody &= "<tr><td style='height:10px' colspan='3'></td></tr>"

        strBody &= "</table>"

        strBody &= "</td>"
        strBody &= "</tr>"
        strBody &= "</table>"


        strBody &= ""

        '!!! UPDATE THIS VALUE TO YOUR EMAIL ADDRESS
        Const FromAddress As String = "itsupport@utilitymasters.co.uk"

        '(1) Create the MailMessage instance
        Dim mm As New MailMessage(FromAddress, AddressTo)

        '(2) Assign the MailMessage's properties
        mm.Subject = "Your UTILISYS Portal Password"
        mm.Body = strBody
        mm.IsBodyHtml = True

        '(3) Create the SmtpClient object
        Dim smtp As New SmtpClient

        '(4) Send the MailMessage (will use the Web.config settings)
        smtp.Send(mm)
    End Sub
End Class
