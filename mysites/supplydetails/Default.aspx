<%@ Page Language="VB" MasterPageFile="~/landing.master" AutoEventWireup="false" CodeFile="Default.aspx.vb" Inherits="mysites_elecsupplydetails_Default" title="My Sites - Electricity Supply Details" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=9.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>


<asp:Content ID="Content1" ContentPlaceHolderID="content_right" Runat="Server">



<style>
.general {background: transparent url('/assets/Schneider/content-back-general.jpg') no-repeat; padding-top:15px; padding-left:10px;}
.navgeneral 
{
position:absolute;top:83px;left:0px;width:220px;height:500px;background: white url('/assets/Schneider/left-nav-sites.png') no-repeat; padding-top:0px; padding-left:0px;
 }

.CompanySite
{
direction:ltr;
overflow-x:hidden;
overflow-y:scroll;
scrollbar-arrow-color: #4e658a;
scrollbar-3dlight-color: #777799;
scrollbar-darkshadow-color: #666677;
scrollbar-face-color: #a1b1c3;
scrollbar-highlight-color: #e9e9e9;
scrollbar-shadow-color: #a1b1c3;
scrollbar-track-color: #ffffff;
position:absolute;top:45px;left:0px;width:217px;height:445px;
}

</style>

    <form id="form1" runat="server">
    
  
<div  class="navgeneral">
    <div id="HJ" align="center" style="text-align:center;position:absolute;top:10px;left:5px;font-family:Myriad Pro;font-size:12px;color:Blue;"></div>
    <div class="CompanySite" 
      <ul class="styleformytreeview" runat="server" id="navOnlineReports">  </ul> 
    </div>
</div>



        <div class="content_back general" id="frmSelection" runat="server" visible="true">
            <asp:RadioButtonList ID="rblReportSelection" runat="server" AutoPostBack="true">
                <asp:ListItem Value="Electricity Supply Details" Text="  Electricity - Supply Details" Selected="True"></asp:ListItem>
                <asp:ListItem Value="Electricity Supply Details - All Sites Current Rates" Text="  Electricity - All Sites Current Rates"></asp:ListItem>
                <asp:ListItem Value="Gas Supply Details" Text="  Gas -    Supply Details"></asp:ListItem>
                <asp:ListItem Value="Gas Supply Details - All Sites Current Rates" Text="  Gas - All Sites Current Rates"></asp:ListItem>
                
                
            </asp:RadioButtonList>
            <asp:Button ID="btnRunReport" runat="server" text="Run Report" />
        
        </div>
        
        <div style="position:absolute; left:0; top: 50; border: solid 1px black; height:560px; background-color:White; " id="frmReport" runat="server" visible="false">
            <table style="margin-left: 0px; margin-right: auto; z-index:100;">
                <tr>
                    <td colspan="4">
                        <rsweb:ReportViewer ID="myReportViewer" Height="510px" AsyncRendering="true" ShowPrintButton="false" runat="server" Width="900px" ShowRefreshButton="false" ShowToolBar="true" ShowZoomControl="false" ShowParameterPrompts="false" ShowBackButton="false" ShowCredentialPrompts="false" ShowFindControls="false" ShowPageNavigationControls="false" ShowPromptAreaButton="false">
                        </rsweb:ReportViewer> 
                    </td>
                </tr>
            </table>
        </div>   
    </form>

</asp:Content>

