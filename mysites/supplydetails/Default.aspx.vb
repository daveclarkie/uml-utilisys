Imports clsReportServerCredentials

Partial Class mysites_elecsupplydetails_Default
    Inherits System.Web.UI.Page


    Public Function fnNoSpecialCharacters(ByVal strTest As String) As String

        Dim sb As New System.Text.StringBuilder

        For Each ch As Char In strTest
            If Char.IsLetterOrDigit(ch) OrElse ch = " "c Then
                sb.Append(ch)
            End If
        Next

        strTest = sb.ToString

        Return strTest
    End Function

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Call LoadTreeview_Report()


    End Sub

    Private Sub LoadTreeview_Report()
        Me.navOnlineReports.InnerHtml = ""
        Dim strSQL As String = "EXEC spPortal_TREEVIEW " & MySession.UserID
        Dim myConnection As New OleDb.OleDbConnection(Core.CurrentConnectionString)

        Dim myCommand As New OleDb.OleDbCommand(strSQL, myConnection)
        myCommand.Connection.Open()
        myCommand.CommandTimeout = 180
        Dim Reader As OleDb.OleDbDataReader = myCommand.ExecuteReader(CommandBehavior.CloseConnection)

        'Me.navOnlineReports.InnerHtml += "<br/>"
        'Me.navOnlineReports.InnerHtml += "<br/>"
        'Me.navOnlineReports.InnerHtml += "<br/>"

        While Reader.Read
            Select Case CStr(Reader(3))
                Case CStr("Company")
                    If Len(Reader(1).ToString) > 20 Then
                        Me.navOnlineReports.InnerHtml += "<li class='styleformytreeview_company'><a href='default.aspx?custid=" & Reader(0).ToString & "&method=Customer'>" & Mid$(Reader(1).ToString, 1, 20) & "..." & "</a></li>"
                    Else
                        Me.navOnlineReports.InnerHtml += "<li class='styleformytreeview_company'><a href='default.aspx?custid=" & Reader(0).ToString & "&method=Customer'>" & Reader(1).ToString & "</a></li>"
                    End If
                    ' Me.navOnlineReports.InnerHtml += "<li class='styleformytreeview_company'><a href='createmeter.aspx?custid=" & Reader(0).ToString & "&method=Customer'>" & Reader(1).ToString & "</a></li>"

                Case CStr("Header")
                    Me.navOnlineReports.InnerHtml += "<li class='styleformytreeview_header'>" & Reader(1).ToString & "</li>"


                Case CStr("Site")
                    If Len(Reader(1).ToString) > 20 Then
                        Me.navOnlineReports.InnerHtml += "<li class='styleformytreeview_site'><a href='default.aspx?custid=" & Reader(0).ToString & "&method=Site'>" & Mid$(Reader(1).ToString, 1, 20) & "..." & "</a></li>"
                    Else
                        Me.navOnlineReports.InnerHtml += "<li class='styleformytreeview_site'><a href='default.aspx?custid=" & Reader(0).ToString & "&method=Site'>" & Reader(1).ToString & "</a></li>"
                    End If

                    '   Me.navOnlineReports.InnerHtml += "<li class='styleformytreeview_site'><a href='createmeter.aspx?custid=" & Reader(0).ToString & "&method=Site'>" & Left(Reader(1).ToString, 23) & "..</a></li>"
                Case CStr("Group")
                    Me.navOnlineReports.InnerHtml += "<li class='styleformytreeview_group'><a href='default.aspx?custid=" & Reader(0).ToString & "&method=Group'>" & Reader(1).ToString & "</a></li>"
            End Select

        End While

        Me.navOnlineReports.InnerHtml += "<li class='styleformytreeview_company' style='height:10px;'></li>"
        Me.navOnlineReports.InnerHtml += "<li class='styleformytreeview_company'><a href='../../my-sites.aspx'><< Back</a></li>"

    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            If Not IIf(Core.data_select_value("select bitAllowMySites from tblUsers where intUserPK = " & MySession.UserID) = "True", True, False) Then
                Response.Redirect(Core.data_select_value("select RawHomeURL from vwUsers where ID = " & MySession.UserID), True)
            End If

            If Request.QueryString("custid") Is Nothing Then
                Dim strSQL As String = "EXEC spPortal_TREEVIEW " & MySession.UserID
                Dim myConnection As New OleDb.OleDbConnection(Core.CurrentConnectionString)

                Dim myCommand As New OleDb.OleDbCommand(strSQL, myConnection)
                myCommand.Connection.Open()
                myCommand.CommandTimeout = 180
                Dim Reader As OleDb.OleDbDataReader = myCommand.ExecuteReader(CommandBehavior.CloseConnection)

                If Reader.HasRows Then
                    Reader.Read()
                    If Reader("Type") = "Company" Then
                        Response.Redirect("default.aspx?custid=" & Reader("id") & "&method=Customer", True)
                    Else
                        Response.Redirect("default.aspx?custid=" & Reader("id") & "&method=" & Reader("Type"), True)
                    End If
                    Reader.Close()
                End If
            End If


            Dim description As HtmlMeta = New HtmlMeta()
            description.Name = "description"
            description.Content = "My Sites - Search"
            Dim keywords As HtmlMeta = New HtmlMeta()
            keywords.Name = "keywords"
            keywords.Content = "My Sites - Search"
            Page.Header.Controls.Add(description)
            Page.Header.Controls.Add(keywords)

            Dim sMyScreenPath As String
            Dim strCustomPage As String = Core.data_select_value("SELECT varDefaultPage FROM tblUsers where intUserPK = " & MySession.UserID)
            sMyScreenPath = System.Web.HttpContext.Current.Request.Url.AbsolutePath


            If Not IsPostBack Then


            End If

        Catch ex As Exception
            '
        End Try



    End Sub

   
    Protected Sub btnRunReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRunReport.Click

        Me.frmSelection.Visible = False
        Me.frmReport.Visible = True

        'Set the processing mode for the ReportViewer to Remote
        myReportViewer.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Remote

        Dim serverReport As Microsoft.Reporting.WebForms.ServerReport
        serverReport = myReportViewer.ServerReport

        serverReport.ReportServerCredentials = New clsReportServerCredentials("administrator", "chicago", "uml")

        'Set the report server URL and report path
        serverReport.ReportServerUrl = _
            New Uri("http://10.44.5.205/reportserver")
        serverReport.ReportPath = "/Portal Reporting/" & Me.rblReportSelection.SelectedItem.Value.ToString

        Dim customerid As New Microsoft.Reporting.WebForms.ReportParameter()
        customerid.Name = "customerid"
        customerid.Values.Add(Request.QueryString("custid"))

        'Set the report parameters for the report
        Dim parameters() As Microsoft.Reporting.WebForms.ReportParameter = {customerid}
        serverReport.SetParameters(parameters)
    End Sub
End Class
