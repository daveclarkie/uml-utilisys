
Partial Class mysites_search_Default
    Inherits System.Web.UI.Page


    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Call LoadTreeview_Report()
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            If Not IIf(Core.data_select_value("select bitAllowMySites from tblUsers where intUserPK = " & MySession.UserID) = "True", True, False) Then
                Response.Redirect(Core.data_select_value("select RawHomeURL from vwUsers where ID = " & MySession.UserID), True)
            End If

            If Request.QueryString("custid") Is Nothing Then
                Dim strSQL As String = "EXEC spPortal_TREEVIEW " & MySession.UserID
                Dim myConnection As New OleDb.OleDbConnection(Core.CurrentConnectionString)

                Dim myCommand As New OleDb.OleDbCommand(strSQL, myConnection)
                myCommand.Connection.Open()
                myCommand.CommandTimeout = 180
                Dim Reader As OleDb.OleDbDataReader = myCommand.ExecuteReader(CommandBehavior.CloseConnection)

                If Reader.HasRows Then
                    Reader.Read()
                    If Reader("Type") = "Company" Then
                        Response.Redirect("default.aspx?custid=" & Reader("id") & "&method=Customer", True)
                    Else
                        Response.Redirect("default.aspx?custid=" & Reader("id") & "&method=" & Reader("Type"), True)
                    End If
                    Reader.Close()
                End If
            End If


            Dim description As HtmlMeta = New HtmlMeta()
            description.Name = "description"
            description.Content = "My Sites - Search"
            Dim keywords As HtmlMeta = New HtmlMeta()
            keywords.Name = "keywords"
            keywords.Content = "My Sites - Search"
            Page.Header.Controls.Add(description)
            Page.Header.Controls.Add(keywords)

            Dim sMyScreenPath As String
            Dim strCustomPage As String = Core.data_select_value("SELECT varDefaultPage FROM tblUsers where intUserPK = " & MySession.UserID)
            sMyScreenPath = System.Web.HttpContext.Current.Request.Url.AbsolutePath


        Catch ex As Exception
            '
        End Try

    End Sub

    Private Sub LoadTreeview_Report()
        Me.navOnlineReports.InnerHtml = ""
        Dim strSQL As String = "EXEC spPortal_TREEVIEW " & MySession.UserID
        Dim myConnection As New OleDb.OleDbConnection(Core.CurrentConnectionString)

        Dim myCommand As New OleDb.OleDbCommand(strSQL, myConnection)
        myCommand.Connection.Open()
        myCommand.CommandTimeout = 180
        Dim Reader As OleDb.OleDbDataReader = myCommand.ExecuteReader(CommandBehavior.CloseConnection)

        'Me.navOnlineReports.InnerHtml += "<br/>"
        'Me.navOnlineReports.InnerHtml += "<br/>"
        'Me.navOnlineReports.InnerHtml += "<br/>"

        While Reader.Read
            Select Case CStr(Reader(3))
                Case CStr("Company")
                    If Len(Reader(1).ToString) > 20 Then
                        Me.navOnlineReports.InnerHtml += "<li class='styleformytreeview_company'><a href='default.aspx?custid=" & Reader(0).ToString & "&method=Customer'>" & Mid$(Reader(1).ToString, 1, 20) & "..." & "</a></li>"
                    Else
                        Me.navOnlineReports.InnerHtml += "<li class='styleformytreeview_company'><a href='default.aspx?custid=" & Reader(0).ToString & "&method=Customer'>" & Reader(1).ToString & "</a></li>"
                    End If
                    ' Me.navOnlineReports.InnerHtml += "<li class='styleformytreeview_company'><a href='createmeter.aspx?custid=" & Reader(0).ToString & "&method=Customer'>" & Reader(1).ToString & "</a></li>"

                Case CStr("Header")
                    Me.navOnlineReports.InnerHtml += "<li class='styleformytreeview_header'>" & Reader(1).ToString & "</li>"


                Case CStr("Site")
                    If Len(Reader(1).ToString) > 20 Then
                        Me.navOnlineReports.InnerHtml += "<li class='styleformytreeview_site'><a href='default.aspx?custid=" & Reader(0).ToString & "&method=Site'>" & Mid$(Reader(1).ToString, 1, 20) & "..." & "</a></li>"
                    Else
                        Me.navOnlineReports.InnerHtml += "<li class='styleformytreeview_site'><a href='default.aspx?custid=" & Reader(0).ToString & "&method=Site'>" & Reader(1).ToString & "</a></li>"
                    End If

                    '   Me.navOnlineReports.InnerHtml += "<li class='styleformytreeview_site'><a href='createmeter.aspx?custid=" & Reader(0).ToString & "&method=Site'>" & Left(Reader(1).ToString, 23) & "..</a></li>"
                Case CStr("Group")
                    Me.navOnlineReports.InnerHtml += "<li class='styleformytreeview_group'><a href='default.aspx?custid=" & Reader(0).ToString & "&method=Group'>" & Reader(1).ToString & "</a></li>"
            End Select

        End While

        Me.navOnlineReports.InnerHtml += "<li class='styleformytreeview_company' style='height:10px;'></li>"
        Me.navOnlineReports.InnerHtml += "<li class='styleformytreeview_company'><a href='../../my-sites.aspx'><< Back</a></li>"

    End Sub

    Public Function fnNoSpecialCharacters(ByVal strTest As String) As String

        Dim sb As New System.Text.StringBuilder

        For Each ch As Char In strTest
            If Char.IsLetterOrDigit(ch) OrElse ch = " "c Then
                sb.Append(ch)
            End If
        Next

        strTest = sb.ToString

        Return strTest
    End Function

    Protected Sub lbtnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbtnSearch.Click

        Me.txtSearchText.Text = fnNoSpecialCharacters(Me.txtSearchText.Text.ToString)


        grdTable.Columns.Clear()

        Dim bf1 As BoundField
        bf1 = New BoundField
        Dim bf2 As BoundField
        bf2 = New BoundField
        Dim bf3 As BoundField
        bf3 = New BoundField
        Dim bf4 As BoundField
        bf4 = New BoundField
        Dim bf5 As BoundField
        bf5 = New BoundField
        Dim bf6 As BoundField
        bf6 = New BoundField

        bf1.DataField = "siteid"
        bf1.HeaderText = "ID"
        grdTable.Columns.Add(bf1)

        bf2.DataField = "SiteName"
        bf2.HeaderText = "Site"
        grdTable.Columns.Add(bf2)

        bf3.DataField = "address_1"
        bf3.HeaderText = "Address"
        grdTable.Columns.Add(bf3)

        bf4.DataField = "pcode"
        bf4.HeaderText = "P.Code"
        grdTable.Columns.Add(bf4)

        bf5.DataField = "varMPAN"
        bf5.HeaderText = "Elec"
        grdTable.Columns.Add(bf5)

        bf6.DataField = "MNumber"
        bf6.HeaderText = "Gas"
        grdTable.Columns.Add(bf6)

        With Me.grdTable
            .DataSource = Core.data_select("EXEC UML_ExtData.dbo.usp_Portal_MySites_Search '" & Me.txtSearchText.Text & "', '" & Me.rblSearchFilter.SelectedItem.Value & "', " & MySession.UserID & ", '" & Request.QueryString("method") & "', " & Request.QueryString("custid"))
            .DataBind()
        End With

        If Me.grdTable.Rows.Count < 1 Then
            Me.lblSearchInformation.Text = "0 rows returned"
        Else
            Me.lblSearchInformation.Text = Me.grdTable.Rows.Count & " rows returned"
        End If

    End Sub

    Protected Sub grdTable_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles grdTable.SelectedIndexChanged
        Me.pnlSiteInformation.Visible = True

        Me.lblSiteID.Text = Me.grdTable.SelectedRow.Cells(1).Text

        Me.txtCustomerSiteReference.Text = Core.data_select_value("SELECT customersiteref FROM UML_CMS.dbo.tblSites WHERE siteid = " & Me.grdTable.SelectedRow.Cells(1).Text)
        Me.lblCustomerSiteReference.Text = Core.data_select_value("SELECT customersiteref FROM UML_CMS.dbo.tblSites WHERE siteid = " & Me.grdTable.SelectedRow.Cells(1).Text)


        Me.lblSiteName.Text = Core.data_select_value("SELECT sitename FROM UML_CMS.dbo.tblSites WHERE siteid = " & Me.grdTable.SelectedRow.Cells(1).Text)
        Me.lblAddress1.Text = Core.data_select_value("SELECT address_1 FROM UML_CMS.dbo.tblSites WHERE siteid = " & Me.grdTable.SelectedRow.Cells(1).Text)
        Me.lblAddress2.Text = Core.data_select_value("SELECT address_2 FROM UML_CMS.dbo.tblSites WHERE siteid = " & Me.grdTable.SelectedRow.Cells(1).Text)
        Me.lblTown.Text = Core.data_select_value("SELECT address_3 FROM UML_CMS.dbo.tblSites WHERE siteid = " & Me.grdTable.SelectedRow.Cells(1).Text)
        Me.lblPostCode.Text = Core.data_select_value("SELECT pcode FROM UML_CMS.dbo.tblSites WHERE siteid = " & Me.grdTable.SelectedRow.Cells(1).Text)




        With Me.grdMPAN
            .DataSource = Core.data_select("EXEC UML_ExtData.dbo.usp_Portal_MySites_Search_MPANDetails " & Me.grdTable.SelectedRow.Cells(1).Text)
            .DataBind()
        End With

        With Me.grdMNumbers
            .DataSource = Core.data_select("EXEC UML_ExtData.dbo.usp_Portal_MySites_Search_MNumberDetails " & Me.grdTable.SelectedRow.Cells(1).Text)
            .DataBind()
        End With


        If Me.grdMPAN.Rows.Count > 0 Then Me.btnElecSupplyDetails.Enabled = True Else Me.btnElecSupplyDetails.Enabled = False
        If Me.grdMNumbers.Rows.Count > 0 Then Me.btnGasSupplyDetails.Enabled = True Else Me.btnGasSupplyDetails.Enabled = False

        If Me.grdMPAN.Rows.Count < 1 Then Me.lblMPANDetails.Text = "There are no MPAN's in the system for this site." Else Me.lblMPANDetails.Text = Nothing
        If Me.grdMNumbers.Rows.Count < 1 Then Me.lblMNumberDetails.Text = "There are no MNumber's in the system for this site." Else Me.lblMNumberDetails.Text = Nothing


    End Sub


    Protected Sub ImageButtonReturnToGrid_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonReturnToGrid.Click
        Me.pnlSiteInformation.Visible = False
        Me.grdTable.SelectedIndex = -1
    End Sub

    Protected Sub imgSmallSaveButton_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgSmallSaveButton.Click
        Me.txtCustomerSiteReference.Text = fnNoSpecialCharacters(Me.txtCustomerSiteReference.Text.ToString)

        Core.data_execute_nonquery("UPDATE UML_CMS.dbo.tblSites SET Customersiteref = '" & Me.txtCustomerSiteReference.Text.ToString & "' WHERE siteid = " & Me.lblSiteID.Text)

        Me.txtCustomerSiteReference.Text = Core.data_select_value("SELECT customersiteref FROM UML_CMS.dbo.tblSites WHERE siteid = " & Me.grdTable.SelectedRow.Cells(1).Text)
        Me.lblCustomerSiteReference.Text = Core.data_select_value("SELECT customersiteref FROM UML_CMS.dbo.tblSites WHERE siteid = " & Me.grdTable.SelectedRow.Cells(1).Text)

        Me.txtCustomerSiteReference.Visible = False
        Me.imgSmallSaveButton.Visible = False
        Me.lnkCancelEditCustomerSiteReference.Visible = False

        Me.lblCustomerSiteReference.Visible = True
        Me.lnkEditCustomerSiteReference.Visible = True

    End Sub

    Protected Sub lnkEditCustomerSiteReference_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkEditCustomerSiteReference.Click
        Me.txtCustomerSiteReference.Visible = True
        Me.imgSmallSaveButton.Visible = True
        Me.lnkCancelEditCustomerSiteReference.Visible = True

        Me.lblCustomerSiteReference.Visible = False
        Me.lnkEditCustomerSiteReference.Visible = False

        Me.txtCustomerSiteReference.Text = Core.data_select_value("SELECT customersiteref FROM UML_CMS.dbo.tblSites WHERE siteid = " & Me.grdTable.SelectedRow.Cells(1).Text)
        Me.lblCustomerSiteReference.Text = Core.data_select_value("SELECT customersiteref FROM UML_CMS.dbo.tblSites WHERE siteid = " & Me.grdTable.SelectedRow.Cells(1).Text)

    End Sub

    Protected Sub lnkCancelEditCustomerSiteReference_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCancelEditCustomerSiteReference.Click

        Me.txtCustomerSiteReference.Visible = False
        Me.imgSmallSaveButton.Visible = False
        Me.lnkCancelEditCustomerSiteReference.Visible = False

        Me.lblCustomerSiteReference.Visible = True
        Me.lnkEditCustomerSiteReference.Visible = True

        Me.txtCustomerSiteReference.Text = Core.data_select_value("SELECT customersiteref FROM UML_CMS.dbo.tblSites WHERE siteid = " & Me.grdTable.SelectedRow.Cells(1).Text)
        Me.lblCustomerSiteReference.Text = Core.data_select_value("SELECT customersiteref FROM UML_CMS.dbo.tblSites WHERE siteid = " & Me.grdTable.SelectedRow.Cells(1).Text)

    End Sub

    Protected Sub btnElecSupplyDetails_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnElecSupplyDetails.Click
        LoadReport("Electricity Supply Detailed Information")
    End Sub

    Protected Sub btnGasSupplyDetails_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGasSupplyDetails.Click
        LoadReport("Gas Supply Detailed Information")
    End Sub

    Private Sub LoadReport(ByVal strReportName As String)

        Me.pnlReportViewer.Visible = True

        'Set the processing mode for the ReportViewer to Remote
        myReportViewer.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Remote

        Dim serverReport As Microsoft.Reporting.WebForms.ServerReport
        serverReport = myReportViewer.ServerReport

        serverReport.ReportServerCredentials = New clsReportServerCredentials("administrator", "chicago", "uml")

        'Set the report server URL and report path
        serverReport.ReportServerUrl = _
            New Uri("http://10.44.5.205/reportserver")
        serverReport.ReportPath = "/Portal Reporting/" & strReportName

        Dim siteid As New Microsoft.Reporting.WebForms.ReportParameter()
        siteid.Name = "siteid"
        siteid.Values.Add(Me.lblSiteID.Text)

        'Set the report parameters for the report
        Dim parameters() As Microsoft.Reporting.WebForms.ReportParameter = {siteid}
        serverReport.SetParameters(parameters)

    End Sub



End Class
