<%@ Page Language="VB" MasterPageFile="~/landing.master" AutoEventWireup="false" CodeFile="Default.aspx.vb" Inherits="mysites_search_Default" title="My Sites - Search" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=9.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>


<asp:Content ID="Content1" ContentPlaceHolderID="content_right" Runat="Server">



    <style>
        .general {background: transparent url('/assets/Schneider/content-back-general.jpg') no-repeat; padding-top:15px; padding-left:10px;}
        .navgeneral 
        {
        position:absolute;top:83px;left:0px;width:220px;height:500px;background: white url('/assets/Schneider/left-nav-sites.png') no-repeat; padding-top:0px; padding-left:0px;
         }

        .CompanySite
        {
        direction:ltr;
        overflow-x:hidden;
        overflow-y:scroll;
        scrollbar-arrow-color: #4e658a;
        scrollbar-3dlight-color: #777799;
        scrollbar-darkshadow-color: #666677;
        scrollbar-face-color: #a1b1c3;
        scrollbar-highlight-color: #e9e9e9;
        scrollbar-shadow-color: #a1b1c3;
        scrollbar-track-color: #ffffff;
        position:absolute;top:45px;left:0px;width:217px;height:445px;
        }
    </style>

    <form id="form1" runat="server">
    
        <div  class="navgeneral">
            <div id="HJ" align="center" style="text-align:center;position:absolute;top:10px;left:5px;font-family:Myriad Pro;font-size:12px;color:Blue;"></div>
            <div class="CompanySite" >
              <ul class="styleformytreeview" runat="server" id="navOnlineReports">  </ul> 
            </div>
        </div>


        <div class="content_back general">
        
            <div class="searchbar_Head">
                
                    <table style="margin-left: 30px; margin-right: auto; width:90%;">
                    
                        <tr>
                            <td colspan="4">
                                <asp:RadioButtonList ID="rblSearchFilter" runat="server" RepeatColumns="7" RepeatDirection="Horizontal" RepeatLayout="Table" Width="96%" AutoPostBack="true">
                                    <asp:ListItem Value="All" Text= "All" Selected="True"></asp:ListItem>
                                    <asp:ListItem Value="SiteName" Text= "Site Name"></asp:ListItem>
                                    <asp:ListItem Value="Address" Text= "Address"></asp:ListItem>
                                    <asp:ListItem Value="PostCode" Text= "Post Code"></asp:ListItem>
                                    <asp:ListItem Value="Meter" Text= "Meter"></asp:ListItem>
                                    <asp:ListItem Value="Supplier" Text= "Supplier"></asp:ListItem>
                                    <asp:ListItem Value="UMLSiteID" Text= "UML Ref"></asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        
                        <tr>
                            <td>
                                <asp:Image ID="Image3" runat="server" ImageUrl="~/assets/Search_25x25.png" />
                            </td>
                            <td>
                                <asp:Label ID="Label1" runat="server" Text="Search:" Font-Names="Myriad Pro" ForeColor="white" Width="50px" style="margin-right:10px;"></asp:Label> 
                            </td>
                            <td>
                                <asp:TextBox ID="txtSearchText" runat="server" Font-Names="Calibri" Width="270px" ToolTip="Search Criteria" Font-Size="14px"></asp:TextBox>
                            </td>
                            <td> 
                                <asp:LinkButton id="lbtnSearch" CssClass="lbtnSearch" runat="server"></asp:LinkButton>
                             </td>
                        </tr>
                        
                    </table>
                
            </div>
            
            <asp:Label ID="lblSearchInformation" runat="server"></asp:Label>
    
            <asp:Panel id="pnlSearchInformation" runat="server"  style="width:650px; height:400px;" ScrollBars="Vertical">
                <asp:GridView borderwidth="0px" ID="grdTable" cssclass="users" runat="server" headerstyle-cssclass="header" AutoGenerateColumns="False" AutoGenerateSelectButton="true">
                    <AlternatingRowStyle CssClass="tralt" />
                    <headerstyle cssclass="header" />
                </asp:GridView>
            </asp:Panel>
            
            <asp:Panel Visible="false" id="pnlSiteInformation" runat="server" CssClass="siteinformation">
                <asp:ImageButton ID="ImageButtonReturnToGrid" visible="true" style="position:absolute;left:625px; top:6px;" height="32px" width="32px" tooltip="Return to search results" runat="server" ImageAlign="Left" ImageUrl="~/assets/metering/remove_48x48.png"></asp:ImageButton>
                
                <div id="dvSiteInformation" runat="server" style="Z-INDEX: 125; LEFT:1%; POSITION: absolute; TOP: 30px;">
                    <table style="width:100%;">
                        <tr>
                            <td style="width:150px;">
                                <b>Site ID</b>
                            </td>
                            <td style="width:550px;">
                                <asp:Label ID="lblSiteID" runat="server"></asp:Label>
                            </td>
                            <td style="width:20px;"></td>
                        </tr>
                        
                        <tr>
                            <td>
                                <b>Your Site Name</b>
                            </td>
                            <td>
                                <asp:TextBox ID="txtCustomerSiteReference" runat="server" Width="300px" Visible="false"></asp:TextBox>
                                <div style="Z-INDEX: 125; LEFT:68%; POSITION: absolute; TOP: 23px;">
                                    <asp:ImageButton ID="imgSmallSaveButton" runat="server" ImageUrl="~/assets/Buttons_Small_Save.png" Visible="false" />
                                    <asp:LinkButton ID="lnkCancelEditCustomerSiteReference" runat="server" Text="cancel" Visible="false"></asp:LinkButton>
                                </div>
                                <asp:label ID="lblCustomerSiteReference" runat="server" Width="300px" Visible="true"></asp:label>
                                <asp:LinkButton ID="lnkEditCustomerSiteReference" runat="server" Text="edit" Visible="true"></asp:LinkButton>
                            </td>
                            <td>
                                
                            </td>
                        </tr>
                        
                        <tr>
                            <td>
                                <b>UML Site Name</b>
                            </td>
                            <td>
                                <asp:Label ID="lblSiteName" runat="server"></asp:Label>
                            </td>
                            <td></td>
                        </tr>
                        
                        <tr>
                            <td>
                                <b>Address</b>
                            </td>
                            <td>
                                <asp:Label ID="lblAddress1" runat="server"></asp:Label>
                            </td>
                            <td></td>
                        </tr>
                        
                        <tr>
                            <td>
                                <b></b>
                            </td>
                            <td>
                                <asp:Label ID="lblAddress2" runat="server"></asp:Label>
                            </td>
                            <td></td>
                        </tr>
                        
                        <tr>
                            <td>
                                <b></b>
                            </td>
                            <td>
                                <asp:Label ID="lblTown" runat="server"></asp:Label>
                            </td>
                            <td></td>
                        </tr>
                        
                        <tr>
                            <td>
                                <b>Post Code</b>
                            </td>
                            <td>
                                <asp:Label ID="lblPostCode" runat="server"></asp:Label>
                            </td>
                            <td></td>
                        </tr>
                        
                        <tr>
                            <td colspan="3">
                                <br />
                                <b>Electricity Meter Details</b><br />
                                <asp:GridView borderwidth="0px" ID="grdMPAN" cssclass="users" runat="server" headerstyle-cssclass="header" AutoGenerateColumns="False" AutoGenerateSelectButton="False">
                                    <AlternatingRowStyle CssClass="tralt" />
                                    <headerstyle cssclass="header" />
                                    <Columns>
                                        <asp:BoundField DataField="profiletype" HeaderText="Profile" />
                                        <asp:BoundField DataField="metertimeswitch" HeaderText="MTS" />
                                        <asp:BoundField DataField="LLF" HeaderText="LLF" />
                                        <asp:BoundField DataField="distribution" HeaderText="Distribution" />
                                        <asp:BoundField DataField="supplier" HeaderText="Supplier" />
                                        <asp:BoundField DataField="varDataCollector" HeaderText="DC" />
                                    </Columns>
                                </asp:GridView>
                                <asp:Label ID="lblMPANDetails" runat="server"></asp:Label>                                
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <asp:Button ID="btnElecSupplyDetails" runat="server" Text="Supply Details" Width="150px" />
                            </td>
                        </tr>
                        
                        <tr>
                            <td colspan="3">
                                <br />
                                <b>Gas Meter Details</b><br />
                                <asp:GridView borderwidth="0px" ID="grdMNumbers" cssclass="users" runat="server" headerstyle-cssclass="header" AutoGenerateColumns="False" AutoGenerateSelectButton="False">
                                    <AlternatingRowStyle CssClass="tralt" />
                                    <headerstyle cssclass="header" />
                                    <Columns>
                                        <asp:BoundField DataField="mnumber" HeaderText="MNumber" />
                                        <asp:BoundField DataField="varSerialNumber" HeaderText="Serial Number" />
                                    </Columns>
                                </asp:GridView>
                                <asp:Label ID="lblMNumberDetails" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <asp:Button ID="btnGasSupplyDetails" runat="server" Text="Supply Details" Width="150px" />
                            </td>
                        </tr>
                        </tr>
                    </table>
                </div>
            </asp:Panel>

            <asp:Panel Visible="false" id="pnlReportViewer" runat="server" style="position:absolute; Z-INDEX: 125; LEFT:0px; top: 85px; border: solid 1px black; height:560px; background-color:White; ">
                <rsweb:ReportViewer ID="myReportViewer" Height="510px" AsyncRendering="true" ShowPrintButton="false" runat="server" Width="900px" ShowRefreshButton="false" ShowToolBar="true" ShowZoomControl="false" ShowParameterPrompts="false" ShowBackButton="false" ShowCredentialPrompts="false" ShowFindControls="false" ShowPageNavigationControls="false" ShowPromptAreaButton="false">
                </rsweb:ReportViewer> 
            </asp:Panel>
        </div>
    </form>
          


</asp:Content>

