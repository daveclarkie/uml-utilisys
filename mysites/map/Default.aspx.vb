
Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports PostcodeToOS
Imports Subgurim.Controles
Imports System.Data.SqlClient
Imports System.Collections.Generic

Imports System.Drawing

Partial Class mysites_map_Default
    Inherits System.Web.UI.Page



    Public GreenIcon As New GIcon()
    Public BlueIcon As New GIcon()
    Public RedIcon As New GIcon()
    Public OrangeIcon As New GIcon()
    Public PurpleIcon As New GIcon()
    Public YellowIcon As New GIcon()

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Call LoadTreeview_Report()
    End Sub
    Private Sub LoadTreeview_Report()
        Me.navOnlineReports.InnerHtml = ""
        Dim strSQL As String = "EXEC spPortal_TREEVIEW " & MySession.UserID
        Dim myConnection As New OleDb.OleDbConnection(Core.CurrentConnectionString)

        Dim myCommand As New OleDb.OleDbCommand(strSQL, myConnection)
        myCommand.Connection.Open()
        myCommand.CommandTimeout = 180
        Dim Reader As OleDb.OleDbDataReader = myCommand.ExecuteReader(CommandBehavior.CloseConnection)

        'Me.navOnlineReports.InnerHtml += "<br/>"
        'Me.navOnlineReports.InnerHtml += "<br/>"
        'Me.navOnlineReports.InnerHtml += "<br/>"

        While Reader.Read
            Select Case CStr(Reader(3))
                Case CStr("Company")
                    If Len(Reader(1).ToString) > 20 Then
                        Me.navOnlineReports.InnerHtml += "<li class='styleformytreeview_company'><a href='default.aspx?custid=" & Reader(0).ToString & "&method=Customer'>" & Mid$(Reader(1).ToString, 1, 20) & "..." & "</a></li>"
                    Else
                        Me.navOnlineReports.InnerHtml += "<li class='styleformytreeview_company'><a href='default.aspx?custid=" & Reader(0).ToString & "&method=Customer'>" & Reader(1).ToString & "</a></li>"
                    End If
                    ' Me.navOnlineReports.InnerHtml += "<li class='styleformytreeview_company'><a href='createmeter.aspx?custid=" & Reader(0).ToString & "&method=Customer'>" & Reader(1).ToString & "</a></li>"

                Case CStr("Header")
                    Me.navOnlineReports.InnerHtml += "<li class='styleformytreeview_header'>" & Reader(1).ToString & "</li>"


                Case CStr("Site")
                    If Len(Reader(1).ToString) > 20 Then
                        Me.navOnlineReports.InnerHtml += "<li class='styleformytreeview_site'><a href='default.aspx?custid=" & Reader(0).ToString & "&method=Site'>" & Mid$(Reader(1).ToString, 1, 20) & "..." & "</a></li>"
                    Else
                        Me.navOnlineReports.InnerHtml += "<li class='styleformytreeview_site'><a href='default.aspx?custid=" & Reader(0).ToString & "&method=Site'>" & Reader(1).ToString & "</a></li>"
                    End If

                    '   Me.navOnlineReports.InnerHtml += "<li class='styleformytreeview_site'><a href='createmeter.aspx?custid=" & Reader(0).ToString & "&method=Site'>" & Left(Reader(1).ToString, 23) & "..</a></li>"
                Case CStr("Group")
                    Me.navOnlineReports.InnerHtml += "<li class='styleformytreeview_group'><a href='default.aspx?custid=" & Reader(0).ToString & "&method=Group'>" & Reader(1).ToString & "</a></li>"
            End Select

        End While

        Me.navOnlineReports.InnerHtml += "<li class='styleformytreeview_company' style='height:10px;'></li>"
        Me.navOnlineReports.InnerHtml += "<li class='styleformytreeview_company'><a href='../../my-sites.aspx'><< Back</a></li>"

    End Sub

    Public Sub LoadIcons()

        GreenIcon.image = "https://portal.utilitymasters.co.uk/assets/icons/green.png"
        GreenIcon.shadow = "https://portal.utilitymasters.co.uk/assets/icons/small_shadow.png"
        GreenIcon.iconSize = New GSize(12, 20)
        GreenIcon.shadowSize = New GSize(12, 20)
        GreenIcon.iconAnchor = New GPoint(10, 20)

        BlueIcon.image = "https://portal.utilitymasters.co.uk/assets/icons/blue.png"
        BlueIcon.shadow = "https://portal.utilitymasters.co.uk/assets/icons/small_shadow.png"
        BlueIcon.iconSize = New GSize(12, 20)
        BlueIcon.shadowSize = New GSize(12, 20)
        BlueIcon.iconAnchor = New GPoint(10, 20)

        RedIcon.image = "https://portal.utilitymasters.co.uk/assets/icons/red.png"
        RedIcon.shadow = "https://portal.utilitymasters.co.uk/assets/icons/small_shadow.png"
        RedIcon.iconSize = New GSize(12, 20)
        RedIcon.shadowSize = New GSize(12, 20)
        RedIcon.iconAnchor = New GPoint(10, 20)

        OrangeIcon.image = "https://portal.utilitymasters.co.uk/assets/icons/orange.png"
        OrangeIcon.shadow = "https://portal.utilitymasters.co.uk/assets/icons/small_shadow.png"
        OrangeIcon.iconSize = New GSize(12, 20)
        OrangeIcon.shadowSize = New GSize(12, 20)
        OrangeIcon.iconAnchor = New GPoint(10, 20)

        PurpleIcon.image = "https://portal.utilitymasters.co.uk/assets/icons/purple.png"
        PurpleIcon.shadow = "https://portal.utilitymasters.co.uk/assets/icons/small_shadow.png"
        PurpleIcon.iconSize = New GSize(12, 20)
        PurpleIcon.shadowSize = New GSize(12, 20)
        PurpleIcon.iconAnchor = New GPoint(10, 20)

        YellowIcon.image = "https://portal.utilitymasters.co.uk/assets/icons/yellow.png"
        YellowIcon.shadow = "https://portal.utilitymasters.co.uk/assets/icons/small_shadow.png"
        YellowIcon.iconSize = New GSize(12, 20)
        YellowIcon.shadowSize = New GSize(12, 20)
        YellowIcon.iconAnchor = New GPoint(10, 20)

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Request.QueryString("custid") Is Nothing Then
            Dim strSQL As String = "EXEC spPortal_TREEVIEW_TopLevel " & MySession.UserID
            Dim myConnection As New OleDb.OleDbConnection(Core.CurrentConnectionString)

            Dim myCommand As New OleDb.OleDbCommand(strSQL, myConnection)
            myCommand.Connection.Open()
            myCommand.CommandTimeout = 180
            Dim Reader As OleDb.OleDbDataReader = myCommand.ExecuteReader(CommandBehavior.CloseConnection)

            If Reader.HasRows Then
                Reader.Read()
                If Reader("type").ToString = "Company" Then
                    Response.Redirect("default.aspx?custid=" & Reader("id") & "&method=Customer", True)
                Else
                    Response.Redirect("default.aspx?custid=" & Reader("id") & "&method=" & Reader("type"), True)
                End If
                Reader.Close()
            End If


        End If

        LoadIcons()

        If Not Len(Request.QueryString("method")) > 0 Then
        Else
            Me.topmenu.Visible = True
            Me.lblDescription.Text = Request.QueryString("method").ToString & ":"
            Me.lblName.Text = "  " & Core.data_select_value("EXEC spSites_MAP_CurentSelection " & MySession.UserID & ", '" & Request.QueryString("method") & "', " & Request.QueryString("custid"))
        End If

        If Me.rblView.SelectedItem.Value = "table" Then
            Me.map_view.Visible = False
            Me.table_view.Visible = True
            LoadTable()
        ElseIf Me.rblView.SelectedItem.Value = "map" Then
            Me.map_view.Visible = True
            Me.table_view.Visible = False
            loadMap_LatLong()
        Else
            Me.map_view.Visible = False
            Me.table_view.Visible = False
        End If

        Try
            If Not IIf(Core.data_select_value("select bitAllowMySites from tblUsers where intUserPK = " & MySession.UserID) = "True", True, False) Then
                Response.Redirect(Core.data_select_value("select RawHomeURL from vwUsers where ID = " & MySession.UserID), True)
            End If

            If Not Page.IsPostBack = True Then
                Dim description As HtmlMeta = New HtmlMeta()
                description.Name = "description"
                description.Content = "Utility Masters customer extranet - My Sites"
                Dim keywords As HtmlMeta = New HtmlMeta()
                keywords.Name = "keywords"
                keywords.Content = "my sites"
                Page.Header.Controls.Add(description)
                Page.Header.Controls.Add(keywords)

                'code to populate map_view
                'Me.map_view.InnerHtml = "<strong>MAP VIEW GOES HERE!</strong>"

                If Len(Request.QueryString("method")) > 0 And Len(Me.txtStoredProcedure.Text) = 0 Then
                    Me.txtView.Text = "map"
                    Me.txtStoredProcedure.Text = "spSites_MAP_ActiveSites"
                    Me.ddlMapSelection.SelectedValue = "spSites_MAP_ActiveSites"
                    PreparePage()

                    loadMap_LatLong()


                End If

            End If

        Catch ex As Exception
            '
        End Try


    End Sub

    Private Sub PreparePage()

        Me.pnlActiveFilter.Visible = False
        Me.pnlHHProfileFilter.Visible = False
        Me.pnlCCAExemptionFilter.Visible = False
        Me.pnlRatesFilter.Visible = False
        Me.pnlCCAPassFail.Visible = False
        grdTable.Columns.Clear()

        If Me.txtStoredProcedure.Text = "spSites_MAP_ActiveSites" Then

            Me.pnlActiveFilter.Visible = True
            Me.chkActive_Active.Checked = True
            Me.chkActive_Inactive.Checked = True

            Dim bf1 As BoundField
            bf1 = New BoundField
            Dim bf2 As BoundField
            bf2 = New BoundField
            Dim bf3 As BoundField
            bf3 = New BoundField
            Dim bf4 As BoundField
            bf4 = New BoundField

            Dim imgf As ImageField
            imgf = New ImageField


            imgf.DataImageUrlField = "iconURL"
            imgf.HeaderText = ""
            grdTable.Columns.Add(imgf)

            bf1.DataField = "siteid"
            bf1.HeaderText = "ID"
            grdTable.Columns.Add(bf1)

            bf2.DataField = "SiteName"
            bf2.HeaderText = "Site Name"
            grdTable.Columns.Add(bf2)

            bf3.DataField = "pcode"
            bf3.HeaderText = "Post Code"
            grdTable.Columns.Add(bf3)

        ElseIf Me.txtStoredProcedure.Text = "spSites_MAP_CCAExemptions" Then

            Me.pnlCCAExemptionFilter.Visible = True
            Me.chkFull_CCAExemption.Checked = True
            Me.chkPart_CCAExemption.Checked = True
            Me.chkNo_CCAExemption.Checked = True

            Dim bf1 As BoundField
            bf1 = New BoundField
            Dim bf2 As BoundField
            bf2 = New BoundField
            Dim bf3 As BoundField
            bf3 = New BoundField
            Dim bf4 As BoundField
            bf4 = New BoundField

            Dim imgf As ImageField
            imgf = New ImageField

            imgf.DataImageUrlField = "iconURL"
            imgf.HeaderText = ""
            grdTable.Columns.Add(imgf)

            bf1.DataField = "siteid"
            bf1.HeaderText = "ID"
            grdTable.Columns.Add(bf1)

            bf2.DataField = "SiteName"
            bf2.HeaderText = "Site Name"
            grdTable.Columns.Add(bf2)

            bf3.DataField = "CCLExemptionLevel"
            bf3.HeaderText = "Level"
            grdTable.Columns.Add(bf3)

        ElseIf Me.txtStoredProcedure.Text = "spSites_MAP_Rates" Then

            Me.pnlRatesFilter.Visible = True
            Me.chkRateBoth.Checked = True
            Me.chkRateElec.Checked = True
            Me.chkRateGas.Checked = True
            Me.chkRateNone.Checked = True

            Dim bf1 As BoundField
            bf1 = New BoundField
            Dim bf2 As BoundField
            bf2 = New BoundField
            Dim bf3 As BoundField
            bf3 = New BoundField
            Dim bf4 As BoundField
            bf4 = New BoundField
            Dim bf5 As BoundField
            bf5 = New BoundField
            Dim bf6 As BoundField
            bf6 = New BoundField

            Dim imgf As ImageField
            imgf = New ImageField

            imgf.DataImageUrlField = "iconURL"
            imgf.HeaderText = ""
            grdTable.Columns.Add(imgf)

            bf1.DataField = "siteid"
            bf1.HeaderText = "ID"
            grdTable.Columns.Add(bf1)

            bf2.DataField = "SiteName"
            bf2.HeaderText = "Site Name"
            grdTable.Columns.Add(bf2)

            bf3.DataField = "varDescription"
            bf3.HeaderText = "Description"
            grdTable.Columns.Add(bf3)

            bf4.DataField = "supplier_name"
            bf4.HeaderText = "Supplier"
            grdTable.Columns.Add(bf4)

            bf5.DataField = "ratestdate"
            bf5.HeaderText = "Start"
            grdTable.Columns.Add(bf5)

            bf6.DataField = "rateenddate"
            bf6.HeaderText = "End"
            grdTable.Columns.Add(bf6)

        ElseIf Me.txtStoredProcedure.Text = "spSites_MAP_CCACurrentPosition" Then

            Me.pnlCCAPassFail.Visible = True
            Me.chkCCAPass.Checked = True
            Me.chkCCAFail.Checked = True

            Dim bf1 As BoundField
            bf1 = New BoundField
            Dim bf2 As BoundField
            bf2 = New BoundField
            Dim bf3 As BoundField
            bf3 = New BoundField
            Dim bf4 As BoundField
            bf4 = New BoundField
            Dim bf5 As BoundField
            bf5 = New BoundField

            Dim imgf As ImageField
            imgf = New ImageField

            imgf.DataImageUrlField = "iconURL"
            imgf.HeaderText = ""
            grdTable.Columns.Add(imgf)

            bf1.DataField = "siteid"
            bf1.HeaderText = "ID"
            grdTable.Columns.Add(bf1)

            bf2.DataField = "SiteName"
            bf2.HeaderText = "Site Name"
            grdTable.Columns.Add(bf2)

            bf3.DataField = "varPass"
            bf3.HeaderText = "Pass/Fail"
            grdTable.Columns.Add(bf3)

            bf4.DataField = "fltResult"
            bf4.HeaderText = "Result"
            grdTable.Columns.Add(bf4)

            bf5.DataField = "varDataDate"
            bf5.HeaderText = "Data To"
            grdTable.Columns.Add(bf5)

        ElseIf Me.txtStoredProcedure.Text = "spSites_MAP_SiteElecProfiles" Then

            Me.pnlHHProfileFilter.Visible = True
            Me.chkHH.Checked = True
            Me.chkNonHH.Checked = True
            Me.chkHHNull.Checked = True

            Dim bf1 As BoundField
            bf1 = New BoundField
            Dim bf2 As BoundField
            bf2 = New BoundField
            Dim bf3 As BoundField
            bf3 = New BoundField
            Dim bf4 As BoundField
            bf4 = New BoundField

            Dim imgf As ImageField
            imgf = New ImageField


            imgf.DataImageUrlField = "iconURL"
            imgf.HeaderText = ""
            grdTable.Columns.Add(imgf)

            bf1.DataField = "siteid"
            bf1.HeaderText = "ID"
            grdTable.Columns.Add(bf1)

            bf2.DataField = "SiteName"
            bf2.HeaderText = "Site Name"
            grdTable.Columns.Add(bf2)

            bf3.DataField = "pcode"
            bf3.HeaderText = "Post Code"
            grdTable.Columns.Add(bf3)

        End If

    End Sub

    Public Function SQLStoredProcedure() As String

        Dim SQLProcedure As String = ""
        SQLProcedure = Me.txtStoredProcedure.Text

        If Me.txtStoredProcedure.Text = "spSites_MAP_ActiveSites" Then

            If Me.chkActive_Active.Checked = True Then
                SQLProcedure = SQLProcedure + " 1,"
            Else
                SQLProcedure = SQLProcedure + " 0,"
            End If

            If Me.chkActive_Inactive.Checked = True Then
                SQLProcedure = SQLProcedure + " 1,"
            Else
                SQLProcedure = SQLProcedure + " 0,"
            End If

            SQLProcedure = SQLProcedure + CStr(MySession.UserID) + ", "

            If Not Request.QueryString("method") Is Nothing Then
                SQLProcedure = SQLProcedure + "'" + CStr(Request.QueryString("method")) + "', "
            End If

            If Not Request.QueryString("custid") Is Nothing Then
                SQLProcedure = SQLProcedure + CStr(Request.QueryString("custid")) + ""
            End If

        ElseIf Me.txtStoredProcedure.Text = "spSites_MAP_CCAExemptions" Then

            If Me.chkFull_CCAExemption.Checked = True Then
                SQLProcedure = SQLProcedure + " 1,"
            Else
                SQLProcedure = SQLProcedure + " 0,"
            End If

            If Me.chkPart_CCAExemption.Checked = True Then
                SQLProcedure = SQLProcedure + " 1,"
            Else
                SQLProcedure = SQLProcedure + " 0,"
            End If

            If Me.chkNo_CCAExemption.Checked = True Then
                SQLProcedure = SQLProcedure + " 1,"
            Else
                SQLProcedure = SQLProcedure + " 0,"
            End If

            SQLProcedure = SQLProcedure + CStr(MySession.UserID) + ", "

            If Not Request.QueryString("method") Is Nothing Then
                SQLProcedure = SQLProcedure + "'" + CStr(Request.QueryString("method")) + "', "
            End If

            If Not Request.QueryString("custid") Is Nothing Then
                SQLProcedure = SQLProcedure + CStr(Request.QueryString("custid")) + ""
            End If


        ElseIf Me.txtStoredProcedure.Text = "spSites_MAP_Rates" Then

            If Me.rblView.SelectedItem.Value = "table" Then
                SQLProcedure = Me.txtStoredProcedure.Text & "Table"
            Else
                SQLProcedure = Me.txtStoredProcedure.Text
            End If

            If Me.chkRateBoth.Checked = True Then
                SQLProcedure = SQLProcedure + " 1,"
            Else
                SQLProcedure = SQLProcedure + " 0,"
            End If

            If Me.chkRateElec.Checked = True Then
                SQLProcedure = SQLProcedure + " 1,"
            Else
                SQLProcedure = SQLProcedure + " 0,"
            End If

            If Me.chkRateGas.Checked = True Then
                SQLProcedure = SQLProcedure + " 1,"
            Else
                SQLProcedure = SQLProcedure + " 0,"
            End If

            If Me.chkRateNone.Checked = True Then
                SQLProcedure = SQLProcedure + " 1,"
            Else
                SQLProcedure = SQLProcedure + " 0,"
            End If

            SQLProcedure = SQLProcedure + CStr(MySession.UserID) + ", "

            If Not Request.QueryString("method") Is Nothing Then
                SQLProcedure = SQLProcedure + "'" + CStr(Request.QueryString("method")) + "', "
            End If

            If Not Request.QueryString("custid") Is Nothing Then
                SQLProcedure = SQLProcedure + CStr(Request.QueryString("custid")) + ""
            End If


        ElseIf Me.txtStoredProcedure.Text = "spSites_MAP_CCACurrentPosition" Then


            If Me.chkCCAPass.Checked = True Then
                SQLProcedure = SQLProcedure + " 1,"
            Else
                SQLProcedure = SQLProcedure + " 0,"
            End If

            If Me.chkCCAFail.Checked = True Then
                SQLProcedure = SQLProcedure + " 1,"
            Else
                SQLProcedure = SQLProcedure + " 0,"
            End If

            SQLProcedure = SQLProcedure + CStr(MySession.UserID) + ", "

            If Not Request.QueryString("method") Is Nothing Then
                SQLProcedure = SQLProcedure + "'" + CStr(Request.QueryString("method")) + "', "
            End If

            If Not Request.QueryString("custid") Is Nothing Then
                SQLProcedure = SQLProcedure + CStr(Request.QueryString("custid")) + ""
            End If

        ElseIf Me.txtStoredProcedure.Text = "spSites_MAP_SiteElecProfiles" Then

            If Me.chkHH.Checked = True Then
                SQLProcedure = SQLProcedure + " 1,"
            Else
                SQLProcedure = SQLProcedure + " 0,"
            End If

            If Me.chkNonHH.Checked = True Then
                SQLProcedure = SQLProcedure + " 1,"
            Else
                SQLProcedure = SQLProcedure + " 0,"
            End If

            If Me.chkHHNull.Checked = True Then
                SQLProcedure = SQLProcedure + " 1,"
            Else
                SQLProcedure = SQLProcedure + " 0,"
            End If


            SQLProcedure = SQLProcedure + CStr(MySession.UserID) + ", "

            If Not Request.QueryString("method") Is Nothing Then
                SQLProcedure = SQLProcedure + "'" + CStr(Request.QueryString("method")) + "', "
            End If

            If Not Request.QueryString("custid") Is Nothing Then
                SQLProcedure = SQLProcedure + CStr(Request.QueryString("custid")) + ""
            End If


        End If

        Return SQLProcedure

    End Function

    Private Sub loadMap_LatLong()

        GMap1.reset()

        Dim sMapKey As String = ConfigurationManager.AppSettings("googlemaps.subgurim.net")
        Dim strLat As String = ""
        Dim strLong As String = ""
        Dim strLatLong As String = ""
        Dim strPostCode As String = ""

StartLoad:

        Dim latlong As New Subgurim.Controles.GLatLng("53.56079", "-4.110862")

        strLat = "53.56079"
        strLong = "-4.110862"

        Dim gLatLngCenter As New Subgurim.Controles.GLatLng(strLat, strLong)
        GMap1.setCenter(gLatLngCenter, 6, Subgurim.Controles.GMapType.GTypes.Hybrid)

        If Me.txtStoredProcedure.Text.Length > 0 Then

            Dim strSQL As String = SQLStoredProcedure()
            Dim myConnection As New OleDb.OleDbConnection(Core.CurrentConnectionString)
            Dim myConnection2 As New OleDb.OleDbConnection(Core.CurrentConnectionString)
            Dim myConnection3 As New OleDb.OleDbConnection(Core.CurrentConnectionString)
            Dim myConnection4 As New OleDb.OleDbConnection(Core.CurrentConnectionString)

            Dim myCommand As New OleDb.OleDbCommand(strSQL, myConnection)
            myCommand.Connection.Open()
            myCommand.CommandTimeout = 180
            Dim Reader As OleDb.OleDbDataReader = myCommand.ExecuteReader(CommandBehavior.CloseConnection)

            While Reader.Read

StartMarker:

                strPostCode = Reader("pcode")
                strLat = Reader("Lat").ToString
                strLong = Reader("Long").ToString

                If Len(strLat) = 0 Then
                    strLat = 0
                End If

                If Len(strLong) = 0 Then
                    strLong = 0
                End If

                If strLat & "," & strLong = "0,0" Then
                    GoTo SkipMarker
                End If

                Dim gLatLng As New Subgurim.Controles.GLatLng(strLat, strLong)

                Dim oMarker As New Subgurim.Controles.GMarker(gLatLng)

                If Reader("Icon") = "GreenIcon" Then
                    oMarker = New Subgurim.Controles.GMarker(gLatLng, GreenIcon)
                ElseIf Reader("Icon") = "BlueIcon" Then
                    oMarker = New Subgurim.Controles.GMarker(gLatLng, BlueIcon)
                ElseIf Reader("Icon") = "RedIcon" Then
                    oMarker = New Subgurim.Controles.GMarker(gLatLng, RedIcon)
                ElseIf Reader("Icon") = "OrangeIcon" Then
                    oMarker = New Subgurim.Controles.GMarker(gLatLng, OrangeIcon)
                ElseIf Reader("Icon") = "PurpleIcon" Then
                    oMarker = New Subgurim.Controles.GMarker(gLatLng, PurpleIcon)
                ElseIf Reader("Icon") = "YellowIcon" Then
                    oMarker = New Subgurim.Controles.GMarker(gLatLng, YellowIcon)
                Else

                End If

                Dim strSiteActive As String = ""
                If Reader("deadoralive") = 0 Then
                    strSiteActive = "Yes"
                Else
                    strSiteActive = "No"
                End If

                Dim options As New GInfoWindowOptions("Max Title", "<iframe src='https://www.google.co.uk' style='width:1005; height:100%;'></iframe>")

                Dim tabs As New List(Of GInfoWindowTab)()
                tabs.Add(New GInfoWindowTab("Site Info", "SiteID : " & Reader("siteid").ToString & "<br>Site Name : " & Reader("sitename").ToString & "<br>Active : " & strSiteActive & "<br>Post Code : " & Reader("pcode").ToString & "<br>Phone Number : " & Reader("phone_1").ToString & "<br>Fax Number : " & Reader("faxnumber").ToString & "<br>Operation : " & Reader("typeofop").ToString & "<br>Customer Site Ref : " & Reader("customersiteref").ToString & ""))


                If Me.txtStoredProcedure.Text = "spSites_MAP_ActiveSites" Then


                ElseIf Me.txtStoredProcedure.Text = "spSites_MAP_CCAExemptions" Then

                    Dim strCCATab As String = ""

                    Dim myCommand2 As New OleDb.OleDbCommand("SELECT * FROM uml_cms.dbo.tblcclsitedetails WHERE fksiteid = " & Reader("siteid"), myConnection2)
                    myCommand2.Connection.Open()
                    myCommand2.CommandTimeout = 180
                    Dim Reader2 As OleDb.OleDbDataReader = myCommand2.ExecuteReader(CommandBehavior.CloseConnection)

                    While Reader2.Read
                        strCCATab += "Exemption Level: " & Reader2("cclexemptionlevel") & "<br>"
                        strCCATab += "Facility Number: " & Reader2("cclref") & "<br><br>"
                    End While

                    Reader2.Close()
                    myConnection2.Close()

                    If Core.CheckForNullInteger(Core.data_select_value("SELECT count(*) FROM [portal.utilitymasters.co.uk].dbo.tempCrownEuropeCCAs WHERE intsitefk = " & Reader("siteid"))) > 0 Then

                        strCCATab &= "Key:    <font style='color:green;'>Pass</font>    <font style='color:red;'>Fail</font><br>"

                        strCCATab &= "<div style='width:400px; height:200px; overflow:auto;'><table style='width:380px'>"
                        strCCATab &= "<tr style='color:black;'><td style='width:70px'>Name</td><td style='width:160px'>Period</td><td style='width:60px'>Result</td><td>Data To</td></tr>"

                        Dim myCommand3 As New OleDb.OleDbCommand("SELECT *, CASE WHEN intPass = 2 THEN '---' ELSE LEFT(DATENAME(month,dtmDataDate),3) + ' ' + CAST(DATEPART(YEAR, dtmDataDate) AS VARCHAR(6)) END as varDataDate FROM [portal.utilitymasters.co.uk].dbo.tempCrownEuropeCCAs WHERE intsitefk = " & Reader("siteid") & " AND intPass IS NOT NULL ORDER BY intCrownEuropeCCAPK DESC ", myConnection3)
                        myCommand3.Connection.Open()
                        myCommand3.CommandTimeout = 180
                        Dim Reader3 As OleDb.OleDbDataReader = myCommand3.ExecuteReader(CommandBehavior.CloseConnection)

                        While Reader3.Read

                            If Len(Reader3("fltResult").ToString) = 0 Then
                                strCCATab &= "<tr style='color:black;'><td style='width:70px'>" & Reader3("varPeriodName") & "</td><td style='width:160px'>" & Reader3("varPeriod") & "</td><td style='width:60px'>---</td><td>" & Reader3("varDataDate") & "</td></tr>"
                            Else
                                If Core.CheckForNullInteger(Reader3("intPass")) = 0 Then
                                    strCCATab &= "<tr style='color:red;'><td style='width:70px'>" & Reader3("varPeriodName") & "</td><td style='width:160px'>" & Reader3("varPeriod") & "</td><td style='width:60px'>" & Reader3("fltResult") & "</td><td>" & Reader3("varDataDate") & "</td></tr>"
                                ElseIf Core.CheckForNullInteger(Reader3("intPass")) = 1 Then
                                    strCCATab &= "<tr style='color:green;'><td style='width:70px'>" & Reader3("varPeriodName") & "</td><td style='width:160px'>" & Reader3("varPeriod") & "</td><td style='width:60px'>" & Reader3("fltResult") & "</td><td>" & Reader3("varDataDate") & "</td></tr>"
                                Else
                                    strCCATab &= "<tr style='color:black;'><td style='width:70px'>" & Reader3("varPeriodName") & "</td><td style='width:160px'>" & Reader3("varPeriod") & "</td><td style='width:60px'>---</td><td>---</td></tr>"
                                End If
                            End If

                        End While

                        Reader3.Close()
                        myConnection3.Close()
                        strCCATab &= "</table></div>"

                    Else

                        strCCATab &= "Key:    <font style='color:green;'>Pass</font>    <font style='color:red;'>Fail</font><br>"

                        strCCATab &= "<div style='width:400px; height:200px; overflow:auto;'><table style='width:380px'>"
                        strCCATab &= "<tr style='color:black;'><td style='width:230px'>Name</td><td style='width:60px'>Result</td><td>Data To</td></tr>"

                        Dim myCommand3 As New OleDb.OleDbCommand("EXEC spSites_MAP_CCAPosition " & Reader("siteid"), myConnection3)
                        myCommand3.Connection.Open()
                        myCommand3.CommandTimeout = 180
                        Dim Reader3 As OleDb.OleDbDataReader = myCommand3.ExecuteReader(CommandBehavior.CloseConnection)

                        While Reader3.Read

                            If Len(Reader3("co2Eqv").ToString) = 0 Then
                                strCCATab &= "<tr style='color:black;'><td style='width:230px'>" & Reader3("varMilestone") & "</td><td style='width:60px'>---</td><td>" & Reader3("dtmMaxDate") & "</td></tr>"
                            Else
                                If Core.CheckForNullString(Reader3("varOutcome")) = "Fail" Then
                                    strCCATab &= "<tr style='color:red;'><td style='width:230px'>" & Reader3("varMilestone") & "</td><td style='width:60px'>" & FormatNumber(Reader3("co2Eqv").ToString, 2, True, True, True) & "</td><td>" & Reader3("dtmMaxDate") & "</td></tr>"
                                ElseIf Core.CheckForNullString(Reader3("varOutcome")) = "Pass" Then
                                    strCCATab &= "<tr style='color:green;'><td style='width:230px'>" & Reader3("varMilestone") & "</td><td style='width:60px'>" & FormatNumber(Reader3("co2Eqv").ToString, 2, True, True, True) & "</td><td>" & Reader3("dtmMaxDate") & "</td></tr>"
                                Else
                                    strCCATab &= "<tr style='color:black;'><td style='width:230px'>" & Reader3("varMilestone") & "</td><td style='width:60px'>---</td><td>---</td></tr>"
                                End If
                            End If

                        End While

                        Reader3.Close()
                        myConnection3.Close()
                        strCCATab &= "</table></div>"

                    End If

                    tabs.Add(New GInfoWindowTab("CCA", strCCATab))


                ElseIf Me.txtStoredProcedure.Text = "spSites_MAP_CCACurrentPosition" Then

                    Dim strCCATab As String = ""

                    Dim myCommand2 As New OleDb.OleDbCommand("SELECT * FROM uml_cms.dbo.tblcclsitedetails WHERE fksiteid = " & Reader("siteid"), myConnection2)
                    myCommand2.Connection.Open()
                    myCommand2.CommandTimeout = 180
                    Dim Reader2 As OleDb.OleDbDataReader = myCommand2.ExecuteReader(CommandBehavior.CloseConnection)

                    While Reader2.Read
                        strCCATab += "Exemption Level: " & Reader2("cclexemptionlevel") & "<br>"
                        strCCATab += "Facility Number: " & Reader2("cclref") & "<br><br>"
                    End While

                    Reader2.Close()
                    myConnection2.Close()

                    If Core.CheckForNullInteger(Core.data_select_value("SELECT count(*) FROM [portal.utilitymasters.co.uk].dbo.tempCrownEuropeCCAs WHERE intsitefk = " & Reader("siteid"))) > 0 Then

                        strCCATab &= "Key:    <font style='color:green;'>Pass</font>    <font style='color:red;'>Fail</font><br>"

                        strCCATab &= "<div style='width:400px; height:200px; overflow:auto;'><table style='width:380px'>"
                        strCCATab &= "<tr style='color:black;'><td style='width:70px'>Name</td><td style='width:160px'>Period</td><td style='width:60px'>Result</td><td>Data To</td></tr>"

                        Dim myCommand3 As New OleDb.OleDbCommand("SELECT *, CASE WHEN intPass = 2 THEN '---' ELSE LEFT(DATENAME(month,dtmDataDate),3) + ' ' + CAST(DATEPART(YEAR, dtmDataDate) AS VARCHAR(6)) END as varDataDate FROM [portal.utilitymasters.co.uk].dbo.tempCrownEuropeCCAs WHERE intsitefk = " & Reader("siteid") & " AND intPass IS NOT NULL ORDER BY intCrownEuropeCCAPK DESC ", myConnection3)
                        myCommand3.Connection.Open()
                        myCommand3.CommandTimeout = 180
                        Dim Reader3 As OleDb.OleDbDataReader = myCommand3.ExecuteReader(CommandBehavior.CloseConnection)

                        While Reader3.Read

                            If Len(Reader3("fltResult").ToString) = 0 Then
                                strCCATab &= "<tr style='color:black;'><td style='width:70px'>" & Reader3("varPeriodName") & "</td><td style='width:160px'>" & Reader3("varPeriod") & "</td><td style='width:60px'>---</td><td>" & Reader3("varDataDate") & "</td></tr>"
                            Else
                                If Core.CheckForNullInteger(Reader3("intPass")) = 0 Then
                                    strCCATab &= "<tr style='color:red;'><td style='width:70px'>" & Reader3("varPeriodName") & "</td><td style='width:160px'>" & Reader3("varPeriod") & "</td><td style='width:60px'>" & Reader3("fltResult") & "</td><td>" & Reader3("varDataDate") & "</td></tr>"
                                ElseIf Core.CheckForNullInteger(Reader3("intPass")) = 1 Then
                                    strCCATab &= "<tr style='color:green;'><td style='width:70px'>" & Reader3("varPeriodName") & "</td><td style='width:160px'>" & Reader3("varPeriod") & "</td><td style='width:60px'>" & Reader3("fltResult") & "</td><td>" & Reader3("varDataDate") & "</td></tr>"
                                Else
                                    strCCATab &= "<tr style='color:black;'><td style='width:70px'>" & Reader3("varPeriodName") & "</td><td style='width:160px'>" & Reader3("varPeriod") & "</td><td style='width:60px'>---</td><td>---</td></tr>"
                                End If
                            End If

                        End While

                        Reader3.Close()
                        myConnection3.Close()
                        strCCATab &= "</table></div>"

                    Else

                        strCCATab &= "Key:    <font style='color:green;'>Pass</font>    <font style='color:red;'>Fail</font><br>"

                        strCCATab &= "<div style='width:400px; height:200px; overflow:auto;'><table style='width:380px'>"
                        strCCATab &= "<tr style='color:black;'><td style='width:230px'>Name</td><td style='width:60px'>Result</td><td>Data To</td></tr>"

                        Dim myCommand3 As New OleDb.OleDbCommand("EXEC spSites_MAP_CCAPosition " & Reader("siteid"), myConnection3)
                        myCommand3.Connection.Open()
                        myCommand3.CommandTimeout = 180
                        Dim Reader3 As OleDb.OleDbDataReader = myCommand3.ExecuteReader(CommandBehavior.CloseConnection)

                        While Reader3.Read

                            If Len(Reader3("CarbonRequired").ToString) = 0 Then
                                strCCATab &= "<tr style='color:black;'><td style='width:230px'>" & Reader3("varMilestone") & "</td><td style='width:60px'>---</td><td>" & Reader3("dtmMaxDate") & "</td></tr>"
                            Else
                                If Core.CheckForNullString(Reader3("varOutcome")) = "Fail" Then
                                    strCCATab &= "<tr style='color:red;'><td style='width:230px'>" & Reader3("varMilestone") & "</td><td style='width:60px'>" & Reader3("CarbonRequired") & "</td><td>" & Reader3("dtmMaxDate") & "</td></tr>"
                                ElseIf Core.CheckForNullString(Reader3("varOutcome")) = "Pass" Then
                                    strCCATab &= "<tr style='color:green;'><td style='width:230px'>" & Reader3("varMilestone") & "</td><td style='width:60px'>" & Reader3("CarbonRequired") & "</td><td>" & Reader3("dtmMaxDate") & "</td></tr>"
                                Else
                                    strCCATab &= "<tr style='color:black;'><td style='width:230px'>" & Reader3("varMilestone") & "</td><td style='width:60px'>---</td><td>---</td></tr>"
                                End If
                            End If

                        End While

                        Reader3.Close()
                        myConnection3.Close()
                        strCCATab &= "</table></div>"

                    End If

                    tabs.Add(New GInfoWindowTab("CCA", strCCATab))





                ElseIf Me.txtStoredProcedure.Text = "spSites_MAP_Rates" Then

                    Dim strRatesTab As String = "<table>"

                    If Reader("varRates") = "Both" Or Reader("varRates") = "Elec" Then

                        strRatesTab &= "<tr><td colspan='2'><b>Electricity</b></td></tr> "

                        strRatesTab &= "<tr><td>Supplier:</td><td>" & Core.data_select_value("SELECT supplier_name FROM uml_cms.dbo.tblSuppliers WHERE SupplierId = " & Reader("supplierid")) & "</td></tr>"
                        strRatesTab &= "<tr><td>Start Date:</td><td>" & Core.data_select_value("SELECT DISTINCT uml_cms.dbo.FormatDateTime(ratestdate,'dd/mm/yyyy')  FROM uml_cms.dbo.qrycurrentelecrates WHERE siteid = " & Reader("siteid")) & "</td></tr>"
                        strRatesTab &= "<tr><td>Start Date:</td><td>" & Core.data_select_value("SELECT DISTINCT uml_cms.dbo.FormatDateTime(rateenddate,'dd/mm/yyyy')  FROM uml_cms.dbo.qrycurrentelecrates WHERE siteid = " & Reader("siteid")) & "</td></tr>"
                        strRatesTab &= "<tr><td></td></tr> "

                    End If
                    If Reader("varRates") = "Both" Or Reader("varRates") = "Gas" Then


                        strRatesTab &= "<tr><td colspan='2'><b>Gas</b></td></tr> "

                        strRatesTab &= "<tr><td>Supplier:</td><td>" & Core.data_select_value("SELECT supplier_name FROM uml_cms.dbo.qrycurrentgasrates INNER JOIN  uml_cms.dbo.tblgasrates ON rateid = gasrateid INNER JOIN uml_cms.dbo.tblSuppliers ON tblgasrates.supplierID = tblSuppliers.SupplierId WHERE qrycurrentgasrates.siteid = " & Reader("siteid")) & "</td></tr>"
                        strRatesTab &= "<tr><td>Start Date:</td><td>" & Core.data_select_value("SELECT uml_cms.dbo.FormatDateTime(ratestdate,'dd/mm/yyyy') FROM uml_cms.dbo.qrycurrentgasrates INNER JOIN  uml_cms.dbo.tblgasrates ON rateid = gasrateid INNER JOIN uml_cms.dbo.tblSuppliers ON tblgasrates.supplierID = tblSuppliers.SupplierId WHERE qrycurrentgasrates.siteid = " & Reader("siteid")) & "</td></tr>"
                        strRatesTab &= "<tr><td>Start Date:</td><td>" & Core.data_select_value("SELECT uml_cms.dbo.FormatDateTime(rateenddate,'dd/mm/yyyy') FROM uml_cms.dbo.qrycurrentgasrates INNER JOIN  uml_cms.dbo.tblgasrates ON rateid = gasrateid INNER JOIN uml_cms.dbo.tblSuppliers ON tblgasrates.supplierID = tblSuppliers.SupplierId WHERE qrycurrentgasrates.siteid = " & Reader("siteid")) & "</td></tr>"
                        strRatesTab &= "<tr><td></td></tr> "
                        strRatesTab &= "</table>"


                    End If

                    If Reader("varRates") = "None" Then
                    Else
                        strRatesTab &= "</table>"
                        tabs.Add(New GInfoWindowTab("Contracts", strRatesTab))
                    End If

                ElseIf Me.txtStoredProcedure.Text = "spSites_MAP_SiteElecProfiles" Then

                    Dim strMPANDetails As String = "<table>"

                    If Reader("profiletype") Is Nothing Then
                    Else
                        strMPANDetails &= "<tr><td colspan='2'><b>Meter Details</b></td></tr> "

                        strMPANDetails &= "<tr><td colspan='2' style='height:10px;'></tr> "

                        Dim myCommand4 As New OleDb.OleDbCommand("select * from uml_cms.dbo.tblsitempans Where fksite_id = " & Reader("siteid"), myConnection3)
                        myCommand4.Connection.Open()
                        myCommand4.CommandTimeout = 180
                        Dim Reader4 As OleDb.OleDbDataReader = myCommand4.ExecuteReader(CommandBehavior.CloseConnection)

                        While Reader4.Read

                            strMPANDetails &= "<tr><td style='width:140px;'>Profile Type:</td><td>" & Reader4("profiletype") & "</td></tr>"
                            strMPANDetails &= "<tr><td>Meter Time Switch:</td><td>" & Reader4("metertimeswitch") & "</td></tr>"
                            strMPANDetails &= "<tr><td>LLF:</td><td>" & Reader4("llf") & "</td></tr>"
                            strMPANDetails &= "<tr><td>Distribution:</td><td>" & Reader4("distribution") & "</td></tr>"
                            strMPANDetails &= "<tr><td>Supplier:</td><td>" & Reader4("supplier") & "</td></tr>"
                            strMPANDetails &= "<tr><td></td></tr> "

                        End While

                        Reader4.Close()
                        myConnection4.Close()


                        strMPANDetails &= "</table>"
                        tabs.Add(New GInfoWindowTab("Meter Details", strMPANDetails))

                    End If


                End If

                tabs.Add(New GInfoWindowTab("UM Info", "Analyst Name : " & Reader("AnalystName").ToString & "<br>Analyst Email : <a href='mailto:" & Reader("AnalystEmail").ToString & "'>" & Reader("AnalystEmail").ToString & "</a><br><br><br>Account Manager Name : " & Reader("AccountManagerName").ToString & "<br>Account Manager Email : <a href='mailto:" & Reader("AccountManagerEmail").ToString & "'>" & Reader("AccountManagerEmail").ToString & "</a>"))
                Dim wTabs As New GInfoWindowTabs(oMarker, tabs, options)

                GMap1.addInfoWindowTabs(wTabs)

SkipMarker:

            End While

            Reader.Close()
            myConnection.Close()

        End If

        Dim screenOverlay As New GScreenOverlay("https://portal.utilitymasters.co.uk/assets/googlemaps/SE_PoweredGoogle.png", New GScreenPoint(0, 0, unitEnum.pixels, unitEnum.pixels), New GScreenPoint(0, 0), New GScreenSize(0, 0))

        GMap1.Add(screenOverlay)

        GMap1.addControl(New GControl(GControl.preBuilt.LargeMapControl))
        GMap1.addControl(New GControl(GControl.preBuilt.MapTypeControl))
        'GMap1.enableHookMouseWheelToZoom = True

        Dim customCursor As New GCustomCursor(cursor.crosshair, cursor.pointer)
        GMap1.addCustomCursor(customCursor)


    End Sub ' New Map Procedure

    Private Sub LoadTable()


        If Me.txtStoredProcedure.Text.Length > 0 Then

            Dim strSQL As String = SQLStoredProcedure()

            With Me.grdTable
                .DataSource = Core.data_select("EXEC " & strSQL)
                .DataBind()
            End With


        End If


    End Sub ' Loads the table view or the stored procedure.


    Protected Sub ddlMapSelection_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlMapSelection.SelectedIndexChanged

        Me.txtStoredProcedure.Text = Me.ddlMapSelection.SelectedValue.ToString

        If Me.txtStoredProcedure.Text = "Current CCA Position" Then
            Me.txtStoredProcedure.Text = "spSites_MAP_CCACurrentPosition"
        End If
        PreparePage()

        If Me.map_view.Visible = True Then
            loadMap_LatLong()
        ElseIf Me.table_view.Visible = True Then
            LoadTable()
        End If



    End Sub

    ' This section is to handle all the filter check box changes 

    Protected Sub chkActive_Active_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkActive_Active.CheckedChanged
        loadMap_LatLong()
    End Sub

    Protected Sub chkActive_Inactive_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkActive_Inactive.CheckedChanged
        loadMap_LatLong()
    End Sub

    Protected Sub chkFull_CCAExemption_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkFull_CCAExemption.CheckedChanged
        loadMap_LatLong()
    End Sub

    Protected Sub chkPart_CCAExemption_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkPart_CCAExemption.CheckedChanged
        loadMap_LatLong()
    End Sub

    Protected Sub chkNo_CCAExemption_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkNo_CCAExemption.CheckedChanged
        loadMap_LatLong()
    End Sub

    Protected Sub rblView_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rblView.SelectedIndexChanged

        If Me.rblView.SelectedItem.Value = "map" Then

            Me.txtView.Text = "map"
            Me.map_view.Visible = True
            Me.table_view.Visible = False

            loadMap_LatLong()

        ElseIf Me.rblView.SelectedItem.Value = "table" Then
            Me.txtView.Text = "table"
            Me.map_view.Visible = False
            Me.table_view.Visible = True

            LoadTable()

        End If


    End Sub

End Class

