<%@ Page Language="VB" MasterPageFile="~/landing.master" AutoEventWireup="false" CodeFile="Default.aspx.vb" Inherits="mysites_map_Default" title="My Sites - Map" %>

<%@ Register Assembly="GMaps" Namespace="Subgurim.Controles" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="content_right" Runat="Server">



<style>
.general {background: transparent url('/assets/Schneider/content-back-general.jpg') no-repeat; padding-top:15px; padding-left:10px;}
.navgeneral 
{
position:absolute;top:83px;left:0px;width:220px;height:500px;background: white url('/assets/Schneider/left-nav-sites.png') no-repeat; padding-top:0px; padding-left:0px;
 }

.CompanySite
{
direction:ltr;
overflow-x:hidden;
overflow-y:scroll;
scrollbar-arrow-color: #4e658a;
scrollbar-3dlight-color: #777799;
scrollbar-darkshadow-color: #666677;
scrollbar-face-color: #a1b1c3;
scrollbar-highlight-color: #e9e9e9;
scrollbar-shadow-color: #a1b1c3;
scrollbar-track-color: #ffffff;
position:absolute;top:45px;left:0px;width:217px;height:445px;
}

</style>

<form id="form1" runat="server">
        <div  class="navgeneral">
            <div id="HJ" align="center" style="text-align:center;position:absolute;top:10px;left:5px;font-family:Myriad Pro;font-size:12px;color:Blue;"></div>
            <div class="CompanySite" >
              <ul class="styleformytreeview" runat="server" id="navOnlineReports">  </ul> 
            </div>
        </div>


        <div class="content_back general">
            
            <div id="topmenu" runat="server">
            
            <table>
                <tr valign="middle">
                    <td width="70px">
                        <asp:Label ID="lblDescription" Text="Description:" runat="server"></asp:Label>
                    </td>
                    <td width="200px" align="left">
                        <asp:Label ID="lblName" Text="Name" runat="server"></asp:Label>
                    </td>
                    <td width="250px">
                        
                    </td>
                </tr>
                <tr valign="middle">
                    <td width="70px" valign="top">
                        <asp:Label ID="lblView" Text="View: " runat="server" AssociatedControlID="rblView"></asp:Label>
                    </td>
                    <td width="200px" align="left">
                        <asp:RadioButtonList runat="server" ID="rblView" RepeatDirection="vertical" Width="100px" AutoPostBack="true" TextAlign="Left" >
                            <asp:ListItem Text="Map...." value="map" Selected="true"> </asp:ListItem>
                            <asp:ListItem Text="Table.." value="table"> </asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                    <td width="250px">
                        
                    </td>
                </tr>
                <tr valign="middle">
                    <td width="70px">
                        <asp:Label ID="lblQuery" Text="Query: " runat="server" AssociatedControlID="ddlMapSelection"></asp:Label>
                    </td>
                    <td colspan="2">
                        <asp:DropDownList ID="ddlMapSelection" runat="server" AutoPostBack="true" Width="150px">
                            <asp:ListItem Value="">-- please select --</asp:ListItem>
                            <asp:ListItem Value="spSites_MAP_ActiveSites">Active Sites</asp:ListItem>
                            <%--<asp:ListItem Value="spSites_MAP_CCAExemptions">CCA Exemptions</asp:ListItem>--%>
                            <asp:ListItem Value="spSites_MAP_Rates">Supply Contracts</asp:ListItem>
                            <asp:ListItem Value="spSites_MAP_SiteElecProfiles">HH Profiles</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
            </table>
                        
                <asp:TextBox ID="txtView" runat="server" Visible="False"></asp:TextBox>
                
            </div>
            
            <div id="map_view" class="map" runat="server"  >
                <div>
                <br />
                    <%--<asp:ImageButton ID="btnTraffic" runat="server" ImageUrl="http://images.utilitymasters.co.uk/stockimages/TrafficLight.gif" Visible="false" />  --%>
                    <asp:TextBox ID="txtStoredProcedure" runat="server" Visible="false"></asp:TextBox>
                    <cc1:GMap ID="GMap1" runat="server" Height="500px" Width="600px" />
                  
                    <asp:Panel ID="pnlActiveFilter" runat="server" Height="50px" Width="150px" Visible="false">
                        <table  style="width:150px;">
                            <tr>
                                <td style="width:50px;">
                                    <asp:Image ID="IconActive" runat="server" ImageUrl="https://portal.utilitymasters.co.uk/assets/icons/green.png"/>
                                </td>
                                <td style="width:100px; text-align:left;">
                                    <asp:Label ID="lblActive" runat="server" Text="Active"></asp:Label>
                                </td>
                                <td style="width:30px;">
                                    <asp:CheckBox ID="chkActive_Active" runat="server" Checked="true" AutoPostBack="true" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Image ID="IconInactive" runat="server" ImageUrl="https://portal.utilitymasters.co.uk/assets/icons/red.png"/>
                                </td>
                                <td style="text-align:left;">
                                    <asp:Label ID="lblInactive" runat="server" Text="Inactive"></asp:Label>
                                </td>
                                <td>
                                    <asp:CheckBox ID="chkActive_Inactive" runat="server" Checked="true" AutoPostBack="true" />
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    
                    <asp:Panel ID="pnlHHProfileFilter" runat="server" Height="50px" Width="150px" Visible="false">
                        <table  style="width:200px;">
                            <tr>
                                <td style="width:50px;">
                                    <asp:Image ID="HHIconHH" runat="server" ImageUrl="https://portal.utilitymasters.co.uk/assets/icons/green.png"/>
                                </td>
                                <td style="width:130px; text-align:left;">
                                    <asp:Label ID="HHlblHH" runat="server" Text="Half Hourly"></asp:Label>
                                </td>
                                <td style="width:30px;">
                                    <asp:CheckBox ID="chkHH" runat="server" Checked="true" AutoPostBack="true" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Image ID="HHIconNonHH" runat="server" ImageUrl="https://portal.utilitymasters.co.uk/assets/icons/orange.png"/>
                                </td>
                                <td style="text-align:left;">
                                    <asp:Label ID="HHlblNonHH" runat="server" Text="Non Half Hourly"></asp:Label>
                                </td>
                                <td>
                                    <asp:CheckBox ID="chkNonHH" runat="server" Checked="true" AutoPostBack="true" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Image ID="HHIconNull" runat="server" ImageUrl="https://portal.utilitymasters.co.uk/assets/icons/red.png"/>
                                </td>
                                <td style="text-align:left;">
                                    <asp:Label ID="HHlblNull" runat="server" Text="Unknown"></asp:Label>
                                </td>
                                <td>
                                    <asp:CheckBox ID="chkHHNull" runat="server" Checked="true" AutoPostBack="true" />
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    
                    <asp:Panel ID="pnlCCAExemptionFilter" runat="server" Height="50px" Width="250px" Visible="false">
                        <table  style="width:300px;">
                            <tr>
                                <td style="width:50px;">
                                    <asp:Image ID="IconFull_CCAExemption" runat="server" ImageUrl="https://portal.utilitymasters.co.uk/assets/icons/green.png"/>
                                </td>
                                <td style="width:200px; text-align:left;">
                                    <asp:Label ID="lblFull_CCAExemption" runat="server" Text="Full CCA Exemption"></asp:Label>
                                </td>
                                <td style="width:30px;">
                                    <asp:CheckBox ID="chkFull_CCAExemption" runat="server" Checked="true" AutoPostBack="true" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Image ID="IconPart" runat="server" ImageUrl="https://portal.utilitymasters.co.uk/assets/icons/orange.png"/>
                                </td>
                                <td style="text-align:left;">
                                    <asp:Label ID="lblPart_CCAExemption" runat="server" Text="Part CCA Exemption"></asp:Label>
                                </td>
                                <td>
                                    <asp:CheckBox ID="chkPart_CCAExemption" runat="server" Checked="true" AutoPostBack="true" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Image ID="IconNo" runat="server" ImageUrl="https://portal.utilitymasters.co.uk/assets/icons/red.png"/>
                                </td>
                                <td style="text-align:left;">
                                    <asp:Label ID="lblNo_CCAExemption" runat="server" Text="No CCA Exemption"></asp:Label>
                                </td>
                                <td>
                                    <asp:CheckBox ID="chkNo_CCAExemption" runat="server" Checked="true" AutoPostBack="true" />
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
   
                    <asp:Panel ID="pnlRatesFilter" runat="server" Height="50px" Width="250px" Visible="false">
                        <table  style="width:300px;">
                            <tr>
                                <td style="width:50px;">
                                    <asp:Image ID="iconRateBoth" runat="server" ImageUrl="https://portal.utilitymasters.co.uk/assets/icons/green.png"/>
                                </td>
                                <td style="width:200px; text-align:left;">
                                    <asp:Label ID="lblRateBoth" runat="server" Text="Electricity and Gas"></asp:Label>
                                </td>
                                <td style="width:30px;">
                                    <asp:CheckBox ID="chkRateBoth" runat="server" Checked="true" AutoPostBack="true" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Image ID="iconRateElec" runat="server" ImageUrl="https://portal.utilitymasters.co.uk/assets/icons/orange.png"/>
                                </td>
                                <td style="text-align:left;">
                                    <asp:Label ID="lblRateElec" runat="server" Text="Electricity Only"></asp:Label>
                                </td>
                                <td>
                                    <asp:CheckBox ID="chkRateElec" runat="server" Checked="true" AutoPostBack="true" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Image ID="iconRateGas" runat="server" ImageUrl="https://portal.utilitymasters.co.uk/assets/icons/blue.png"/>
                                </td>
                                <td style="text-align:left;">
                                    <asp:Label ID="lblRateGas" runat="server" Text="Gas Only"></asp:Label>
                                </td>
                                <td>
                                    <asp:CheckBox ID="chkRateGas" runat="server" Checked="true" AutoPostBack="true" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Image ID="iconRateNone" runat="server" ImageUrl="https://portal.utilitymasters.co.uk/assets/icons/red.png"/>
                                </td>
                                <td style="text-align:left;">
                                    <asp:Label ID="lblRateNone" runat="server" Text="No Rates"></asp:Label>
                                </td>
                                <td>
                                    <asp:CheckBox ID="chkRateNone" runat="server" Checked="true" AutoPostBack="true" />
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    
   
                    <asp:Panel ID="pnlCCAPassFail" runat="server" Height="50px" Width="250px" Visible="false">
                        <table  style="width:150px;">
                            <tr>
                                <td style="width:50px;">
                                    <asp:Image ID="iconCCAPass" runat="server" ImageUrl="https://portal.utilitymasters.co.uk/assets/icons/green.png"/>
                                </td>
                                <td style="width:70px; text-align:left;">
                                    <asp:Label ID="lblCCAPass" runat="server" Text="Pass"></asp:Label>
                                </td>
                                <td style="width:30px;">
                                    <asp:CheckBox ID="chkCCAPass" runat="server" Checked="true" AutoPostBack="true" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Image ID="iconCCAFail" runat="server" ImageUrl="https://portal.utilitymasters.co.uk/assets/icons/red.png"/>
                                </td>
                                <td style="text-align:left;">
                                    <asp:Label ID="lblCCAFail" runat="server" Text="Fail"></asp:Label>
                                </td>
                                <td>
                                    <asp:CheckBox ID="chkCCAFail" runat="server" Checked="true" AutoPostBack="true" />
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                                      
                </div>
            </div>
            
            <div id="table_view" class="table" runat="server" visible="false">
            <br />
                <asp:Panel ID="pnlTable" runat="server" ScrollBars="Vertical" Height="300" Width="100%">
                  
                    <asp:GridView borderwidth="0px" ID="grdTable" cssclass="users" runat="server" headerstyle-cssclass="header" AutoGenerateColumns="False">
                        <AlternatingRowStyle CssClass="tralt" />
                        <headerstyle cssclass="header" />
                    </asp:GridView>
                </asp:Panel>
            </div>
            
            
        </div>
    </form>
    
</asp:Content>

