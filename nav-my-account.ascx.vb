
Partial Class nav_my_account
    Inherits System.Web.UI.UserControl



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        Me.my_treeview.InnerHtml = ""
        Dim strSQL As String = "EXEC spPortal_TREEVIEW " & MySession.UserID
        Dim myConnection As New OleDb.OleDbConnection(Core.CurrentConnectionString)

        Dim myCommand As New OleDb.OleDbCommand(strSQL, myConnection)
        myCommand.Connection.Open()
        myCommand.CommandTimeout = 180
        Dim Reader As OleDb.OleDbDataReader = myCommand.ExecuteReader(CommandBehavior.CloseConnection)

        While Reader.Read

            Select Case CStr(Reader(3))
                Case "Company"
                    Me.my_treeview.InnerHtml += "<li class='styleformytreeview_company'><a href='my-account.aspx?custid=" & Reader(0).ToString & "'>" & Reader(1).ToString & "</a></li>"
                Case "Header"
                    Me.my_treeview.InnerHtml &= "<li class='styleformytreeview_header'>" & Reader(1).ToString & "</li>"
                Case "Site"
                    Me.my_treeview.InnerHtml &= "<li class='styleformytreeview_site'>" & Left(Reader(1).ToString, 23) & "..</li>"
                Case "Group"
                    Me.my_treeview.InnerHtml &= "<li class='styleformytreeview_group'>" & Reader(1).ToString & "</li>"

            End Select


        End While


    End Sub
End Class
