
Partial Class nav_my_sites
    Inherits System.Web.UI.UserControl
    
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        HideMenuItems()

        Dim strURL As String = Core.CheckForNullString(Me.Page.Request.Url.PathAndQuery.ToString)

        'sets the menu item to green when on the relevant page
        If strURL.Contains("/my-sites.aspx") Then
            LoadMainMenu()
            Me.lnkSitesHome.Text = "My Sites Home"
        ElseIf strURL.Contains("/mysites/map/default.aspx") Then
            'LoadCustomerSiteList()
            Me.lnkSitesHome.Text = "My Sites Map"
        ElseIf strURL.Contains("/mysites/search/default.aspx") Then
            'LoadCustomerSiteList()
            Me.lnkSitesHome.Text = "My Sites Search"
        ElseIf strURL.Contains("/supplydetails/default.asp") Then
            'LoadCustomerSiteList()
            Me.lnkSitesHome.Text = "My Site"



        End If


    End Sub

    Private Sub LoadCustomerSiteList()


        Me.nav.Visible = False
        Me.my_treeview.Visible = True

        Me.my_treeview.InnerHtml = ""
        Dim strSQL As String = "EXEC spPortal_TREEVIEW " & MySession.UserID
        Dim myConnection As New OleDb.OleDbConnection(Core.CurrentConnectionString)

        Dim myCommand As New OleDb.OleDbCommand(strSQL, myConnection)
        myCommand.Connection.Open()
        myCommand.CommandTimeout = 180
        Dim Reader As OleDb.OleDbDataReader = myCommand.ExecuteReader(CommandBehavior.CloseConnection)

        While Reader.Read

            Select Case CStr(Reader(3))
                Case "Company"
                    Me.my_treeview.InnerHtml += "<li class='styleformytreeview_company'><a href='default.aspx?custid=" & Reader(0).ToString & "&method=Customer'>" & Reader(1).ToString & "</a></li>"
                Case "Header"
                    Me.my_treeview.InnerHtml += "<li class='styleformytreeview_header'>" & Reader(1).ToString & "</li>"
                Case "Site"
                    Me.my_treeview.InnerHtml += "<li class='styleformytreeview_site'><a href='default.aspx?custid=" & Reader(0).ToString & "&method=Site'>" & Left(Reader(1).ToString, 23) & "..</a></li>"
                Case "Group"
                    Me.my_treeview.InnerHtml += "<li class='styleformytreeview_group'><a href='default.aspx?custid=" & Reader(0).ToString & "&method=Group'>" & Reader(1).ToString & "</a></li>"
            End Select

        End While


        Me.my_treeview.InnerHtml += "<li class='styleformytreeview_company' style='height:10px;'></li>"
        Me.my_treeview.InnerHtml += "<li class='styleformytreeview_company'><a href='../../my-sites.aspx'><< Back</a></li>"
    End Sub

    Private Sub LoadMainMenu()

        Me.nav.Visible = True
        Me.my_treeview.Visible = False
    End Sub

    Private Sub HideMenuItems()
        Me.nav.Visible = False
        Me.my_treeview.Visible = False
    End Sub


End Class
