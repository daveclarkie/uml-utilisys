Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports PostcodeToOS
Imports Subgurim.Controles
Imports System.Data.SqlClient
Imports System.Collections.Generic

Imports System.Drawing
Partial Class CustomerMap
    Inherits System.Web.UI.Page

    Public GreenIcon As New GIcon()
    Public BlueIcon As New GIcon()
    Public RedIcon As New GIcon()
    Public OrangeIcon As New GIcon()
    Public PurpleIcon As New GIcon()
    Public YellowIcon As New GIcon()

    Public Sub LoadIcons()

        GreenIcon.image = "https://portal.utilitymasters.co.uk/assets/icons/green.png"
        GreenIcon.shadow = "https://portal.utilitymasters.co.uk/assets/icons/small_shadow.png"
        GreenIcon.iconSize = New GSize(12, 20)
        GreenIcon.shadowSize = New GSize(12, 20)
        GreenIcon.iconAnchor = New GPoint(10, 20)

        BlueIcon.image = "https://portal.utilitymasters.co.uk/assets/icons/blue.png"
        BlueIcon.shadow = "https://portal.utilitymasters.co.uk/assets/icons/small_shadow.png"
        BlueIcon.iconSize = New GSize(12, 20)
        BlueIcon.shadowSize = New GSize(12, 20)
        BlueIcon.iconAnchor = New GPoint(10, 20)

        RedIcon.image = "https://portal.utilitymasters.co.uk/assets/icons/red.png"
        RedIcon.shadow = "https://portal.utilitymasters.co.uk/assets/icons/small_shadow.png"
        RedIcon.iconSize = New GSize(12, 20)
        RedIcon.shadowSize = New GSize(12, 20)
        RedIcon.iconAnchor = New GPoint(10, 20)

        OrangeIcon.image = "https://portal.utilitymasters.co.uk/assets/icons/orange.png"
        OrangeIcon.shadow = "https://portal.utilitymasters.co.uk/assets/icons/small_shadow.png"
        OrangeIcon.iconSize = New GSize(12, 20)
        OrangeIcon.shadowSize = New GSize(12, 20)
        OrangeIcon.iconAnchor = New GPoint(10, 20)

        PurpleIcon.image = "https://portal.utilitymasters.co.uk/assets/icons/purple.png"
        PurpleIcon.shadow = "https://portal.utilitymasters.co.uk/assets/icons/small_shadow.png"
        PurpleIcon.iconSize = New GSize(12, 20)
        PurpleIcon.shadowSize = New GSize(12, 20)
        PurpleIcon.iconAnchor = New GPoint(10, 20)

        YellowIcon.image = "https://portal.utilitymasters.co.uk/assets/icons/yellow.png"
        YellowIcon.shadow = "https://portal.utilitymasters.co.uk/assets/icons/small_shadow.png"
        YellowIcon.iconSize = New GSize(12, 20)
        YellowIcon.shadowSize = New GSize(12, 20)
        YellowIcon.iconAnchor = New GPoint(10, 20)

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        LoadIcons()
        
        Me.map_view.Visible = True
        loadMap_LatLong()

    End Sub

    Private Sub loadMap_LatLong()

        GMap1.reset()

        Dim sMapKey As String = ConfigurationManager.AppSettings("googlemaps.subgurim.net")
        Dim strLat As String = ""
        Dim strLong As String = ""
        Dim strLatLong As String = ""
        Dim strPostCode As String = ""

StartLoad:

        Dim latlong As New Subgurim.Controles.GLatLng("53.56079", "-4.110862")

        strLat = "53.56079"
        strLong = "-4.110862"

        Dim gLatLngCenter As New Subgurim.Controles.GLatLng(strLat, strLong)
        GMap1.setCenter(gLatLngCenter, 6, Subgurim.Controles.GMapType.GTypes.Hybrid)


        Dim strSQL As String = "EXEC spCustomer_MAP_AttendedCRCSeminar"
        Dim myConnection As New OleDb.OleDbConnection(Core.CurrentConnectionString)

        Dim myCommand As New OleDb.OleDbCommand(strSQL, myConnection)
        myCommand.Connection.Open()
        myCommand.CommandTimeout = 180
        Dim Reader As OleDb.OleDbDataReader = myCommand.ExecuteReader(CommandBehavior.CloseConnection)

        While Reader.Read

StartMarker:

            'strPostCode = Reader("pcode")
            strLat = Reader("Lat").ToString
            strLong = Reader("Long").ToString

            If Len(strLat) = 0 Then
                strLat = 0
            End If

            If Len(strLong) = 0 Then
                strLong = 0
            End If

            If strLat & "," & strLong = "0,0" Then
                GoTo SkipMarker
            End If

            Dim gLatLng As New Subgurim.Controles.GLatLng(strLat, strLong)

            Dim oMarker As New Subgurim.Controles.GMarker(gLatLng)

            If Reader("Icon") = "GreenIcon" Then
                oMarker = New Subgurim.Controles.GMarker(gLatLng, GreenIcon)
            ElseIf Reader("Icon") = "BlueIcon" Then
                oMarker = New Subgurim.Controles.GMarker(gLatLng, BlueIcon)
            ElseIf Reader("Icon") = "RedIcon" Then
                oMarker = New Subgurim.Controles.GMarker(gLatLng, RedIcon)
            ElseIf Reader("Icon") = "OrangeIcon" Then
                oMarker = New Subgurim.Controles.GMarker(gLatLng, OrangeIcon)
            ElseIf Reader("Icon") = "PurpleIcon" Then
                oMarker = New Subgurim.Controles.GMarker(gLatLng, PurpleIcon)
            ElseIf Reader("Icon") = "YellowIcon" Then
                oMarker = New Subgurim.Controles.GMarker(gLatLng, YellowIcon)
            Else

            End If


            Dim options As New GInfoWindowOptions("Max Title", "<iframe src='http://www.google.co.uk' style='width:1005; height:100%;'></iframe>")

            Dim tabs As New List(Of GInfoWindowTab)()
            tabs.Add(New GInfoWindowTab("Customer", "CustID : " & Reader("custid").ToString & "<br>Customer Name : " & Reader("customername").ToString & ""))



            Dim wTabs As New GInfoWindowTabs(oMarker, tabs, options)

            GMap1.addInfoWindowTabs(wTabs)

SkipMarker:

        End While

        Reader.Close()
        myConnection.Close()

        Dim screenOverlay As New GScreenOverlay("https://portal.utilitymasters.co.uk/assets/googlemaps/MC_PoweredGoogle.png", New GScreenPoint(0, 0, unitEnum.pixels, unitEnum.pixels), New GScreenPoint(0, 0), New GScreenSize(0, 0))

        GMap1.Add(screenOverlay)

        GMap1.addControl(New GControl(GControl.preBuilt.LargeMapControl))
        GMap1.addControl(New GControl(GControl.preBuilt.MapTypeControl))
        GMap1.enableHookMouseWheelToZoom = True

        Dim customCursor As New GCustomCursor(cursor.crosshair, cursor.pointer)
        GMap1.addCustomCursor(customCursor)


    End Sub ' New Map Procedure

End Class
