<%@ Page Language="VB" AutoEventWireup="false" CodeFile="CustomerMap.aspx.vb" Inherits="CustomerMap" title="Customer Map" %>
<%@ Register Assembly="GMaps" Namespace="Subgurim.Controles" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">

</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
            
           
            <div id="map_view" class="map" runat="server"  >
                <div>
                <br />
                    
                    <cc1:GMap ID="GMap1" runat="server" Height="768px" Width="1024px" />
                  
                    <asp:Panel ID="pnlActiveFilter" runat="server" Height="50px" Width="150px" Visible="True">
                        <table  style="width:150px;">
                            <tr>
                                <td style="width:50px;">
                                    <asp:Image ID="IconAttended" runat="server" ImageUrl="https://portal.utilitymasters.co.uk/assets/icons/green.png"/>
                                </td>
                                <td style="width:100px; text-align:left;">
                                    <asp:Label ID="lblAttended" runat="server" Text="Attended"></asp:Label>
                                </td>
                                
                            </tr>
                            <tr>
                                <td>
                                    <asp:Image ID="IconNotAttended" runat="server" ImageUrl="https://portal.utilitymasters.co.uk/assets/icons/red.png"/>
                                </td>
                                <td style="text-align:left;">
                                    <asp:Label ID="lblNotAttended" runat="server" Text="Not Attended"></asp:Label>
                                </td>
                                
                            </tr>
                        </table>
                    </asp:Panel>
                                      
                </div>
            </div>
            
    </form>
</body>
</html>
