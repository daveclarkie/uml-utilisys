Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports PostcodeToOS
Imports Subgurim.Controles
Imports System.Data.SqlClient
Imports System.Collections.Generic


Partial Class postcodetodatabase
    Inherits System.Web.UI.Page
    Dim connectionstringCMS As String = "Provider=SQLOLEDB.1;Data Source=uk-ed0-sqlcl-01.MCEG.local;database=UML_CMS;Trusted_Connection=True;"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim sMapKey As String = ConfigurationManager.AppSettings("googlemaps.subgurim.net")
        Dim strLat As String = ""
        Dim strLong As String = ""
        Dim strLatLong As String = ""
        Dim strPostCode As String = ""
        Dim intSiteID As Integer = 0

        Dim strSQL As String = "select * from tblSites where pcode is not null and Lat is null and len(pcode) > 4"
        Dim myConnection As New OleDb.OleDbConnection(connectionstringCMS)

        Dim myCommand As New OleDb.OleDbCommand(strSQL, myConnection)
        myCommand.Connection.Open()
        myCommand.CommandTimeout = 180
        Dim Reader As OleDb.OleDbDataReader = myCommand.ExecuteReader(CommandBehavior.CloseConnection)

        While Reader.Read

StartPostCode:

            intSiteID = Reader("siteid")
            strPostCode = Reader("pcode")
            strLat = PostcodeToOS.Postcode.ToLat(Reader("pcode"), 0, 0)
            strLong = PostcodeToOS.Postcode.ToLong(Reader("pcode"), 0, 0)

            Dim strLatTEST As String = PostcodeToOS.Postcode.ToLat(Reader("pcode"), 0, 0)
            Dim strLongTEST As String = PostcodeToOS.Postcode.ToLong(Reader("pcode"), 0, 0)

            If strLat <> strLatTEST Then
                GoTo StartPostCode
            End If

            If strLong <> strLongTEST Then
                GoTo StartPostCode
            End If


            Dim strSQL2 As String = "UPDATE tblSites SET Lat = '" & strLat & "', Long = '" & strLong & "' WHERE siteid = " & intSiteID
            Dim myConnection2 As New OleDb.OleDbConnection(connectionstringCMS)

            Dim myCommand2 As New OleDb.OleDbCommand(strSQL2, myConnection2)
            myCommand2.Connection.Open()
            myCommand2.CommandTimeout = 180
            Dim Reader2 As OleDb.OleDbDataReader = myCommand2.ExecuteReader(CommandBehavior.CloseConnection)

            Reader2.Read()
            Reader2.Close()

            myConnection2.Close()

        End While

        Reader.Close()
        myConnection.Close()
    End Sub
End Class
