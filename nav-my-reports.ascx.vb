
Partial Class nav_my_reports
    Inherits System.Web.UI.UserControl



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        LoadMenu()
        HideMenuItems()

        Dim strURL As String = Core.CheckForNullString(Me.Page.Request.Url.PathAndQuery.ToString)

        'sets the menu item to green when on the relevant page
        If strURL.Contains("/my-reports.aspx") Then
            Me.navOnlineReports.Visible = True
            LoadReports()
        ElseIf strURL.Contains("/reports/dataanalysis/default.aspx") Then
            Me.navOnlineReports.Visible = True
            LoadReportsDataAnalysis()
        ElseIf strURL.Contains("/reports/scheduledreports/default.aspx") Then
            Me.navOnlineReports.Visible = True
            LoadReports_ScheduledReports()
        ElseIf strURL.Contains("/reports/utilityanalysis/default.aspx") Then
            Me.navOnlineReports.Visible = True
            LoadReports_UtilityAnalysis()
        ElseIf strURL.Contains("/reports/financialreports/default.aspx") Then
            Me.navOnlineReports.Visible = True
            LoadReports_FinancialReports()
        ElseIf strURL.Contains("/reports/carbonreports/default.aspx") Then
            Me.navOnlineReports.Visible = True
            LoadReports_CarbonReports()
        ElseIf strURL.Contains("/reports/carbonreports/crcmanagementarea/default.aspx") Then
            Me.navOnlineReports.Visible = True
            LoadTreeview_CRC()
        ElseIf strURL.Contains("/reports/marketinformation/") Then
            Me.navOnlineReports.Visible = True
            LoadReports_MarketInformation()
        ElseIf strURL.Contains("/reports/bespokereporting/default.aspx") Then
            Me.navOnlineReports.Visible = True
            LoadReports_BespokeReports()
        ElseIf strURL.Contains("/reports/carbonreports/crcmanagementarea/CRCEvidencePack/default.aspx?phase") Then

            Me.pnlCRCEvidencePackMenu.Visible = True
            Me.lnkReportsHome.Text = "<< CRC Management Area"
            Me.lnkReportsHome.NavigateUrl = "~/reports/carbonreports/crcmanagementarea/default.aspx"
            Me.lnkReportsHome.Visible = False
            LoadCRCEvidencePack()

        ElseIf strURL.Contains("/reports/MyReports/Default.aspx") Then
            Me.navOnlineReports.Visible = True
            LoadMyReports()
        Else
            Me.navOnlineReports.Visible = True
            LoadTreeview_Report()
        End If
        '
    End Sub

    Private Sub HideMenuItems()
        Me.nav.Visible = False
        Me.navOnlineReports.Visible = False
        Me.pnlCRCEvidencePackMenu.Visible = False
    End Sub

    Private Sub LoadCRCEvidencePack()
        Dim ReturnedID As Integer = Trim(Request.QueryString("id"))
        Dim ReturnedPhase As Integer = Trim(Request.QueryString("phase"))
        Dim ReturnedNet As String = "true"
        If MySession.UserID = 77 Or MySession.UserID = 195 Then
            ReturnedNet = "false"
        End If

        Session("phase") = ReturnedPhase

        Dim controlList As New ArrayList()
        AddControls(Page.Controls, controlList)
        For Each str As String In controlList
            If str.StartsWith("sub") Then
                Dim link As HyperLink = (Me.FindControl(str))
                link.NavigateUrl = link.NavigateUrl & "?id=" & ReturnedID & "&net=" & ReturnedNet & "&phase=" & ReturnedPhase
            End If
        Next
    End Sub

    Private Sub ListControlCollections()
        Dim controlList As New ArrayList()
        AddControls(Page.Controls, controlList)
        For Each str As String In controlList
            Response.Write(str & "<br/>")
        Next
        Response.Write("Total Controls:" & controlList.Count.ToString)
    End Sub
    Private Sub AddControls(ByVal page As ControlCollection, ByVal controlList As ArrayList)
        For Each c As Control In page
            If c.ID IsNot Nothing Then
                controlList.Add(c.ID)
            End If
            If c.HasControls() Then
                AddControls(c.Controls, controlList)
            End If
        Next
    End Sub

    Private Sub LoadReports()

        Me.lnkReportsHome.Text = "Custom Report Page"
        Me.lnkReportsHome.NavigateUrl = "~/reports/MyReports/Default.aspx"

        Dim myConnection As New OleDb.OleDbConnection(Core.CurrentConnectionString)


        Dim strSQL As String = "EXEC spReports_ListReportTypes " & MySession.UserID


        Using connection As New SqlConnection(Core.ConnectionString)
            connection.Open()
            Dim command As New SqlCommand(strSQL, connection)
            command.CommandType = CommandType.Text
            Dim reader As SqlDataReader = command.ExecuteReader()

            While reader.Read

                Dim strUrl As String = reader("varURLPath").ToString

                If reader("intCount") > 0 Then
                    Me.navOnlineReports.InnerHtml += "<li class='styleformytreeview_company'><a href='" & strUrl & "'>" & reader("varReportTypeNiceName") & "</a></li>"
                Else
                    Me.navOnlineReports.InnerHtml += "<li class='styleformytreeview_company'><a style='color:silver;'>" & reader("varReportTypeNiceName") & "</a></li>"
                End If

            End While

        End Using

        myConnection.Close()


    End Sub
    Private Sub LoadMyReports()

        Me.lnkReportsHome.Text = "Back to Reports Home"
        Me.lnkReportsHome.NavigateUrl = "~/my-reports.aspx"

        Dim myConnection As New OleDb.OleDbConnection(Core.CurrentConnectionString)
        Dim strSQL As String = "SELECT tlkpReports.* FROM [portal.utilitymasters.co.uk].dbo.tblUsersMyReports INNER JOIN [portal.utilitymasters.co.uk].dbo.tlkpReports ON intReportPK = intReportFK WHERE intUserFK = " & MySession.UserID

        Using connection As New SqlConnection(Core.ConnectionString)
            connection.Open()
            Dim command As New SqlCommand(strSQL, connection)
            command.CommandType = CommandType.Text
            Dim reader As SqlDataReader = command.ExecuteReader()

            While reader.Read

                Dim strUrl As String = reader("varOnlineURL").ToString
                Me.navOnlineReports.InnerHtml += "<li class='styleformytreeview_company'><a href='" & strUrl & "'>" & reader("varReportName") & "</a></li>"


            End While

        End Using

        myConnection.Close()



    End Sub

    Private Sub LoadReports_ScheduledReports()

        Me.lnkReportsHome.Text = "Back to Reports Home"
        Me.lnkReportsHome.NavigateUrl = "~/my-reports.aspx"

        Dim myConnection As New OleDb.OleDbConnection(Core.CurrentConnectionString)


        Dim strSQL As String = "EXEC spReports_ListReports " & MySession.UserID & ", 8"


        Using connection As New SqlConnection(Core.ConnectionString)
            connection.Open()
            Dim command As New SqlCommand(strSQL, connection)
            command.CommandType = CommandType.Text
            Dim reader As SqlDataReader = command.ExecuteReader()

            While reader.Read

                Dim strUrl As String = reader("varOnlineURL").ToString

                If reader("intReportDisabled") = 0 Then
                    Me.navOnlineReports.InnerHtml += "<li class='styleformytreeview_company'><a href='" & strUrl & "'>" & Replace(reader("varReportName"), " Scheduled", "") & "</a></li>"
                Else
                    Me.navOnlineReports.InnerHtml += "<li class='styleformytreeview_company'><a style='color:silver;'>" & Replace(reader("varReportName"), " Scheduled", "") & "</a></li>"
                End If

            End While

        End Using

        myConnection.Close()

        'Me.navOnlineReports.InnerHtml += "<li class='styleformytreeview_company' style='height:10px;'></li>"
        'Me.navOnlineReports.InnerHtml += "<li class='styleformytreeview_company'><a href='../../my-reports.aspx'><< Back</a></li>"

    End Sub
    Private Sub LoadReports_CarbonReports()
        Me.lnkReportsHome.Text = "Back to Reports Home"
        Me.lnkReportsHome.NavigateUrl = "~/my-reports.aspx"

        Dim myConnection As New OleDb.OleDbConnection(Core.CurrentConnectionString)


        Dim strSQL As String = "EXEC spReports_ListReports " & MySession.UserID & ", 10"


        Using connection As New SqlConnection(Core.ConnectionString)
            connection.Open()
            Dim command As New SqlCommand(strSQL, connection)
            command.CommandType = CommandType.Text
            Dim reader As SqlDataReader = command.ExecuteReader()

            While reader.Read

                Dim strUrl As String = reader("varOnlineURL").ToString

                If reader("intReportDisabled") = 0 Then
                    Me.navOnlineReports.InnerHtml += "<li class='styleformytreeview_company'><a href='" & strUrl & "'>" & reader("varReportName") & "</a></li>"
                Else
                    Me.navOnlineReports.InnerHtml += "<li class='styleformytreeview_company'><a style='color:silver;'>" & reader("varReportName") & "</a></li>"
                End If

            End While

        End Using

        myConnection.Close()

    End Sub

    Private Sub LoadTreeview_CRC()

        Me.lnkReportsHome.Text = "Back to Carbon Reports"
        Me.lnkReportsHome.NavigateUrl = "~/reports/CarbonReports/default.aspx"

        Me.navOnlineReports.InnerHtml = ""
        Dim strSQL As String = "EXEC spPortal_TREEVIEW_CRC " & MySession.UserID
        Dim myConnection As New OleDb.OleDbConnection(Core.CurrentConnectionString)

        Dim myCommand As New OleDb.OleDbCommand(strSQL, myConnection)
        myCommand.Connection.Open()
        myCommand.CommandTimeout = 180
        Dim Reader As OleDb.OleDbDataReader = myCommand.ExecuteReader(CommandBehavior.CloseConnection)

        While Reader.Read


            Me.navOnlineReports.InnerHtml += "<li class='styleformytreeview_company'><a href='default.aspx?crccustid=" & Reader(0).ToString & "'>" & Reader(1).ToString & "</a></li>"

        End While
    End Sub








    Private Sub LoadReportsDataAnalysis()

        Me.lnkReportsHome.Text = "Back to Reports Home"
        Me.lnkReportsHome.NavigateUrl = "~/my-reports.aspx"

        Dim myConnection As New OleDb.OleDbConnection(Core.CurrentConnectionString)


        Dim strSQL As String = "EXEC spReports_ListReports " & MySession.UserID & ", 1"


        Using connection As New SqlConnection(Core.ConnectionString)
            connection.Open()
            Dim command As New SqlCommand(strSQL, connection)
            command.CommandType = CommandType.Text
            Dim reader As SqlDataReader = command.ExecuteReader()

            While reader.Read

                Dim strUrl As String = reader("varOnlineURL").ToString

                If reader("intReportDisabled") = 0 Then
                    Me.navOnlineReports.InnerHtml += "<li class='styleformytreeview_company'><a href='" & strUrl & "'>" & reader("varReportName") & "</a></li>"
                Else
                    Me.navOnlineReports.InnerHtml += "<li class='styleformytreeview_company'><a style='color:silver;'>" & reader("varReportName") & "</a></li>"
                End If

            End While

        End Using

        myConnection.Close()

        'Me.navOnlineReports.InnerHtml += "<li class='styleformytreeview_company' style='height:10px;'></li>"
        'Me.navOnlineReports.InnerHtml += "<li class='styleformytreeview_company'><a href='../../my-reports.aspx'><< Back</a></li>"

    End Sub

    Private Sub LoadReports_UtilityAnalysis()

        Me.lnkReportsHome.Text = "Back to Reports Home"
        Me.lnkReportsHome.NavigateUrl = "~/my-reports.aspx"

        Dim myConnection As New OleDb.OleDbConnection(Core.CurrentConnectionString)


        Dim strSQL As String = "EXEC spReports_ListReports " & MySession.UserID & ", 12"


        Using connection As New SqlConnection(Core.ConnectionString)
            connection.Open()
            Dim command As New SqlCommand(strSQL, connection)
            command.CommandType = CommandType.Text
            Dim reader As SqlDataReader = command.ExecuteReader()

            While reader.Read

                Dim strUrl As String = reader("varOnlineURL").ToString

                If reader("intReportDisabled") = 0 Then
                    Me.navOnlineReports.InnerHtml += "<li class='styleformytreeview_company'><a href='" & strUrl & "'>" & reader("varReportName") & "</a></li>"
                Else
                    Me.navOnlineReports.InnerHtml += "<li class='styleformytreeview_company'><a style='color:silver;'>" & reader("varReportName") & "</a></li>"
                End If

            End While

        End Using

        myConnection.Close()

        ' Me.navOnlineReports.InnerHtml += "<li class='styleformytreeview_company' style='height:10px;'></li>"
        ' Me.navOnlineReports.InnerHtml += "<li class='styleformytreeview_company'><a href='../../my-reports.aspx'><< Back</a></li>"

    End Sub

    Private Sub LoadReports_FinancialReports()
        Me.lnkReportsHome.Text = "Back to Reports Home"
        Me.lnkReportsHome.NavigateUrl = "~/my-reports.aspx"

        Dim myConnection As New OleDb.OleDbConnection(Core.CurrentConnectionString)


        Dim strSQL As String = "EXEC spReports_ListReports " & MySession.UserID & ", 11"


        Using connection As New SqlConnection(Core.ConnectionString)
            connection.Open()
            Dim command As New SqlCommand(strSQL, connection)
            command.CommandType = CommandType.Text
            Dim reader As SqlDataReader = command.ExecuteReader()

            While reader.Read

                Dim strUrl As String = reader("varOnlineURL").ToString

                If reader("intReportDisabled") = 0 Then
                    Me.navOnlineReports.InnerHtml += "<li class='styleformytreeview_company'><a href='" & strUrl & "'>" & reader("varReportName") & "</a></li>"
                Else
                    Me.navOnlineReports.InnerHtml += "<li class='styleformytreeview_company'><a style='color:silver;'>" & reader("varReportName") & "</a></li>"
                End If

            End While

        End Using

        myConnection.Close()

        'Me.navOnlineReports.InnerHtml += "<li class='styleformytreeview_company' style='height:10px;'></li>"
        'Me.navOnlineReports.InnerHtml += "<li class='styleformytreeview_company'><a href='../../my-reports.aspx'><< Back</a></li>"

    End Sub



    Private Sub LoadReports_MarketInformation()

        Me.navOnlineReports.InnerHtml = ""

        Me.lnkReportsHome.Text = "Back to Reports Home"
        Me.lnkReportsHome.NavigateUrl = "~/my-reports.aspx"

        Dim myConnection As New OleDb.OleDbConnection(Core.CurrentConnectionString)


        Dim strSQL As String = "EXEC spReports_ListReports " & MySession.UserID & ", 9"


        Using connection As New SqlConnection(Core.ConnectionString)
            connection.Open()
            Dim command As New SqlCommand(strSQL, connection)
            command.CommandType = CommandType.Text
            Dim reader As SqlDataReader = command.ExecuteReader()

            While reader.Read

                Dim strUrl As String = reader("varOnlineURL").ToString

                If reader("intReportDisabled") = 0 Then
                    Me.navOnlineReports.InnerHtml += "<li class='styleformytreeview_company'><a href='" & strUrl & "'>" & reader("varReportName") & "</a></li>"
                Else
                    Me.navOnlineReports.InnerHtml += "<li class='styleformytreeview_company'><a style='color:silver;'>" & reader("varReportName") & "</a></li>"
                End If

            End While

        End Using

        myConnection.Close()

        'Me.navOnlineReports.InnerHtml += "<li class='styleformytreeview_company' style='height:10px;'></li>"
        'Me.navOnlineReports.InnerHtml += "<li class='styleformytreeview_company'><a href='../../../my-reports.aspx'><< Back</a></li>"

    End Sub


    Private Sub LoadReports_BespokeReports()

        Me.navOnlineReports.InnerHtml = ""

        Me.lnkReportsHome.Text = "Back to Reports Home"
        Me.lnkReportsHome.NavigateUrl = "~/my-reports.aspx"

        Dim myConnection As New OleDb.OleDbConnection(Core.CurrentConnectionString)


        Dim strSQL As String = "EXEC spReports_ListReports " & MySession.UserID & ", 15"


        Using connection As New SqlConnection(Core.ConnectionString)
            connection.Open()
            Dim command As New SqlCommand(strSQL, connection)
            command.CommandType = CommandType.Text
            Dim reader As SqlDataReader = command.ExecuteReader()

            While reader.Read

                Dim strUrl As String = reader("varOnlineURL").ToString

                If reader("intReportDisabled") = 0 Then
                    Me.navOnlineReports.InnerHtml += "<li class='styleformytreeview_company'><a href='" & strUrl & "'>" & reader("varReportName") & "</a></li>"
                Else
                    Me.navOnlineReports.InnerHtml += "<li class='styleformytreeview_company'><a style='color:silver;'>" & reader("varReportName") & "</a></li>"
                End If

            End While

        End Using

        myConnection.Close()

        'Me.navOnlineReports.InnerHtml += "<li class='styleformytreeview_company' style='height:10px;'></li>"
        'Me.navOnlineReports.InnerHtml += "<li class='styleformytreeview_company'><a href='../../../my-reports.aspx'><< Back</a></li>"

    End Sub


    Private Sub LoadTreeview_Report()
        Me.lnkReportsHome.Text = ""
        Me.lnkReportsHome.NavigateUrl = ""

        Me.navOnlineReports.InnerHtml = ""
        Dim strSQL As String = "EXEC spPortal_TREEVIEW " & MySession.UserID
        Dim myConnection As New OleDb.OleDbConnection(Core.CurrentConnectionString)

        Dim myCommand As New OleDb.OleDbCommand(strSQL, myConnection)
        myCommand.Connection.Open()
        myCommand.CommandTimeout = 180
        Dim Reader As OleDb.OleDbDataReader = myCommand.ExecuteReader(CommandBehavior.CloseConnection)

        While Reader.Read

            Select Case CStr(Reader(3))
                Case CStr("Company")
                    Me.navOnlineReports.InnerHtml += "<li class='styleformytreeview_company'><a href='default.aspx?custid=" & Reader(0).ToString & "&method=Customer'>" & Reader(1).ToString & "</a></li>"
                Case CStr("Header")
                    Me.navOnlineReports.InnerHtml += "<li class='styleformytreeview_header'>" & Reader(1).ToString & "</li>"
                Case CStr("Site")
                    Me.navOnlineReports.InnerHtml += "<li class='styleformytreeview_site'><a href='default.aspx?custid=" & Reader(0).ToString & "&method=Site'>" & Left(Reader(1).ToString, 23) & "..</a></li>"
                Case CStr("Group")
                    Me.navOnlineReports.InnerHtml += "<li class='styleformytreeview_group'><a href='default.aspx?custid=" & Reader(0).ToString & "&method=Group'>" & Reader(1).ToString & "</a></li>"
            End Select


        End While
        Me.navOnlineReports.InnerHtml += "<li class='styleformytreeview_company' style='height:10px;'></li>"
        Me.navOnlineReports.InnerHtml += "<li class='styleformytreeview_company'><a href='../default.aspx'><< Back</a></li>"
    End Sub

    Private Sub LoadMenu()

        'Me.litNewFormatMenu.Text = ""
        'Me.litNewFormatMenu.Text &= ""

        'Me.litNewFormatMenu.Text &= "<table border='0' cellpadding='0' cellspacing='0' style='height:100%; '>"
        'Me.litNewFormatMenu.Text &= "    <tr>"
        'Me.litNewFormatMenu.Text &= "        <td valign='top>"
        'Me.litNewFormatMenu.Text &= "            <table id='mastertable' border='0' cellspacing='1' cellpadding='0' style='height:100%; width:210px;' bgcolor='white'>"
        'Me.litNewFormatMenu.Text &= "                <tr>"
        'Me.litNewFormatMenu.Text &= "                    <td valign='top'>"
        'Me.litNewFormatMenu.Text &= "		                <div id='masterdiv'>"
        'Me.litNewFormatMenu.Text &= "		                    <div class='Menu' onclick='SwitchMenu(1)'>"
        'Me.litNewFormatMenu.Text &= "			                    <img id='A1' style='vertical-align: middle' width='20' height='20' src='assets/icons/open.gif' border='0' hspace='3' />"
        'Me.litNewFormatMenu.Text &= "			                    <span class='tag'>1</span> &nbsp;&nbsp; Heading 1 "
        'Me.litNewFormatMenu.Text &= "		                    </div>"
        'Me.litNewFormatMenu.Text &= "	                        <div class='Options' id='sub1'>"
        'Me.litNewFormatMenu.Text &= "	                            <div class='Option' onMouseOver='this.style.background=sub_menu_back_color_over' onMouseOut='this.style.background=sub_menu_back_color_out'>"
        'Me.litNewFormatMenu.Text &= "				                    <a href='http://www.google.com'>"
        'Me.litNewFormatMenu.Text &= "					                    <img style='vertical-align: middle' width='16' height='16' src='assets/icons/txtfolder.gif' border='0' hspace='3' />"
        'Me.litNewFormatMenu.Text &= "					                    <span class='tag'>1.1</span> &nbsp;&nbsp; Sub 1.1"
        'Me.litNewFormatMenu.Text &= "				                    </a>"
        'Me.litNewFormatMenu.Text &= "             		            </div>"
        'Me.litNewFormatMenu.Text &= "                            </div>"
        'Me.litNewFormatMenu.Text &= "                            <div class='Menu' onclick='SwitchMenu(2)'>"
        'Me.litNewFormatMenu.Text &= "                           <img id='A2' style='vertical-align: middle' width='20' height='20' src='assets/icons/open.gif' border='0' hspace='3' />"
        'Me.litNewFormatMenu.Text &= "                           <span class='tag'>2</span> &nbsp;&nbsp; Heading 2"
        'Me.litNewFormatMenu.Text &= "                       </div>"
        'Me.litNewFormatMenu.Text &= "                        <div class='Options' id='sub2'>"
        'Me.litNewFormatMenu.Text &= "                                    <div class='Option' onmouseover='this.style.background=sub_menu_back_color_over' onmouseout='this.style.background=sub_menu_back_color_out'>"
        'Me.litNewFormatMenu.Text &= "                                        <a href='http://www.google.com'>"
        'Me.litNewFormatMenu.Text &= "                                            <img style='vertical-align: middle' width='16' height='16' src='assets/icons/txtfolder.gif' border='0' hspace='3' />"
        'Me.litNewFormatMenu.Text &= "                                            <span class='tag'>2.1</span> &nbsp;&nbsp; Sub 2.1 "
        'Me.litNewFormatMenu.Text &= "                                        </a>"
        'Me.litNewFormatMenu.Text &= "                                    </div>"
        'Me.litNewFormatMenu.Text &= "                                </div>"
        'Me.litNewFormatMenu.Text &= "                                <!-- End of Menu -->"
        'Me.litNewFormatMenu.Text &= "                            </div>"
        'Me.litNewFormatMenu.Text &= "                        </td>"
        'Me.litNewFormatMenu.Text &= "                    </tr>"
        'Me.litNewFormatMenu.Text &= "                </table>"
        'Me.litNewFormatMenu.Text &= "            </td>"
        'Me.litNewFormatMenu.Text &= "        </tr>"
        'Me.litNewFormatMenu.Text &= "    </table>"


    End Sub





End Class
