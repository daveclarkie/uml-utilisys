<%@ Page Language="VB" maintainscrollpositiononpostback="true" MasterPageFile="~/landing.master" AutoEventWireup="false" CodeFile="maintenance.aspx.vb" Inherits="maintenance" title="Administration Area | Edit User"%>

<asp:content contentplaceholderid="content_right" runat="server">
    <div class="content_back my_admin">
        <div id="admin" class="admin_area" runat="server">
            <form id="frmUser" runat="server"> 
                <div id="pnlStatus" class="status_message" runat="server" visible="false">
                    <div class="status"><asp:label id="lblStatus" runat="server" /></div>
                </div>

                <!-- EDIT MENU -->
                <ul class='menu'>
                    <li><asp:hyperlink runat="server" id="lnkReports" navigateurl='maintenance.aspx?tab=reports' cssclass='current'>Reports</asp:hyperlink></li>
                </ul>

                <!-- REPORTS -->
                <asp:Panel ID="pnlReports" runat="server">
                    <fieldset id="user_reports">
                        <legend>Active Reports</legend>
                        <asp:GridView cssclass="thin_table" autogeneratecolumns="false" borderwidth="0" ID="grdUsersReports" runat="server">
                            <Columns>
                                <asp:buttonfield commandname="delete" text="delete" />
                                <asp:BoundField DataField="intReportPK" HeaderText="ID" />
                                <asp:BoundField DataField="varReportName" HeaderText="Report Name" />
                                <asp:BoundField DataField="intReportTypeFK" HeaderText="Report Type" />
                            </Columns>
                        </asp:GridView>
                    </fieldset>
                    
                    <!-- INACTIVE REPORTS -->
                    <fieldset id="inactive_reports">
                        <legend>Inactive Reports</legend>
                        <asp:GridView cssclass="thin_table" autogeneratecolumns="false" borderwidth="0" ID="grdInactiveReports" runat="server">
                            <Columns>
                                <asp:buttonfield commandname="add" text="add" />
                                <asp:BoundField DataField="intReportPK" HeaderText="ID" />
                                <asp:BoundField DataField="varReportName" HeaderText="Report Name" />
                                <asp:BoundField DataField="intReportTypeFK" HeaderText="Report Type" />
                            </Columns>
                        </asp:GridView>
                    </fieldset>
                    
                    <!-- NEW REPORTS -->
                    <fieldset id="new_reports">
                        <legend>New Reports</legend>
                        <asp:Table runat="server">
                            <asp:TableRow>
                                <asp:TableCell ColumnSpan="2"><asp:Label ID="lblNewReportError" runat="server" ForeColor="Red"></asp:Label></asp:TableCell>
                            </asp:TableRow>
                            
                            <asp:TableRow>
                                <asp:TableCell>Report Type</asp:TableCell>
                                <asp:TableCell>
                                    <asp:DropDownList ID="ddlNewReportType" runat="server" Width="354px" AutoPostBack="true">
                                        <asp:ListItem Value="2" Text="Offline"></asp:ListItem>
                                        <asp:ListItem Value="1" Text="Online"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Utilisys Online"></asp:ListItem>
                                    </asp:DropDownList>
                                </asp:TableCell>
                            </asp:TableRow>
                            
                            <asp:TableRow>
                                <asp:TableCell>Name</asp:TableCell>
                                <asp:TableCell><asp:TextBox ID="txtNewReportName" runat="server"></asp:TextBox></asp:TableCell>
                            </asp:TableRow>
                            
                            <asp:TableRow>
                                <asp:TableCell>Online URL</asp:TableCell>
                                <asp:TableCell><asp:TextBox ID="txtNewOnlineURL" runat="server"></asp:TextBox></asp:TableCell>
                            </asp:TableRow>
                            
                            <asp:TableRow ID="trOfflineURL" runat="server">
                                <asp:TableCell>Offline URL</asp:TableCell>
                                <asp:TableCell><asp:TextBox ID="txtNewOfflineURL" runat="server"></asp:TextBox></asp:TableCell>
                            </asp:TableRow>
                            
                            <asp:TableRow>
                                <asp:TableCell></asp:TableCell>
                                <asp:TableCell><asp:LinkButton ID="btnNewAddReport" runat="server" CssClass="btnAddReport" /></asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </fieldset>

                    <!-- AVAILABLE REPORTS -->
                    <asp:GridView borderwidth="0px" ID="grdReports" cssclass="left" runat="server" headerstyle-cssclass="header" AutoGenerateColumns="False">
                        <Columns>
                            <asp:buttonfield commandname="add" text="add" />
                            <asp:BoundField DataField="intReportPK" HeaderText="ID" />
                            <asp:BoundField DataField="varReportName" HeaderText="Report Name" />
                            <asp:BoundField DataField="intReportTypeFK" HeaderText="Report Type" />
                        </Columns>
                        <AlternatingRowStyle CssClass="tralt" />
                        <headerstyle cssclass="header" />
                    </asp:GridView>
                    
                </asp:Panel>

            </form>
        </div>
    </div>
</asp:content>
