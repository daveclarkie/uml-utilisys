﻿//Scarlet Partnership custom Javascript functions.

function externalLinks()
 {
    if (!document.getElementsByTagName) return;
    var anchors = document.getElementsByTagName("a");
    for (var i=0; i<anchors.length; i++) {
    var anchor = anchors[i];
    if (anchor.getAttribute("href") &&
       anchor.getAttribute("rel") == "external")
     anchor.target = "_blank";
    }
}

// This is to view company and stite name with long ext ect "Anglo beef pro... 
function showmj(what, clearme)
 {
   if (clearme == "Y")
     {  document.getElementById("HJ").innerHTML = "";  }
     else 
     { document.getElementById("HJ").innerHTML = what; }
}