<%@ Page Language="VB" maintainscrollpositiononpostback="true" MasterPageFile="~/landing.master" AutoEventWireup="false" CodeFile="user.aspx.vb" Inherits="user" title="Administration Area | Edit User"%>

<asp:content contentplaceholderid="content_right" runat="server">
    <div class="content_back my_admin">
        <div id="admin" class="admin_area" runat="server">
            <form id="frmUser" runat="server"> 
                <div id="pnlStatus" class="status_message" runat="server" visible="false">
                    <div class="status"><asp:label id="lblStatus" runat="server" Visible="false" /></div>
                </div>

                <!-- EDIT MENU -->
                <ul class='menu'>
                    <li><asp:hyperlink runat="server" id="lnkMaster" cssclass='current' navigateurl='user.aspx?tab=master'>Details</asp:hyperlink></li>
                    <li><asp:hyperlink runat="server" id="lnkCompanies" navigateurl='user.aspx?tab=sites'>Companies</asp:hyperlink></li>
                    <li><asp:hyperlink runat="server" id="lnkGroups" navigateurl='user.aspx?tab=sites'>Groups</asp:hyperlink></li>
                    <li><asp:hyperlink runat="server" id="lnkSites" navigateurl='user.aspx?tab=sites'>Sites</asp:hyperlink></li>
                    <li><asp:hyperlink runat="server" id="lnkReports" navigateurl='user.aspx?tab=reports'>Reports</asp:hyperlink></li>
                    <li><asp:hyperlink runat="server" id="lnkPacks" navigateurl='user.aspx?tab=packs'>Packs</asp:hyperlink></li>
                </ul>

                <!-- USER EDIT PANEL -->
                <asp:Panel ID="pnlMaster" runat="server">
                <div class="buttons">
                    <asp:button id="btnNew" runat="server" cssclass="button" text="New User"></asp:button>
                    <asp:button id="btnSave" runat="server" cssclass="button" text="Save" PostBackUrl="user.aspx?action=save"></asp:button>
                    <asp:button id="btnSaveClose" runat="server" cssclass="button" text="Save & Close" PostBackUrl="user.aspx?action=save"></asp:button>
                    <asp:button id="btnCancel" runat="server" cssclass="button" text="Close" enabled="false"></asp:button>
                    <asp:button id="btnDisable" runat="server" cssclass="button" text="Disable"></asp:button>
                    <div class="hr"><hr /></div>
                </div>
                    <fieldset runat="server" id="user_basics">
                        <legend>User Details</legend>
                        <p id="UserID" runat="server">
                            <label for="txtID">User ID:</label>
                            <asp:TextBox id="txtID" runat="server" Text="0" enabled="false"></asp:TextBox>
                        </p>
                        <p>
                            <label for="txtForename">Forename:</label>
                            <asp:TextBox id="txtForename" runat="server" MaxLength="100"></asp:TextBox>
                        </p>
                        <p>
                            <label for="txtSurname">Surname:</label>
                            <asp:TextBox id="txtSurname" runat="server" MaxLength="100"></asp:TextBox>
                        </p>
                        <p>
                            <label for="txtEmail">Email:</label>
                            <asp:TextBox id="txtEmail" runat="server" MaxLength="255"></asp:TextBox>
                        </p>
                        <p>
                            <label for="txtPassword">Password:</label>
                            <asp:TextBox id="txtPassword" runat="server" MaxLength="50"></asp:TextBox>
                            <asp:Button ID="btnGenPwd" runat="server" Width="30px" Text="X" />
                        </p>
                        <p class="textarea">
                            <label for="txtNotes">Notes:</label>
                            <asp:TextBox id="txtNotes" runat="server" MaxLength="500" TextMode="MultiLine"></asp:TextBox>
                        </p>
                    </fieldset>
                    <fieldset runat="server" id="User_details">
                        <legend>Access Details</legend>
                        <p>
                            <label for="ddlPageTypes">Homepage URL:</label>
                            <asp:DropDownList id="ddlPageTypes" runat="server"></asp:DropDownList>
                        </p>

                        <p>
                            <label for="chkAllowMySites">Allow My Sites?</label>
                            <asp:CheckBox id="chkAllowMySites" runat="server"></asp:CheckBox>
                        </p>
                        <p>
                            <label for="chkAllowMyReports">Allow My Reports?</label>
                            <asp:CheckBox id="chkAllowMyReports" runat="server"></asp:CheckBox>
                        </p>
                        <p>
                            <label for="chkAllowMyAccount">Allow My Account?</label>
                            <asp:CheckBox id="chkAllowMyAccount" runat="server"></asp:CheckBox>
                        </p>
                        <p>
                            <label for="chkAdmin">Admin?</label>
                            <asp:CheckBox id="chkAdmin" runat="server"></asp:CheckBox>
                        </p>
                    </fieldset>
                </asp:Panel>

                <!-- USERS COMPANIES -->
                <asp:Panel ID="pnlCompanies" runat="server">
                    <fieldset id="users_companies">
                        <legend>Users Companies</legend>
                        <asp:GridView autogeneratecolumns="false" borderwidth="0" ID="grdUsersCompanies" runat="server">
                            <Columns>
                                <asp:buttonfield commandname="delete" text="delete" />
                                <asp:BoundField DataField="ID" HeaderText="ID" />
                                <asp:BoundField DataField="User ID" HeaderText="User ID" />
                                <asp:BoundField DataField="Company Name" HeaderText="Company Name" />
                            </Columns>
                        </asp:GridView>
                    </fieldset>

                    <div class="hr"><hr /></div>

                    <!-- COMPANIES TO PICK FROM -->
                    <asp:GridView borderwidth="0px" ID="grdCompanies" cssclass="left" runat="server" headerstyle-cssclass="header" AutoGenerateColumns="False">
                        <Columns>
                            <asp:buttonfield commandname="add" text="add" />
                            <asp:BoundField DataField="ID" HeaderText="ID" />
                            <asp:BoundField DataField="Company Name" HeaderText="Company Name" />
                        </Columns>
                        <AlternatingRowStyle CssClass="tralt" />
                        <headerstyle cssclass="header" />
                    </asp:GridView>
                </asp:Panel>

                <!-- USERS GROUPS -->
                <asp:Panel ID="pnlGroups" runat="server">
                    <fieldset id="users_groups">
                        <legend>Users Groups</legend>
                        <asp:GridView autogeneratecolumns="false" borderwidth="0" ID="grdUsersGroups" runat="server">
                            <Columns>
                                <asp:buttonfield commandname="delete" text="delete" />
                                <asp:BoundField DataField="ID" HeaderText="ID" />
                                <asp:BoundField DataField="User ID" HeaderText="User ID" />
                                <asp:BoundField DataField="Group Name" HeaderText="Group Name" />
                                <asp:BoundField DataField="Company Name" HeaderText="Company Name" />
                            </Columns>
                        </asp:GridView>
                    </fieldset>

                    <div class="hr"><hr /></div>

                    <!-- GROUPS TO PICK FROM -->
                    <asp:GridView borderwidth="0px" ID="grdGroups" cssclass="left" runat="server" headerstyle-cssclass="header" AutoGenerateColumns="False">
                        <Columns>
                            <asp:buttonfield commandname="add" text="add" />
                            <asp:BoundField DataField="Group ID" HeaderText="ID" />
                            <asp:BoundField DataField="Group Name" HeaderText="Group Name" />
                        </Columns>
                        <AlternatingRowStyle CssClass="tralt" />
                        <headerstyle cssclass="header" />
                    </asp:GridView>
                </asp:Panel>

                <!-- USERS SITES -->
                <asp:Panel ID="pnlSites" runat="server">
                    <fieldset id="user_sites">
                        <legend>User Sites</legend>
                        <asp:GridView cssclass="thin_table" autogeneratecolumns="false" borderwidth="0" ID="grdUsersSites" runat="server">
                            <Columns>
                                <asp:buttonfield commandname="delete" text="delete" />
                                <asp:BoundField DataField="ID" HeaderText="ID" />
                                <asp:BoundField DataField="Site Name" HeaderText="Site Name" />
                                <asp:BoundField DataField="Company Name" HeaderText="Company Name" />
                            </Columns>
                        </asp:GridView>
                    </fieldset>

                    <div class="site_buttons">
                        <div class="left" style="width:240px; margin-top:5px;">
                            <asp:textbox id="txtSearch" cssclass="search" runat="server"></asp:textbox>
                            <asp:button id="btnSearch" runat="server" cssclass="button" text="Search"></asp:button>
                        </div>
                        <asp:panel runat="server" id="pnlFilters">
                            <div id="paging" class="right"><span class="left">Show&nbsp;</span>
                                <asp:dropdownlist runat="server" cssclass="left" id="ddlSitesPerPage" autopostback="true" tooltip="Select number of users to display per page" width="50px">
                                    <asp:listitem value="10" text="10" />
                                    <asp:listitem value="20" text="20" selected="True" />
                                    <asp:listitem value="30" text="30" />
                                    <asp:listitem value="40" text="40" />
                                    <asp:listitem value="50" text="50" />
                                    <asp:listitem value="60" text="60" />
                                    <asp:listitem value="70" text="70" />
                                    <asp:listitem value="80" text="80" />
                                    <asp:listitem value="90" text="90" />
                                    <asp:listitem value="100" text="100" />
                                </asp:dropdownlist>
                                <span class="left">&nbsp;sites per page&nbsp;</span>
                            </div>
                            <ul class="pages" id="pages" runat="server"></ul>
                        </asp:panel>
                        <div class="hr"><hr /></div>
                    </div>

                    <!-- AVAILABLE SITES -->
                    <asp:GridView borderwidth="0px" ID="grdSites" cssclass="left" runat="server" headerstyle-cssclass="header" AutoGenerateColumns="False">
                        <Columns>
                            <asp:buttonfield commandname="add" text="add" />
                            <asp:BoundField DataField="ID" HeaderText="ID" />
                            <asp:BoundField DataField="Site Name" HeaderText="Site Name" />
                            <asp:BoundField DataField="Company Name" HeaderText="Company Name" />
                        </Columns>
                        <AlternatingRowStyle CssClass="tralt" />
                        <headerstyle cssclass="header" />
                    </asp:GridView>
                    
                    <asp:panel runat="server" id="pnlFilters2">
                        <div id="paging2" class="right">
                            <ul class="pages" id="pages2" runat="server"></ul>
                        </div>
                    </asp:panel>
                </asp:Panel>
                
                
                <!-- USERS SYSTEMS -->
                <asp:Panel ID="pnlSystems" runat="server">
                    <fieldset id="user_systems">
                        <legend>User Systems</legend>
                        <asp:GridView cssclass="left"  autogeneratecolumns="false" borderwidth="0" ID="grdUsersSystems" runat="server">
                            <Columns>
                                <asp:buttonfield commandname="delete" text="delete" />
                                <asp:BoundField DataField="ID" HeaderText="ID" />
                                <asp:BoundField DataField="System Name" HeaderText="System Name" />
                                <asp:BoundField DataField="Customer Name" HeaderText="Customer Name" />
                                <asp:BoundField DataField="Username" HeaderText="Username" />
                                <asp:BoundField DataField="Password" HeaderText="Password" />
                            </Columns>
                        </asp:GridView>
                    </fieldset>

                    <!-- AVAILABLE SYSTEMS -->
                    <asp:GridView borderwidth="0px" ID="grdSystems" cssclass="left" runat="server" headerstyle-cssclass="header" AutoGenerateColumns="False">
                        <Columns>
                            <asp:buttonfield commandname="add" text="add" />
                                <asp:BoundField DataField="ID" HeaderText="ID" />
                                <asp:BoundField DataField="System Name" HeaderText="System Name" />
                                <asp:BoundField DataField="Customer Name" HeaderText="Customer Name" />
                                <asp:BoundField DataField="Username" HeaderText="Username" />
                                <asp:BoundField DataField="Password" HeaderText="Password" />
                        </Columns>
                        <AlternatingRowStyle CssClass="tralt" />
                        <headerstyle cssclass="header" />
                    </asp:GridView>
                    
                </asp:Panel>
               

                <!-- USERS REPORTS -->
                <asp:Panel ID="pnlReports" runat="server">
                
                        <asp:GridView borderwidth="0px" ID="grdUsersReports" cssclass="left" runat="server" headerstyle-cssclass="header" AutoGenerateColumns="False">
                            <Columns>
                               <asp:buttonfield commandname="delete" text="delete" />
                                <asp:BoundField DataField="intReportPK" HeaderText="ID" />
                                <asp:BoundField DataField="varReportName" HeaderText="Report Name" />
                                <asp:BoundField DataField="varReportTypeNiceName" HeaderText="varReportTypeNiceName" />
                                <asp:BoundField DataField="varStatusName" HeaderText="Status" />
                            </Columns>
                            <AlternatingRowStyle CssClass="tralt" />
                            <headerstyle cssclass="header" />
                        </asp:GridView>

                    <!-- AVAILABLE REPORTS -->
                        <asp:GridView borderwidth="0px" ID="grdReports" cssclass="left" runat="server" headerstyle-cssclass="header" AutoGenerateColumns="False">
                            <Columns>
                                <asp:buttonfield commandname="add" text="add" />
                                <asp:BoundField DataField="intReportPK" HeaderText="ID" />
                                <asp:BoundField DataField="varReportName" HeaderText="Report Name" />
                                <asp:BoundField DataField="varReportTypeNiceName" HeaderText="varReportTypeNiceName" />
                                <asp:BoundField DataField="varStatusName" HeaderText="Status" />
                            </Columns>
                            <AlternatingRowStyle CssClass="tralt" />
                            <headerstyle cssclass="header" />
                        </asp:GridView>
                    
                </asp:Panel>
                
                <!-- USERS PACKS -->
              <asp:Panel ID="pnlPacks" runat="server">
              
                 <ul class="box_links_reports">
                    <li><a id="lnkReports1" class="packs_Procurement" runat="server" title="Procurement"></a></li>
                    <li><a id="lnkReports2" class="packs_EnergyMangement" runat="server" title="Energy Mangement"></a></li>
                </ul>
            
                <ul class="box_links_reports">
                    <li><a id="lnkReports3" class="packs_CCA" runat="server" title="CCA"></a></li>
                    <li><a id="lnkReports4" class="packs_CRC" runat="server" title="CRC"></a></li>
                </ul>
              
              </asp:Panel>

            </form>
        </div>
    </div>
</asp:content>
