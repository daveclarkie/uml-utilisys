<%@ Page Language="VB" MasterPageFile="~/landing.master" AutoEventWireup="false" CodeFile="my-account.aspx.vb" Inherits="my_account" title="My Account" %>

<asp:content contentplaceholderid="content_right" runat="server">
    <div class="content_back my_account">
        <form id="frmAccount" runat="server">
                    
            <fieldset id="account_summary" title="Account summary">
                <h2>Company Details</h2>
                <div>
                    <label for="txtCompany">Company:</label>
                    <asp:textbox runat="server" id="txtCompany" readonly="true"></asp:textbox>
                    <label for="txtAddress1">HQ Address:</label>
                    <asp:textbox runat="server" id="txtAddress" readonly="true"></asp:textbox>
                    <label for="txtAddress2">&nbsp;</label>
                    <asp:textbox runat="server" id="txtAddress2" readonly="true"></asp:textbox>
                    <label for="txtAddress3">&nbsp;</label>
                    <asp:textbox runat="server" id="txtAddress3" readonly="true"></asp:textbox>
                    <label for="txtAddress4">&nbsp;</label>
                    <asp:textbox runat="server" id="txtAddress4" readonly="true"></asp:textbox>
                    <label for="txtCounty">&nbsp;</label>
                    <asp:textbox runat="server" id="txtCounty" readonly="true"></asp:textbox>
                    <label for="txtPostcode">&nbsp;</label>
                    <asp:textbox runat="server" id="txtPostcode" readonly="true"></asp:textbox>
                    <label for="txtCompanyNumber">Company No.</label>
                    <asp:textbox runat="server" id="txtCompanyNumber" readonly="true"></asp:textbox>
                    <label for="txtVATReg">VAT No.</label>
                    <asp:textbox runat="server" id="txtVATReg" readonly="true"></asp:textbox>
                    <label for="txtAccountManager">Account Manager:</label>
                    <asp:textbox runat="server" id="txtAccountManager" readonly="true"></asp:textbox>
                    
                </div>
                <p></p>
            </fieldset>
            <fieldset id="account_contacts" title="Account contacts">
                <h2>Primary Contact Details</h2>
                <div>
                    <label for="txtPrimaryContact">1st Contact:</label>
                    <asp:textbox runat="server" id="txtPrimaryContact" readonly="true"></asp:textbox>
                    <label for="txtPrimaryContactNumber">1st Contact No:</label>
                    <asp:textbox runat="server" id="txtPrimaryContactNumber" readonly="true"></asp:textbox>
                    <label for="txtPrimaryContactEmail">1st Contact Email:</label>
                    <asp:textbox runat="server" id="txtPrimaryContactEmail" readonly="true"></asp:textbox>
                    <label for="txtSecondaryContact">2nd Contact:</label>
                    <asp:textbox runat="server" id="txtSecondaryContact" readonly="true"></asp:textbox>
                    <label for="txtSecondaryContactNumber">2nd Contact No:</label>
                    <asp:textbox runat="server" id="txtSecondaryContactNumber" readonly="true"></asp:textbox>
                    <label for="txtSecondaryContactEmail">2nd Contact Email:</label>
                    <asp:textbox runat="server" id="txtSecondaryContactEmail" readonly="true"></asp:textbox>
                </div>
                <p></p>
            </fieldset>
        </form>
    </div>
</asp:content>
