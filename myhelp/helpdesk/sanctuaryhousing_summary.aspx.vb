﻿Imports System.Net
Imports System.Xml
Imports System.IO
Imports System.Text

Partial Class sanctuaryhousing_summary
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            If Not IIf(Core.data_select_value("SELECT COUNT(*) FROM dbo.tblUsersRequests WHERE intRequestFK = 4 AND intUserFK = " & MySession.UserID) > 0, True, False) Then
                Response.Redirect("~/myaccount/default.aspx", True)
            End If

            If Not Page.IsPostBack = True Then

                Dim description As HtmlMeta = New HtmlMeta()
                description.Name = "description"
                description.Content = "Utilisys - Helpdesk"
                Dim keywords As HtmlMeta = New HtmlMeta()
                keywords.Name = "keywords"
                keywords.Content = "Helpdesk"
                Page.Header.Controls.Add(description)
                Page.Header.Controls.Add(keywords)

            End If



            Dim strSQL As String = ""
            strSQL &= "SELECT SR.*"
            strSQL &= ", (SELECT STATUSNAME FROM STATUSDEFINITION WHERE STATUSID=WOS.STATUSID) [status]"
            strSQL &= ", [se].[fn_GetDateTime](WO.createdtime) [created_time]"
            strSQL &= ", replace(T.TEMPLATENAME, ' - Sanctuary Housing Association', '') [type]"



            strSQL &= " FROM servicereq_3601 SR"
            strSQL &= " INNER JOIN workorder WO ON WO.workorderid=SR.workorderid"
            strSQL &= " INNER JOIN workorderstates WOS ON WOS.workorderid=SR.workorderid"
            strSQL &= " INNER JOIN servicecatalog_fields SCF ON SCF.workorderid=SR.workorderid"
            strSQL &= " INNER JOIN RequestTemplate_List T ON T.TEMPLATEID=WO.TEMPLATEID"
            strSQL &= " LEFT JOIN Workorder_queue as WOQ ON WOQ.workorderid= SR.workorderid"
            strSQL &= " ORDER BY SR.workorderid"





            Dim dt As New DataTable
            dt = Core.data_select(strSQL, 1)
            System.Diagnostics.Debug.Print(dt.Rows.Count)
            rptSummary.DataSource = dt
            rptSummary.DataBind()


        Catch ex As Exception
            '
        End Try

    End Sub

  

End Class
