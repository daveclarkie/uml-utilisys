﻿<%@ Page Title="UTILISYS - my help" Language="VB" MasterPageFile="~/landing.master" AutoEventWireup="false" CodeFile="sanctuaryhousing_summary.aspx.vb" Inherits="sanctuaryhousing_summary"  %>

<asp:Content ID="Content1" ContentPlaceHolderID="content_right" Runat="Server">

<form runat="server">

    <div class="content_back general">
            <table cellpadding="0" cellspacing="0">
               
                <tr>
                    
                    <td colspan="1">
                        <table cellpadding="0" cellspacing="0">

                            <tr>
                                <td style="height:3px;">
                                    
                                </td>
                            </tr>

                             <tr>
                                <td>
                                     <b>Sanctuary Housing Association</b>
                                </td>
                            </tr>
                                                        
                            <tr>
                                <td style="height:3px;">
                                    
                                </td>
                            </tr>

                             <tr>
                                <td>
                                     <b>Request Summary</b>
                                </td>
                            </tr>
                                                        
                            <tr>
                                <td style="height:3px;">
                                    
                                </td>
                            </tr>

                             <tr>
                                <td>
                                     <asp:Label runat="server" ID="formError" ForeColor="Red"></asp:Label>
                                </td>
                            </tr>
                                                        
                        </table>
                    </td>
                </tr> <!-- Meter Selection Table -->
                <tr>
                    <td>
                        <asp:Panel runat="server" ID="panelForm">
                            <table width="850px" cellpadding="5px" cellspacing="0" style="border:1px #ccc solid;" align="center">
                                       
                            <tr style="height:10px;">
		                        <td colspan="8"></td>
	                        </tr>
                                                    
	                        <tr>
                                <td style="width:5px;"></td>
		                        <td>
                                    <asp:Repeater ID="rptSummary" runat="server">
                                        <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td style="width:5px;">
                                
                                                </td>
                                                <td style="width:50px;">
                                                    <h4>ID</h4>
                                                </td>
                                                <td style="width:200px;">
                                                    <h4>Site</h4>
                                                </td>
                                                <td style="width:50px;">
                                                    <h4>Type</h4>
                                                </td>
                                                <td style="width:80px;">
                                                    <h4>Fuel</h4>
                                                </td>
                                                <td style="width:200px;">
                                                    <h4>Meter</h4>
                                                </td>
                                                <td style="width:200px;">
                                                    <h4>Status</h4>
                                                </td>
                                                <td></td>
                                            </tr>
                                        </HeaderTemplate> 
                                        <ItemTemplate>
                                                <%--<a href='agreements.aspx?pg=tg&pk=<%# Container.DataItem("agreement_fk") %>&xt=<%# Container.DataItem("agreementtarget_pk") %>'></a>--%>
                                            <tr>
                                                <td style="text-align:center;">
                                                    <%--<a href='agreements.aspx?pg=nt&pk=<%# sm.targetAgreement %>&spk=<%# Container.DataItem("note_pk") %>'><h6>edit</h6></a>--%>
                                                </td>
                                                <td>
                                                    <%# Container.DataItem(0)%>
                                                </td>
                                                <td>
                                                    <%# Container.DataItem(5)%>
                                                </td>
                                                <td>
                                                    <%# Container.DataItem("type")%>
                                                </td>
                                                <td>
                                                    <%# Container.DataItem(3)%>
                                                </td>
                                                <td>
                                                    <%# Container.DataItem(4)%>
                                                </td>
                                                <td>
                                                    <%# Container.DataItem("status")%>
                                                </td>
                                                <td>
                                                                                   
                                                </td>
                                            </tr>
                                        </ItemTemplate> 
                                        <FooterTemplate>
                                            <tr style="height:5px;">
                                            </tr>
                                        </table>
                                        </FooterTemplate> 
                                    </asp:Repeater>
                                </td>
		                        <td></td>
	                        </tr>                                
                                  

                        </table>
                        </asp:Panel>
                        
                    </td>                
                </tr>
                <tr>
                    <td>
                    </td>
                </tr>
            </table>
    </div>
   
</form>



</asp:Content>

