﻿<%@ Page Title="UTILISYS - my help" Language="VB" MasterPageFile="~/landing.master" AutoEventWireup="false" CodeFile="includesanctuaryhousing.aspx.vb" Inherits="includesanctuaryhousing"  %>

<asp:Content ID="Content1" ContentPlaceHolderID="content_right" Runat="Server">

<form runat="server">

    <div class="content_back general">
            <table cellpadding="0" cellspacing="0">
               
                <tr>
                    
                    <td colspan="1">
                        <table cellpadding="0" cellspacing="0">

                            <tr>
                                <td style="height:3px;">
                                    
                                </td>
                            </tr>

                             <tr>
                                <td>
                                     <b>Sanctuary Housing Association</b>
                                </td>
                            </tr>
                                                        
                            <tr>
                                <td style="height:3px;">
                                    
                                </td>
                            </tr>

                             <tr>
                                <td>
                                     <b>Site Addition</b>
                                </td>
                            </tr>
                                                        
                            <tr>
                                <td style="height:3px;">
                                    
                                </td>
                            </tr>

                             <tr>
                                <td>
                                     <asp:Label runat="server" ID="formError" ForeColor="Red"></asp:Label>
                                </td>
                            </tr>
                                                        
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Panel runat="server" ID="panelForm">
                            <table width="650px" cellpadding="5px" cellspacing="0" style="border:1px #ccc solid;" align="center">
                                       
                            <tr style="height:10px;">
		                        <td colspan="8"></td>
	                        </tr>
                                                    
	                        <tr>
                                <td style="width:5px;"></td>
		                        <td>Request Type</td>
		                        <td>:</td>
		                        <td>Include</td>

                                <td style="width:20px;"></td>
		                            
                                <td>Department <span style="color:Red;">*</span></td>
		                        <td>:</td>
		                        <td><asp:TextBox runat="server" id="Department" style="width:150px;" ></asp:TextBox></td>
	                        </tr>                                
                                                            
	                        <tr>
                                <td></td>
		                        <td>Date of Request <span style="color:Red;">*</span></td>
		                        <td>:</td>
		                        <td><asp:Label runat="server" id="DateofRequest" style="width:150px;"></asp:Label></td>

                                <td style="width:20px;"></td>
		                            
                                <td>Legal Entity <span style="color:Red;">*</span></td>
		                        <td>:</td>
		                        <td><asp:TextBox runat="server" id="LegalEntity" style="width:150px;" ></asp:TextBox></td>
	                        </tr>                                
                                                            
	                        <tr>
                                <td></td>
		                        <td>Fuel Type <span style="color:Red;">*</span></td>
		                        <td>:</td>
		                        <td><asp:DropDownList runat="server" id="FuelType" style="width:154px;" >
                                    <asp:ListItem Value="0" Text="-- select --" Selected="True"></asp:ListItem>
                                    <asp:ListItem Value="Gas" Text="Gas" ></asp:ListItem>
                                    <asp:ListItem Value="HH Elec" Text="HH Elec" ></asp:ListItem>
                                    <asp:ListItem Value="NHH Elec" Text="NHH Elec" ></asp:ListItem>
                                </asp:DropDownList></td>

                                <td style="width:20px;"></td>
		                            
                                <td>MPAN/MPR <span style="color:Red;">*</span></td>
		                        <td>:</td>
		                        <td><asp:TextBox runat="server" id="MPANMPR" style="width:150px;" ></asp:TextBox></td>
	                        </tr>
                                                            
	                        <tr>
                                <td></td>
		                        <td>Site Name <span style="color:Red;">*</span></td>
		                        <td>:</td>
		                        <td colspan="5"><asp:TextBox runat="server" id="SiteName" style="width:350px;" ></asp:TextBox></td>
	                        </tr>
                                                            
	                        <tr>
                                <td></td>
		                        <td>Address 1</td>
		                        <td>:</td>
		                        <td colspan="5"><asp:TextBox runat="server" id="Address1" style="width:350px;" ></asp:TextBox></td>
	                        </tr>
                                                            
	                        <tr>
                                <td></td>
		                        <td>Address 2</td>
		                        <td>:</td>
		                        <td colspan="5"><asp:TextBox runat="server" id="Address2" style="width:350px;" ></asp:TextBox></td>
	                        </tr>
                                                            
	                        <tr>
                                <td></td>
		                        <td>Address 3</td>
		                        <td>:</td>
		                        <td colspan="5"><asp:TextBox runat="server" id="Address3" style="width:350px;" ></asp:TextBox></td>
	                        </tr>
                                                            
	                        <tr>
                                <td></td>
		                        <td>County</td>
		                        <td>:</td>
		                        <td colspan="5"><asp:DropDownList runat="server" id="County" style="width:354px;" ></asp:DropDownList></td>
	                        </tr>
                                                            
	                        <tr>
                                <td></td>
		                        <td>Post Code</td>
		                        <td>:</td>
		                        <td colspan="5"><asp:TextBox runat="server" id="PostCode" style="width:350px;" ></asp:TextBox></td>
	                        </tr>
                            

                                                                
                                                            
	                        <tr>
                                <td></td>
		                        <td>Current Supplier <span style="color:Red;">*</span></td>
		                        <td>:</td>
		                        <td><asp:TextBox runat="server" id="CurrentSupplier" style="width:150px;" ></asp:TextBox></td>

                                <td style="width:20px;"></td>
		                            
                                <td>VAT Rate</td>
		                        <td>:</td>
		                        <td><asp:TextBox runat="server" id="VATRate" style="width:150px;" ></asp:TextBox></td>
	                        </tr>                                
                                                            
	                        <tr>
                                <td></td>
		                        <td>Current Contract Status <span style="color:Red;">*</span></td>
		                        <td>:</td>
		                        <td><asp:TextBox runat="server" id="CurrentContractStatus" style="width:150px;" ></asp:TextBox></td>

                                <td style="width:20px;"></td>
		                            
                                <td>Stark Ref No</td>
		                        <td>:</td>
		                        <td><asp:TextBox runat="server" id="StarkRefNo" style="width:150px;" ></asp:TextBox></td>
	                        </tr>                                
                                                            
	                        <tr>
                                <td></td>
		                        <td>Current Contract End Date</td>
		                        <td>:</td>
		                        <td><asp:TextBox runat="server" id="CurrentContractEndDate" style="width:150px;" ></asp:TextBox></td>

                                <td style="width:20px;"></td>
		                            
                                <td>Estimated kWh</td>
		                        <td>:</td>
		                        <td><asp:TextBox runat="server" id="EstimatedkWh" style="width:150px;" ></asp:TextBox></td>
	                        </tr>   
                              
                                                            
	                        <tr>
                                <td></td>
		                        <td>MOP Provider</td>
		                        <td>:</td>
		                        <td><asp:DropDownList runat="server" id="mopprovider" style="width:152px;" ></asp:DropDownList></td>

                                <td style="width:20px;"></td>
		                            
                                <td>DC Provider</td>
		                        <td>:</td>
		                        <td><asp:DropDownList runat="server" id="dcprovider" style="width:152px;" ></asp:DropDownList></td>
	                        </tr>   

	                        <tr style="height:10px;">
		                        <td colspan="8"></td>
	                        </tr>                             
                                                            
	                        <tr>
                                <td></td>
		                        <td colspan="7"><b>Additional Notes</b></td>
                            </tr>

                            <tr>
                                <td></td>
		                        <td colspan="7"><asp:TextBox runat="server" id="AdditionalNotes" TextMode="MultiLine" style="width:613px; height: 120px;"  ></asp:TextBox></td>
	                        </tr>   

	                        <tr style="height:10px;">
		                        <td colspan="8"></td>
	                        </tr>

                            <tr>
                                <td></td>
		                        <td colspan="7"><asp:Button ID="btnSubmitForm" runat="server" Text="Add Request to Helpdesk" Width="620px" /></td>
	                        </tr>   

	                        <tr style="height:10px;">
		                        <td colspan="8"></td>
	                        </tr>

                        </table>
                        </asp:Panel>
                        
                        <asp:Panel runat="server" ID="panelResponse" Visible="false">
                                <table width="650px" cellpadding="5px" cellspacing="0" style="border:1px #ccc solid;" align="center">
                                       
                                    <tr>
                                        <td></td>
		                                <td><asp:label runat="server" id="formResponse" ></asp:label></td>
	                                </tr>   
                                </table>
                        </asp:Panel>
                    </td>                
                </tr>
                <tr>
                    <td>
                    </td>
                </tr>
            </table>
    </div>
   
</form>



</asp:Content>

