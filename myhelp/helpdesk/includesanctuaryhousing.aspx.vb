﻿Imports System.Net
Imports System.Xml
Imports System.IO
Imports System.Text

Partial Class includesanctuaryhousing
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            If Not IIf(Core.data_select_value("SELECT COUNT(*) FROM dbo.tblUsersRequests WHERE intRequestFK = 3 AND intUserFK = " & MySession.UserID) > 0, True, False) Then
                Response.Redirect("~/myaccount/default.aspx", True)
            End If

            If Not Page.IsPostBack = True Then

                Dim description As HtmlMeta = New HtmlMeta()
                description.Name = "description"
                description.Content = "Utilisys - Helpdesk"
                Dim keywords As HtmlMeta = New HtmlMeta()
                keywords.Name = "keywords"
                keywords.Content = "Helpdesk"
                Page.Header.Controls.Add(description)
                Page.Header.Controls.Add(keywords)

                With Me.County
                    .Items.Clear()
                    .DataSource = Core.data_select("SELECT '-- select --' [county] UNION SELECT DISTINCT county FROM uml_cms.dbo.tblcounty WHERE CountryFK = 826 ORDER BY county")
                    .DataValueField = "county"
                    .DataTextField = "county"
                    .DataBind()
                End With

                With Me.mopprovider
                    .Items.Clear()
                    .DataSource = Core.data_select("SELECT 'Unknown' [value], 0 [odr] UNION SELECT DISTINCT mop, 1 from uml_cms.[dbo].[lkpmop] WHERE pkmop not in (-1,13)  order by [odr],value")
                    .DataValueField = "value"
                    .DataTextField = "value"
                    .DataBind()
                End With

                With Me.dcprovider
                    .Items.Clear()
                    .DataSource = Core.data_select("SELECT 'Unknown' [value], 0 [odr] UNION SELECT DISTINCT dadcdesc, 1 from uml_cms.[dbo].[lkpDADC] WHERE pkdadc_id not in (-1,1)  order by [odr],value")
                    .DataValueField = "value"
                    .DataTextField = "value"
                    .DataBind()
                End With

            End If

            Me.DateofRequest.Text = Date.Now.ToString("dd/MM/yyyy HH:mm tt ")

        Catch ex As Exception
            '
        End Try

    End Sub

    Protected Sub PostTo(url As String, postData As String)
        Dim myWebRequest As HttpWebRequest = TryCast(WebRequest.Create(url), HttpWebRequest)
        myWebRequest.Method = "POST"
        Dim byteArray As Byte() = System.Text.Encoding.[Default].GetBytes(postData)
        myWebRequest.ContentType = "application/x-www-form-urlencoded"
        myWebRequest.ContentLength = byteArray.Length
        Dim dataStream As System.IO.Stream = myWebRequest.GetRequestStream()
        dataStream.Write(byteArray, 0, byteArray.Length)
        dataStream.Close()

        Dim myWebResponse As WebResponse = myWebRequest.GetResponse()
        'Response.Write((DirectCast(myWebResponse, HttpWebResponse)).StatusDescription)
        dataStream = myWebResponse.GetResponseStream()
        Dim reader As New System.IO.StreamReader(dataStream)
        Dim responseFromServer As String = "<?xml version='1.0'?>" & reader.ReadToEnd()
        'Response.Write(responseFromServer + ":")

        Dim xmlDoc As New XmlDocument
        xmlDoc.LoadXml(responseFromServer)

        Dim formResponse As String = "<table>"
        Dim outcome As String = xmlDoc.SelectSingleNode("/operation/operationstatus").InnerText
        formResponse &= "<tr><td width='100px'>Outcome</td> <td>" & outcome & "</td></tr>"
        If outcome = "Success" Then
            formResponse &= "<tr><td>Request ID</td> <td>" & xmlDoc.SelectSingleNode("/operation/workorderid").InnerText & "</td></tr>"
        ElseIf outcome = "Failure" Then
            Dim xmlReason As String = xmlDoc.SelectSingleNode("/operation/message").InnerText
            formResponse &= "<tr><td>Reason</td> <td>" & xmlReason & "</td></tr>"
            If xmlReason = "Invalid UserName or Password" Then
                formResponse &= "<tr><td></td> <td>Please contact <a style='color:blue;text-decoration:underline;' href='mailto:dave.clarke@ems.schneider-electric.com?Subject=UTILISYS Helpdesk User Account Error'>Dave Clarke</a></td></tr>"
            End If
        End If
        formResponse &= "</table>"

        reader.Close()
        dataStream.Close()
        myWebResponse.Close()

        Me.formResponse.Text = formResponse
        Me.panelForm.Visible = False
        Me.panelResponse.Visible = True

    End Sub

    Protected Sub btnSubmitForm_Click(sender As Object, e As System.EventArgs) Handles btnSubmitForm.Click

        Dim URL As String = "http://uk-ed0-sdsk-02.mceg.local/servlets/RequestServlet"

        Dim DateofRequest As String = Me.DateofRequest.Text
        Dim FuelType As String = Me.FuelType.SelectedItem.Text

        Dim Department As String = Me.Department.Text
        Dim LegalEntity As String = Me.LegalEntity.Text
        Dim Meter As String = Me.MPANMPR.Text

        Dim SiteName As String = Me.SiteName.Text
        Dim Address1 As String = Me.Address1.Text
        Dim Address2 As String = Me.Address2.Text
        Dim Address3 As String = Me.Address3.Text
        Dim County As String = Me.County.SelectedItem.Text
        Dim Postcode As String = Me.PostCode.Text

        Dim CurrentSupplier As String = Me.CurrentSupplier.Text
        Dim CurrentContractStatus As String = Me.CurrentContractStatus.Text
        Dim CurrentContractEndDate As String = Me.CurrentContractEndDate.Text

        Dim VATRate As String = Me.VATRate.Text
        Dim StarkRefNo As String = Me.StarkRefNo.Text

        Dim EstimatedAnnualkWh As String = Me.EstimatedkWh.Text

        Dim Description As String = Me.AdditionalNotes.Text
        Dim title As String = FuelType & " - " & SiteName


        Dim reqtemplate As String = "Include - Sanctuary Housing Association"

        Dim targetURL As String = "/servlets/RequestServlet"
        Dim operation As String = "AddRequest"
        Dim technician As String = "Unassigned"


        Dim username As String = Core.data_select_value("SELECT TOP 1 LOWER(username) FROM dbo.tblUserHelpdeskDetails WHERE intDisabled = 0 AND intUserFK = " & MySession.UserID).ToString
        Dim password As String = Core.data_select_value("SELECT TOP 1 password FROM dbo.tblUserHelpdeskDetails WHERE intDisabled = 0 AND intUserFK = " & MySession.UserID).ToString
        Dim logonDomainName As String = Core.data_select_value("SELECT TOP 1 logonDomainName FROM dbo.tblUserHelpdeskDetails WHERE intDisabled = 0 AND intUserFK = " & MySession.UserID).ToString
        Dim domain_name As String = Core.data_select_value("SELECT TOP 1 CASE WHEN DOMAIN_NAME IS NULL THEN '' ELSE DOMAIN_NAME END [DOMAIN_NAME] FROM dbo.tblUserHelpdeskDetails WHERE intDisabled = 0 AND intUserFK = " & MySession.UserID).ToString

        'username = "sanctuaryhousingassociation"
        'password = "Cabbage1"
        'logonDomainName = "Local Authentication"
        'domain_name = ""

        Dim postData As String = ""
        postData &= "targetURL=" & targetURL
        postData &= "&reqtemplate=" & reqtemplate
        postData &= "&technician=" & technician
        postData &= "&username=" & username
        postData &= "&password=" & password
        postData &= "&logonDomainName=" & logonDomainName
        If domain_name.Length > 0 Then postData &= "&DOMAIN_NAME=" & domain_name
        postData &= "&operation=" & operation

        postData &= "&Date of Request=" & DateofRequest
        postData &= "&Department=" & Department
        postData &= "&Legal Entity=" & LegalEntity
        postData &= "&Fuel Type=" & FuelType
        postData &= "&MPAN/MPR=" & Meter
        postData &= "&Site Name=" & SiteName
        postData &= "&Address1=" & Address1
        postData &= "&Address2=" & Address2
        postData &= "&Address3=" & Address3
        postData &= "&County=" & County
        postData &= "&Post Code=" & Postcode
        postData &= "&Current Supplier=" & CurrentSupplier
        postData &= "&Current Contract Status=" & CurrentContractStatus
        postData &= "&Current Contract End Date=" & CurrentContractEndDate
        postData &= "&Estimated Annual kWh=" & EstimatedAnnualkWh
        postData &= "&VAT Rate=" & VATRate
        postData &= "&Stark Ref No=" & StarkRefNo
        postData &= "&description=" & Description
        postData &= "&title=" & title

        Me.Department.BackColor = Nothing
        Me.LegalEntity.BackColor = Nothing
        Me.FuelType.BackColor = Nothing
        Me.MPANMPR.BackColor = Nothing
        Me.SiteName.BackColor = Nothing
        Me.CurrentSupplier.BackColor = Nothing
        Me.CurrentContractStatus.BackColor = Nothing
        Me.formError.Text = ""

        If Department.Length = 0 Then Me.formError.Text &= "Department is a required field" & "</br>" : Me.Department.BackColor = Drawing.Color.Red
        If LegalEntity.Length = 0 Then Me.formError.Text &= "Legal Entity is a required field" & "</br>" : Me.LegalEntity.BackColor = Drawing.Color.Red
        If FuelType = "-- select --" Then Me.formError.Text &= "Fuel Type is a required field" & "</br>" : Me.FuelType.BackColor = Drawing.Color.Red
        If Meter.Length = 0 Then Me.formError.Text &= "MPAN/MPR is a required field" & "</br>" : Me.MPANMPR.BackColor = Drawing.Color.Red
        If SiteName.Length = 0 Then Me.formError.Text &= "Site Name is a required field" & "</br>" : Me.SiteName.BackColor = Drawing.Color.Red
        If CurrentSupplier.Length = 0 Then Me.formError.Text &= "Current Supplier is a required field" & "</br>" : Me.CurrentSupplier.BackColor = Drawing.Color.Red
        If CurrentContractStatus.Length = 0 Then Me.formError.Text &= "Current Contract Status is a required field" & "</br>" : Me.CurrentContractStatus.BackColor = Drawing.Color.Red

        If FuelType.Contains(" Elec") And Not UCase$(Meter) Like "[0-9][0-9] [0-9][0-9][0-9] [0-9][0-9][0-9] [0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]" Then
            Me.formError.Text &= "For Elec meters you need to include the top line of the MPAN in the following format:" & "</br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;00 000 000 0000000000000" & "</br>"
            Me.MPANMPR.BackColor = Drawing.Color.Red
        End If


            If Me.formError.Text.Length = 0 Then PostTo(URL, postData)

    End Sub
End Class
