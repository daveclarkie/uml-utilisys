﻿<%@ Page Title="UTILISYS - my help" Language="VB" MasterPageFile="~/landing.master" AutoEventWireup="false" CodeFile="excludesanctuaryhousing.aspx.vb" Inherits="excludesanctuaryhousing"  %>

<asp:Content ID="Content1" ContentPlaceHolderID="content_right" Runat="Server">

<form runat="server">

    <div class="content_back general">
            <table cellpadding="0" cellspacing="0">
               
                <tr>
                    
                    <td colspan="1">
                        <table cellpadding="0" cellspacing="0">

                            <tr>
                                <td style="height:3px;">
                                    
                                </td>
                            </tr>

                             <tr>
                                <td>
                                     <b>Sanctuary Housing Association</b>
                                </td>
                            </tr>
                                                        
                            <tr>
                                <td style="height:3px;">
                                    
                                </td>
                            </tr>

                             <tr>
                                <td>
                                     <b>Site Removal</b>
                                </td>
                            </tr>
                                                        
                            <tr>
                                <td style="height:3px;">
                                    
                                </td>
                            </tr>

                             <tr>
                                <td>
                                     <asp:Label runat="server" ID="formError" ForeColor="Red"></asp:Label>
                                </td>
                            </tr>
                                                        
                        </table>
                    </td>
                </tr> <!-- Meter Selection Table -->
                <tr>
                    <td>
                        <asp:Panel runat="server" ID="panelForm">
                            <table width="650px" cellpadding="5px" cellspacing="0" style="border:1px #ccc solid;" align="center">
                                       
                            <tr style="height:10px;">
		                        <td colspan="8"></td>
	                        </tr>
                                                    
	                        <tr>
                                <td style="width:5px;"></td>
		                        <td>Request Type</td>
		                        <td>:</td>
		                        <td>Exclude</td>

                                <td style="width:20px;"></td>
		                            
                                <td></td>
		                        <td></td>
		                        <td></td>
	                        </tr>                                
                                                            
	                        <tr>
                                <td></td>
		                        <td>Date of Request <span style="color:Red;">*</span></td>
		                        <td>:</td>
		                        <td><asp:Label runat="server" id="DateofRequest" style="width:150px;"></asp:Label></td>

                                <td style="width:20px;"></td>
		                            
                                <td>End Date <span style="color:Red;">*</span></td>
		                        <td>:</td>
		                        <td><asp:TextBox runat="server" id="EndDate" style="width:150px;" ></asp:TextBox></td>
	                        </tr>  
                            
	                        <tr style="height:10px;">
		                        <td colspan="8"></td>
	                        </tr>                                              
                                                                              
	                        <tr>
                                <td></td>
		                        <td>MPAN/MPR <span style="color:Red;">*</span></td>
		                        <td>:</td>
		                        <td colspan="4"><asp:TextBox runat="server" id="MPANMPR" style="width:250px;" ></asp:TextBox></td>
                                <td><asp:Button runat="server" ID="btnLookupMeter" Text="Look Up Meter Point" style="width:150px;" /></td>
	                        </tr>
                                      
	                        <tr style="height:10px;">
		                        <td colspan="8"></td>
	                        </tr>                                
                                                            
	                        <tr>
                                <td></td>
		                        <td>Fuel Type <span style="color:Red;">*</span></td>
		                        <td>:</td>
		                        <td><asp:TextBox runat="server" id="FuelType" style="width:150px;" Enabled="false" ></asp:TextBox></td>

                                <td style="width:20px;"></td>
		                            
                                <td></td>
		                        <td></td>
		                        <td></td>
	                        </tr>                  
                            
                            <tr>
                                <td></td>
		                        <td>Site Name <span style="color:Red;">*</span></td>
		                        <td>:</td>
		                        <td colspan="5"><asp:TextBox runat="server" id="SiteName" style="width:350px;" Enabled="false" ></asp:TextBox></td>
	                        </tr>
                                                            
	                        <tr>
                                <td></td>
		                        <td>Address 1</td>
		                        <td>:</td>
		                        <td colspan="5"><asp:TextBox runat="server" id="Address1" style="width:350px;" Enabled="false" ></asp:TextBox></td>
	                        </tr>
                                                            
	                        <tr>
                                <td></td>
		                        <td>Address 2</td>
		                        <td>:</td>
		                        <td colspan="5"><asp:TextBox runat="server" id="Address2" style="width:350px;" Enabled="false" ></asp:TextBox></td>
	                        </tr>
                                                            
	                        <tr>
                                <td></td>
		                        <td>Address 3</td>
		                        <td>:</td>
		                        <td colspan="5"><asp:TextBox runat="server" id="Address3" style="width:350px;" Enabled="false" ></asp:TextBox></td>
	                        </tr>
                                                            
	                        <tr>
                                <td></td>
		                        <td>County</td>
		                        <td>:</td>
		                        <td colspan="5"><asp:TextBox runat="server" id="County" style="width:350px;" Enabled="false" ></asp:TextBox></td>
	                        </tr>
                                                            
	                        <tr>
                                <td></td>
		                        <td>Post Code</td>
		                        <td>:</td>
		                        <td colspan="5"><asp:TextBox runat="server" id="PostCode" style="width:350px;" Enabled="false" ></asp:TextBox></td>
	                        </tr>
                            
                               
	                        <tr>
                                <td></td>
		                        <td>Current Supplier</td>
		                        <td>:</td>
		                        <td><asp:TextBox runat="server" id="CurrentSupplier" style="width:150px;" Enabled="false" ></asp:TextBox></td>

                                <td style="width:20px;"></td>
		                            
		                        <td>Current Status</td>
		                        <td>:</td>
		                        <td><asp:TextBox runat="server" id="CurrentContractStatus" style="width:150px;" Enabled="false" ></asp:TextBox></td>
	                        </tr>                                
                                                            
	                        <tr>
                                <td></td>
		                        <td>Current Contract End Date</td>
		                        <td>:</td>
		                        <td><asp:TextBox runat="server" id="CurrentContractEndDate" style="width:150px;" Enabled="false" ></asp:TextBox></td>

                                <td style="width:20px;"></td>
		                            
                                <td></td>
		                        <td></td>
		                        <td></td>
	                        </tr>   

	                        <tr style="height:10px;">
		                        <td colspan="8"></td>
	                        </tr>                             
                                                            
	                        <tr>
                                <td></td>
		                        <td colspan="7"><b>Additional Notes</b></td>
                            </tr>

                            <tr>
                                <td></td>
		                        <td colspan="7"><asp:TextBox runat="server" id="AdditionalNotes" TextMode="MultiLine" style="width:613px; height: 120px;"  ></asp:TextBox></td>
	                        </tr>   

	                        <tr style="height:10px;">
		                        <td colspan="8"></td>
	                        </tr>

                            <tr>
                                <td></td>
		                        <td colspan="7"><asp:Button ID="btnSubmitForm" runat="server" Text="Add Request to Helpdesk" Width="620px" /></td>
	                        </tr>   

	                        <tr style="height:10px;">
		                        <td colspan="8"></td>
	                        </tr>

                        </table>
                        </asp:Panel>
                        
                        <asp:Panel runat="server" ID="panelResponse" Visible="false">
                                <table width="650px" cellpadding="5px" cellspacing="0" style="border:1px #ccc solid;" align="center">
                                       
                                    <tr>
                                        <td></td>
		                                <td><asp:label runat="server" id="formResponse" ></asp:label></td>
	                                </tr>   
                                </table>
                        </asp:Panel>
                    </td>                
                </tr>
                <tr>
                    <td>
                    </td>
                </tr>
            </table>
    </div>
   
</form>



</asp:Content>

