<%@ Page Language="VB" maintainscrollpositiononpostback="true" MasterPageFile="~/landing.master" AutoEventWireup="false" CodeFile="loginanalysis.aspx.vb" Inherits="loginanalysis" title="Administration Area | Edit User"%>

<%@ Register Assembly="DundasWebChart" Namespace="Dundas.Charting.WebControl" TagPrefix="DCWC" %>

<asp:content ID="Content1" contentplaceholderid="content_right" runat="server">
    <div class="content_back my_admin">
        <div id="admin" class="admin_area" runat="server">
            <form id="frmUser" runat="server"> 
                <div id="pnlStatus" class="status_message" runat="server" visible="false">
                    <div class="status"><asp:label id="lblStatus" runat="server" /></div>
                </div>
                
                <!-- MENU -->
                <ul class='menu'>
                    <li><asp:hyperlink runat="server" id="lnkloginbyuser" navigateurl='loginanalysis.aspx?tab=loginbyuser' cssclass=''>Login by User</asp:hyperlink></li>
                    <li><asp:hyperlink runat="server" id="lnkdailyanalysis" navigateurl='loginanalysis.aspx?tab=dailyanalysis' cssclass='current'>Daily Analysis</asp:hyperlink></li>
                </ul>
                <!-- CHART Daily Analysis -->
                <asp:Panel ID="pnldailyanalysis" runat="server">
                    <DCWC:Chart ID="Chart1" Width="650" Height="450" runat="server" BackColor="#FFFFC0" BorderLineColor="Black" BorderLineStyle="Solid" BorderLineWidth="2" DataSourceID="portal_utilitymasters_co_uk" Palette="Pastel" >
                        <Legends>
                            <DCWC:Legend Name="Default" LegendStyle="Row" BorderColor="Black" Docking="Bottom"></DCWC:Legend>
                        </Legends>
                        
                        <Titles>
                            <DCWC:Title Name="Title1" Text="Daily Login Analysis" Font="Tahoma, 9.75pt"></DCWC:Title>
                        </Titles>
                        
                        <Series>
                            
                            <DCWC:Series Name="Unique Logins" ValueMemberX="dtmDate" ValueMembersY="intUniqueLogins" XValueType="DateTime" ShadowOffset="1" ChartType="Line" ShowLabelAsValue="False" BorderColor="64, 64, 64" BorderWidth="2" Font="Tahoma, 8.25pt" CustomAttributes="LabelStyle=TopLeft">
                                
                            </DCWC:Series>
                            
                        </Series>
                        
                        <ChartAreas>
                            <DCWC:ChartArea Name="Default">
                                <AxisY Title="Visits" TitleFont="Tahoma, 8.25pt">
                                    <LabelStyle Format="N0"></LabelStyle>
                                </AxisY>
                                <AxisX Title="Date" TitleFont="Tahoma, 8.25pt" Margin="False" Interval="1">
                                    <LabelStyle Format="d"></LabelStyle>
                                </AxisX>
                            </DCWC:ChartArea>
                        </ChartAreas>

                        <BorderSkin PageColor="AliceBlue" SkinStyle="Emboss"></BorderSkin>
                    </DCWC:Chart>
                    
                    <asp:SqlDataSource ID="portal_utilitymasters_co_uk" runat="server" ConnectionString="<%$ ConnectionStrings:portal.utilitymasters.co.ukConnectionString %>" SelectCommand="usp_Admin_UsersLogin_Analysis" SelectCommandType="StoredProcedure">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="rblInternal" Name="intInternal" PropertyName="SelectedValue" Type="Int32" DefaultValue="0" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                    
                </asp:Panel>
                <!-- TABLE  -->
                <asp:Panel ID="pnlloginbyuser" runat="server">
                    <asp:GridView ID="GridView1" AutoGenerateSelectButton="true" runat="server" AutoGenerateColumns="False" DataKeyNames="intUserPK" DataSourceID="loginbyuser" AllowSorting="True" Width="650" cssclass="users" headerstyle-cssclass="header" >
                        <Columns>
                            <asp:BoundField DataField="intUserPK" HeaderText="ID" InsertVisible="False" ReadOnly="True" SortExpression="intUserPK" />
                            <asp:BoundField DataField="varEmailAddress" HeaderText="Email Address" SortExpression="varEmailAddress" ItemStyle-Width="250" />
                            <asp:BoundField DataField="intLoginToday" HeaderText="Today" ReadOnly="True" SortExpression="intLoginToday" />
                            <asp:BoundField DataField="intLogin7Days" HeaderText="1 - 7 days" ReadOnly="True" SortExpression="intLogin7Days" />
                            <asp:BoundField DataField="intLogin30Days" HeaderText="8 - 30 days" ReadOnly="True" SortExpression="intLogin30Days" />
                            <asp:BoundField DataField="intLogin90Days" HeaderText="31 - 90 days" ReadOnly="True" SortExpression="intLogin90Days" />
                            <asp:BoundField DataField="intLoginTotal" HeaderText="Total" ReadOnly="True" SortExpression="intLoginTotal" />
                        </Columns>
                    </asp:GridView>
                    
                    <asp:SqlDataSource ID="loginbyuser" runat="server" ConnectionString="<%$ ConnectionStrings:portal.utilitymasters.co.ukConnectionString %>" SelectCommand="usp_Admin_UsersLogin_LoginByDates" SelectCommandType="StoredProcedure">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="rblDays" Name="intDays" PropertyName="SelectedValue" Type="Int32" DefaultValue="1" />
                            <asp:ControlParameter ControlID="rblInternal" Name="intInternal" PropertyName="SelectedValue" Type="Int32" DefaultValue="0" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                    
                    
                    <DCWC:Chart ID="Chart2" Width="650" Height="300" runat="server" BackColor="#FFFFC0" BorderLineColor="Black" BorderLineStyle="Solid" BorderLineWidth="2" DataSourceID="loginbyuser_Chart" Palette="Pastel" >
                        <Legends>
                            <DCWC:Legend Name="Default" LegendStyle="Row" BorderColor="Black" Docking="Bottom"></DCWC:Legend>
                        </Legends>
                        
                        <Titles>
                            <DCWC:Title Name="Title1" Text="Daily Analysis" Font="Tahoma, 9.75pt"></DCWC:Title>
                        </Titles>
                        
                        <Series>
                            
                            <DCWC:Series Name="Logins" ValueMemberX="dtmDate" ValueMembersY="intTotalLogins" XValueType="DateTime" ShadowOffset="1" ChartType="Line" ShowLabelAsValue="False" BorderColor="64, 64, 64" BorderWidth="2" Font="Tahoma, 8.25pt" CustomAttributes="LabelStyle=TopLeft">                                
                            </DCWC:Series>
             
                            <DCWC:Series Name="Password Errors" ValueMemberX="dtmDate" ValueMembersY="intInvalidLogins" XValueType="DateTime" ShadowOffset="1" ChartType="Line" ShowLabelAsValue="False" BorderColor="64, 64, 64" BorderWidth="2" Font="Tahoma, 8.25pt" CustomAttributes="LabelStyle=TopLeft">                                
                            </DCWC:Series>
                            
                        </Series>
                        
                        <ChartAreas>
                            <DCWC:ChartArea Name="Default">
                                <AxisY Title="Visits" TitleFont="Tahoma, 8.25pt">
                                    <LabelStyle Format="N0"></LabelStyle>
                                </AxisY>
                                <AxisX Title="Date" TitleFont="Tahoma, 8.25pt" Margin="False" Interval="1">
                                    <LabelStyle Format="d"></LabelStyle>
                                </AxisX>
                            </DCWC:ChartArea>
                        </ChartAreas>

                        <BorderSkin PageColor="AliceBlue" SkinStyle="Emboss"></BorderSkin>
                    </DCWC:Chart>
                    
                    <asp:SqlDataSource ID="loginbyuser_Chart" runat="server" ConnectionString="<%$ ConnectionStrings:portal.utilitymasters.co.ukConnectionString %>" SelectCommand="usp_Admin_UsersLogin_DailyAnalysisbyUser" SelectCommandType="StoredProcedure">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="GridView1" Name="intUserPK" PropertyName="SelectedValue" Type="Int32" DefaultValue="0" />
                        </SelectParameters>
                    </asp:SqlDataSource>              
                    
                    
                </asp:Panel>
                <!-- FILTER  -->
                <asp:Panel ID="pnlFilter" runat="server">
                    <table cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                                <asp:table ID="tblFilter" runat="server" style="background-color:#FFFFC0; border: solid 1px black;" >
                                    <asp:TableRow>
                                        <asp:TableCell style="height:5px; " ColumnSpan="4">
                                            <asp:Button ID="btnFilter" runat="server" Text="Show Filter" Width="100px" />
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow ID="trwHeader" runat="server" Visible="false">
                                        <asp:TableCell style="width:10px; ">
                                        
                                        </asp:TableCell>
                                        <asp:TableCell style="width:100px; vertical-align:top;">
                                            <b>UML Employees</b>
                                        </asp:TableCell>
                                        <asp:TableCell style="width:100px; vertical-align:top;">
                                            <b>Date Filter</b>
                                        </asp:TableCell>
                                        <asp:TableCell style="width:10px;">
                                        
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow ID="trwContent" runat="server" Visible="false">
                                        <asp:TableCell>
                                        
                                        </asp:TableCell>
                                        <asp:TableCell style="vertical-align:top;">
                                            <asp:RadioButtonList ID="rblInternal" runat="server" AutoPostBack="true" RepeatLayout="Table" RepeatColumns="1" RepeatDirection="Vertical" Width="150px">
                                                <asp:ListItem Value="0" Text="    Exclude" Selected="True"></asp:ListItem>
                                                <asp:ListItem Value="1" Text="    Include"></asp:ListItem>
                                            </asp:RadioButtonList>
                                        </asp:TableCell>
                                        <asp:TableCell style="vertical-align:top;">
                                            <asp:RadioButtonList ID="rblDays" runat="server" AutoPostBack="true" RepeatLayout="Table" RepeatColumns="1" RepeatDirection="Vertical" Width="150px">
                                                <asp:ListItem Value="0" Text="    Today"></asp:ListItem>
                                                <asp:ListItem Value="1" Text="    1 - 7 days"></asp:ListItem>
                                                <asp:ListItem Value="2" Text="    8 - 30 days"></asp:ListItem>
                                                <asp:ListItem Value="3" Text="    31 - 90 days"></asp:ListItem>
                                                <asp:ListItem Value="4" Text="    Total" Selected="True"></asp:ListItem>
                                            </asp:RadioButtonList>
                                        </asp:TableCell>
                                        <asp:TableCell>
                                        
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow ID="trwFooter" runat="server" Visible="false">
                                        <asp:TableCell style="height:5px;" ColumnSpan="4">
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:table> 
                                
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </form>
        </div>
    </div>
</asp:content>
