<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Default.aspx.vb" Inherits="myadmin_Default" %>

<%@ Register Assembly="DundasWebChart" Namespace="Dundas.Charting.WebControl" TagPrefix="DCWC" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <DCWC:Chart ID="Chart1" runat="server" BackColor="#FFFFC0" BorderLineColor="Black" BorderLineStyle="Solid" BorderLineWidth="2" DataSourceID="portal_utilitymasters_co_uk" Palette="Pastel">
            <Legends>
                <DCWC:Legend Name="Default" LegendStyle="Row" BorderColor="Black" Docking="Bottom"></DCWC:Legend>
            </Legends>
            
            <Titles>
                <DCWC:Title Name="Title1" Text="Daily Login Analysis" Font="Tahoma, 9.75pt"></DCWC:Title>
            </Titles>
            
            <Series>
                <DCWC:Series Name="Total" ValueMemberX="dtmDate" ValueMembersY="intTotalLogins" XValueType="DateTime" ShadowOffset="1" ChartType="Line" ShowLabelAsValue="True" BorderColor="64, 64, 64" BorderWidth="2" Font="Tahoma, 8.25pt" CustomAttributes="LabelStyle=TopLeft">
                    <SmartLabels Enabled="True"></SmartLabels>
                </DCWC:Series>
                <DCWC:Series Name="Unique" ValueMemberX="dtmDate" ValueMembersY="intUniqueLogins" XValueType="DateTime" ShadowOffset="1" ChartType="Line" ShowLabelAsValue="True" BorderColor="64, 64, 64" BorderWidth="2" Font="Tahoma, 8.25pt" CustomAttributes="LabelStyle=TopLeft">
                    <SmartLabels Enabled="True"></SmartLabels>
                </DCWC:Series>
                <DCWC:Series Name="Invalid" ValueMemberX="dtmDate" ValueMembersY="intInvalidLogins" XValueType="DateTime" ChartType="Line" ShowLabelAsValue="True" Font="Tahoma, 8.25pt" CustomAttributes="LabelStyle=TopLeft">
                    <SmartLabels Enabled="True"></SmartLabels>
                </DCWC:Series>
            </Series>
            
            <ChartAreas>
                <DCWC:ChartArea Name="Default">
                    <AxisY Title="Visits" TitleFont="Tahoma, 8.25pt">
                        <LabelStyle Format="N0"></LabelStyle>
                    </AxisY>
                    <AxisX Title="Date" TitleFont="Tahoma, 8.25pt" Margin="False">
                        <LabelStyle Format="d"></LabelStyle>
                    </AxisX>
                </DCWC:ChartArea>
            </ChartAreas>

            <BorderSkin PageColor="AliceBlue" SkinStyle="Emboss"></BorderSkin>
        </DCWC:Chart>
        
        <asp:SqlDataSource ID="portal_utilitymasters_co_uk" runat="server" ConnectionString="<%$ ConnectionStrings:portal.utilitymasters.co.ukConnectionString %>"
            SelectCommand="usp_Admin_UsersLogin_Analysis" SelectCommandType="StoredProcedure">
        </asp:SqlDataSource>
        
        
        
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="intUserPK"
            DataSourceID="loginbyuser" AllowSorting="True">
            <Columns>
                <asp:BoundField DataField="intUserPK" HeaderText="ID" InsertVisible="False" ReadOnly="True" SortExpression="intUserPK" />
                <asp:BoundField DataField="varEmail" HeaderText="Email Address" SortExpression="varEmail" />
                <asp:BoundField DataField="LoginToday" HeaderText="Login Today" ReadOnly="True" SortExpression="LoginToday" />
                <asp:BoundField DataField="InvalidToday" HeaderText="Invalid Today" ReadOnly="True" SortExpression="InvalidToday" />
                <asp:BoundField DataField="LoginAll" HeaderText="Login All Time" ReadOnly="True" SortExpression="LoginAll" />
                <asp:BoundField DataField="InvalidAll" HeaderText="Invalid All Time" ReadOnly="True" SortExpression="InvalidAll" />
            </Columns>
        </asp:GridView>
        <asp:SqlDataSource ID="loginbyuser" runat="server" ConnectionString="<%$ ConnectionStrings:portal.utilitymasters.co.ukConnectionString %>"
            SelectCommand="usp_Admin_UsersLogin_LoginByDates" SelectCommandType="StoredProcedure">
            <SelectParameters>
                <asp:ControlParameter ControlID="GridView1" Name="intDays" PropertyName="SelectedValue" Type="Int32" />
                <asp:QueryStringParameter Name="intInternal" QueryStringField="asd" Type="Int32" />
            </SelectParameters>
        </asp:SqlDataSource>
    
    </div>
    </form>
</body>
</html>
