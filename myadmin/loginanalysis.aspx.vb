
Partial Class loginanalysis

    Inherits System.Web.UI.Page

    Private _mtxtID As TextBox = Nothing
    Private _mstrSearch As String = ""
    Private _mstrTab As String = "loginbyuser"
    Private _mstrAction As String = "edit"
    Private _mintID As Integer = 0
    Private _mintSitesPerPage As Integer = 20 'default
    Private _mintPage As Integer = 1

    'INITIALISATION

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim description As HtmlMeta = New HtmlMeta()
        description.Name = "description"
        description.Content = "Utility Masters customer extranet - Edit User"
        Dim keywords As HtmlMeta = New HtmlMeta()
        keywords.Name = "keywords"
        keywords.Content = "admin"
        Page.Header.Controls.Add(description)
        Page.Header.Controls.Add(keywords)

        'for paging etc
        If Not Request.QueryString("page") Is Nothing Then Me._mintPage = Request.QueryString("page")
        If Not Request.QueryString("tab") Is Nothing Then Me._mstrTab = Request.QueryString("tab")
        If Not Request.QueryString("id") Is Nothing Then Me._mintID = Request.QueryString("id")
        If Not Request.QueryString("action") Is Nothing Then Me._mstrAction = Request.QueryString("action")

        'setup form
        If Not Page.IsPostBack Then

            'get cookie values for paging
        Else
            Me.Response.Cookies("um_sites_page").Expires = Now().AddYears(10)
        End If

        'check user level is admin and redirect to their home page if not!
        If Not IIf(Core.data_select_value("select bitAllowAdmin from tblUsers where intUserPK = " & MySession.UserID) = "True", True, False) Then
            Response.Redirect(Core.data_select_value("select varHomepageURL from tblUsers where intUserPK = " & MySession.UserID))
        Else

            SetupForm()
        End If
    End Sub

    Private Sub SetupForm()
        'reset view tabs

        Me.pnlStatus.Visible = False

        Me.pnldailyanalysis.Visible = False
        Me.lnkdailyanalysis.CssClass = ""

        Me.pnlloginbyuser.Visible = False
        Me.lnkloginbyuser.CssClass = ""

        Try

            Me.lnkdailyanalysis.NavigateUrl = "~/myadmin/loginanalysis.aspx?tab=dailyanalysis&action=view&id=" & Me._mintID.ToString
            Me.lnkloginbyuser.NavigateUrl = "~/myadmin/loginanalysis.aspx?tab=loginbyuser&action=view&id=" & Me._mintID.ToString

            'display the correct panel
            Dim pnl As Panel
            pnl = Me.Form.FindControl("pnl" & StrConv(_mstrTab, VbStrConv.ProperCase))
            pnl.Visible = True
            'select correct tab
            Dim lnk As HyperLink
            lnk = Me.Form.FindControl("lnk" & StrConv(_mstrTab, VbStrConv.ProperCase))
            lnk.CssClass = "current"

            Select Case _mstrTab
                Case "dailyanalysis"
                    LoadDailyAnalysis()
            End Select
        Catch ex As Exception
            Core.SendAdminStatusReport("Error loading User in setup form. Error: " & ex.ToString)
        End Try
    End Sub

    Private Sub LoadDailyAnalysis()

    End Sub

    Private Sub SetStatus(ByVal Message As String, ByVal StatusType As Core.enmStatus)

        If Len(Message) > 0 Then
            Me.pnlStatus.Visible = True
            If StatusType = Core.enmStatus.StatusError Then
                Me.pnlStatus.Attributes.Add("class", "error")
            Else
                Me.pnlStatus.Attributes.Add("class", "ok")
            End If
            Me.lblStatus.Text = Message
        End If
    End Sub

    Protected Sub btnFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFilter.Click
        If Me.trwHeader.Visible = False Then
            Me.trwHeader.Visible = True
            Me.trwContent.Visible = True
            Me.trwFooter.Visible = True
            Me.btnFilter.Text = "Hide Filter"
        Else
            Me.trwHeader.Visible = False
            Me.trwContent.Visible = False
            Me.trwFooter.Visible = False
            Me.btnFilter.Text = "Show Filter"
        End If
    End Sub
End Class
