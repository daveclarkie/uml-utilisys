﻿Imports Core
Imports Dundas.Charting.WebControl
Imports Dundas.Charting.WebControl.Utilities

Imports System.Data.SqlClient
Imports System.Data
Imports System.Drawing
Imports System.Windows

Partial Class myadmin_Users
    Inherits System.Web.UI.Page

    Dim strNotes As String = ""
    Public conn As String = Core.ConnectionString()


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Me.Page.IsPostBack = False Then

            Me.lblCountUsers.Text = Core.data_select_value("SELECT COUNT(*) FROM tblUsers WHERE bitDisabled = 'FALSE'")

            Me.lblCountUsers_24Hours.Text = Core.data_select_value("SELECT COUNT(*) FROM (SELECT DISTINCT intUserFK FROM tblUsersLoginAttempts WHERE intDisabled = 0 and dtmCreated > getdate()-1 and varOutcome = 'Success')A")

            Me.lblCountUsers_7Days.Text = Core.data_select_value("SELECT COUNT(*) FROM (SELECT DISTINCT intUserFK FROM tblUsersLoginAttempts WHERE intDisabled = 0 and dtmCreated > getdate()-7 and varOutcome = 'Success')A")

            ExecuteSearch(Me.txtSearchCriteria.Text)

            If Me.grdTable.SelectedIndex > -1 Then
                LoadUserDetails()
            End If

        End If

    End Sub

    Protected Sub btnSearchCriteria_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearchCriteria.Click

        ExecuteSearch(Me.txtSearchCriteria.Text)

    End Sub

    Protected Sub txtSearchCriteria_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearchCriteria.TextChanged
        ExecuteSearch(Me.txtSearchCriteria.Text)
    End Sub

    Private Sub ExecuteSearch(ByVal strCriteria As String)

        Dim strSortingCriteria As String = "varFullname"

        grdTable.Columns.Clear()

        Dim bf1 As BoundField
        bf1 = New BoundField
        Dim bf2 As BoundField
        bf2 = New BoundField
        Dim bf3 As BoundField
        bf3 = New BoundField
        Dim bf4 As BoundField
        bf4 = New BoundField

        bf1.DataField = "intUserPK"
        bf1.HeaderText = "ID"
        grdTable.Columns.Add(bf1)

        bf2.DataField = "varFullname"
        bf2.HeaderText = "Name"
        grdTable.Columns.Add(bf2)

        bf3.DataField = "varEmail"
        bf3.HeaderText = "Email"
        grdTable.Columns.Add(bf3)

        bf4.DataField = "varStatus"
        bf4.HeaderText = "Status"
        grdTable.Columns.Add(bf4)

        With Me.grdTable
            .DataSource = Core.data_select("EXEC [portal.utilitymasters.co.uk].dbo.usp_Admin_Users_Search '" & strCriteria & "', '" & strSortingCriteria & "'")

            .DataBind()
        End With

        Me.lblSearchCriteriaMatches.Text = Me.grdTable.Rows.Count

    End Sub

    Protected Sub grdTable_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles grdTable.SelectedIndexChanged

        Me.pnlSearch.Visible = False
        Me.pnlDisplayUser.Visible = True

        LoadUserDetails(Me.grdTable.SelectedRow.Cells(1).Text)
        Me.lblUserDetailsID.Text = Me.grdTable.SelectedRow.Cells(1).Text

    End Sub

    Protected Sub btnCloseDisplayUser_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCloseDisplayUser.Click
        Me.pnlSearch.Visible = True
        Me.pnlDisplayUser.Visible = False

        ExecuteSearch(Me.txtSearchCriteria.Text)

    End Sub

    Protected Sub btnSearchCriteriaReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearchCriteriaReset.Click
        Me.txtSearchCriteria.Text = Nothing
        ExecuteSearch(Me.txtSearchCriteria.Text)
    End Sub


    Private Sub ResetUserDetailsPanels()

        Me.pnlDisplayUserMainDetails.Visible = False
        Me.btnShowUserDetailsPanel.BackColor = Nothing

        Me.pnlDisplayUserCompanies.Visible = False
        Me.btnShowUserCompanies.BackColor = Nothing

        Me.pnlDisplayUserReports.Visible = False
        Me.btnShowUserReports.BackColor = Nothing

        Me.pnlDisplayUserAnalysis.Visible = False
        Me.btnShowUserAnalysis.BackColor = Nothing

        Me.pnlDisplayUserMainDetails.Visible = True
        Me.btnShowUserDetailsPanel.BackColor = Drawing.Color.LimeGreen



        Me.txtUserDetailsForename.Text = Nothing
        Me.txtUserDetailsSurname.Text = Nothing
        Me.txtUserDetailsEmailAddress.Text = Nothing
        Me.txtUserDetailsPassword.Text = Nothing
        Me.txtUserDetailsNotes.Text = Nothing
        Me.Image1.ImageUrl = "~/assets/My_Profile_Silhouette.jpg"

        Me.imgUserDetailsEmailAddress.ImageUrl = "~/assets/Metering/warning_48.png"
        Me.imgUserDetailsForename.ImageUrl = "~/assets/Metering/warning_48.png"
        Me.imgUserDetailsSurname.ImageUrl = "~/assets/Metering/warning_48.png"
        Me.imgUserDetailsPassword.ImageUrl = "~/assets/Metering/warning_48.png"


    End Sub

    Private Sub LoadUserDetails(Optional ByVal intUserPK As Integer = 0)
        ResetUserDetailsPanels()




        If intUserPK > 0 Then

            Me.lblUserDetailsID.Text = intUserPK

            Me.txtUserDetailsForename.Visible = True
            Me.txtUserDetailsSurname.Visible = True
            Me.txtUserDetailsPassword.Visible = True
            Me.txtUserDetailsNotes.Visible = True
            Me.imgUpload.Visible = True
            Me.btnSaveImage.Visible = True
            Me.btnDeleteImage.Visible = True
            Me.btnUserStatus.Visible = True
            Me.Image1.Visible = True


            Me.lblHeaderImage.Visible = True
            Me.lblHeaderUpload.Visible = True
            Me.lblHeaderNotes.Visible = True
            Me.lblHeaderPassword.Visible = True
            Me.lblHeaderSurname.Visible = True
            Me.lblHeaderForename.Visible = True



            Me.imgUserDetailsEmailAddress.Visible = True
            Me.imgUserDetailsForename.Visible = True
            Me.imgUserDetailsSurname.Visible = True
            Me.imgUserDetailsPassword.Visible = True

            Me.txtUserDetailsForename.Text = Core.data_select_value("SELECT varForename FROM [portal.utilitymasters.co.uk].dbo.tblUsers WHERE intUserPK = " & intUserPK)
            Me.txtUserDetailsSurname.Text = Core.data_select_value("SELECT varSurname FROM [portal.utilitymasters.co.uk].dbo.tblUsers WHERE intUserPK = " & intUserPK)
            Me.txtUserDetailsEmailAddress.Text = Core.data_select_value("SELECT varEmail FROM [portal.utilitymasters.co.uk].dbo.tblUsers WHERE intUserPK = " & intUserPK)
            Me.txtUserDetailsPassword.Text = Core.data_select_value("SELECT varPassword FROM [portal.utilitymasters.co.uk].dbo.tblUsers WHERE intUserPK = " & intUserPK)
            Me.txtUserDetailsPassword.ToolTip = Core.data_select_value("SELECT varPassword FROM [portal.utilitymasters.co.uk].dbo.tblUsers WHERE intUserPK = " & intUserPK)
            Me.txtUserDetailsNotes.Text = Core.data_select_value("SELECT varNotes FROM [portal.utilitymasters.co.uk].dbo.tblUsers WHERE intUserPK = " & intUserPK)

            Dim intUserEnabled As Integer = Core.data_select_value("SELECT CASE WHEN bitDisabled = 1 THEN 1 ELSE 0 END FROM [portal.utilitymasters.co.uk].dbo.tblUsers WHERE intUserPK = " & intUserPK)
            If intUserEnabled = 0 Then
                Me.btnUserStatus.Text = "ACTIVE"
                Me.btnUserStatus.BackColor = Drawing.Color.LimeGreen
            Else
                Me.btnUserStatus.Text = "DISABLED"
                Me.btnUserStatus.BackColor = Drawing.Color.Red
            End If

            Me.imgUserDetailsEmailAddress.ImageUrl = "~/assets/Metering/accepted_48.png"
            Me.imgUserDetailsForename.ImageUrl = "~/assets/Metering/accepted_48.png"
            Me.imgUserDetailsSurname.ImageUrl = "~/assets/Metering/accepted_48.png"
            Me.imgUserDetailsPassword.ImageUrl = "~/assets/Metering/accepted_48.png"


            ' Display the image from the database
            Dim strResultImage As String = Core.data_select_value("SELECT CASE WHEN image is null then 0 else 1 END FROM [portal.utilitymasters.co.uk].dbo.tblUsers WHERE intUserPK = " & intUserPK)
            If strResultImage = 1 Then
                Image1.ImageUrl = "~/ShowImage.ashx?id=" & intUserPK
            Else
                Image1.ImageUrl = "~/assets/Avatar/~Avatar-1.png"
            End If

        Else


            Me.txtUserDetailsForename.Visible = False
            Me.txtUserDetailsSurname.Visible = False
            Me.txtUserDetailsPassword.Visible = False
            Me.txtUserDetailsNotes.Visible = False
            Me.imgUpload.Visible = False
            Me.btnSaveImage.Visible = False
            Me.btnDeleteImage.Visible = False
            Me.btnUserStatus.Visible = False
            Me.Image1.Visible = False

            Me.lblHeaderImage.Visible = False
            Me.lblHeaderUpload.Visible = False
            Me.lblHeaderNotes.Visible = False
            Me.lblHeaderPassword.Visible = False
            Me.lblHeaderSurname.Visible = False
            Me.lblHeaderForename.Visible = False



            Me.imgUserDetailsEmailAddress.Visible = True
            Me.imgUserDetailsForename.Visible = False
            Me.imgUserDetailsSurname.Visible = False
            Me.imgUserDetailsPassword.Visible = False



        End If

    End Sub

    Protected Sub txtUserDetailsPassword_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtUserDetailsPassword.PreRender
        Me.txtUserDetailsPassword.Attributes("value") = Me.txtUserDetailsPassword.Text


    End Sub

    Protected Sub btnNewUser_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNewUser.Click
        Me.pnlSearch.Visible = False
        Me.pnlDisplayUser.Visible = True

        LoadUserDetails(0)
        Me.lblUserDetailsID.Text = "--"
    End Sub

    Public Function fnNoSpecialCharacters(ByVal strTest As String) As String

        Dim sb As New System.Text.StringBuilder

        For Each ch As Char In strTest
            If Char.IsLetterOrDigit(ch) OrElse ch = " "c Then
                sb.Append(ch)
            End If
        Next

        strTest = sb.ToString

        Return strTest
    End Function


    Protected Sub txtUserDetailsEmailAddress_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtUserDetailsEmailAddress.TextChanged
        Me.txtUserDetailsEmailAddress.Text = Me.txtUserDetailsEmailAddress.Text.ToLower

        If EmailAddressCheck(Me.txtUserDetailsEmailAddress.Text.ToLower) = True Then

            Dim intCheckedOK As Integer = 0
            Dim intUserPK As Integer = 0
            Dim strTextEntered As String = Me.txtUserDetailsEmailAddress.Text.ToString

            If Me.lblUserDetailsID.Text <> "--" Then

                intUserPK = Me.lblUserDetailsID.Text

                If Core.data_select_value("SELECT count(*) FROM [portal.utilitymasters.co.uk].dbo.tblUsers WHERE varEmail = '" & strTextEntered & "' AND intUserPK <> " & intUserPK) > 0 Then
                    intCheckedOK += 1
                End If

                If intCheckedOK = 0 Then
                    Me.imgUserDetailsEmailAddress.ImageUrl = "~/assets/Metering/accepted_48.png"

                    strNotes = Date.Now() & " - Email changed " & vbCrLf & Left(Me.txtUserDetailsNotes.Text, 300)
                    Core.data_execute_nonquery("UPDATE [portal.utilitymasters.co.uk].dbo.tblUsers SET varNotes = '" & strNotes & "', varEmail = '" & strTextEntered & "' WHERE intUserPK = " & intUserPK)
                Else
                    Me.imgUserDetailsEmailAddress.ImageUrl = "~/assets/Metering/smalcancel.png"
                End If

            Else

                If Core.data_select_value("SELECT count(*) FROM [portal.utilitymasters.co.uk].dbo.tblUsers WHERE varEmail = '" & strTextEntered & "' AND intUserPK <> " & intUserPK) > 0 Then
                    intCheckedOK += 1
                End If

                If intCheckedOK = 0 Then
                    Me.imgUserDetailsEmailAddress.ImageUrl = "~/assets/Metering/accepted_48.png"

                    strNotes = Date.Now() & " - Email added " & vbCrLf & Left(Me.txtUserDetailsNotes.Text, 300)
                    Dim newPK As Integer = Core.data_select_value("INSERT INTO [portal.utilitymasters.co.uk].dbo.tblUsers (varEmail, varNotes) VALUES ('" & strTextEntered & "', '" & strNotes & "'); select scope_identity()")
                    LoadUserDetails(newPK)
                Else
                    Me.imgUserDetailsEmailAddress.ImageUrl = "~/assets/Metering/smalcancel.png"
                End If

            End If
        Else
            Me.imgUserDetailsEmailAddress.ImageUrl = "~/assets/Metering/smalcancel.png"
        End If



    End Sub


    Protected Sub txtUserDetailsForename_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtUserDetailsForename.TextChanged

        Me.txtUserDetailsForename.Text = Trim(Core.fn_NoSpecialCharacters(Me.txtUserDetailsForename.Text))

        If Me.txtUserDetailsForename.Text.Length = 0 Then
            Me.imgUserDetailsForename.ImageUrl = "~/assets/Metering/smalcancel.png"
        Else

            Dim intUserPK As Integer = 0
            If Me.lblUserDetailsID.Text <> "--" Then intUserPK = Me.lblUserDetailsID.Text

            strNotes = Date.Now() & " - Forename changed " & vbCrLf & Left(Me.txtUserDetailsNotes.Text, 300)
            Core.data_execute_nonquery("UPDATE [portal.utilitymasters.co.uk].dbo.tblUsers SET varNotes = '" & strNotes & "', varForename = '" & Me.txtUserDetailsForename.Text & "' WHERE intUserPK = " & intUserPK)

            Me.imgUserDetailsForename.ImageUrl = "~/assets/Metering/accepted_48.png"
            LoadUserDetails(intUserPK)
        End If

        Me.txtUserDetailsSurname.Focus()


    End Sub

    Protected Sub txtUserDetailsSurname_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtUserDetailsSurname.TextChanged

        Me.txtUserDetailsSurname.Text = Trim(Core.fn_NoSpecialCharacters(Me.txtUserDetailsSurname.Text))

        If Me.txtUserDetailsSurname.Text.Length = 0 Then
            Me.imgUserDetailsSurname.ImageUrl = "~/assets/Metering/smalcancel.png"
        Else

            Dim intUserPK As Integer = 0
            If Me.lblUserDetailsID.Text <> "--" Then intUserPK = Me.lblUserDetailsID.Text

            strNotes = Date.Now() & " - Surname changed " & vbCrLf & Left(Me.txtUserDetailsNotes.Text, 300)
            Core.data_execute_nonquery("UPDATE [portal.utilitymasters.co.uk].dbo.tblUsers SET varNotes = '" & strNotes & "', varSurname = '" & Me.txtUserDetailsSurname.Text & "' WHERE intUserPK = " & intUserPK)

            LoadUserDetails(intUserPK)
            Me.imgUserDetailsSurname.ImageUrl = "~/assets/Metering/accepted_48.png"
        End If
    End Sub

    Protected Sub txtUserDetailsPassword_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtUserDetailsPassword.TextChanged
        Me.txtUserDetailsPassword.Text = Trim(Core.fn_NoSpecialCharacters(Me.txtUserDetailsPassword.Text))

        If Me.txtUserDetailsPassword.Text.Length = 0 Then
            Me.imgUserDetailsPassword.ImageUrl = "~/assets/Metering/smalcancel.png"
        Else

            Dim intUserPK As Integer = 0
            If Me.lblUserDetailsID.Text <> "--" Then

                intUserPK = Me.lblUserDetailsID.Text

                strNotes = Date.Now() & " - Password changed " & vbCrLf & Left(Me.txtUserDetailsNotes.Text, 300)
                Core.data_execute_nonquery("UPDATE [portal.utilitymasters.co.uk].dbo.tblUsers SET varNotes = '" & strNotes & "', varPassword = '" & Me.txtUserDetailsPassword.Text & "' WHERE intUserPK = " & intUserPK)

                Me.imgUserDetailsPassword.ImageUrl = "~/assets/Metering/accepted_48.png"
                LoadUserDetails(intUserPK)
            End If

        End If


    End Sub

    Protected Sub btnSaveImage_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveImage.Click


        Dim intUserPK As Integer = 0

        If Me.lblUserDetailsID.Text <> "--" Then intUserPK = Me.lblUserDetailsID.Text

        Dim connection As SqlConnection = Nothing
        Try
            Dim img As FileUpload = CType(imgUpload, FileUpload)
            Dim imgByte As Byte() = Nothing
            If img.HasFile AndAlso Not img.PostedFile Is Nothing Then
                'To create a PostedFile
                Dim File As HttpPostedFile = imgUpload.PostedFile
                'Create byte Array with file len
                imgByte = New Byte(File.ContentLength - 1) {}
                'force the control to load data in array
                File.InputStream.Read(imgByte, 0, File.ContentLength)
            End If
            ' Insert the employee name and image into db
            ' Insert the employee name and image into db
            Dim conn As String = ConfigurationManager.ConnectionStrings("portal.utilitymasters.co.ukConnectionString").ConnectionString
            connection = New SqlConnection(conn)

            connection.Open()
            strNotes = Date.Now() & " - Image added " & vbCrLf & Left(Me.txtUserDetailsNotes.Text, 300)
            Dim sql As String = "UPDATE [portal.utilitymasters.co.uk].dbo.tblUsers SET varNotes = '" & strNotes & "', Image = @eimg WHERE intUserPK = " & intUserPK & "; SELECT " & intUserPK & ""
            Dim cmd As SqlCommand = New SqlCommand(sql, connection)

            cmd.Parameters.AddWithValue("@eimg", imgByte)
            Dim id As Integer = Convert.ToInt32(cmd.ExecuteScalar())
            'lblResult.Text = String.Format("image uploaded ok")

            ' Display the image from the database
            Image1.ImageUrl = "~/ShowImage.ashx?id=" & id
            'Dim sql As String = "UPDATE [portal.utilitymasters.co.uk].dbo.tblUsers SET Image = '" & imgByte.ToString & "' WHERE intUserPK = " & MySession.UserID & "; SELECT " & MySession.UserID & ""
            LoadUserDetails(intUserPK)
            'Dim id As Integer = Core.data_select_value(sql)
            'lblResult.Text = String.Format("Employee ID is {0}", id)
        Catch
            'lblResult.Text = "error"
        Finally
            connection.Close()
        End Try


    End Sub

    Protected Sub btnDeleteImage_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDeleteImage.Click
        Dim intUserPK As Integer = 0

        If Me.lblUserDetailsID.Text <> "--" Then intUserPK = Me.lblUserDetailsID.Text

        If intUserPK > 0 Then
            strNotes = Date.Now() & " - Image deleted " & vbCrLf & Left(Me.txtUserDetailsNotes.Text, 300)
            Core.data_execute_nonquery("UPDATE [portal.utilitymasters.co.uk].dbo.tblUsers SET varNotes = '" & strNotes & "', Image = Null WHERE intUserPK = " & intUserPK)
            Image1.ImageUrl = "~/assets/My_Profile_Silhouette.jpg"
        End If



        LoadUserDetails(intUserPK)

    End Sub

    Protected Sub btnUserStatus_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUserStatus.Click
        Dim intUserPK As Integer = 0

        If Me.lblUserDetailsID.Text <> "--" Then intUserPK = Me.lblUserDetailsID.Text
        If Me.btnUserStatus.Text = "ACTIVE" Then
            strNotes = Date.Now() & " - User disabled " & vbCrLf & Left(Me.txtUserDetailsNotes.Text, 300)
            Core.data_execute_nonquery("UPDATE [portal.utilitymasters.co.uk].dbo.tblUsers SET varNotes = '" & strNotes & "', bitDisabled = 1, dtmModified = getdate(), intUserModifiedBy = " & MySession.UserID & " WHERE intUserPK = " & intUserPK)
        Else
            strNotes = Date.Now() & " - User enabled " & vbCrLf & Left(Me.txtUserDetailsNotes.Text, 300)
            Core.data_execute_nonquery("UPDATE [portal.utilitymasters.co.uk].dbo.tblUsers SET varNotes = '" & strNotes & "', bitDisabled = 0, dtmModified = getdate(), intUserModifiedBy = " & MySession.UserID & " WHERE intUserPK = " & intUserPK)
        End If

        LoadUserDetails(intUserPK)
    End Sub
    Private Sub CleanChart()

        chtDisplayUserAnalysis.Serializer.Reset()

        With Me.chtDisplayUserAnalysis

            Dim series1 As Series
            For Each series1 In .Series
                .Annotations.Clear()
            Next

            .Visible = True
            .Series.Clear()
            .Titles.Clear()
            .Legends.Clear()
            .Legends.Add("Default")

            ' Setup Frame
            .BorderColor = System.Drawing.ColorTranslator.FromHtml("#009530") ' Corp Green
            .BorderStyle = ChartDashStyle.Solid
            .BorderWidth = 2
            .BorderSkin.SkinStyle = BorderSkinStyle.Emboss
            'Setup Toolbar
            .UI.Toolbar.Enabled = True
            .UI.Toolbar.Docking = ToolbarDocking.Top
            .UI.Toolbar.Placement = ToolbarPlacement.InsideChart
            .UI.Toolbar.BorderSkin.SkinStyle = BorderSkinStyle.Emboss
            .UI.Toolbar.BorderSkin.FrameBackColor = System.Drawing.ColorTranslator.FromHtml("#6EBB1F") ' Corp Green

            Dim commands As CommandCollection = chtDisplayUserAnalysis.UI.Commands

            Dim cmdProperties As Command = commands.FindCommand(ChartCommandType.Properties)
            Dim cmdPalette As Command = commands.FindCommand(ChartCommandType.SelectPaletteGroup)
            Dim cmdChartType As Command = commands.FindCommand(ChartCommandType.SelectChartGroup)
            Dim cmdToggle3D As Command = commands.FindCommand(ChartCommandType.Toggle3D)

            cmdProperties.Visible = False
            cmdPalette.Visible = False
            cmdChartType.Visible = False
            cmdToggle3D.Visible = False

            Me.chtDisplayUserAnalysis.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFFFFF") ' Corp Green
        End With

        chtDisplayUserAnalysis.ChartAreas.Add("Default")
        'Chart1.Series.Add(0)
        Me.chtDisplayUserAnalysis.Width = "600"
        Me.chtDisplayUserAnalysis.Height = "450"


    End Sub
    Private Sub GenerateUserAnalysisChart(ByVal intUserPK As Integer)

        CleanChart()

        Dim strSQL As String
        strSQL = ""
        strSQL = " EXEC [portal.utilitymasters.co.uk].dbo.usp_Admin_UsersLogin_DailyAnalysisbyUser " & intUserPK

        With chtDisplayUserAnalysis
            .Visible = True
            .Series.Clear()
            .Titles.Clear()

            .DataSource = Core.data_select(strSQL)
            .DataBind()
            .Series.Add("Default")
            .Series(0).ValueMemberX = "dtmDate"
            .Series(0).ValueMembersY = "intTotalLogins"
            .Series(0).Type = Dundas.Charting.WebControl.SeriesChartType.Line

            .Legends(0).Enabled = False

            .Titles.Add("Daily Login Analysis")

            ' Enable 3D charts
            '.ChartAreas(0).Area3DStyle.Enable3D = True
            '.ChartAreas(0).Area3DStyle.YAngle = 5
            '.ChartAreas(0).Area3DStyle.PointDepth = 50
            '.ChartAreas(0).Area3DStyle.PointGapDepth = 0

            '.ChartAreas.Add("Default")
            .ChartAreas(0).BackColor = Color.Silver
            .ChartAreas(0).AxisY.LabelStyle.Format = "#,##0"
            .ChartAreas(0).AxisX.Margin = False

            .BorderColor = Color.Black
            .BorderStyle = ChartDashStyle.Solid
            .BorderWidth = 2
            .BorderSkin.SkinStyle = BorderSkinStyle.Emboss

            .UI.Toolbar.Enabled = True
            .UI.Toolbar.Docking = ToolbarDocking.Top
            .UI.Toolbar.Placement = ToolbarPlacement.InsideChart
            .UI.Toolbar.BorderSkin.SkinStyle = BorderSkinStyle.Emboss
            .UI.Toolbar.BorderSkin.FrameBackColor = Color.Black

        End With


    End Sub



    Protected Sub btnShowUserDetailsPanel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnShowUserDetailsPanel.Click

        Me.pnlDisplayUserMainDetails.Visible = False
        Me.btnShowUserDetailsPanel.BackColor = Nothing

        Me.pnlDisplayUserCompanies.Visible = False
        Me.btnShowUserCompanies.BackColor = Nothing

        Me.pnlDisplayUserReports.Visible = False
        Me.btnShowUserReports.BackColor = Nothing

        Me.pnlDisplayUserAnalysis.Visible = False
        Me.btnShowUserAnalysis.BackColor = Nothing


        Me.pnlDisplayUserMainDetails.Visible = True
        Me.btnShowUserDetailsPanel.BackColor = Drawing.Color.LimeGreen

        Dim intUserPK As Integer = 0

        If Me.lblUserDetailsID.Text <> "--" Then intUserPK = Me.lblUserDetailsID.Text
        LoadUserDetails(intUserPK)
    End Sub



    Protected Sub btnShowUserCompanies_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnShowUserCompanies.Click

        Me.pnlDisplayUserMainDetails.Visible = False
        Me.btnShowUserDetailsPanel.BackColor = Nothing

        Me.pnlDisplayUserCompanies.Visible = False
        Me.btnShowUserCompanies.BackColor = Nothing

        Me.pnlDisplayUserReports.Visible = False
        Me.btnShowUserReports.BackColor = Nothing

        Me.pnlDisplayUserAnalysis.Visible = False
        Me.btnShowUserAnalysis.BackColor = Nothing

        Me.pnlDisplayUserCompanies.Visible = True
        Me.btnShowUserCompanies.BackColor = Drawing.Color.LimeGreen

    End Sub


    Protected Sub btnShowUserAnalysis_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnShowUserAnalysis.Click

        Me.pnlDisplayUserMainDetails.Visible = False
        Me.btnShowUserDetailsPanel.BackColor = Nothing

        Me.pnlDisplayUserCompanies.Visible = False
        Me.btnShowUserCompanies.BackColor = Nothing

        Me.pnlDisplayUserReports.Visible = False
        Me.btnShowUserReports.BackColor = Nothing

        Me.pnlDisplayUserAnalysis.Visible = False
        Me.btnShowUserAnalysis.BackColor = Nothing


        Me.pnlDisplayUserAnalysis.Visible = True
        Me.btnShowUserAnalysis.BackColor = Drawing.Color.LimeGreen



        Dim intUserPK As Integer = 0

        If Me.lblUserDetailsID.Text <> "--" Then intUserPK = Me.lblUserDetailsID.Text
        GenerateUserAnalysisChart(intUserPK)

        Dim datelogginin As DateTime = Core.data_select_value("SELECT TOP 1 dtmCreated FROM [portal.utilitymasters.co.uk].dbo.tblUsersLoginAttempts WHERE intUserFK = " & intUserPK & " AND varOutcome = 'Success' ORDER BY dtmCreated DESC")
        Dim datenow As DateTime = Date.Now

        Dim dayssincelogin As Integer = DateDiff(DateInterval.Day, datelogginin, datenow)


        Me.lblUserAnalysisLastLoggedIn.Text = "Last logged in on: " & datelogginin & " (" & dayssincelogin & " days ago)"

    End Sub

End Class
