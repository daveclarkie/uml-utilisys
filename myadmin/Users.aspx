﻿<%@ Page Title="" Language="VB" MasterPageFile="~/landing.master" AutoEventWireup="false" CodeFile="Users.aspx.vb" Inherits="myadmin_Users" %>
<%@ Register Assembly="DundasWebChart" Namespace="Dundas.Charting.WebControl" TagPrefix="DCWC" %>

<asp:Content ID="Content1" ContentPlaceHolderID="content_right" Runat="Server">

    <div class="content_back my_admin">
        <form id="frmUser" runat="server"> 
            <asp:Panel ID="pnlSearch" runat="server" Height="650px" Width="650px" ScrollBars="none">
                There are currently <asp:Label ID="lblCountUsers" runat="server"></asp:Label> active users registered on the system.<br />

                <asp:Label ID="lblCountUsers_24Hours" runat="server"></asp:Label> have logged-in in the last 24 hours.<br />

                <asp:Label ID="lblCountUsers_7Days" runat="server"></asp:Label> have logged-in in the last 7 days.<br />
                <br />
                <asp:TextBox ID="txtSearchCriteria" runat="server" Width="250px"></asp:TextBox>
                <asp:Button ID="btnSearchCriteria" runat="server" Text="search" />
                <asp:Button ID="btnSearchCriteriaReset" runat="server" Text="reset" />
                <asp:Label ID="lblSearchCriteriaMatches" runat="server" Text="0"></asp:Label> matches
                <br />
                <asp:Button ID="btnNewUser" runat="server" Text="new user" />
                <br />
                <asp:Panel ID="pnlSearchResults" runat="server" Height="450px" Width="650px" ScrollBars="Vertical">
                    <asp:GridView borderwidth="0px" ID="grdTable" cssclass="users" runat="server" AutoGenerateColumns="False" AutoGenerateSelectButton="true"></asp:GridView>
                </asp:Panel>
            </asp:Panel>


            <asp:Panel ID="pnlDisplayUser" runat="server" Height="650px" Width="800px" ScrollBars="none" Visible="false">


                <table width="650px">
                    <tr>
                        <td>
                            <asp:Button ID="btnCloseDisplayUser" runat="server" Height="50" Width="100" Text="back" />
                        </td>
                        <td>
                            <asp:Button ID="btnShowUserDetailsPanel" runat="server" Height="50" Width="100" Text="User Details" BackColor="LimeGreen" />
                        </td>
                        <td>
                            <asp:Button ID="btnShowUserCompanies" runat="server" Height="50" Width="100" Text="Companies" />
                        </td>
                        <td>
                            <asp:Button ID="btnShowUserReports" runat="server" Height="50" Width="100" Text="Reports" />
                        </td>
                        <td>
                            <asp:Button ID="btnShowUserAnalysis" runat="server" Height="50" Width="100" Text="Analysis" />
                        </td>
                    </tr>
                </table>

                <asp:Panel id="pnlDisplayUserMainDetails" runat="server" Height="550px" Width="650px" BorderStyle="Solid" >
                            <asp:Table ID="tblDisplayUserMainDetails" runat="server" CellPadding="10" Width="100%">
                                <asp:TableRow>
                                    <asp:TableCell></asp:TableCell>
                                    <asp:TableCell height="10px" ColumnSpan="2">
                                        <b>User Details</b>
                                    </asp:TableCell>
                                </asp:TableRow>
                            
                                <asp:TableRow>
                                    <asp:TableCell height="10px">
                                    
                                    </asp:TableCell>
                                </asp:TableRow>
                            
                                <asp:TableRow>
                                    <asp:TableCell>
                                    
                                    </asp:TableCell>
                                    <asp:TableCell Font-Bold="true" Width="100px">
                                        <asp:Label ID="lblHeaderID" runat="server" Text="ID"></asp:Label>
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:Label ID="lblUserDetailsID" runat="server"></asp:Label>
                                    </asp:TableCell>
                                </asp:TableRow>

                                <asp:TableRow>
                                    <asp:TableCell>

                                    </asp:TableCell>
                                    <asp:TableCell Font-Bold="true">
                                        <asp:Label ID="lblHeaderEmail" runat="server" Text="Email"></asp:Label>
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:TextBox ID="txtUserDetailsEmailAddress" AutoPostBack="true" runat="server" Width="250px"></asp:TextBox>
                                        &nbsp;<asp:Image ID="imgUserDetailsEmailAddress" runat="server" ImageUrl="~/assets/Metering/warning_48.png" Height="18" Width="18" />
                                    </asp:TableCell>
                                </asp:TableRow>

                                <asp:TableRow>
                                    <asp:TableCell Width="20px">
                                    
                                    </asp:TableCell>
                                    <asp:TableCell Font-Bold="true">
                                        <asp:Label ID="lblHeaderForename" runat="server" Text="Forename"></asp:Label>
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:TextBox ID="txtUserDetailsForename" AutoPostBack="true" runat="server" Width="300px"></asp:TextBox>
                                        &nbsp;<asp:Image ID="imgUserDetailsForename" runat="server" ImageUrl="~/assets/Metering/warning_48.png" Height="18" Width="18" />
                                    </asp:TableCell>
                                </asp:TableRow>

                                <asp:TableRow>
                                    <asp:TableCell>

                                    </asp:TableCell>
                                    <asp:TableCell Font-Bold="true">
                                        <asp:Label ID="lblHeaderSurname" runat="server" Text="Surname"></asp:Label>
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:TextBox ID="txtUserDetailsSurname" AutoPostBack="true" runat="server" Width="300px"></asp:TextBox>
                                        &nbsp;<asp:Image ID="imgUserDetailsSurname" runat="server" ImageUrl="~/assets/Metering/warning_48.png" Height="18" Width="18" />
                                    </asp:TableCell>
                                </asp:TableRow>

                                <asp:TableRow>
                                    <asp:TableCell>

                                    </asp:TableCell>
                                    <asp:TableCell Font-Bold="true">
                                        <asp:Label ID="lblHeaderPassword" runat="server" Text="Password"></asp:Label>
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:TextBox ID="txtUserDetailsPassword" AutoPostBack="true" runat="server" TextMode="SingleLine" Width="300px"></asp:TextBox>
                                        <asp:Label ID="lblUserDetailsPassword" runat="server" Visible="false" Width="300px"></asp:Label>
                                    
                                        &nbsp;<asp:Image ID="imgUserDetailsPassword" runat="server" ImageUrl="~/assets/Metering/warning_48.png" Height="18" Width="18" />
                                    </asp:TableCell>
                                </asp:TableRow>
                            
                                <asp:TableRow>
                                    <asp:TableCell>

                                    </asp:TableCell>
                                    <asp:TableCell Font-Bold="true" VerticalAlign="Top">
                                        <asp:Label ID="lblHeaderNotes" runat="server" Text="Notes"></asp:Label>
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:TextBox id="txtUserDetailsNotes" runat="server" TextMode="MultiLine" Height="70" Width="450"></asp:TextBox>
                                    </asp:TableCell>
                                </asp:TableRow>

                            
                                <asp:TableRow>
                                    <asp:TableCell>

                                    </asp:TableCell>
                                    <asp:TableCell Font-Bold="true" VerticalAlign="Top">
                                        <asp:Label ID="lblHeaderUpload" runat="server" Text="Upload"></asp:Label>
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:FileUpload ID="imgUpload" runat="server" />
                                        <asp:Button ID="btnSaveImage" runat="server" Text="save" Width="80px" />
                                        <asp:Button ID="btnDeleteImage" runat="server" Text="delete" Width="80px" />
                                    </asp:TableCell>
                                </asp:TableRow>
                                
                                <asp:TableRow>
                                    <asp:TableCell>

                                    </asp:TableCell>
                                    <asp:TableCell Font-Bold="true" VerticalAlign="Top">
                                        <asp:Label ID="lblHeaderImage" runat="server" Text="Image"></asp:Label>
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:Image ID="Image1" style="width:132px; height:170px;" Runat="server" BorderStyle="Solid" />
                                    </asp:TableCell>
                                </asp:TableRow>

                                <asp:TableRow>
                                    <asp:TableCell ColumnSpan="3" HorizontalAlign="Center" >
                                        <asp:Button ID="btnUserStatus" runat="server"  Width="100%" Height="75px" ForeColor="White" BackColor="LimeGreen" text="ACTIVE" Font-Bold="true" />
                                    </asp:TableCell>
                                </asp:TableRow>

                            </asp:Table>
            
                            
                        </asp:Panel>
                        
                <asp:Panel id="pnlDisplayUserCompanies" runat="server" Height="550px" Width="650px" BorderStyle="Solid" Visible="false" >
                            <asp:Table ID="Table1" runat="server" CellPadding="10">
                                <asp:TableRow>
                                    <asp:TableCell></asp:TableCell>
                                    <asp:TableCell height="10px" ColumnSpan="2">
                                        <b>Companies</b>
                                    </asp:TableCell>
                                </asp:TableRow>
                            
                                <asp:TableRow>
                                    <asp:TableCell height="10px">
                                    
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </asp:Panel>
                
                <asp:Panel id="pnlDisplayUserReports" runat="server" Height="550px" Width="650px" BorderStyle="Solid" Visible="false" >
                    132                
                            
                </asp:Panel>

                <asp:Panel id="pnlDisplayUserAnalysis" runat="server" Height="550px" Width="650px" BorderStyle="Solid" Visible="false" >

                <asp:Table ID="Table2" runat="server" CellPadding="10">
                                <asp:TableRow>
                                    <asp:TableCell></asp:TableCell>
                                    <asp:TableCell height="10px" ColumnSpan="2">
                                        <b>Analysis</b>
                                    </asp:TableCell>
                                </asp:TableRow>
                            
                                <asp:TableRow>
                                    <asp:TableCell height="10px">
                                    
                                    </asp:TableCell>
                                </asp:TableRow>
                            
                                <asp:TableRow>
                                    <asp:TableCell></asp:TableCell>
                                    <asp:TableCell >
                                        <asp:Label ID="lblUserAnalysisLastLoggedIn" runat="server" ></asp:Label>
                                    </asp:TableCell>
                                </asp:TableRow>

                                <asp:TableRow>
                                    <asp:TableCell></asp:TableCell>
                                    <asp:TableCell >
                                        <DCWC:Chart ID="chtDisplayUserAnalysis" Width="600" Height="450" runat="server" BackColor="#FFFFC0" BorderLineColor="Black" BorderLineStyle="Solid" BorderLineWidth="2" Palette="Pastel" >
                                        </DCWC:Chart>
                                    </asp:TableCell>
                                </asp:TableRow>



                            </asp:Table>
                    
                    

                    
                            
                </asp:Panel>




                

            </asp:Panel>


        </form>
    </div>

</asp:Content>

