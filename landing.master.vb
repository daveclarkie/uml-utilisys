
Partial Class MasterPage
    Inherits System.Web.UI.MasterPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim strURL As String = Core.CheckForNullString(Me.Page.Request.Url.PathAndQuery.ToString)

            Me.navMySites.Visible = False
            Me.navMyReports.Visible = False
            Me.navMyAccount.Visible = False
            Me.navMyApps.Visible = False
            Me.navAdmin.Visible = False
            Me.navMyHelp.Visible = False

            'display left navigation control depending on URL
            If strURL.Contains("/my-sites.aspx") Then
                Me.navMySites.Visible = True
            ElseIf strURL.Contains("/mysites/") Then
                Me.navMySites.Visible = True
            ElseIf strURL.Contains("/my-reports.aspx") Then
                Me.navMyReports.Visible = True
            ElseIf strURL.Contains("/my-account.aspx") Then
                Me.navMyAccount.Visible = True
            ElseIf strURL.Contains("/user") Then
                Me.navAdmin.Visible = True
            ElseIf strURL.Contains("/reports") Then
                Me.navMyReports.Visible = True
            ElseIf strURL.Contains("/maintenance.aspx") Then
                Me.navAdmin.Visible = True
            ElseIf strURL.Contains("/my-apps.aspx") Then
                Me.navMyApps.Visible = True
            ElseIf strURL.Contains("/myapps/") Then
                Me.navMyApps.Visible = True
            ElseIf strURL.Contains("/myadmin") Then
                Me.navAdmin.Visible = True
            ElseIf strURL.Contains("/my-help.aspx") Then
                Me.navMyHelp.Visible = True
            ElseIf strURL.Contains("/myhelp/") Then
                Me.navMyHelp.Visible = True
            End If

            If Me.navMyReports.Visible Then
                'LoadReports()
            End If

        Catch ex As Exception
            Throw
        End Try
    End Sub


    




End Class

