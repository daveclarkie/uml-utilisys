Partial Class my_apps
    Inherits System.Web.UI.Page

    Private _mintCompanyID As Integer = 0

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            If Not IIf(Core.data_select_value("select bitAllowMyApps from tblUsers where intUserPK = " & MySession.UserID) = "True", True, False) Then
                Response.Redirect(Core.data_select_value("select RawHomeURL from vwUsers where ID = " & MySession.UserID), True)
            End If

            Dim description As HtmlMeta = New HtmlMeta()
            description.Name = "description"
            description.Content = "UTILISYS - My Apps"
            Dim keywords As HtmlMeta = New HtmlMeta()
            keywords.Name = "keywords"
            keywords.Content = "UTILISYS - My Apps"
            Page.Header.Controls.Add(description)
            Page.Header.Controls.Add(keywords)

            LoadApps()

            If Not Request.QueryString("custid") Is Nothing Then Me._mintCompanyID = Request.QueryString("custid")
        Catch ex As Exception
            '
        End Try
    End Sub



    Private Sub LoadApps()

        Me.ulBox.InnerHtml = ""

        Dim strSQL As String = "EXEC spReports_ListReports " & MySession.UserID & ", 14"

        Using connection As New SqlConnection(Core.ConnectionString)
            connection.Open()
            Dim command As New SqlCommand(strSQL, connection)
            command.CommandType = CommandType.Text
            Dim reader As SqlDataReader = command.ExecuteReader()

            While reader.Read

                Dim strUrl As String
                If reader("intReportDisabled") = 0 Then
                    strUrl = reader("varOnlineURL").ToString
                    Me.ulBox.InnerHtml &= "<li><a id='lnkReports" & reader("intReportPK") & "' class='" & reader("varCSS") & "' runat='server' title='" & reader("varReportName") & "' href='" & strUrl & "' ></a></li>"
                End If




            End While

        End Using

        'core.data_Reader(strSQL)

    End Sub

End Class
