<%@ Control Language="VB" AutoEventWireup="false" CodeFile="nav-my-reports.ascx.vb" Inherits="nav_my_reports" %>

<div id="left_nav_reports" class="left_nav">
    <div>
        <asp:hyperlink runat="server" id="lnkMySites" navigateurl="~/my-reports.aspx" cssclass="nav_home" tooltip="Everything about your sites" />
        <h2 class="nav_heading"><asp:hyperlink runat="server" id="lnkReportsHome" ForeColor="#009530"  navigateurl="~/reports/MyReports/Default.aspx" tooltip="Your reports" Text="My Reports Home"></asp:hyperlink></h2>
        <ul class="nav" runat="server" id="nav"></ul>
        <ul class="styleformytreeview" runat="server" id="navOnlineReports"></ul>
        <asp:Literal ID="litNewFormatMenu" runat="server"></asp:Literal>
    </div>

    <asp:Panel ID="pnlCRCEvidencePackMenu" runat="server">
        <table border="0" cellpadding="0" cellspacing="0" style="height:100%; width:240px; ">
        <tr>
            <td valign="top">
                <table id="mastertable" class="mastertable" border="0" cellspacing="1" cellpadding="0" style="height:100%; " bgcolor="white">

   
                    <tr>
	                    <td valign="top">
    		                <div id="masterdiv">

                                <!-- Heading 1-->
  			                    <div class="Menu" onclick="SwitchMenu('sub2')">
				                    <img id="A2" style="vertical-align: middle" width="20" height="20" src="../../../../../../../../assets/icons/open.gif" border="0" hspace="3" />
				                    <span class="tag">2</span> &nbsp;&nbsp; Summary of key emissions data 
			                    </div>
          
		                        <div class="Options" id="sub2" style="display:block;">
		                            <div class="Option" onmouseover="this.style.background=sub_menu_back_color_over" onmouseout="this.style.background=sub_menu_back_color_out">
						                <img style="vertical-align: middle" width="16" height="16" src="../../../../../../../../assets/icons/textfolder.gif" border="0" hspace="3" />
						                <span class="tag">2.1</span> &nbsp;&nbsp; <asp:HyperLink ID="sub2_1" runat="server" Text="Qualification data" NavigateUrl="~/reports/CarbonReports/CRCManagementArea/CRCEvidencePack/Pack/CCA_Page21.asp" Target="evidencepack"></asp:HyperLink>
                		            </div>
                                    
                                    <div class="Option" onmouseover="this.style.background=sub_menu_back_color_over" onmouseout="this.style.background=sub_menu_back_color_out">
						                <img style="vertical-align: middle" width="16" height="16" src="../../../../assets/icons/txtfolder.gif" border="0" hspace="3" />
						                <span class="tag">2.2</span> &nbsp;&nbsp; <asp:HyperLink ID="sub2_2" runat="server" Text="Registration" NavigateUrl="~/reports/CarbonReports/CRCManagementArea/CRCEvidencePack/Pack/CCA_Page22.asp" Target="evidencepack"></asp:HyperLink>
                		            </div>
                                    
                                    <div class="Option" onmouseover="this.style.background=sub_menu_back_color_over" onmouseout="this.style.background=sub_menu_back_color_out">
						                <img style="vertical-align: middle" width="16" height="16" src="../../../../assets/icons/txtfolder.gif" border="0" hspace="3" />
						                <span class="tag">2.3</span> &nbsp;&nbsp; <asp:HyperLink ID="sub2_3" runat="server" Text="Footprint report" NavigateUrl="~/reports/CarbonReports/CRCManagementArea/CRCEvidencePack/Pack/CCA_Page23.asp" Target="evidencepack"></asp:HyperLink>
                		            </div>



                                </div>


                                <!-- Heading 2-->
                                <div class="Menu" onclick="SwitchMenu('sub3')">
                                    <img id="A3" style="vertical-align: middle" width="20" height="20" src="../../../../assets/icons/open.gif" border="0" hspace="3" />
                                    <span class="tag">3</span> &nbsp;&nbsp; Organisational structure
                                </div>

                                <div class="Options" id="sub3">
                                    
                                    <div class="Option" onmouseover="this.style.background=sub_menu_back_color_over" onmouseout="this.style.background=sub_menu_back_color_out">
						                <img style="vertical-align: middle" width="16" height="16" src="../../../../assets/icons/txtfolder.gif" border="0" hspace="3" />
						                <span class="tag">3.1</span> &nbsp;&nbsp; <asp:HyperLink ID="sub3_1" runat="server" Text="Organisation family tree" NavigateUrl="~/reports/CarbonReports/CRCManagementArea/CRCEvidencePack/Pack/CCA_Page31.asp" Target="evidencepack"></asp:HyperLink>
                		            </div>
                                    
                                    <div class="Option" onmouseover="this.style.background=sub_menu_back_color_over" onmouseout="this.style.background=sub_menu_back_color_out">
						                <img style="vertical-align: middle" width="16" height="16" src="../../../../assets/icons/txtfolder.gif" border="0" hspace="3" />
						                <span class="tag">3.2.1</span> &nbsp;&nbsp; <asp:HyperLink ID="sub3_2_1" runat="server" Text="Structure and relationships" NavigateUrl="~/reports/CarbonReports/CRCManagementArea/CRCEvidencePack/Pack/CCA_Page321.asp" Target="evidencepack"></asp:HyperLink>
                		            </div>
                                    
                                    <div class="Option" onmouseover="this.style.background=sub_menu_back_color_over" onmouseout="this.style.background=sub_menu_back_color_out">
						                <img style="vertical-align: middle" width="16" height="16" src="../../../../assets/icons/txtfolder.gif" border="0" hspace="3" />
						                <span class="tag">3.2.2</span> &nbsp;&nbsp; <asp:HyperLink ID="sub3_2_2" runat="server" Text="Structure and relationships" NavigateUrl="~/reports/CarbonReports/CRCManagementArea/CRCEvidencePack/Pack/CCA_Page322.asp" Target="evidencepack"></asp:HyperLink>
                		            </div>
                                    
                                    <div class="Option" onmouseover="this.style.background=sub_menu_back_color_over" onmouseout="this.style.background=sub_menu_back_color_out">
						                <img style="vertical-align: middle" width="16" height="16" src="../../../../assets/icons/txtfolder.gif" border="0" hspace="3" />
						                <span class="tag">3.2.3</span> &nbsp;&nbsp; <asp:HyperLink ID="sub3_2_3" runat="server" Text="Structure and relationships" NavigateUrl="~/reports/CarbonReports/CRCManagementArea/CRCEvidencePack/Pack/CCA_Page323.asp" Target="evidencepack"></asp:HyperLink>
                		            </div>
                                </div>


                                <!-- Heading 3-->
                                <div class="Menu" onclick="SwitchMenu('sub4')">
                                    <img id="A4" style="vertical-align: middle" width="20" height="20" src="../../../../assets/icons/open.gif" border="0" hspace="3" />
                                    <span class="tag">4</span> &nbsp;&nbsp; Responsibilities
                                </div>

                                <div class="Options" id="sub4">
                                    
                                    <div class="Option" onmouseover="this.style.background=sub_menu_back_color_over" onmouseout="this.style.background=sub_menu_back_color_out">
						                <img style="vertical-align: middle" width="16" height="16" src="../../../../assets/icons/txtfolder.gif" border="0" hspace="3" />
						                <span class="tag">4.1</span> &nbsp;&nbsp; <asp:HyperLink ID="sub4_1" runat="server" Text="Nominated senior officer" NavigateUrl="~/reports/CarbonReports/CRCManagementArea/CRCEvidencePack/Pack/CCA_Page41.asp" Target="evidencepack"></asp:HyperLink>
                		            </div>

                                    <div class="Option" onmouseover="this.style.background=sub_menu_back_color_over" onmouseout="this.style.background=sub_menu_back_color_out">
						                <img style="vertical-align: middle" width="16" height="16" src="../../../../assets/icons/txtfolder.gif" border="0" hspace="3" />
						                <span class="tag">4.2</span> &nbsp;&nbsp; <asp:HyperLink ID="sub4_2" runat="server" Text="CRC team members" NavigateUrl="~/reports/CarbonReports/CRCManagementArea/CRCEvidencePack/Pack/CCA_Page42.asp" Target="evidencepack"></asp:HyperLink>
                		            </div>

                                    <div class="Option" onmouseover="this.style.background=sub_menu_back_color_over" onmouseout="this.style.background=sub_menu_back_color_out">
						                <img style="vertical-align: middle" width="16" height="16" src="../../../../assets/icons/txtfolder.gif" border="0" hspace="3" />
						                <span class="tag">4.3</span> &nbsp;&nbsp; <asp:HyperLink ID="sub4_3" runat="server" Text="Organisation arrangements" NavigateUrl="~/reports/CarbonReports/CRCManagementArea/CRCEvidencePack/Pack/CCA_Page43.asp" Target="evidencepack"></asp:HyperLink>
                		            </div>

                                    <div class="Option" onmouseover="this.style.background=sub_menu_back_color_over" onmouseout="this.style.background=sub_menu_back_color_out">
						                <img style="vertical-align: middle" width="16" height="16" src="../../../../assets/icons/txtfolder.gif" border="0" hspace="3" />
						                <span class="tag">4.4</span> &nbsp;&nbsp; <asp:HyperLink ID="sub4_4" runat="server" Text="CRC written procedures" NavigateUrl="~/reports/CarbonReports/CRCManagementArea/CRCEvidencePack/Pack/CCA_Page44.asp" Target="evidencepack"></asp:HyperLink>
                		            </div>
                                </div>


                                <!-- Heading 4-->
                                <div class="Menu" onclick="SwitchMenu('sub5')">
                                    <img id="A5" style="vertical-align: middle" width="20" height="20" src="../../../../assets/icons/open.gif" border="0" hspace="3" />
                                    <span class="tag">5</span> &nbsp;&nbsp; Emissions sources and source list
                                </div>

                                <div class="Options" id="sub5">

                                    <div class="Option" onmouseover="this.style.background=sub_menu_back_color_over" onmouseout="this.style.background=sub_menu_back_color_out">
						                <img style="vertical-align: middle" width="16" height="16" src="../../../../assets/icons/txtfolder.gif" border="0" hspace="3" />
						                <span class="tag">5.1.1</span> &nbsp;&nbsp; <asp:HyperLink ID="sub5_1_1" runat="server" Text="Source list" NavigateUrl="~/reports/CarbonReports/CRCManagementArea/CRCEvidencePack/Pack/CCA_Page511.asp" Target="evidencepack"></asp:HyperLink>
                		            </div>

                                    <div class="Option" onmouseover="this.style.background=sub_menu_back_color_over" onmouseout="this.style.background=sub_menu_back_color_out">
						                <img style="vertical-align: middle" width="16" height="16" src="../../../../assets/icons/txtfolder.gif" border="0" hspace="3" />
						                <span class="tag">5.1.2</span> &nbsp;&nbsp; <asp:HyperLink ID="sub5_1_2" runat="server" Text="Source list" NavigateUrl="~/reports/CarbonReports/CRCManagementArea/CRCEvidencePack/Pack/CCA_Page512.asp" Target="evidencepack"></asp:HyperLink>
                		            </div>

                                    <div class="Option" onmouseover="this.style.background=sub_menu_back_color_over" onmouseout="this.style.background=sub_menu_back_color_out">
						                <img style="vertical-align: middle" width="16" height="16" src="../../../../assets/icons/txtfolder.gif" border="0" hspace="3" />
						                <span class="tag">5.1.3</span> &nbsp;&nbsp; <asp:HyperLink ID="sub5_1_3" runat="server" Text="Source list" NavigateUrl="~/reports/CarbonReports/CRCManagementArea/CRCEvidencePack/Pack/CCA_Page513.asp" Target="evidencepack"></asp:HyperLink>
                		            </div>

                                    <div class="Option" onmouseover="this.style.background=sub_menu_back_color_over" onmouseout="this.style.background=sub_menu_back_color_out">
						                <img style="vertical-align: middle" width="16" height="16" src="../../../../assets/icons/txtfolder.gif" border="0" hspace="3" />
						                <span class="tag">5.1.4</span> &nbsp;&nbsp; <asp:HyperLink ID="sub5_1_4" runat="server" Text="Source list" NavigateUrl="~/reports/CarbonReports/CRCManagementArea/CRCEvidencePack/Pack/CCA_Page514.asp" Target="evidencepack"></asp:HyperLink>
                		            </div>

                                    <div class="Option" onmouseover="this.style.background=sub_menu_back_color_over" onmouseout="this.style.background=sub_menu_back_color_out">
						                <img style="vertical-align: middle" width="16" height="16" src="../../../../assets/icons/txtfolder.gif" border="0" hspace="3" />
						                <span class="tag">5.1.5</span> &nbsp;&nbsp; <asp:HyperLink ID="sub5_1_5" runat="server" Text="Source list" NavigateUrl="~/reports/CarbonReports/CRCManagementArea/CRCEvidencePack/Pack/CCA_Page515.asp" Target="evidencepack"></asp:HyperLink>
                		            </div>

                                    <div class="Option" onmouseover="this.style.background=sub_menu_back_color_over" onmouseout="this.style.background=sub_menu_back_color_out">
						                <img style="vertical-align: middle" width="16" height="16" src="../../../../assets/icons/txtfolder.gif" border="0" hspace="3" />
						                <span class="tag">5.1.6</span> &nbsp;&nbsp; <asp:HyperLink ID="sub5_1_6" runat="server" Text="Source list" NavigateUrl="~/reports/CarbonReports/CRCManagementArea/CRCEvidencePack/Pack/CCA_Page516.asp" Target="evidencepack"></asp:HyperLink>
                		            </div>
                                </div>


                                <!-- Heading 5-->
                                <div class="Menu" onclick="SwitchMenu('sub6')">
                                    <img id="A6" style="vertical-align: middle" width="20" height="20" src="../../../../assets/icons/open.gif" border="0" hspace="3" />
                                    <span class="tag">6</span> &nbsp;&nbsp; Footprint report
                                </div>

                                <div class="Options" id="sub6">

                                    <div class="Option" onmouseover="this.style.background=sub_menu_back_color_over" onmouseout="this.style.background=sub_menu_back_color_out">
						                <img style="vertical-align: middle" width="16" height="16" src="../../../../assets/icons/txtfolder.gif" border="0" hspace="3" />
						                <span class="tag">6.1.1</span> &nbsp;&nbsp; <asp:HyperLink ID="sub6_1_1" runat="server" Text="Phase coverage" NavigateUrl="~/reports/CarbonReports/CRCManagementArea/CRCEvidencePack/Pack/CCA_Page611.asp" Target="evidencepack"></asp:HyperLink>
                		            </div>

                                    <div class="Option" onmouseover="this.style.background=sub_menu_back_color_over" onmouseout="this.style.background=sub_menu_back_color_out">
						                <img style="vertical-align: middle" width="16" height="16" src="../../../../assets/icons/txtfolder.gif" border="0" hspace="3" />
						                <span class="tag">6.1.2</span> &nbsp;&nbsp; <asp:HyperLink ID="sub6_1_2" runat="server" Text="Phase coverage" NavigateUrl="~/reports/CarbonReports/CRCManagementArea/CRCEvidencePack/Pack/CCA_Page612.asp" Target="evidencepack"></asp:HyperLink>
                		            </div>

                                    <div class="Option" onmouseover="this.style.background=sub_menu_back_color_over" onmouseout="this.style.background=sub_menu_back_color_out">
						                <img style="vertical-align: middle" width="16" height="16" src="../../../../assets/icons/txtfolder.gif" border="0" hspace="3" />
						                <span class="tag">6.2.1</span> &nbsp;&nbsp; <asp:HyperLink ID="sub6_2_1" runat="server" Text="Total footprint" NavigateUrl="~/reports/CarbonReports/CRCManagementArea/CRCEvidencePack/Pack/CCA_Page621.asp" Target="evidencepack"></asp:HyperLink>
                		            </div>

                                    <div class="Option" onmouseover="this.style.background=sub_menu_back_color_over" onmouseout="this.style.background=sub_menu_back_color_out">
						                <img style="vertical-align: middle" width="16" height="16" src="../../../../assets/icons/txtfolder.gif" border="0" hspace="3" />
						                <span class="tag">6.3</span> &nbsp;&nbsp; <asp:HyperLink ID="sub6_3" runat="server" Text="CCA emissions" NavigateUrl="~/reports/CarbonReports/CRCManagementArea/CRCEvidencePack/Pack/CCA_Page63.asp" Target="evidencepack"></asp:HyperLink>
                		            </div>
                                    
                                    <div class="Option" onmouseover="this.style.background=sub_menu_back_color_over" onmouseout="this.style.background=sub_menu_back_color_out">
						                <img style="vertical-align: middle" width="16" height="16" src="../../../../assets/icons/txtfolder.gif" border="0" hspace="3" />
						                <span class="tag">6.4</span> &nbsp;&nbsp; <asp:HyperLink ID="sub6_4" runat="server" Text="EU ETS emissions" NavigateUrl="~/reports/CarbonReports/CRCManagementArea/CRCEvidencePack/Pack/CCA_Page64.asp" Target="evidencepack"></asp:HyperLink>
                		            </div>

                                    <div class="Option" onmouseover="this.style.background=sub_menu_back_color_over" onmouseout="this.style.background=sub_menu_back_color_out">
						                <img style="vertical-align: middle" width="16" height="16" src="../../../../assets/icons/txtfolder.gif" border="0" hspace="3" />
						                <span class="tag">6.5</span> &nbsp;&nbsp; <asp:HyperLink ID="sub6_5" runat="server" Text="Electricity generating credits" NavigateUrl="~/reports/CarbonReports/CRCManagementArea/CRCEvidencePack/Pack/CCA_Page65.asp" Target="evidencepack"></asp:HyperLink>
                		            </div>

                                    <div class="Option" onmouseover="this.style.background=sub_menu_back_color_over" onmouseout="this.style.background=sub_menu_back_color_out">
						                <img style="vertical-align: middle" width="16" height="16" src="../../../../assets/icons/txtfolder.gif" border="0" hspace="3" />
						                <span class="tag">6.6</span> &nbsp;&nbsp; <asp:HyperLink ID="sub6_6" runat="server" Text="Residual measurement list" NavigateUrl="~/reports/CarbonReports/CRCManagementArea/CRCEvidencePack/Pack/CCA_Page66.asp" Target="evidencepack"></asp:HyperLink>
                		            </div>

                                </div>


                                <!-- Heading 6-->
                                <div class="Menu" onclick="SwitchMenu('sub7')">
                                    <img id="A7" style="vertical-align: middle" width="20" height="20" src="../../../../assets/icons/open.gif" border="0" hspace="3" />
                                    <span class="tag">7</span> &nbsp;&nbsp; Annual report
                                </div>

                                <div class="Options" id="sub7">

                                    <div class="Option" onmouseover="this.style.background=sub_menu_back_color_over" onmouseout="this.style.background=sub_menu_back_color_out">
						                <img style="vertical-align: middle" width="16" height="16" src="../../../../assets/icons/txtfolder.gif" border="0" hspace="3" />
						                <span class="tag">7.1</span> &nbsp;&nbsp; <asp:HyperLink ID="sub7_1" runat="server" Text="Most recent annual report" NavigateUrl="~/reports/CarbonReports/CRCManagementArea/CRCEvidencePack/Pack/CCA_Page71.asp" Target="evidencepack"></asp:HyperLink>
                		            </div>

                                    <div class="Option" onmouseover="this.style.background=sub_menu_back_color_over" onmouseout="this.style.background=sub_menu_back_color_out">
						                <img style="vertical-align: middle" width="16" height="16" src="../../../../assets/icons/txtfolder.gif" border="0" hspace="3" />
						                <span class="tag">7.2.1</span> &nbsp;&nbsp; <asp:HyperLink ID="sub7_2_1" runat="server" Text="Energy supply data" NavigateUrl="~/reports/CarbonReports/CRCManagementArea/CRCEvidencePack/Pack/CCA_Page721.asp" Target="evidencepack"></asp:HyperLink>
                		            </div>

                                    <div class="Option" onmouseover="this.style.background=sub_menu_back_color_over" onmouseout="this.style.background=sub_menu_back_color_out">
						                <img style="vertical-align: middle" width="16" height="16" src="../../../../assets/icons/txtfolder.gif" border="0" hspace="3" />
						                <span class="tag">7.2.2</span> &nbsp;&nbsp; <asp:HyperLink ID="sub7_2_2" runat="server" Text="Energy supply data" NavigateUrl="~/reports/CarbonReports/CRCManagementArea/CRCEvidencePack/Pack/CCA_Page722.asp" Target="evidencepack"></asp:HyperLink>
                		            </div>

                                    <div class="Option" onmouseover="this.style.background=sub_menu_back_color_over" onmouseout="this.style.background=sub_menu_back_color_out">
						                <img style="vertical-align: middle" width="16" height="16" src="../../../../assets/icons/txtfolder.gif" border="0" hspace="3" />
						                <span class="tag">7.2.3</span> &nbsp;&nbsp; <asp:HyperLink ID="sub7_2_3" runat="server" Text="Energy supply data" NavigateUrl="~/reports/CarbonReports/CRCManagementArea/CRCEvidencePack/Pack/CCA_Page723.asp" Target="evidencepack"></asp:HyperLink>
                		            </div>

                                    <div class="Option" onmouseover="this.style.background=sub_menu_back_color_over" onmouseout="this.style.background=sub_menu_back_color_out">
						                <img style="vertical-align: middle" width="16" height="16" src="../../../../assets/icons/txtfolder.gif" border="0" hspace="3" />
						                <span class="tag">7.2.4</span> &nbsp;&nbsp; <asp:HyperLink ID="sub7_2_4" runat="server" Text="Energy supply data" NavigateUrl="~/reports/CarbonReports/CRCManagementArea/CRCEvidencePack/Pack/CCA_Page724.asp" Target="evidencepack"></asp:HyperLink>
                		            </div>

                                    <div class="Option" onmouseover="this.style.background=sub_menu_back_color_over" onmouseout="this.style.background=sub_menu_back_color_out">
						                <img style="vertical-align: middle" width="16" height="16" src="../../../../assets/icons/txtfolder.gif" border="0" hspace="3" />
						                <span class="tag">7.2.6</span> &nbsp;&nbsp; <asp:HyperLink ID="sub7_2_6" runat="server" Text="Energy supply data" NavigateUrl="~/reports/CarbonReports/CRCManagementArea/CRCEvidencePack/Pack/CCA_Page726.asp" Target="evidencepack"></asp:HyperLink>
                		            </div>

                                    <div class="Option" onmouseover="this.style.background=sub_menu_back_color_over" onmouseout="this.style.background=sub_menu_back_color_out">
						                <img style="vertical-align: middle" width="16" height="16" src="../../../../assets/icons/txtfolder.gif" border="0" hspace="3" />
						                <span class="tag">7.2.7</span> &nbsp;&nbsp; <asp:HyperLink ID="sub7_2_7" runat="server" Text="Energy supply data" NavigateUrl="~/reports/CarbonReports/CRCManagementArea/CRCEvidencePack/Pack/CCA_Page727.asp" Target="evidencepack"></asp:HyperLink>
                		            </div>

                                    <div class="Option" onmouseover="this.style.background=sub_menu_back_color_over" onmouseout="this.style.background=sub_menu_back_color_out">
						                <img style="vertical-align: middle" width="16" height="16" src="../../../../assets/icons/txtfolder.gif" border="0" hspace="3" />
						                <span class="tag">7.2.8</span> &nbsp;&nbsp; <asp:HyperLink ID="sub7_2_8" runat="server" Text="Energy supply data" NavigateUrl="~/reports/CarbonReports/CRCManagementArea/CRCEvidencePack/Pack/CCA_Page728.asp" Target="evidencepack"></asp:HyperLink>
                		            </div>

                                    <div class="Option" onmouseover="this.style.background=sub_menu_back_color_over" onmouseout="this.style.background=sub_menu_back_color_out">
						                <img style="vertical-align: middle" width="16" height="16" src="../../../../assets/icons/txtfolder.gif" border="0" hspace="3" />
						                <span class="tag">7.2.9</span> &nbsp;&nbsp; <asp:HyperLink ID="sub7_2_9" runat="server" Text="Energy supply data" NavigateUrl="~/reports/CarbonReports/CRCManagementArea/CRCEvidencePack/Pack/CCA_Page729.asp" Target="evidencepack"></asp:HyperLink>
                		            </div>

                                    <div class="Option" onmouseover="this.style.background=sub_menu_back_color_over" onmouseout="this.style.background=sub_menu_back_color_out">
						                <img style="vertical-align: middle" width="16" height="16" src="../../../../assets/icons/txtfolder.gif" border="0" hspace="3" />
						                <span class="tag">7.2.10</span> &nbsp;&nbsp; <asp:HyperLink ID="sub7_2_10" runat="server" Text="Energy supply data" NavigateUrl="~/reports/CarbonReports/CRCManagementArea/CRCEvidencePack/Pack/CCA_Page7210.asp" Target="evidencepack"></asp:HyperLink>
                		            </div>

                                    <div class="Option" onmouseover="this.style.background=sub_menu_back_color_over" onmouseout="this.style.background=sub_menu_back_color_out">
						                <img style="vertical-align: middle" width="16" height="16" src="../../../../assets/icons/txtfolder.gif" border="0" hspace="3" />
						                <span class="tag">7.3.1</span> &nbsp;&nbsp; <asp:HyperLink ID="sub7_3_1" runat="server" Text="Metrics information" NavigateUrl="~/reports/CarbonReports/CRCManagementArea/CRCEvidencePack/Pack/CCA_Page731.asp" Target="evidencepack"></asp:HyperLink>
                		            </div>

                                    <div class="Option" onmouseover="this.style.background=sub_menu_back_color_over" onmouseout="this.style.background=sub_menu_back_color_out">
						                <img style="vertical-align: middle" width="16" height="16" src="../../../../assets/icons/txtfolder.gif" border="0" hspace="3" />
						                <span class="tag">7.3.2</span> &nbsp;&nbsp; <asp:HyperLink ID="sub7_3_2" runat="server" Text="Metrics information" NavigateUrl="~/reports/CarbonReports/CRCManagementArea/CRCEvidencePack/Pack/CCA_Page732.asp" Target="evidencepack"></asp:HyperLink>
                		            </div>

                                    <div class="Option" onmouseover="this.style.background=sub_menu_back_color_over" onmouseout="this.style.background=sub_menu_back_color_out">
						                <img style="vertical-align: middle" width="16" height="16" src="../../../../assets/icons/txtfolder.gif" border="0" hspace="3" />
						                <span class="tag">7.4.1</span> &nbsp;&nbsp; <asp:HyperLink ID="sub7_4_1" runat="server" Text="Info on carbon mgmt" NavigateUrl="~/reports/CarbonReports/CRCManagementArea/CRCEvidencePack/Pack/CCA_Page741.asp" Target="evidencepack"></asp:HyperLink>
                		            </div>

                                    <div class="Option" onmouseover="this.style.background=sub_menu_back_color_over" onmouseout="this.style.background=sub_menu_back_color_out">
						                <img style="vertical-align: middle" width="16" height="16" src="../../../../assets/icons/txtfolder.gif" border="0" hspace="3" />
						                <span class="tag">7.5</span> &nbsp;&nbsp; <asp:HyperLink ID="sub7_5" runat="server" Text="Supplied and archived" NavigateUrl="~/reports/CarbonReports/CRCManagementArea/CRCEvidencePack/Pack/CCA_Page75.asp" Target="evidencepack"></asp:HyperLink>
                		            </div>
                                </div>


                                <!-- Heading 7-->
                                <div class="Menu" onclick="SwitchMenu('sub8')">
                                    <img id="A8" style="vertical-align: middle" width="20" height="20" src="../../../../assets/icons/open.gif" border="0" hspace="3" />
                                    <span class="tag">8</span> &nbsp;&nbsp; Special events / change records
                                </div>

                                <div class="Options" id="sub8">
                                
                                    <div class="Option" onmouseover="this.style.background=sub_menu_back_color_over" onmouseout="this.style.background=sub_menu_back_color_out">
						                <img style="vertical-align: middle" width="16" height="16" src="../../../../assets/icons/txtfolder.gif" border="0" hspace="3" />
						                <span class="tag">8.1.1</span> &nbsp;&nbsp; <asp:HyperLink ID="sub8_1_1" runat="server" Text="Info for officers" NavigateUrl="~/reports/CarbonReports/CRCManagementArea/CRCEvidencePack/Pack/CCA_Page811.asp" Target="evidencepack"></asp:HyperLink>
                		            </div>

                                    <div class="Option" onmouseover="this.style.background=sub_menu_back_color_over" onmouseout="this.style.background=sub_menu_back_color_out">
						                <img style="vertical-align: middle" width="16" height="16" src="../../../../assets/icons/txtfolder.gif" border="0" hspace="3" />
						                <span class="tag">8.1.2</span> &nbsp;&nbsp; <asp:HyperLink ID="sub8_1_2" runat="server" Text="Info for officers" NavigateUrl="~/reports/CarbonReports/CRCManagementArea/CRCEvidencePack/Pack/CCA_Page812.asp" Target="evidencepack"></asp:HyperLink>
                		            </div>

                                    <div class="Option" onmouseover="this.style.background=sub_menu_back_color_over" onmouseout="this.style.background=sub_menu_back_color_out">
						                <img style="vertical-align: middle" width="16" height="16" src="../../../../assets/icons/txtfolder.gif" border="0" hspace="3" />
						                <span class="tag">8.2.1</span> &nbsp;&nbsp; <asp:HyperLink ID="sub8_2_1" runat="server" Text="Source list or supplier" NavigateUrl="~/reports/CarbonReports/CRCManagementArea/CRCEvidencePack/Pack/CCA_Page821.asp" Target="evidencepack"></asp:HyperLink>
                		            </div>

                                    <div class="Option" onmouseover="this.style.background=sub_menu_back_color_over" onmouseout="this.style.background=sub_menu_back_color_out">
						                <img style="vertical-align: middle" width="16" height="16" src="../../../../assets/icons/txtfolder.gif" border="0" hspace="3" />
						                <span class="tag">8.2.2</span> &nbsp;&nbsp; <asp:HyperLink ID="sub8_2_2" runat="server" Text="Source list or supplier" NavigateUrl="~/reports/CarbonReports/CRCManagementArea/CRCEvidencePack/Pack/CCA_Page822.asp" Target="evidencepack"></asp:HyperLink>
                		            </div>

                                    <div class="Option" onmouseover="this.style.background=sub_menu_back_color_over" onmouseout="this.style.background=sub_menu_back_color_out">
						                <img style="vertical-align: middle" width="16" height="16" src="../../../../assets/icons/txtfolder.gif" border="0" hspace="3" />
						                <span class="tag">8.3</span> &nbsp;&nbsp; <asp:HyperLink ID="sub8_3" runat="server" Text="Company structure" NavigateUrl="~/reports/CarbonReports/CRCManagementArea/CRCEvidencePack/Pack/CCA_Page83.asp" Target="evidencepack"></asp:HyperLink>
                		            </div>

                                    <div class="Option" onmouseover="this.style.background=sub_menu_back_color_over" onmouseout="this.style.background=sub_menu_back_color_out">
						                <img style="vertical-align: middle" width="16" height="16" src="../../../../assets/icons/txtfolder.gif" border="0" hspace="3" />
						                <span class="tag">8.4</span> &nbsp;&nbsp; <asp:HyperLink ID="sub8_4" runat="server" Text="EU ETS/CCA coverage" NavigateUrl="~/reports/CarbonReports/CRCManagementArea/CRCEvidencePack/Pack/CCA_Page84.asp" Target="evidencepack"></asp:HyperLink>
                		            </div>

                                    <div class="Option" onmouseover="this.style.background=sub_menu_back_color_over" onmouseout="this.style.background=sub_menu_back_color_out">
						                <img style="vertical-align: middle" width="16" height="16" src="../../../../assets/icons/txtfolder.gif" border="0" hspace="3" />
						                <span class="tag">8.5</span> &nbsp;&nbsp; <asp:HyperLink ID="sub8_5" runat="server" Text="Faults affecting reporting" NavigateUrl="~/reports/CarbonReports/CRCManagementArea/CRCEvidencePack/Pack/CCA_Page85.asp" Target="evidencepack"></asp:HyperLink>
                		            </div>

                                    <div class="Option" onmouseover="this.style.background=sub_menu_back_color_over" onmouseout="this.style.background=sub_menu_back_color_out">
						                <img style="vertical-align: middle" width="16" height="16" src="../../../../assets/icons/txtfolder.gif" border="0" hspace="3" />
						                <span class="tag">8.6</span> &nbsp;&nbsp; <asp:HyperLink ID="sub8_6" runat="server" Text="Usage over scheme year" NavigateUrl="~/reports/CarbonReports/CRCManagementArea/CRCEvidencePack/Pack/CCA_Page86.asp" Target="evidencepack"></asp:HyperLink>
                		            </div>

                                    <div class="Option" onmouseover="this.style.background=sub_menu_back_color_over" onmouseout="this.style.background=sub_menu_back_color_out">
						                <img style="vertical-align: middle" width="16" height="16" src="../../../../assets/icons/txtfolder.gif" border="0" hspace="3" />
						                <span class="tag">8.7</span> &nbsp;&nbsp; <asp:HyperLink ID="sub8_7" runat="server" Text="Accuracy over scheme year" NavigateUrl="~/reports/CarbonReports/CRCManagementArea/CRCEvidencePack/Pack/CCA_Page87.asp" Target="evidencepack"></asp:HyperLink>
                		            </div>

                                    <div class="Option" onmouseover="this.style.background=sub_menu_back_color_over" onmouseout="this.style.background=sub_menu_back_color_out">
						                <img style="vertical-align: middle" width="16" height="16" src="../../../../assets/icons/txtfolder.gif" border="0" hspace="3" />
						                <span class="tag">8.8</span> &nbsp;&nbsp; <asp:HyperLink ID="sub8_8" runat="server" Text="Contact with regulators" NavigateUrl="~/reports/CarbonReports/CRCManagementArea/CRCEvidencePack/Pack/CCA_Page88.asp" Target="evidencepack"></asp:HyperLink>
                		            </div>

                                    <div class="Option" onmouseover="this.style.background=sub_menu_back_color_over" onmouseout="this.style.background=sub_menu_back_color_out">
						                <img style="vertical-align: middle" width="16" height="16" src="../../../../assets/icons/txtfolder.gif" border="0" hspace="3" />
						                <span class="tag">8.9</span> &nbsp;&nbsp; <asp:HyperLink ID="sub8_9" runat="server" Text="Designated changes" NavigateUrl="~/reports/CarbonReports/CRCManagementArea/CRCEvidencePack/Pack/CCA_Page89.asp" Target="evidencepack"></asp:HyperLink>
                		            </div>
                                </div>


                                <!-- Heading 8-->
                                <div class="Menu" onclick="SwitchMenu('sub9')">
                                    <img id="A9" style="vertical-align: middle" width="20" height="20" src="../../../../assets/icons/open.gif" border="0" hspace="3" />
                                    <span class="tag">9</span> &nbsp;&nbsp; Exemptions and exclusions
                                </div>

                                <div class="Options" id="sub9">

                                    <div class="Option" onmouseover="this.style.background=sub_menu_back_color_over" onmouseout="this.style.background=sub_menu_back_color_out">
						                <img style="vertical-align: middle" width="16" height="16" src="../../../../assets/icons/txtfolder.gif" border="0" hspace="3" />
						                <span class="tag">9.1</span> &nbsp;&nbsp; <asp:HyperLink ID="sub9_1" runat="server" Text="Registration CCA exemption " NavigateUrl="~/reports/CarbonReports/CRCManagementArea/CRCEvidencePack/Pack/CCA_Page91.asp" Target="evidencepack"></asp:HyperLink>
                		            </div>

                                    <div class="Option" onmouseover="this.style.background=sub_menu_back_color_over" onmouseout="this.style.background=sub_menu_back_color_out">
						                <img style="vertical-align: middle" width="16" height="16" src="../../../../assets/icons/txtfolder.gif" border="0" hspace="3" />
						                <span class="tag">9.2a</span> &nbsp;&nbsp; <asp:HyperLink ID="sub9_2_a" runat="server" Text="Group CCA exemption" NavigateUrl="~/reports/CarbonReports/CRCManagementArea/CRCEvidencePack/Pack/CCA_Page92a.asp" Target="evidencepack"></asp:HyperLink>
                		            </div>

                                    <div class="Option" onmouseover="this.style.background=sub_menu_back_color_over" onmouseout="this.style.background=sub_menu_back_color_out">
						                <img style="vertical-align: middle" width="16" height="16" src="../../../../assets/icons/txtfolder.gif" border="0" hspace="3" />
						                <span class="tag">9.2b</span> &nbsp;&nbsp; <asp:HyperLink ID="sub9_2_b" runat="server" Text="Group CCA exemption" NavigateUrl="~/reports/CarbonReports/CRCManagementArea/CRCEvidencePack/Pack/CCA_Page92b.asp" Target="evidencepack"></asp:HyperLink>
                		            </div>

                                    <div class="Option" onmouseover="this.style.background=sub_menu_back_color_over" onmouseout="this.style.background=sub_menu_back_color_out">
						                <img style="vertical-align: middle" width="16" height="16" src="../../../../assets/icons/txtfolder.gif" border="0" hspace="3" />
						                <span class="tag">9.3</span> &nbsp;&nbsp; <asp:HyperLink ID="sub9_3" runat="server" Text="Footprint report CCA exemption" NavigateUrl="~/reports/CarbonReports/CRCManagementArea/CRCEvidencePack/Pack/CCA_Page93.asp" Target="evidencepack"></asp:HyperLink>
                		            </div>

                                    <div class="Option" onmouseover="this.style.background=sub_menu_back_color_over" onmouseout="this.style.background=sub_menu_back_color_out">
						                <img style="vertical-align: middle" width="16" height="16" src="../../../../assets/icons/txtfolder.gif" border="0" hspace="3" />
						                <span class="tag">9.4a</span> &nbsp;&nbsp; <asp:HyperLink ID="sub9_4_a" runat="server" Text="Group CCA exemption" NavigateUrl="~/reports/CarbonReports/CRCManagementArea/CRCEvidencePack/Pack/CCA_Page94a.asp" Target="evidencepack"></asp:HyperLink>
                		            </div>

                                    <div class="Option" onmouseover="this.style.background=sub_menu_back_color_over" onmouseout="this.style.background=sub_menu_back_color_out">
						                <img style="vertical-align: middle" width="16" height="16" src="../../../../assets/icons/txtfolder.gif" border="0" hspace="3" />
						                <span class="tag">9.4b</span> &nbsp;&nbsp; <asp:HyperLink ID="sub9_4_b" runat="server" Text="Group CCA exemption" NavigateUrl="~/reports/CarbonReports/CRCManagementArea/CRCEvidencePack/Pack/CCA_Page94b.asp" Target="evidencepack"></asp:HyperLink>
                		            </div>

                                    <div class="Option" onmouseover="this.style.background=sub_menu_back_color_over" onmouseout="this.style.background=sub_menu_back_color_out">
						                <img style="vertical-align: middle" width="16" height="16" src="../../../../assets/icons/txtfolder.gif" border="0" hspace="3" />
						                <span class="tag">9.5</span> &nbsp;&nbsp; <asp:HyperLink ID="sub9_5" runat="server" Text="CCA exclusion footprint year" NavigateUrl="~/reports/CarbonReports/CRCManagementArea/CRCEvidencePack/Pack/CCA_Page95.asp" Target="evidencepack"></asp:HyperLink>
                		            </div>

                                    <div class="Option" onmouseover="this.style.background=sub_menu_back_color_over" onmouseout="this.style.background=sub_menu_back_color_out">
						                <img style="vertical-align: middle" width="16" height="16" src="../../../../assets/icons/txtfolder.gif" border="0" hspace="3" />
						                <span class="tag">9.6.1</span> &nbsp;&nbsp; <asp:HyperLink ID="sub9_6_1" runat="server" Text="EU ETS exclusion footprint year" NavigateUrl="~/reports/CarbonReports/CRCManagementArea/CRCEvidencePack/Pack/CCA_Page961.asp" Target="evidencepack"></asp:HyperLink>
                		            </div>

                                    <div class="Option" onmouseover="this.style.background=sub_menu_back_color_over" onmouseout="this.style.background=sub_menu_back_color_out">
						                <img style="vertical-align: middle" width="16" height="16" src="../../../../assets/icons/txtfolder.gif" border="0" hspace="3" />
						                <span class="tag">9.7</span> &nbsp;&nbsp; <asp:HyperLink ID="sub9_7" runat="server" Text="Source list exclusions" NavigateUrl="~/reports/CarbonReports/CRCManagementArea/CRCEvidencePack/Pack/CCA_Page97.asp" Target="evidencepack"></asp:HyperLink>
                		            </div>
                                    <div class="Option" onmouseover="this.style.background=sub_menu_back_color_over" onmouseout="this.style.background=sub_menu_back_color_out">
						                <img style="vertical-align: middle" width="16" height="16" src="../../../../assets/icons/txtfolder.gif" border="0" hspace="3" />
						                <span class="tag">9.8</span> &nbsp;&nbsp; <asp:HyperLink ID="su9_8" runat="server" Text="Transport exclusions footprint year" NavigateUrl="~/reports/CarbonReports/CRCManagementArea/CRCEvidencePack/Pack/CCA_Page98.asp" Target="evidencepack"></asp:HyperLink>
                		            </div>

                                    <div class="Option" onmouseover="this.style.background=sub_menu_back_color_over" onmouseout="this.style.background=sub_menu_back_color_out">
						                <img style="vertical-align: middle" width="16" height="16" src="../../../../assets/icons/txtfolder.gif" border="0" hspace="3" />
						                <span class="tag">9.9</span> &nbsp;&nbsp; <asp:HyperLink ID="sub9_9" runat="server" Text="Domestic exclusions" NavigateUrl="~/reports/CarbonReports/CRCManagementArea/CRCEvidencePack/Pack/CCA_Page99.asp" Target="evidencepack"></asp:HyperLink>
                		            </div>

                                    <div class="Option" onmouseover="this.style.background=sub_menu_back_color_over" onmouseout="this.style.background=sub_menu_back_color_out">
						                <img style="vertical-align: middle" width="16" height="16" src="../../../../assets/icons/txtfolder.gif" border="0" hspace="3" />
						                <span class="tag">9.10.1</span> &nbsp;&nbsp; <asp:HyperLink ID="sub9_10_1" runat="server" Text="Unconsumed supply" NavigateUrl="~/reports/CarbonReports/CRCManagementArea/CRCEvidencePack/Pack/CCA_Page9101.asp" Target="evidencepack"></asp:HyperLink>
                		            </div>

                                </div>

                                <!-- End of Menu -->
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </asp:Panel>

</div>

