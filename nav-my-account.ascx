<%@ Control Language="VB" AutoEventWireup="false" CodeFile="nav-my-account.ascx.vb" Inherits="nav_my_account" %>

<div id="left_nav_account" class="left_nav">
    <div>
        <asp:hyperlink rel="nofollow" runat="server" id="lnkMyAccount" navigateurl="~/my-account.aspx" cssclass="nav_home" tooltip="Your Utility Masters account information" />
        <h2 class="nav_heading"><asp:hyperlink runat="server" id="lnkAccountHome" navigateurl="~/my-account.aspx" tooltip="Your reports">My Account Home</asp:hyperlink></h2>
        <ul class="styleformytreeview" id="my_treeview" runat="server"></ul>
        
    </div>
</div>

