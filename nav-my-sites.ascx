<%@ Control Language="VB" AutoEventWireup="false" CodeFile="nav-my-sites.ascx.vb" Inherits="nav_my_sites" %>

<div id="left_nav_sites" class="left_nav">
    <div>
        <asp:hyperlink rel="nofollow" runat="server" id="lnkMySites" navigateurl="~/my-sites.aspx" cssclass="nav_home" tooltip="Everything about your sites" />
        
        <h2 class="nav_heading"><asp:hyperlink runat="server" id="lnkSitesHome" ForeColor="#009530" navigateurl="~/my-sites.aspx" tooltip="Everything about your sites" Text="My Sites Home"></asp:hyperlink></h2>
        
        <ul class="nav" runat="server" id="nav">
            <li runat="server" id="liLink1"><asp:hyperlink runat="server" id="lnkLink1" navigateurl="mysites/map/default.aspx" tooltip="Map View" Text="Map View"></asp:hyperlink></li>
            <li runat="server" id="liLink2"><asp:hyperlink runat="server" id="lnkLink2" navigateurl="mysites/search/default.aspx" tooltip="Search" Text="Search"></asp:hyperlink></li>
            <li runat="server" id="liLink3"><asp:hyperlink runat="server" id="lnkLink3" navigateurl="mysites/supplydetails/default.aspx" tooltip="Supply Details" Text="Supply Details"></asp:hyperlink></li>
        </ul>
        
        <ul class="styleformytreeview" id="my_treeview" runat="server" ></ul>
        
    </div>
</div>

