﻿Imports clsReportServerCredentials

Partial Class reports_FinancialReports_MonthlyConsumptionReport_Report
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim QSvarMeter As String = Request.QueryString("Meter")

        Dim QSDate As String = Request.QueryString("Date")

        Dim QSReportLevel As String = Request.QueryString("ReportLevel")

        Me.frmReport.Visible = True

        'Set the processing mode for the ReportViewer to Remote
        myReportViewer.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Remote

        Dim serverReport As Microsoft.Reporting.WebForms.ServerReport
        serverReport = myReportViewer.ServerReport

        'serverReport.ReportServerCredentials = New clsReportServerCredentials("administrator", "chicago", "uml")

        'Set the report server URL and report path
        serverReport.ReportServerUrl =
            New Uri("http://reports.mceg.local/reportserver")
        serverReport.ReportPath = "/UML/UTILISYS/Monthly Consumption Report"

        Dim varMeter As New Microsoft.Reporting.WebForms.ReportParameter()
        varMeter.Name = "MeterFK"
        varMeter.Values.Add(QSvarMeter)

        Dim varDate As New Microsoft.Reporting.WebForms.ReportParameter()
        varDate.Name = "Date"
        varDate.Values.Add(QSDate)

        Dim ReportLevel As New Microsoft.Reporting.WebForms.ReportParameter()
        ReportLevel.Name = "ReportLevel"
        ReportLevel.Values.Add(QSReportLevel)

        'Set the report parameters for the report
        Dim parameters() As Microsoft.Reporting.WebForms.ReportParameter = {varMeter, varDate, ReportLevel}
        serverReport.SetParameters(parameters)

    End Sub

End Class
