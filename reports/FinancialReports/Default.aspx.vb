
Partial Class reports_OnlineUtilisys_Default
    Inherits System.Web.UI.Page



    Private Sub LoadReports()

        Me.ulBox.InnerHtml = ""

        Dim strSQL As String = "EXEC spReports_ListReports " & MySession.UserID & ", 11"

        Using connection As New SqlConnection(Core.ConnectionString)
            connection.Open()
            Dim command As New SqlCommand(strSQL, connection)
            command.CommandType = CommandType.Text
            Dim reader As SqlDataReader = command.ExecuteReader()

            While reader.Read

                Dim strUrl As String
                If reader("intReportDisabled") = 0 Then
                    strUrl = reader("varOnlineURL").ToString
                Else
                    strUrl = ""
                End If
                Me.ulBox.InnerHtml &= "<li><a id='lnkReports" & reader("intReportPK") & "' class='" & reader("varCSS") & "' runat='server' title='" & reader("varReportName") & "' href='" & strUrl & "' >"

                Try
                    'If reader("intReportPK") = 52 Then Me.ulBox.InnerHtml &= reader("varReportName")
                Catch ex As Exception
                End Try

                Me.ulBox.InnerHtml &= "</a></li>"

            End While

        End Using

        'core.data_Reader(strSQL)

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            If Not IIf(Core.data_select_value("select bitAllowMyReports from tblUsers where intUserPK = " & MySession.UserID) = "True", True, False) Then
                Response.Redirect(Core.data_select_value("select RawHomeURL from vwUsers where ID = " & MySession.UserID), True)
            End If

            If Not Page.IsPostBack = True Then
                Dim description As HtmlMeta = New HtmlMeta()
                description.Name = "description"
                description.Content = "Utilisys - Financial Reports"
                Dim keywords As HtmlMeta = New HtmlMeta()
                keywords.Name = "keywords"
                keywords.Content = "Utilisys - Financial Reports"
                Page.Header.Controls.Add(description)
                Page.Header.Controls.Add(keywords)

            End If

        Catch ex As Exception
            '
        End Try

        If Request.QueryString("custid") Is Nothing Then
            Dim strSQL As String = "EXEC spPortal_TREEVIEW " & MySession.UserID
            Dim myConnection As New OleDb.OleDbConnection(Core.CurrentConnectionString)

            Dim myCommand As New OleDb.OleDbCommand(strSQL, myConnection)
            myCommand.Connection.Open()
            myCommand.CommandTimeout = 180
            Dim Reader As OleDb.OleDbDataReader = myCommand.ExecuteReader(CommandBehavior.CloseConnection)

            If Reader.HasRows Then
                Reader.Read()
                If Reader("Type") = "Company" Then
                    Response.Redirect("default.aspx?custid=" & Reader("id") & "&method=customer", True)
                Else
                    Response.Redirect("default.aspx?custid=" & Reader("id") & "&method=" & Reader("Type"), True)
                End If
                Reader.Close()
            End If
        End If

        If Not Page.IsPostBack Then

            '            loadlinks()
            LoadReports()
        End If

    End Sub
End Class
