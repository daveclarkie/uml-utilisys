﻿Imports clsReportServerCredentials

Partial Class reports_FinancialReports_WeeklyForecastSpend_Report
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim QScustid As String = Request.QueryString("custid")

        Dim QSsiteid As String = Request.QueryString("siteid")

        Dim QSWeeknum As Date = Request.QueryString("Weeknum")

        Me.frmReport.Visible = True

        'Set the processing mode for the ReportViewer to Remote
        myReportViewer.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Remote

        Dim serverReport As Microsoft.Reporting.WebForms.ServerReport
        serverReport = myReportViewer.ServerReport

        serverReport.ReportServerCredentials = New clsReportServerCredentials("administrator", "chicago", "uml")

        'Set the report server URL and report path
        serverReport.ReportServerUrl = _
            New Uri("http://10.44.5.205/reportserver")
        serverReport.ReportPath = "/Portal Reporting/Weekly Forecast Spend"

        Dim custid As New Microsoft.Reporting.WebForms.ReportParameter()
        custid.Name = "custid"
        custid.Values.Add(QScustid)


        Dim siteid As New Microsoft.Reporting.WebForms.ReportParameter()
        siteid.Name = "siteid"
        siteid.Values.Add(QSsiteid)


        Dim Weeknum As New Microsoft.Reporting.WebForms.ReportParameter()
        Weeknum.Name = "Weeknum"
        Weeknum.Values.Add(QSWeeknum)


        'Set the report parameters for the report
        Dim parameters() As Microsoft.Reporting.WebForms.ReportParameter = {custid, siteid, Weeknum}
        serverReport.SetParameters(parameters)


    End Sub

End Class
