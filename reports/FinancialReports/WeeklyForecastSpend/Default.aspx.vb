﻿Imports System.Drawing

Partial Class reports_FinancialReports_WeeklyForecastSpend_Default
    Inherits System.Web.UI.Page

    Public Sub ThickBox()
        'Me.lnkNewLink.NavigateUrl = "report.aspx?custid=" + "530" + "&siteid=" + "6764" + "&Weeknum=" + "30/05/2011" + "&?keepThis=true&TB_iframe=true&height=550&width=850"
        'Me.lnkNewLink.Text = "Generate Report"
        'Me.lnkNewLink.ImageUrl = "../../../assets/Schneider/buttons-myreports-general-generatereport.png"
        'Me.lnkNewLink.CssClass = "thickbox"
        'Me.lnkNewLink.ToolTip = "Monthly Consumption Report"

        Dim intError As Integer = 0

        Dim s As String = ListAssignedMeters()
        If Me.ddlDate.Items.Count > 0 Then
            If Me.ddlDate.SelectedItem.Value = "01/01/1900" Then
                Me.lnkNewLink.NavigateUrl = ""
                Me.lnkNewLink.Text = ""
                Me.lnkNewLink.ImageUrl = "../../../assets/Schneider/buttons-myreports-general-generatereport-disabled.png"
                Me.lnkNewLink.CssClass = ""
                Me.lnkNewLink.ToolTip = ""
            Else
                If intError = 0 Then 'ok
                    If s.Length > 0 Then
                        Me.lnkNewLink.NavigateUrl = "report.aspx?custid=" + Request.QueryString("custid") + "&siteid=" + CStr(s) + "&Weeknum=" + Me.ddlDate.SelectedItem.Value + "&?keepThis=true&TB_iframe=true&height=550&width=850"
                        Me.lnkNewLink.Text = "Generate Report"
                        Me.lnkNewLink.ImageUrl = "../../../assets/Schneider/buttons-myreports-general-generatereport.png"
                        Me.lnkNewLink.CssClass = "thickbox"
                        Me.lnkNewLink.ToolTip = "Monthly Consumption Report"
                    Else
                        Me.lnkNewLink.NavigateUrl = ""
                        Me.lnkNewLink.Text = ""
                        Me.lnkNewLink.ImageUrl = "../../../assets/Schneider/buttons-myreports-general-generatereport-disabled.png"
                        Me.lnkNewLink.CssClass = ""
                        Me.lnkNewLink.ToolTip = ""
                    End If
                Else
                    Me.lnkNewLink.NavigateUrl = ""
                    Me.lnkNewLink.Text = ""
                    Me.lnkNewLink.ImageUrl = "../../../assets/Schneider/buttons-myreports-general-generatereport-disabled.png"
                    Me.lnkNewLink.CssClass = ""
                    Me.lnkNewLink.ToolTip = ""
                End If

            End If
        Else
            Me.lnkNewLink.NavigateUrl = ""
            Me.lnkNewLink.Text = ""
            Me.lnkNewLink.ImageUrl = "../../../assets/Schneider/buttons-myreports-general-generatereport-disabled.png"
            Me.lnkNewLink.CssClass = ""
            Me.lnkNewLink.ToolTip = ""
        End If


    End Sub
    Public Overloads Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)

        ' Confirms that an HtmlForm control is rendered for the specified ASP.NET server control at run time.




    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        Try
            If Not IIf(Core.data_select_value("select bitAllowMyReports from tblUsers where intUserPK = " & MySession.UserID) = "True", True, False) Then
                Response.Redirect(Core.data_select_value("select RawHomeURL from vwUsers where ID = " & MySession.UserID), True)
            End If

            If Request.QueryString("custid") Is Nothing Then
                Dim strSQL As String = "EXEC spPortal_TREEVIEW_TopLevel " & MySession.UserID
                Dim myConnection As New OleDb.OleDbConnection(Core.CurrentConnectionString)

                Dim myCommand As New OleDb.OleDbCommand(strSQL, myConnection)
                myCommand.Connection.Open()
                myCommand.CommandTimeout = 180
                Dim Reader As OleDb.OleDbDataReader = myCommand.ExecuteReader(CommandBehavior.CloseConnection)

                If Reader.HasRows Then
                    Reader.Read()
                    If Reader("Type") = "Company" Then
                        Response.Redirect("default.aspx?custid=" & Reader("id") & "&method=Customer", True)
                    Else
                        Response.Redirect("default.aspx?custid=" & Reader("id") & "&method=" & Reader("Type"), True)
                    End If
                    Reader.Close()
                End If
            End If

            If Not Page.IsPostBack = True Then

                MeterPoints_Show("All")

                Dim description As HtmlMeta = New HtmlMeta()
                description.Name = "description"
                description.Content = "UTILISYS - Weekly Forecast Spend Report"
                Dim keywords As HtmlMeta = New HtmlMeta()
                keywords.Name = "keywords"
                keywords.Content = "UTILISYS - Weekly Forecast Spend Report"
                Page.Header.Controls.Add(description)
                Page.Header.Controls.Add(keywords)


            End If



            If Not Page.IsPostBack Then

                LoadWeekBeginingDropDown()


            End If

            CountAssignedMeters()

            ThickBox()


            Me.lblDescription.Text = Request.QueryString("method").ToString
            Me.lblName.Text = Core.data_select_value("EXEC spSites_MAP_CurentSelection " & MySession.UserID & ", '" & Request.QueryString("method") & "', " & Request.QueryString("custid"))

        Catch ex As Exception
            '
        End Try


    End Sub

    Private Sub MeterPoints_Show(ByVal Type As String)


        With Me.lbAvailableMeters
            .DataSource = Core.data_select(" EXEC [portal.utilitymasters.co.uk].dbo.usp_Admin_SiteList_Display " & MySession.UserID & ", '" & Request.QueryString("method") & "', " & Request.QueryString("custid") & ",'" & ListAssignedMeters() & "', '" & Me.txtCustomFilter.Text.ToString & "'")
            .DataValueField = "intSiteFK"
            .DataTextField = "SiteName"
            .DataBind()
        End With

        CountAssignedMeters()
        LoadWeekBeginingDropDown()
    End Sub

    Private Function ListAssignedMeters() As String
        Dim s As String = ""

        If Me.lbAssignedMeters.Items.Count > 0 Then
            For Each x As ListItem In lbAssignedMeters.Items
                s &= "," & x.Value
            Next
            If s.Length > 0 Then
                s = s.Remove(0, 1)
            End If
        End If

        Return s

    End Function

    Private Sub CountAssignedMeters()
        Dim i As Integer = 0
        For Each x As ListItem In lbAssignedMeters.Items
            i += 1
        Next

        If i > 4 Then
            Me.lbAvailableMeters.Enabled = False
        Else
            Me.lbAvailableMeters.Enabled = True
        End If

    End Sub

    Protected Sub rblMeterFilter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rblMeterFilter.SelectedIndexChanged
        If Me.rblMeterFilter.SelectedItem.Value = "Custom" Then
            Me.txtCustomFilter.Text = Nothing
            Me.secFilter.Visible = True
        Else
            Me.txtCustomFilter.Text = Nothing
            Me.secFilter.Visible = False
        End If


        MeterPoints_Show(Me.rblMeterFilter.SelectedItem.Value)
    End Sub

    Protected Sub lbAvailableMeters_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbAvailableMeters.SelectedIndexChanged


        Dim iAssigned As Integer = 0
        For Each xAssigned As ListItem In lbAssignedMeters.Items
            iAssigned += 1
        Next
        If iAssigned < 1 Then
            Me.lblAssignedMeterError.Text = ""

            Dim i As Integer = 0
            For Each x As ListItem In lbAvailableMeters.Items
                If x.Selected Then
                    i += 1
                End If
            Next

            If i > 0 Then
repeat:
                If lbAvailableMeters.Items.Count > 0 Then

                    Dim li As ListItem

                    For Each li In lbAvailableMeters.Items
                        If li.Selected = True Then
                            Dim iAssigned2 As Integer = 0
                            For Each xAssigned2 As ListItem In lbAssignedMeters.Items
                                iAssigned2 += 1
                            Next
                            If iAssigned2 = 1 Then
                                GoTo meterlimitexceeded
                            End If
                            lbAssignedMeters.Items.Add(li)
                            lbAvailableMeters.Items.Remove(li)
                            GoTo repeat
                        End If

                    Next

                End If
            End If

            Me.lbAssignedMeters.SelectedIndex = -1
            Me.lbAvailableMeters.SelectedIndex = -1

        Else
            Me.lblAssignedMeterError.Text = "you are currently limited to 1 site."
        End If


        MeterPoints_Show(Me.rblMeterFilter.SelectedItem.Value)

meterlimitexceeded:
        Me.lblAssignedMeterError.Text = "you are currently limited to 1 site."
        CountAssignedMeters()

        ThickBox()

    End Sub

    Protected Sub lbAssignedMeters_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbAssignedMeters.SelectedIndexChanged

        Me.lblAssignedMeterError.Text = ""

        Dim i As Integer = 0
        For Each x As ListItem In lbAssignedMeters.Items
            If x.Selected Then
                i += 1
            End If
        Next


        If i > 0 Then
repeat:
            If lbAssignedMeters.Items.Count > 0 Then

                Dim li As ListItem

                For Each li In lbAssignedMeters.Items
                    If li.Selected = True Then
                        lbAvailableMeters.Items.Add(li)
                        lbAssignedMeters.Items.Remove(li)
                        GoTo repeat
                    End If

                Next

            End If
        End If

        Me.lbAssignedMeters.SelectedIndex = -1
        Me.lbAvailableMeters.SelectedIndex = -1

        MeterPoints_Show(Me.rblMeterFilter.SelectedItem.Value)
        CountAssignedMeters()
        LoadWeekBeginingDropDown()
        ThickBox()
    End Sub


    Private Sub LoadWeekBeginingDropDown()

        Dim i As Integer = 0
        Dim s As String = ListAssignedMeters()

        i = s.Length

        If i > 0 Then


            With Me.ddlDate
                .Items.Clear()
                .DataSource = Core.data_select("SELECT DISTINCT Week, CAST(uml_extdata.dbo.FormatDateTime(Week,'dd/mm/yyyy') AS VARCHAR(100)) AS varValue FROM uml_cms.dbo.DIM_dates WHERE pk_date < GETDATE()-1 ORDER BY week desc")
                .DataValueField = "Week"
                .DataTextField = "varValue"
                .DataBind()
            End With
        Else
            With Me.ddlDate
                .Items.Clear()
                .DataSource = Core.data_select("SELECT '01/01/1900' as date, 'please select a meter' as nicedate")
                .DataValueField = "date"
                .DataTextField = "nicedate"
                .DataBind()
            End With
        End If

        ThickBox()

    End Sub
    Protected Sub ddlDate_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlDate.SelectedIndexChanged

        Dim strMeters As String = ListAssignedMeters()
        If strMeters.Length > 0 Then
            ThickBox()
        End If

    End Sub


    Protected Sub btnFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFilter.Click
        Me.txtCustomFilter.Text = fnNoSpecialCharacters(Me.txtCustomFilter.Text.ToString)
        MeterPoints_Show(Me.rblMeterFilter.SelectedItem.Value)

    End Sub

    Public Function fnNoSpecialCharacters(ByVal strTest As String) As String

        Dim sb As New System.Text.StringBuilder

        For Each ch As Char In strTest
            If Char.IsLetterOrDigit(ch) OrElse ch = " "c Then
                sb.Append(ch)
            End If
        Next

        strTest = sb.ToString

        Return strTest
    End Function


End Class
