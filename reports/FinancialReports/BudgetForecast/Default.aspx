﻿<%@ Page Title="UTILISYS - Weekly Forecast Spend" Language="VB" MasterPageFile="~/landing.master" AutoEventWireup="false" CodeFile="Default.aspx.vb" Inherits="reports_FinancialReports_BudgetForecast_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="content_right" Runat="Server">


        <script type="text/javascript" src="../../../ThickBox/jquery-1.3.2.min.js"></script>
        <script type="text/javascript" src="../../../ThickBox/thickbox.js"></script>
        <link rel="stylesheet" href="../../../ThickBox/thickbox.css" type="text/css" media="screen" />   
        
            
<form id="Form1" runat="server">

    <div class="content_back general">
            <table cellpadding="0" cellspacing="0">
     <tr valign="middle">
<tr valign="middle">
                    <td style="width:450px;" >
                        
                    </td>
                    <td style="width:150px;">
                    </td>
                </tr> <!-- Company selection -->
                
                <tr>
                    
                    <td colspan="1">
                        <table cellpadding="0" cellspacing="0">

                            <tr>
                                <td style="width:120px;" valign="top">
                                     <asp:Label ID="lblDescription" Text="Description" runat="server" Font-Bold="true" Width="120px" ></asp:Label>
                                </td>
                                <td style="width:400px;" valign="middle">
                                     <asp:Label ID="lblName" Text="Name" runat="server"></asp:Label>
                                </td>
                            </tr>
                                                        
                            <tr>
                                <td style="height:3px;">
                                    
                                </td>
                            </tr>

                             <tr>
                                <td style="width:120px;" valign="top">
                                     <b>Report</b>
                                </td>
                                <td style="width:400px;" valign="middle">
                                    Forecast Budget Report
                                </td>
                            </tr>
                                                        
                            <tr>
                                <td style="height:3px;">
                                    
                                </td>
                            </tr>

                           


                            <tr valign="middle">
                                <td valign="top" >
                                    <b>Week Begining</b>
                                </td>
                                <td align="left">
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <asp:DropDownList ID="ddlDate" runat="server" Width="200px" AutoPostBack="true"></asp:DropDownList>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td >
                        
                                </td>
                            </tr> <!-- Dates -->

                               
                        </table>
                    </td>
                </tr> <!-- Meter Selection Table -->
                
                <tr>
                    <td colspan="3" style="text-align:center;">
                        <asp:HyperLink  ID="lnkNewLink" runat="server" CssClass="thickbox"></asp:HyperLink>
                    </td>
                </tr> <!-- Run button -->  
                   
                <tr>
                    <td colspan="3" style="text-align:center;">
                        <asp:label  ID="lblError" runat="server"></asp:label>
                    </td>
                </tr> <!-- Error message -->               
            </table>
        </div>

</form>










</asp:Content>

