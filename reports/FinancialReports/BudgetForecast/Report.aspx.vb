﻿Imports clsReportServerCredentials

Partial Class reports_FinancialReports_BudgetForecast_Report
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim QScustid As Integer = Request.QueryString("custid")

        Dim QSstartdate As Integer = Request.QueryString("startdate")

        Me.frmReport.Visible = True

        'Set the processing mode for the ReportViewer to Remote
        myReportViewer.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Remote

        Dim serverReport As Microsoft.Reporting.WebForms.ServerReport
        serverReport = myReportViewer.ServerReport

        serverReport.ReportServerCredentials = New clsReportServerCredentials("administrator", "chicago", "uml")

        'Set the report server URL and report path
        serverReport.ReportServerUrl = _
            New Uri("http://10.44.5.205/reportserver")
        serverReport.ReportPath = "/Portal Reporting/Budget Forecast"

        Dim custid As New Microsoft.Reporting.WebForms.ReportParameter()
        custid.Name = "custid"
        custid.Values.Add(QScustid)

        Dim startdate As New Microsoft.Reporting.WebForms.ReportParameter()
        startdate.Name = "startdate"
        startdate.Values.Add(QSstartdate)


        'Set the report parameters for the report
        Dim parameters() As Microsoft.Reporting.WebForms.ReportParameter = {custid, startdate}
        serverReport.SetParameters(parameters)


    End Sub

End Class
