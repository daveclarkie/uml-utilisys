﻿Imports System.Drawing

Partial Class reports_FinancialReports_BudgetForecast_Default
    Inherits System.Web.UI.Page

    Public Sub ThickBox()

        Dim intError As Integer = 0

        Try
            If Me.ddlDate.SelectedIndex = -1 Then
                intError += 1
            End If
        Catch ex As Exception
            intError += 1
        End Try

        If Me.ddlDate.Items.Count > 0 Then
            If Me.ddlDate.SelectedItem.Value = 0 Then
                Me.lnkNewLink.NavigateUrl = ""
                Me.lnkNewLink.Text = ""
                Me.lnkNewLink.ImageUrl = "../../../assets/Schneider/buttons-myreports-general-generatereport-disabled.png"
                Me.lnkNewLink.CssClass = ""
                Me.lnkNewLink.ToolTip = ""
            Else
                If intError = 0 Then 'ok
                    Me.lnkNewLink.NavigateUrl = "report.aspx?custid=" + Request.QueryString("custid") + "&startdate=" + Me.ddlDate.SelectedItem.Value + "&?keepThis=true&TB_iframe=true&height=550&width=850"
                    Me.lnkNewLink.Text = "Generate Report"
                    Me.lnkNewLink.ImageUrl = "../../../assets/Schneider/buttons-myreports-general-generatereport.png"
                    Me.lnkNewLink.CssClass = "thickbox"
                    Me.lnkNewLink.ToolTip = "Budget Forecast Report"
                Else
                    Me.lnkNewLink.NavigateUrl = ""
                    Me.lnkNewLink.Text = ""
                    Me.lnkNewLink.ImageUrl = "../../../assets/Schneider/buttons-myreports-general-generatereport-disabled.png"
                    Me.lnkNewLink.CssClass = ""
                    Me.lnkNewLink.ToolTip = ""
                End If

            End If
        Else
            Me.lnkNewLink.NavigateUrl = ""
            Me.lnkNewLink.Text = ""
            Me.lnkNewLink.ImageUrl = "../../../assets/Schneider/buttons-myreports-general-generatereport-disabled.png"
            Me.lnkNewLink.CssClass = ""
            Me.lnkNewLink.ToolTip = ""
        End If


    End Sub
    Public Overloads Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)

        ' Confirms that an HtmlForm control is rendered for the specified ASP.NET server control at run time.




    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        Try
            If Not IIf(Core.data_select_value("select bitAllowMyReports from tblUsers where intUserPK = " & MySession.UserID) = "True", True, False) Then
                Response.Redirect(Core.data_select_value("select RawHomeURL from vwUsers where ID = " & MySession.UserID), True)
            End If

            If Request.QueryString("custid") Is Nothing Then
                Dim strSQL As String = "EXEC spPortal_TREEVIEW_TopLevel " & MySession.UserID
                Dim myConnection As New OleDb.OleDbConnection(Core.CurrentConnectionString)

                Dim myCommand As New OleDb.OleDbCommand(strSQL, myConnection)
                myCommand.Connection.Open()
                myCommand.CommandTimeout = 180
                Dim Reader As OleDb.OleDbDataReader = myCommand.ExecuteReader(CommandBehavior.CloseConnection)

                If Reader.HasRows Then
                    Reader.Read()
                    If Reader("Type") = "Company" Then
                        Response.Redirect("default.aspx?custid=" & Reader("id") & "&method=Customer", True)
                    Else
                        Response.Redirect("default.aspx?custid=" & Reader("id") & "&method=" & Reader("Type"), True)
                    End If
                    Reader.Close()
                End If
            End If

            If Not Page.IsPostBack = True Then


                Dim description As HtmlMeta = New HtmlMeta()
                description.Name = "description"
                description.Content = "UTILISYS - Budget Forecast Report"
                Dim keywords As HtmlMeta = New HtmlMeta()
                keywords.Name = "keywords"
                keywords.Content = "UTILISYS - Budget Forecast Report"
                Page.Header.Controls.Add(description)
                Page.Header.Controls.Add(keywords)


            End If



            If Not Page.IsPostBack Then

                LoadWeekBeginingDropDown()


            End If


            ThickBox()


            Me.lblDescription.Text = Request.QueryString("method").ToString
            Me.lblName.Text = Core.data_select_value("SELECT [portal.utilitymasters.co.uk].dbo.formatCustomerName(" & Request.QueryString("custid").ToString & ")")

        Catch ex As Exception
            '
        End Try


    End Sub


    Private Sub LoadWeekBeginingDropDown()

        With Me.ddlDate
            .Items.Clear()
            .DataSource = Core.data_select("select min(intdatepk) as [pk], sat_to_fri_week_name as [value] from uml_cms.dbo.dim_dates where pk_date > '31 dec 2011' and pk_date < getdate()-1 group by sat_to_fri_week, sat_to_fri_week_name order by min(intdatepk) desc")
            .DataValueField = "pk"
            .DataTextField = "value"
            .DataBind()
        End With
        ThickBox()

    End Sub
    Protected Sub ddlDate_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlDate.SelectedIndexChanged

            ThickBox()
        
    End Sub

End Class
