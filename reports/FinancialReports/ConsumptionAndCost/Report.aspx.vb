﻿Imports clsReportServerCredentials


Partial Class reports_Financial_ConsumptionandCost
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim QSvarMeterList As String = Request.QueryString("MeterList")
        Dim QSDateType As String = Request.QueryString("DateType")
        Dim QSdtmDateS As Date = Request.QueryString("StartDate")
        Dim QSdtmDateE As Date

        If QSDateType = 0 Then
            QSDateType = "Week"
            QSdtmDateE = DateAdd(DateInterval.Day, -1, DateAdd(DateInterval.WeekOfYear, 1, QSdtmDateS))
        ElseIf QSDateType = 1 Then
            QSDateType = "Month"
            QSdtmDateE = DateAdd(DateInterval.Day, -1, DateAdd(DateInterval.Month, 1, QSdtmDateS))
        End If

        Me.frmReport.Visible = True

        'Set the processing mode for the ReportViewer to Remote
        myReportViewer.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Remote

        Dim serverReport As Microsoft.Reporting.WebForms.ServerReport
        serverReport = myReportViewer.ServerReport

        'serverReport.ReportServerCredentials = New clsReportServerCredentials("administrator", "chicago", "uml")

        'Set the report server URL and report path
        serverReport.ReportServerUrl = _
            New Uri("http://reports.mceg.local/ReportServer12")
        serverReport.ReportPath = "/UML/UTILISYS/Consumption And Cost Report OnLine"

        Dim varMeterList As New Microsoft.Reporting.WebForms.ReportParameter()
        varMeterList.Name = "MeterList"
        varMeterList.Values.Add(QSvarMeterList)


        Dim DateType As New Microsoft.Reporting.WebForms.ReportParameter()
        DateType.Name = "DateType"
        DateType.Values.Add(QSDateType)


        Dim DateStart As New Microsoft.Reporting.WebForms.ReportParameter()
        DateStart.Name = "dtmStart"
        DateStart.Values.Add(QSdtmDateS)


        Dim DateEnd As New Microsoft.Reporting.WebForms.ReportParameter()
        DateEnd.Name = "dtmEnd"
        DateEnd.Values.Add(QSdtmDateE)



        'Set the report parameters for the report
        Dim parameters() As Microsoft.Reporting.WebForms.ReportParameter = {varMeterList, DateType, DateStart, DateEnd}
        serverReport.SetParameters(parameters)


    End Sub


End Class
