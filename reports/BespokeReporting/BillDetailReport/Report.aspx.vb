﻿Imports clsReportServerCredentials


Partial Class reports_Financial_ConsumptionandCost
    Inherits System.Web.UI.Page



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim QSperiod As String = Request.QueryString("period")
        Dim QScustid As String = Request.QueryString("custid")
        Dim QSsupplier As String = Request.QueryString("supplier")

        Me.TextBox1.Text = QSperiod
        Me.TextBox2.Text = QScustid
        Me.TextBox3.Text = QSsupplier

        Me.frmReport.Visible = True

        'Set the processing mode for the ReportViewer to Remote
        myReportViewer.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Remote

        Dim serverReport As Microsoft.Reporting.WebForms.ServerReport
        serverReport = myReportViewer.ServerReport

        serverReport.ReportServerCredentials = New clsReportServerCredentials("administrator", "chicago", "uml")

        'Set the report server URL and report path
        serverReport.ReportServerUrl = _
            New Uri("http://10.44.5.205/reportserver")
        serverReport.ReportPath = "/Portal Reporting/ElectricityBillExport"

        Dim varperiod As New Microsoft.Reporting.WebForms.ReportParameter()
        varperiod.Name = "period"
        varperiod.Values.Add(QSperiod)

        Dim varcustid As New Microsoft.Reporting.WebForms.ReportParameter()
        varcustid.Name = "custid"
        varcustid.Values.Add(QScustid)

        Dim varsupplier As New Microsoft.Reporting.WebForms.ReportParameter()
        varsupplier.Name = "supplier"
        varsupplier.Values.Add(QSsupplier)

        'Set the report parameters for the report
        Dim parameters() As Microsoft.Reporting.WebForms.ReportParameter = {varperiod, varcustid, varsupplier}
        serverReport.SetParameters(parameters)


    End Sub


End Class
