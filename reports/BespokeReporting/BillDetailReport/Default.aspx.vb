Imports Dundas.Charting.WebControl
Imports System.Drawing

Partial Class reports_onlinegraph_Default
    Inherits System.Web.UI.Page

    Public Sub ThickBox()


        If Me.lbDate.SelectedIndex = -1 Then
            Me.lnkNewLink.NavigateUrl = ""
            Me.lnkNewLink.Text = ""
            Me.lnkNewLink.ImageUrl = "../../../assets/Schneider/buttons-myreports-general-generatereport-disabled.png"
            Me.lnkNewLink.CssClass = ""
            Me.lnkNewLink.ToolTip = ""
        ElseIf Me.lbDate.SelectedItem.Value = "Please Select a Supplier" Then
            Me.lnkNewLink.NavigateUrl = ""
            Me.lnkNewLink.Text = ""
            Me.lnkNewLink.ImageUrl = "../../../assets/Schneider/buttons-myreports-general-generatereport-disabled.png"
            Me.lnkNewLink.CssClass = ""
            Me.lnkNewLink.ToolTip = ""
        ElseIf Me.lbDate.SelectedItem.Value.StartsWith("No Bills in for this supplier") Then
            Me.lnkNewLink.NavigateUrl = ""
            Me.lnkNewLink.Text = ""
            Me.lnkNewLink.ImageUrl = "../../../assets/Schneider/buttons-myreports-general-generatereport-disabled.png"
            Me.lnkNewLink.CssClass = ""
            Me.lnkNewLink.ToolTip = ""
        ElseIf Me.lbSupplier.SelectedIndex = -1 Then
            Me.lnkNewLink.NavigateUrl = ""
            Me.lnkNewLink.Text = ""
            Me.lnkNewLink.ImageUrl = "../../../assets/Schneider/buttons-myreports-general-generatereport-disabled.png"
            Me.lnkNewLink.CssClass = ""
            Me.lnkNewLink.ToolTip = ""
        ElseIf Request.QueryString("method") <> "Customer" Then
            Me.lnkNewLink.NavigateUrl = ""
            Me.lnkNewLink.Text = ""
            Me.lnkNewLink.ImageUrl = "../../../assets/Schneider/buttons-myreports-general-generatereport-disabled.png"
            Me.lnkNewLink.CssClass = ""
            Me.lnkNewLink.ToolTip = ""
        Else

            Dim strCustID As String = Request.QueryString("custid")
            Dim strSupplier As String = Me.lbSupplier.SelectedItem.Value
            Dim strPeriod As String = Me.lbDate.SelectedItem.Value

            Me.lnkNewLink.NavigateUrl = "report.aspx?custid=" + strCustID + "&supplier=" + strSupplier + "&period=" + strPeriod + "&?keepThis=true&TB_iframe=true&height=550&width=850"
            Me.lnkNewLink.Text = "Generate Report"
            Me.lnkNewLink.ImageUrl = "../../../assets/Schneider/buttons-myreports-general-generatereport.png"
            Me.lnkNewLink.CssClass = "thickbox"
            Me.lnkNewLink.ToolTip = "Bill Detail Report"
        End If


    End Sub
    Public Overloads Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)

        ' Confirms that an HtmlForm control is rendered for the specified ASP.NET server control at run time.

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        Try
            If Not IIf(Core.data_select_value("select bitAllowMyReports from tblUsers where intUserPK = " & MySession.UserID) = "True", True, False) Then
                Response.Redirect(Core.data_select_value("select RawHomeURL from vwUsers where ID = " & MySession.UserID), True)
            End If

            If Request.QueryString("custid") Is Nothing Then
                Response.Redirect(Core.TreeviewRedirectURL(), True)
            End If

            If Not Page.IsPostBack = True Then

                Dim description As HtmlMeta = New HtmlMeta()
                description.Name = "description"
                description.Content = "Utility Masters - Bill Detail Report"
                Dim keywords As HtmlMeta = New HtmlMeta()
                keywords.Name = "keywords"
                keywords.Content = "Bill Detail Report"
                Page.Header.Controls.Add(description)
                Page.Header.Controls.Add(keywords)
                LoadAvailableSuppliers()
                LoadAvailableDates()

            End If

            ThickBox()

            Me.lblDescription.Text = Request.QueryString("method").ToString
            Me.lblName.Text = Core.data_select_value("EXEC spSites_MAP_CurentSelection " & MySession.UserID & ", '" & Request.QueryString("method") & "', " & Request.QueryString("custid"))

        Catch ex As Exception
            Me.lblDescription.Text = ex.Message
        End Try


    End Sub

    Private Sub LoadAvailableSuppliers()

        If Request.QueryString("method") = "Customer" Then

            Dim strSQL As String = "EXEC [portal.utilitymasters.co.uk].dbo.usp_MyReports_Bespoke_BizSpace_ElecBillDetails_Param_Supplier " & Request.QueryString("custid")
            With Me.lbSupplier
                .Items.Clear()
                .DataSource = Core.data_select(strSQL)
                .DataValueField = "Supplier_Name"
                .DataTextField = "Supplier_Name"
                .DataBind()
            End With


        End If
    End Sub

    Protected Sub lbSupplier_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbSupplier.SelectedIndexChanged
        LoadAvailableDates()
        ThickBox()
    End Sub

    Private Sub LoadAvailableDates()

        If Me.lbSupplier.SelectedIndex > -1 Then

            Dim intCustomerFK As Integer = Request.QueryString("custid")
            Dim strSupplier As String = Me.lbSupplier.SelectedItem.Value
            Dim strSQL As String = ""

            strSQL &= "EXEC [portal.utilitymasters.co.uk].dbo.usp_MyReports_Bespoke_BizSpace_ElecBillDetails_Param_Dates " & intCustomerFK & ", '" & strSupplier & "'"

            With Me.lbDate
                .Items.Clear()
                .DataSource = Core.data_select(strSQL)
                .DataValueField = "dtmValue"
                .DataTextField = "varText"
                .DataBind()
            End With

        Else
            Me.lbDate.Items.Clear()
            Me.lbDate.Items.Add("Please Select a Supplier")
        End If

        If Me.lbDate.Items.Count = 0 Then
            Me.lbDate.Items.Clear()
            Me.lbDate.Items.Add("No Bills in for this supplier > November 2010")
        End If
        Me.lbDate.SelectedIndex = 0

    End Sub



    Protected Sub lbDate_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbDate.SelectedIndexChanged
        ThickBox()
    End Sub

End Class
