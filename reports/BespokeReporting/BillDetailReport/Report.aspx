﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Report.aspx.vb" Inherits="reports_Financial_ConsumptionandCost" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=9.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>SEC Report</title>
    <link rel="stylesheet" href="../../../modalbox/modalreport.css" type="text/css" media="screen" />   
    <script language="javascript" type="text/javascript"  >
        function showmj(what, clearme) {
            if (clearme == "Y")
            { document.getElementById("HJ").innerHTML = ""; }
            else
            { document.getElementById("HJ").innerHTML = what; }
        }
</script>
</head>
<body>
    <form id="form1" runat="server">

    
  
        <div  class="navgeneral">
            <div id="HJ" align="center" style="text-align:center;position:absolute;top:10px;left:5px;font-family:Arial;font-size:12px;color:Blue;"></div>
            <div class="CompanySite" onmouseout="javascript:showmj('','Y')">
              <ul class="styleformytreeview" runat="server" id="navOnlineReports">  </ul> 
            </div>
        </div>
        
        
        <div style="position:absolute; left:0; top: 50; height:520px; background-color:White; " id="frmReport" runat="server" visible="false">
            <table style="margin-left: 0px; margin-right: auto; z-index:100;">
                <tr>
                    <td colspan="4">

                    
                    <asp:TextBox ID="TextBox1" runat="server" Visible="true"></asp:TextBox>
                    <asp:TextBox ID="TextBox2" runat="server" Visible="true"></asp:TextBox>
                    <asp:TextBox ID="TextBox3" runat="server" Visible="true"></asp:TextBox>
                    <asp:TextBox ID="TextBox4" runat="server" Visible="false"></asp:TextBox>

                        <rsweb:ReportViewer ID="myReportViewer" Height="500px" AsyncRendering="true" ShowPrintButton="false" runat="server" Width="850px" ShowRefreshButton="false" ShowToolBar="true" ShowZoomControl="false" ShowParameterPrompts="false" ShowBackButton="false" ShowCredentialPrompts="false" ShowFindControls="false" ShowPageNavigationControls="false" ShowPromptAreaButton="false">
                        </rsweb:ReportViewer> 
                    </td>
                </tr>
            </table>
        </div>   

    </form>
</body>
</html>
