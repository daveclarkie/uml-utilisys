Imports Dundas.Charting.WebControl
Imports System.Drawing

Partial Class reports_SitePortfolioReport_Default
    Inherits System.Web.UI.Page

    Public Sub ThickBox()

        If Request.QueryString("method") <> "Customer" Then
            Me.lnkNewLink.NavigateUrl = ""
            Me.lnkNewLink.Text = ""
            Me.lnkNewLink.ImageUrl = "../../../assets/Schneider/buttons-myreports-general-generatereport-disabled.png"
            Me.lnkNewLink.CssClass = ""
            Me.lnkNewLink.ToolTip = ""
        Else

            Dim strCustID As String = Request.QueryString("custid")

            Me.lnkNewLink.NavigateUrl = "report.aspx?customer_fk=" + strCustID + "&?keepThis=true&TB_iframe=true&height=550&width=850"
            Me.lnkNewLink.Text = "Generate Report"
            Me.lnkNewLink.ImageUrl = "../../../assets/Schneider/buttons-myreports-general-generatereport.png"
            Me.lnkNewLink.CssClass = "thickbox"
            Me.lnkNewLink.ToolTip = "Site Portfolio Report"
        End If


    End Sub
    Public Overloads Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)

        ' Confirms that an HtmlForm control is rendered for the specified ASP.NET server control at run time.

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        Try
            If Not IIf(Core.data_select_value("select bitAllowMyReports from tblUsers where intUserPK = " & MySession.UserID) = "True", True, False) Then
                Response.Redirect(Core.data_select_value("select RawHomeURL from vwUsers where ID = " & MySession.UserID), True)
            End If

            If Request.QueryString("custid") Is Nothing Then
                Response.Redirect(Core.TreeviewRedirectURL(), True)
            End If

            If Not Page.IsPostBack = True Then

                Dim description As HtmlMeta = New HtmlMeta()
                description.Name = "description"
                description.Content = "Utility Masters - Site Portfolio Report"
                Dim keywords As HtmlMeta = New HtmlMeta()
                keywords.Name = "keywords"
                keywords.Content = "Site Portfolio Report"
                Page.Header.Controls.Add(description)
                Page.Header.Controls.Add(keywords)
            End If

            ThickBox()

            Me.lblDescription.Text = Request.QueryString("method").ToString
            Me.lblName.Text = Core.data_select_value("EXEC spSites_MAP_CurentSelection " & MySession.UserID & ", '" & Request.QueryString("method") & "', " & Request.QueryString("custid"))

        Catch ex As Exception
            Me.lblDescription.Text = ex.Message
        End Try


    End Sub

End Class
