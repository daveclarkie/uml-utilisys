<%@ Page Language="VB" MasterPageFile="~/landing.master" AutoEventWireup="false" CodeFile="Default.aspx.vb" Inherits="reports_SitePortfolioReport_Default" title="UTILISYS - Site Portfolio Report" %>

<%@ Register Assembly="DundasWebChart" Namespace="Dundas.Charting.WebControl" TagPrefix="DCWC" %>
<asp:Content ID="Content1" ContentPlaceHolderID="content_right" Runat="Server">

        <script type="text/javascript" src="../../../ThickBox/jquery-1.3.2.min.js"></script>
        <script type="text/javascript" src="../../../ThickBox/thickbox.js"></script>
        <link rel="stylesheet" href="../../../ThickBox/thickbox.css" type="text/css" media="screen" />   
        
<form id="Form1" runat="server">

    <div class="content_back general">
            <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td style="width:200px;">
                        <asp:Label ID="lblDescription" Text="Description" runat="server" Font-Bold="true" Width="150px"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblName" Text="Name" runat="server"></asp:Label>
                    </td>
                </tr> <!-- Company selection -->
                
                <tr>
                    <td >
                        <asp:Label ID="lblReportName" Text="Selected Report" runat="server" Font-Bold="true" Width="150px"></asp:Label>
                    </td>
                    <td>
                        Site Portfolio Report
                    </td>
                </tr> <!-- Report Name -->
                                
                <tr style="height:25px;">
                    <td></td>
                </tr>

                <tr>
                    <td></td>
                    <td>
                        <asp:HyperLink  ID="lnkNewLink" runat="server" CssClass="thickbox"></asp:HyperLink>
                    </td>
                </tr> <!-- Run button -->  
                   
                <tr>
                    <td></td>
                    <td>
                        <asp:label  ID="lblError" runat="server"></asp:label>
                    </td>
                </tr> <!-- Error message -->               
            </table>
        </div>

</form>
</asp:Content>

