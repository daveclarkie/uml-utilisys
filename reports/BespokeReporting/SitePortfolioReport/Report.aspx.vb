﻿Imports clsReportServerCredentials


Partial Class reports_SitePortfolioReport_Report
    Inherits System.Web.UI.Page



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim QScustid As String = Request.QueryString("customer_fk")
        
        Me.TextBox1.Text = QScustid

        Me.frmReport.Visible = True

        'Set the processing mode for the ReportViewer to Remote
        myReportViewer.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Remote

        Dim serverReport As Microsoft.Reporting.WebForms.ServerReport
        serverReport = myReportViewer.ServerReport

        serverReport.ReportServerCredentials = New clsReportServerCredentials("administrator", "chicago", "uml")

        'Set the report server URL and report path
        serverReport.ReportServerUrl = _
            New Uri("http://10.44.5.205/reportserver")
        serverReport.ReportPath = "/Portal Reporting/Site Portfolio Report"

        Dim varcustid As New Microsoft.Reporting.WebForms.ReportParameter()
        varcustid.Name = "customer_fk"
        varcustid.Values.Add(QScustid)

        'Set the report parameters for the report
        Dim parameters() As Microsoft.Reporting.WebForms.ReportParameter = {varcustid}
        serverReport.SetParameters(parameters)


    End Sub


End Class
