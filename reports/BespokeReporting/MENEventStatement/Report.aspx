<%@ Page Language="VB" StyleSheetTheme="GridViewTheme" AutoEventWireup="false" CodeFile="Report.aspx.vb" Inherits="reports_BespokeReporting_MENEventStatement_Report" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>MEN Event Statement Report</title>
</head>
<body>  
    <form id="form1" runat="server">
    <div>
        <table>
            <tr>
                <td>
                    <asp:Image ID="imgSMGLogo" runat="server" ImageUrl="~/assets/CustomerLogo_SMGEurope.gif" />
                </td>
                <td>
                    <table>
                        <tr>
                            <td>
                                <asp:Label ID="lblReportName" runat="server" Font-Bold="true" Font-Names="verdana" Font-Size="24pt" ></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label id="lblDateRange" runat="server"  Font-Names="verdana" Font-Size="10pt" ></asp:Label>
                            </td>
                        </tr>
                    </table>
                    
                </td>
            </tr>
        </table>
        
        
        <asp:GridView id="gridview1" runat="server" Width="680px" AutoGenerateColumns="false" SkinID="Professional" >
            <Columns>
                <asp:BoundField HeaderText="Meter Point" DataField="varMeterID" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" /> 
                <asp:BoundField HeaderText="Units (kWh)" DataField="fltUnit" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:#,##0.##;(#,##0.##);0}" /> 
                <asp:BoundField HeaderText="Rate (p/kWh)" DataField="fltRate" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" /> 
                <asp:BoundField HeaderText="Cost" DataField="fltCost" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:�#,##0.00;(�#,##0.00);0}" /> 
            </Columns>
        </asp:GridView>    
        
        <asp:label iD="lblDisclaimer" runat="server" Font-Bold="true" Font-Names="verdana" Font-Size="6pt" width="650px" ></asp:label>
    </div>
    </form>
</body>
</html>
