
Partial Class reports_BespokeReporting_MENEventStatement_Default
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IIf(Core.data_select_value("select bitAllowMyReports from tblUsers where intUserPK = " & MySession.UserID) = "True", True, False) Then
            Response.Redirect(Core.data_select_value("select RawHomeURL from vwUsers where ID = " & MySession.UserID), True)
        End If

        If Request.QueryString("custid") Is Nothing Then
            Dim strSQL As String = "EXEC spPortal_TREEVIEW_TopLevel " & MySession.UserID
            Dim myConnection As New OleDb.OleDbConnection(Core.CurrentConnectionString)

            Dim myCommand As New OleDb.OleDbCommand(strSQL, myConnection)
            myCommand.Connection.Open()
            myCommand.CommandTimeout = 180
            Dim Reader As OleDb.OleDbDataReader = myCommand.ExecuteReader(CommandBehavior.CloseConnection)

            If Reader.HasRows Then
                Reader.Read()
                If Reader("Type") = "Company" Then
                    Response.Redirect("default.aspx?custid=" & Reader("id") & "&method=Customer", True)
                Else
                    Response.Redirect("default.aspx?custid=" & Reader("id") & "&method=" & Reader("Type"), True)
                End If
                Reader.Close()
            End If
        End If
        PopulateGridView()

        If Not Page.IsPostBack = True Then

            MeterPoints_Show("ALL")
            PopulateDropDownLists()
            Dim description As HtmlMeta = New HtmlMeta()
            description.Name = "description"
            description.Content = "UTILISYS - MEN Event Statement"
            Dim keywords As HtmlMeta = New HtmlMeta()
            keywords.Name = "keywords"
            keywords.Content = "UTILISYS - MEN Event Statement"
            Page.Header.Controls.Add(description)
            Page.Header.Controls.Add(keywords)
            PopulateGridView()

            With Me.ddlStartYear
                .DataSource = Core.data_select(" SELECT DISTINCT DATEPART(YEAR, PK_DATE) as dtmYear FROM UML_CMS.dbo.DIM_Dates WHERE pk_date < getdate()+1 ORDER BY dtmYear desc ")
                .DataValueField = "dtmYear"
                .DataTextField = "dtmYear"
                .DataBind()
            End With


            With Me.ddlEndYear
                .DataSource = Core.data_select(" SELECT DISTINCT DATEPART(YEAR, PK_DATE) as dtmYear FROM UML_CMS.dbo.DIM_Dates WHERE pk_date < getdate()+1 ORDER BY dtmYear desc ")
                .DataValueField = "dtmYear"
                .DataTextField = "dtmYear"
                .DataBind()
            End With

            Core.data_execute_nonquery("TRUNCATE TABLE [portal.utilitymasters.co.uk].dbo.TEMP_MENBespokeEventStatement_Table1")

            MeterPoints_Show(Me.rblMeterFilter.SelectedItem.Value)
            PopulateGridView()

            Dim DateToday As DateTime
            Dim MonthName As String

            DateToday = Date.Today

            MonthName = DateToday.ToString("MMMM")


            Me.ddlStartMonth.SelectedValue = MonthName
            Me.ddlEndMonth.SelectedValue = MonthName

        End If


        ReadOnlyTheOptions()

        Me.lblDescription.Text = Request.QueryString("method").ToString
        Me.lblName.Text = Core.data_select_value("EXEC spSites_MAP_CurentSelection " & MySession.UserID & ", '" & Request.QueryString("method") & "', " & Request.QueryString("custid"))

        Opennewwindow(btnRunReport, "Report.aspx?strReportName=" & Me.txtReportName.Text.ToString)
    End Sub
    Private Sub PopulateDropDownLists()

        With Me.ddlMeterGrouping
            .Items.Clear()
            .DataSource = Core.data_select("SELECT 0 as ID, '-- select a group / create a new group --' as varName, 0 as intOrder UNION SELECT DISTINCT intMeterGrouping_NamePK, varMeterGrouping_Name, 1 FROM [portal.utilitymasters.co.uk].dbo.tblMeterGrouping_Name WHERE intCustomerFK = " & Request.QueryString("custid") & " ORDER BY intOrder, varName ASC")
            .DataValueField = "ID"
            .DataTextField = "varName"
            .DataBind()
        End With

        If Me.ddlMeterGrouping.Items.Count > 1 Then Me.ddlMeterGrouping.Enabled = True Else Me.ddlMeterGrouping.Enabled = False

        If Me.ddlMeterGrouping.Items.Count = 1 Then Me.ddlMeterGrouping.SelectedItem.Text = "-- no groups configured - create a new group --"

        Me.btnConfigureMeterGrouping.Text = "create"

    End Sub
    Public Overloads Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)

        ' Confirms that an HtmlForm control is rendered for the specified ASP.NET server control at run time.

    End Sub

    Private Sub MeterPoints_Show(ByVal Type As String)

        With Me.lbAvailableMeters
            .DataSource = Core.data_select(" EXEC [portal.utilitymasters.co.uk].dbo.usp_MeterPoints_Display " & MySession.UserID & ", '" & Request.QueryString("method") & "', " & Request.QueryString("custid") & ",'" & Type & "', '" & ListAssignedMeters() & "', '" & Me.txtCustomFilter.Text.ToString & "', " & Me.rblHalfHourMeterOnly.SelectedItem.Value.ToString)
            .DataValueField = "ID"
            .DataTextField = "varNiceName"
            .DataBind()
        End With


    End Sub

    Private Function ListAssignedMeters() As String
        Dim s As String = ""

        With Me.lbAssignedMeters
            .DataSource = Core.data_select("SELECT varMeterID FROM [portal.utilitymasters.co.uk].dbo.TEMP_MENBespokeEventStatement_Table1")
            .DataValueField = "varMeterID"
            .DataTextField = "varMeterID"
            .DataBind()
        End With

        If Me.lbAssignedMeters.Items.Count > 0 Then
            For Each x As ListItem In lbAssignedMeters.Items
                s &= "," & x.Value
            Next
            If s.Length > 0 Then
                s = s.Remove(0, 1)
            End If
        End If

        Return s

    End Function


    Private Sub GroupingMeterPoints_Show(ByVal Type As String)


        With Me.lbMeterGroupingAvailable
            .DataSource = Core.data_select(" EXEC [portal.utilitymasters.co.uk].dbo.usp_MeterPoints_Display " & MySession.UserID & ", '" & Request.QueryString("method") & "', " & Request.QueryString("custid") & ",'" & Type & "', '" & GroupingListAssignedMeters() & "'")
            ', '" & Me.txtCustomFilter.Text.ToString & "', " & Me.rblHalfHourMeterOnly.SelectedItem.Value.ToString)
            .DataValueField = "ID"
            .DataTextField = "varNiceName"
            .DataBind()
        End With



    End Sub


    Private Function GroupingListAssignedMeters() As String
        Dim s As String = ""
        Dim intMeterGroupingID As Integer = Me.lblMeterGroupingNameID.Text


        With Me.lbMeterGroupingAssigned
            .DataSource = Core.data_select("SELECT varMeterViewID, varNiceName FROM [portal.utilitymasters.co.uk].dbo.tblMeterGrouping_Meters INNER JOIN uvw_DisplayAllMeterPoints ON ID = varMeterViewID WHERE intMeterGrouping_NameFK = " & intMeterGroupingID)
            .DataValueField = "varMeterViewID"
            .DataTextField = "varNiceName"
            .DataBind()
        End With

        If Me.lbMeterGroupingAssigned.Items.Count > 0 Then
            For Each x As ListItem In lbMeterGroupingAssigned.Items
                s &= "," & x.Value
            Next
            If s.Length > 0 Then
                s = s.Remove(0, 1)
            End If
        End If

        Return s

    End Function



    Private Sub PopulateGridView()
        Dim myCommand2 = New SqlDataAdapter("SELECT DISTINCT intIdentifierPK,  SUBSTRING(varMeterName,6,30) + '...' as varMeterName, varRate, varStartDate, varEndDate FROM [portal.utilitymasters.co.uk].dbo.TEMP_MENBespokeEventStatement_Table1", Core.ConnectionString)
        Dim ds As Data.DataSet = New Data.DataSet
        myCommand2.Fill(ds)
        gridview1.DataSource = ds
        gridview1.DataBind()
        myCommand2.dispose()
        ReadOnlyTheOptions()

    End Sub

    Protected Sub btnAddtoStatement_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddtoStatement.Click
        For Each x As ListItem In lbAvailableMeters.Items
            If x.Selected Then

                If Core.data_select_value("SELECT COUNT(*) FROM [portal.utilitymasters.co.uk].dbo.TEMP_MENBespokeEventStatement_Table1 WHERE varMeterID = '" & x.Value & "'") = 0 Then
                    Dim strInsertSQL As String = "INSERT INTO [portal.utilitymasters.co.uk].dbo.TEMP_MENBespokeEventStatement_Table1 "
                    strInsertSQL &= "(varMeterID, varMeterName, varRate, varStartDate, varEndDate) "
                    strInsertSQL &= " VALUES ('"
                    strInsertSQL &= x.Value & "', '"
                    strInsertSQL &= x.Text & "', '"
                    strInsertSQL &= Me.txtUnitRate.Text & "', '"
                    strInsertSQL &= Me.ddlStartDay.SelectedItem.Value & " " & Me.ddlStartMonth.SelectedItem.Value & " " & Me.ddlStartYear.SelectedItem.Value & " " & Me.ddlStartHour.SelectedItem.Value & ":" & Me.ddlStartMinute.SelectedItem.Value & "', '"
                    strInsertSQL &= Me.ddlEndDay.SelectedItem.Value & " " & Me.ddlEndMonth.SelectedItem.Value & " " & Me.ddlEndYear.SelectedItem.Value & " " & Me.ddlEndHour.SelectedItem.Value & ":" & Me.ddlEndMinute.SelectedItem.Value & "')"
                    Core.data_execute_nonquery(strInsertSQL)
                End If
            End If
        Next
        MeterPoints_Show(Me.rblMeterFilter.SelectedItem.Value)
        PopulateGridView()
        
    End Sub

    Protected Sub btnClearStatement_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClearStatement.Click
        Core.data_execute_nonquery("TRUNCATE TABLE [portal.utilitymasters.co.uk].dbo.TEMP_MENBespokeEventStatement_Table1")
        Me.ddlMeterGrouping.SelectedIndex = 0

        MeterPoints_Show(Me.rblMeterFilter.SelectedItem.Value)
        PopulateGridView()

        
    End Sub

    Public Shared Sub Opennewwindow(ByVal Opener As System.Web.UI.WebControls.WebControl, ByVal PagePath As String)
        Dim Clientscript As String
        Clientscript = "window.open('" & PagePath & "')"
        Opener.Attributes.Add("Onclick", Clientscript)
    End Sub

    Private Sub ReadOnlyTheOptions()

        Dim intRowCount As Integer = Core.data_select_value("SELECT count(*) FROM [portal.utilitymasters.co.uk].dbo.TEMP_MENBespokeEventStatement_Table1")

        If intRowCount > 0 Then
            Me.txtUnitRate.Enabled = False
            Me.ddlStartDay.Enabled = False
            Me.ddlStartMonth.Enabled = False
            Me.ddlStartYear.Enabled = False
            Me.ddlStartHour.Enabled = False
            Me.ddlStartMinute.Enabled = False
            Me.ddlEndDay.Enabled = False
            Me.ddlEndMonth.Enabled = False
            Me.ddlEndYear.Enabled = False
            Me.ddlEndHour.Enabled = False
            Me.ddlEndMinute.Enabled = False
        Else
            Me.txtUnitRate.Enabled = True
            Me.ddlStartDay.Enabled = True
            Me.ddlStartMonth.Enabled = True
            Me.ddlStartYear.Enabled = True
            Me.ddlStartHour.Enabled = True
            Me.ddlStartMinute.Enabled = True
            Me.ddlEndDay.Enabled = True
            Me.ddlEndMonth.Enabled = True
            Me.ddlEndYear.Enabled = True
            Me.ddlEndHour.Enabled = True
            Me.ddlEndMinute.Enabled = True
        End If


    End Sub

    Protected Sub gridview1_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gridview1.RowDeleting

        Core.data_execute_nonquery("DELETE FROM [portal.utilitymasters.co.uk].dbo.TEMP_MENBespokeEventStatement_Table1 WHERE intIdentifierPK = '" & gridview1.Rows(e.RowIndex).Cells(1).Text.ToString & "'")

        MeterPoints_Show(Me.rblMeterFilter.SelectedItem.Value)
        PopulateGridView()

    End Sub

    Protected Sub ddlMeterGrouping_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlMeterGrouping.SelectedIndexChanged

        Dim intSelectedGroup As Integer = 0
       
        If Me.ddlMeterGrouping.SelectedItem.Value > 0 Then intSelectedGroup = Me.ddlMeterGrouping.SelectedItem.Value

        If intSelectedGroup > 0 Then
            Me.btnSelectMeterGrouping.Enabled = True
            Me.btnConfigureMeterGrouping.Text = "config"
        Else
            Me.btnSelectMeterGrouping.Enabled = False
            Me.btnConfigureMeterGrouping.Text = "create"
        End If



    End Sub


    Protected Sub btnSelectMeterGrouping_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSelectMeterGrouping.Click

        Dim strInsertSQL As String = ""
        Dim intSelectedGroup As Integer = 0

        If Me.ddlMeterGrouping.SelectedItem.Value > 0 Then intSelectedGroup = Me.ddlMeterGrouping.SelectedItem.Value

        If intSelectedGroup > 0 Then

            strInsertSQL = "  TRUNCATE TABLE [portal.utilitymasters.co.uk].dbo.TEMP_MENBespokeEventStatement_Table1 ;"
            strInsertSQL &= " INSERT INTO [portal.utilitymasters.co.uk].dbo.TEMP_MENBespokeEventStatement_Table1 "
            strInsertSQL &= "(varMeterID, varMeterName, varRate, varStartDate, varEndDate) "
            strInsertSQL &= " SELECT c.ID"
            strInsertSQL &= ", c.varNiceName  "
            strInsertSQL &= ", '" & Me.txtUnitRate.Text & "'"
            strInsertSQL &= ", '" & Me.ddlStartDay.SelectedItem.Value & " " & Me.ddlStartMonth.SelectedItem.Value & " " & Me.ddlStartYear.SelectedItem.Value & " " & Me.ddlStartHour.SelectedItem.Value & ":" & Me.ddlStartMinute.SelectedItem.Value & "'"
            strInsertSQL &= ", '" & Me.ddlEndDay.SelectedItem.Value & " " & Me.ddlEndMonth.SelectedItem.Value & " " & Me.ddlEndYear.SelectedItem.Value & " " & Me.ddlEndHour.SelectedItem.Value & ":" & Me.ddlEndMinute.SelectedItem.Value & "'"
            strInsertSQL &= " FROM [portal.utilitymasters.co.uk].dbo.tblMeterGrouping_Name a"
            strInsertSQL &= "   INNER JOIN [portal.utilitymasters.co.uk].dbo.tblMeterGrouping_Meters b ON a.intMeterGrouping_NamePK = b.intMeterGrouping_NameFK"
            strInsertSQL &= "   INNER JOIN [UML_CMS].dbo.uvw_Admin_DisplayAllMeterPoints c ON b.varMeterViewID = c.ID COLLATE SQL_Latin1_General_CP1_CI_AS"
            strInsertSQL &= " WHERE(intMeterGrouping_NamePK = " & intSelectedGroup & ")"

        Else
            strInsertSQL = "TRUNCATE TABLE [portal.utilitymasters.co.uk].dbo.TEMP_MENBespokeEventStatement_Table1"
        End If


        Core.data_execute_nonquery(strInsertSQL)

        MeterPoints_Show(Me.rblMeterFilter.SelectedItem.Value)
        PopulateGridView()
    End Sub

    Protected Sub btnConfigureMeterGrouping_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnConfigureMeterGrouping.Click
        If Me.pnlMain.Visible = True Then
            Me.pnlMain.Visible = False
            Me.pnlConfigureMeterGrouping.Visible = True
        Else
            Me.pnlMain.Visible = True
            Me.pnlConfigureMeterGrouping.Visible = False
        End If

        ConfigureMeterGrouping(Me.ddlMeterGrouping.SelectedItem.Value)

    End Sub


    Private Sub ConfigureMeterGrouping(ByVal intMeterGroupingID As Integer)

        Me.txtMeterGroupingName.Text = Nothing

        Me.imgMeterGroupingName.Visible = False


        If intMeterGroupingID = 0 Then
            'Me.lblConfigMeterGroupingInfo.Text = "New Grouping"
            Me.btnMeterGroupingName.Text = "create"
            Me.btnMeterGroupingNameDelete.Visible = False
        Else
            'Me.lblConfigMeterGroupingInfo.Text = Core.data_select_value(" SELECT varMeterGrouping_Name FROM [portal.utilitymasters.co.uk].dbo.tblMeterGrouping_Name WHERE intMeterGrouping_NamePK = " & intMeterGroupingID)
            Me.btnMeterGroupingName.Text = "update"
            Me.btnMeterGroupingNameDelete.Visible = True
        End If

        Me.lblMeterGroupingNameID.Text = intMeterGroupingID

        If intMeterGroupingID > 0 Then
            Me.pnlAssignMeterGrouping.Visible = True

            Me.txtMeterGroupingName.Text = Core.data_select_value(" SELECT varMeterGrouping_Name FROM [portal.utilitymasters.co.uk].dbo.tblMeterGrouping_Name WHERE intMeterGrouping_NamePK = " & intMeterGroupingID)

            GroupingMeterPoints_Show("All")

        Else
            Me.pnlAssignMeterGrouping.Visible = False
        End If

    End Sub

    Protected Sub btnMeterGroupingName_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnMeterGroupingName.Click

        Me.txtMeterGroupingName.Text = Trim(Core.fn_NoSpecialCharacters(Me.txtMeterGroupingName.Text))

        Dim strMeterGroupingName As String = Me.txtMeterGroupingName.Text
        Dim intMeterGroupingID As Integer = Me.lblMeterGroupingNameID.Text
        Dim strSQLCriteria As String = ""

        strSQLCriteria = "select count(*) from [portal.utilitymasters.co.uk].dbo.tblMeterGrouping_Name WHERE intCustomerFK = " & Request.QueryString("custid") & " AND varMeterGrouping_Name = '" & strMeterGroupingName & "'"

        If intMeterGroupingID > 0 Then
            strSQLCriteria &= " AND intMeterGrouping_NamePK <> " & intMeterGroupingID
        End If


        If Core.CheckForNullInteger(Core.data_select_value(strSQLCriteria)) > 0 Then
            'NOT OK - Already Exists
            'Me.txtMeterGroupingName.BackColor = Drawing.Color.Red
            Me.imgMeterGroupingName.ImageUrl = "~/assets/Metering/smalcancel.png"

        ElseIf strMeterGroupingName.Length = 0 Then
            'NOT OK - No Name
            'Me.txtMeterGroupingName.BackColor = Drawing.Color.Red
            Me.imgMeterGroupingName.ImageUrl = "~/assets/Metering/smalcancel.png"

        Else
            ' OK
            'Me.txtMeterGroupingName.BackColor = Drawing.Color.LimeGreen
            Me.imgMeterGroupingName.ImageUrl = "~/assets/Metering/accepted_48.png"


            If intMeterGroupingID = 0 Then
                ' new group
                Dim strInsertSQL As String = "INSERT INTO [portal.utilitymasters.co.uk].dbo.tblMeterGrouping_Name(varMeterGrouping_Name, intCustomerFK, intUserCreatedFK) VALUES ('" & strMeterGroupingName & "', " & Request.QueryString("custid") & ", " & MySession.UserID & ") ;select scope_identity()"

                Dim intNewID As Integer = Core.data_select_value(strInsertSQL)
                ConfigureMeterGrouping(intNewID)
            Else
                ' existing group
                Core.data_execute_nonquery("UPDATE [portal.utilitymasters.co.uk].dbo.tblMeterGrouping_Name SET varMeterGrouping_Name = '" & strMeterGroupingName & "' WHERE intMeterGrouping_NamePK = " & intMeterGroupingID)
                ConfigureMeterGrouping(intMeterGroupingID)
            End If

        End If

        Me.imgMeterGroupingName.Visible = True


    End Sub

    Protected Sub btnMeterGroupingNameDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnMeterGroupingNameDelete.Click
        Dim intMeterGroupingID As Integer = Me.lblMeterGroupingNameID.Text

        If intMeterGroupingID > 0 Then

            Core.data_execute_nonquery("DELETE FROM [portal.utilitymasters.co.uk].dbo.tblMeterGrouping_Meters WHERE intMeterGrouping_NameFK = " & intMeterGroupingID)
            Core.data_execute_nonquery("DELETE FROM [portal.utilitymasters.co.uk].dbo.tblMeterGrouping_Name WHERE intMeterGrouping_NamePK = " & intMeterGroupingID)
            Me.pnlMain.Visible = True
            Me.pnlConfigureMeterGrouping.Visible = False
            PopulateDropDownLists()

        End If

    End Sub

    Protected Sub btnMeterGroupingAvailable_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnMeterGroupingAvailable.Click
        ' add meter

        Dim intMeterGroupingID As Integer = Me.lblMeterGroupingNameID.Text


        If Me.lbMeterGroupingAvailable.SelectedIndex > -1 Then
            Core.data_execute_nonquery("INSERT INTO tblMeterGrouping_Meters (intMeterGrouping_NameFK, varMeterViewID, intUserCreatedFK) VALUES (" & intMeterGroupingID & ", '" & Me.lbMeterGroupingAvailable.SelectedItem.Value & "', " & MySession.UserID & ")")

        End If

        GroupingMeterPoints_Show("All")
    End Sub

    Protected Sub btnMeterGroupingAssigned_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnMeterGroupingAssigned.Click
        ' remove meter

        Dim intMeterGroupingID As Integer = Me.lblMeterGroupingNameID.Text


        If Me.lbMeterGroupingAssigned.SelectedIndex > -1 Then
            Core.data_execute_nonquery("DELETE FROM tblMeterGrouping_Meters WHERE intMeterGrouping_NameFK = " & intMeterGroupingID & " AND varMeterViewID = '" & Me.lbMeterGroupingAssigned.SelectedItem.Value & "'")

        End If

        GroupingMeterPoints_Show("All")
    End Sub

    Protected Sub ImageButtonReturnToReport_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonReturnToReport.Click
        If Me.pnlMain.Visible = True Then
            Me.pnlMain.Visible = False
            Me.pnlConfigureMeterGrouping.Visible = True
        Else
            Me.pnlMain.Visible = True
            Me.pnlConfigureMeterGrouping.Visible = False
            PopulateDropDownLists()
        End If


    End Sub


    Protected Sub rblMeterFilter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rblMeterFilter.SelectedIndexChanged
        If Me.rblMeterFilter.SelectedItem.Value = "Custom" Then
            Me.txtCustomFilter.Text = Nothing
            Me.secFilter.Visible = True
        Else
            Me.txtCustomFilter.Text = Nothing
            Me.secFilter.Visible = False
        End If


        MeterPoints_Show(Me.rblMeterFilter.SelectedItem.Value)
    End Sub


    Protected Sub btnFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFilter.Click
        Me.txtCustomFilter.Text = Trim(Core.fn_NoSpecialCharacters(Me.txtCustomFilter.Text.ToString))
        MeterPoints_Show(Me.rblMeterFilter.SelectedItem.Value)

    End Sub

    Public Function fnNoSpecialCharacters(ByVal strTest As String) As String

        Dim sb As New System.Text.StringBuilder

        For Each ch As Char In strTest
            If Char.IsLetterOrDigit(ch) OrElse ch = " "c Then
                sb.Append(ch)
            End If
        Next

        strTest = sb.ToString

        Return strTest
    End Function

    Protected Sub rblHalfHourMeterOnly_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rblHalfHourMeterOnly.SelectedIndexChanged

        MeterPoints_Show(Me.rblMeterFilter.SelectedItem.Value)
    End Sub
End Class

