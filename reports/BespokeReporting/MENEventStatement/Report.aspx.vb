
Partial Class reports_BespokeReporting_MENEventStatement_Report
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Me.lblReportName.Text = Request.QueryString("strReportName")
        Me.lblDateRange.text = Core.data_select_value("SELECT TOP 1 '(' + varStartDate + '  to ' + varEndDate + ')' FROM [portal.utilitymasters.co.uk].dbo.TEMP_MENBespokeEventStatement_Table1")
        PopulateGridView()

        Me.lblDisclaimer.Text = "The unit cost per kWh is an average cost and is based on the contracted electricity day/night rates and associated costs."
        Me.lblDisclaimer.Text += "These associated costs include contracted supplier Transmission & Distribution Losses, standing charges and the Climate Change Levy; in addition some cost allowance associated with the maintenance of the MEN electricity distribution system and costs associated with the Carbon Reduction Commitment have also been included."

    End Sub

    Private Sub PopulateGridView()

        Dim myCommand2 = New SqlDataAdapter("EXEC [portal.utilitymasters.co.uk].dbo.usp_MyReports_Bespoke_MENEventStatement_Return_Report", Core.ConnectionString)
        Dim ds As Data.DataSet = New Data.DataSet
        myCommand2.Fill(ds)
        gridview1.DataSource = ds
        gridview1.DataBind()
        myCommand2.dispose()

    End Sub

    Protected Sub gridview1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gridview1.RowDataBound
        If e.Row.Cells(0).Text = "Total" Then
            e.Row.Font.Bold = True
        Else
            e.Row.Font.Bold = False
        End If
    End Sub
End Class
