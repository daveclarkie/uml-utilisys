<%@ Page Language="VB" StyleSheetTheme="GridViewTheme" MasterPageFile="~/landing.master" AutoEventWireup="false" CodeFile="Default.aspx.vb" Inherits="reports_BespokeReporting_MENEventStatement_Default" title="UTILISYS - MEN Event Statement" %>
<asp:Content ID="Content1" ContentPlaceHolderID="content_right" Runat="Server">

        <script type="text/javascript" src="../../../ThickBox/jquery-1.3.2.min.js"></script>
        <script type="text/javascript" src="../../../ThickBox/thickbox.js"></script> 
        <link rel="stylesheet" href="../../../ThickBox/thickbox.css" type="text/css" media="screen" />   
        
         
    <form id="Form1" runat="server" >
        <div class="content_back general">
            <asp:Panel ID="pnlMain" runat="server">
                <table cellpadding="0" cellspacing="0">
                <tr valign="middle">
                    <td style="width:100px;" >
                        <asp:Label ID="lblDescription" Text="Description" runat="server" Font-Bold="true"></asp:Label>
                    </td>
                    <td style="width:400px;" align="left" colspan="7">
                        <asp:Label ID="lblName" Text="Name" runat="server"></asp:Label>
                    </td>
                    <td style="width:150px;">
                        
                    </td>
                </tr> <!-- Company selection -->
                
                <tr valign="middle">
                    <td >
                        <b>Report</b>
                    </td>
                    <td align="left" colspan="7">
                        <asp:TextBox ID="txtReportName" runat="server" text="Event Name - Department" Width="350px" BackColor="#FFFFC0"></asp:TextBox>
                    </td>
                    <td>
                        
                    </td>
                </tr> <!-- Report Name -->
                
                <tr valign="middle">
                    <td >
                        <b>Unit Rate</b>
                    </td>
                    <td align="left" colspan="7">
                        <asp:textbox id="txtUnitRate" runat="server" Text="8" Width="25px" BackColor="#FFFFC0"></asp:textbox> p/kWh
                    </td>
                    <td>
                        
                    </td>
                </tr> <!-- Unit Rates -->
                
                <tr valign="middle">
                    <td valign="top" >
                        <b>Start</b>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlStartDay" runat="server" Width="50px" BackColor="#FFFFC0" AutoPostBack="true">
                            <asp:ListItem Value="01" Text="01"></asp:ListItem>
                            <asp:ListItem Value="02" Text="02"></asp:ListItem>
                            <asp:ListItem Value="03" Text="03"></asp:ListItem>
                            <asp:ListItem Value="04" Text="04"></asp:ListItem>
                            <asp:ListItem Value="05" Text="05"></asp:ListItem>
                            <asp:ListItem Value="06" Text="06"></asp:ListItem>
                            <asp:ListItem Value="07" Text="07"></asp:ListItem>
                            <asp:ListItem Value="08" Text="08"></asp:ListItem>
                            <asp:ListItem Value="09" Text="09"></asp:ListItem>
                            <asp:ListItem Value="10" Text="10"></asp:ListItem>
                            <asp:ListItem Value="11" Text="11"></asp:ListItem>
                            <asp:ListItem Value="12" Text="12"></asp:ListItem>
                            <asp:ListItem Value="13" Text="13"></asp:ListItem>
                            <asp:ListItem Value="14" Text="14"></asp:ListItem>
                            <asp:ListItem Value="15" Text="15"></asp:ListItem>
                            <asp:ListItem Value="16" Text="16"></asp:ListItem>
                            <asp:ListItem Value="17" Text="17"></asp:ListItem>
                            <asp:ListItem Value="18" Text="18"></asp:ListItem>
                            <asp:ListItem Value="19" Text="19"></asp:ListItem>
                            <asp:ListItem Value="20" Text="20"></asp:ListItem>
                            <asp:ListItem Value="21" Text="21"></asp:ListItem>
                            <asp:ListItem Value="22" Text="22"></asp:ListItem>
                            <asp:ListItem Value="23" Text="23"></asp:ListItem>
                            <asp:ListItem Value="24" Text="24"></asp:ListItem>
                            <asp:ListItem Value="25" Text="25"></asp:ListItem>
                            <asp:ListItem Value="26" Text="26"></asp:ListItem>
                            <asp:ListItem Value="27" Text="27"></asp:ListItem>
                            <asp:ListItem Value="28" Text="28"></asp:ListItem>
                            <asp:ListItem Value="29" Text="29"></asp:ListItem>
                            <asp:ListItem Value="30" Text="30"></asp:ListItem>
                            <asp:ListItem Value="31" Text="31"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlStartMonth" runat="server" Width="132px" BackColor="#FFFFC0" AutoPostBack="true">
                            <asp:ListItem Value="January" Text="January"></asp:ListItem>
                            <asp:ListItem Value="February" Text="February"></asp:ListItem>
                            <asp:ListItem Value="March" Text="March"></asp:ListItem>
                            <asp:ListItem Value="April" Text="April"></asp:ListItem>
                            <asp:ListItem Value="May" Text="May"></asp:ListItem>
                            <asp:ListItem Value="June" Text="June"></asp:ListItem>
                            <asp:ListItem Value="July" Text="July"></asp:ListItem>
                            <asp:ListItem Value="August" Text="August"></asp:ListItem>
                            <asp:ListItem Value="September" Text="September"></asp:ListItem>
                            <asp:ListItem Value="October" Text="October"></asp:ListItem>
                            <asp:ListItem Value="November" Text="November"></asp:ListItem>
                            <asp:ListItem Value="December" Text="December"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlStartYear" runat="server" Width="80px" BackColor="#FFFFC0" AutoPostBack="true"></asp:DropDownList>
                    </td>
                                
                    <td>
                        -
                    </td>
                                
                    <td>
                        <asp:DropDownList ID="ddlStartHour" runat="server" Width="50px" BackColor="#FFFFC0" AutoPostBack="true">
                            <asp:ListItem Value="00" Text="00"></asp:ListItem>
                            <asp:ListItem Value="01" Text="01"></asp:ListItem>
                            <asp:ListItem Value="02" Text="02"></asp:ListItem>
                            <asp:ListItem Value="03" Text="03"></asp:ListItem>
                            <asp:ListItem Value="04" Text="04"></asp:ListItem>
                            <asp:ListItem Value="05" Text="05"></asp:ListItem>
                            <asp:ListItem Value="06" Text="06"></asp:ListItem>
                            <asp:ListItem Value="07" Text="07"></asp:ListItem>
                            <asp:ListItem Value="08" Text="08"></asp:ListItem>
                            <asp:ListItem Value="09" Text="09"></asp:ListItem>
                            <asp:ListItem Value="10" Text="10"></asp:ListItem>
                            <asp:ListItem Value="11" Text="11"></asp:ListItem>
                            <asp:ListItem Value="12" Text="12"></asp:ListItem>
                            <asp:ListItem Value="13" Text="13"></asp:ListItem>
                            <asp:ListItem Value="14" Text="14"></asp:ListItem>
                            <asp:ListItem Value="15" Text="15"></asp:ListItem>
                            <asp:ListItem Value="16" Text="16"></asp:ListItem>
                            <asp:ListItem Value="17" Text="17"></asp:ListItem>
                            <asp:ListItem Value="18" Text="18"></asp:ListItem>
                            <asp:ListItem Value="19" Text="19"></asp:ListItem>
                            <asp:ListItem Value="20" Text="20"></asp:ListItem>
                            <asp:ListItem Value="21" Text="21"></asp:ListItem>
                            <asp:ListItem Value="22" Text="22"></asp:ListItem>
                            <asp:ListItem Value="23" Text="23"></asp:ListItem>
                        </asp:DropDownList>
                                    
                    </td>
                    <td style="width:10px;">
                        :
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlStartMinute" runat="server" Width="50px" BackColor="#FFFFC0" AutoPostBack="true">
                            <asp:ListItem Value="00" Text="00"></asp:ListItem>
                            <asp:ListItem Value="30" Text="30"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                                
                    <td style="width:250px;">
                                    
                    </td>
                </tr>  <!-- Start Dates -->
                                
                <tr>
                    <td>
                        <b>End</b>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlEndDay" runat="server" Width="50px" BackColor="#FFFFC0" AutoPostBack="true">
                            <asp:ListItem Value="01" Text="01"></asp:ListItem>
                            <asp:ListItem Value="02" Text="02"></asp:ListItem>
                            <asp:ListItem Value="03" Text="03"></asp:ListItem>
                            <asp:ListItem Value="04" Text="04"></asp:ListItem>
                            <asp:ListItem Value="05" Text="05"></asp:ListItem>
                            <asp:ListItem Value="06" Text="06"></asp:ListItem>
                            <asp:ListItem Value="07" Text="07"></asp:ListItem>
                            <asp:ListItem Value="08" Text="08"></asp:ListItem>
                            <asp:ListItem Value="09" Text="09"></asp:ListItem>
                            <asp:ListItem Value="10" Text="10"></asp:ListItem>
                            <asp:ListItem Value="11" Text="11"></asp:ListItem>
                            <asp:ListItem Value="12" Text="12"></asp:ListItem>
                            <asp:ListItem Value="13" Text="13"></asp:ListItem>
                            <asp:ListItem Value="14" Text="14"></asp:ListItem>
                            <asp:ListItem Value="15" Text="15"></asp:ListItem>
                            <asp:ListItem Value="16" Text="16"></asp:ListItem>
                            <asp:ListItem Value="17" Text="17"></asp:ListItem>
                            <asp:ListItem Value="18" Text="18"></asp:ListItem>
                            <asp:ListItem Value="19" Text="19"></asp:ListItem>
                            <asp:ListItem Value="20" Text="20"></asp:ListItem>
                            <asp:ListItem Value="21" Text="21"></asp:ListItem>
                            <asp:ListItem Value="22" Text="22"></asp:ListItem>
                            <asp:ListItem Value="23" Text="23"></asp:ListItem>
                            <asp:ListItem Value="24" Text="24"></asp:ListItem>
                            <asp:ListItem Value="25" Text="25"></asp:ListItem>
                            <asp:ListItem Value="26" Text="26"></asp:ListItem>
                            <asp:ListItem Value="27" Text="27"></asp:ListItem>
                            <asp:ListItem Value="28" Text="28"></asp:ListItem>
                            <asp:ListItem Value="29" Text="29"></asp:ListItem>
                            <asp:ListItem Value="30" Text="30"></asp:ListItem>
                            <asp:ListItem Value="31" Text="31"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlEndMonth" runat="server" Width="132px" BackColor="#FFFFC0" AutoPostBack="true">
                            <asp:ListItem Value="January" Text="January"></asp:ListItem>
                            <asp:ListItem Value="February" Text="February"></asp:ListItem>
                            <asp:ListItem Value="March" Text="March"></asp:ListItem>
                            <asp:ListItem Value="April" Text="April"></asp:ListItem>
                            <asp:ListItem Value="May" Text="May"></asp:ListItem>
                            <asp:ListItem Value="June" Text="June"></asp:ListItem>
                            <asp:ListItem Value="July" Text="July"></asp:ListItem>
                            <asp:ListItem Value="August" Text="August"></asp:ListItem>
                            <asp:ListItem Value="September" Text="September"></asp:ListItem>
                            <asp:ListItem Value="October" Text="October"></asp:ListItem>
                            <asp:ListItem Value="November" Text="November"></asp:ListItem>
                            <asp:ListItem Value="December" Text="December"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlEndYear" runat="server" Width="80px" BackColor="#FFFFC0" AutoPostBack="true"></asp:DropDownList>
                    </td>
                                
                    <td>
                        -
                    </td>
                                
                    <td>
                        <asp:DropDownList ID="ddlEndHour" runat="server" Width="50px" BackColor="#FFFFC0" AutoPostBack="true">
                            <asp:ListItem Value="00" Text="00"></asp:ListItem>
                            <asp:ListItem Value="01" Text="01"></asp:ListItem>
                            <asp:ListItem Value="02" Text="02"></asp:ListItem>
                            <asp:ListItem Value="03" Text="03"></asp:ListItem>
                            <asp:ListItem Value="04" Text="04"></asp:ListItem>
                            <asp:ListItem Value="05" Text="05"></asp:ListItem>
                            <asp:ListItem Value="06" Text="06"></asp:ListItem>
                            <asp:ListItem Value="07" Text="07"></asp:ListItem>
                            <asp:ListItem Value="08" Text="08"></asp:ListItem>
                            <asp:ListItem Value="09" Text="09"></asp:ListItem>
                            <asp:ListItem Value="10" Text="10"></asp:ListItem>
                            <asp:ListItem Value="11" Text="11"></asp:ListItem>
                            <asp:ListItem Value="12" Text="12"></asp:ListItem>
                            <asp:ListItem Value="13" Text="13"></asp:ListItem>
                            <asp:ListItem Value="14" Text="14"></asp:ListItem>
                            <asp:ListItem Value="15" Text="15"></asp:ListItem>
                            <asp:ListItem Value="16" Text="16"></asp:ListItem>
                            <asp:ListItem Value="17" Text="17"></asp:ListItem>
                            <asp:ListItem Value="18" Text="18"></asp:ListItem>
                            <asp:ListItem Value="19" Text="19"></asp:ListItem>
                            <asp:ListItem Value="20" Text="20"></asp:ListItem>
                            <asp:ListItem Value="21" Text="21"></asp:ListItem>
                            <asp:ListItem Value="22" Text="22"></asp:ListItem>
                            <asp:ListItem Value="23" Text="23"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td style="width:10px;">
                        :
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlEndMinute" runat="server" Width="50px" BackColor="#FFFFC0" AutoPostBack="true">
                            <asp:ListItem Value="00" Text="00"></asp:ListItem>
                            <asp:ListItem Value="30" Text="30"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td style="width:150px;">
                                    
                    </td>
                </tr> <!-- End Dates -->
                         
                <tr valign="middle">
                    <td >
                        <b>Template</b>
                    </td>
                    <td align="left" colspan="7">
                        <asp:DropDownList id="ddlMeterGrouping" runat="server" Width="350px" BackColor="#FFFFC0" AutoPostBack="true">
                            <asp:ListItem Text="-- none --" Value="0" Selected="True"></asp:ListItem>
                        </asp:DropDownList>
                        <asp:Button ID="btnSelectMeterGrouping" runat="server" Text="select" Enabled="false" Width="50px"/>
                        <asp:Button ID="btnConfigureMeterGrouping" runat="server" Text="create" Width="50px"/>
                    </td>
                    <td>
                        
                    </td>
                </tr> <!-- Meter Templates -->

                <tr>
                    <td style="width:120px;" valign="top">
                            <asp:label ID="lblHalfHourMeterOnly" runat="server" Text="Meters" Width="120px" Font-Bold="true"></asp:label>
                    </td>
                    <td style="width:400px;" valign="middle" colspan="7">
                        <asp:RadioButtonList Width="200px" ID="rblHalfHourMeterOnly" runat="server" RepeatLayout="table" RepeatDirection="Horizontal" AutoPostBack="true">
                            <asp:ListItem Value="0" Text="All Meters" Selected="True"></asp:ListItem>
                            <asp:ListItem Value="1" Text="Half Hour"></asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>

                <tr>
                    <td style="width:120px;" valign="top">
                        <asp:label ID="lblFilterNameAvailable" runat="server" Text="Available Meters" Width="120px" Font-Bold="true"></asp:label>
                    </td>
                    <td style="width:400px;" valign="top" colspan="7">
                        <asp:RadioButtonList Width="450px" ID="rblMeterFilter" runat="server" RepeatLayout="table" RepeatDirection="Horizontal" AutoPostBack="true">
                            <asp:ListItem Value="All" Text="Show All" Selected="True"></asp:ListItem>
                            <asp:ListItem Value="A" Text="Elec"></asp:ListItem>
                            <asp:ListItem Value="B" Text="Gas"></asp:ListItem>
                            <asp:ListItem Value="C" Text="Sub"></asp:ListItem>
                            <asp:ListItem Value="D" Text="Virtual"></asp:ListItem>
                            <asp:ListItem Value="Custom" Text="Custom"></asp:ListItem>
                        </asp:RadioButtonList>
                        <div id="secFilter" runat="server" visible="false">
                            <asp:textbox ID="txtCustomFilter" runat="server" BackColor="#FFFFC0" style="width:400px;"></asp:textbox>
                            <asp:Button ID="btnFilter" runat="server" Text="Go" />
                        </div>
                    </td>
                </tr>
                            
                <tr>
                    <td colspan="8">
                        <asp:ListBox ID="lbAvailableMeters" runat="server" style="width:630px; height:150px;" SelectionMode="Multiple" AutoPostBack="true" BackColor="#FFFFC0" >
                        </asp:ListBox>
                    </td>
                </tr>
         
                <tr>
                    <td style="text-align:center;" colspan="8">
                        <table>
                            <tr>
                                <td>
                                    <asp:Button ID="btnAddtoStatement" runat="server" Text="Add to Statement" Width="150px"/>
                                </td>
                                <td>
                                    <asp:Button ID="btnClearStatement" runat="server" Text="Clear Statement" Width="150px"/>
                                </td>
                                <td>
                                                
                                    <asp:Button ID="btnRunReport" runat="server" Text="Run Report" Width="150px" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr> <!-- Run button -->  
                                               
                <tr>
                    <td colspan="8">
                        <asp:ListBox Visible="false" ID="lbAssignedMeters" runat="server" style="width:630px; height:100px;" SelectionMode="Multiple" AutoPostBack="true" BackColor="#FFFFC0" >
                        </asp:ListBox>     
                        <asp:GridView id="gridview1" runat="server" Width="630px" AutoGenerateDeleteButton="true" AutoGenerateColumns="false" SkinID="Professional">
                            <Columns>
                                <asp:BoundField HeaderText="ID" DataField="intIdentifierPK" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />                                         
                                <asp:BoundField HeaderText="Meter Point" DataField="varMeterName" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" /> 
                                <asp:BoundField HeaderText="Rate" DataField="varRate" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" /> 
                                <asp:BoundField HeaderText="From" DataField="varStartDate" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" /> 
                                <asp:BoundField HeaderText="To" DataField="varEndDate" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" /> 
                            </Columns>
                        </asp:GridView>                      
                    </td>
                </tr>
                <!-- Meter Selection Table -->
                
              
                                  
            </table>
            </asp:Panel>
            <asp:Panel ID="pnlConfigureMeterGrouping" runat="server" Visible="false">
        
            
             <asp:ImageButton ID="ImageButtonReturnToReport"  
                             style="Z-INDEX:1; LEFT: 850px; POSITION: absolute; TOP:100px"
                            height="32px"  
                            width="32px"   
                            runat="server"   
                            ImageAlign="Left"  
                            ImageUrl="~/assets/metering/remove_48x48.png" 
                        />

                <asp:Label ID="lblMeterGroupingNameID" runat="server" Visible="false" ></asp:Label>
                <asp:Label ID="lblConfigMeterGroupingInfo" runat="server"  Visible="false" ></asp:Label>
                <br />
                <br />
                Grouping Name<br />
                <asp:TextBox ID="txtMeterGroupingName" runat="server" Width="450px" BackColor="#FFFFC0"></asp:TextBox>
                <asp:Image ID="imgMeterGroupingName" runat="server" ImageUrl="~/assets/Metering/warning_48.png" Height="18" Width="18" />
                <asp:Button ID="btnMeterGroupingName" runat="server" Width="100px" Text="save" />
                <asp:Button ID="btnMeterGroupingNameDelete" runat="server" Width="80px" Text="delete" />

                <asp:Panel ID="pnlAssignMeterGrouping" runat="server" Visible="false">
                    <table>
                        <tr>
                            <td style="width:550px;">
                                Available<br />
                                <asp:ListBox ID="lbMeterGroupingAvailable" runat="server" Width="450px" Height="200px" SelectionMode="Single" BackColor="#FFFFC0"></asp:ListBox>
                                <asp:Button ID="btnMeterGroupingAvailable" runat="server" Text="add" Width="450px" />
                            </td>
                        </tr>

                        <tr>
                            <td >
                                <asp:Button ID="btnMeterGroupingAssigned" runat="server" Text="remove" Width="450px" />
                                <asp:ListBox ID="lbMeterGroupingAssigned" runat="server" Width="450px" Height="200px" SelectionMode="Single" BackColor="#FFFFC0"></asp:ListBox>
                                <br />Assigned
                            </td>
                        </tr>
                    </table>
                </asp:Panel>

            </asp:Panel>
        </div>
    </form>
</asp:Content>

