Imports System.IO
Imports System.Diagnostics

Partial Class reports_Reports_DataServices_Default
    Inherits System.Web.UI.Page


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        Try
            If Not IIf(Core.data_select_value("select bitAllowMyReports from tblUsers where intUserPK = " & MySession.UserID) = "True", True, False) Then
                Response.Redirect(Core.data_select_value("select RawHomeURL from vwUsers where ID = " & MySession.UserID), True)
            End If

            If Not Page.IsPostBack = True Then
                Dim description As HtmlMeta = New HtmlMeta()
                description.Name = "description"
                description.Content = "Utility Masters customer extranet - Reports"
                Dim keywords As HtmlMeta = New HtmlMeta()
                keywords.Name = "keywords"
                keywords.Content = "Reports"
                Page.Header.Controls.Add(description)
                Page.Header.Controls.Add(keywords)


            End If

        Catch ex As Exception
            '
        End Try

        ' Retrieve the querystring values
        Using q As New QueryString()
            If (q("url") IsNot Nothing) Then
                DownloadDocument(q("url"))
            End If

        End Using

        If Request.QueryString("custid") Is Nothing Then
            Dim strSQL As String = "EXEC spPortal_TREEVIEW_TopLevel " & MySession.UserID
            Dim myConnection As New OleDb.OleDbConnection(Core.CurrentConnectionString)

            Dim myCommand As New OleDb.OleDbCommand(strSQL, myConnection)
            myCommand.Connection.Open()
            myCommand.CommandTimeout = 180
            Dim Reader As OleDb.OleDbDataReader = myCommand.ExecuteReader(CommandBehavior.CloseConnection)

            If Reader.HasRows Then
                Reader.Read()
                If Reader("Type") = "Company" Then
                    Response.Redirect("default.aspx?custid=" & Reader("id") & "&method=Customer", True)
                Else
                    Response.Redirect("default.aspx?custid=" & Reader("id") & "&method=" & Reader("Type"), True)
                End If
                Reader.Close()
            End If
        End If



        If Not Page.IsPostBack Then

            Dim strSQL2 As String = "EXEC spReports_DataServices_DateList "
            strSQL2 = strSQL2 + CStr(MySession.UserID) + ", "

            If Not Request.QueryString("method") Is Nothing Then
                strSQL2 = strSQL2 + "'" + CStr(Request.QueryString("method")) + "', "
            End If

            If Not Request.QueryString("custid") Is Nothing Then
                strSQL2 = strSQL2 + CStr(Request.QueryString("custid")) + ""
            End If

            With Me.ddlDate
                .Items.Clear()
                .DataSource = Core.data_select(strSQL2)
                .DataValueField = "pk_date"
                .DataTextField = "month_name"
                .DataBind()
                .SelectedIndex = 0
            End With

            If Me.ddlDate.Items.Count > 0 Then

                LoadReports(Me.ddlDate.SelectedItem.Value)

            End If
        End If
        Me.lblDescription.Text = Request.QueryString("method").ToString
        Me.lblName.Text = Core.data_select_value("EXEC spSites_MAP_CurentSelection " & MySession.UserID & ", '" & Request.QueryString("method") & "', " & Request.QueryString("custid"))

    End Sub

    Private Sub LoadReports(ByVal reportdate As String)

        Dim objConnect As New SqlConnection(Core.ConnectionString())
        Dim objReader As SqlDataReader

        Dim strSQL As String = "EXEC spReports_DataServices_Date "

        strSQL = strSQL + CStr(MySession.UserID) + ", "

        If Not Request.QueryString("method") Is Nothing Then
            strSQL = strSQL + "'" + CStr(Request.QueryString("method")) + "', "
        End If

        If Not Request.QueryString("custid") Is Nothing Then
            strSQL = strSQL + CStr(Request.QueryString("custid")) + ", "
        End If

        strSQL = strSQL + "'" + reportdate + "'"

        Me.lstReportList.InnerHtml = "<li>"
        Me.lstReportList.InnerHtml &= "<table style='width:600px;'>"

        Me.lstReportList.InnerHtml &= "<tr style='background-color:#009530; color:white;'>"
        'Me.lstReportList.InnerHtml &= "<td></td>"
        Me.lstReportList.InnerHtml &= "<td>Report Name</td>"
        Me.lstReportList.InnerHtml &= "<td></td>" ' style='background-color:white; color:black;'
        Me.lstReportList.InnerHtml &= "</tr>"

        Try
            Dim objCommand As New SqlCommand(strSQL, objConnect)
            objCommand.CommandType = CommandType.Text
            objConnect.Open()
            objReader = objCommand.ExecuteReader(CommandBehavior.CloseConnection)
            While objReader.Read()


                Me.lstReportList.InnerHtml &= "<tr>"

                Dim intSiteFK As Integer = objReader("siteid")
                Dim intCustomerFK As Integer = Core.data_select_value("SELECT custid FROM UML_CMS.dbo.tblSites WHERE siteid = " & intSiteFK)

                Dim strCustomerName As String = Core.data_select_value("SELECT CustomerName FROM UML_CMS.dbo.tblCustomer WHERE custid = " & intCustomerFK)


                Dim strFile As String = Core.CustomerDirectory & strCustomerName & "\Utility Management\Data Services\" & objReader("varReportURL")

                'Me.lstReportList.InnerHtml &= "<td>" & objReader("intReportLogPK") & "</td>"
                Me.lstReportList.InnerHtml &= "<td>" & objReader("varReportURL") & "</td>"

                Dim pairs As String = [String].Format("url={0}", strFile)
                Dim encoded As String

                ' Set the querystring values
                Using q As New QueryString()
                    encoded = q.EncodePairs(pairs)
                End Using
                Dim strParams As String = ""
                Dim intParams As Integer
                intParams = Request.QueryString.Count
                If intParams > 0 Then
                    Dim intCurrentParam As Integer = 0
                    While intCurrentParam < intParams


                        If intCurrentParam = 0 Then
                            strParams &= Request.QueryString.Keys(intCurrentParam) & "=" & Request.QueryString.Item(intCurrentParam)
                        Else
                            strParams &= "&" & Request.QueryString.Keys(intCurrentParam) & "=" & Request.QueryString.Item(intCurrentParam)
                        End If

                        intCurrentParam += 1
                    End While


                End If


                Me.lstReportList.InnerHtml &= "<td><a href='default.aspx?" & strParams & "&q=" & encoded & "'>view</a></td>" '  style='background-color:white; color:black;'

                Me.lstReportList.InnerHtml &= "</tr>"
            End While
            objReader.Close()
        Catch ex As Exception
            Throw
        Finally
            objConnect.Close()
        End Try

        Me.lstReportList.InnerHtml &= "</table>"
        Me.lstReportList.InnerHtml &= " "
        Me.lstReportList.InnerHtml &= "</li>"

    End Sub

    Public Sub AlertMessage(ByVal strMessage As String)

        'finishes server processing, returns to client.
        Dim strScript As String = "<script language=JavaScript>"
        strScript += "alert(""" & strMessage & """);"
        strScript += "</script>"

        Page.ClientScript.RegisterStartupScript(Me.GetType, "clientScript", strScript)

    End Sub


    Public Sub DownloadDocument(ByVal strFilePath As String)

        Dim MyFileStream As New FileStream(strFilePath, FileMode.Open)

        Dim FileSize As Integer = MyFileStream.Length
        Dim Buffer(CType(FileSize, Integer)) As Byte
        MyFileStream.Read(Buffer, 0, CType(MyFileStream.Length, Integer))
        MyFileStream.Close()
        Response.ContentType = "application/pdf"
        Response.AddHeader("content-disposition", "attachment; filename=MyReport.PDF")
        Response.BinaryWrite(Buffer)

    End Sub

    Protected Sub ddlDate_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlDate.SelectedIndexChanged
        LoadReports(Me.ddlDate.SelectedItem.Value)
    End Sub
End Class
