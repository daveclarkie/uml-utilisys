Imports System.IO
Imports System.Diagnostics

Partial Class ElecBillValidation_Default
    Inherits System.Web.UI.Page


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        Try
            If Not IIf(Core.data_select_value("select bitAllowMyReports from tblUsers where intUserPK = " & MySession.UserID) = "True", True, False) Then
                Response.Redirect(Core.data_select_value("select RawHomeURL from vwUsers where ID = " & MySession.UserID), True)
            End If

            If Request.QueryString("custid") Is Nothing Then
                Response.Redirect(Core.TreeviewRedirectURL(), True)
            End If

            Me.lblDescription.Text = Request.QueryString("method").ToString
            Me.lblName.Text = Core.data_select_value("EXEC spSites_MAP_CurentSelection " & MySession.UserID & ", '" & Request.QueryString("method") & "', " & Request.QueryString("custid"))


            If Not Page.IsPostBack = True Then

                Dim description As HtmlMeta = New HtmlMeta()
                description.Name = "description"
                description.Content = "Elec Bill Validation"
                Dim keywords As HtmlMeta = New HtmlMeta()
                keywords.Name = "keywords"
                keywords.Content = "Elec Bill Validation"
                Page.Header.Controls.Add(description)
                Page.Header.Controls.Add(keywords)

                Dim strSQL2 As String = "EXEC spReports_ScheduledReport_SiteList " + CStr(MySession.UserID) + ", '" + CStr(Request.QueryString("method")) + "', " + CStr(Request.QueryString("custid")) + ", 38"
                Core.data_FillList(ddlFilter, Core.data_select(strSQL2), "sitename", "siteid")
                If Me.ddlFilter.Items.Count > 0 Then
                    LoadReports(Me.ddlFilter.SelectedItem.Value)
                End If

            End If

            ' Retrieve the querystring values
            Using q As New QueryString()
                If (q("url") IsNot Nothing) Then
                    DownloadDocument(q("url"))
                End If
            End Using


        Catch ex As Exception
            '
        End Try

    End Sub

    Private Sub LoadReports(ByVal intSiteFK As Integer)

        Dim objConnect As New SqlConnection(Core.ConnectionString())
        Dim objReader As SqlDataReader

        Dim strSQL As String = "EXEC spReports_ScheduledReport_BillValidationElec_SiteFK " + CStr(MySession.UserID) + ", '" + CStr(Request.QueryString("method")) + "', " + CStr(Request.QueryString("custid")) + ", " + intSiteFK.ToString

        Me.lstReportList.InnerHtml = "<li>"
        Me.lstReportList.InnerHtml &= "<table style='width:600px;'><tr style='background-color:#009530; color:white;'><td>Report Name</td><td></td></tr>"

        Try
            Dim objCommand As New SqlCommand(strSQL, objConnect)
            objCommand.CommandType = CommandType.Text
            objConnect.Open()
            objReader = objCommand.ExecuteReader(CommandBehavior.CloseConnection)
            While objReader.Read()


                Me.lstReportList.InnerHtml &= "<tr>"

                Dim intCustomerFK As Integer = Core.data_select_value("SELECT custid FROM UML_CMS.dbo.tblSites WHERE siteid = " & intSiteFK)
                Dim strCustomerName As String = Core.data_select_value("SELECT CustomerName FROM UML_CMS.dbo.tblCustomer WHERE custid = " & intCustomerFK)

                Dim strFile As String = Core.CustomerDirectory & strCustomerName & "\Utility Management\Bill Validation - Electricity\" & objReader("varReportURL")

                Me.lstReportList.InnerHtml &= "<td>" & objReader("varReportURL") & "</td>"

                Dim pairs As String = [String].Format("url={0}", strFile)
                Dim encoded As String

                ' Set the querystring values
                Using q As New QueryString()
                    encoded = q.EncodePairs(pairs)
                End Using

                Dim strParams As String = ""
                Dim intParams As Integer
                intParams = Request.QueryString.Count
                If intParams > 0 Then
                    Dim intCurrentParam As Integer = 0
                    While intCurrentParam < intParams


                        If intCurrentParam = 0 Then
                            strParams &= Request.QueryString.Keys(intCurrentParam) & "=" & Request.QueryString.Item(intCurrentParam)
                        Else
                            strParams &= "&" & Request.QueryString.Keys(intCurrentParam) & "=" & Request.QueryString.Item(intCurrentParam)
                        End If

                        intCurrentParam += 1
                    End While


                End If

                If objReader("intStatus") = 2 Then
                    Me.lstReportList.InnerHtml &= "<td><a href='default.aspx?" & strParams & "&q=" & encoded & "'>view</a></td>" '  style='background-color:white; color:black;'
                ElseIf objReader("intStatus") = 1 Then
                    Me.lstReportList.InnerHtml &= "<td>checked and ready</td>" '  style='background-color:white; color:black;'
                ElseIf objReader("intStatus") = 0 Then
                    Me.lstReportList.InnerHtml &= "<td>waiting for data</td>" '  style='background-color:white; color:black;'
                End If

                Me.lstReportList.InnerHtml &= "</tr>"
            End While
            objReader.Close()
        Catch ex As Exception
            Throw
        Finally
            objConnect.Close()
        End Try

        Me.lstReportList.InnerHtml &= "</table>"
        Me.lstReportList.InnerHtml &= " "
        Me.lstReportList.InnerHtml &= "</li>"

    End Sub

    Public Sub AlertMessage(ByVal strMessage As String)

        'finishes server processing, returns to client.
        Dim strScript As String = "<script language=JavaScript>"
        strScript += "alert(""" & strMessage & """);"
        strScript += "</script>"

        Page.ClientScript.RegisterStartupScript(Me.GetType, "clientScript", strScript)

    End Sub

    Public Sub DownloadDocument(ByVal strFilePath As String)

        Dim MyFileStream As New FileStream(strFilePath, FileMode.Open)

        Dim FileSize As Integer = MyFileStream.Length
        Dim Buffer(CType(FileSize, Integer)) As Byte
        MyFileStream.Read(Buffer, 0, CType(MyFileStream.Length, Integer))
        MyFileStream.Close()
        Response.ContentType = "application/pdf"
        Response.AddHeader("content-disposition", "attachment; filename=Bill Validation - Electricity.pdf")
        Response.BinaryWrite(Buffer)

    End Sub

    Protected Sub ddlFilter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlFilter.SelectedIndexChanged
        LoadReports(Me.ddlFilter.SelectedItem.Value)
    End Sub
End Class
