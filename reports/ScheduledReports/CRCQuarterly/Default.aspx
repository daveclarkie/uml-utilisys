<%@ Page Language="VB" MasterPageFile="~/landing.master" AutoEventWireup="false" CodeFile="Default.aspx.vb" Inherits="reports_Reports_DataServices_Default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="content_right" Runat="Server">

<div class="content_back general">
<form runat="server" id="Form1">
    <table>
    
        <tr valign="middle">
            <td width="150px" >
                Report
            </td>
            <td width="400px" align="left">
                CRC Quarterly
            </td>
            <td width="50px">
                
            </td>
        </tr>
        <tr valign="middle">
            <td width="90px" >
                <asp:Label ID="lblDescription" Text="Description" runat="server"></asp:Label>
            </td>
            <td width="400px" align="left">
                <asp:Label ID="lblName" Text="Name" runat="server"></asp:Label>
            </td>
            <td width="50px">
            </td>
        </tr>
    
        <tr valign="middle">
            <td width="150px" >
                Filter
            </td>
            <td width="400px" align="left">
                <asp:DropDownList ID="ddlDate" runat="server" AutoPostBack="true">
                </asp:DropDownList>
            </td>
            <td width="50px">
                
            </td>
        </tr>
    </table>
    <asp:Table ID="tblSummary" runat="server" Width="650px">
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                <ul id="lstReportList" runat="server"></ul>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                Having trouble viewing these reports? click here   <a href="http://get.adobe.com/uk/reader/" target="_blank"><img alt="Adbobe Reader" src="../../../assets/get_adobe_reader.gif" /></a>
            </asp:TableCell>
        </asp:TableRow>
        
    </asp:Table> 
   
    
    </form>
    </div>

</asp:Content>

