
Partial Class reports_OnlineUtilisys_Default
    Inherits System.Web.UI.Page


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        LoadReports()
    End Sub

    Private Sub LoadReports()

        Me.ulBox.InnerHtml = ""

        Dim strSQL As String = "EXEC spReports_ListReports " & MySession.UserID & ", 12"

        Using connection As New SqlConnection(Core.ConnectionString)
            connection.Open()
            Dim command As New SqlCommand(strSQL, connection)
            command.CommandType = CommandType.Text
            Dim reader As SqlDataReader = command.ExecuteReader()

            While reader.Read

                Dim strUrl As String
                If reader("intReportDisabled") = 0 Then
                    strUrl = reader("varOnlineURL").ToString
                Else
                    strUrl = ""
                End If
                Me.ulBox.InnerHtml &= "<li><a id='lnkReports" & reader("intReportPK") & "' class='" & reader("varCSS") & "' runat='server' title='" & reader("varReportName") & "' href='" & strUrl & "' ></a></li>"



            End While

        End Using

        'core.data_Reader(strSQL)

    End Sub


End Class
