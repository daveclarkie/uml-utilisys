﻿Imports clsReportServerCredentials


Partial Class reports_DataAnalysis_GR206_Report
    Inherits System.Web.UI.Page


    Public Function fnNoSpecialCharacters(ByVal strTest As String) As String

        Dim sb As New System.Text.StringBuilder

        For Each ch As Char In strTest
            If Char.IsLetterOrDigit(ch) OrElse ch = " "c Then
                sb.Append(ch)
            End If
        Next

        strTest = sb.ToString

        Return strTest
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim QSvarMeterList As String = Request.QueryString("varMeterList")
        Dim QSintProductFK As Integer = Request.QueryString("intProductFK")
        Dim QSDateType As String = Request.QueryString("varDateType")
        Dim QSdtmDate As String = Request.QueryString("dtmDate")



        Me.frmSelection.Visible = False
        Me.frmReport.Visible = True

        'Set the processing mode for the ReportViewer to Remote
        myReportViewer.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Remote

        Dim serverReport As Microsoft.Reporting.WebForms.ServerReport
        serverReport = myReportViewer.ServerReport

        serverReport.ReportServerCredentials = New clsReportServerCredentials("administrator", "chicago", "uml")

        'Set the report server URL and report path
        serverReport.ReportServerUrl = _
            New Uri("http://10.44.5.205/reportserver")
        serverReport.ReportPath = "/Portal Reporting/SEC Report Online"

        Dim varMeterList As New Microsoft.Reporting.WebForms.ReportParameter()
        varMeterList.Name = "varMeterList"
        varMeterList.Values.Add(QSvarMeterList)


        Dim intProductFK As New Microsoft.Reporting.WebForms.ReportParameter()
        intProductFK.Name = "intProductFK"
        intProductFK.Values.Add(QSintProductFK)


        Dim DateType As New Microsoft.Reporting.WebForms.ReportParameter()
        DateType.Name = "DateType"
        DateType.Values.Add(QSDateType)


        Dim dtmDate As New Microsoft.Reporting.WebForms.ReportParameter()
        dtmDate.Name = "dtmDate"
        dtmDate.Values.Add(QSdtmDate)



        'Set the report parameters for the report
        Dim parameters() As Microsoft.Reporting.WebForms.ReportParameter = {varMeterList, intProductFK, DateType, dtmDate}
        serverReport.SetParameters(parameters)

    End Sub
End Class
