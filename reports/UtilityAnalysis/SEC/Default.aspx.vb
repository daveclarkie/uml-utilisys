Imports Dundas.Charting.WebControl
Imports System.Drawing

Partial Class reports_GR206_Default
    Inherits System.Web.UI.Page


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            If Not IIf(Core.data_select_value("select bitAllowMyReports from tblUsers where intUserPK = " & MySession.UserID) = "True", True, False) Then
                Response.Redirect(Core.data_select_value("select RawHomeURL from vwUsers where ID = " & MySession.UserID), True)
            End If

            If Request.QueryString("custid") Is Nothing Then
                Dim strSQL As String = "EXEC spPortal_TREEVIEW_TopLevel " & MySession.UserID
                Dim myConnection As New OleDb.OleDbConnection(Core.CurrentConnectionString)

                Dim myCommand As New OleDb.OleDbCommand(strSQL, myConnection)
                myCommand.Connection.Open()
                myCommand.CommandTimeout = 180
                Dim Reader As OleDb.OleDbDataReader = myCommand.ExecuteReader(CommandBehavior.CloseConnection)

                If Reader.HasRows Then
                    Reader.Read()
                    If Reader("Type") = "Company" Then
                        Response.Redirect("default.aspx?custid=" & Reader("id") & "&method=Customer", True)
                    Else
                        Response.Redirect("default.aspx?custid=" & Reader("id") & "&method=" & Reader("Type"), True)
                    End If
                    Reader.Close()
                End If
            End If

            If Not Page.IsPostBack = True Then
                Dim description As HtmlMeta = New HtmlMeta()
                description.Name = "description"
                description.Content = "UTILISYS - SEC"
                Dim keywords As HtmlMeta = New HtmlMeta()
                keywords.Name = "keywords"
                keywords.Content = "UTILISYS - SEC"
                Page.Header.Controls.Add(description)
                Page.Header.Controls.Add(keywords)
                PopulateDates()
                MeterPoints_Show("All")
                DriverPoints_Show()
            End If


            Me.lblDescription.Text = Request.QueryString("method").ToString
            Me.lblName.Text = Core.data_select_value("EXEC spSites_MAP_CurentSelection " & MySession.UserID & ", '" & Request.QueryString("method") & "', " & Request.QueryString("custid"))


            ThickBox()

        Catch ex As Exception
            '
        End Try
    End Sub

    Public Sub ThickBox()


        Dim intError As Integer = 0

        If intError = 0 Then

            If Me.lbAvailableMeters.SelectedIndex = -1 Then intError += 1
            If Me.lbAvailableDrivers.SelectedIndex = -1 Then intError += 1

        End If

        If intError = 0 Then 'ok

            'Me.TextBox1.Text = "varMeterList = " + CStr(Me.lbAvailableMeters.SelectedItem.Value) + " & intProductFK = " + CStr(Me.lbAvailableDrivers.SelectedItem.Value) + " & varDateType = " + CStr(Me.rblGranularity.SelectedItem.Value) + " & dtmDate = " + CStr(Me.ddldtmDate.SelectedItem.Value.ToString) + ""

            Me.lnkNewLink.NavigateUrl = "report.aspx?varMeterList=" + CStr(Me.lbAvailableMeters.SelectedItem.Value) + "&intProductFK=" + CStr(Me.lbAvailableDrivers.SelectedItem.Value) + "&varDateType=" + CStr(Me.rblGranularity.SelectedItem.Value) + "&dtmDate=" + CStr(Me.ddldtmDate.SelectedItem.Value.ToString) + "&?keepThis=true&TB_iframe=true&height=570&width=900"
            Me.lnkNewLink.Text = "View Report"
            Me.lnkNewLink.ImageUrl = "../../../assets/Schneider/buttons-myreports-general-generatereport.png"
            Me.lnkNewLink.CssClass = "thickbox"
            Me.lnkNewLink.ToolTip = "Comparison Report"
        Else
            Me.lnkNewLink.NavigateUrl = ""
            Me.lnkNewLink.Text = "* please select a meter"
            Me.lnkNewLink.ImageUrl = "../../../assets/Schneider/buttons-myreports-general-generatereport-disabled.png"
            Me.lnkNewLink.CssClass = ""
            'Me.lnkbtnDownload.Visible = False
        End If


    End Sub

    Public Overloads Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)

        ' Confirms that an HtmlForm control is rendered for the specified ASP.NET server control at run time.

    End Sub

    Private Sub MeterPoints_Show(ByVal Type As String)

        With Me.lbAvailableMeters
            .DataSource = Core.data_select(" EXEC [portal.utilitymasters.co.uk].dbo.usp_MeterPoints_Display " & MySession.UserID & ", '" & Request.QueryString("method") & "', " & Request.QueryString("custid") & ",'" & Type & "', ''")
            .DataValueField = "ID"
            .DataTextField = "varNiceName"
            .DataBind()
        End With

    End Sub

    Private Sub DriverPoints_Show()

        With Me.lbAvailableDrivers
            .DataSource = Core.data_select(" EXEC   [portal.utilitymasters.co.uk].dbo.usp_DriverPoints_Display " & MySession.UserID & ", '" & Request.QueryString("method") & "', " & Request.QueryString("custid"))
            .DataValueField = "ID"
            .DataTextField = "Name"
            .DataBind()
        End With

    End Sub

    Protected Sub rblMeterFilter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rblMeterFilter.SelectedIndexChanged
        MeterPoints_Show(Me.rblMeterFilter.SelectedItem.Value)
    End Sub

    Protected Sub lbAvailableMeters_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbAvailableMeters.SelectedIndexChanged

        ThickBox()

    End Sub
    Protected Sub lbAvailableDrivers_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbAvailableDrivers.SelectedIndexChanged

        ThickBox()

    End Sub

    
    Private Sub PopulateDates()

        With Me.ddldtmDate
            .DataSource = Core.data_select("SELECT DISTINCT year, 'Calendar Year ' + CAST(DATEPART(YEAR, Year) AS  VARCHAR) AS NiceName FROM uml_cms.dbo.DIM_dates WHERE pk_date < GETDATE() ORDER BY year desc")
            .DataValueField = "year"
            .DataTextField = "NiceName"
            .DataBind()
        End With
        

    End Sub

    Protected Sub rblGranularity_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rblGranularity.SelectedIndexChanged

      

        ThickBox()

    End Sub

End Class
