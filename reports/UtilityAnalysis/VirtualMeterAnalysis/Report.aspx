﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Report.aspx.vb" Inherits="VirtualMeterAnalysis_Report" %>

<%@ Register Assembly="DundasWebChart" Namespace="Dundas.Charting.WebControl" TagPrefix="DCWC" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Compare Meter Points</title>
    <link rel="stylesheet" href="../../../modalbox/modalreport.css" type="text/css" media="screen" />   

</head>

<body>

<form id="form1" runat="server">
    <asp:Label ID="lblInformation" runat="server"></asp:Label>

    <asp:Panel ID="pnlReport" width="870px" runat="server">

        
        <table border="0" cellpadding="0" cellspacing="0" >
            <tr>
                <td style="width:150px;">
                
                </td>
                <td>
                    <DCWC:Chart ID="Chart1" runat="server" >
                        <Legends>
                            <DCWC:Legend Name="Default">
                            </DCWC:Legend>
                        </Legends>
                    </DCWC:Chart>
                </td>
            </tr>
        </table>
        <%--Charts--%>
        <table width="865px" style="background-color:#B7E566; border:1px solid black; text-align:justify;">
            <tr>
                <td style="font-weight:bold; width:130px;">
                    Granularity
                </td>
                <td style="font-weight:bold; width:100px;">
                    Type
                </td>
                <td style="font-weight:bold; width:150px;">
                    Element
                </td>
                <td style="font-weight:bold; width:130px;">
                    Timescale
                </td>
                <td style="width:380px;">
                    
                </td>
            </tr>
            <tr>
                <td style="text-align:left; vertical-align:top;">
                    
                    <asp:RadioButtonList ID="rblFrequency" runat="server" AutoPostBack="true" RepeatDirection="Vertical" RepeatColumns="1" RepeatLayout="Table" TextAlign="Right"></asp:RadioButtonList>
                </td>
                <td style="text-align:left; vertical-align:top;">
                    
                    <asp:RadioButtonList ID="rblChartType" runat="server" AutoPostBack="true" RepeatDirection="Vertical" RepeatColumns="1" RepeatLayout="Table" TextAlign="Right"></asp:RadioButtonList>
                </td>
                <td style="text-align:left; vertical-align:top;">
                    <asp:CheckBox ID="chkEvents" runat="server" AutoPostBack="true" Visible="false" />
                    <asp:RadioButtonList ID="rblReadingType" runat="server" AutoPostBack="true" RepeatDirection="Vertical" RepeatColumns="1" RepeatLayout="Table" TextAlign="Right"></asp:RadioButtonList>
                </td>
                <td style="text-align:left; vertical-align:top;">
                    
                    <asp:RadioButtonList ID="rblTimescales" runat="server" AutoPostBack="true" RepeatDirection="Vertical" RepeatColumns="1" RepeatLayout="Table" TextAlign="Right"></asp:RadioButtonList>
                </td>
                <td>
                    <asp:Table ID="tblOtherDates" runat="server" BorderColor="#082B61" BorderStyle="Ridge" BorderWidth="1px" Visible="false" Width="95%" Height="100%">
                    
                        <asp:TableRow>
                            <asp:TableCell Width="150px">
                                Date Type
                            </asp:TableCell>
                            <asp:TableCell Width="230px">
                                <asp:DropDownList ID="ddlDateType" runat="server" AutoPostBack="true" Width="100%"></asp:DropDownList>
                            </asp:TableCell>
                        </asp:TableRow>

                        <asp:TableRow>
                            <asp:TableCell Width="150px">
                                Date Select
                            </asp:TableCell>
                            <asp:TableCell Width="230px">
                                <asp:DropDownList ID="ddlDateSelect" runat="server" AutoPostBack="true" Width="100%"></asp:DropDownList>
                            </asp:TableCell>
                        </asp:TableRow>

                        <asp:TableRow>
                            <asp:TableCell Width="150px">
                                From
                            </asp:TableCell>
                            <asp:TableCell Width="230px">
                                <asp:TextBox ID="txtStartDate" runat="server" Visible="false"></asp:TextBox>
                                <asp:Label ID="lblStartDate" runat="server" ></asp:Label>
                            </asp:TableCell>
                        </asp:TableRow>

                        <asp:TableRow>
                            <asp:TableCell Width="150px">
                                To
                            </asp:TableCell>
                            <asp:TableCell Width="230px">
                                <asp:TextBox ID="txtEndDate" runat="server" Visible="false"></asp:TextBox>
                                <asp:Label ID="lblEndDate" runat="server" ></asp:Label>
                            </asp:TableCell>
                        </asp:TableRow>
                    
                        <asp:TableRow>
                            <asp:TableCell Width="150px">
                            </asp:TableCell>
                        </asp:TableRow>
                    
                    </asp:Table>
                </td>
            </tr>
            
            <tr>
                <td colspan="4">
                    <asp:TextBox ID="txtMeterID" runat="server" Text="" Visible="false" Width="10"></asp:TextBox>
                    <asp:TextBox ID="txtFrequency" runat="server" Text="" Visible="false" Width="10"></asp:TextBox>
                    <asp:TextBox ID="txtStart" runat="server" Text="" Visible="false" Width="100"></asp:TextBox> 
                    <asp:TextBox ID="txtEnd" runat="server" Text="" Visible="false" Width="100"></asp:TextBox>
                    <asp:TextBox ID="txtGraphType" runat="server" Text="" Visible="false" Width="10"></asp:TextBox>
                    <asp:TextBox ID="txtReadingType" runat="server" Text="Cons - unit" Visible="false" Width="100"></asp:TextBox>
                </td>
            </tr>
            
        </table>

    </asp:Panel>

</form>

</body>
</html>
