﻿Imports Dundas.Charting.WebControl
Imports Dundas.Charting.WebControl.Utilities

Imports Dundas.Charting.WebControl.ChartTypes

Imports System.Data.SqlClient
Imports System.Data
Imports System.Drawing
Imports System.Windows

Partial Class VirtualMeterAnalysis_Report

    Inherits System.Web.UI.Page

    Public conn As String = Core.ConnectionString()

    Shared cDefault As Color = System.Drawing.ColorTranslator.FromHtml("#009530")

    Shared cTwo As Color = System.Drawing.ColorTranslator.FromHtml("#f18308") ' Blue / Purple
    Shared cThree As Color = System.Drawing.ColorTranslator.FromHtml("#b7d110") ' Red
    Shared cFour As Color = System.Drawing.ColorTranslator.FromHtml("#26a7e7") ' Light Orange
    Shared cFive As Color = System.Drawing.ColorTranslator.FromHtml("#009530") ' Green
    Shared cSix As Color = System.Drawing.ColorTranslator.FromHtml("#b3f0ff") ' Light Blue
    Shared cSeven As Color = System.Drawing.ColorTranslator.FromHtml("#cf36ff") ' Lilac
    Shared cEight As Color = System.Drawing.ColorTranslator.FromHtml("#5496ff") ' Brown
    Shared cNine As Color = System.Drawing.ColorTranslator.FromHtml("#5a9e68") ' Light Orange
    Shared cTen As Color = System.Drawing.ColorTranslator.FromHtml("#005710") ' Green
    Shared cEleven As Color = System.Drawing.ColorTranslator.FromHtml("#e6ff42") ' Light Blue
    Shared cTwelve As Color = System.Drawing.ColorTranslator.FromHtml("#0fff00") ' Lilac
    Shared cThirteen As Color = System.Drawing.ColorTranslator.FromHtml("#ff5200") ' Brown

    Shared colorSet() As Color = {cDefault, cTwo, cThree, cFour, cFive, cSix, cSeven, cEight, cNine, cTen, cEleven, cTwelve, cThirteen}

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        Session.LCID = 2057

        If Not Page.IsPostBack = True Then


            Me.txtFrequency.Text = "Half Hour"
            Me.txtMeterID.Text = Request.QueryString("meterid")
            Me.txtGraphType.Text = "3"


            SetupTimescales()
            SetupDateType()
            SetupFrequency()
            SetupChartType()
            SetupReadingType()


            GenerateGraphs()


        End If

        'SetupButtons()

    End Sub

    Protected Overloads Sub MakeGroupLabels(ByVal chart As Chart, ByVal series As Series, ByVal ParamArray fields() As String)
        ' This is how many rows of labels we need to make.
        Dim numLabels As Integer = fields.Length

        Dim chartArea As ChartArea = chart.ChartAreas(series.ChartArea)
        Dim group As String() = New String(numLabels - 1) {}
        Dim labels As CustomLabel() = New CustomLabel(numLabels - 1) {}

        ' Initialize each row's current label.
        For i As Integer = 0 To numLabels - 1
            group(i) = ""
        Next

        ' For x-value indexed series, count from 1.
        Dim xValue As Integer = 1

        ' Set the PointWidth default value if it's null
        If series("PointWidth") Is Nothing Then
            series("PointWidth") = "0.8"
        End If

        ' Iterate over each point in the series.  Check the group fields and 
        ' update the ending value for each label or start a new label.
        For Each dataPoint As DataPoint In series.Points
            For i As Integer = 0 To numLabels - 1
                If group(i) = dataPoint(fields(i)) Then
                    ' Still in the same group.  Update the custom label's end position.
                    labels(i).[To] = xValue + Double.Parse(series("PointWidth")) / 2
                Else
                    ' Create a new label here with the group text.
                    group(i) = dataPoint(fields(i))
                    labels(i) = New CustomLabel()
                    labels(i).From = xValue - Double.Parse(series("PointWidth")) / 2
                    labels(i).[To] = xValue + Double.Parse(series("PointWidth")) / 2
                    labels(i).RowIndex = (i + 1)
                    labels(i).Text = group(i)
                    labels(i).LabelMark = LabelMark.LineSideMark
                    chartArea.AxisX.CustomLabels.Add(labels(i))
                End If
            Next
            xValue += 1
        Next
    End Sub

    Private Sub CleanChart()

        Chart1.Serializer.Reset()

        With Me.Chart1

            Dim series1 As Series
            For Each series1 In .Series
                .Annotations.Clear()
            Next

            .Visible = True
            .Series.Clear()
            .Titles.Clear()
            .Legends.Clear()
            .Legends.Add("Default")

            ' legend stuff
            .Legends(0).Docking = LegendDocking.Bottom
            .Legends(0).Alignment = StringAlignment.Far
            .Legends(0).Reversed = AutoBool.False
            .Legends(0).Position.Auto = True
            .Legends(0).CellColumns.Add(New LegendCellColumn("", LegendCellColumnType.SeriesSymbol, ""))
            .Legends(0).CellColumns.Add(New LegendCellColumn("", LegendCellColumnType.Text, "#LEGENDTEXT", Drawing.ContentAlignment.MiddleLeft))

            ' Zooming
            Dim chartarea1 As ChartArea
            For Each chartarea1 In .ChartAreas
                chartarea1.CursorX.UserEnabled = True
            Next

            ' Setup Frame
            .BorderColor = Color.Black
            .BorderStyle = ChartDashStyle.Solid
            .BorderWidth = 2
            .BorderSkin.SkinStyle = BorderSkinStyle.Emboss
            'Setup Toolbar
            .UI.Toolbar.Enabled = True
            .UI.Toolbar.Docking = ToolbarDocking.Top
            .UI.Toolbar.Placement = ToolbarPlacement.InsideChart
            .UI.Toolbar.BorderSkin.SkinStyle = BorderSkinStyle.Emboss
            .UI.Toolbar.BorderSkin.FrameBackColor = Color.Black

            Dim commands As CommandCollection = Chart1.UI.Commands

            Dim cmdProperties As Command = commands.FindCommand(ChartCommandType.Properties)
            Dim cmdPalette As Command = commands.FindCommand(ChartCommandType.SelectPaletteGroup)
            Dim cmdChartType As Command = commands.FindCommand(ChartCommandType.SelectChartGroup)
            Dim cmdToggle3D As Command = commands.FindCommand(ChartCommandType.Toggle3D)

            cmdProperties.Visible = False
            cmdPalette.Visible = False
            cmdChartType.Visible = False
            cmdToggle3D.Visible = False


        End With



        Chart1.ChartAreas.Add("Default")
        'Chart1.Series.Add(0)
        Me.Chart1.Width = "837"
        Me.Chart1.Height = "500"

        Me.Chart1.ChartAreas(0).CursorX.UserEnabled = True

    End Sub

    Private Sub GenerateGraphs()

        GenerateGraph2(Me.txtMeterID.Text, Me.txtFrequency.Text, Me.txtStartDate.Text, Me.txtEndDate.Text, Me.txtGraphType.Text)

    End Sub
    Private Sub GenerateGraph2(ByVal varMeterID As String, ByVal varFrequency As String, ByVal intStartDate As Integer, ByVal intEndDate As Integer, ByVal intGraphType As Integer)

        CleanChart()
        'SetupButtons()

        Dim chartAreaClear2 As ChartArea
        For Each chartAreaClear2 In Chart1.ChartAreas
            chartAreaClear2.AxisX.CustomLabels.Clear()
        Next




        Dim strSQL As String
        strSQL = ""
        strSQL = strSQL & " EXEC [portal.utilitymasters.co.uk].dbo.usp_MyReports_VirtualMeterAnalysis '" & varMeterID & "', " & intStartDate & ",  " & intEndDate & ", '" & varFrequency & "'"


        Using connection As New SqlConnection(conn)
            connection.Open()
            Dim command As New SqlCommand(strSQL, connection)
            command.CommandType = CommandType.Text
            Dim reader As SqlDataReader = command.ExecuteReader()
            If reader.HasRows Then
                With Chart1

                    If Me.txtFrequency.Text = "Half Hour" Then


                        If Me.txtReadingType.Text = "Cons - unit" Then

                            .Titles.Add("Consumption (" & Me.lblStartDate.Text & " - " & Me.lblEndDate.Text & ")")
                            .ChartAreas("Default").AxisY.Title = "Consumption (unit)"
                            .ChartAreas("Default").AxisY.LabelStyle.Format = "#,##0.##"
                            .DataBindCrossTab(reader, "varDisplayName", "dtmTime", "fltReading", "Date=dtmDate")
                            MakeGroupLabels(Me.Chart1, Me.Chart1.Series(0), "Date")

                        ElseIf Me.txtReadingType.Text = "Cost - x/unit" Then

                            .Titles.Add("Cost p/unit (" & Me.lblStartDate.Text & " - " & Me.lblEndDate.Text & ")")
                            .ChartAreas("Default").AxisY.Title = "Cost (p/unit)"
                            .ChartAreas("Default").AxisY.LabelStyle.Format = "#,##0.##"
                            .DataBindCrossTab(reader, "varDisplayName", "dtmTime", "fltCost", "Date=dtmDate")
                            MakeGroupLabels(Me.Chart1, Me.Chart1.Series(0), "Date")

                        ElseIf Me.txtReadingType.Text = "Carbon - CRC" Then

                            .Titles.Add("CRC Carbon (" & Me.lblStartDate.Text & " - " & Me.lblEndDate.Text & ")")
                            .ChartAreas("Default").AxisY.Title = "Carbon (CO2 tonnes)"
                            .ChartAreas("Default").AxisY.LabelStyle.Format = "#,##0.##"
                            .DataBindCrossTab(reader, "varDisplayName", "dtmTime", "fltCarbon_CRC", "Date=dtmDate")
                            MakeGroupLabels(Me.Chart1, Me.Chart1.Series(0), "Date")

                        ElseIf Me.txtReadingType.Text = "Carbon - CCA" Then

                            .Titles.Add("CCA Carbon (" & Me.lblStartDate.Text & " - " & Me.lblEndDate.Text & ")")
                            .ChartAreas("Default").AxisY.Title = "Carbon (CO2 tonnes)"
                            .ChartAreas("Default").AxisY.LabelStyle.Format = "#,##0.##"
                            .DataBindCrossTab(reader, "varDisplayName", "dtmTime", "fltCarbon_CCA", "Date=dtmDate")
                            MakeGroupLabels(Me.Chart1, Me.Chart1.Series(0), "Date")

                        End If



                    Else


                        If Me.txtReadingType.Text = "Cons - unit" Then

                            .Titles.Add("Consumption (" & Me.lblStartDate.Text & " - " & Me.lblEndDate.Text & ")")
                            .ChartAreas("Default").AxisY.Title = "Consumption (unit)"
                            .ChartAreas("Default").AxisY.LabelStyle.Format = "#,##0.##"
                            .DataBindCrossTab(reader, "varDisplayName", "dtmDate", "fltReading", "")
                            Dim chartAreaClear As ChartArea
                            For Each chartAreaClear In .ChartAreas
                                chartAreaClear.AxisX.CustomLabels.Clear()
                            Next

                        ElseIf Me.txtReadingType.Text = "Cost - x/unit" Then


                            .Titles.Add("Cost £/unit (" & Me.lblStartDate.Text & " - " & Me.lblEndDate.Text & ")")
                            .ChartAreas("Default").AxisY.Title = "Cost (£/unit)"
                            .ChartAreas("Default").AxisY.LabelStyle.Format = "#,##0.##"
                            .DataBindCrossTab(reader, "varDisplayName", "dtmDate", "fltCost", "")
                            Dim chartAreaClear As ChartArea
                            For Each chartAreaClear In .ChartAreas
                                chartAreaClear.AxisX.CustomLabels.Clear()
                            Next

                        ElseIf Me.txtReadingType.Text = "Carbon - CRC" Then

                            .Titles.Add("CRC Carbon (" & Me.lblStartDate.Text & " - " & Me.lblEndDate.Text & ")")
                            .ChartAreas("Default").AxisY.Title = "Carbon (CO2 tonnes)"
                            .ChartAreas("Default").AxisY.LabelStyle.Format = "#,##0.##"
                            .DataBindCrossTab(reader, "varDisplayName", "dtmDate", "fltCarbon_CRC", "")
                            Dim chartAreaClear As ChartArea
                            For Each chartAreaClear In .ChartAreas
                                chartAreaClear.AxisX.CustomLabels.Clear()
                            Next

                        ElseIf Me.txtReadingType.Text = "Carbon - CCA" Then

                            .Titles.Add("CCA Carbon (" & Me.lblStartDate.Text & " - " & Me.lblEndDate.Text & ")")
                            .ChartAreas("Default").AxisY.Title = "Carbon (CO2 tonnes)"
                            .ChartAreas("Default").AxisY.LabelStyle.Format = "#,##0.##"
                            .DataBindCrossTab(reader, "varDisplayName", "dtmDate", "fltCarbon_CCA", "")
                            Dim chartAreaClear As ChartArea
                            For Each chartAreaClear In .ChartAreas
                                chartAreaClear.AxisX.CustomLabels.Clear()
                            Next

                        End If


                    End If

                    ' titles and axis format
                    .Series(0)("PointWidth") = "0.9"
                    .ChartAreas("Default").AxisX.Margin = True
                    .Series(0)("EmptyPointValue") = "Zero"
                    .PaletteCustomColors = colorSet


                    Dim series As Series
                    For Each series In Me.Chart1.Series
                        series.Type = SeriesChartType.Line
                        series.XValueType = ChartValueTypes.DateTime

                        If series.Name = "Virtual Meter" Then
                            series.Type = SeriesChartType.Column
                        Else
                            series.Type = SeriesChartType.Line
                            series.BorderWidth = 2
                        End If

                    Next


                End With
            End If
        End Using

        If varFrequency = "Half Hour" Then
            Me.Chart1.ChartAreas("Default").AxisX.View.Size = 340
            Me.Chart1.ChartAreas("Default").AxisX.Interval = 24
            Me.Chart1.ChartAreas("Default").AxisX.LineWidth = "2"
            Me.Chart1.ChartAreas("Default").AxisX.LineStyle = ChartDashStyle.Solid
            'Me.Chart1.ChartAreas(0).AxisX.Enabled = True

            Me.Chart1.ChartAreas("Default").AxisX.MinorGrid.Enabled = True
            Me.Chart1.ChartAreas("Default").AxisX.MinorGrid.Interval = 2
            Me.Chart1.ChartAreas("Default").AxisX.MinorGrid.LineStyle = ChartDashStyle.Dot

            Me.Chart1.ChartAreas("Default").AxisX.LabelsAutoFit = True
        Else

            Me.Chart1.ChartAreas("Default").AxisX.Interval = 1

        End If



        If Me.Chart1.Series.Count = 0 Then
            Me.lblInformation.Text = "There is no data available for the period selected (" & Me.lblStartDate.Text & " - " & Me.lblEndDate.Text & ")"
            Me.lblInformation.ForeColor = Color.Red
            Me.Chart1.Height = "430"
        Else
            Me.lblInformation.Text = Nothing
            Me.lblInformation.ForeColor = Nothing
            Me.Chart1.Height = "450"
        End If

    End Sub


    Public Shared Function FromExcelSerialDate(ByVal SerialDate As Integer) As DateTime
        If SerialDate > 59 Then SerialDate -= 1 ''// Excel/Lotus 2/29/1900 bug    
        Return New DateTime(1899, 12, 31).AddDays(SerialDate)
    End Function

    Private Sub SetupStartandEndDates()

        If Me.rblTimescales.SelectedItem.Value = "Other" Then

            Dim strDateType As String = Replace(Me.ddlDateType.SelectedItem.Value, " ", "_")
            Dim strDateSelected As String = Me.ddlDateSelect.SelectedItem.Value

            Dim strSQL As String = "SELECT MIN(intDatePK) as ID FROM UML_CMS.dbo.DIM_Dates WHERE [" & strDateType & "] = UML_EXTData.dbo.formatdatetime('" & strDateSelected & "', 'dd/mm/yyyy')"
            Me.txtStartDate.Text = Core.data_select_value(strSQL)
            strSQL = "SELECT MAX(intDatePK) as ID FROM UML_CMS.dbo.DIM_Dates WHERE [" & strDateType & "] = UML_EXTData.dbo.formatdatetime('" & strDateSelected & "', 'dd/mm/yyyy')"
            Me.txtEndDate.Text = Core.data_select_value(strSQL)

            Me.lblStartDate.Text = Core.data_select_value("SELECT [Date_Name] FROM UML_CMS.dbo.Dim_Dates WHERE intDatePK = " & Me.txtStartDate.Text)
            Me.lblEndDate.Text = Core.data_select_value("SELECT [Date_Name] FROM UML_CMS.dbo.Dim_Dates WHERE intDatePK = " & Me.txtEndDate.Text)

            GenerateGraphs()
        End If
    End Sub

    Private Sub SetupDateSelect()

        With Me.ddlDateSelect
            .Items.Clear()
            .DataSource = Core.data_select("EXEC [portal.utilitymasters.co.uk].dbo.usp_MyReports_Parameters_CustomDateRanges '" & Me.ddlDateType.SelectedItem.Value & "'")
            .DataValueField = "ID"
            .DataTextField = "Value"
            .DataBind()
        End With

        SetupStartandEndDates()

    End Sub
    Private Sub SetupDateType()

        Me.ddlDateType.Items.Clear()

        'Me.ddlDateType.Items.Add("Custom")
        Me.ddlDateType.Items.Add("Week")
        Me.ddlDateType.Items.Add("Month")
        Me.ddlDateType.Items.Add("Quarter")
        Me.ddlDateType.Items.Add("Year")
        Me.ddlDateType.Items.Add("Fiscal Week")
        Me.ddlDateType.Items.Add("Fiscal Month")
        Me.ddlDateType.Items.Add("Fiscal Quarter")
        Me.ddlDateType.Items.Add("Fiscal Year")
        'Me.ddlDateType.Items.Add("4-4-5")
        Me.ddlDateType.SelectedItem.Value = "Week"

        SetupDateSelect()

    End Sub
    Private Sub SetupFrequency()

        Me.rblFrequency.Items.Add("Month")
        Me.rblFrequency.Items.Add("Day")
        Me.rblFrequency.Items.Add("Half Hour")

        Me.rblFrequency.SelectedValue = "Half Hour"

    End Sub
    Private Sub SetupTimescales()

        Me.rblTimescales.Items.Clear()
        Me.rblTimescales.Items.Add("Last 1 Week")
        Me.rblTimescales.Items.Add("Last 1 Month")
        Me.rblTimescales.Items.Add("Last 1 Year")
        Me.rblTimescales.Items.Add("Other")
        Me.rblTimescales.SelectedValue = "Last 1 Week"

        Dim strDateStart As String = DateAdd(DateInterval.Day, -7, System.DateTime.Now.Date)
        Dim strDateEnd As String = System.DateTime.Now.Date

        Dim strSQL As String = "SELECT MIN(intDatePK) as ID FROM UML_CMS.dbo.DIM_Dates WHERE [pk_date] = UML_EXTData.dbo.formatdatetime('" & strDateStart & "', 'dd/mm/yyyy')"
        Me.txtStartDate.Text = Core.data_select_value(strSQL)
        strSQL = "SELECT MAX(intDatePK) as ID FROM UML_CMS.dbo.DIM_Dates WHERE [pk_date] = UML_EXTData.dbo.formatdatetime('" & strDateEnd & "', 'dd/mm/yyyy')"
        Me.txtEndDate.Text = Core.data_select_value(strSQL)

        Me.lblStartDate.Text = Core.data_select_value("SELECT [Date_Name] FROM UML_CMS.dbo.Dim_Dates WHERE intDatePK = " & Me.txtStartDate.Text)
        Me.lblEndDate.Text = Core.data_select_value("SELECT [Date_Name] FROM UML_CMS.dbo.Dim_Dates WHERE intDatePK = " & Me.txtEndDate.Text)


    End Sub
    Private Sub SetupChartType()

        Me.rblChartType.Items.Clear()
        Me.rblChartType.Items.Add("Line")
        Me.rblChartType.Items.Add("Column")
        Me.rblChartType.SelectedValue = "Line"

    End Sub
    Private Sub SetupReadingType()

        Me.rblReadingType.Items.Clear()
        Me.rblReadingType.Items.Add("Cons - unit")
        Me.rblReadingType.Items.Add("Cost - x/unit")
        Me.rblReadingType.Items.Add("Carbon - CRC")
        Me.rblReadingType.Items.Add("Carbon - CCA")
        Me.rblReadingType.SelectedValue = "Cons - unit"

    End Sub

    Protected Sub chkEvents_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkEvents.CheckedChanged
        If Me.chkEvents.Checked Then
            GenerateGraphs()
        Else
            Chart1.ChartAreas("TimeLine").Visible = False
            Chart1.ChartAreas("Default").Position.Height = 65
        End If
    End Sub


    Protected Sub rblFrequency_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rblFrequency.SelectedIndexChanged

        Me.txtFrequency.Text = Me.rblFrequency.SelectedItem.Text

        GenerateGraphs()

        Me.chkEvents.Visible = False
        If Me.Chart1.ChartAreas.Count > 1 Then
            Chart1.ChartAreas("TimeLine").Visible = False
        End If

        Chart1.ChartAreas("Default").Position.X = 0
        Chart1.ChartAreas("Default").Position.Y = 23
        Chart1.ChartAreas("Default").Position.Height = 60
        Chart1.ChartAreas("Default").Position.Width = 95

    End Sub

    Protected Sub rblChartType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rblChartType.SelectedIndexChanged
        If Me.rblChartType.SelectedItem.Value = "Line" Then
            Me.txtGraphType.Text = "3"
            Dim series As Series
            For Each series In Me.Chart1.Series
                series.Type = SeriesChartType.Line
            Next
        ElseIf Me.rblChartType.SelectedItem.Value = "Column" Then
            Me.txtGraphType.Text = "10"
            Dim series As Series
            For Each series In Me.Chart1.Series
                series.Type = SeriesChartType.Column
            Next
        End If
    End Sub

    Protected Sub rblReadingType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rblReadingType.SelectedIndexChanged
        If Me.rblReadingType.SelectedIndex > -1 Then
            Me.txtReadingType.Text = Me.rblReadingType.SelectedItem.Value.ToString
        Else
            Me.txtReadingType.Text = "Cons - unit"
        End If

        GenerateGraphs()
    End Sub

    Protected Sub rblTimescales_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rblTimescales.SelectedIndexChanged
        Me.tblOtherDates.Visible = False

        If Me.rblTimescales.SelectedItem.Value = "Last 1 Year" Then

            Dim strDateStart As String = DateAdd(DateInterval.Year, -1, System.DateTime.Now.Date)
            Dim strDateEnd As String = System.DateTime.Now.Date

            Dim strSQL As String = "SELECT MIN(intDatePK) as ID FROM UML_CMS.dbo.DIM_Dates WHERE [pk_date] = UML_EXTData.dbo.formatdatetime('" & strDateStart & "', 'dd/mm/yyyy')"
            Me.txtStartDate.Text = Core.data_select_value(strSQL)
            strSQL = "SELECT MAX(intDatePK) as ID FROM UML_CMS.dbo.DIM_Dates WHERE [pk_date] = UML_EXTData.dbo.formatdatetime('" & strDateEnd & "', 'dd/mm/yyyy')"
            Me.txtEndDate.Text = Core.data_select_value(strSQL)

            Me.lblStartDate.Text = Core.data_select_value("SELECT [Date_Name] FROM UML_CMS.dbo.Dim_Dates WHERE intDatePK = " & Me.txtStartDate.Text)
            Me.lblEndDate.Text = Core.data_select_value("SELECT [Date_Name] FROM UML_CMS.dbo.Dim_Dates WHERE intDatePK = " & Me.txtEndDate.Text)



            GenerateGraphs()

        ElseIf Me.rblTimescales.SelectedItem.Value = "Last 1 Month" Then

            Dim strDateStart As String = DateAdd(DateInterval.Month, -1, System.DateTime.Now.Date)
            Dim strDateEnd As String = System.DateTime.Now.Date

            Dim strSQL As String = "SELECT MIN(intDatePK) as ID FROM UML_CMS.dbo.DIM_Dates WHERE [pk_date] = UML_EXTData.dbo.formatdatetime('" & strDateStart & "', 'dd/mm/yyyy')"
            Me.txtStartDate.Text = Core.data_select_value(strSQL)
            strSQL = "SELECT MAX(intDatePK) as ID FROM UML_CMS.dbo.DIM_Dates WHERE [pk_date] = UML_EXTData.dbo.formatdatetime('" & strDateEnd & "', 'dd/mm/yyyy')"
            Me.txtEndDate.Text = Core.data_select_value(strSQL)

            Me.lblStartDate.Text = Core.data_select_value("SELECT [Date_Name] FROM UML_CMS.dbo.Dim_Dates WHERE intDatePK = " & Me.txtStartDate.Text)
            Me.lblEndDate.Text = Core.data_select_value("SELECT [Date_Name] FROM UML_CMS.dbo.Dim_Dates WHERE intDatePK = " & Me.txtEndDate.Text)

            GenerateGraphs()

        ElseIf Me.rblTimescales.SelectedItem.Value = "Last 1 Week" Then

            Dim strDateStart As String = DateAdd(DateInterval.Day, -7, System.DateTime.Now.Date)
            Dim strDateEnd As String = System.DateTime.Now.Date

            Dim strSQL As String = "SELECT MIN(intDatePK) as ID FROM UML_CMS.dbo.DIM_Dates WHERE [pk_date] = UML_EXTData.dbo.formatdatetime('" & strDateStart & "', 'dd/mm/yyyy')"
            Me.txtStartDate.Text = Core.data_select_value(strSQL)
            strSQL = "SELECT MAX(intDatePK) as ID FROM UML_CMS.dbo.DIM_Dates WHERE [pk_date] = UML_EXTData.dbo.formatdatetime('" & strDateEnd & "', 'dd/mm/yyyy')"
            Me.txtEndDate.Text = Core.data_select_value(strSQL)

            Me.lblStartDate.Text = Core.data_select_value("SELECT [Date_Name] FROM UML_CMS.dbo.Dim_Dates WHERE intDatePK = " & Me.txtStartDate.Text)
            Me.lblEndDate.Text = Core.data_select_value("SELECT [Date_Name] FROM UML_CMS.dbo.Dim_Dates WHERE intDatePK = " & Me.txtEndDate.Text)


            GenerateGraphs()


        ElseIf Me.rblTimescales.SelectedItem.Value = "Other" Then
            Me.tblOtherDates.Visible = True


            Dim strDateType As String = Me.ddlDateType.SelectedItem.Value
            Dim strDateSelected As String = Me.ddlDateSelect.SelectedItem.Value

            Dim strSQL As String = "SELECT MIN(intDatePK) as ID FROM UML_CMS.dbo.DIM_Dates WHERE [" & strDateType & "] = UML_EXTData.dbo.formatdatetime('" & strDateSelected & "', 'dd/mm/yyyy')"
            Me.txtStartDate.Text = Core.data_select_value(strSQL)
            strSQL = "SELECT MAX(intDatePK) as ID FROM UML_CMS.dbo.DIM_Dates WHERE [" & strDateType & "] = UML_EXTData.dbo.formatdatetime('" & strDateSelected & "', 'dd/mm/yyyy')"
            Me.txtEndDate.Text = Core.data_select_value(strSQL)

            Me.lblStartDate.Text = Core.data_select_value("SELECT [Date_Name] FROM UML_CMS.dbo.Dim_Dates WHERE intDatePK = " & Me.txtStartDate.Text)
            Me.lblEndDate.Text = Core.data_select_value("SELECT [Date_Name] FROM UML_CMS.dbo.Dim_Dates WHERE intDatePK = " & Me.txtEndDate.Text)

            GenerateGraphs()

        End If



    End Sub

    Protected Sub ddlDateType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlDateType.SelectedIndexChanged

        SetupDateSelect()
        'SetupStartandEndDates()

    End Sub

    Protected Sub ddlDateSelect_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlDateSelect.SelectedIndexChanged
        SetupStartandEndDates()

    End Sub



End Class