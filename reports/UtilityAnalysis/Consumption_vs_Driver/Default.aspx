<%@ Page Language="VB" MasterPageFile="~/landing.master" AutoEventWireup="false" CodeFile="Default.aspx.vb" Inherits="reports_onlinegraph_Default" title="UTILISYS - Consumption vs Driver" %>

<%@ Register Assembly="DundasWebChart" Namespace="Dundas.Charting.WebControl" TagPrefix="DCWC" %>
<asp:Content ID="Content1" ContentPlaceHolderID="content_right" Runat="Server">

        <script type="text/javascript" src="../../../ThickBox/jquery-1.3.2.min.js"></script>
        <script type="text/javascript" src="../../../ThickBox/thickbox.js"></script>
        <link rel="stylesheet" href="../../../ThickBox/thickbox.css" type="text/css" media="screen" />   
        
<form id="Form1" runat="server">

    <div class="content_back general">
            <table cellpadding="0" cellspacing="0">
                <tr valign="middle">
                    <td style="width:100px;" >
                        <asp:Label ID="lblDescription" Text="Description" runat="server" Font-Bold="true"></asp:Label>
                    </td>
                    <td style="width:400px;" align="left">
                        <asp:Label ID="lblName" Text="Name" runat="server"></asp:Label>
                    </td>
                    <td style="width:150px;">
                        
                    </td>
                </tr> <!-- Company selection -->
                
                <tr valign="middle">
                    <td >
                        <b>Report</b>
                    </td>
                    <td align="left">
                        Consumption vs Driver Report
                    </td>
                    <td>
                        
                    </td>
                </tr> <!-- Report Name -->
                
                <tr>
                    
                    <td colspan="3">
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td style="width:120px;" valign="top">
                                    <asp:label ID="lblFilterNameAvailable" runat="server" Text="Available Meters" Width="120px" Font-Bold="true"></asp:label>
                                </td>
                                <td style="width:400px;" valign="middle">
                                    <asp:RadioButtonList Width="450px" ID="rblMeterFilter" runat="server" RepeatLayout="table" RepeatDirection="Horizontal" AutoPostBack="true">
                                        <asp:ListItem Value="All" Text="Show All" Selected="True"></asp:ListItem>
                                        <asp:ListItem Value="Main" Text="Main Meters"></asp:ListItem>
                                        <asp:ListItem Value="Sub" Text="Sub Meters"></asp:ListItem>
                                        <asp:ListItem Value="Virtual" Text="Virtual Meters"></asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>
                            </tr>
                            
                            <tr>
                                <td colspan="2">
                                    <asp:ListBox ID="lbAvailableMeters" runat="server" style="width:630px; height:100px;" SelectionMode="Single" AutoPostBack="true" BackColor="#FFFFC0" >
                                    </asp:ListBox>
                                </td>
                            </tr>
                            
                            <tr>
                                <td>
                                    <asp:label ID="lblFilterNameSelected" runat="server" Text="Available Drivers" Width="120px" Font-Bold="true"></asp:label>
                                </td>
                                <td>
                                    <asp:Label ID="lblAssignedMeterError" Text="" runat="server" ForeColor="Red"></asp:Label>
                                </td>
                            </tr>
                            
                            <tr>
                                <td colspan="2">
                                    <asp:ListBox ID="lbAvailableDrivers" runat="server" style="width:630px; height:100px;" SelectionMode="Single" AutoPostBack="true"  BackColor="#FFFFC0">
                                    </asp:ListBox>                                    
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr> <!-- Meter Selection Table -->
                
                <tr valign="middle">
                    <td valign="top" >
                        <b>Granularity</b>
                    </td>
                    <td align="left">
                        <asp:RadioButtonList Width="450px" ID="rblGranularity" runat="server" RepeatLayout="table" RepeatDirection="Horizontal" AutoPostBack="true">
                            <asp:ListItem Value="Daily" Text="Daily" Selected="True"></asp:ListItem>
                            <asp:ListItem Value="Weekly" Text="Weekly"></asp:ListItem>
                            <asp:ListItem Value="Monthly" Text="Monthly"></asp:ListItem>
                        </asp:RadioButtonList>                        
                    </td>
                    <td >
                        
                    </td>
                </tr> <!-- Granularity -->
                
                <tr valign="middle">
                    <td valign="top" >
                        <b>Dates</b>
                    </td>
                    <td align="left">
                        <asp:RadioButtonList Width="450px" ID="rblDate" runat="server" RepeatLayout="table" RepeatDirection="Horizontal" AutoPostBack="true">
                            <asp:ListItem Value="Week" Text="Rolling Week" Selected="True"></asp:ListItem>
                            <asp:ListItem Value="Month" Text="Rolling Month"></asp:ListItem>
                            <asp:ListItem Value="Year" Text="Rolling Year"></asp:ListItem>
                            <asp:ListItem Value="Custom" Text="Custom"></asp:ListItem>
                        </asp:RadioButtonList>                        
                    </td>
                    <td >
                        
                    </td>
                </tr> <!-- Dates -->
                
                <tr valign="middle">
                    <td valign="top" colspan="3" >
                        <asp:Table ID="tblCustomDates" runat="server" Visible="true">
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:DropDownList ID="ddlStartDay" runat="server" Width="50px" BackColor="#FFFFC0" AutoPostBack="true"></asp:DropDownList>
                                </asp:TableCell>
                                <asp:TableCell>
                                    <asp:DropDownList ID="ddlStartMonth" runat="server" Width="132px" BackColor="#FFFFC0" AutoPostBack="true"></asp:DropDownList>
                                </asp:TableCell>
                                <asp:TableCell>
                                    <asp:DropDownList ID="ddlStartYear" runat="server" Width="80px" BackColor="#FFFFC0" AutoPostBack="true"></asp:DropDownList>
                                </asp:TableCell>
                                
                                <asp:TableCell>
                                    to
                                </asp:TableCell>
                                
                                <asp:TableCell>
                                    <asp:DropDownList ID="ddlEndDay" runat="server" Width="50px" BackColor="#FFFFC0" AutoPostBack="true"></asp:DropDownList>
                                </asp:TableCell>
                                <asp:TableCell>
                                    <asp:DropDownList ID="ddlEndMonth" runat="server" Width="132px" BackColor="#FFFFC0" AutoPostBack="true"></asp:DropDownList>
                                </asp:TableCell>
                                <asp:TableCell>
                                    <asp:DropDownList ID="ddlEndYear" runat="server" Width="80px" BackColor="#FFFFC0" AutoPostBack="true"></asp:DropDownList>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </td>
                </tr> <!-- Dates -->
                
                <tr>
                    <td style="height:10px;">
                    </td>
                </tr>
                
                <tr>
                    <td colspan="3" style="text-align:center;">
                        <asp:HyperLink  ID="lnkNewLink" runat="server" CssClass="thickbox"></asp:HyperLink>
                    </td>
                </tr> <!-- Run button -->                 
            </table>
        </div>
 
  
  
</form>
</asp:Content>

