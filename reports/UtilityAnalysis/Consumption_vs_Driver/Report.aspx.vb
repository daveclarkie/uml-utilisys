﻿Imports Dundas.Charting.WebControl
Imports Dundas.Charting.WebControl.Utilities

Imports System.Data.SqlClient
Imports System.Data
Imports System.Drawing
Imports System.Windows


Partial Class reports_DataAnalysis_GR206_Report
    Inherits System.Web.UI.Page

    Public conn As String = Core.ConnectionString()

    Shared cPurple As Color = System.Drawing.ColorTranslator.FromHtml("#009530")
    Shared cOrange As Color = System.Drawing.ColorTranslator.FromHtml("#F18308")
    Shared cBlue As Color = System.Drawing.ColorTranslator.FromHtml("#26A7E7")

    Shared colorSet() As Color = {cPurple, cOrange}
    Private Sub CleanChart()

        Chart1.Serializer.Reset()

        With Me.Chart1

            Dim series1 As Series
            For Each series1 In .Series
                .Annotations.Clear()
            Next

            .Visible = True
            .Series.Clear()
            .Titles.Clear()
            .Legends.Clear()
            .Legends.Add("Default")

            ' legend stuff
            .Legends(0).Docking = LegendDocking.Bottom
            .Legends(0).Alignment = StringAlignment.Far
            .Legends(0).Reversed = AutoBool.False
            .Legends(0).Position.Auto = True
            .Legends(0).CellColumns.Add(New LegendCellColumn("", LegendCellColumnType.SeriesSymbol, ""))
            .Legends(0).CellColumns.Add(New LegendCellColumn("", LegendCellColumnType.Text, "#LEGENDTEXT", Drawing.ContentAlignment.MiddleLeft))
            ' Setup Frame
            .BorderColor = cPurple ' Corp Green
            .BorderStyle = ChartDashStyle.Solid
            .BorderWidth = 2
            .BorderSkin.SkinStyle = BorderSkinStyle.Emboss
            'Setup Toolbar
            .UI.Toolbar.Enabled = True
            .UI.Toolbar.Docking = ToolbarDocking.Top
            .UI.Toolbar.Placement = ToolbarPlacement.InsideChart
            .UI.Toolbar.BorderSkin.SkinStyle = BorderSkinStyle.Emboss
            .UI.Toolbar.BorderSkin.FrameBackColor = cPurple ' Corp Green

            Dim commands As CommandCollection = Chart1.UI.Commands

            Dim cmdProperties As Command = commands.FindCommand(ChartCommandType.Properties)
            Dim cmdPalette As Command = commands.FindCommand(ChartCommandType.SelectPaletteGroup)
            Dim cmdChartType As Command = commands.FindCommand(ChartCommandType.SelectChartGroup)
            Dim cmdToggle3D As Command = commands.FindCommand(ChartCommandType.Toggle3D)

            cmdProperties.Visible = False
            cmdPalette.Visible = False
            cmdChartType.Visible = False
            cmdToggle3D.Visible = False

            Me.Chart1.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFFFFF")
        End With

        Chart1.ChartAreas.Add("Default")
        'Chart1.Series.Add(0)
        Me.Chart1.Width = "800"
        Me.Chart1.Height = "400"

    End Sub
    Private Sub GenerateChart()

        Dim txtParameter_strMeter As String = Me.txtParameter_strMeter.Text
        Dim txtParameter_strDriver As String = Me.txtParameter_strDriver.Text
        Dim txtParameter_strGranularity As String = Me.txtParameter_strGranularity.Text
        Dim txtParameter_strStartDate As String = Me.txtParameter_strStartDate.Text
        Dim txtParameter_strEndDate As String = Me.txtParameter_strEndDate.Text

        Dim strSeriesName As String = "XXX"
        CleanChart()
        'SetupButtons()

        Dim strSQL As String
        strSQL = ""

        'strSQL = strSQL & " SELECT MeterID ,RecordedDate ,CAST(Figure as FLOAT) as Figure,PersonnelID ,MeterName ,MeterDepartment ,dbo.tblCustomer_Meters.TypeOf ,ScheduleType ,UsedFor FROM dbo.tblCustomer_ScheduledMeters INNER JOIN dbo.tblCustomer_Meters ON dbo.tblCustomer_ScheduledMeters.MeterID = dbo.tblCustomer_Meters.id WHERE meterid = 92 and figure > 0 ORDER BY RecordedDate asc "
        'strSQL = strSQL & " SELECT 1 as MeterID, monthdate, consumption, production FROM uml_cms.dbo.tblSECtargets INNER JOIN uml_cms.dbo.tblSECtarget_fuel ON uml_cms.dbo.tblSECtargets.sectargetid = uml_cms.dbo.tblSECtarget_fuel.SECtargetid WHERE tblSECtargets.productid = 10 AND fuelid = 1 ORDER BY production asc"
        'strSQL = strSQL & " EXEC [UML_ExtData].dbo.usp_Portal_MyReports_Consumption_vs_Driver '2336500064010_1', '93', 'Daily', '07 March 2009', '07 April 2010' "
        strSQL = strSQL & " EXEC [UML_ExtData].dbo.usp_Portal_MyReports_Consumption_vs_Driver '" & txtParameter_strMeter & "', '" & txtParameter_strDriver & "', '" & txtParameter_strGranularity & "', '" & txtParameter_strStartDate & "', '" & txtParameter_strEndDate & "' "

        Using connection As New SqlConnection(conn)
            connection.Open()
            Dim command As New SqlCommand(strSQL, connection)
            command.CommandType = CommandType.Text
            Dim reader As SqlDataReader = command.ExecuteReader()
            If reader.HasRows Then
                With Chart1

                    .DataBindCrossTab(reader, "varName", "fltDriver", "fltConsumption", "Date=dtmDate{D}")

                    ' titles and axis format    
                    .Series(0)("PointWidth") = "1.5"
                    .ChartAreas("Default").AxisX.Margin = False
                    .Titles.Add("Consumption vs Driver Report")


                    .ChartAreas("Default").AxisX.Minimum = 0
                    .Series(0)("EmptyPointValue") = "Zero"
                    .ChartAreas("Default").AxisY.Title = "Consumption (unit)"
                    .ChartAreas("Default").AxisY.LabelStyle.Format = "#,###"
                    .ChartAreas("Default").AxisX.Title = "Driver"

                    .PaletteCustomColors = colorSet
                    .ChartAreas(0).BackColor = Color.White

                    Dim series As Series
                    For Each series In Me.Chart1.Series
                        series.Type = SeriesChartType.FastPoint
                        series.XValueType = ChartValueTypes.Double
                        strSeriesName = series.Name

                        series.BorderWidth = 2
                        'series.XValueType = ChartValueTypes.DateTime
                        If series.Name = 1 Then
                            series.ShowInLegend = False
                        End If
                    Next

                    ' Add the TrendLine series
                    .Series.Add("TrendLine")
                    .Series("TrendLine").Type = SeriesChartType.Line
                    ' Line of best fit is linear
                    ' "Linear", "Exponential", "Logarithmic", or "Power".
                    ' Polynomial is represented by an integer value in the form of a string.
                    Dim typeRegression As String = "Linear"
                    ' The number of days for Forecasting
                    Dim forecasting As String = "10"
                    ' Show Error as a range chart.
                    Dim [error] As String = "False"
                    ' Show Forecasting Error as a range chart.
                    Dim forecastingError As String = "false"
                    ' Formula parameters
                    Dim parameters As String = typeRegression + ","c + forecasting + ","c + [error] + ","c + forecastingError
                    ' Create Forecasting Series.
                    .DataManipulator.FormulaFinancial(FinancialFormula.Forecasting, parameters, Chart1.Series(0), Chart1.Series("TrendLine"))

                End With
            End If
        End Using
        If strSeriesName <> "XXX" Then
            ' Calculate Mean
            Dim mean As Double = Chart1.DataManipulator.Statistics.Mean(strSeriesName)
            ' Calculate Median
            Dim median As Double = Chart1.DataManipulator.Statistics.Median(strSeriesName)
            ' Calculate Variance, and then Standard Deviation
            Dim variance As Double = Chart1.DataManipulator.Statistics.Variance(strSeriesName, True)
            Dim stdeviation As Double = Math.Sqrt(variance)

            Dim stripLine As New StripLine
            stripLine.IntervalOffset = mean - stdeviation
            stripLine.StripWidth = stdeviation * 2
            stripLine.BackColor = Color.PowderBlue
            stripLine.BorderColor = Color.Red
            Chart1.ChartAreas("Default").AxisY.StripLines.Add(stripLine)
        End If

        Dim point As DataPoint
        If Me.Chart1.Series.Count > 0 Then
            For Each point In Chart1.Series(0).Points
                point.ToolTip = "Date: " + point("Date") + ControlChars.Lf + " Driver: " + "#VALX" + ControlChars.Lf + " Cons: " + "#VALY"
            Next
        End If
        '.Series(0).ToolTip = Chart1.Series(0).ToolTip & ControlChars.Lf +  + ControlChars.Lf + 

    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim strMeter As String = ""
        Dim strDriver As String = ""
        Dim strGranularity As String = ""
        Dim strStartDate As String = ""
        Dim strEndDate As String = ""
        Dim strDateType As String = ""

        If Not Page.IsPostBack Then
            strMeter = Request.QueryString("meterid")
            strDriver = Request.QueryString("driverid")
            strGranularity = Request.QueryString("granularity")
            strStartDate = Request.QueryString("startdate")
            strEndDate = Request.QueryString("enddate")
            strDateType = Request.QueryString("datetype")

            Me.txtParameter_strMeter.Text = strMeter
            Me.txtParameter_strDriver.Text = strDriver
            Me.txtParameter_strGranularity.Text = strGranularity
            Me.txtParameter_strStartDate.Text = strStartDate
            Me.txtParameter_strEndDate.Text = strEndDate

            LoadDates()


            Me.rblGranularity.SelectedValue = Me.txtParameter_strGranularity.Text

            Dim strStartDay As String = DatePart(DateInterval.Day, CDate(Me.txtParameter_strStartDate.Text))
            If strStartDay.Length = 1 Then strStartDay = "0" & strStartDay
            Me.ddlStartDay.SelectedValue = strStartDay
            Me.ddlStartMonth.SelectedValue = MonthName(DatePart(DateInterval.Month, CDate(Me.txtParameter_strStartDate.Text)))
            Me.ddlStartYear.SelectedValue = DatePart(DateInterval.Year, CDate(Me.txtParameter_strStartDate.Text))

            Dim strEndDay As String = DatePart(DateInterval.Day, CDate(Me.txtParameter_strEndDate.Text))
            If strEndDay.Length = 1 Then strEndDay = "0" & strEndDay
            Me.ddlEndDay.SelectedValue = strEndDay
            Me.ddlEndMonth.SelectedValue = MonthName(DatePart(DateInterval.Month, CDate(Me.txtParameter_strEndDate.Text)))
            Me.ddlEndYear.SelectedValue = DatePart(DateInterval.Year, CDate(Me.txtParameter_strEndDate.Text))

            Me.rblDate.SelectedValue = strDateType

        Else

        End If

        Dim dtmStartDate As String = Me.ddlStartDay.SelectedValue & " " & Me.ddlStartMonth.SelectedValue & " " & Me.ddlStartYear.SelectedValue
        Dim dtmEndDate As String = Me.ddlEndDay.SelectedValue & " " & Me.ddlEndMonth.SelectedValue & " " & Me.ddlEndYear.SelectedValue

        Me.txtParameter_strStartDate.Text = dtmStartDate
        Me.txtParameter_strEndDate.Text = dtmEndDate


        GenerateChart()

    End Sub



    Protected Sub rblGranularity_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rblGranularity.SelectedIndexChanged


        SortOutTheRadioButtonLists()
        Me.txtParameter_strGranularity.Text = Me.rblGranularity.SelectedItem.Value


        GenerateChart()
    End Sub

    Private Sub LoadDates()

        Me.ddlStartDay.Items.Clear()
        Me.ddlEndDay.Items.Clear()

        Me.ddlStartMonth.Items.Clear()
        Me.ddlEndMonth.Items.Clear()

        With Me.ddlStartDay
            .Items.Add("01")
            .Items.Add("02")
            .Items.Add("03")
            .Items.Add("04")
            .Items.Add("05")
            .Items.Add("06")
            .Items.Add("07")
            .Items.Add("08")
            .Items.Add("09")
            .Items.Add("10")
            .Items.Add("11")
            .Items.Add("12")
            .Items.Add("13")
            .Items.Add("14")
            .Items.Add("15")
            .Items.Add("16")
            .Items.Add("17")
            .Items.Add("18")
            .Items.Add("19")
            .Items.Add("20")
            .Items.Add("21")
            .Items.Add("22")
            .Items.Add("23")
            .Items.Add("24")
            .Items.Add("25")
            .Items.Add("26")
            .Items.Add("27")
            .Items.Add("28")
            .Items.Add("29")
            .Items.Add("30")
            .Items.Add("31")
        End With
        With Me.ddlStartMonth
            .Items.Add("January")
            .Items.Add("February")
            .Items.Add("March")
            .Items.Add("April")
            .Items.Add("May")
            .Items.Add("June")
            .Items.Add("July")
            .Items.Add("August")
            .Items.Add("September")
            .Items.Add("October")
            .Items.Add("November")
            .Items.Add("December")
        End With
        With Me.ddlStartYear
            .Items.Clear()
            .DataSource = Core.data_select("SELECT DISTINCT DATEPART(YEAR, YEAR) AS Year FROM uml_cms.dbo.DIM_dates WHERE pk_date < GETDATE() AND pk_date > '01/01/2008' ORDER BY DATEPART(YEAR, YEAR) DESC")
            .DataValueField = "Year"
            .DataTextField = "Year"
            .DataBind()
        End With

        With Me.ddlEndDay
            .Items.Add("01")
            .Items.Add("02")
            .Items.Add("03")
            .Items.Add("04")
            .Items.Add("05")
            .Items.Add("06")
            .Items.Add("07")
            .Items.Add("08")
            .Items.Add("09")
            .Items.Add("10")
            .Items.Add("11")
            .Items.Add("12")
            .Items.Add("13")
            .Items.Add("14")
            .Items.Add("15")
            .Items.Add("16")
            .Items.Add("17")
            .Items.Add("18")
            .Items.Add("19")
            .Items.Add("20")
            .Items.Add("21")
            .Items.Add("22")
            .Items.Add("23")
            .Items.Add("24")
            .Items.Add("25")
            .Items.Add("26")
            .Items.Add("27")
            .Items.Add("28")
            .Items.Add("29")
            .Items.Add("30")
            .Items.Add("31")
        End With
        With Me.ddlEndMonth
            .Items.Add("January")
            .Items.Add("February")
            .Items.Add("March")
            .Items.Add("April")
            .Items.Add("May")
            .Items.Add("June")
            .Items.Add("July")
            .Items.Add("August")
            .Items.Add("September")
            .Items.Add("October")
            .Items.Add("November")
            .Items.Add("December")
        End With
        With Me.ddlEndYear
            .Items.Clear()
            .DataSource = Core.data_select("SELECT DISTINCT DATEPART(YEAR, YEAR) AS Year FROM uml_cms.dbo.DIM_dates WHERE pk_date < GETDATE() AND pk_date > '01/01/2008' ORDER BY DATEPART(YEAR, YEAR) DESC")
            .DataValueField = "Year"
            .DataTextField = "Year"
            .DataBind()
        End With

    End Sub

    Private Sub SortOutTheRadioButtonLists()

        Me.rblDate.Items.Item(0).Enabled = False
        Me.rblDate.Items.Item(1).Enabled = False
        Me.rblDate.Items.Item(2).Enabled = False
        Me.rblDate.Items.Item(3).Enabled = False

        If Me.rblGranularity.SelectedItem.Value = "Daily" Then
            Me.rblDate.Items.Item(0).Enabled = True
            Me.rblDate.Items.Item(1).Enabled = True
            Me.rblDate.Items.Item(2).Enabled = True
            Me.rblDate.Items.Item(3).Enabled = True
            If Me.rblDate.SelectedItem.Enabled = False Then Me.rblDate.SelectedIndex = 0
        End If
        If Me.rblGranularity.SelectedItem.Value = "Weekly" Then
            Me.rblDate.Items.Item(1).Enabled = True
            Me.rblDate.Items.Item(2).Enabled = True
            Me.rblDate.Items.Item(3).Enabled = True
            If Me.rblDate.SelectedItem.Enabled = False Then Me.rblDate.SelectedIndex = 1
        End If
        If Me.rblGranularity.SelectedItem.Value = "Monthly" Then
            Me.rblDate.Items.Item(2).Enabled = True
            Me.rblDate.Items.Item(3).Enabled = True
            If Me.rblDate.SelectedItem.Enabled = False Then Me.rblDate.SelectedIndex = 2
        End If

    End Sub

    Protected Sub rblDate_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rblDate.SelectedIndexChanged


        If Me.rblDate.SelectedItem.Value = "Custom" Then
            Me.tblCustomDates.Visible = True
        Else
            Me.tblCustomDates.Visible = False
        End If

        Dim strRange As String = Me.rblDate.SelectedItem.Value

        Dim DateNow As Date = Now()
        Dim varDateDay As String = DatePart("d", DateNow)
        If varDateDay.Length = 1 Then varDateDay = "0" & varDateDay

        Me.ddlEndDay.SelectedValue = varDateDay
        Me.ddlEndMonth.SelectedValue = MonthName(DatePart("m", DateNow))
        Me.ddlEndYear.SelectedValue = DatePart("yyyy", DateNow)

        Dim DateThen As Date
        Dim varDateDayThen As String

        If strRange = "Week" Then
            DateThen = DateAdd(DateInterval.Day, -7, Now())
        ElseIf strRange = "Month" Then
            DateThen = DateAdd(DateInterval.Month, -1, Now())
        ElseIf strRange = "Year" Then
            DateThen = DateAdd(DateInterval.Year, -1, Now())
        ElseIf strRange = "Custom" Then
            DateThen = DateAdd(DateInterval.Month, -1, Now())

        End If

        varDateDayThen = DatePart("d", DateThen)
        If varDateDayThen.Length = 1 Then varDateDayThen = "0" & varDateDayThen
        Me.ddlStartDay.SelectedValue = varDateDayThen
        Me.ddlStartMonth.SelectedValue = MonthName(DatePart("m", DateThen))
        Me.ddlStartYear.SelectedValue = DatePart("yyyy", DateThen)

        Dim dtmStartDate As String = Me.ddlStartDay.SelectedValue & " " & Me.ddlStartMonth.SelectedValue & " " & Me.ddlStartYear.SelectedValue
        Dim dtmEndDate As String = Me.ddlEndDay.SelectedValue & " " & Me.ddlEndMonth.SelectedValue & " " & Me.ddlEndYear.SelectedValue

        Me.txtParameter_strStartDate.Text = dtmStartDate
        Me.txtParameter_strEndDate.Text = dtmEndDate

        GenerateChart()
    End Sub

    
End Class
