﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Report.aspx.vb" Inherits="reports_DataAnalysis_GR206_Report" %>

<%@ Register Assembly="DundasWebChart" Namespace="Dundas.Charting.WebControl" TagPrefix="DCWC" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Consumption vs Driver</title>
    <link rel="stylesheet" href="../../../modalbox/modalreport.css" type="text/css" media="screen" />      

</head>
<body>
    <form id="form1" runat="server">

<table>
    
    
    
    

    <tr>
        <td>
            <table>
                <tr>
                    <td>
                        <DCWC:Chart ID="Chart1" runat="server" Width="837px" Height="350px">
                            <Legends>
                                <DCWC:Legend Name="Default"></DCWC:Legend>
                            </Legends>
                            <Series>
                                <DCWC:Series Name="Default">
                                </DCWC:Series>
                            </Series>
                            <ChartAreas>
                                <DCWC:ChartArea Name="Default">
                                </DCWC:ChartArea>
                            </ChartAreas>
                        </DCWC:Chart>
                    </td>
                </tr>
            </table> <%--Charts--%>
        </td>
    </tr><%--chart--%>
    
    <tr>
        <td style="vertical-align:top;">
            <table width="800px" style="background-color:#B7E566; border-color:Black; text-align:justify;">
                <tr>
                    <td style="width:20px;">
                    
                    </td>
                    <td style="font-weight:bold; width:150px;">
                        Granularity
                    </td>
                    <td style="font-weight:bold; width:150px;">
                        Timescale
                    </td>
                    <td style="font-weight:bold; width:150px;">
                        Custom Dates
                    </td>
                    <td style="font-weight:bold; width:150px;">
                        
                    </td>
                    <td style="width:300px;">
                        
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td style="text-align:left; vertical-align:top;">
                        <asp:RadioButtonList ID="rblGranularity" runat="server" AutoPostBack="true" RepeatDirection="Vertical" RepeatColumns="1" RepeatLayout="Table" TextAlign="Right" Width="120px">
                            <asp:ListItem Value="Daily" Text="Daily"></asp:ListItem>
                            <asp:ListItem Value="Weekly" Text="Weekly"></asp:ListItem>
                            <asp:ListItem Value="Monthly" Text="Monthly"></asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                    <td style="text-align:left; vertical-align:top;">
                        <asp:RadioButtonList ID="rblDate" runat="server" AutoPostBack="true" RepeatDirection="Vertical" RepeatColumns="1" RepeatLayout="Table" TextAlign="Right" Width="120px">
                            <asp:ListItem Value="Week" Text="Rolling Week"></asp:ListItem>
                            <asp:ListItem Value="Month" Text="Rolling Month"></asp:ListItem>
                            <asp:ListItem Value="Year" Text="Rolling Year"></asp:ListItem>
                            <asp:ListItem Value="Custom" Text="Custom"></asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                    <td style="text-align:left; vertical-align:middle;" colspan="2">
                        <asp:Table ID="tblCustomDates" runat="server" Visible="false" Width="262px" BackColor="white" BorderColor="black" BorderStyle="Solid" BorderWidth="1px">
                                                        
                            <asp:TableRow>    
                                <asp:TableCell ColumnSpan="3">
                                    Start Date
                                </asp:TableCell>
                            </asp:TableRow>
                            
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:DropDownList ID="ddlStartDay" runat="server" Width="50px" BackColor="#FFFFC0" AutoPostBack="true"></asp:DropDownList>
                                </asp:TableCell>
                                <asp:TableCell>
                                    <asp:DropDownList ID="ddlStartMonth" runat="server" Width="132px" BackColor="#FFFFC0" AutoPostBack="true"></asp:DropDownList>
                                </asp:TableCell>
                                <asp:TableCell>
                                    <asp:DropDownList ID="ddlStartYear" runat="server" Width="80px" BackColor="#FFFFC0" AutoPostBack="true"></asp:DropDownList>
                                </asp:TableCell>
                            </asp:TableRow>
                            
                            <asp:TableRow>    
                                <asp:TableCell ColumnSpan="3">
                                    End Date
                                </asp:TableCell>
                            </asp:TableRow>
                            
                            <asp:TableRow>    
                                <asp:TableCell>
                                    <asp:DropDownList ID="ddlEndDay" runat="server" Width="50px" BackColor="#FFFFC0" AutoPostBack="true"></asp:DropDownList>
                                </asp:TableCell>
                                <asp:TableCell>
                                    <asp:DropDownList ID="ddlEndMonth" runat="server" Width="132px" BackColor="#FFFFC0" AutoPostBack="true"></asp:DropDownList>
                                </asp:TableCell>
                                <asp:TableCell>
                                    <asp:DropDownList ID="ddlEndYear" runat="server" Width="80px" BackColor="#FFFFC0" AutoPostBack="true"></asp:DropDownList>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </td>
                    <td style="text-align:left; vertical-align:top;">
                        
                    </td>
                </tr>
                
                <tr>
                    <td colspan="5">
                        <asp:TextBox ReadOnly="true" ID="txtParameter_strMeter" runat="server" Width="10px" Visible="false"></asp:TextBox>
                        <asp:TextBox ReadOnly="true" ID="txtParameter_strDriver" runat="server" Width="10px" Visible="false"></asp:TextBox>
                        <asp:TextBox ReadOnly="true" ID="txtParameter_strGranularity" runat="server" Width="10px" Visible="false"></asp:TextBox>
                        <asp:TextBox ReadOnly="true" ID="txtParameter_strStartDate" runat="server" Width="100px" Visible="false"></asp:TextBox>
                        <asp:TextBox ReadOnly="true" ID="txtParameter_strEndDate" runat="server" Width="100px" Visible="false"></asp:TextBox>
                    </td>
                </tr>
                
            </table>
        </td>
    </tr><%--table--%>           

</table> 



  
       


    </form>
</body>
</html>
