Imports Dundas.Charting.WebControl
Imports System.Drawing

Partial Class reports_onlinegraph_Default
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            If Not IIf(Core.data_select_value("select bitAllowMyReports from tblUsers where intUserPK = " & MySession.UserID) = "True", True, False) Then
                Response.Redirect(Core.data_select_value("select RawHomeURL from vwUsers where ID = " & MySession.UserID), True)
            End If

            If Request.QueryString("custid") Is Nothing Then
                Dim strSQL As String = "EXEC spPortal_TREEVIEW_TopLevel " & MySession.UserID
                Dim myConnection As New OleDb.OleDbConnection(Core.CurrentConnectionString)

                Dim myCommand As New OleDb.OleDbCommand(strSQL, myConnection)
                myCommand.Connection.Open()
                myCommand.CommandTimeout = 180
                Dim Reader As OleDb.OleDbDataReader = myCommand.ExecuteReader(CommandBehavior.CloseConnection)

                If Reader.HasRows Then
                    Reader.Read()
                    If Reader("Type") = "Company" Then
                        Response.Redirect("default.aspx?custid=" & Reader("id") & "&method=Customer", True)
                    Else
                        Response.Redirect("default.aspx?custid=" & Reader("id") & "&method=" & Reader("Type"), True)
                    End If
                    Reader.Close()
                End If
            End If

            If Not Page.IsPostBack = True Then
                Dim description As HtmlMeta = New HtmlMeta()
                description.Name = "description"
                description.Content = "UTILISYS - Consumption vs Driver"
                Dim keywords As HtmlMeta = New HtmlMeta()
                keywords.Name = "keywords"
                keywords.Content = "UTILISYS - Consumption vs Driver"
                Page.Header.Controls.Add(description)
                Page.Header.Controls.Add(keywords)
                PopulateDates()
                MeterPoints_Show("All")
                DriverPoints_Show()
            End If


            Me.lblDescription.Text = Request.QueryString("method").ToString
            Me.lblName.Text = Core.data_select_value("EXEC spSites_MAP_CurentSelection " & MySession.UserID & ", '" & Request.QueryString("method") & "', " & Request.QueryString("custid"))


            ThickBox()

        Catch ex As Exception
            '
        End Try
    End Sub

    Public Sub ThickBox()

        If Not Page.IsPostBack = True Then
            LoadDates()
        End If


        Dim intError As Integer = 0

        Dim dtmStartDate As String = Me.ddlStartDay.SelectedValue & " " & Me.ddlStartMonth.SelectedValue & " " & Me.ddlStartYear.SelectedValue
        Dim intStartDate As Integer = Core.data_select_value("SELECT count(pk_date) FROM uml_cms.dbo.dim_dates WHERE pk_date = '" & dtmStartDate & "'")

        Dim dtmEndDate As String = Me.ddlEndDay.SelectedValue & " " & Me.ddlEndMonth.SelectedValue & " " & Me.ddlEndYear.SelectedValue
        Dim intEndDate As Integer = Core.data_select_value("SELECT count(pk_date) FROM uml_cms.dbo.dim_dates WHERE pk_date = '" & dtmEndDate & "'")

        If intStartDate = 0 Then intError += 1
        If intEndDate = 0 Then intError += 1

        If intError = 0 Then

            If CDate(dtmEndDate) < CDate(dtmStartDate) Then intError += 1
            If CDate(dtmStartDate) > Now() Then intError += 1
            If CDate(dtmEndDate) > Now() Then intError += 1

            If Me.lbAvailableMeters.SelectedIndex = -1 Then intError += 1
            If Me.lbAvailableDrivers.SelectedIndex = -1 Then intError += 1

        End If

        If intError = 0 Then 'ok

            Me.lnkNewLink.NavigateUrl = "report.aspx?meterid=" + CStr(Me.lbAvailableMeters.SelectedItem.Value) + "&driverid=" + CStr(Me.lbAvailableDrivers.SelectedItem.Value) + "&granularity=" + CStr(Me.rblGranularity.SelectedItem.Value) + "&startdate=" + CStr(dtmStartDate) + "&enddate=" + CStr(dtmEndDate) + "&datetype=" + CStr(Me.rblDate.SelectedItem.Value) + "&?keepThis=true&TB_iframe=true&height=550&width=850"
            Me.lnkNewLink.Text = "View Report"
            Me.lnkNewLink.ImageUrl = "../../../assets/Schneider/buttons-myreports-general-generatereport.png"
            Me.lnkNewLink.CssClass = "thickbox"
            Me.lnkNewLink.ToolTip = "Consumption vs Driver Report"
        Else
            Me.lnkNewLink.NavigateUrl = ""
            Me.lnkNewLink.Text = "* please select a meter"
            Me.lnkNewLink.ImageUrl = "../../../assets/Schneider/buttons-myreports-general-generatereport-disabled.png"
            Me.lnkNewLink.CssClass = ""
            'Me.lnkbtnDownload.Visible = False
        End If


    End Sub

    Public Overloads Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)

        ' Confirms that an HtmlForm control is rendered for the specified ASP.NET server control at run time.

    End Sub

    Private Sub MeterPoints_Show(ByVal Type As String)

        With Me.lbAvailableMeters
            .DataSource = Core.data_select(" EXEC uml_extdata.dbo.usp_portal_MeterPoints_Display " & MySession.UserID & ", '" & Request.QueryString("method") & "', " & Request.QueryString("custid") & ",'" & Type & "', ''")
            .DataValueField = "MPAN"
            .DataTextField = "MPANSiteName"
            .DataBind()
        End With

    End Sub

    Private Sub DriverPoints_Show()

        With Me.lbAvailableDrivers
            .DataSource = Core.data_select(" EXEC  uml_extdata.dbo.usp_portal_DriverPoints_Display " & MySession.UserID & ", '" & Request.QueryString("method") & "', " & Request.QueryString("custid"))
            .DataValueField = "ID"
            .DataTextField = "Name"
            .DataBind()
        End With

    End Sub

    Protected Sub rblMeterFilter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rblMeterFilter.SelectedIndexChanged
        MeterPoints_Show(Me.rblMeterFilter.SelectedItem.Value)
    End Sub

    Protected Sub lbAvailableMeters_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbAvailableMeters.SelectedIndexChanged

        ThickBox()

    End Sub
    Protected Sub lbAvailableDrivers_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbAvailableDrivers.SelectedIndexChanged

        ThickBox()

    End Sub

    Protected Sub rblDate_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rblDate.SelectedIndexChanged

        If Me.rblDate.SelectedItem.Value = "Custom" Then
            Me.tblCustomDates.Visible = True
        Else
            Me.tblCustomDates.Visible = True
        End If

        LoadDates()


    End Sub
    Private Sub LoadDates()

        Dim DateNow As Date = Now()
        Dim varDateDay As String = DatePart("d", DateNow)
        If varDateDay.Length = 1 Then varDateDay = "0" & varDateDay

        Me.ddlEndDay.SelectedValue = varDateDay
        Me.ddlEndMonth.SelectedValue = MonthName(DatePart("m", DateNow))
        Me.ddlEndYear.SelectedValue = DatePart("yyyy", DateNow)

        Dim DateThen As Date
        Dim varDateDayThen As String

        If Me.rblDate.SelectedItem.Value = "Week" Then
            DateThen = DateAdd(DateInterval.Day, -7, Now())
        ElseIf Me.rblDate.SelectedItem.Value = "Month" Then
            DateThen = DateAdd(DateInterval.Month, -1, Now())
        ElseIf Me.rblDate.SelectedItem.Value = "Year" Then
            DateThen = DateAdd(DateInterval.Year, -1, Now())
        ElseIf Me.rblDate.SelectedItem.Value = "Custom" Then
            DateThen = DateAdd(DateInterval.Month, -1, Now())

        End If

        varDateDayThen = DatePart("d", DateThen)
        If varDateDayThen.Length = 1 Then varDateDayThen = "0" & varDateDayThen
        Me.ddlStartDay.SelectedValue = varDateDayThen
        Me.ddlStartMonth.SelectedValue = MonthName(DatePart("m", DateThen))
        Me.ddlStartYear.SelectedValue = DatePart("yyyy", DateThen)


    End Sub
    Private Sub PopulateDates()

        Me.ddlStartDay.Items.Clear()
        Me.ddlEndDay.Items.Clear()

        Me.ddlStartMonth.Items.Clear()
        Me.ddlEndMonth.Items.Clear()

        With Me.ddlStartDay
            .Items.Add("01")
            .Items.Add("02")
            .Items.Add("03")
            .Items.Add("04")
            .Items.Add("05")
            .Items.Add("06")
            .Items.Add("07")
            .Items.Add("08")
            .Items.Add("09")
            .Items.Add("10")
            .Items.Add("11")
            .Items.Add("12")
            .Items.Add("13")
            .Items.Add("14")
            .Items.Add("15")
            .Items.Add("16")
            .Items.Add("17")
            .Items.Add("18")
            .Items.Add("19")
            .Items.Add("20")
            .Items.Add("21")
            .Items.Add("22")
            .Items.Add("23")
            .Items.Add("24")
            .Items.Add("25")
            .Items.Add("26")
            .Items.Add("27")
            .Items.Add("28")
            .Items.Add("29")
            .Items.Add("30")
            .Items.Add("31")
        End With
        With Me.ddlStartMonth
            .Items.Add("January")
            .Items.Add("February")
            .Items.Add("March")
            .Items.Add("April")
            .Items.Add("May")
            .Items.Add("June")
            .Items.Add("July")
            .Items.Add("August")
            .Items.Add("September")
            .Items.Add("October")
            .Items.Add("November")
            .Items.Add("December")
        End With
        With Me.ddlStartYear
            .Items.Clear()
            .DataSource = Core.data_select("SELECT DISTINCT DATEPART(YEAR, YEAR) AS Year FROM uml_cms.dbo.DIM_dates WHERE pk_date < GETDATE() AND pk_date > '01/01/2008' ORDER BY DATEPART(YEAR, YEAR) DESC")
            .DataValueField = "Year"
            .DataTextField = "Year"
            .DataBind()
        End With

        With Me.ddlEndDay
            .Items.Add("01")
            .Items.Add("02")
            .Items.Add("03")
            .Items.Add("04")
            .Items.Add("05")
            .Items.Add("06")
            .Items.Add("07")
            .Items.Add("08")
            .Items.Add("09")
            .Items.Add("10")
            .Items.Add("11")
            .Items.Add("12")
            .Items.Add("13")
            .Items.Add("14")
            .Items.Add("15")
            .Items.Add("16")
            .Items.Add("17")
            .Items.Add("18")
            .Items.Add("19")
            .Items.Add("20")
            .Items.Add("21")
            .Items.Add("22")
            .Items.Add("23")
            .Items.Add("24")
            .Items.Add("25")
            .Items.Add("26")
            .Items.Add("27")
            .Items.Add("28")
            .Items.Add("29")
            .Items.Add("30")
            .Items.Add("31")
        End With
        With Me.ddlEndMonth
            .Items.Add("January")
            .Items.Add("February")
            .Items.Add("March")
            .Items.Add("April")
            .Items.Add("May")
            .Items.Add("June")
            .Items.Add("July")
            .Items.Add("August")
            .Items.Add("September")
            .Items.Add("October")
            .Items.Add("November")
            .Items.Add("December")
        End With
        With Me.ddlEndYear
            .Items.Clear()
            .DataSource = Core.data_select("SELECT DISTINCT DATEPART(YEAR, YEAR) AS Year FROM uml_cms.dbo.DIM_dates WHERE pk_date < GETDATE() AND pk_date > '01/01/2008' ORDER BY DATEPART(YEAR, YEAR) DESC")
            .DataValueField = "Year"
            .DataTextField = "Year"
            .DataBind()
        End With

       LoadDates


    End Sub

    Protected Sub rblGranularity_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rblGranularity.SelectedIndexChanged

        Me.rblDate.Items.Item(0).Enabled = False
        Me.rblDate.Items.Item(1).Enabled = False
        Me.rblDate.Items.Item(2).Enabled = False
        Me.rblDate.Items.Item(3).Enabled = False

        If Me.rblGranularity.SelectedItem.Value = "Daily" Then
            Me.rblDate.Items.Item(0).Enabled = True
            Me.rblDate.Items.Item(1).Enabled = True
            Me.rblDate.Items.Item(2).Enabled = True
            Me.rblDate.Items.Item(3).Enabled = True
            If Me.rblDate.SelectedItem.Enabled = False Then Me.rblDate.SelectedIndex = 0
        End If
        If Me.rblGranularity.SelectedItem.Value = "Weekly" Then
            Me.rblDate.Items.Item(1).Enabled = True
            Me.rblDate.Items.Item(2).Enabled = True
            Me.rblDate.Items.Item(3).Enabled = True
            If Me.rblDate.SelectedItem.Enabled = False Then Me.rblDate.SelectedIndex = 1
        End If
        If Me.rblGranularity.SelectedItem.Value = "Monthly" Then
            Me.rblDate.Items.Item(2).Enabled = True
            Me.rblDate.Items.Item(3).Enabled = True
            If Me.rblDate.SelectedItem.Enabled = False Then Me.rblDate.SelectedIndex = 2
        End If

        ThickBox()

    End Sub
End Class
