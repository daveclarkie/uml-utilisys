Imports Dundas.Charting.WebControl
Imports Dundas.Charting.WebControl.Utilities

Imports Dundas.Charting.WebControl.ChartTypes

Partial Class reports_UtilityAnalysis_CompareMeterPoints_Gantt
    Inherits System.Web.UI.Page

    Protected Sub Chart_Customize(ByVal sender As Chart) Handles Chart1.Customize

        For Each label As CustomLabel In Me.Chart1.ChartAreas(0).AxisX.CustomLabels
            If label.RowIndex = 0 Then
                label.ToolTip = label.Text
                label.Href = "http://www.google.com/search?q=" + label.Text
                label.Text = ""
                'label.MapAreaAttributes = "onmouseover=""MyJavaScriptFunction();"""
            End If
        Next

        Dim SeriesChecked As Series
        For Each SeriesChecked In Chart1.Series
            If SeriesChecked.Name = "Gantt" Then

                Dim point As DataPoint
                For Each point In SeriesChecked.Points

                    Dim d1 As Double
                    Dim d2 As Double
                    Dim i As Integer
                    d1 = point.YValues(0)
                    d2 = point.YValues(1)
                    i = point.XValue

                    Dim dt1 As DateTime
                    Dim dt2 As DateTime
                    dt1 = DateTime.FromOADate(d1)
                    dt2 = DateTime.FromOADate(d2)

                    Dim strEvent As String = Core.data_select_value("SELECT varEventDescription FROM UML_CMS.dbo.tblSitesEvents WHERE dtmEventStart = '" & dt1 & "' and dtmEventEnd = '" & dt2 & "' and intSiteFK = " & i)

                    point.Label = strEvent

                    SeriesChecked.SmartLabels.Enabled = True

                Next
            End If
        Next

        Dim annotation As Annotation
        For Each annotation In Chart1.Annotations
            annotation.SmartLabels.Enabled = True
        Next annotation

        Dim chartarea1 As ChartArea
        For Each chartarea1 In Me.Chart1.ChartAreas
            chartarea1.CursorX.UserEnabled = True
        Next

        Chart1.ChartAreas("Gantt").AxisX.LabelStyle.Enabled = False
        Chart1.ChartAreas("Gantt").AxisX.MajorTickMark.Enabled = False
        Chart1.ChartAreas("Gantt").AxisX.MinorTickMark.Enabled = False
        Chart1.ChartAreas("Gantt").AxisY.LabelStyle.ShowEndLabels = False
        Chart1.ChartAreas("Line").AlignOrientation = AreaAlignOrientation.Vertical
        Chart1.ChartAreas("Line").AxisY.LabelStyle.ShowEndLabels = False
        Chart1.ChartAreas("Line").AlignWithChartArea = "Gantt"
        Chart1.ChartAreas("Line").AlignType = AreaAlignType.All

        Me.Chart1.ChartAreas(0).CursorX.UserEnabled = True

    End Sub
End Class
