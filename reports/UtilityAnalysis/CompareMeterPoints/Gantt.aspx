<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Gantt.aspx.vb" Inherits="reports_UtilityAnalysis_CompareMeterPoints_Gantt" %>

<%@ Register Assembly="DundasWebChart" Namespace="Dundas.Charting.WebControl" TagPrefix="DCWC" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <dcwc:chart id="Chart1" runat="server" backcolor="#FFFFC0" borderlinecolor="LightSlateGray" borderlinestyle="Solid" datasourceid="SqlDataSource_Gantt" palette="Tanzanite" width="1022px" Height="700px">
        
            <Legends>
                <DCWC:Legend Name="Default" Enabled="False" BackColor="237, 244, 247" BorderColor="LightSlateGray" ShadowOffset="1"></DCWC:Legend>
            </Legends>

            <Series>
                <DCWC:Series ChartArea="Gantt" Name="Gantt" ValueMemberX="intSiteFK" ValueMembersY="dtmDate, dtmDate2" YValueType="DateTime" YValuesPerPoint="2" ChartType="Gantt" ShowLabelAsValue="True" BorderColor="100, 0, 0, 0" CustomAttributes="DrawingStyle=LightToDark">
                    <SmartLabels Enabled="True" />
                </DCWC:Series>
              
                <DCWC:Series ChartArea="Line" Name="Line" ValueMembersY="fltReading" ValueMemberX="dtmDatetime" XValueType="DateTime" BorderColor="64, 64, 64" BorderWidth="2" ChartType="Line" ShadowOffset="1" >
                    <SmartLabels Enabled="True" />
                </DCWC:Series>
            </Series>

            <ChartAreas>
                <DCWC:ChartArea ShadowOffset="1" BorderStyle="Solid" Name="Line">
                    <AxisY Title="Date">
                        <MajorGrid LineColor="100, 0, 0, 0" LineStyle="Dash"></MajorGrid>
                        <LabelStyle Format="g"></LabelStyle>
                    </AxisY>

                    <AxisX TitleAlignment="Far" StartFromZero="False">
                        <MajorGrid LineColor="100, 0, 0, 0" LineStyle="Dash" Enabled="False"></MajorGrid>
                        <MinorTickMark Size="2"></MinorTickMark>
                        <LabelStyle ShowEndLabels="False" />
                    </AxisX>
                </DCWC:ChartArea>
                
                <DCWC:ChartArea ShadowOffset="1" BorderStyle="Solid" Name="Gantt">
                    <AxisY Title="Date">
                        <MajorGrid LineColor="100, 0, 0, 0" LineStyle="Dash"></MajorGrid>
                        <LabelStyle Format="g"></LabelStyle>
                    </AxisY>

                    <AxisX TitleAlignment="Far" StartFromZero="False">
                        <MajorGrid LineColor="100, 0, 0, 0" LineStyle="Dash" Enabled="False"></MajorGrid>
                        <MinorTickMark Size="2"></MinorTickMark>
                        <LabelStyle ShowEndLabels="False" />
                    </AxisX>
                </DCWC:ChartArea>
                
                
            </ChartAreas>

            <BorderSkin PageColor="WhiteSmoke" SkinStyle="Emboss" FrameBackColor="SteelBlue" FrameBorderColor="100, 0, 0, 0" FrameBackGradientEndColor="LightBlue" FrameBorderWidth="2"></BorderSkin>
        </dcwc:chart>
        
        <asp:SqlDataSource ID="SqlDataSource_Gantt" runat="server" ConnectionString="<%$ ConnectionStrings:portal.utilitymasters.co.ukConnectionString %>" SelectCommand="EXEC UML_EXTData.dbo.usp_Portal_MyReports_CompareMeterPoints_HalfHourly_dev @intVirtualMeterPKList, @dtmStartDate, @dtmEndDate, @intShowSiteEvents">
            <SelectParameters>
                <asp:QueryStringParameter DefaultValue="1600000172001_1" Name="intVirtualMeterPKList" QueryStringField="meterlist" />
                <asp:QueryStringParameter DefaultValue="01/10/2009" Name="dtmStartDate" QueryStringField="startdate" />
                <asp:QueryStringParameter DefaultValue="12/10/2009" Name="dtmEndDate" QueryStringField="enddate" />
                <asp:QueryStringParameter DefaultValue="1" Name="intShowSiteEvents" QueryStringField="showevents" />
            </SelectParameters>
            
        </asp:SqlDataSource>
            
    </div>
    </form>
</body>
</html>
