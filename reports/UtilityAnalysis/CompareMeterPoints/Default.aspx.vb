Imports Dundas.Charting.WebControl
Imports System.Drawing
Imports System
Imports System.IO

Partial Class reports_onlinegraph_Default

    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
       
        Try
            If Not IIf(Core.data_select_value("select bitAllowMyReports from tblUsers where intUserPK = " & MySession.UserID) = "True", True, False) Then
                Response.Redirect(Core.data_select_value("select RawHomeURL from vwUsers where ID = " & MySession.UserID), True)
            End If

            If Request.QueryString("custid") Is Nothing Then
                Dim strSQL As String = "EXEC spPortal_TREEVIEW_TopLevel " & MySession.UserID
                Dim myConnection As New OleDb.OleDbConnection(Core.CurrentConnectionString)

                Dim myCommand As New OleDb.OleDbCommand(strSQL, myConnection)
                myCommand.Connection.Open()
                myCommand.CommandTimeout = 180
                Dim Reader As OleDb.OleDbDataReader = myCommand.ExecuteReader(CommandBehavior.CloseConnection)

                If Reader.HasRows Then
                    Reader.Read()
                    If Reader("Type") = "Company" Then
                        Response.Redirect("default.aspx?custid=" & Reader("id") & "&method=Customer", True)
                    Else
                        Response.Redirect("default.aspx?custid=" & Reader("id") & "&method=" & Reader("Type"), True)
                    End If
                    Reader.Close()
                End If
            End If

            If Not Page.IsPostBack = True Then
                PopulateDateDropDowns()
                MeterPoints_Show("All")

                Dim description As HtmlMeta = New HtmlMeta()
                description.Name = "description"
                description.Content = "UTILISYS - Compare Meter Points"
                Dim keywords As HtmlMeta = New HtmlMeta()
                keywords.Name = "keywords"
                keywords.Content = "Compare Meter Points"
                Page.Header.Controls.Add(description)
                Page.Header.Controls.Add(keywords)
                
                MeterPoints_Show(Me.rblMeterFilter.SelectedItem.Value)
                
                ThickBox()
                
            End If

            Me.lblDescription.Text = Request.QueryString("method").ToString
            Me.lblName.Text = Core.data_select_value("EXEC spSites_MAP_CurentSelection " & MySession.UserID & ", '" & Request.QueryString("method") & "', " & Request.QueryString("custid"))

            CountAssignedMeters()

            ThickBox()


            Dim sMyScreenPath As String
            Dim strCustomPage As String = Core.data_select_value("SELECT varDefaultPage FROM tblUsers where intUserPK = " & MySession.UserID)
            sMyScreenPath = System.Web.HttpContext.Current.Request.Url.AbsolutePath

            If strCustomPage = sMyScreenPath Then
                'Me.btnDefaultPage.Text = "unset default page"
            Else
                'Me.btnDefaultPage.Text = "set default page"
            End If

        Catch ex As Exception
            'MesgBox(ex.Message.ToString)
        End Try

    End Sub

    Private Sub MesgBox(ByVal sMessage As String)
        Dim msg As String
        msg = "<script language='javascript'>"
        msg += "alert('" & sMessage & "');"
        msg += "<" & "/script>"
        Response.Write(msg)
    End Sub

    Public Overloads Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)

        ' Confirms that an HtmlForm control is rendered for the specified ASP.NET server control at run time.

    End Sub
    Public Sub ThickBox()

        Dim intError As Integer = 0

        Dim dtmStartDate As String = Me.ddlStartDay.SelectedValue & " " & Me.ddlStartMonth.SelectedValue & " " & Me.ddlStartYear.SelectedValue
        Dim intStartDate As Integer = Core.data_select_value("SELECT count(pk_date) FROM uml_cms.dbo.dim_dates WHERE pk_date = '" & dtmStartDate & "'")

        Dim dtmEndDate As String = Me.ddlEndDay.SelectedValue & " " & Me.ddlEndMonth.SelectedValue & " " & Me.ddlEndYear.SelectedValue
        Dim intEndDate As Integer = Core.data_select_value("SELECT count(pk_date) FROM uml_cms.dbo.dim_dates WHERE pk_date = '" & dtmEndDate & "'")

        If intStartDate = 0 Then intError += 1
        If intEndDate = 0 Then intError += 1

        If CDate(dtmEndDate) < CDate(dtmStartDate) Then intError += 1
        If CDate(dtmStartDate) > Now() Then intError += 1
        If CDate(dtmEndDate) > Now() Then intError += 1



        Dim s As String = ListAssignedMeters()

        If intError = 0 Then 'ok
            If s.Length > 0 Then
                Me.lnkNewLink.NavigateUrl = "report.aspx?meterid=" + CStr(s) + "&channelid=" + CStr(1) + "&startdate=" + CStr(dtmStartDate) + "&enddate=" + CStr(dtmEndDate) + "&graphtype=10&?keepThis=true&TB_iframe=true&height=600&width=850"
                Me.lnkNewLink.Text = "Generate Report"
                Me.lnkNewLink.ImageUrl = "../../../assets/Schneider/buttons-myreports-general-generatereport.png"
                Me.lnkNewLink.CssClass = "thickbox"
                'Me.lnkbtnDownload.Visible = True
            Else
                Me.lnkNewLink.NavigateUrl = ""
                Me.lnkNewLink.Text = "* please select a meter"
                Me.lnkNewLink.ImageUrl = "../../../assets/Schneider/buttons-myreports-general-generatereport-disabled.png"
                Me.lnkNewLink.CssClass = ""
                'Me.lnkbtnDownload.Visible = False
            End If
        Else
            Me.lnkNewLink.NavigateUrl = ""
            Me.lnkNewLink.Text = "* please select a meter"
            Me.lnkNewLink.ImageUrl = "../../../assets/Schneider/buttons-myreports-general-generatereport-disabled.png"
            Me.lnkNewLink.CssClass = ""
            'Me.lnkbtnDownload.Visible = False

        End If



    End Sub

    Private Sub MeterPoints_Show(ByVal Type As String)
        Try

            'MesgBox(MySession.UserID)
            'MesgBox(Request.QueryString("method"))
            'MesgBox(Request.QueryString("custid"))
            'MesgBox(Type)
            'MesgBox(ListAssignedMeters())
            'MesgBox(Me.txtCustomFilter.Text.ToString)
            'MesgBox(Me.rblHalfHourMeterOnly.SelectedItem.Value.ToString)
            Dim strsql As String = " EXEC [portal.utilitymasters.co.uk].dbo.usp_MeterPoints_Display " & MySession.UserID & ", '" & Request.QueryString("method") & "', " & Request.QueryString("custid") & ",'" & Type & "', '" & ListAssignedMeters() & "', '" & Me.txtCustomFilter.Text.ToString & "', " & Me.rblHalfHourMeterOnly.SelectedItem.Value.ToString
            'MesgBox(strsql)
            With Me.lbAvailableMeters
                .DataSource = Core.data_select(strsql)
                .DataValueField = "ID"
                .DataTextField = "varNiceName"
                .DataBind()
            End With

        Catch ex As Exception

        End Try

    End Sub
    Private Function ListAssignedMeters() As String
        Dim s As String = ""

        If Me.lbAssignedMeters.Items.Count > 0 Then
            For Each x As ListItem In lbAssignedMeters.Items
                s &= "," & x.Value
            Next
            If s.Length > 0 Then
                s = s.Remove(0, 1)
            End If
        End If

        Return s

    End Function
    
    Private Sub CountAssignedMeters()
        Dim i As Integer = 0
        For Each x As ListItem In lbAssignedMeters.Items
            i += 1
        Next

        If i > 4 Then
            Me.lbAvailableMeters.Enabled = False
        Else
            Me.lbAvailableMeters.Enabled = True
        End If

    End Sub

    Protected Sub rblMeterFilter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rblMeterFilter.SelectedIndexChanged
        If Me.rblMeterFilter.SelectedItem.Value = "Custom" Then
            Me.txtCustomFilter.Text = Nothing
            Me.secFilter.Visible = True
        Else
            Me.txtCustomFilter.Text = Nothing
            Me.secFilter.Visible = False
        End If


        MeterPoints_Show(Me.rblMeterFilter.SelectedItem.Value)
    End Sub

    Protected Sub lbAvailableMeters_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbAvailableMeters.SelectedIndexChanged


        Dim iAssigned As Integer = 0
        For Each xAssigned As ListItem In lbAssignedMeters.Items
            iAssigned += 1
        Next
        If iAssigned < 5 Then
            Me.lblAssignedMeterError.Text = ""

            Dim i As Integer = 0
            For Each x As ListItem In lbAvailableMeters.Items
                If x.Selected Then
                    i += 1
                End If
            Next

            If i > 0 Then
repeat:
                If lbAvailableMeters.Items.Count > 0 Then

                    Dim li As ListItem

                    For Each li In lbAvailableMeters.Items
                        If li.Selected = True Then
                            Dim iAssigned2 As Integer = 0
                            For Each xAssigned2 As ListItem In lbAssignedMeters.Items
                                iAssigned2 += 1
                            Next
                            If iAssigned2 = 5 Then
                                GoTo meterlimitexceeded
                            End If
                            lbAssignedMeters.Items.Add(li)
                            lbAvailableMeters.Items.Remove(li)
                            GoTo repeat
                        End If

                    Next

                End If
            End If

            Me.lbAssignedMeters.SelectedIndex = -1
            Me.lbAvailableMeters.SelectedIndex = -1

        Else
            Me.lblAssignedMeterError.Text = "you are currently limited to 5 meter points."
        End If


        MeterPoints_Show(Me.rblMeterFilter.SelectedItem.Value)

meterlimitexceeded:
        Me.lblAssignedMeterError.Text = "you are currently limited to 5 meter points."
        CountAssignedMeters()

        ThickBox()

    End Sub

    Protected Sub lbAssignedMeters_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbAssignedMeters.SelectedIndexChanged

        Me.lblAssignedMeterError.Text = ""

        Dim i As Integer = 0
        For Each x As ListItem In lbAssignedMeters.Items
            If x.Selected Then
                i += 1
            End If
        Next


        If i > 0 Then
repeat:
            If lbAssignedMeters.Items.Count > 0 Then

                Dim li As ListItem

                For Each li In lbAssignedMeters.Items
                    If li.Selected = True Then
                        lbAvailableMeters.Items.Add(li)
                        lbAssignedMeters.Items.Remove(li)
                        GoTo repeat
                    End If

                Next

            End If
        End If

        Me.lbAssignedMeters.SelectedIndex = -1
        Me.lbAvailableMeters.SelectedIndex = -1

        MeterPoints_Show(Me.rblMeterFilter.SelectedItem.Value)
        CountAssignedMeters()
        ThickBox()
    End Sub

    Private Sub PopulateDateDropDowns()

        With Me.ddlStartDay
            .Items.Add("01")
            .Items.Add("02")
            .Items.Add("03")
            .Items.Add("04")
            .Items.Add("05")
            .Items.Add("06")
            .Items.Add("07")
            .Items.Add("08")
            .Items.Add("09")
            .Items.Add("10")
            .Items.Add("11")
            .Items.Add("12")
            .Items.Add("13")
            .Items.Add("14")
            .Items.Add("15")
            .Items.Add("16")
            .Items.Add("17")
            .Items.Add("18")
            .Items.Add("19")
            .Items.Add("20")
            .Items.Add("21")
            .Items.Add("22")
            .Items.Add("23")
            .Items.Add("24")
            .Items.Add("25")
            .Items.Add("26")
            .Items.Add("27")
            .Items.Add("28")
            .Items.Add("29")
            .Items.Add("30")
            .Items.Add("31")
        End With
        With Me.ddlStartMonth
            .Items.Add("January")
            .Items.Add("February")
            .Items.Add("March")
            .Items.Add("April")
            .Items.Add("May")
            .Items.Add("June")
            .Items.Add("July")
            .Items.Add("August")
            .Items.Add("September")
            .Items.Add("October")
            .Items.Add("November")
            .Items.Add("December")
        End With
        With Me.ddlStartYear
            .Items.Clear()
            .DataSource = Core.data_select("SELECT DISTINCT DATEPART(YEAR, YEAR) AS Year FROM uml_cms.dbo.DIM_dates WHERE pk_date < GETDATE() AND pk_date > '01/01/2008' ORDER BY DATEPART(YEAR, YEAR) DESC")
            .DataValueField = "Year"
            .DataTextField = "Year"
            .DataBind()
        End With

        With Me.ddlEndDay
            .Items.Add("01")
            .Items.Add("02")
            .Items.Add("03")
            .Items.Add("04")
            .Items.Add("05")
            .Items.Add("06")
            .Items.Add("07")
            .Items.Add("08")
            .Items.Add("09")
            .Items.Add("10")
            .Items.Add("11")
            .Items.Add("12")
            .Items.Add("13")
            .Items.Add("14")
            .Items.Add("15")
            .Items.Add("16")
            .Items.Add("17")
            .Items.Add("18")
            .Items.Add("19")
            .Items.Add("20")
            .Items.Add("21")
            .Items.Add("22")
            .Items.Add("23")
            .Items.Add("24")
            .Items.Add("25")
            .Items.Add("26")
            .Items.Add("27")
            .Items.Add("28")
            .Items.Add("29")
            .Items.Add("30")
            .Items.Add("31")
        End With
        With Me.ddlEndMonth
            .Items.Add("January")
            .Items.Add("February")
            .Items.Add("March")
            .Items.Add("April")
            .Items.Add("May")
            .Items.Add("June")
            .Items.Add("July")
            .Items.Add("August")
            .Items.Add("September")
            .Items.Add("October")
            .Items.Add("November")
            .Items.Add("December")
        End With
        With Me.ddlEndYear
            .Items.Clear()
            .DataSource = Core.data_select("SELECT DISTINCT DATEPART(YEAR, YEAR) AS Year FROM uml_cms.dbo.DIM_dates WHERE pk_date < GETDATE() AND pk_date > '01/01/2008' ORDER BY DATEPART(YEAR, YEAR) DESC")
            .DataValueField = "Year"
            .DataTextField = "Year"
            .DataBind()
        End With

        'Dim DateNow As Date = DateAdd("m", -1, Now())
        Dim DateNow As Date = Now()

        Dim varDateDay As String = DatePart("d", DateNow)
        Dim varDateDay2 As String = DatePart("d", DateAdd("d", -7, DateNow))

        If varDateDay.Length = 1 Then varDateDay = "0" & varDateDay
        If varDateDay2.Length = 1 Then varDateDay2 = "0" & varDateDay2

        Me.ddlEndDay.SelectedValue = varDateDay
        Me.ddlEndMonth.SelectedValue = MonthName(DatePart("m", DateNow))
        Me.ddlEndYear.SelectedValue = DatePart("yyyy", DateNow)


        Me.ddlStartDay.SelectedValue = varDateDay2
        Me.ddlStartMonth.SelectedValue = MonthName(DatePart("m", DateAdd("d", -7, DateNow)))
        Me.ddlStartYear.SelectedValue = DatePart("yyyy", DateAdd("d", -7, DateNow))

    End Sub

    Protected Sub btnFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFilter.Click
        Me.txtCustomFilter.Text = fnNoSpecialCharacters(Me.txtCustomFilter.Text.ToString)
        MeterPoints_Show(Me.rblMeterFilter.SelectedItem.Value)

    End Sub

    Public Function fnNoSpecialCharacters(ByVal strTest As String) As String

        Dim sb As New System.Text.StringBuilder

        For Each ch As Char In strTest
            If Char.IsLetterOrDigit(ch) OrElse ch = " "c Then
                sb.Append(ch)
            End If
        Next

        strTest = sb.ToString

        Return strTest
    End Function

    Protected Sub rblHalfHourMeterOnly_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rblHalfHourMeterOnly.SelectedIndexChanged

        MeterPoints_Show(Me.rblMeterFilter.SelectedItem.Value)
    End Sub
End Class
