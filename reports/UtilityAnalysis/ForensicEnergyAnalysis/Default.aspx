<%@ Page Language="VB" MasterPageFile="~/landing.master" AutoEventWireup="false" CodeFile="Default.aspx.vb" Inherits="reports_onlinegraph_Default" title="UTILISYS - Forensic Energy Analysis" %>

<%@ Register Assembly="DundasWebChart" Namespace="Dundas.Charting.WebControl" TagPrefix="DCWC" %>
<asp:Content ID="Content1" ContentPlaceHolderID="content_right" Runat="Server">

        <script type="text/javascript" src="../../../ThickBox/jquery-1.3.2.min.js"></script>
        <script type="text/javascript" src="../../../ThickBox/thickbox.js"></script>
        <link rel="stylesheet" href="../../../ThickBox/thickbox.css" type="text/css" media="screen" />   
                
<form id="Form1" runat="server">

    <div class="content_back general">
    <asp:Panel id="pnlDefaultPage" style="Z-INDEX: 101; LEFT: 730px; POSITION: absolute; TOP: 86px" runat="server">
         <asp:Button ID="btnDefaultPage" runat="server" Text="" Width="150px" />
    </asp:Panel>

            
            <table cellpadding="0" cellspacing="0">
                <tr valign="middle">
                    <td style="width:100px;" >
                        <asp:Label ID="lblDescription" Text="Description" runat="server" Font-Bold="true"></asp:Label>
                    </td>
                    <td style="width:200px;" align="left">
                        <asp:Label ID="lblName" Text="Name" runat="server"></asp:Label>
                    </td>
                    <td style="width:150px;">
                        
                    </td>
                    <td style="width:150px;">
                    </td>
                </tr> <!-- Company selection -->
                
                <tr valign="middle">
                    <td >
                        <b>Report</b>
                    </td>
                    <td align="left">
                        Forensic Energy Analysis
                    </td>
                    <td>
                        
                    </td>
                </tr> <!-- Report Name -->
                
                <tr>
                    
                    <td colspan="3">
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td style="width:120px;" valign="top">
                                    <asp:label ID="lblFilterNameAvailable" runat="server" Text="Available Meters" Width="120px" Font-Bold="true"></asp:label>
                                </td>
                                <td style="width:400px;" valign="middle">
                                    <asp:RadioButtonList Width="450px" ID="rblMeterFilter" runat="server" RepeatLayout="table" RepeatDirection="Horizontal" AutoPostBack="true">
                                        <asp:ListItem Value="All" Text="Show All" Selected="True"></asp:ListItem>
                                        <asp:ListItem Value="Main" Text="Main Meters"></asp:ListItem>
                                        <asp:ListItem Value="Sub" Text="Sub Meters"></asp:ListItem>
                                        <asp:ListItem Value="Virtual" Text="Virtual Meters"></asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>
                            </tr>
                            
                            <tr>
                                <td colspan="2">
                                    <asp:ListBox ID="lbAvailableMeters" runat="server" style="width:630px; height:200px;" SelectionMode="Multiple" AutoPostBack="true" BackColor="#FFFFC0" >
                                    </asp:ListBox>
                                </td>
                            </tr>
                            
                            <tr>
                                <td>
                                    <asp:label ID="lblFilterNameSelected" runat="server" Text="Available Meters" Width="120px" Font-Bold="true"></asp:label>
                                </td>
                                <td>
                                    <asp:Label ID="lblAssignedMeterError" Text="" runat="server" ForeColor="Red"></asp:Label>
                                </td>
                            </tr>
                            
                            <tr>
                                <td colspan="2">
                                    <asp:ListBox ID="lbAssignedMeters" runat="server" style="width:630px; height:100px;" SelectionMode="Multiple" AutoPostBack="true" BackColor="#FFFFC0" >
                                    </asp:ListBox>                                    
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr> <!-- Meter Selection Table -->
                
                <tr>
                    <td colspan="3" style="text-align:center;">
                        <asp:HyperLink  ID="lnkNewLink" runat="server" CssClass="thickbox"></asp:HyperLink>
                    </td>
                </tr> <!-- Run button -->  
                                  
            </table>
        </div>

</form>
</asp:Content>

