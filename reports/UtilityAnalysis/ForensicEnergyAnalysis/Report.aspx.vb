﻿Imports Dundas.Charting.WebControl
Imports Dundas.Charting.WebControl.Utilities

Imports Dundas.Charting.WebControl.ChartTypes
Imports Dundas.Extensions

Imports System.Data.SqlClient
Imports System.Data
Imports System.Drawing
Imports System.Windows

Partial Class reports_onlineutilisys_comparemeterpoints_Report

    Inherits System.Web.UI.Page

    Public conn As String = Core.ConnectionString()

    Shared cDefault As Color = System.Drawing.ColorTranslator.FromHtml("#9ac83c")

    Shared cOne As Color = System.Drawing.ColorTranslator.FromHtml("#095783") ' Dark Green
    Shared cTwo As Color = System.Drawing.ColorTranslator.FromHtml("#2D04A2") ' Blue / Purple
    Shared cThree As Color = System.Drawing.ColorTranslator.FromHtml("#D5360D") ' Red
    Shared cFour As Color = System.Drawing.ColorTranslator.FromHtml("#DEA317") ' Light Orange
    Shared cFive As Color = System.Drawing.ColorTranslator.FromHtml("#44B934") ' Green
    Shared cSix As Color = System.Drawing.ColorTranslator.FromHtml("#0694d8") ' Light Blue
    Shared cSeven As Color = System.Drawing.ColorTranslator.FromHtml("#b760ee") ' Lilac
    Shared cEight As Color = System.Drawing.ColorTranslator.FromHtml("#7a5a10") ' Brown

    Shared colorSet() As Color = {cOne, cTwo, cThree, cFour, cFive, cSix, cSeven, cEight}

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Session.LCID = 2057
        If Not Page.IsPostBack = True Then

            Me.txtMeterID.Text = Request.QueryString("meterid")

            GenerateGraph2(Me.txtMeterID.Text)

            SetupTimescales()
            SetupFrequency()
            SetupChartType()
        End If

        'SetupButtons()

    End Sub

    Protected Overloads Sub MakeGroupLabels(ByVal chart As Chart, ByVal series As Series, ByVal ParamArray fields() As String)
        ' This is how many rows of labels we need to make.
        Dim numLabels As Integer = fields.Length

        Dim chartArea As ChartArea = chart.ChartAreas(series.ChartArea)
        Dim group As String() = New String(numLabels - 1) {}
        Dim labels As CustomLabel() = New CustomLabel(numLabels - 1) {}

        ' Initialize each row's current label.
        For i As Integer = 0 To numLabels - 1
            group(i) = ""
        Next

        ' For x-value indexed series, count from 1.
        Dim xValue As Integer = 1

        ' Set the PointWidth default value if it's null
        If series("PointWidth") Is Nothing Then
            series("PointWidth") = "0.8"
        End If

        ' Iterate over each point in the series.  Check the group fields and 
        ' update the ending value for each label or start a new label.
        For Each dataPoint As DataPoint In series.Points
            For i As Integer = 0 To numLabels - 1
                If group(i) = dataPoint(fields(i)) Then
                    ' Still in the same group.  Update the custom label's end position.
                    labels(i).[To] = xValue + Double.Parse(series("PointWidth")) / 2
                Else
                    ' Create a new label here with the group text.
                    group(i) = dataPoint(fields(i))
                    labels(i) = New CustomLabel()
                    labels(i).From = xValue - Double.Parse(series("PointWidth")) / 2
                    labels(i).[To] = xValue + Double.Parse(series("PointWidth")) / 2
                    labels(i).RowIndex = (i + 1)
                    labels(i).Text = group(i)
                    labels(i).LabelMark = LabelMark.LineSideMark
                    chartArea.AxisX.CustomLabels.Add(labels(i))
                End If
            Next
            xValue += 1
        Next
    End Sub

    Private Sub CleanChart()

        Chart1.Serializer.Reset()

        With Me.Chart1

            Dim series1 As Series
            For Each series1 In .Series
                .Annotations.Clear()
            Next

            .Visible = True
            .Series.Clear()
            .Titles.Clear()
            .Legends.Clear()
            .Legends.Add("Default")

            ' legend stuff
            .Legends(0).Docking = LegendDocking.Bottom
            .Legends(0).Alignment = StringAlignment.Far
            .Legends(0).Reversed = AutoBool.False
            .Legends(0).Position.Auto = True
            .Legends(0).CellColumns.Add(New LegendCellColumn("", LegendCellColumnType.SeriesSymbol, ""))
            .Legends(0).CellColumns.Add(New LegendCellColumn("", LegendCellColumnType.Text, "#LEGENDTEXT", Drawing.ContentAlignment.MiddleLeft))

            ' Zooming
            Dim chartarea1 As ChartArea
            For Each chartarea1 In .ChartAreas
                chartarea1.CursorX.UserEnabled = True
            Next

            ' Setup Frame
            .BorderColor = System.Drawing.ColorTranslator.FromHtml("#009530") ' Corp Green
            .BorderStyle = ChartDashStyle.Solid
            .BorderWidth = 2
            .BorderSkin.SkinStyle = BorderSkinStyle.Emboss
            'Setup Toolbar
            .UI.Toolbar.Enabled = True
            .UI.Toolbar.Docking = ToolbarDocking.Top
            .UI.Toolbar.Placement = ToolbarPlacement.InsideChart
            .UI.Toolbar.BorderSkin.SkinStyle = BorderSkinStyle.Emboss
            .UI.Toolbar.BorderSkin.FrameBackColor = System.Drawing.ColorTranslator.FromHtml("#009530") ' Corp Green

            Dim commands As CommandCollection = Chart1.UI.Commands

            Dim cmdProperties As Command = commands.FindCommand(ChartCommandType.Properties)
            Dim cmdPalette As Command = commands.FindCommand(ChartCommandType.SelectPaletteGroup)
            Dim cmdChartType As Command = commands.FindCommand(ChartCommandType.SelectChartGroup)
            Dim cmdToggle3D As Command = commands.FindCommand(ChartCommandType.Toggle3D)

            cmdProperties.Visible = False
            cmdPalette.Visible = False
            cmdChartType.Visible = False
            cmdToggle3D.Visible = False

            Me.Chart1.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFFFC0") ' Corp Green
        End With

        Chart1.ChartAreas.Add("Default")
        'Chart1.Series.Add(0)
        Me.Chart1.Width = "800"
        Me.Chart1.Height = "450"

        Me.Chart1.ChartAreas(0).CursorX.UserEnabled = True

    End Sub
    Private Sub GenerateGraph2(ByVal varMeterID As String)

        CleanChart()
        'SetupButtons()

        Dim strSite As String = Core.data_select_value("EXEC uml_extdata.dbo.usp_Portal_UtilisysClassic_ReturnSiteName '" & Request.QueryString("meterid") & "'")
        Dim strMeter As String = Core.data_select_value("EXEC UML_EXTData.dbo.usp_Portal_UtilisysClassic_ReturnMeterName '" & Request.QueryString("meterid") & "'") 'varMeterID


        Dim strSQL As String = "EXEC uml_extdata.dbo.usp_Portal_MyReports_ForensicEnergyAnalysis '" & varMeterID & "'"

        With Chart1

            .Visible = True
            .Series.Clear()
            .Titles.Clear()

            .DataSource = Core.data_select(strSQL)
            .DataBind()
            .Series.Add("Consumption")
            .Series(0).ValueMemberX = "dtmDate"
            .Series(0).ValueMembersY = "fltTotal"
            .Series(0).Type = Dundas.Charting.WebControl.SeriesChartType.FastLine

            .Series.Add("Baseload")
            .Series(1).ValueMemberX = "dtmDate"
            .Series(1).ValueMembersY = "fltMinimum"
            .Series(1).Type = Dundas.Charting.WebControl.SeriesChartType.Line

            .Series.Add("Rolling Avg")
            .Series(2).ValueMemberX = "dtmDate"
            .Series(2).ValueMembersY = "fltAverage"
            .Series(2).Type = Dundas.Charting.WebControl.SeriesChartType.Line

            .ChartAreas(0).AxisY.IntervalAutoMode = IntervalAutoMode.VariableCount
            .ChartAreas(0).AxisY.MajorGrid.LineStyle = ChartDashStyle.Dot
            .ChartAreas(0).AxisX.MajorGrid.LineStyle = ChartDashStyle.Dot

            .Legends(0).Enabled = True
            .Legends(0).CellColumns.Clear()
            .Legends(0).CellColumns.Add(New LegendCellColumn("Sym", LegendCellColumnType.SeriesSymbol, ""))
            .Legends(0).CellColumns.Add(New LegendCellColumn("Name", LegendCellColumnType.Text, "#LEGENDTEXT", Drawing.ContentAlignment.MiddleLeft))

            .Titles.Add("")
            .ChartAreas(0).AxisY.Title = "Consumption (kWh)"
            .ChartAreas(0).AxisY.LabelStyle.Format = "#,###"
            .ChartAreas(0).AxisX.Title = "Date"
            .ChartAreas(0).AxisX.Margin = False

            .PaletteCustomColors = colorSet
            .ChartAreas(0).BackColor = Color.Silver

            .BorderColor = Color.Black
            .BorderStyle = ChartDashStyle.Solid
            .BorderWidth = 2
            .BorderSkin.SkinStyle = BorderSkinStyle.Emboss

            .UI.Toolbar.Enabled = True
            .UI.Toolbar.Docking = ToolbarDocking.Top
            .UI.Toolbar.Placement = ToolbarPlacement.InsideChart
            .UI.Toolbar.BorderSkin.SkinStyle = BorderSkinStyle.Emboss
            .UI.Toolbar.BorderSkin.FrameBackColor = Color.Black
            .Titles.Add(1).Text = "Forensic Energy Analysis for " & strSite

        End With

        Dim series As Series
        For Each series In Me.Chart1.Series
            series.BorderWidth = 2
            series.XValueType = ChartValueTypes.DateTime
        Next

        Dim commands As CommandCollection = Chart1.UI.Commands

        Dim cmdProperties As Command = commands.FindCommand(ChartCommandType.Properties)
        Dim cmdPalette As Command = commands.FindCommand(ChartCommandType.SelectPaletteGroup)
        Dim cmdChartType As Command = commands.FindCommand(ChartCommandType.SelectChartGroup)
        Dim cmdToggle3D As Command = commands.FindCommand(ChartCommandType.Toggle3D)

        cmdProperties.Visible = False
        cmdPalette.Visible = False
        cmdChartType.Visible = False
        cmdToggle3D.Visible = False


    End Sub

    Public Shared Function FromExcelSerialDate(ByVal SerialDate As Integer) As DateTime
        If SerialDate > 59 Then SerialDate -= 1 ''// Excel/Lotus 2/29/1900 bug    
        Return New DateTime(1899, 12, 31).AddDays(SerialDate)
    End Function


    Private Sub SetupFrequency()

        Me.ddlFrequency.Items.Clear()

        Me.ddlFrequency.Items.Add("Monthly")
        Me.ddlFrequency.Items.Add("Week")
        Me.ddlFrequency.Items.Add("Day")
        Me.ddlFrequency.Items.Add("Half Hour")

        Me.rblFrequency.Items.Add("Month")
        Me.rblFrequency.Items.Add("Week")
        Me.rblFrequency.Items.Add("Day")
        Me.rblFrequency.Items.Add("Half Hour")

        Me.ddlFrequency.SelectedItem.Value = Me.txtFrequency.Text
        Me.rblFrequency.SelectedValue = "Half Hour"

    End Sub
    Private Sub SetupTimescales()

        Me.ddlTimescales.Items.Clear()
        Me.ddlTimescales.Items.Add("Last 1 Week")
        Me.ddlTimescales.Items.Add("Last 1 Month")
        Me.ddlTimescales.Items.Add("Last 6 Months")
        Me.ddlTimescales.Items.Add("Last 1 Year")

        Me.rblTimescales.Items.Clear()
        Me.rblTimescales.Items.Add("Last 1 Week")
        Me.rblTimescales.Items.Add("Last 1 Month")
        Me.rblTimescales.Items.Add("Last 6 Months")
        Me.rblTimescales.Items.Add("Last 1 Year")

    End Sub
    Private Sub SetupChartType()

        Me.ddlChartType.Items.Clear()
        Me.ddlChartType.Items.Add("Line")
        Me.ddlChartType.Items.Add("Column")

        Me.rblChartType.Items.Clear()
        Me.rblChartType.Items.Add("Line")
        Me.rblChartType.Items.Add("Column")
        Me.rblChartType.SelectedValue = "Line"

    End Sub

End Class