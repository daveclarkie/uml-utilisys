﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Report.aspx.vb" Inherits="reports_onlineutilisys_comparemeterpoints_Report" %>

<%@ Register Assembly="DundasWebChart" Namespace="Dundas.Charting.WebControl" TagPrefix="DCWC" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Compare Meter Points</title>
    <link rel="stylesheet" href="../../../modalbox/modalreport.css" type="text/css" media="screen" />   

</head>

<body style="background-color:#FFFFFF;">

<form id="form1" runat="server">

    <asp:Panel ID="pnlReport" width="800" runat="server">

        
        <table border="0" cellpadding="0" cellspacing="0" >
            <tr>
                <td style="width:150px;">
                
                </td>
                <td>
                    <DCWC:Chart ID="Chart1" runat="server" >
                        <Legends>
                            <DCWC:Legend Name="Default">
                            </DCWC:Legend>
                        </Legends>
                    </DCWC:Chart>
                </td>
            </tr>
        </table>
        <%--Charts--%>
        <table width="800px" style="background-color:#FFFFC0; border-color:Black; text-align:justify;">
            <tr>
                <td style="font-weight:bold; width:150px;">
                    Frequency
                </td>
                <td style="font-weight:bold; width:150px;">
                    Timescale
                </td>
                <td style="font-weight:bold; width:150px;">
                    Type
                </td>
                <td style="font-weight:bold; width:150px;">
                    Events
                </td>
                <td style="width:300px;">
                    
                </td>
            </tr>
            <tr>
                <td style="text-align:left; vertical-align:top;">
                    <asp:DropDownList ID="ddlFrequency" runat="server" AutoPostBack="true" Width="130" Visible="false"></asp:DropDownList>
                    <asp:RadioButtonList ID="rblFrequency" runat="server" AutoPostBack="true" RepeatDirection="Vertical" RepeatColumns="1" RepeatLayout="Table" TextAlign="Right"></asp:RadioButtonList>
                </td>
                <td style="text-align:left; vertical-align:top;">
                    <asp:DropDownList ID="ddlTimescales" runat="server" AutoPostBack="true" Width="130" Visible="false"></asp:DropDownList>
                    <asp:RadioButtonList ID="rblTimescales" runat="server" AutoPostBack="true" RepeatDirection="Vertical" RepeatColumns="1" RepeatLayout="Table" TextAlign="Right"></asp:RadioButtonList>
                </td>
                <td style="text-align:left; vertical-align:top;">
                    <asp:DropDownList ID="ddlChartType" runat="server" AutoPostBack="true" Width="130" Visible="false"></asp:DropDownList>
                    <asp:RadioButtonList ID="rblChartType" runat="server" AutoPostBack="true" RepeatDirection="Vertical" RepeatColumns="1" RepeatLayout="Table" TextAlign="Right"></asp:RadioButtonList>
                </td>
                <td style="text-align:left; vertical-align:top;">
                    <asp:CheckBox ID="chkEvents" runat="server" AutoPostBack="true" />
                </td>
            </tr>
            
            <tr>
                <td colspan="4">
                    <asp:TextBox ID="txtMeterID" runat="server" Text="" Visible="false" Width="10"></asp:TextBox>
                    <asp:TextBox ID="txtFrequency" runat="server" Text="" Visible="false" Width="10"></asp:TextBox>
                    <asp:TextBox ID="txtStart" runat="server" Text="" Visible="false" Width="10"></asp:TextBox>
                    <asp:TextBox ID="txtEnd" runat="server" Text="" Visible="false" Width="10"></asp:TextBox>
                    <asp:TextBox ID="txtGraphType" runat="server" Text="" Visible="false" Width="10"></asp:TextBox>
                </td>
            </tr>
            
        </table>

    </asp:Panel>

</form>

</body>
</html>
