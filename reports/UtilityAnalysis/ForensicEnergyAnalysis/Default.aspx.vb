Imports Dundas.Charting.WebControl
Imports System.Drawing
Imports System
Imports System.IO

Partial Class reports_onlinegraph_Default

    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            If Not IIf(Core.data_select_value("select bitAllowMyReports from tblUsers where intUserPK = " & MySession.UserID) = "True", True, False) Then
                Response.Redirect(Core.data_select_value("select RawHomeURL from vwUsers where ID = " & MySession.UserID), True)
            End If

            If Request.QueryString("custid") Is Nothing Then
                Dim strSQL As String = "EXEC spPortal_TREEVIEW_TopLevel " & MySession.UserID
                Dim myConnection As New OleDb.OleDbConnection(Core.CurrentConnectionString)

                Dim myCommand As New OleDb.OleDbCommand(strSQL, myConnection)
                myCommand.Connection.Open()
                myCommand.CommandTimeout = 180
                Dim Reader As OleDb.OleDbDataReader = myCommand.ExecuteReader(CommandBehavior.CloseConnection)

                If Reader.HasRows Then
                    Reader.Read()
                    If Reader("Type") = "Company" Then
                        Response.Redirect("default.aspx?custid=" & Reader("id") & "&method=Customer", True)
                    Else
                        Response.Redirect("default.aspx?custid=" & Reader("id") & "&method=" & Reader("Type"), True)
                    End If
                    Reader.Close()
                End If
            End If

            If Not Page.IsPostBack = True Then

                MeterPoints_Show("All")

                Dim description As HtmlMeta = New HtmlMeta()
                description.Name = "description"
                description.Content = "Utility Masters - Forensic Energy Analysis"
                Dim keywords As HtmlMeta = New HtmlMeta()
                keywords.Name = "keywords"
                keywords.Content = "Forensic Energy Analysis"
                Page.Header.Controls.Add(description)
                Page.Header.Controls.Add(keywords)

                MeterPoints_Show(Me.rblMeterFilter.SelectedItem.Value)

                ThickBox()

            End If


            Me.lblDescription.Text = Request.QueryString("method").ToString
            Me.lblName.Text = Core.data_select_value("EXEC spSites_MAP_CurentSelection " & MySession.UserID & ", '" & Request.QueryString("method") & "', " & Request.QueryString("custid"))

            CountAssignedMeters()

            ThickBox()


            Dim sMyScreenPath As String
            Dim strCustomPage As String = Core.data_select_value("SELECT varDefaultPage FROM tblUsers where intUserPK = " & MySession.UserID)
            sMyScreenPath = System.Web.HttpContext.Current.Request.Url.AbsolutePath

            If strCustomPage = sMyScreenPath Then
                Me.btnDefaultPage.Text = "unset default page"
            Else
                Me.btnDefaultPage.Text = "set default page"
            End If

        Catch ex As Exception
            '
        End Try

    End Sub

    Protected Sub btnDefaultPage_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDefaultPage.Click

        Dim sMyScreenPath As String
        Dim strCustomPage As String = Core.data_select_value("SELECT varDefaultPage FROM tblUsers where intUserPK = " & MySession.UserID)
        sMyScreenPath = System.Web.HttpContext.Current.Request.Url.AbsolutePath

        If strCustomPage = sMyScreenPath Then
            Core.data_execute_nonquery("UPDATE tblUsers SET varDefaultPage = null WHERE intUserPK = " & MySession.UserID)
        Else
            Core.data_execute_nonquery("UPDATE tblUsers SET varDefaultPage = '" & sMyScreenPath & "' WHERE intUserPK = " & MySession.UserID)
        End If

        Server.Transfer(sMyScreenPath)

    End Sub

    Public Overloads Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)

        ' Confirms that an HtmlForm control is rendered for the specified ASP.NET server control at run time.

    End Sub
    Public Sub ThickBox()

        Dim intError As Integer = 0

        Dim s As String = ListAssignedMeters()

        If intError = 0 Then 'ok
            If s.Length > 0 Then
                Me.lnkNewLink.NavigateUrl = "report.aspx?meterid=" + CStr(s) + "&channelid=" + CStr(1) + "&frequency=Monthly&graphtype=10&?keepThis=true&TB_iframe=true&height=600&width=850"
                Me.lnkNewLink.Text = "Generate Report"
                Me.lnkNewLink.ImageUrl = "../../../assets/Schneider/buttons-myreports-general-generatereport.png"
                Me.lnkNewLink.CssClass = "thickbox"
                'Me.lnkbtnDownload.Visible = True
            Else
                Me.lnkNewLink.NavigateUrl = ""
                Me.lnkNewLink.Text = "* please select a meter"
                Me.lnkNewLink.ImageUrl = "../../../assets/Schneider/buttons-myreports-general-generatereport-disabled.png"
                Me.lnkNewLink.CssClass = ""
                'Me.lnkbtnDownload.Visible = False
            End If
        Else
            Me.lnkNewLink.NavigateUrl = ""
            Me.lnkNewLink.Text = "* please select a meter"
            Me.lnkNewLink.ImageUrl = "../../../assets/Schneider/buttons-myreports-general-generatereport-disabled.png"
            Me.lnkNewLink.CssClass = ""
            'Me.lnkbtnDownload.Visible = False

        End If



    End Sub

    Private Sub MeterPoints_Show(ByVal Type As String)

        With Me.lbAvailableMeters
            .DataSource = Core.data_select(" EXEC uml_extdata.dbo.usp_portal_MeterPoints_Display " & MySession.UserID & ", '" & Request.QueryString("method") & "', " & Request.QueryString("custid") & ",'" & Type & "', '" & ListAssignedMeters() & "'")
            .DataValueField = "MPAN"
            .DataTextField = "MPANSiteName"
            .DataBind()
        End With


    End Sub
    Private Function ListAssignedMeters() As String
        Dim s As String = ""

        If Me.lbAssignedMeters.Items.Count > 0 Then
            For Each x As ListItem In lbAssignedMeters.Items
                s &= "," & x.Value
            Next
            If s.Length > 0 Then
                s = s.Remove(0, 1)
            End If
        End If

        Return s

    End Function
    
    Private Sub CountAssignedMeters()
        Dim i As Integer = 0
        For Each x As ListItem In lbAssignedMeters.Items
            i += 1
        Next

        If i > 4 Then
            Me.lbAvailableMeters.Enabled = False
        Else
            Me.lbAvailableMeters.Enabled = True
        End If

    End Sub

    Protected Sub rblMeterFilter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rblMeterFilter.SelectedIndexChanged
        MeterPoints_Show(Me.rblMeterFilter.SelectedItem.Value)
    End Sub

    Protected Sub lbAvailableMeters_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbAvailableMeters.SelectedIndexChanged


        Dim iAssigned As Integer = 0
        For Each xAssigned As ListItem In lbAssignedMeters.Items
            iAssigned += 1
        Next
        If iAssigned < 5 Then
            Me.lblAssignedMeterError.Text = ""

            Dim i As Integer = 0
            For Each x As ListItem In lbAvailableMeters.Items
                If x.Selected Then
                    i += 1
                End If
            Next

            If i > 0 Then
repeat:
                If lbAvailableMeters.Items.Count > 0 Then

                    Dim li As ListItem

                    For Each li In lbAvailableMeters.Items
                        If li.Selected = True Then
                            Dim iAssigned2 As Integer = 0
                            For Each xAssigned2 As ListItem In lbAssignedMeters.Items
                                iAssigned2 += 1
                            Next
                            If iAssigned2 = 1 Then
                                GoTo meterlimitexceeded
                            End If
                            lbAssignedMeters.Items.Add(li)
                            lbAvailableMeters.Items.Remove(li)
                            GoTo repeat
                        End If

                    Next

                End If
            End If

            Me.lbAssignedMeters.SelectedIndex = -1
            Me.lbAvailableMeters.SelectedIndex = -1

        Else
            Me.lblAssignedMeterError.Text = "you are currently limited to 1 meter point."
        End If


        MeterPoints_Show(Me.rblMeterFilter.SelectedItem.Value)

meterlimitexceeded:
        Me.lblAssignedMeterError.Text = "you are currently limited to 1 meter point."
        CountAssignedMeters()

        ThickBox()

    End Sub

    Protected Sub lbAssignedMeters_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbAssignedMeters.SelectedIndexChanged

        Me.lblAssignedMeterError.Text = ""

        Dim i As Integer = 0
        For Each x As ListItem In lbAssignedMeters.Items
            If x.Selected Then
                i += 1
            End If
        Next


        If i > 0 Then
repeat:
            If lbAssignedMeters.Items.Count > 0 Then

                Dim li As ListItem

                For Each li In lbAssignedMeters.Items
                    If li.Selected = True Then
                        lbAvailableMeters.Items.Add(li)
                        lbAssignedMeters.Items.Remove(li)
                        GoTo repeat
                    End If

                Next

            End If
        End If

        Me.lbAssignedMeters.SelectedIndex = -1
        Me.lbAvailableMeters.SelectedIndex = -1

        MeterPoints_Show(Me.rblMeterFilter.SelectedItem.Value)
        CountAssignedMeters()
        ThickBox()
    End Sub


End Class
