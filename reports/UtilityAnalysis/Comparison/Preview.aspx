﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Preview.aspx.vb" Inherits="reports_UtilisysClassic_Comparison_Preview" %>

<%@ Register Assembly="DundasWebChart" Namespace="Dundas.Charting.WebControl" TagPrefix="DCWC" %>


        <DCWC:Chart ID="Chart1" runat="server" Height="200px" Width="200px">
            <Legends>
            <DCWC:Legend Name="Default"></DCWC:Legend>
            </Legends>
            <Series>
                <DCWC:Series Name="Default">
                </DCWC:Series>
            </Series>
            <ChartAreas>
                <DCWC:ChartArea Name="Default">
                </DCWC:ChartArea>
            </ChartAreas>
        </DCWC:Chart>