﻿Imports Dundas.Charting.WebControl
Imports Dundas.Charting.WebControl.Utilities

Imports System.Data.SqlClient
Imports System.Data
Imports System.Drawing
Imports System.Windows


Partial Class reports_DataAnalysis_Comparison_Report
    Inherits System.Web.UI.Page

    Public conn As String = Core.ConnectionString()

    Shared cGreen As Color = System.Drawing.ColorTranslator.FromHtml("#009530")
    Shared cBlue As Color = System.Drawing.ColorTranslator.FromHtml("#F18308")
    Shared cYellow As Color = System.Drawing.ColorTranslator.FromHtml("#26A7E7")

    Shared colorSet() As Color = {cGreen, cBlue, cYellow, Color.Purple}

    Private Sub AnimateGraph(ByVal Chart As Chart)


        With Chart
            .AnimationTheme = AnimationTheme.GrowingOneByOne
            .ImageType = ChartImageType.Flash
            .AnimationDuration = 5

        End With

        Dim seriesAnimation As New GrowingAnimation()
        seriesAnimation.AnimatedElements.Add(Chart1.Series(0))
        seriesAnimation.StartTime = 2.0
        seriesAnimation.Duration = 3.5
        seriesAnimation.OneByOne = True
        Chart.CustomAnimation.Add(seriesAnimation)

        ' Disable axes lines and major grid lines animation
        Dim axesAnimation As New FadingAnimation()
        axesAnimation.AnimatedElements.Add(Chart1.ChartAreas("Default").AxisX, AxisAnimationElement.MajorGridlines)
        axesAnimation.AnimatedElements.Add(Chart1.ChartAreas("Default").AxisX, AxisAnimationElement.AxisLine)
        axesAnimation.AnimatedElements.Add(Chart1.ChartAreas("Default").AxisY, AxisAnimationElement.MajorGridlines)
        axesAnimation.AnimatedElements.Add(Chart1.ChartAreas("Default").AxisY, AxisAnimationElement.AxisLine)
        axesAnimation.Enabled = False
        Chart.CustomAnimation.Add(axesAnimation)


    End Sub
    Private Sub GenerateGraphs(ByVal varMeterID As String, ByVal dtmDate As DateTime)

        Dim strSite As String = Core.data_select_value("SELECT sitename FROM uvw_DisplayAllMeterPoints WHERE ID = '" & Request.QueryString("meterid") & "'")
        Dim strMeter As String = Core.data_select_value("SELECT varNiceName FROM uvw_DisplayAllMeterPoints WHERE ID = '" & Request.QueryString("meterid") & "'") 'varMeterID
        Dim strPeriod As String = Core.data_select_value("EXEC uml_extdata.dbo.usp_portal_UtilisysClassic_All_DateName 'Monthly', '" & dtmDate & "'")

        Dim strSQL As String = "EXEC uml_extdata.dbo.usp_Portal_MyReports_Comparison_PerMonth '" & varMeterID & "', '" & dtmDate & "'"

        With Chart1

            .Visible = True
            .Series.Clear()
            .Titles.Clear()

            .DataSource = Core.data_select(strSQL)
            .DataBind()
            .Series.Add("Current")
            .Series(0).ValueMemberX = "dtmDate"
            .Series(0).ValueMembersY = "actual"
            .Series(0).Type = Dundas.Charting.WebControl.SeriesChartType.Column

            .Series.Add("Previous")
            .Series(1).ValueMemberX = "dtmDate"
            .Series(1).ValueMembersY = "target"
            .Series(1).Type = Dundas.Charting.WebControl.SeriesChartType.Line

            .ChartAreas(0).AxisY.IntervalAutoMode = IntervalAutoMode.VariableCount
            .ChartAreas(0).AxisY.MajorGrid.LineStyle = ChartDashStyle.Dot
            .ChartAreas(0).AxisX.MajorGrid.LineStyle = ChartDashStyle.Dot

            .Legends(0).Enabled = True
            .Legends(0).CellColumns.Add(New LegendCellColumn("Sym", LegendCellColumnType.SeriesSymbol, ""))
            .Legends(0).CellColumns.Add(New LegendCellColumn("Name", LegendCellColumnType.Text, "#LEGENDTEXT", Drawing.ContentAlignment.MiddleLeft))

            .Titles.Add("")
            .ChartAreas(0).AxisY.Title = "Consumption (units)"
            .ChartAreas(0).AxisY.LabelStyle.Format = "#,###"
            .ChartAreas(0).AxisX.Title = "Date"
            .ChartAreas(0).AxisX.Margin = False

            ' Enable 3D charts
            '.ChartAreas(0).Area3DStyle.Enable3D = True
            '.ChartAreas(0).Area3DStyle.YAngle = 5
            '.ChartAreas(0).Area3DStyle.PointDepth = 50
            '.ChartAreas(0).Area3DStyle.PointGapDepth = 0

            .PaletteCustomColors = colorSet
            .ChartAreas(0).BackColor = Color.Silver

            .BorderColor = Color.Black
            .BorderStyle = ChartDashStyle.Solid
            .BorderWidth = 2
            .BorderSkin.SkinStyle = BorderSkinStyle.Emboss

            .UI.Toolbar.Enabled = True
            .UI.Toolbar.Docking = ToolbarDocking.Top
            .UI.Toolbar.Placement = ToolbarPlacement.InsideChart
            .UI.Toolbar.BorderSkin.SkinStyle = BorderSkinStyle.Emboss
            .UI.Toolbar.BorderSkin.FrameBackColor = Color.Black
            .Titles.Add(1).Text = "Comparison Report: " & strMeter & " - " & strPeriod

            .Series(0).MapAreaAttributes = "onmouseover=""DisplayTooltip('Preview.aspx?meterid=" + Request.QueryString("meterid") + "&channelid=1&date=#VALX');"" onmouseout=""DisplayTooltip('');"""
            .Series(0).Href = "DrillDown.aspx?meterid=" + Request.QueryString("meterid") + "&channelid=1&date=#VALX"

        End With

        Dim commands As CommandCollection = Chart1.UI.Commands

        Dim cmdProperties As Command = commands.FindCommand(ChartCommandType.Properties)
        Dim cmdPalette As Command = commands.FindCommand(ChartCommandType.SelectPaletteGroup)
        Dim cmdChartType As Command = commands.FindCommand(ChartCommandType.SelectChartGroup)
        Dim cmdToggle3D As Command = commands.FindCommand(ChartCommandType.Toggle3D)

        cmdProperties.Visible = False
        cmdPalette.Visible = False
        cmdChartType.Visible = False
        cmdToggle3D.Visible = False

    End Sub
    Private Sub GenerateTable1(ByVal varMeterID As String, ByVal intChannelID As Integer, ByVal varFrequency As String, ByVal dtmDate As DateTime)

        Dim strSQL As String = "EXEC uml_extdata.dbo.usp_portal_UtilisysClassic_Comparison_1 '" & varMeterID & "', " & intChannelID & ", '" & varFrequency & "', '" & dtmDate & "'"
        Using connection As New SqlConnection(conn)
            connection.Open()
            Dim command As New SqlCommand(strSQL, connection)
            command.CommandType = CommandType.Text
            Dim reader As SqlDataReader = command.ExecuteReader()

            With Me.table1

                .InnerHtml = "<table>"

                .InnerHtml &= "<tr>"
                .InnerHtml &= "<td style='color:#FFFFFF;'></td>"
                .InnerHtml &= "<td style='border:solid 1px black;background-color:#082B61;color:#FFFFFF;'>Actual kWh</td>"
                .InnerHtml &= "<td style='border:solid 1px black;background-color:#082B61;color:#FFFFFF;'>Target kWh</td>"
                .InnerHtml &= "<td style='border:solid 1px black;background-color:#082B61;color:#FFFFFF;'>Variance kWh</td>"
                .InnerHtml &= "<td style='border:solid 1px black;background-color:#082B61;color:#FFFFFF;'>Variance %</td>"
                .InnerHtml &= "<td style='border:solid 1px black;background-color:#082B61;color:#FFFFFF;'>Variance £</td>"
                .InnerHtml &= "</tr>"

                While reader.Read

                    .InnerHtml &= "<tr>"
                    .InnerHtml &= "<td style='border:solid 1px black;background-color:#082B61;color:#FFFFFF; width:80px;'>" & reader(0) & "</td>"
                    .InnerHtml &= "<td style='border:solid 1px black;background-color:#FFFFFF; width:80px;'>" & Math.Round(reader(1), 0) & "</td>"
                    .InnerHtml &= "<td style='border:solid 1px black;background-color:#FFFFFF; width:80px;'>" & Math.Round(reader(2), 0) & "</td>"
                    .InnerHtml &= "<td style='border:solid 1px black;background-color:#FFFFFF; width:80px;'>" & Math.Round(reader(5), 0) & "</td>"
                    .InnerHtml &= "<td style='border:solid 1px black;background-color:#FFFFFF; width:80px;'>" & Math.Round(reader(6), 2) & "</td>"
                    .InnerHtml &= "<td style='border:solid 1px black;background-color:#FFFFFF; width:80px;'>" & Math.Round(reader(7), 2) & "</td>"
                    .InnerHtml &= "</tr>"

                End While

                .InnerHtml &= "</table>"

            End With

        End Using

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        Session.LCID = 2057
        Dim varMeterID As String = Request.QueryString("meterid")
        Dim dtmDate As String = Request.QueryString("date")

        'Dim varMeterID As String = "1014570127402"
        'Dim intChannelID As Integer = 1
        'Dim varFrequency As String = "Monthly"
        'Dim dtmDate As String = "01/02/2009"

        GenerateGraphs(varMeterID, dtmDate)
        'GenerateTable1(varMeterID, dtmDate)

        

    End Sub

End Class
