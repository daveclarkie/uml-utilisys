﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="DrillDown.aspx.vb" Inherits="reports_UtilisysClassic_Comparison_DrillDown" %>

<%@ Register Assembly="DundasWebChart" Namespace="Dundas.Charting.WebControl" TagPrefix="DCWC" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Comparison</title>
    <link rel="stylesheet" href="../../../modalbox/modalreport.css" type="text/css" media="screen" />   

</head>
<body>
    <form id="form1" runat="server">
    
        <DCWC:Chart ID="Chart1" runat="server" Width="837px" Height="400px">
            <Legends>
                <DCWC:Legend Name="Default"></DCWC:Legend>
            </Legends>
            <Series>
                <DCWC:Series Name="Default">
                </DCWC:Series>
            </Series>
            <ChartAreas>
                <DCWC:ChartArea Name="Default">
                </DCWC:ChartArea>
            </ChartAreas>
        </DCWC:Chart>
        
        <a href='javascript:history.back();'>- back -</a>
    </form>
</body>
</html>

