﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Report.aspx.vb" Inherits="reports_DataAnalysis_Comparison_Report" %>

<%@ Register Assembly="DundasWebChart" Namespace="Dundas.Charting.WebControl" TagPrefix="DCWC" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Comparison</title>
    <link rel="stylesheet" href="../../../modalbox/modalreport.css" type="text/css" media="screen" />   

</head>
<body>
    <form id="form1" runat="server">

<table>
    <tr>
        <td>
            <table>
                <tr>
                    <td>
                        <DCWC:Chart ID="Chart1" runat="server" Width="837px" Height="400px">
                            <Legends>
                                <DCWC:Legend Name="Default"></DCWC:Legend>
                            </Legends>
                            <Series>
                                <DCWC:Series Name="Default">
                                </DCWC:Series>
                            </Series>
                            <ChartAreas>
                                <DCWC:ChartArea Name="Default">
                                </DCWC:ChartArea>
                            </ChartAreas>
                        </DCWC:Chart>
                    </td>
                </tr>
            </table> <%--Charts--%>
        </td>
    </tr><%--charts--%>
    <tr>
        <td style="vertical-align:top;">
            <table>
                <tr>
                    <td ID="table1" runat="server" >
                        
                    </td>
                </tr>
            </table>
        </td>
    </tr><%--table--%>
</table> 


  
       


    </form>
    
    <div class="FadingTooltip" id="FADINGTOOLTIP" style="Z-INDEX: 999; VISIBILITY: hidden; POSITION: absolute"></div>

    <script type="text/javascript" language="javascript">
        var FADINGTOOLTIP;
        var wnd_height, wnd_width;
        var tooltip_height, tooltip_width;
        var tooltip_shown = false;
        var transparency = 100;
        var timer_id = 1;
                
        // override events

        window.onload = WindowLoading;
        window.onresize = UpdateWindowSize;
        document.onmousemove = AdjustToolTipPosition;

        function DisplayTooltip(tooltip_url)
        {
            FADINGTOOLTIP.innerHTML = '<IMG SRC="' + tooltip_url + '">';
            tooltip_shown = (tooltip_url != "") ? true : false;
            
            if(tooltip_url != "")
            {
                // Get tooltip window height
                tooltip_height=(FADINGTOOLTIP.style.pixelHeight)? FADINGTOOLTIP.style.pixelHeight : FADINGTOOLTIP.offsetHeight;
                transparency=0;
                ToolTipFading();
            } 
            else 
                {
                    clearTimeout(timer_id);
                    FADINGTOOLTIP.style.visibility="hidden";
                }
            }

            function AdjustToolTipPosition(e)
            {
                if(tooltip_shown)
                {
                    // Depending on IE/Firefox, find out what object to use to find mouse position
                    var ev;
                    if(e)
                        ev = e;
                    else
                        ev = event;

                    offset_y = (ev.clientY + tooltip_height - document.body.scrollTop + 30 >= wnd_height) ? - 15 - tooltip_height: 20;
                    FADINGTOOLTIP.style.visibility = "visible";
                    FADINGTOOLTIP.style.left = Math.min(wnd_width - tooltip_width - 10, Math.max(3, ev.clientX + 6)) + document.body.scrollLeft + 'px';
                    FADINGTOOLTIP.style.top = ev.clientY + offset_y + document.body.scrollTop + 'px';
                }
            }

            function WindowLoading()
            {
                FADINGTOOLTIP=document.getElementById('FADINGTOOLTIP');
        
                // Get tooltip  window width 
                tooltip_width = (FADINGTOOLTIP.style.pixelWidth) ? FADINGTOOLTIP.style.pixelWidth : FADINGTOOLTIP.offsetWidth;
                    
                // Get tooltip window height
                tooltip_height=(FADINGTOOLTIP.style.pixelHeight)? FADINGTOOLTIP.style.pixelHeight : FADINGTOOLTIP.offsetHeight;

                UpdateWindowSize();
            }
                
            function ToolTipFading()
            {
                if(transparency <= 100)
                {
                    FADINGTOOLTIP.style.filter="alpha(opacity="+transparency+")";
                    FADINGTOOLTIP.style.opacity=transparency/100;
                    transparency += 5;
                    timer_id = setTimeout('ToolTipFading()', 35);
                }
            }

            function UpdateWindowSize() 
            {
                wnd_height=document.body.clientHeight;
                wnd_width=document.body.clientWidth;
            }
        </script>
</body>
</html>
