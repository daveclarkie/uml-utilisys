﻿Imports Dundas.Charting.WebControl
Imports Dundas.Charting.WebControl.Utilities

Imports System.Data.SqlClient
Imports System.Data
Imports System.Drawing
Imports System.Windows



Partial Class reports_UtilisysClassic_Comparison_Preview
    Inherits System.Web.UI.Page
    Public conn As String = Core.ConnectionString()

    Shared cGreen As Color = System.Drawing.ColorTranslator.FromHtml("#009530")
    Shared cBlue As Color = System.Drawing.ColorTranslator.FromHtml("#F18308")
    Shared cYellow As Color = System.Drawing.ColorTranslator.FromHtml("#26A7E7")

    Shared colorSet() As Color = {cGreen, cBlue, cYellow, Color.Purple}

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        Session.LCID = 2057
        Dim varMeterID As String = Core.data_select_value("SELECT varMeterID FROM uvw_DisplayAllMeterPoints WHERE ID = '" & Request.QueryString("meterid") & "'") 'varMeterID
        Dim intChannelID As Integer = Core.data_select_value("SELECT intChannelID FROM uvw_DisplayAllMeterPoints WHERE ID = '" & Request.QueryString("meterid") & "'") 'varMeterID
        Dim strMeter As String = Core.data_select_value("SELECT varNiceName FROM uvw_DisplayAllMeterPoints WHERE ID = '" & Request.QueryString("meterid") & "'") 'varMeterID
        Dim dtmDate As String = Request.QueryString("date")

        'Dim varMeterID As String = "1014570127402"
        'Dim intChannelID As Integer = 1
        'Dim dtmDate As String = "01/02/2009"


        Dim strSQL As String = "EXEC uml_extdata.dbo.usp_Portal_MyReports_Comparison_PerDay '" & Request.QueryString("meterid") & "', '" & dtmDate & "'"

        With Chart1

            .Visible = True
            .Series.Clear()
            .Titles.Clear()
            .Legends("Default").Enabled = False

            .Titles.Add(strMeter & " - " & dtmDate)

            .DataSource = Core.data_select(strSQL)
            .DataBind()
            .Series.Add("Default")
            .Series(0).ValueMemberX = "dtmTime"
            .Series(0).ValueMembersY = "fltReading"
            .Series(0).Type = Dundas.Charting.WebControl.SeriesChartType.Column

            .Series.Add("Target")
            .Series(1).ValueMemberX = "dtmTime"
            .Series(1).ValueMembersY = "fltReadingOld"
            .Series(1).Type = Dundas.Charting.WebControl.SeriesChartType.Line

            .ChartAreas(0).AxisY.IntervalAutoMode = IntervalAutoMode.VariableCount
            .ChartAreas(0).AxisY.MajorGrid.LineStyle = ChartDashStyle.Dot
            .ChartAreas(0).AxisX.MajorGrid.LineStyle = ChartDashStyle.Dot

            .ChartAreas(0).AxisY.LabelStyle.Format = "#,###"
            .ChartAreas(0).AxisX.Margin = False

            .PaletteCustomColors = colorSet
            .ChartAreas(0).BackColor = Color.Silver

            .BorderColor = Color.Black
            .BorderStyle = ChartDashStyle.Solid
            .BorderWidth = 2
            .BorderSkin.SkinStyle = BorderSkinStyle.Emboss

            .RenderType = RenderType.BinaryStreaming
        End With

    End Sub

End Class
