﻿<%@ Page Title="UTILISYS - Internal League Table Report" Language="VB" MasterPageFile="~/landing.master" AutoEventWireup="false" CodeFile="Default.aspx.vb" Inherits="reports_Performance_InternalLeagueTable_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="content_right" Runat="Server">


        <script type="text/javascript" src="../../../ThickBox/jquery-1.3.2.min.js"></script>
        <script type="text/javascript" src="../../../ThickBox/thickbox.js"></script>
        <link rel="stylesheet" href="../../../ThickBox/thickbox.css" type="text/css" media="screen" />   
        
            
<form id="Form1" runat="server">

    <div class="content_back general">
            <table cellpadding="0" cellspacing="0">
     <tr valign="middle">
<tr valign="middle">
                    <td style="width:450px;" >
                        
                    </td>
                    <td style="width:150px;">
                    </td>
                </tr> <!-- Company selection -->
                
                <tr>
                    
                    <td colspan="1">
                        <table cellpadding="0" cellspacing="0">

                            <tr>
                                <td style="width:120px;" valign="top">
                                     <asp:Label ID="lblDescription" Text="Description" runat="server" Font-Bold="true" Width="120px" ></asp:Label>
                                </td>
                                <td style="width:400px;" valign="middle">
                                     <asp:Label ID="lblName" Text="Name" runat="server"></asp:Label>
                                </td>
                            </tr>
                                                        
                            <tr>
                                <td style="height:3px;">
                                    
                                </td>
                            </tr>

                             <tr>
                                <td style="width:120px;" valign="top">
                                     <b>Report</b>
                                </td>
                                <td style="width:400px;" valign="middle">
                                    Internal League Table Report
                                </td>
                            </tr>
                                                        
                            <tr>
                                <td style="height:3px;">
                                    
                                </td>
                            </tr>

                            <tr>
                                <td style="width:120px;" valign="top">
                                    <asp:label ID="lblFilterNameAvailable" runat="server" Text="Available Meters" Width="120px" Font-Bold="true"></asp:label>
                                </td>
                                <td style="width:400px;" valign="top">
                                    <asp:RadioButtonList Width="450px" ID="rblMeterFilter" runat="server" RepeatLayout="table" RepeatDirection="Horizontal" AutoPostBack="true">
                                        <asp:ListItem Value="All" Text="Show All" Selected="True"></asp:ListItem>
                                        <asp:ListItem Value="A" Text="Elec"></asp:ListItem>
                                        <asp:ListItem Value="B" Text="Gas"></asp:ListItem>
                                        <asp:ListItem Value="C" Text="Sub"></asp:ListItem>
                                        <asp:ListItem Value="Custom" Text="Custom"></asp:ListItem>
                                    </asp:RadioButtonList>
                                    <div id="secFilter" runat="server" visible="false">
                                        <asp:textbox ID="txtCustomFilter" runat="server" AutoPostBack="true" BackColor="#FFFFC0" style="width:400px;"></asp:textbox>
                                        <asp:Button ID="btnFilter" runat="server" Text="Go" />
                                    </div>
                                </td>
                            </tr>
                            
                            <tr>
                                <td colspan="2">
                                    
                                </td>
                            </tr>
                            
                            <tr>
                                <td colspan="2">
                                    <asp:ListBox ID="lbAvailableMeters" runat="server" style="width:630px; height:200px;" SelectionMode="Multiple" AutoPostBack="true" BackColor="#FFFFC0" >
                                    </asp:ListBox>
                                </td>
                            </tr>

                            <tr valign="middle">
                                <td >
                                    <b>Meter Groups</b>
                                </td>
                                <td align="left" colspan="7">
                                    <asp:DropDownList id="ddlMeterGrouping" runat="server" Width="350px" BackColor="#FFFFC0" AutoPostBack="true">
                                        <asp:ListItem Text="-- none --" Value="0" Selected="True"></asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:Button ID="btnSelectMeterGrouping" runat="server" Text="select" visible="false" Width="50px"/>
                                </td>
                            </tr> <!-- Meter Templates -->
                            
                            <tr>
                                <td>
                                    <asp:label ID="lblFilterNameSelected" runat="server" Text="Assigned Meters" Width="120px" Font-Bold="true"></asp:label>
                                    <asp:Button ID="btnAssignedMetersRemoveAll" runat="server" Text="Unassign All Meters" Width="150px" Enabled="false" />
                                </td>
                                <td>
                                    <asp:Label ID="lblAssignedMeterError" Text="" runat="server" ForeColor="Red"></asp:Label>
                                </td>
                            </tr>
                            
                            <tr>
                                <td colspan="2">
                                    <asp:ListBox ID="lbAssignedMeters" runat="server" style="width:630px; height:100px;" SelectionMode="Multiple" AutoPostBack="true" BackColor="#FFFFC0" >
                                    </asp:ListBox>                                    
                                </td>
                            </tr>
                                   
                            <tr valign="middle">
                                <td valign="top" >
                                    <b>Date Type</b>
                                </td>
                                <td align="left">
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <asp:DropDownList ID="ddlDateType" runat="server" Width="200px" AutoPostBack="true"></asp:DropDownList>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td >
                        
                                </td>
                            </tr> <!-- Date Type -->
                
                            <tr valign="middle">
                                <td valign="top" >
                                    <b>Select Current Date</b>
                                </td>
                                <td align="left">
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <asp:DropDownList ID="ddlDateSelect" runat="server" Width="200px" AutoPostBack="true"></asp:DropDownList>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td >
                        
                                </td>
                            </tr> <!-- Dates -->
                
                            <tr valign="middle">
                                <td valign="top" >
                                    <b>Select Baseline Date</b>
                                </td>
                                <td align="left">
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <asp:DropDownList ID="ddlDateSelectBaseline" runat="server" Width="200px" AutoPostBack="true"></asp:DropDownList>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td >
                        
                                </td>
                            </tr> <!-- Dates -->

                            <tr valign="middle">
                                <td valign="top" >
                                    <b>Target % Decrease</b>
                                </td>
                                <td align="left">
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="txtPercentageDecrease" runat="server" Width="200px" Text="0" AutoPostBack="true"   ></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td >
                        
                                </td>
                            </tr> <!-- Target -->

                            <tr valign="middle">
                                <td valign="top" >
                                    <b>Ranking Element</b>
                                </td>
                                <td align="left">
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <asp:DropDownList ID="ddlElement" runat="server" Width="200px" AutoPostBack="true">
                                                    <asp:ListItem Value="Consumption" Text="Consumption" Selected="True"></asp:ListItem>
                                                    <asp:ListItem Value="Cost" Text="Cost"></asp:ListItem>
                                                    <asp:ListItem Value="CRCCarbon" Text="Carbon - CRC"></asp:ListItem>
                                                    <asp:ListItem Value="CCACarbon" Text="Carbon - CCA"></asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td >
                        
                                </td>
                            </tr> <!-- Ranking Element -->

                        </table>
                    </td>
                </tr> <!-- Meter Selection Table -->
                
                <tr>
                    <td colspan="3" style="text-align:center;">
                        <asp:HyperLink  ID="lnkNewLink" runat="server" CssClass="thickbox"></asp:HyperLink>
                    </td>
                </tr> <!-- Run button -->  
                   
                <tr>
                    <td colspan="3" style="text-align:center;">
                        <asp:label  ID="lblError" runat="server"></asp:label>
                    </td>
                </tr> <!-- Error message -->               
            </table>
        </div>

</form>










</asp:Content>

