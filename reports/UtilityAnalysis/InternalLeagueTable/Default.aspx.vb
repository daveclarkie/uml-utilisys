﻿Imports System.Drawing

Partial Class reports_Performance_InternalLeagueTable_Default
    Inherits System.Web.UI.Page
    Dim intMaxMeters As Integer = 300

    Public Overloads Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)

        ' Confirms that an HtmlForm control is rendered for the specified ASP.NET server control at run time.




    End Sub

    Private Sub ResetForm()
        Me.lbAssignedMeters.Items.Clear()
        MeterGroupingDropDownBox()
        MeterPoints_Show(Me.rblMeterFilter.SelectedItem.Value)


    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        Try
            If Not IIf(Core.data_select_value("select bitAllowMyReports from tblUsers where intUserPK = " & MySession.UserID) = "True", True, False) Then
                Response.Redirect(Core.data_select_value("select RawHomeURL from vwUsers where ID = " & MySession.UserID), True)
            End If

            If Request.QueryString("custid") Is Nothing Then
                Dim strSQL As String = "EXEC spPortal_TREEVIEW_TopLevel " & MySession.UserID
                Dim myConnection As New OleDb.OleDbConnection(Core.CurrentConnectionString)

                Dim myCommand As New OleDb.OleDbCommand(strSQL, myConnection)
                myCommand.Connection.Open()
                myCommand.CommandTimeout = 180
                Dim Reader As OleDb.OleDbDataReader = myCommand.ExecuteReader(CommandBehavior.CloseConnection)

                If Reader.HasRows Then
                    Reader.Read()
                    If Reader("Type") = "Company" Then
                        Response.Redirect("default.aspx?custid=" & Reader("id") & "&method=Customer", True)
                    Else
                        Response.Redirect("default.aspx?custid=" & Reader("id") & "&method=" & Reader("Type"), True)
                    End If
                    Reader.Close()
                End If
            End If

            If Not Page.IsPostBack = True Then

                MeterPoints_Show("All")

                Dim description As HtmlMeta = New HtmlMeta()
                description.Name = "description"
                description.Content = "UTILISYS - Monthly Consumption Report"
                Dim keywords As HtmlMeta = New HtmlMeta()
                keywords.Name = "keywords"
                keywords.Content = "UTILISYS - Monthly Consumption Report"
                Page.Header.Controls.Add(description)
                Page.Header.Controls.Add(keywords)


            End If

            If Not Page.IsPostBack Then

                LoadDateTypes()
                MeterGroupingDropDownBox()
            End If

            CountAssignedMeters()

            ThickBox()


            Me.lblDescription.Text = Request.QueryString("method").ToString
            Me.lblName.Text = Core.data_select_value("EXEC spSites_MAP_CurentSelection " & MySession.UserID & ", '" & Request.QueryString("method") & "', " & Request.QueryString("custid"))
            

        Catch ex As Exception
            '
        End Try


    End Sub

    Private Sub MeterPoints_Show(ByVal Type As String)


        With Me.lbAvailableMeters
            .DataSource = Core.data_select(" EXEC [portal.utilitymasters.co.uk].dbo.usp_MeterPoints_Display " & MySession.UserID & ", '" & Request.QueryString("method") & "', " & Request.QueryString("custid") & ",'" & Type & "', '" & ListAssignedMeters() & "', '" & Me.txtCustomFilter.Text.ToString & "', 1")
            .DataValueField = "ID"
            .DataTextField = "varNiceName"
            .DataBind()
        End With

        CountAssignedMeters()
        LoadDateTypes()
    End Sub

    Protected Sub ddlMeterGrouping_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlMeterGrouping.SelectedIndexChanged

        Dim intSelectedGroup As Integer = 0

        If Me.ddlMeterGrouping.SelectedItem.Value > 0 Then intSelectedGroup = Me.ddlMeterGrouping.SelectedItem.Value

        If intSelectedGroup > 0 Then
            Dim intMeterGroupingID As Integer = intSelectedGroup

            With Me.lbAssignedMeters
                .DataSource = Core.data_select("SELECT varMeterViewID, varNiceName FROM [portal.utilitymasters.co.uk].dbo.tblMeterGrouping_Meters INNER JOIN uvw_DisplayAllMeterPoints ON ID = varMeterViewID WHERE intMeterGrouping_NameFK = " & intMeterGroupingID)
                .DataValueField = "varMeterViewID"
                .DataTextField = "varNiceName"
                .DataBind()
            End With
            CountAssignedMeters()
            MeterPoints_Show(Me.rblMeterFilter.SelectedItem.Value)
        Else
            ResetForm()

        End If



        

    End Sub

    Protected Sub rblMeterFilter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rblMeterFilter.SelectedIndexChanged
        If Me.rblMeterFilter.SelectedItem.Value = "Custom" Then
            Me.txtCustomFilter.Text = Nothing
            Me.secFilter.Visible = True
        Else
            Me.txtCustomFilter.Text = Nothing
            Me.secFilter.Visible = False
        End If


        MeterPoints_Show(Me.rblMeterFilter.SelectedItem.Value)
    End Sub

    Protected Sub lbAvailableMeters_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbAvailableMeters.SelectedIndexChanged

        Dim iAssigned As Integer = 0
        For Each xAssigned As ListItem In lbAssignedMeters.Items
            iAssigned += 1
        Next
        If iAssigned < intMaxMeters Then
            Me.lblAssignedMeterError.Text = ""

            Dim i As Integer = 0
            For Each x As ListItem In lbAvailableMeters.Items
                If x.Selected Then
                    i += 1
                End If
            Next

            If i > 0 Then
repeat:
                If lbAvailableMeters.Items.Count > 0 Then

                    Dim li As ListItem

                    For Each li In lbAvailableMeters.Items
                        If li.Selected = True Then
                            Dim iAssigned2 As Integer = 0
                            For Each xAssigned2 As ListItem In lbAssignedMeters.Items
                                iAssigned2 += 1
                            Next
                            If iAssigned2 = intMaxMeters Then
                                GoTo meterlimitexceeded
                            End If
                            lbAssignedMeters.Items.Add(li)
                            lbAvailableMeters.Items.Remove(li)
                            GoTo repeat
                        End If

                    Next

                End If
            End If

            Me.lbAssignedMeters.SelectedIndex = -1
            Me.lbAvailableMeters.SelectedIndex = -1

        Else

        End If
        Me.lblAssignedMeterError.Text = "(" & returncountofassignedmeters() & "/" & intMaxMeters & ")"

        MeterPoints_Show(Me.rblMeterFilter.SelectedItem.Value)
        Exit Sub



meterlimitexceeded:
        Me.lblAssignedMeterError.Text = "you are currently limited to " & intMaxMeters & " meter point."
        CountAssignedMeters()

        ThickBox()

    End Sub

    Protected Sub lbAssignedMeters_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbAssignedMeters.SelectedIndexChanged


        Me.lblAssignedMeterError.Text = ""

        Dim i As Integer = 0
        For Each x As ListItem In lbAssignedMeters.Items
            If x.Selected Then
                i += 1
            End If
        Next


        If i > 0 Then
repeat:
            If lbAssignedMeters.Items.Count > 0 Then

                Dim li As ListItem

                For Each li In lbAssignedMeters.Items
                    If li.Selected = True Then
                        lbAvailableMeters.Items.Add(li)
                        lbAssignedMeters.Items.Remove(li)
                        GoTo repeat
                    End If

                Next

            End If
        End If

        Me.lbAssignedMeters.SelectedIndex = -1
        Me.lbAvailableMeters.SelectedIndex = -1

        MeterPoints_Show(Me.rblMeterFilter.SelectedItem.Value)
        CountAssignedMeters()
        LoadDateTypes()
        ThickBox()
    End Sub

    Private Function ListAssignedMeters() As String
        Dim s As String = ""

        If Me.lbAssignedMeters.Items.Count > 0 Then
            For Each x As ListItem In lbAssignedMeters.Items
                s &= "," & x.Value
            Next
            If s.Length > 0 Then
                s = s.Remove(0, 1)
            End If
        End If

        Return s

    End Function

    Private Function returncountofassignedmeters() As String
        Dim i As Integer = 0
        For Each x As ListItem In lbAssignedMeters.Items
            i += 1
        Next
        Return i
    End Function

    Private Sub CountAssignedMeters()
        Dim i As Integer = 0
        For Each x As ListItem In lbAssignedMeters.Items
            i += 1
        Next

        If i > (intMaxMeters - 1) Then
            Me.lbAvailableMeters.Enabled = False
        Else
            Me.lbAvailableMeters.Enabled = True
        End If

        If i > 0 Then
            Me.btnAssignedMetersRemoveAll.Enabled = True
        Else
            Me.btnAssignedMetersRemoveAll.Enabled = False
        End If
        Me.lblAssignedMeterError.Text = "(" & returncountofassignedmeters() & "/" & intMaxMeters & ")"
    End Sub


    'Create the drop down lists

    Private Sub LoadDateTypes()

        Dim i As Integer = 0
        Dim s As String = ListAssignedMeters()

        i = s.Length

        If i > 0 Then
            With Me.ddlDateType
                .Items.Clear()
                .DataSource = Core.data_select("EXEC [portal.utilitymasters.co.uk].dbo.usp_MyReports_Parameters_CustomDateTypes 2")
                .DataValueField = "intOrder"
                .DataTextField = "varValue"
                .DataBind()
            End With
        Else
            With Me.ddlDateType
                .Items.Clear()
                .DataSource = Core.data_select("SELECT 0 as [varValue], 'please select a meter' as [varText]")
                .DataValueField = "varValue"
                .DataTextField = "varText"
                .DataBind()
            End With
        End If


        LoadCurrentDates()


    End Sub
    Private Sub LoadCurrentDates()

        Dim i As Integer = Me.ddlDateType.SelectedItem.Value
        Dim x As String = Me.ddlDateType.SelectedItem.Text
        Dim s As String = ListAssignedMeters()

        If i > 0 Then

            With Me.ddlDateSelect
                .Items.Clear()
                .DataSource = Core.data_select("EXEC [portal.utilitymasters.co.uk].dbo.usp_MyReports_Parameters_CustomDateRanges '" & x & "'")
                .DataValueField = "ID"
                .DataTextField = "Value"
                .DataBind()
            End With
        Else
            With Me.ddlDateSelect
                .Items.Clear()
                .DataSource = Core.data_select("SELECT '01/01/1900' as ID, 'please select a date type' as Value")
                .DataValueField = "ID"
                .DataTextField = "Value"
                .DataBind()
            End With
        End If
        LoadBaselineDates()

    End Sub
    Private Sub LoadBaselineDates()

        Dim i As Date = Me.ddlDateSelect.SelectedItem.Value
        Dim x As String = Me.ddlDateType.SelectedItem.Text
        Dim s As String = ListAssignedMeters()

        If i > "01/01/1900" Then

            With Me.ddlDateSelectBaseline
                .Items.Clear()
                .DataSource = Core.data_select("EXEC [portal.utilitymasters.co.uk].dbo.usp_MyReports_Parameters_CustomDateRanges '" & x & "'")
                .DataValueField = "ID"
                .DataTextField = "Value"
                .DataBind()
            End With
        Else
            With Me.ddlDateSelectBaseline
                .Items.Clear()
                .DataSource = Core.data_select("SELECT '01/01/1900' as ID, 'please select a current date' as Value")
                .DataValueField = "ID"
                .DataTextField = "Value"
                .DataBind()
            End With
        End If
        ThickBox()
    End Sub
    Private Sub MeterGroupingDropDownBox()
        With Me.ddlMeterGrouping
            .Items.Clear()
            .DataSource = Core.data_select("SELECT 0 as ID, '-- select a group / create a new group --' as varName, 0 as intOrder UNION SELECT DISTINCT intMeterGrouping_NamePK, varMeterGrouping_Name, 1 FROM [portal.utilitymasters.co.uk].dbo.tblMeterGrouping_Name WHERE intCustomerFK = " & Request.QueryString("custid") & " ORDER BY intOrder, varName ASC")
            .DataValueField = "ID"
            .DataTextField = "varName"
            .DataBind()
        End With

        If Me.ddlMeterGrouping.Items.Count > 1 Then Me.ddlMeterGrouping.Enabled = True Else Me.ddlMeterGrouping.Enabled = False

        If Me.ddlMeterGrouping.Items.Count = 1 Then Me.ddlMeterGrouping.SelectedItem.Text = "-- no groups configured - create a new group --"

    End Sub


    Protected Sub ddlDateType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlDateType.SelectedIndexChanged
        LoadCurrentDates()
    End Sub

    Protected Sub ddlDate_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlDateSelect.SelectedIndexChanged

        LoadBaselineDates()
        ThickBox()
    End Sub


    Protected Sub btnFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFilter.Click
        Me.txtCustomFilter.Text = fnNoSpecialCharacters(Me.txtCustomFilter.Text.ToString)
        MeterPoints_Show(Me.rblMeterFilter.SelectedItem.Value)

    End Sub

    Public Function fnNoSpecialCharacters(ByVal strTest As String) As String

        Dim sb As New System.Text.StringBuilder

        For Each ch As Char In strTest
            If Char.IsLetterOrDigit(ch) OrElse ch = " "c Then
                sb.Append(ch)
            End If
        Next

        strTest = sb.ToString

        Return strTest
    End Function

    Function fnNumbersOnly(ByVal strTest As String) As String

        Dim sb As New System.Text.StringBuilder

        For Each ch As Char In strTest
            If Char.IsDigit(ch) Then
                sb.Append(ch)
            End If
        Next

        strTest = sb.ToString

        Return strTest
    End Function


    Protected Sub txtPercentageDecrease_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtPercentageDecrease.TextChanged
        Me.txtPercentageDecrease.Text = fnNumbersOnly(Me.txtPercentageDecrease.Text)
        ThickBox()
    End Sub

    Protected Sub ddlElement_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlElement.SelectedIndexChanged
        ThickBox()
    End Sub


    Public Sub ThickBox()


        Dim intError As Integer = 0

        Dim s As String = ListAssignedMeters()
        If Me.ddlDateSelect.Items.Count > 0 Then
            If Me.ddlDateSelect.SelectedItem.Value = "01/01/1900" Then
                Me.lnkNewLink.NavigateUrl = ""
                Me.lnkNewLink.Text = ""
                Me.lnkNewLink.ImageUrl = "../../../assets/Schneider/buttons-myreports-general-generatereport-disabled.png"
                Me.lnkNewLink.CssClass = ""
                Me.lnkNewLink.ToolTip = ""
                Me.lblError.Text = "you need to select a current date"
            ElseIf CDate(Me.ddlDateSelect.SelectedItem.Value) <= CDate(Me.ddlDateSelectBaseline.SelectedItem.Value) Then
                Me.lnkNewLink.NavigateUrl = ""
                Me.lnkNewLink.Text = ""
                Me.lnkNewLink.ImageUrl = "../../../assets/Schneider/buttons-myreports-general-generatereport-disabled.png"
                Me.lnkNewLink.CssClass = ""
                Me.lnkNewLink.ToolTip = ""
                Me.lblError.Text = "the baseline needs to be before the current date"
            Else
                If intError = 0 Then 'ok
                    If s.Length > 0 Then
                        Me.lnkNewLink.NavigateUrl = "report.aspx?Meter=" + CStr(s) + "&DateType=" + CStr(Me.ddlDateType.SelectedItem.Text) + "&CurrentDate=" + CStr(Me.ddlDateSelect.SelectedItem.Value) + "&BaselineDate=" + CStr(Me.ddlDateSelectBaseline.SelectedItem.Value) + "&Target=" & Me.txtPercentageDecrease.Text.ToString & "&Element=" & Me.ddlElement.SelectedItem.Value & "&?keepThis=true&TB_iframe=true&height=550&width=850"
                        Me.lnkNewLink.Text = "Generate Report"
                        Me.lnkNewLink.ImageUrl = "../../../assets/Schneider/buttons-myreports-general-generatereport.png"
                        Me.lnkNewLink.CssClass = "thickbox"
                        Me.lnkNewLink.ToolTip = "Internal League Table"
                        Me.lblError.Text = "" 'Me.lnkNewLink.NavigateUrl.ToString
                    Else
                        Me.lnkNewLink.NavigateUrl = ""
                        Me.lnkNewLink.Text = ""
                        Me.lnkNewLink.ImageUrl = "../../../assets/Schneider/buttons-myreports-general-generatereport-disabled.png"
                        Me.lnkNewLink.CssClass = ""
                        Me.lnkNewLink.ToolTip = ""
                        Me.lblError.Text = "there are no meters selected."
                    End If
                Else
                    Me.lnkNewLink.NavigateUrl = ""
                    Me.lnkNewLink.Text = ""
                    Me.lnkNewLink.ImageUrl = "../../../assets/Schneider/buttons-myreports-general-generatereport-disabled.png"
                    Me.lnkNewLink.CssClass = ""
                    Me.lnkNewLink.ToolTip = ""
                End If

            End If
        Else
            Me.lnkNewLink.NavigateUrl = ""
            Me.lnkNewLink.Text = ""
            Me.lnkNewLink.ImageUrl = "../../../assets/Schneider/buttons-myreports-general-generatereport-disabled.png"
            Me.lnkNewLink.CssClass = ""
            Me.lnkNewLink.ToolTip = ""
        End If

        'Me.lblError.Text = Me.lnkNewLink.NavigateUrl.ToString

    End Sub


    Protected Sub btnAssignedMetersRemoveAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAssignedMetersRemoveAll.Click
        ResetForm()
    End Sub
End Class
