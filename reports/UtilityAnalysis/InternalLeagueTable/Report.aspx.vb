﻿Imports clsReportServerCredentials

Partial Class reports_Performance_InternalLeagueTable_Report
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim QSvarMeter As String = Request.QueryString("Meter")
        Dim QSDateType As String = Request.QueryString("DateType")
        Dim QSDateCurrent As Date = Request.QueryString("CurrentDate")
        Dim QSDateBaseline As Date = Request.QueryString("BaselineDate")
        Dim QSTarget As String = Request.QueryString("Target")
        Dim QSElement As String = Request.QueryString("Element")

        Me.frmReport.Visible = True

        'Set the processing mode for the ReportViewer to Remote
        myReportViewer.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Remote

        Dim serverReport As Microsoft.Reporting.WebForms.ServerReport
        serverReport = myReportViewer.ServerReport

        serverReport.ReportServerCredentials = New clsReportServerCredentials("administrator", "chicago", "uml")

        'Set the report server URL and report path
        serverReport.ReportServerUrl = _
            New Uri("http://10.44.5.205/reportserver")
        serverReport.ReportPath = "/Portal Reporting/Internal League Table Report"

        Dim varMeterList As New Microsoft.Reporting.WebForms.ReportParameter()
        varMeterList.Name = "varMeterList"
        varMeterList.Values.Add(QSvarMeter)

        Dim varDateType As New Microsoft.Reporting.WebForms.ReportParameter()
        varDateType.Name = "varDateType"
        varDateType.Values.Add(QSDateType)

        Dim dtmDateStart As New Microsoft.Reporting.WebForms.ReportParameter()
        dtmDateStart.Name = "dtmDateStart"
        dtmDateStart.Values.Add(QSDateCurrent)

        Dim dtmBaselineStart As New Microsoft.Reporting.WebForms.ReportParameter()
        dtmBaselineStart.Name = "dtmBaselineStart"
        dtmBaselineStart.Values.Add(QSDateBaseline)

        Dim fltTarget As New Microsoft.Reporting.WebForms.ReportParameter()
        fltTarget.Name = "fltTarget"
        fltTarget.Values.Add(QSTarget)

        Dim SortElement As New Microsoft.Reporting.WebForms.ReportParameter()
        SortElement.Name = "SortElement"
        SortElement.Values.Add(QSElement)

        'Set the report parameters for the report
        Dim parameters() As Microsoft.Reporting.WebForms.ReportParameter = {varMeterList, varDateType, dtmDateStart, dtmBaselineStart, fltTarget, SortElement}
        serverReport.SetParameters(parameters)


    End Sub

End Class
