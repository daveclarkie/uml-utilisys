Imports Dundas.Charting.WebControl
Imports System.Drawing

Imports Dundas.Charting.WebControl.Utilities

Imports System.Data.SqlClient
Imports System.Data
Imports System.IO
Imports System.Windows

Partial Class reports_onlinegraph_Default
    Inherits System.Web.UI.Page

    Public conn As String = Core.ConnectionString()

    Shared cGreen As Color = System.Drawing.ColorTranslator.FromHtml("#6EBB1F")
    Shared cBlue As Color = System.Drawing.ColorTranslator.FromHtml("#082B61")
    Shared cYellow As Color = System.Drawing.ColorTranslator.FromHtml("#FFFFC0")

    Shared colorSet() As Color = {cBlue, cGreen, Color.Purple, cYellow}


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        Try
            If Not IIf(Core.data_select_value("select bitAllowMySites from tblUsers where intUserPK = " & MySession.UserID) = "True", True, False) Then
                Response.Redirect(Core.data_select_value("select RawHomeURL from vwUsers where ID = " & MySession.UserID), True)
            End If

            If Not Page.IsPostBack = True Then
                Dim description As HtmlMeta = New HtmlMeta()
                description.Name = "description"
                description.Content = "UILISYS - Energy Matters"
                Dim keywords As HtmlMeta = New HtmlMeta()
                keywords.Name = "keywords"
                keywords.Content = "UTILISYS - Energy Matters"
                Page.Header.Controls.Add(description)
                Page.Header.Controls.Add(keywords)

                PrepareDropDownLists()

            End If

            If Request.QueryString("custid") Is Nothing Then
                Dim strSQL As String = "EXEC spPortal_TREEVIEW_TopLevel " & MySession.UserID
                Dim myConnection As New OleDb.OleDbConnection(Core.CurrentConnectionString)

                Dim myCommand As New OleDb.OleDbCommand(strSQL, myConnection)
                myCommand.Connection.Open()
                myCommand.CommandTimeout = 180
                Dim Reader As OleDb.OleDbDataReader = myCommand.ExecuteReader(CommandBehavior.CloseConnection)

                If Reader.HasRows Then
                    Reader.Read()
                    If Reader("Type") = "Company" Then
                        Response.Redirect("default.aspx?custid=" & Reader("id") & "&method=Customer", True)
                    Else
                        Response.Redirect("default.aspx?custid=" & Reader("id") & "&method=" & Reader("Type"), True)
                    End If
                    Reader.Close()
                End If


            End If

        Catch ex As Exception
            '
        End Try

        'ListBox1.Items.Clear()


        

        LoadFiles()




    End Sub
    Public Overloads Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)

        ' Confirms that an HtmlForm control is rendered for the specified ASP.NET server control at run time.

    End Sub

    Private Sub PrepareDropDownLists()

        With Me.ddlYearFilter
            .Items.Clear()
            .DataSource = Core.data_select("select distinct datepart(Year, pk_date) as dtmYear, year from uml_cms.dbo.dim_dates where pk_date < getdate() and datepart(Year, pk_date) > 2007 order by year desc")
            .DataValueField = "dtmYear"
            .DataTextField = "dtmYear"
            .DataBind()
        End With
        Me.ddlYearFilter.SelectedIndex = 0

        With Me.ddlMonthFilter
            .Items.Clear()
            .DataSource = Core.data_select("select distinct datename(month, month) as dtmMonthName, month as dtmMonth from uml_cms.dbo.dim_dates where pk_date < getdate() and datepart(year, month) = " & Me.ddlYearFilter.Text.ToString & " order by month desc")
            .DataValueField = "dtmMonthName"
            .DataTextField = "dtmMonthName"
            .DataBind()
        End With
        Me.ddlMonthFilter.SelectedIndex = 0


    End Sub

    Protected Sub ddlYearFilter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlYearFilter.SelectedIndexChanged

        With Me.ddlMonthFilter
            .Items.Clear()
            .DataSource = Core.data_select("select distinct datename(month, month) as dtmMonthName, month as dtmMonth from uml_cms.dbo.dim_dates where pk_date < getdate() and datepart(year, month) = " & Me.ddlYearFilter.Text.ToString & " order by month desc")
            .DataValueField = "dtmMonthName"
            .DataTextField = "dtmMonthName"
            .DataBind()
        End With
        Me.ddlMonthFilter.SelectedIndex = 0
        LoadFiles()
    End Sub

    Protected Sub ddlMonthFilter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlMonthFilter.SelectedIndexChanged

        
        LoadFiles()

    End Sub

    Private Sub LoadFiles()

        Me.ulBox.InnerHtml = ""

        Dim files As String()
        Dim File As String
        Dim filename As String
        Dim xFileCount As Integer = 0

        If IO.Directory.Exists(Core.DataDirectory & "Energy Reporting\Energy Matters\" & Me.ddlYearFilter.SelectedItem.Value.ToString & "\" & Me.ddlMonthFilter.SelectedItem.Value.ToString & "\") Then
            files = IO.Directory.GetFiles(Core.DataDirectory & "Energy Reporting\Energy Matters\" & Me.ddlYearFilter.SelectedItem.Value.ToString & "\" & Me.ddlMonthFilter.SelectedItem.Value.ToString & "\", "*.pdf")

            For Each File In files
                filename = Replace(File, Core.DataDirectory & "Energy Reporting\Energy Matters\" & Me.ddlYearFilter.SelectedItem.Value.ToString & "\" & Me.ddlMonthFilter.SelectedItem.Value.ToString & "\", "")
                Me.ulBox.InnerHtml &= "<li><a id='lnkReports" & xFileCount & "' runat='server' title='" & filename & "' href='" & File & "' >" & filename & "</a></li>"
                xFileCount += 1
                'ListBox1.Items.Add(Replace(File, Core.DataDirectory & "Energy Reporting\Energy Matters\", ""))
            Next
        Else
            Me.ulBox.InnerHtml &= "<li><a id='lnkReports0' runat='server' title='No files Exist' >There are no files archived for this period.</a></li>"
        End If

    End Sub
End Class
