<%@ Page Language="VB" MasterPageFile="~/landing.master" AutoEventWireup="false" CodeFile="Default.aspx.vb" Inherits="reports_onlinegraph_Default" title="UTILISYS - Energy Matters" %>

<%@ Register Assembly="DundasWebChart" Namespace="Dundas.Charting.WebControl" TagPrefix="DCWC" %>
<asp:Content ID="Content1" ContentPlaceHolderID="content_right" Runat="Server">

        <script type="text/javascript" src="../../../ThickBox/jquery-1.3.2.min.js"></script>
        <script type="text/javascript" src="../../../ThickBox/thickbox.js"></script>
        <link rel="stylesheet" href="../../../ThickBox/thickbox.css" type="text/css" media="screen" /> 
        
<form id="Form1" runat="server">

    <div class="content_back general">
            <table>
            
                <tr valign="middle">
                    <td width="150px" >
                        Report
                    </td>
                    <td width="400px" align="left">
                        Energy Matters
                    </td>
                    <td width="50px">
                        
                    </td>
                </tr>
                
                <tr valign="middle">
                    <td width="150px" >
                        Date Filter
                    </td>
                    <td width="400px" align="left">
                        <asp:DropDownList ID="ddlYearFilter" runat="server" AutoPostBack="true" Width="100px"></asp:DropDownList>
                        <asp:DropDownList ID="ddlMonthFilter" runat="server" AutoPostBack="true" Width="100px"></asp:DropDownList>
                    </td>
                    <td width="50px">
                        
                    </td>
                </tr>
                
                
                <tr>
                    <td style="height:25px;">
                        
                    </td>
                </tr>
                
                <tr>
                    <td style="vertical-align:text-top;">
                        Archived Files
                    </td>
                    <td>
                        <ul id="ulBox" runat="server">
            
            
                        </ul>
                    </td>
                </tr>
                
                
            </table>
            
</div>

  
</form>
</asp:Content>

