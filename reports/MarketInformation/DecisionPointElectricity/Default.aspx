<%@ Page Language="VB" MasterPageFile="~/landing.master" AutoEventWireup="false" CodeFile="Default.aspx.vb" Inherits="reports_onlinegraph_Default" title="Daily Consumption and Cost" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=9.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<%@ Register Assembly="DundasWebChart" Namespace="Dundas.Charting.WebControl" TagPrefix="DCWC" %>
<asp:Content ID="Content1" ContentPlaceHolderID="content_right" Runat="Server">

        <script type="text/javascript" src="../../../ThickBox/jquery-1.3.2.min.js"></script>
        <script type="text/javascript" src="../../../ThickBox/thickbox.js"></script>
        <link rel="stylesheet" href="../../../css/layout.css" type="text/css" media="screen" />  
        <link rel="stylesheet" href="../../../ThickBox/thickbox.css" type="text/css" media="screen" /> 
        
<form id="Form1" runat="server">

    <div class="content_back general">
            <table>
            
                <tr valign="middle">
                    <td width="150px" >
                        Report
                    </td>
                    <td width="400px" align="left">
                        Decision Point Electricity
                    </td>
                    <td width="50px">
                        
                    </td>
                </tr>
                
                <tr valign="middle">
                    <td width="150px" >
                        Latest Price
                    </td>
                    <td width="400px" align="left">
                        <asp:Label ID="lblLatestPrice" Text="Not Available" runat="server"></asp:Label>
                    </td>
                    <td width="50px">
                        
                    </td>
                </tr>
                
                <tr>
                    <td style="height:5px;">
                    </td>
                </tr>
                
            </table>
            <div style="position:absolute; left:240px; top: 50; border: solid 1px black; height:500px; background-color:White; " id="frmReport" runat="server" visible="false">
                <table style="margin-left: 0px; margin-right: auto; z-index:100;">
                    <tr>
                        <td colspan="4">
                            <rsweb:ReportViewer ID="myReportViewer" Height="450px" AsyncRendering="true" ShowPrintButton="false" runat="server" Width="650px" ShowRefreshButton="false" ShowToolBar="true" ShowZoomControl="false" ShowParameterPrompts="false" ShowBackButton="false" ShowCredentialPrompts="false" ShowFindControls="false" ShowPageNavigationControls="False" ShowPromptAreaButton="false">
                            </rsweb:ReportViewer> 
                        </td>
                    </tr>
                </table>
            </div>               
</div>


  
</form>
</asp:Content>

