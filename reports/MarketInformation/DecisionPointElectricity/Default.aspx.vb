Imports Dundas.Charting.WebControl
Imports System.Drawing

Imports Dundas.Charting.WebControl.Utilities

Imports System.Data.SqlClient
Imports System.Data
Imports System.Windows

Partial Class reports_onlinegraph_Default
    Inherits System.Web.UI.Page

    Public conn As String = Core.ConnectionString()

    Shared cGreen As Color = System.Drawing.ColorTranslator.FromHtml("#6EBB1F")
    Shared cBlue As Color = System.Drawing.ColorTranslator.FromHtml("#082B61")
    Shared cYellow As Color = System.Drawing.ColorTranslator.FromHtml("#FFFFC0")

    Shared colorSet() As Color = {cBlue, cGreen, Color.Purple, cYellow}


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        Try
            If Not IIf(Core.data_select_value("select bitAllowMySites from tblUsers where intUserPK = " & MySession.UserID) = "True", True, False) Then
                Response.Redirect(Core.data_select_value("select RawHomeURL from vwUsers where ID = " & MySession.UserID), True)
            End If

            If Not Page.IsPostBack = True Then
                Dim description As HtmlMeta = New HtmlMeta()
                description.Name = "description"
                description.Content = "Utility Masters customer extranet - Online Graph"
                Dim keywords As HtmlMeta = New HtmlMeta()
                keywords.Name = "keywords"
                keywords.Content = "daily analysis report"
                Page.Header.Controls.Add(description)
                Page.Header.Controls.Add(keywords)

            End If

            If Request.QueryString("custid") Is Nothing Then
                Dim strSQL As String = "EXEC spPortal_TREEVIEW_TopLevel " & MySession.UserID
                Dim myConnection As New OleDb.OleDbConnection(Core.CurrentConnectionString)

                Dim myCommand As New OleDb.OleDbCommand(strSQL, myConnection)
                myCommand.Connection.Open()
                myCommand.CommandTimeout = 180
                Dim Reader As OleDb.OleDbDataReader = myCommand.ExecuteReader(CommandBehavior.CloseConnection)

                If Reader.HasRows Then
                    Reader.Read()
                    If Reader("Type") = "Company" Then
                        Response.Redirect("default.aspx?custid=" & Reader("id") & "&method=Customer", True)
                    Else
                        Response.Redirect("default.aspx?custid=" & Reader("id") & "&method=" & Reader("Type"), True)
                    End If
                    Reader.Close()
                End If


            End If

        Catch ex As Exception
            '
        End Try

        Me.lblLatestPrice.Text = Core.data_select_value("EXEC [portal.utilitymasters.co.uk].dbo.usp_MyReports_MarketInfo_ElectricityPrices_LatestPrice '" & DateAdd(DateInterval.Day, -30, Now.Date) & "', '" & DateAdd(DateInterval.Day, 1, Now.Date) & "'")
        RunReport()
      
    End Sub
    Public Overloads Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)

        ' Confirms that an HtmlForm control is rendered for the specified ASP.NET server control at run time.

    End Sub

    Private Sub RunReport()

        If Request.QueryString("method") = "Customer" Then

            If Request.QueryString("custid") > "0" Then

                Me.frmReport.Visible = True

                'Set the processing mode for the ReportViewer to Remote
                myReportViewer.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Remote

                Dim serverReport As Microsoft.Reporting.WebForms.ServerReport
                serverReport = myReportViewer.ServerReport

                serverReport.ReportServerCredentials = New clsReportServerCredentials("administrator", "chicago", "uml")

                'Set the report server URL and report path
                serverReport.ReportServerUrl = _
                    New Uri("http://10.44.5.205/reportserver")
                serverReport.ReportPath = "/Portal Reporting/Decision Point Report"

                Dim customerid As New Microsoft.Reporting.WebForms.ReportParameter()
                customerid.Name = "custid"
                customerid.Values.Add(Request.QueryString("custid"))

                'Set the report parameters for the report
                Dim parameters() As Microsoft.Reporting.WebForms.ReportParameter = {customerid}
                serverReport.SetParameters(parameters)

            End If

        Else

        End If

    End Sub
    
End Class
