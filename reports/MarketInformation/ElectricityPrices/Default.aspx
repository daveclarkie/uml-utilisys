<%@ Page Language="VB" MasterPageFile="~/landing.master" AutoEventWireup="false" CodeFile="Default.aspx.vb" Inherits="reports_onlinegraph_Default" title="UTILISYS - Electricity Prices" %>

<%@ Register Assembly="DundasWebChart" Namespace="Dundas.Charting.WebControl" TagPrefix="DCWC" %>
<asp:Content ID="Content1" ContentPlaceHolderID="content_right" Runat="Server">

        <script type="text/javascript" src="../../../ThickBox/jquery-1.3.2.min.js"></script>
        <script type="text/javascript" src="../../../ThickBox/thickbox.js"></script>
        
        <link rel="stylesheet" href="../../../ThickBox/thickbox.css" type="text/css" media="screen" /> 
        
<form id="Form1" runat="server">

    <div class="content_back general">
            <table>
            
                <tr valign="middle">
                    <td width="150px" >
                        Report
                    </td>
                    <td width="400px" align="left">
                        Electricity Prices
                    </td>
                    <td width="50px">
                        
                    </td>
                </tr>
                
                <tr valign="middle">
                    <td width="150px" >
                        Latest Price
                    </td>
                    <td width="400px" align="left">
                        <asp:Label ID="lblLatestPrice" Text="Not Available" runat="server"></asp:Label>
                    </td>
                    <td width="50px">
                        
                    </td>
                </tr>
                
                <tr>
                    <td style="height:5px;">
                    </td>
                </tr>
                
                <tr>
                    <td width="150px" >
                        <asp:TextBox ID="txtList" runat="server" Visible="false"></asp:TextBox>
                        <asp:TextBox ID="txtStart" runat="server" Visible="false"></asp:TextBox>
                        <asp:TextBox ID="txtEnd" runat="server" Visible="false"></asp:TextBox>
                        <asp:TextBox ID="txtChartName" runat="server" Visible="false"></asp:TextBox>
                    </td>
                    <td width="400px" align="left">
                        
                    </td>
                    <td width="50px">
                        
                    </td>
                </tr>
                
                <tr>
                    <td colspan="3">
                        <DCWC:Chart ID="Chart1" runat="server" Width="650px" Height="400px">
                            <Legends>
                                <DCWC:Legend Name="Default"></DCWC:Legend>
                            </Legends>
                            <Series>
                                <DCWC:Series Name="Default">
                                </DCWC:Series>
                            </Series>
                            <ChartAreas>
                                <DCWC:ChartArea Name="Default">
                                </DCWC:ChartArea>
                            </ChartAreas>
                        </DCWC:Chart>
                    </td>
                </tr>
                
            </table>
            <table style="background-color:#cbb5cc; border-color:#000000; text-align:justify; width:650px; border: solid 1px #000000;">
                <tr>
                    <td style="width:10px;"></td>
                    <td> 
                        <b>Timescale</b>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td style="width:640px;">
                        <asp:RadioButtonList ID="rblTimescales" runat="server" AutoPostBack="true" RepeatDirection="Horizontal" RepeatColumns="5" RepeatLayout="Table" TextAlign="Right"></asp:RadioButtonList>
                    </td>
                </tr>
                   
            </table> <%-- Periods --%>
</div>

  
</form>
</asp:Content>

