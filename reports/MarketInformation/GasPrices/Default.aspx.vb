Imports Dundas.Charting.WebControl
Imports System.Drawing

Imports Dundas.Charting.WebControl.Utilities

Imports System.Data.SqlClient
Imports System.Data
Imports System.Windows

Partial Class reports_onlinegraph_Default
    Inherits System.Web.UI.Page

    Public conn As String = Core.ConnectionString()

    Shared cGreen As Color = System.Drawing.ColorTranslator.FromHtml("#B7D110")
    Shared cBlue As Color = System.Drawing.ColorTranslator.FromHtml("#26A7E7")
    Shared cYellow As Color = System.Drawing.ColorTranslator.FromHtml("#F18308")
    Shared cDPurple As Color = System.Drawing.ColorTranslator.FromHtml("#009530")
    Shared cLPurple As Color = System.Drawing.ColorTranslator.FromHtml("#8F2E89")

    Shared colorSet() As Color = {cDPurple, cBlue, cYellow, cGreen, cLPurple}

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        Try
            If Not IIf(Core.data_select_value("select bitAllowMySites from tblUsers where intUserPK = " & MySession.UserID) = "True", True, False) Then
                Response.Redirect(Core.data_select_value("select RawHomeURL from vwUsers where ID = " & MySession.UserID), True)
            End If

            If Not Page.IsPostBack = True Then
                Dim description As HtmlMeta = New HtmlMeta()
                description.Name = "description"
                description.Content = "UTILISYS - Gas Prices"
                Dim keywords As HtmlMeta = New HtmlMeta()
                keywords.Name = "keywords"
                keywords.Content = "UTILISYS - Gas Prices"
                Page.Header.Controls.Add(description)
                Page.Header.Controls.Add(keywords)


                Me.rblTimescales.Items.Clear()
                Me.rblTimescales.Items.Add(" 1 Week")
                Me.rblTimescales.Items.Add(" 1 Month")
                Me.rblTimescales.Items.Add(" 6 Months")
                Me.rblTimescales.Items.Add(" 1 Year")
                Me.rblTimescales.Items.Add(" 2 Years")

                Me.rblTimescales.SelectedIndex = 2

            End If

            If Request.QueryString("custid") Is Nothing Then
                Dim strSQL As String = "EXEC spPortal_TREEVIEW_TopLevel " & MySession.UserID
                Dim myConnection As New OleDb.OleDbConnection(Core.CurrentConnectionString)

                Dim myCommand As New OleDb.OleDbCommand(strSQL, myConnection)
                myCommand.Connection.Open()
                myCommand.CommandTimeout = 180
                Dim Reader As OleDb.OleDbDataReader = myCommand.ExecuteReader(CommandBehavior.CloseConnection)

                If Reader.HasRows Then
                    Reader.Read()
                    If Reader("Type") = "Company" Then
                        Response.Redirect("default.aspx?custid=" & Reader("id") & "&method=Customer", True)
                    Else
                        Response.Redirect("default.aspx?custid=" & Reader("id") & "&method=" & Reader("Type"), True)
                    End If
                    Reader.Close()
                End If


            End If

        Catch ex As Exception
            '
        End Try


        Dim dtmNow As DateTime = System.DateTime.Now.Date
        Dim dtmThen As DateTime = DateAdd(DateInterval.Month, -6, dtmNow)

        Dim dtmStart As String = dtmThen
        Dim dtmEnd As String = dtmNow

        Me.txtStart.Text = dtmStart
        Me.txtEnd.Text = dtmEnd
        Me.txtChartName.Text = "6 Months"

        GenerateGraph(Me.txtStart.Text, Me.txtEnd.Text, Me.txtChartName.Text)

        Me.Chart1.ChartAreas("Default").CursorX.UserEnabled = True
    End Sub
    Private Sub CleanChart()

        Chart1.Serializer.Reset()

        With Me.Chart1

            Dim series1 As Series
            For Each series1 In .Series
                .Annotations.Clear()
            Next

            .Visible = True
            .Series.Clear()
            .Titles.Clear()
            .Legends.Clear()
            .Legends.Add("Default")

            ' legend stuff
            .Legends(0).Docking = LegendDocking.Bottom
            .Legends(0).Alignment = StringAlignment.Far
            .Legends(0).Reversed = AutoBool.False
            .Legends(0).Position.Auto = True
            .Legends(0).CellColumns.Add(New LegendCellColumn("", LegendCellColumnType.SeriesSymbol, ""))
            .Legends(0).CellColumns.Add(New LegendCellColumn("", LegendCellColumnType.Text, "#LEGENDTEXT", Drawing.ContentAlignment.MiddleLeft))

            ' Zooming
            Dim chartarea1 As ChartArea
            For Each chartarea1 In .ChartAreas
                chartarea1.CursorX.UserEnabled = True
            Next

            ' Setup Frame
            .BorderColor = cDPurple
            .BorderStyle = ChartDashStyle.Solid
            .BorderWidth = 2
            .BorderSkin.SkinStyle = BorderSkinStyle.Emboss
            'Setup Toolbar
            .UI.Toolbar.Enabled = True
            .UI.Toolbar.Docking = ToolbarDocking.Top
            .UI.Toolbar.Placement = ToolbarPlacement.InsideChart
            .UI.Toolbar.BorderSkin.SkinStyle = BorderSkinStyle.Emboss
            .UI.Toolbar.BorderSkin.FrameBackColor = cDPurple

            Dim commands As CommandCollection = Chart1.UI.Commands

            Dim cmdProperties As Command = commands.FindCommand(ChartCommandType.Properties)
            Dim cmdPalette As Command = commands.FindCommand(ChartCommandType.SelectPaletteGroup)
            Dim cmdChartType As Command = commands.FindCommand(ChartCommandType.SelectChartGroup)
            Dim cmdToggle3D As Command = commands.FindCommand(ChartCommandType.Toggle3D)

            cmdProperties.Visible = False
            cmdPalette.Visible = False
            cmdChartType.Visible = False
            cmdToggle3D.Visible = False

            Me.Chart1.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFFFFF") ' Corp Green
        End With

        Chart1.ChartAreas.Add("Default")
        'Chart1.Series.Add(0)
        Me.Chart1.Width = "650"
        Me.Chart1.Height = "450"

        Me.Chart1.ChartAreas(0).CursorX.UserEnabled = True

    End Sub
    Private Sub GenerateGraph(ByVal dtmStart As DateTime, ByVal dtmEnd As DateTime, ByVal strChartName As String)

        CleanChart()

        Me.lblLatestPrice.Text = Core.data_select_value("EXEC [portal.utilitymasters.co.uk].dbo.usp_MyReports_MarketInfo_GasPrices_LatestPrice '" & dtmStart & "', '" & DateAdd(DateInterval.Day, 1, dtmEnd) & "'")


        Chart1.Series.Clear()
        Chart1.Titles.Clear()
        Chart1.Legends.Clear()
        Chart1.Legends.Add("Default")


        Dim strSQL As String = "EXEC [portal.utilitymasters.co.uk].dbo.usp_MyReports_MarketInfo_GasPrices '" & dtmStart & "', '" & DateAdd(DateInterval.Day, 1, dtmEnd) & "'"

        Dim intMaxPrice As Integer = Core.data_select_value("EXEC [portal.utilitymasters.co.uk].dbo.usp_MyReports_MarketInfo_GasPrices_MaxPrice '" & dtmStart & "', '" & DateAdd(DateInterval.Day, 1, dtmEnd) & "'")

        Using connection As New SqlConnection(conn)
            connection.Open()
            Dim command As New SqlCommand(strSQL, connection)
            command.CommandType = CommandType.Text
            Dim reader As SqlDataReader = command.ExecuteReader()
            If reader.HasRows Then
                With Chart1
                    ' house keeping 
                    .Series.Clear()
                    .Titles.Clear()

                    ' data stuff
                    .DataBindCrossTab(reader, "varPeriod", "dtmDate", "decPrice", "")

                    ' legend stuff
                    .Legends(0).Docking = LegendDocking.Bottom
                    .Legends(0).Alignment = StringAlignment.Far
                    .Legends(0).Reversed = AutoBool.False
                    .Legends(0).Position.Auto = True
                    .Legends(0).CellColumns.Add(New LegendCellColumn("", LegendCellColumnType.SeriesSymbol, ""))
                    .Legends(0).CellColumns.Add(New LegendCellColumn("", LegendCellColumnType.Text, "#LEGENDTEXT", Drawing.ContentAlignment.MiddleLeft))
                    ' titles and axis format
                    .ChartAreas(0).AxisY.Interval = intMaxPrice * 0.25
                    .Series(0)("PointWidth") = "0.8"
                    .ChartAreas(0).AxisX.Margin = False

                    .ChartAreas(0).AxisY.Title = "Cost (p / therm)"
                    .ChartAreas(0).AxisY.LabelStyle.Format = " #,###"
                    .ChartAreas(0).AxisX.Title = "Date"

                    .Series(0)("EmptyPointValue") = "Zero"
                    .PaletteCustomColors = colorSet
                    .ChartAreas(0).BackColor = Color.Silver

                    .Series(0).XValueType = ChartValueTypes.Date


                End With
            End If
        End Using

        ' stacks the colum
        Dim series As Series
        For Each series In Me.Chart1.Series
            series.Type = SeriesChartType.Line

        Next


        Dim bShowTotals As Boolean = False
        bShowTotals = False

        With Me.Chart1

            .BorderColor = Color.Black
            .BorderStyle = ChartDashStyle.Solid
            .BorderWidth = 2
            .BorderSkin.SkinStyle = BorderSkinStyle.Emboss

            .UI.Toolbar.Enabled = True
            .UI.Toolbar.Docking = ToolbarDocking.Top
            .UI.Toolbar.Placement = ToolbarPlacement.InsideChart
            .UI.Toolbar.BorderSkin.SkinStyle = BorderSkinStyle.Emboss
            .UI.Toolbar.BorderSkin.FrameBackColor = Color.Black
            .Titles.Add(1).Text = "Gas Prices - Last " & strChartName

            ' Setup Frame
            .BorderColor = cDPurple
            .BorderStyle = ChartDashStyle.Solid
            .BorderWidth = 2
            .BorderSkin.SkinStyle = BorderSkinStyle.Emboss
            'Setup Toolbar
            .UI.Toolbar.Enabled = True
            .UI.Toolbar.Docking = ToolbarDocking.Top
            .UI.Toolbar.Placement = ToolbarPlacement.InsideChart
            .UI.Toolbar.BorderSkin.SkinStyle = BorderSkinStyle.Emboss
            .UI.Toolbar.BorderSkin.FrameBackColor = cDPurple

            Dim commands As CommandCollection = Chart1.UI.Commands

            Dim cmdProperties As Command = commands.FindCommand(ChartCommandType.Properties)
            Dim cmdPalette As Command = commands.FindCommand(ChartCommandType.SelectPaletteGroup)
            Dim cmdChartType As Command = commands.FindCommand(ChartCommandType.SelectChartGroup)
            Dim cmdToggle3D As Command = commands.FindCommand(ChartCommandType.Toggle3D)

            cmdProperties.Visible = False
            cmdPalette.Visible = False
            cmdChartType.Visible = False
            cmdToggle3D.Visible = False

            Me.Chart1.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFFFFF") ' Corp Green
        End With



    End Sub
    Public Overloads Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)

        ' Confirms that an HtmlForm control is rendered for the specified ASP.NET server control at run time.

    End Sub
    Protected Sub rblTimescales_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rblTimescales.SelectedIndexChanged
        If Me.rblTimescales.SelectedIndex = 0 Then ' week

            Me.txtStart.Text = DateAdd(DateInterval.Day, -7, System.DateTime.Now.Date)
            Me.txtEnd.Text = System.DateTime.Now.Date
            Me.txtChartName.Text = "1 Week"

        ElseIf Me.rblTimescales.SelectedIndex = 1 Then ' month

            Me.txtStart.Text = DateAdd(DateInterval.Month, -1, System.DateTime.Now.Date)
            Me.txtEnd.Text = System.DateTime.Now.Date
            Me.txtChartName.Text = "1 Month"

        ElseIf Me.rblTimescales.SelectedIndex = 2 Then ' 6 month

            Me.txtStart.Text = DateAdd(DateInterval.Month, -6, System.DateTime.Now.Date)
            Me.txtEnd.Text = System.DateTime.Now.Date
            Me.txtChartName.Text = "6 Months"

        ElseIf Me.rblTimescales.SelectedIndex = 3 Then ' 1 year

            Me.txtStart.Text = DateAdd(DateInterval.Year, -1, System.DateTime.Now.Date)
            Me.txtEnd.Text = System.DateTime.Now.Date
            Me.txtChartName.Text = "1 Year"

        ElseIf Me.rblTimescales.SelectedIndex = 4 Then ' 2 years

            Me.txtStart.Text = DateAdd(DateInterval.Year, -2, System.DateTime.Now.Date)
            Me.txtEnd.Text = System.DateTime.Now.Date
            Me.txtChartName.Text = "2 Years"

        End If

        GenerateGraph(Me.txtStart.Text, Me.txtEnd.Text, Me.txtChartName.Text)

    End Sub
End Class
