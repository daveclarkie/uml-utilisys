Imports Dundas.Charting.WebControl
Imports System.Drawing

Partial Class reports_onlinegraph_Default
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try

            If Request.QueryString("custid") Is Nothing Then
                Dim strSQL As String = "EXEC spPortal_TREEVIEW_TopLevel " & MySession.UserID
                Dim myConnection As New OleDb.OleDbConnection(Core.CurrentConnectionString)

                Dim myCommand As New OleDb.OleDbCommand(strSQL, myConnection)
                myCommand.Connection.Open()
                myCommand.CommandTimeout = 180
                Dim Reader As OleDb.OleDbDataReader = myCommand.ExecuteReader(CommandBehavior.CloseConnection)

                If Reader.HasRows Then
                    Reader.Read()
                    If Reader("Type") = "Company" Then
                        Response.Redirect("default.aspx?custid=" & Reader("id") & "&method=Customer", True)
                    Else
                        Response.Redirect("default.aspx?custid=" & Reader("id") & "&method=" & Reader("Type"), True)
                    End If
                    Reader.Close()
                End If
            End If

            If Not IIf(Core.data_select_value("select bitAllowMySites from tblUsers where intUserPK = " & MySession.UserID) = "True", True, False) Then
                Response.Redirect(Core.data_select_value("select RawHomeURL from vwUsers where ID = " & MySession.UserID), True)
            End If

            If Not Page.IsPostBack = True Then

                Dim description As HtmlMeta = New HtmlMeta()
                description.Name = "description"
                description.Content = "Utility Masters customer extranet - CRC Reports"
                Dim keywords As HtmlMeta = New HtmlMeta()
                keywords.Name = "keywords"
                keywords.Content = "CRC Reports"
                Page.Header.Controls.Add(description)
                Page.Header.Controls.Add(keywords)

                PopulateDropDowns()

            End If

            ThickBox()

            Me.lblDescription.Text = Request.QueryString("method").ToString
            Me.lblName.Text = Core.data_select_value("EXEC spSites_MAP_CurentSelection " & MySession.UserID & ", '" & Request.QueryString("method") & "', " & Request.QueryString("custid"))

        Catch ex As Exception
            '
        End Try

    End Sub

    Private Sub PopulateDropDowns()
        With Me.ddlFinancialYear
            .Items.Clear()
            .DataSource = Core.data_select("EXEC uml_extdata.dbo.usp_portal_CarbonReports_CRC_ReturnDateList")
            .DataValueField = "fiscal_year"
            .DataTextField = "nice_fiscal_year"
            .DataBind()
        End With

        With Me.ddlQuarter
            .Items.Clear()
            .DataSource = Core.data_select("EXEC uml_extdata.dbo.usp_portal_CarbonReports_CRC_ReturnQuarterList '01/01/1900'")
            .DataValueField = "Fiscal_Quarter"
            .DataTextField = "Nice_Fiscal_Quarter"
            .DataBind()
        End With

    End Sub

    Public Sub ThickBox()


        Me.lnkExecSummary.NavigateUrl = ""
        Me.lnkExecSummary.Text = ""
        Me.lnkExecSummary.CssClass = ""
        Me.lnkExecSummary.ToolTip = ""
        Me.lnkExecSummary.ImageUrl = "../../../assets/buttons-reports-carbon-crcquarterly-executivesummary-disabled.gif"

        Me.lnkCarbonChart.NavigateUrl = ""
        Me.lnkCarbonChart.Text = ""
        Me.lnkCarbonChart.CssClass = ""
        Me.lnkCarbonChart.ToolTip = ""
        Me.lnkCarbonChart.ImageUrl = "../../../assets/buttons-reports-carbon-crcquarterly-carbonchart-disabled.gif"

        Me.lnkCarbonAccount.NavigateUrl = ""
        Me.lnkCarbonAccount.Text = ""
        Me.lnkCarbonAccount.CssClass = ""
        Me.lnkCarbonAccount.ToolTip = ""
        Me.lnkCarbonAccount.ImageUrl = "../../../assets/buttons-reports-carbon-crcquarterly-carbonaccount-disabled.gif"

        Me.lnkEUETSPrice.NavigateUrl = ""
        Me.lnkEUETSPrice.Text = ""
        Me.lnkEUETSPrice.CssClass = ""
        Me.lnkEUETSPrice.ToolTip = ""
        Me.lnkEUETSPrice.ImageUrl = "../../../assets/buttons-reports-carbon-crcquarterly-euetschartprices-disabled.gif"

        If Me.ddlQuarter.Items.Count > 1 Then
            If Me.ddlQuarter.SelectedItem.Value > "01/01/1900" Then

                Me.lnkExecSummary.NavigateUrl = "executivesummary.aspx?dtmCRCPeriod=" + Me.ddlFinancialYear.SelectedItem.Value + "&dtmReportPeriod=" + Me.ddlQuarter.SelectedItem.Value + "&intCustomerFK=" + CStr(Request.QueryString("custid")) + "&?keepThis=true&TB_iframe=true&height=550&width=850"
                Me.lnkExecSummary.Text = "View Report"
                Me.lnkExecSummary.ImageUrl = "../../../assets/buttons-reports-carbon-crcquarterly-executivesummary.gif"
                Me.lnkExecSummary.CssClass = "thickbox"
                Me.lnkExecSummary.ToolTip = "Executive Summary"
                Me.lnkExecSummary.Visible = True

                Me.lnkCarbonChart.NavigateUrl = "carbonchart.aspx?dtmStartDate=" + Me.ddlFinancialYear.SelectedItem.Value + "&strMethod=" + Request.QueryString("method").ToString + "&intCustomerFK=" + CStr(Request.QueryString("custid")) + "&?keepThis=true&TB_iframe=true&height=550&width=850"
                Me.lnkCarbonChart.Text = "View Report"
                Me.lnkCarbonChart.ImageUrl = "../../../assets/buttons-reports-carbon-crcquarterly-carbonchart.gif"
                Me.lnkCarbonChart.CssClass = "thickbox"
                Me.lnkCarbonChart.ToolTip = "Carbon Chart"
                Me.lnkCarbonChart.Visible = True

                Me.lnkCarbonAccount.NavigateUrl = "carbonaccount.aspx?dtmCRCPeriod=" + Me.ddlFinancialYear.SelectedItem.Value + "&dtmReportPeriod=" + Me.ddlQuarter.SelectedItem.Value + "&intCustomerFK=" + CStr(Request.QueryString("custid")) + "&?keepThis=true&TB_iframe=true&height=550&width=850"
                Me.lnkCarbonAccount.Text = "View Report"
                Me.lnkCarbonAccount.ImageUrl = "../../../assets/buttons-reports-carbon-crcquarterly-carbonaccount.gif"
                Me.lnkCarbonAccount.CssClass = "thickbox"
                Me.lnkCarbonAccount.ToolTip = "Carbon Account"
                Me.lnkCarbonAccount.Visible = True

                Me.lnkEUETSPrice.NavigateUrl = "../../marketinformation/euetsprices/report.aspx?dtmCRCPeriod=" + Me.ddlFinancialYear.SelectedItem.Value + "&dtmReportPeriod=" + Me.ddlQuarter.SelectedItem.Value + "&intCustomerFK=" + CStr(Request.QueryString("custid")) + "&?keepThis=true&TB_iframe=true&height=550&width=850"
                Me.lnkEUETSPrice.Text = "View Report"
                Me.lnkEUETSPrice.ImageUrl = "../../../assets/buttons-reports-carbon-crcquarterly-euetschartprices.gif"
                Me.lnkEUETSPrice.CssClass = "thickbox"
                Me.lnkEUETSPrice.ToolTip = "EU ETS Prices"
                Me.lnkEUETSPrice.Visible = True

            End If
        End If


    End Sub
    Public Overloads Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)

        ' Confirms that an HtmlForm control is rendered for the specified ASP.NET server control at run time.

    End Sub


    Protected Sub ddlFinancialYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlFinancialYear.SelectedIndexChanged

        With Me.ddlQuarter
            .Items.Clear()
            .DataSource = Core.data_select("EXEC uml_extdata.dbo.usp_portal_CarbonReports_CRC_ReturnQuarterList '" & Me.ddlFinancialYear.SelectedItem.Value & "'")
            .DataValueField = "Fiscal_Quarter"
            .DataTextField = "Nice_Fiscal_Quarter"
            .DataBind()
        End With
        ThickBox()

    End Sub
End Class
