﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="CarbonChart.aspx.vb" Inherits="CarbonReports_CRC_CarbonChart" %>

<%@ Register Assembly="DundasWebChart" Namespace="Dundas.Charting.WebControl" TagPrefix="DCWC" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Carbon Chart</title>
    <link rel="stylesheet" href="../../../modalbox/modalreport.css" type="text/css" media="screen" />   

</head>
<body>
    <form id="form1" runat="server">

<table>
    <tr>
        <td style="vertical-align:top;">
            <table style="width:350px;">
                <tr>
                    <td ID="table1" runat="server" >
                        
                    </td>
                </tr>
                <tr>
                    <td ID="table2" runat="server" >
                        
                    </td>
                </tr>
            </table>
        </td>
        <td style="width:10px;">
        
        </td>
        <td style="vertical-align:top;">
            <table style="width:430px;">
                <tr>
                    <td ID="table3" runat="server">
                        
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table> <%--tables--%>
<table>
    <tr>
        <td>
            <table>
                <tr>
                    <td>
                        <DCWC:Chart ID="Chart1" runat="server" Width="837px" Height="400px">
                            <Legends>
                                <DCWC:Legend Name="Default"></DCWC:Legend>
                            </Legends>
                            <Series>
                                <DCWC:Series Name="Default">
                                </DCWC:Series>
                            </Series>
                            <ChartAreas>
                                <DCWC:ChartArea Name="Default">
                                </DCWC:ChartArea>
                            </ChartAreas>
                        </DCWC:Chart>
                    </td>
                </tr>
            </table> <%--Charts--%>
        </td>
    </tr>
</table> <%--charts--%>


  
       


    </form>
</body>
</html>
