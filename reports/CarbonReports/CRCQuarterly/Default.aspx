<%@ Page Language="VB" MasterPageFile="~/landing.master" AutoEventWireup="false" CodeFile="Default.aspx.vb" Inherits="reports_onlinegraph_Default" title="Daily Consumption and Cost" %>

<%@ Register Assembly="DundasWebChart" Namespace="Dundas.Charting.WebControl" TagPrefix="DCWC" %>
<asp:Content ID="Content1" ContentPlaceHolderID="content_right" Runat="Server">

        <script type="text/javascript" src="../../../ThickBox/jquery-1.3.2.min.js"></script>
        <script type="text/javascript" src="../../../ThickBox/thickbox.js"></script>
        <link rel="stylesheet" href="../../../css/layout.css" type="text/css" media="screen" />  
        <link rel="stylesheet" href="../../../ThickBox/thickbox.css" type="text/css" media="screen" />   
        
<form id="Form1" runat="server">

    <div class="content_back general">
            <table>
            
                <tr valign="middle">
                    <td width="150px" >
                        <asp:Label ID="lblDescription" Text="Description" runat="server"></asp:Label>
                    </td>
                    <td width="400px" align="left">
                        <asp:Label ID="lblName" Text="Name" runat="server"></asp:Label>
                    </td>
                    <td width="50px">
                        
                    </td>
                </tr>
                
                <tr valign="middle">
                    <td width="150px" >
                        Financial Year
                    </td>
                    <td width="400px" align="left">
                        <asp:DropDownList ID="ddlFinancialYear" runat="server" AutoPostBack="true"></asp:DropDownList>
                    </td>
                    <td width="50px">
                        
                    </td>
                </tr>
                
                <tr valign="middle">
                    <td width="150px" >
                        Quarter
                    </td>
                    <td width="400px" align="left">
                        <asp:DropDownList ID="ddlQuarter" runat="server" AutoPostBack="true"></asp:DropDownList>
                    </td>
                    <td width="50px">
                        
                    </td>
                </tr>
                                
                <tr>
                    <td style="height:5px;">
                    </td>
                </tr>
                
                <tr>
                    <td width="600px" align="left" colspan="3">
                        <table>
                            <tr>
                                <td>
                                    <asp:HyperLink  ID="lnkExecSummary" runat="server" CssClass="thickbox" ></asp:HyperLink>
                                </td>
                                <td style="width:20px;"></td>
                                <td>
                                    <asp:HyperLink  ID="lnkCarbonAccount" runat="server" CssClass="thickbox" ></asp:HyperLink>
                                </td>
                                <td style="width:20px;"></td>
                                <td>
                                     <asp:HyperLink  ID="lnkCarbonChart" runat="server" CssClass="thickbox" ></asp:HyperLink>
                                </td>
                            </tr> <tr>
                                <td>
                                    <asp:HyperLink  ID="lnkEUETSPrice" runat="server" CssClass="thickbox" ></asp:HyperLink>
                                </td>
                                <td style="width:20px;"></td>
                                <td>
                                    
                                </td>
                                <td style="width:20px;"></td>
                                <td>
                                    
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                
                <tr>
                    <td width="150px" >
                    
                    </td>
                    <td width="400px" align="left">
                        
                    </td>
                    <td width="50px">
                        
                    </td>
                </tr>
                
            </table>
</div>

        <asp:Panel id="pnlMenuCompareMeters" style="width:660px; height:500px; border:solid 1px black;Z-INDEX: 125; LEFT:26%; POSITION: absolute; TOP: 57px; background-color:#D3D3D3;" runat="server" visible="false">
        <!-- This is the content -->
	        <div>
	            <table style="width:660px;" cellpadding="0" cellspacing="0" >
	                <tr>
	                    <td>
    	                
	                    </td>
	                    <td colspan="3">
	                        <table cellspacing="0" cellpadding="0"> 
	                            <tr>
	                                <td>
	                                    <asp:LinkButton ID="btnShowAll" CssClass="btnMeters_ShowAll" runat="server" Text="All"></asp:LinkButton>
	                                </td>
	                                <td>
	                                    <asp:LinkButton ID="btnShowMPAN" CssClass="btnMeters_ShowMPAN" runat="server" Text="MPAN"></asp:LinkButton>
	                                </td>
	                                <td>
	                                    <asp:LinkButton ID="btnShowLoggers" CssClass="btnMeters_ShowLoggers" runat="server" Text="Loggers"></asp:LinkButton>
	                                </td>
	                                <td>
	                                    <asp:LinkButton ID="btnShowVirtuals" CssClass="btnMeters_ShowVirtuals" runat="server" Text="Virtuals"></asp:LinkButton>
	                                </td>
	                                <td>
	                                    <asp:textbox ID="txtFilterType" runat="server" Text="All" Visible="false"></asp:textbox>
	                                </td>
	                            </tr>
	                        </table>
	                        <%--
	                        <asp:ListBox ID="lbMeterPoints" runat="server" style="width:640px; height:150px;" SelectionMode="Multiple" AutoPostBack="true"> 
	                        </asp:ListBox>--%>
	                    </td>
	                </tr>
	                
	                <tr>
	                    <td style="width:2px;">
    	                
	                    </td>
	                    <td style="width:290px;">
	                        <asp:ListBox ID="lbAvailableMeters" runat="server" style="width:290px; height:350px;" SelectionMode="Multiple" AutoPostBack="true">
	                        </asp:ListBox>
	                    </td>
	                    
	                    <td style="width:50px;">
	                        <table cellpadding="0" cellspacing="0" style="width:50px;">
	                            <tr>
	                                <td>
	                                    <a style="color:Silver;">.</a>
	                                    <asp:LinkButton ID="btnAddRight" CssClass="btnAddRight" runat="server" Text="Add" Width="50px"></asp:LinkButton>
	                                </td>
	                            </tr>
	                            <tr>
	                                <td>
	                                    <a style="color:Silver;">.</a>
	                                    <asp:LinkButton ID="btnAddLeft" CssClass="btnAddLeft" runat="server" Text="Remove" Width="50px"></asp:LinkButton>
	                                </td>
	                            </tr>
	                            
	                        </table>
	                    </td>
	                    
	                    <td style="width:290px;">
	                        <asp:ListBox ID="lbAssignedMeters" runat="server" style="width:290px; height:350px;" SelectionMode="Multiple" AutoPostBack="true">
	                        </asp:ListBox>
	                    </td>
	                </tr>
	                
	               <tr>
	                    <td></td>
	                    <td></td>
	                    <td></td>
	                    <td colspan="2">
	                        <asp:LinkButton ID="btnRemoveAll" CssClass="btnRemoveAllMeters" runat="server" Text="Remove All" ></asp:LinkButton>
	                    </td>
	                </tr> 
	                
	               <tr>
	                    <td></td>
	                    <td>
	                        <asp:Label ID="lblAvailableMetersCount" Text="there are 0 meters available" runat="server"></asp:Label>
	                    </td>
	                    <td></td>
	                    <td colspan="2">
	                        <asp:Label ID="lblSelectedMetersCount" Text="there are 0 meters selected" runat="server"></asp:Label>
	                    </td>
	                </tr>                
	                
	               <tr>
	                    <td colspan="3">
	                       
	                    </td>
	                    <td colspan="2">
	                        <asp:Label ID="lblAssignedMeterError" Text="" runat="server" ForeColor="Red"></asp:Label>
	                    </td>
	                </tr>              
	                
	               <tr>
	                    <td></td>
	                    <td colspan="3">
	                         <asp:LinkButton ID="btnCloseMeter" CssClass="btnClose" runat="server" Text="Close"></asp:LinkButton>
	                    </td>
	                </tr>
	            </table>
	        </div>
	        <!-- End content -->
        </asp:Panel>
  
</form>
</asp:Content>

