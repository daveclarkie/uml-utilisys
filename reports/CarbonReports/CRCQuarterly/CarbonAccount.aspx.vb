﻿Imports Dundas.Charting.WebControl
Imports Dundas.Charting.WebControl.Utilities

Imports System.Data.SqlClient
Imports System.Data
Imports System.Drawing
Imports System.Windows


Partial Class CarbonReports_CRC_CarbonAccount
    Inherits System.Web.UI.Page

    Public conn As String = Core.ConnectionString()

    Shared cGreen As Color = System.Drawing.ColorTranslator.FromHtml("#6EBB1F")
    Shared cBlue As Color = System.Drawing.ColorTranslator.FromHtml("#082B61")
    Shared cYellow As Color = System.Drawing.ColorTranslator.FromHtml("#FFFFC0")

    Shared colorSet() As Color = {cBlue, cGreen, cYellow, Color.Purple}

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim intCustomerFK As Integer = Request.QueryString("intCustomerFK")
        Dim dtmCRCPeriod As DateTime = Request.QueryString("dtmCRCPeriod")
        Dim dtmReportPeriod As DateTime = Request.QueryString("dtmReportPeriod")

        GenerateTable(intCustomerFK, dtmCRCPeriod, dtmReportPeriod)

    End Sub

    Private Sub GenerateTable(ByVal intCustomerFK As Integer, ByVal dtmCRCPeriod As DateTime, ByVal dtmReportPeriod As DateTime)

        Dim strSQL As String = "EXEC uml_extdata.dbo.usp_portal_CarbonReports_CRC_CarbonAccountStatement " & intCustomerFK & ", '" & dtmCRCPeriod & "', '" & dtmReportPeriod & "'"

        Dim xRowCount As Integer = 0

        Using connection As New SqlConnection(conn)
            connection.Open()
            Dim command As New SqlCommand(strSQL, connection)
            command.CommandType = CommandType.Text
            Dim reader As SqlDataReader = command.ExecuteReader()

            With Me.table1

                .InnerHtml = "<table style='' cellpadding='0' cellspacing='0'>"

                .InnerHtml &= "<tr>"
                .InnerHtml &= "<td style='background-color:#082B61;color:#FFFFFF;width:80px; font-size:10pt;color:#6EBB1F;'>Date</td>"
                .InnerHtml &= "<td style='text-align:left;background-color:#082B61;color:#FFFFFF;width:150px; font-size:10pt;'>Account Number</td>"
                .InnerHtml &= "<td style='text-align:left;background-color:#082B61;color:#FFFFFF;width:250px; font-size:10pt;'>Description</td>"
                .InnerHtml &= "<td style='text-align:right;background-color:#082B61;color:#FFFFFF;width:80px; font-size:10pt;'>(£) Price</td>"
                .InnerHtml &= "<td style='text-align:right;background-color:#082B61;color:#FFFFFF;width:80px; font-size:10pt;'>Credit</td>"
                .InnerHtml &= "<td style='text-align:right;background-color:#082B61;color:#FFFFFF;width:80px; font-size:10pt;'>Debit</td>"
                .InnerHtml &= "<td style='text-align:right;background-color:#082B61;color:#FFFFFF;width:100px; font-size:10pt;'>Balance</td>"
                .InnerHtml &= "</tr>"


                While reader.Read

                    If xRowCount Mod 2 = 0 Then
                        .InnerHtml &= "<tr style='background-color:#FFFFFF;'>"
                    Else
                        .InnerHtml &= "<tr style='background-color:#FFFFC0;'>"
                    End If


                    .InnerHtml &= "<td >" & reader(0) & "</td>"
                    .InnerHtml &= "<td style='text-align:left; font-size:10pt;'>" & reader(1) & "</td>"
                    .InnerHtml &= "<td style='text-align:left; font-size:10pt;'>" & reader(2) & "</td>"
                    .InnerHtml &= "<td style='text-align:right; font-size:10pt;'>" & reader(3) & "</td>"
                    .InnerHtml &= "<td style='text-align:right; font-size:10pt;'>" & reader(4) & "</td>"
                    .InnerHtml &= "<td style='text-align:right; font-size:10pt;'>" & reader(5) & "</td>"
                    .InnerHtml &= "<td style='text-align:right; font-size:10pt;'>" & reader(6) & "</td>"
                    .InnerHtml &= "</tr>"

                    xRowCount += 1

                End While

            End With
            connection.Close()

        End Using

        Using connection2 As New SqlConnection(conn)


            Dim strSQL2 As String = "EXEC uml_extdata.dbo.usp_portal_CarbonReports_CRC_CarbonAccountStatement_Balance " & intCustomerFK & ", '" & dtmCRCPeriod & "', '" & dtmReportPeriod & "'"

            connection2.Open()
            Dim command2 As New SqlCommand(strSQL2, connection2)
            command2.CommandType = CommandType.Text
            Dim reader2 As SqlDataReader = command2.ExecuteReader()

            With Me.table1
                While reader2.Read

                    If xRowCount Mod 2 = 0 Then
                        .InnerHtml &= "<tr style='background-color:#FFFFFF;height:15px;'>"
                        .InnerHtml &= "<td colspan='7' style='Color:#FFFFFF;'>1</td>"
                        .InnerHtml &= "</tr>"
                    Else
                        .InnerHtml &= "<tr style='background-color:#FFFFC0;height:15px;'>"
                        .InnerHtml &= "<td colspan='7' style='Color:#FFFFC0;'>2</td>"
                        .InnerHtml &= "</tr>"
                    End If

                    .InnerHtml &= "<tr style='background-color:#082B61;'>"
                    .InnerHtml &= "<td ></td>"
                    .InnerHtml &= "<td style='text-align:center; font-size:10pt;'></td>"
                    .InnerHtml &= "<td style='text-align:right; font-size:10pt;'></td>"
                    .InnerHtml &= "<td style='text-align:right; font-size:10pt;'></td>"
                    .InnerHtml &= "<td style='text-align:right; font-size:10pt; color:#FFFFFF;' colspan='2'><b>Available Balance</b></td>"
                    .InnerHtml &= "<td style='text-align:right; font-size:10pt; color:#FFFFFF;'><b>" & reader2(0) & "</b></td>"
                    .InnerHtml &= "</tr>"

                End While

                .InnerHtml &= "</table>"

            End With

        End Using

    End Sub

End Class
