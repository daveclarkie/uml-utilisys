﻿Imports Dundas.Charting.WebControl
Imports Dundas.Charting.WebControl.Utilities

Imports System.Data.SqlClient
Imports System.Data
Imports System.Drawing
Imports System.Windows


Partial Class CarbonReports_CRC_CarbonChart
    Inherits System.Web.UI.Page

    Public conn As String = Core.ConnectionString()

    Shared cGreen As Color = System.Drawing.ColorTranslator.FromHtml("#6EBB1F")
    Shared cBlue As Color = System.Drawing.ColorTranslator.FromHtml("#082B61")
    Shared cYellow As Color = System.Drawing.ColorTranslator.FromHtml("#FFFFC0")

    Shared colorSet() As Color = {cBlue, cGreen, cYellow, Color.Purple}

    Private Sub GenerateGraph1(ByVal UserID As Integer, ByVal StartDate As DateTime, ByVal Method As String, ByVal CustomerFK As Integer)

        Dim strSQL As String = "EXEC uml_extdata.dbo.usp_portal_CarbonReports_CRC_CarbonChart " & UserID & ", '" & StartDate & "', '" & Method & "', " & CustomerFK

        With Chart1

            .Visible = True
            .Series.Clear()
            .Titles.Clear()

            .DataSource = Core.data_select(strSQL)
            .DataBind()
            .Series.Add("forecast")
            .Series(0).ValueMemberX = "dtmMonth"
            .Series(0).ValueMembersY = "forecast"
            .Series(0).Type = Dundas.Charting.WebControl.SeriesChartType.Column

            .Series.Add("co2")
            .Series(1).ValueMemberX = "dtmMonth"
            .Series(1).ValueMembersY = "co2"
            .Series(1).Type = Dundas.Charting.WebControl.SeriesChartType.Column

            .ChartAreas(0).AxisY.IntervalAutoMode = IntervalAutoMode.VariableCount
            .ChartAreas(0).AxisY.MajorGrid.LineStyle = ChartDashStyle.Dot
            .ChartAreas(0).AxisX.MajorGrid.LineStyle = ChartDashStyle.Dot

            .Legends(0).Enabled = True
            .Legends(0).CellColumns.Add(New LegendCellColumn("Sym", LegendCellColumnType.SeriesSymbol, ""))
            .Legends(0).CellColumns.Add(New LegendCellColumn("Name", LegendCellColumnType.Text, "#LEGENDTEXT", Drawing.ContentAlignment.MiddleLeft))

            .Titles.Add("")
            .ChartAreas(0).AxisY.Title = "Carbon Tonnes Total"
            .ChartAreas(0).AxisY.LabelStyle.Format = "#,###"
            .ChartAreas(0).AxisX.Title = "Date"
            .ChartAreas(0).AxisX.Margin = False

            .PaletteCustomColors = colorSet
            .ChartAreas(0).BackColor = Color.Silver

            .BorderColor = Color.Black
            .BorderStyle = ChartDashStyle.Solid
            .BorderWidth = 2
            .BorderSkin.SkinStyle = BorderSkinStyle.Emboss

            .UI.Toolbar.Enabled = True
            .UI.Toolbar.Docking = ToolbarDocking.Top
            .UI.Toolbar.Placement = ToolbarPlacement.InsideChart
            .UI.Toolbar.BorderSkin.SkinStyle = BorderSkinStyle.Emboss
            .UI.Toolbar.BorderSkin.FrameBackColor = Color.Black
            .Titles.Add(1).Text = "Carbon Chart"

        End With

        Dim commands As CommandCollection = Chart1.UI.Commands

        Dim cmdProperties As Command = commands.FindCommand(ChartCommandType.Properties)
        Dim cmdPalette As Command = commands.FindCommand(ChartCommandType.SelectPaletteGroup)
        Dim cmdChartType As Command = commands.FindCommand(ChartCommandType.SelectChartGroup)
        Dim cmdToggle3D As Command = commands.FindCommand(ChartCommandType.Toggle3D)

        cmdProperties.Visible = False
        cmdPalette.Visible = False
        cmdChartType.Visible = False
        cmdToggle3D.Visible = False

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim intUserID As Integer = MySession.UserID
        Dim dtmStartDate As DateTime = Request.QueryString("dtmStartDate")
        Dim strMethod As String = Request.QueryString("strMethod")
        Dim intCustomerFK As Integer = Request.QueryString("intCustomerFK")

        GenerateGraph1(intUserID, dtmStartDate, strMethod, intCustomerFK)

    End Sub


End Class
