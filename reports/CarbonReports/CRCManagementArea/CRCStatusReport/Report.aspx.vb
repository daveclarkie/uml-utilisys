﻿
Partial Class reports_CarbonReports_CRCManagementArea_CRCStatusReport_Report
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim myURL As String = Request.Url.ToString()
        Dim ParamStart As Integer = myURL.IndexOf("?")


        Dim ReturnedID As Integer = Trim(Request.QueryString("id"))
        Dim ReturnedNet As String = "true"
        Dim ReturnedPhase As String = Trim(Request.QueryString("phase"))

        Me.frmReport.Visible = True

        'Set the processing mode for the ReportViewer to Remote
        myReportViewer.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Remote

        Dim serverReport As Microsoft.Reporting.WebForms.ServerReport
        serverReport = myReportViewer.ServerReport

        serverReport.ReportServerCredentials = New clsReportServerCredentials("administrator", "chicago", "uml")

        'Set the report server URL and report path
        serverReport.ReportServerUrl = _
            New Uri("http://10.44.5.205/reportserver")
        serverReport.ReportPath = "/Operational Reports/Energy Management/CRC Quarterly Consumption Report"

        Dim CRCID As New Microsoft.Reporting.WebForms.ReportParameter()
        CRCID.Name = "CRCID"
        CRCID.Values.Add(ReturnedID)

        Dim FootPrintID As New Microsoft.Reporting.WebForms.ReportParameter()
        FootPrintID.Name = "FootPrintID"
        FootPrintID.Values.Add(ReturnedPhase)

        'Set the report parameters for the report
        Dim parameters() As Microsoft.Reporting.WebForms.ReportParameter = {CRCID, FootPrintID}
        serverReport.SetParameters(parameters)
    End Sub
End Class
