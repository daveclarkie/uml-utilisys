﻿
Partial Class reports_CarbonReports_CRCManagementArea_CRCStatusReport_Default
    Inherits System.Web.UI.Page




    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        '
        Call LoadTreeview_Report()

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim myURL As String = Request.Url.ToString()
        Dim ParamStart As Integer = myURL.IndexOf("?")


        Dim ReturnedID As Integer = Trim(Request.QueryString("id"))
        Dim ReturnedNet As String = "true"
        Dim ReturnedPhase As String = Trim(Request.QueryString("phase"))

        'position:absolute;top:50px;left:5px;height:450px;width:660px; overflow:auto; background: transparent;border:solid 1px lightgray;
        Me.linkBack.NavigateUrl = "~/reports/carbonreports/crcmanagementarea/CRCStatusReport/Default.aspx?id=" & ReturnedID
        Me.linkBack2.NavigateUrl = "~/reports/carbonreports/crcmanagementarea/default.aspx"

        If ReturnedPhase = "" Then
            Me.txtSelectedPhase.Text = Nothing
        Else
            Me.txtSelectedPhase.Text = ReturnedPhase
        End If

        If Me.txtSelectedPhase.Text.Length = 0 Then
            Me.navgeneral.Visible = True
            'Me.TopLabel.Visible = True
            Me.frmReport.Visible = False
            Me.Options.Text = "Please select a valid phase"
            Me.lblSelectedPhase.Text = Nothing

        Else
            Me.lblSelectedPhase.Text = "Evidence pack phase " & ReturnedPhase
            Me.Options.Text = Nothing
            Me.navgeneral.Visible = False
            'Me.TopLabel.Visible = False
            Me.frmReport.Visible = True

            'Set the processing mode for the ReportViewer to Remote
            myReportViewer.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Remote

            Dim serverReport As Microsoft.Reporting.WebForms.ServerReport
            serverReport = myReportViewer.ServerReport

            serverReport.ReportServerCredentials = New clsReportServerCredentials("administrator", "chicago", "uml")

            'Set the report server URL and report path
            serverReport.ReportServerUrl = _
                New Uri("http://10.44.5.205/reportserver")
            serverReport.ReportPath = "/Operational Reports/Energy Management/CRC Quarterly Consumption Report"

            Dim CRCID As New Microsoft.Reporting.WebForms.ReportParameter()
            CRCID.Name = "CRCID"
            CRCID.Values.Add(ReturnedID)

            Dim FootPrintID As New Microsoft.Reporting.WebForms.ReportParameter()
            FootPrintID.Name = "FootPrintID"
            FootPrintID.Values.Add(ReturnedPhase)

            'Set the report parameters for the report
            Dim parameters() As Microsoft.Reporting.WebForms.ReportParameter = {CRCID, FootPrintID}
            serverReport.SetParameters(parameters)

            'Me.evidencepack.Style.Item("position") = "absolute"
            'Me.evidencepack.Style.Item("top") = "23px"
            'Me.evidencepack.Style.Item("width") = "650px"
            'Me.evidencepack.Style.Item("height") = "475px"
            'Me.evidencepack.Attributes("src") = "https://portal.utilitymasters.co.uk/reports/carbonreports/crcmanagementarea/CRCEvidencePack/Pack/CCA_Page21.asp?id=" & ReturnedID & "&net=" & ReturnedNet & "&phase=" & ReturnedPhase & ""
        End If

        '"~/reports/CarbonReports/CRCManagementArea/CRCEvidencePack/Pack/CCA_Page21.asp"

    End Sub
    Private Sub LoadTreeview_Report()


        Dim ReturnedID As String = Trim(Request.QueryString("id"))
        Me.navOnlineReports.InnerHtml = ""
        '  Dim strSQL As String = "EXEC spPortal_TREEVIEW " & MySession.UserID
        Dim strSQL As String = " EXEC Proc_Customer_CRC_Phases " & ReturnedID
        Dim nReturned As String = "<li class='styleformytreeview_company'><a href='javascript:history.back()'></a></li>"
        'Dim nReturned As String = "<li class='styleformytreeview_company'><a href='javascript:history.back()'><< Back </a></li>"
        Dim myConnection As New OleDb.OleDbConnection(Core.CurrentConnectionString)
        Dim myCommand As New OleDb.OleDbCommand(strSQL, myConnection)
        myCommand.Connection.Open()
        myCommand.CommandTimeout = 180
        Dim Reader As OleDb.OleDbDataReader = myCommand.ExecuteReader(CommandBehavior.CloseConnection)
        While Reader.Read
            'Me.navOnlineReports.InnerHtml += "<li class='styleformytreeview_company'><a target='evidencepack' href='/reports/carbonreports/crcmanagementarea/CRCEvidencePack/Pack/page1.asp?id=" & ReturnedID & "&net=true" & "&phase=" & Reader(0).ToString & "'</a>" & "Evidence pack Phase (" & Trim(Reader(0).ToString) & ")" & "</li>"
            Me.navOnlineReports.InnerHtml += "<li class='styleformytreeview_company'><a href='/reports/carbonreports/crcmanagementarea/CRCStatusReport/default.aspx?phase=" & Trim(Reader(0).ToString) & "&id=" & ReturnedID & "'>" & "Evidence pack Phase (" & Trim(Reader(0).ToString) & ")" & "</a></li>"
        End While
        Me.navOnlineReports.InnerHtml += "<br/>"
        Me.navOnlineReports.InnerHtml += nReturned
        Me.navOnlineReports.InnerHtml += "<br/>"
    End Sub



End Class
