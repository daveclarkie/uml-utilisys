﻿<%@ Page Title="" Language="VB" MasterPageFile="~/landing.master" AutoEventWireup="false" CodeFile="Default.aspx.vb" Inherits="reports_CarbonReports_CRCManagementArea_CRCStatusReport_Default" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=9.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>


<asp:Content ID="Content1" ContentPlaceHolderID="content_right" Runat="Server">

     <form id="form1" runat="server">

<style type="text/css" media="screen">
 .hiddencol  { display:none;  }
.viscol { display:block; }
.general {position:absolute;top:82px;left:233px;height:480px;width:670px;background: transparent url('/assets/content-back-general.jpg') no-repeat; padding-top:15px; padding-left:10px;}
 #evidencepack {position:absolute;top:50px;left:5px;height:450px;width:660px; overflow:auto; background: transparent;border:solid 1px lightgray;}
.navgeneral {position:absolute;top:83px;left:0px;width:220px;height:500px;background: white url('/assets/left-nav-account.png') no-repeat; padding-top:0px; padding-left:0px;}
.navgeneral2 {position:absolute;top:83px;left:0px;width:220px;height:50px;background: white url('/assets/left-nav-account.png') no-repeat; padding-top:0px; padding-left:0px;}
.TopLabel {position:absolute;top:50px;left:25px;height:23px;width:618px; font-size:12px;color:Navy; font-family:Verdana;   }
.TopLabel1{position:absolute;top:26px;left:115px;height:23px;width:618px; font-family:Arial;font-size:10px;color:red;   }
.TopLabel2 {position:absolute;top:13px;left:17px;height:23px;width:618px; font-size:12px;color:Navy; font-family:Verdana;   }
.TopLabel3 {position:absolute;top:55px;left:17px;height:23px;width:618px; font-size:12px;color:Navy; font-family:Verdana;   }
     #form1
     {
         height: 437px; 
     }
</style>


<div class="navgeneral2" id="navgeneral2" runat="server" >
<asp:HyperLink ID="linkBack" runat="server" CssClass="TopLabel2"  Text="click here to select new phase" BorderStyle="None"></asp:HyperLink>
<asp:label ID="lblSelectedPhase" runat="server" CssClass="TopLabel3"  Text="xx" BorderStyle="None"></asp:label>
</div>

<div class="navgeneral" id="navgeneral" runat="server" >

    <asp:HyperLink ID="linkBack2" runat="server" CssClass="TopLabel2"  Text="click here to change customer" BorderStyle="None"></asp:HyperLink>
    <asp:Label ID="Options" runat="server" CssClass="TopLabel3" Text="" BorderStyle="None"></asp:Label>
    
    
 <br />
 <br />
 <br />
 <br />

  <ul class="styleformytreeview" runat="server" id="navOnlineReports">  </ul> 
  <asp:TextBox ID="txtSelectedPhase" runat="server" Visible="false"></asp:TextBox>
   
   
</div>


    <div class="general" >
    <%--<asp:Label ID="TopLabel" runat="server" CssClass="TopLabel"  Text="Please select a valid Phase from the left" BorderStyle="None"></asp:Label>--%>

      
        <div style="position:absolute; left:0; top: 50; border: solid 1px black; height:560px; background-color:White; " id="frmReport" runat="server" visible="false">
            <table style="margin-left: 0px; margin-right: auto; z-index:100;">
                <tr>
                    <td colspan="4">
                        <rsweb:ReportViewer ID="myReportViewer" Height="510px" AsyncRendering="true" ShowPrintButton="false" runat="server" Width="900px" ShowRefreshButton="false" ShowToolBar="true" ShowZoomControl="false" ShowParameterPrompts="false" ShowBackButton="false" ShowCredentialPrompts="false" ShowFindControls="false" ShowPageNavigationControls="false" ShowPromptAreaButton="false">
                        </rsweb:ReportViewer> 
                    </td>
                </tr>
            </table>
        </div>   

    </div>
    
    </form>

</asp:Content>

