﻿
Partial Class reports_CRCEvidencePack_EvidencePack
    Inherits System.Web.UI.Page
 



    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        '
        Call LoadTreeview_Report()

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim myURL As String = Request.Url.ToString()
        Dim ParamStart As Integer = myURL.IndexOf("?")

        'Me.evidencepack.Attributes("src") = "https://portal.utilitymasters.co.uk/reports/carbonreports/crcmanagementarea/CRCCompanyStructure/Reports/matrix.asp?id=" & Trim(Request.QueryString("id")) & "&net=true&phase=1"

        Me.evidencepack.Style.Item("position") = "absolute"
        Me.evidencepack.Style.Item("top") = "50px"
        Me.evidencepack.Style.Item("width") = "650px"
        Me.evidencepack.Style.Item("height") = "450px"

        'If Me.evidencepack.Attributes("src").Length = 0 Then
        '    Me.TopLabel.Visible = False
        'Else
        '    Me.TopLabel.Visible = True
        'End If

        'position:absolute;top:50px;left:5px;height:450px;width:660px; overflow:auto; background: transparent;border:solid 1px lightgray;
    End Sub
    Private Sub LoadTreeview_Report()
        Dim ReturnedID As String = Trim(Request.QueryString("id"))
        If ReturnedID.Length = 0 Then ReturnedID = 0

        Me.navOnlineReports.InnerHtml = ""
        '  Dim strSQL As String = "EXEC spPortal_TREEVIEW " & MySession.UserID
        Dim strSQL As String = "" '" SELECT uml_extdata.dbo.FormatDateTime(ModifiedOn, 'dd/mm/yyyy') AS NiceDate FROM UML_CMS.dbo.tbl_Virtual_Customer_Snapshots WHERE VirtualID =  " & ReturnedID & " ORDER BY ModifiedOn asc"

        strSQL &= " SELECT "
        strSQL &= " [UML_CMS].dbo.tbl_Virtual_Customer_Snapshots.ID as ' Itemid' "
        'strSQL &= " uml_extdata.dbo.FormatDateTime(ModifiedOn, 'dd/mm/yyyy') AS NiceDate "
        strSQL &= " , [UML_CMS].dbo.tblCRC_Organisation_Profile.description"
        strSQL &= " FROM [UML_CMS].dbo.tbl_Virtual_Customer_Snapshots"
        strSQL &= " INNER JOIN [UML_CMS].dbo.tblCRC_Organisation_Profile ON [UML_CMS].dbo.tblCRC_Organisation_Profile.id ="
        strSQL &= " [UML_CMS].dbo.tbl_Virtual_Customer_Snapshots.profileid"
        strSQL &= " WHERE(VirtualID = " & ReturnedID & " And ProfileID > 0)"
        strSQL &= " ORDER BY profileid"


        Dim nReturned As String = "<li class='styleformytreeview_company'><a href='/reports/carbonreports/crcmanagementarea/default.aspx?crccustid=" & ReturnedID & "'><< Back </a></li>"
        Dim myConnection As New OleDb.OleDbConnection(Core.CurrentConnectionString)
        Dim myCommand As New OleDb.OleDbCommand(strSQL, myConnection)
        myCommand.Connection.Open()
        myCommand.CommandTimeout = 180
        Dim Reader As OleDb.OleDbDataReader = myCommand.ExecuteReader(CommandBehavior.CloseConnection)
        While Reader.Read
            'Me.navOnlineReports.InnerHtml += "<li class='styleformytreeview_company'><a target='evidencepack' href='/reports/carbonreports/crcmanagementarea/CRCCompanyStructure/Reports/matrix.asp?id=" & ReturnedID & "&date=" & Reader(0).ToString & "'</a>" & Trim(Reader(1).ToString) & "</li>"
            Me.navOnlineReports.InnerHtml += "<li class='styleformytreeview_company'><a target='evidencepack' href='/reports/carbonreports/crcmanagementarea/CRCCompanyStructure/Reports/matrix.asp?id=" & ReturnedID & "&itemid=" & Reader(0).ToString & "'>" & Trim(Reader(1).ToString) & "</a></li>"

        End While
        Me.navOnlineReports.InnerHtml += "<br/>"
        Me.navOnlineReports.InnerHtml += nReturned
        Me.navOnlineReports.InnerHtml += "<br/>"
    End Sub
End Class
