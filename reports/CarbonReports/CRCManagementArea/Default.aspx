<%@ Page Language="VB" MasterPageFile="~/landing.master" AutoEventWireup="false" CodeFile="Default.aspx.vb" Inherits="reports_CarbonReport_CRCManagementArea_Default" title="CRC Management Area" %>
<asp:Content ID="Content1" ContentPlaceHolderID="content_right" Runat="Server">

 <%--
  '  CRC carbon reports for john
--%>
  
    <div class="content_back general">
        <%--<asp:Panel id="pnlDefaultPage" style="Z-INDEX: 101; LEFT: 865px; POSITION: absolute; TOP: 93px; vertical-align:middle;" runat="server">
         <asp:hyperlink ImageUrl="~/assets/add-icon.png" ID="lnkAddCustomReport" runat="server" Text="" Visible="True" Width="25px" NavigateUrl="~/reports/MyReports/Default.aspx?method=add&report=35" ></asp:hyperlink>
    </asp:Panel>--%>
        <asp:Label runat="server" ID="lblCRCOrganisationName"></asp:Label>
    
        <ul class="box_links_reports" id="ulBox" runat="server">
            <li><a id="lnkReports41" class="reports_CarbonReports_CRCEvidencePack" runat="server" title="CRC Evidence Pack" style="background: transparent url('/assets/Schneider/buttons-reports-carbon-crcmanagement-crcevidencepack.gif') no-repeat;"></a></li>
        </ul>
        
    </div>
    
</asp:Content>

