﻿<%@ Page Language="VB"   MasterPageFile="~/landing.master" AutoEventWireup="false" CodeFile="Default.aspx.vb" Inherits="reports_CRCEvidencePack_EvidencePack" title="CRC Evidence Pack" %>

 <asp:Content ID="Content1" ContentPlaceHolderID="content_right" Runat="Server"> 
     <form id="form1" runat="server">

<style type="text/css" media="screen">
 .hiddencol  { display:none;  }
.viscol { display:block; }
.general {position:absolute;top:82px;left:233px;height:480px;width:670px;background: transparent url('/assets/content-back-general.jpg') no-repeat; padding-top:15px; padding-left:10px;}
 #evidencepack {position:absolute;top:50px;left:5px;height:450px;width:660px; overflow:auto; background: transparent;border:solid 1px lightgray;}
.navgeneral {position:absolute;top:83px;left:0px;width:220px;height:500px;background: white url('/assets/left-nav-account.png') no-repeat; padding-top:0px; padding-left:0px;}
.navgeneral2 {position:absolute;top:83px;left:0px;width:220px;height:50px;background: white url('/assets/left-nav-account.png') no-repeat; padding-top:0px; padding-left:0px;}
.TopLabel {position:absolute;top:50px;left:25px;height:23px;width:618px; font-size:12px;color:Navy; font-family:Verdana;   }
.TopLabel1{position:absolute;top:26px;left:115px;height:23px;width:618px; font-family:Arial;font-size:10px;color:red;   }
.TopLabel2 {position:absolute;top:13px;left:17px;height:23px;width:618px; font-size:12px;color:Navy; font-family:Verdana;   }
.TopLabel3 {position:absolute;top:42px;left:17px;height:23px;width:618px; font-size:12px;color:Navy; font-family:Verdana; font-weight:bold;   }
.TopLabel4 {position:absolute;top:58px;left:17px;height:23px;width:618px; font-size:10px;color:Navy; font-family:Verdana;   }
.TopLabel5 {position:absolute;top:70px;left:17px;height:23px;width:618px; font-size:10px;color:Navy; font-family:Verdana;   }
     #form1
     {
         height: 437px; 
     }
</style>


<div class="navgeneral2" id="navgeneral2" runat="server" >
<asp:HyperLink ID="linkBack" runat="server" CssClass="TopLabel2"  Text="click here to select new phase" BorderStyle="None"></asp:HyperLink>
<asp:label ID="lblSelectedPhase" runat="server" CssClass="TopLabel3"  Text="xx" BorderStyle="None"></asp:label>
<asp:LinkButton ID="lnkAttachedDocs" runat="server" CssClass="TopLabel4"  Text="View Attachments" BorderStyle="None"></asp:LinkButton>
<asp:LinkButton ID="lnkFrontCover" runat="server" CssClass="TopLabel5"  Text="Evidence Pack Cover Sheet" BorderStyle="None"></asp:LinkButton>
</div>

<div class="navgeneral" id="navgeneral" runat="server" >

    <asp:HyperLink ID="linkBack2" runat="server" CssClass="TopLabel2"  Text="click here to change customer" BorderStyle="None"></asp:HyperLink>
    <asp:Label ID="Options" runat="server" CssClass="TopLabel3" Text="" BorderStyle="None"></asp:Label>
    
    
 <br />
 <br />
 <br />
 <br />

  <ul class="styleformytreeview" runat="server" id="navOnlineReports">  </ul> 
  <asp:TextBox ID="txtSelectedPhase" runat="server" Visible="false"></asp:TextBox>
   
   
</div>


    <div class="general" >
    <%--<asp:Label ID="TopLabel" runat="server" CssClass="TopLabel"  Text="Please select a valid Phase from the left" BorderStyle="None"></asp:Label>--%>

      <iframe runat="server" id="evidencepack" FRAMEBORDER="0" name="evidencepack"></iframe> 
    
    </div>
    
    </form>
  </asp:Content> 
 