﻿
Partial Class reports_CRCEvidencePack_EvidencePack
    Inherits System.Web.UI.Page
 



    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        '
        Call LoadTreeview_Report()

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim myURL As String = Request.Url.ToString()
        Dim ParamStart As Integer = myURL.IndexOf("?")


        Dim ReturnedID As Integer = Trim(Request.QueryString("id"))
        Dim ReturnedNet As String = "true"

        If MySession.UserID = 77 Or MySession.UserID = 195 Then
            ReturnedNet = "false"
        End If
        Dim ReturnedPhase As String = Trim(Request.QueryString("phase"))



        ' Check you have rights to see the organisation
        Dim strCheckSQL As String = "SELECT COUNT(*) FROM UML_CMS.dbo.tblCRC_Company INNER JOIN (SELECT DISTINCT OrganisationID, Used, TypeOf FROM UML_CMS.dbo.tbl_Customer_BOM WHERE TypeOf = 'C' AND isNumeric(Used) = 1)tblA ON Used = CustID INNER JOIN UML_CMS.dbo.tbl_Virtual_Customer ON VirtualID = OrganisationID WHERE PRO_Custid IN (SELECT ID FROM [portal.utilitymasters.co.uk].dbo.vwUsersCompanies where [User ID] = " & MySession.UserID & ") AND OrganisationID = " & ReturnedID

        Dim intCheckUserOrg As Integer = Core.data_select_value(strCheckSQL)

        If intCheckUserOrg = 0 Then
            Response.Redirect("~/reports/carbonreports/default.aspx", True)
        End If

        'Me.evidencepack.Attributes("src") = "https://portal.utilitymasters.co.uk/reports/carbonreports/crcmanagementarea/CRCEvidencePack/Pack/page1.asp?id=" & Trim(Request.QueryString("id")) & "&net=true&phase=1"


        'If Me.evidencepack.Attributes("src").Length = 0 Then
        '    Me.TopLabel.Visible = False
        'Else
        '    Me.TopLabel.Visible = True
        'End If

        'position:absolute;top:50px;left:5px;height:450px;width:660px; overflow:auto; background: transparent;border:solid 1px lightgray;
        Me.linkBack.NavigateUrl = "~/reports/carbonreports/crcmanagementarea/CRCEvidencePack/Default.aspx?id=" & ReturnedID
        Me.linkBack2.NavigateUrl = "~/reports/carbonreports/crcmanagementarea/default.aspx"

        If ReturnedPhase = "" Then
            Me.txtSelectedPhase.Text = Nothing
        Else
            Me.txtSelectedPhase.Text = ReturnedPhase
        End If

        If Me.txtSelectedPhase.Text.Length = 0 Then
            Me.navgeneral.Visible = True
            'Me.TopLabel.Visible = True
            Me.evidencepack.Visible = False
            Me.Options.Text = "Please select a valid phase"
            Me.lblSelectedPhase.Text = Nothing

        Else
            Me.lblSelectedPhase.Text = "Evidence pack phase " & ReturnedPhase

            Me.Options.Text = Nothing
            Me.navgeneral.Visible = False
            'Me.TopLabel.Visible = False
            Me.evidencepack.Visible = True
            Me.evidencepack.Style.Item("position") = "absolute"
            Me.evidencepack.Style.Item("top") = "23px"
            Me.evidencepack.Style.Item("width") = "650px"
            Me.evidencepack.Style.Item("height") = "475px"
            Me.evidencepack.Attributes("src") = "https://portal.utilitymasters.co.uk/reports/carbonreports/crcmanagementarea/CRCEvidencePack/Pack/CCA_Page21.asp?id=" & ReturnedID & "&net=" & ReturnedNet & "&phase=" & ReturnedPhase & ""
        End If

        '"~/reports/CarbonReports/CRCManagementArea/CRCEvidencePack/Pack/CCA_Page21.asp"

    End Sub
    Private Sub LoadTreeview_Report()


        Dim ReturnedID As String = Trim(Request.QueryString("id"))
        Me.navOnlineReports.InnerHtml = ""
        '  Dim strSQL As String = "EXEC spPortal_TREEVIEW " & MySession.UserID
        Dim strSQL As String = " EXEC Proc_Customer_CRC_Phases " & ReturnedID
        Dim nReturned As String = "<li class='styleformytreeview_company'><a href='javascript:history.back()'></a></li>"
        'Dim nReturned As String = "<li class='styleformytreeview_company'><a href='javascript:history.back()'><< Back </a></li>"
        Dim myConnection As New OleDb.OleDbConnection(Core.CurrentConnectionString)
        Dim myCommand As New OleDb.OleDbCommand(strSQL, myConnection)
        myCommand.Connection.Open()
        myCommand.CommandTimeout = 180
        Dim Reader As OleDb.OleDbDataReader = myCommand.ExecuteReader(CommandBehavior.CloseConnection)
        While Reader.Read
            'Me.navOnlineReports.InnerHtml += "<li class='styleformytreeview_company'><a target='evidencepack' href='/reports/carbonreports/crcmanagementarea/CRCEvidencePack/Pack/page1.asp?id=" & ReturnedID & "&net=true" & "&phase=" & Reader(0).ToString & "'</a>" & "Evidence pack Phase (" & Trim(Reader(0).ToString) & ")" & "</li>"
            Me.navOnlineReports.InnerHtml += "<li class='styleformytreeview_company'><a href='/reports/carbonreports/crcmanagementarea/CRCEvidencePack/default.aspx?phase=" & Trim(Reader(0).ToString) & "&id=" & ReturnedID & "'>" & "Evidence pack Phase (" & Trim(Reader(0).ToString) & ")" & "</a></li>"
        End While
        Me.navOnlineReports.InnerHtml += "<br/>"
        Me.navOnlineReports.InnerHtml += nReturned
        Me.navOnlineReports.InnerHtml += "<br/>"
    End Sub

    Protected Sub lnkAttachedDocs_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAttachedDocs.Click
        Dim ReturnedID As Integer = Trim(Request.QueryString("id"))
        Dim ReturnedPhase As String = Trim(Request.QueryString("phase"))
        Me.evidencepack.Visible = True
        Me.evidencepack.Style.Item("position") = "absolute"
        Me.evidencepack.Style.Item("top") = "23px"
        Me.evidencepack.Style.Item("width") = "650px"
        Me.evidencepack.Style.Item("height") = "475px"
        Me.evidencepack.Attributes("src") = "https://portal.utilitymasters.co.uk/reports/carbonreports/crcmanagementarea/CRCEvidencePack/Pack/listalldocumnts.asp?id=" & Server.UrlEncode(ReturnedID) & "&Phase=" & Server.UrlEncode(ReturnedPhase)

    End Sub

    Protected Sub lnkFrontCover_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkFrontCover.Click
        Dim ReturnedID As Integer = Trim(Request.QueryString("id"))
        Dim ReturnedPhase As String = Trim(Request.QueryString("phase"))
        Dim ReturnedNet As String = "true"

        If MySession.UserID = 77 Or MySession.UserID = 195 Then
            ReturnedNet = "false"
        End If

        Me.evidencepack.Visible = True
        Me.evidencepack.Style.Item("position") = "absolute"
        Me.evidencepack.Style.Item("top") = "23px"
        Me.evidencepack.Style.Item("width") = "650px"
        Me.evidencepack.Style.Item("height") = "475px"
        Me.evidencepack.Attributes("src") = "https://portal.utilitymasters.co.uk/reports/carbonreports/crcmanagementarea/CRCEvidencePack/Pack/page1.asp?id=" & Server.UrlEncode(ReturnedID) & "&net=" & Server.UrlEncode(ReturnedNet) & "&phase=" & Server.UrlEncode(ReturnedPhase)

    End Sub
End Class
