﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO

Partial Class reports_CarbonReports_CRCManagementArea_CRCEvidencePack_Pack_readblob
    Inherits System.Web.UI.Page
    Dim dt2 As DataTable

    Protected Sub download(ByVal dt As DataTable)

        Try
            Dim bytes() As Byte = CType(dt.Rows(0)("Contents"), Byte())
            Response.Buffer = True
            Response.Charset = ""
            Response.Cache.SetCacheability(HttpCacheability.Private)
            Select Case dt.Rows(0)("Type").ToString()
                Case "doc"
                    ' response.setHeader("Content-description","My Description")
                    Response.ContentType = "application/msword"
                    'Response.AddHeader("Content-disposition", "inline; filename=" & NAF)
                Case "docx"
                    ' response.setHeader("Content-description","My Description")
                    Response.ContentType = "application/msword"
                    'Response.AddHeader("Content-disposition", "inline; filename=" & NAF)
                Case "xls"
                    ' response.setHeader("Content-description","My Description")
                    Response.ContentType = "application/msexcel"
                    'Response.AddHeader("Content-disposition", "attachment; filename=" & NAF)
                Case "xlsx"
                    ' response.setHeader("Content-description","My Description")
                    Response.ContentType = "application/msexcel"
                    'Response.AddHeader "Content-disposition", "attachment; filename=Something.xlsx"
                    'Response.AddHeader("Content-disposition", "attachment; filename=" & NAF)
                Case "pdf"
                    Response.ContentType = "application/pdf"
                    ' Response.AddHeader("Content-disposition", "inline; filename=" & NAF)
                Case "zip"
                    Response.ContentType = "application/octet-stream"
                    ' Response.AddHeader("Content-disposition", "attachment; filename=" & NAF)
                Case Else
            End Select

            Response.AddHeader("content-disposition", "attachment;filename=" & dt.Rows(0)("Description").ToString() & "." & dt.Rows(0)("Type").ToString())
            Response.BinaryWrite(bytes)
        Catch ex As Exception
            Response.Write("EEROR: " & ex.Message)
        Finally

            Response.Flush()
            Response.End()
        End Try

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim strQuery As String = " SELECT * FROM tblCRC_CCA_Page_Attachments where Id=" & Request.QueryString("ID")
        Dim cmd As SqlCommand = New SqlCommand(strQuery)
        dt2 = GetData(cmd)
        If dt2 IsNot Nothing Then
            Me.btnDownload.Enabled = True
            Me.lblError.Text = Nothing
        Else
            Me.btnDownload.Enabled = False
            Me.lblError.Text = "Error preparing file"
        End If
    End Sub

    Public Function GetData(ByVal cmd As SqlCommand) As DataTable
        Dim dt As New DataTable
        Dim strConnString As String = "server=uk-ed0-sqlcl-01.MCEG.local;Database=uml_cms;Trusted_Connection=True;"
        Dim con As New SqlConnection(strConnString)
        Dim sda As New SqlDataAdapter
        cmd.CommandType = CommandType.Text
        cmd.Connection = con
        Try
            con.Open()
            sda.SelectCommand = cmd
            sda.Fill(dt)
            Return dt
        Catch ex As Exception
            Response.Write(ex.Message)
            Return Nothing
        Finally
            con.Close()
            sda.Dispose()
            con.Dispose()
        End Try
    End Function


    Protected Sub btnDownload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDownload.Click
        download(dt2)
    End Sub
End Class
