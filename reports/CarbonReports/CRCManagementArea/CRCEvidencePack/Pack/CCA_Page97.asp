<%
Dim TheID 
         TheID=request.QueryString("id")  

Dim TheNet
         TheNet=request.QueryString("net")  

Dim Current_Phase
         Current_Phase=SESSION("phase")  


Dim ReportPhase
         ReportPhase=Request.QueryString("RP")  
If ReportPhase="" then
           Current_Phase=SESSION("phase")  
Else
           Current_Phase=Request.QueryString("RP")  
End If


Dim OnOff 
         if TheNet="true" then
               vis="Display:none;" 
         else
             vis="Display:;"  
        end if


 
 
 



         
%> 


   <html>  
 <%
Public dComm  
Public rComm  
 
Public Function OpenConnection()
  Set dComm = CreateObject("ADODB.Connection")
      dComm.Open "Provider=SQLOLEDB.1;Data Source=uk-ed0-sqlcl-01.MCEG.local;Initial Catalog=UML_CMS;Integrated Security=SSPI;"
End Function

Public Function OpenRecordset(nSQL)
 Set rComm = CreateObject("ADODB.recordset")
     rComm.ActiveConnection = dComm
     rComm.CursorLocation = 3 ' 2
     rComm.CursorType = 3 '1
     rComm.LockType = 1 '3
     rComm.Source = nSQL
     rComm.Open
End Function

Public Function CloseConnection()
  Set dComm = Nothing
  Set rComm = Nothing
End Function


'=========================================================================================
strsql = "SELECT * FROM dbo.tblCRC_CCA_Page97 WHERE virtualID = " & TheId
strsql = strsql & " AND TagID='9.7'" & " AND phase=" & "'" & Current_Phase & "'"

Call OpenConnection
Call OpenRecordset(strsql)
if rComm.EOF = true then
   continue=false
   f1_date=""
   f2_date=""
   f3_date=""
   f4_date=""
   f5_date=""
   f6_date=""
   f7_date=""
   f8_date=""
   
   f1=""
   f2=""
   f3=""
   f4=""
   f5=""
   f6=""
   f7=""
   f8=""
  
   
   
   Response.Write "<script language=""javascript"" type=""text/javascript"">" & vbnewline
   Response.write "var VirtualID=" & TheId & vbnewline
   Response.write "var EditID=" & "0"  & vbnewline
   Response.write "var TagId=" & "'" & "9.7" & "'" & vbnewline
   Response.Write "</script>" &  vbnewline
else
   continue=true
f1_date=trim(rcomm("f1_date"))
f2_date=trim(rcomm("f2_date"))

f1=rcomm("f1")
f2=rcomm("f2")
 
         
   Response.Write "<script language=""javascript"" type=""text/javascript"">" & vbnewline
   Response.write "var VirtualID=" & TheId & vbnewline
   Response.write "var EditID=" & rComm("ID") & vbnewline
   Response.write "var TagId=" & "'" & "9.7'" & vbnewline
   Response.Write "</script>" &  vbnewline
end if
 
  
'=========================================================================================

 %>
 
<head>
    <title>Page One</title>
<LINK REL=StyleSheet HREF="CCA_stylesheet.css" TYPE="text/css" >
<script type="text/javascript" src="mree.js"></script>
<script language="javascript" type="text/javascript">

// <!CDATA[
function icomplete(what)
{
   var total=0
        document.getElementById("mylabel").innerText="";
        total = total + validate("F1_Date");
        total = total + validate("F2_Date");
        validateall(2,total,VirtualID,"9.7")
}


function NotApplicable()
{
 var d = new Date() 
 var dy =     PadDigits(  d.getFullYear()  , 4)
 var dm =    PadDigits(     d.getMonth()  + 1 , 2)
 var dd =      PadDigits(   d.getDate() , 2)

document.getElementById("F1_Date").value=dd + "/" + dm + "/" + dy;
document.getElementById("F1_Date").style.backgroundColor="white"; 
 document.getElementById("F2_Date").value=dd + "/" + dm + "/" + dy;
document.getElementById("F2_Date").style.backgroundColor="white"; 
  //updateme() ;
}

function updateme()
{
  document.getElementById("myform").action = "evidenceupdate.asp?virtualid=" + VirtualID + "&base=tblCRC_CCA_Page97" + "&tag=" + TagId + "&editid="  + EditID ;
  document.getElementById("myform").submit();
}


// ]]>
</script>
</head>

<body>
 <form name="myform"  action="evidenceupdate.asp" method="post" target="myframe"> 
    <div  id="labelbanner">
        Source list exclusions (9.7)
    </div>
 <br />
<!-----  R E T U R N ---->

 <button class="backbutton" onclick="window.location.href=<%="'page1.asp?id=" & TheID & "&net=" & TheNet  & "&phase=" & Current_Phase  & "'" %>" title="Return back main menu" ><< Back</button>

<%if TheNet="false" then %>
 <input class="completebutton" title="Assign these questions with a Completed status Tag " type="button" value="Complete" onclick="javascript:icomplete('9.7');" />
 <input class="completebutton" title="Not Applicable" type="button" value="N/A" onclick="javascript:NotApplicable('9.7');" />
<%end if%>
<!-----  R E T U R N ---->
<br />
    
 

<label class="messagelabel" id="myLabel"></label>
    <table >
          <tr>
            <td   class="One" style="width: 291px"> Confirm that source list exclusions
                <br />
                for EU ETS and CCA are separately
                <br />
                identified under Table 5.1.6 of
                <br />
                the evidence pack
           </td>
            <td>
                <select id="F1" class="Two" name="F1">
                   	<%if continue=true then %>
					   <% if rComm("F1")="Yes" then %>
					       <option selected="selected" value="Yes">Yes</option> 
					       <option value="No">No</option>
					   <% Else %>
					       <option value="Yes">Yes</option>
					       <option selected="selected" value="No">No</option> 
					   <% End If %>
					<% else %>
<option selected value=""></option>
<option value="Yes">Yes</option>
<option value="No">No</option>
					<% End If %>
                </select></td>
              <td   >
                  <input id="F1_Date"  style="<%=vis%>"  type="text" name="F1_Date"  value="<%=F1_Date%>" class="four"  /></td>
        </tr>
        
          <tr>
            <td   class="One" style="width: 291px"> Confirm that the CCA supplies
                <br />
                and emissions have been split
                <br />
                by source/fuel type supplied</td>
            <td><select id="F2" class="Two" name="F2">
                <%if continue=true then %>
                <% if rComm("F2")="Yes" then %>
                <option selected="selected" value="Yes">Yes</option>
                <option value="No">No</option>
                <% Else %>
                <option value="Yes">Yes</option>
                <option selected="selected" value="No">No</option>
                <% End If %>
                <% else %>
<option selected value=""></option>
<option value="Yes">Yes</option>
<option value="No">No</option>
                <% End If %>
            </select>
               <td   >
             <input id="F2_Date"   style="<%=vis%>"  type="text" name="F2_Date"  value="<%=F2_Date%>" class="four"  /></td>
        </tr>
        
    </table>
<%if TheNet="false" then %>
 <INPUT type="button" value="Submit" id="button1" name="button1" onclick="javascript:updateme();" class="backbutton"/>
<%end if %>


<iframe id="myframe" name="myframe" src="blank2.htm" frameborder="no" scrolling="no"/>

 </form>
</body>
</html>
<%
 

 Call CloseConnection
 %> 