// VIETSON WEB WINDOW 1.0
// This web component is designed for Programmers.
// Browser: MSIE 4.0 or greater.
// Created by Long Ngo, 12/10/2000
// Email: LongNgo78@hotmail.com
// URL: http://www.VietSon.com

var _STATUS = 0;
var WIN_DRAG_APPROVED = false;
var _WIN_X, _WIN_Y, _WIN_O;
var _WIN_UPPER_LIMIT  = 0;
var _WIN_CLICK_X, _WIN_CLICK_Y;
var _WIN_DEF_HEIGHT = 150;
var _WIN_DOC_MOUSE_MOVE_BAK;
var _WIN_HTML = '';
var _WIN_IMG_DIR = 'Images';
var LastFocusWinName = null; 

// Variable Name must be the same WinID
function CWindow(WinID, Title, X, Y, Width, Height)
{
	this.WinID = WinID;
	this.Title = Title;
	this.X = X;
	this.Y = Y;
	
	this.Width = (Width==null)?(300):(Width);
	this.Height = (Height==null)?(''):(Height);
	
	this._Z_INDEX = 0;
	this._ON_TOP = 0;
	this._MINIMIZE = 1;
	this._MAXIMIZE = 1;
	this._CLOSE = 1;
	this._BUTTON_W = '';
	this._AUTO_FREE_MEM = 1;
	
	this.Draw = Win_Draw;
	this.UnDraw = Win_UnDraw;
	this.Hide = Win_Hide;
	this.Show = Win_Show;
	
	this.TitleStyleClass = 'WindowTitle';
	this.TitleHeader = '';
	this.BodyHTML = '';
	this.IconURL = '';
	this.IconWidth = 1;
	
	this.OnIconClick = '';
	this.OnStartDrag = '';
	this.OnEndDrag = '';
	this.OnMinimize = '';
	this.OnMaximize = '';
	this.OnClose = '';
	
	this.Status = new Array();
	this.Status._VIEW = 1;
	this.Status._MIN = 0;
	this.Status._MAX = 0;
}
//------------ Create a Window Layer
function Win_Draw()
{	
	var sTempW;
	if (this._BUTTON_W != '') sTempW = 'width = ' + this._BUTTON_W;
	_WIN_HTML = '';	
	Win_InsertHTML('<Div style="position:absolute;" id=Div_'+ this.WinID + '>');
	Win_InsertHTML('<Table ID='+ this.WinID +' Border=1 class=WindowBorder OnMouseDown="Win_SetFocus(\''+this.WinID+'\')" width=' + this.Width + ' cellpadding=0 cellspacing=0>');	
		Win_InsertHTML('<TR><TD><Table ID=Title_'+ this.WinID +' class='+this.TitleStyleClass+' width=100% border=0 cellpadding=1 cellspacing=0><TR>');
			if (this.IconURL != '')
				Win_InsertHTML('<TD valign=Top width='+this.IconWidth+' onclick="'+this.OnIconClick+'"><Img src='+this.IconURL+' style="cursor:default;"></TD>');
			Win_InsertHTML('<TD width=1>' + this.TitleHeader + '</TD>');			
			Win_InsertHTML('<TD class=WindowDragMe  onmousedown="Win_StartDrag(\''+this.WinID+'\');" onmouseup="Win_EndDrag(\''+this.WinID+'\');" ondblclick="Win_Minimize(\''+this.WinID+'\');">' + this.Title + '</TD>');			
			if (this._MINIMIZE == 1)				
				Win_InsertHTML('<TD width=1 onclick="Win_Minimize(\''+this.WinID+'\');"><Img src='+_WIN_IMG_DIR+'/icoUp.gif name=Img_'+this.WinID+'_Arrow '+sTempW+'></TD>');
			if (this._MAXIMIZE == 1)
				Win_InsertHTML('<TD width=1 onclick="Win_Maximize(\''+this.WinID+'\');"><Img src='+_WIN_IMG_DIR+'/icoRestore.gif name=Img_'+this.WinID+'_Maximize '+sTempW+'></TD>');
			if (this._CLOSE == 1)
				Win_InsertHTML('<TD width=1 onclick="'+this.WinID+'.UnDraw();"><Img src='+_WIN_IMG_DIR+'/icoClose.gif '+sTempW+'></TD>');
		Win_InsertHTML('</TR></Table>');
		
		Win_InsertHTML('<TR><TD id=Div_'+ this.WinID +'_Body>');		
		
		//Scroll bar
		Win_InsertHTML('<DIV class=WindowScrollDiv id=Div_'+ this.WinID +'_Scroll>');Win_InsertHTML('<Table width=100% border=0 cellpadding=0 cellspacing=0 class=WindowScrollDiv><TD>');
		//----------
			//BODY			
			Win_InsertHTML(this.BodyHTML);

		//END Scroll bar
		Win_InsertHTML('</TD></Table>');Win_InsertHTML('</Div>');
		//--------------
		
		Win_InsertHTML('</TD></TR>');
		
	Win_InsertHTML('</Table>');	
	Win_InsertHTML('</Div>');
	document.body.insertAdjacentHTML("BeforeEnd", _WIN_HTML); _WIN_HTML = '';
	if (this.Status._VIEW == 0)
		document.all['Div_'+this.WinID].style.visibility = 'hidden';
	if (this.Status._MIN == 1)
		Win_Minimize(this.WinID);
	if (this.Status._MAX == 1)
		Win_Maximize(this.WinID);
		
	if (this._Z_INDEX != 0)
		document.all['Div_'+this.WinID].style.zIndex = this._Z_INDEX;
	document.all['Div_'+this.WinID].style.top = this.Y;
	document.all['Div_'+this.WinID].style.left = this.X;	
	
	if (this._AUTO_FREE_MEM)
	{
		this.TitleStyleClass = '';
		this.TitleHeader = '';
		this.BodyHTML = '';
		this.IconURL = '';
	}
}
function Win_UnDraw()
{	
	eval(this.OnClose);
	var ObjName = 'Div_' + this.WinID; 
	document.all[ObjName].innerHTML = '';	
	document.all[ObjName].id = '';
	var wObj = eval(this.WinID);
	wObj = null;
}
function Win_Hide()
{
	eval(this.OnClose);
	var ObjName = 'Div_' + this.WinID; 
	document.all[ObjName].style.visibility = 'hidden';
	this.Status._VIEW = 0;
}
function Win_Show()
{
	eval(this.OnClose);
	var ObjName = 'Div_' + this.WinID; 
	document.all[ObjName].style.visibility = 'visible';	
	this.Status._VIEW = 1;
}
//------------   Insert Str to HTML STACK
function Win_InsertHTML(Str)
{
	_WIN_HTML += Str;
}
//------------
function Win_Move()
{
	if (_WIN_DRAG_APPROVED)
	{
		_WIN_O.style.pixelLeft = _WIN_X + event.clientX - _WIN_CLICK_X;
		if (_WIN_Y + event.clientY - _WIN_CLICK_Y >= _WIN_UPPER_LIMIT)
			_WIN_O.style.pixelTop = _WIN_Y + event.clientY - _WIN_CLICK_Y;
		return false;
	}
}
function Win_StartDrag(WinID)
{
	eval(eval(WinID+'.OnStartDrag'));
	var ObjName = 'Div_' + WinID;
	_WIN_DRAG_APPROVED = true;	
	_WIN_O = document.all[ObjName];
	_WIN_X = _WIN_O.style.pixelLeft;
	_WIN_Y = _WIN_O.style.pixelTop;
	_WIN_CLICK_X = event.clientX;
	_WIN_CLICK_Y = event.clientY;
	_WIN_DOC_MOUSE_MOVE_BAK = document.onmousemove;
	document.onmousemove = Win_Move;
}
function Win_EndDrag(WinID)
{
	eval(eval(WinID+'.OnEndDrag'));
	_WIN_DRAG_APPROVED = false;		
	document.onmousemove = _WIN_DOC_MOUSE_MOVE_BAK;
}
function Win_Minimize(WinID)
{
	eval(eval(WinID+'.OnMinimize'));
	var ObjName = 'Div_' + WinID + '_Body';
	
	var Ok = document.all[ObjName].style.display != 'none';
	if (Ok)
	{		
		document.all[ObjName].style.display = 'none';
		Win_ChangeImageSrc('Img_' + WinID + '_Arrow',_WIN_IMG_DIR + '/icoDown.gif');				
		eval(WinID+'.Status._MIN = 1;');
		return 0;
	}
	else
	{
		document.all[ObjName].style.display = 'block';
		Win_ChangeImageSrc('Img_' + WinID + '_Arrow',_WIN_IMG_DIR + '/icoUp.gif');
		eval(WinID+'.Status._MIN = 0;');
		return 1;
	}		
}
//
function Win_Maximize(WinID)
{		
	eval(eval(WinID+'.OnMaximize'));
	var ObjName = 'Div_' + WinID + '_Scroll';
	//alert('_'+document.all[ObjName].style.overflowY+'_');
	if (document.all[ObjName].style.overflowY != 'scroll')
	{		
		document.all[ObjName].style.overflowY = 'scroll';
		document.all[ObjName].style.height = _WIN_DEF_HEIGHT;
		Win_ChangeImageSrc('Img_' + WinID + '_Maximize',_WIN_IMG_DIR + '/icoMaximize.gif');		
		eval(WinID+'.Status._MAX = 0;');
		return 0;
	}
	else
	{
		document.all[ObjName].style.overflowY = 'hidden';
		document.all[ObjName].style.height = 'auto';
		Win_ChangeImageSrc('Img_' + WinID + '_Maximize',_WIN_IMG_DIR + '/icoRestore.gif');
		eval(WinID+'.Status._MAX = 1;');
		return 1;
	}	
}
function Win_SetFocus(WinID)
{	
	var wObj = eval(WinID);
	if (wObj._ON_TOP != 0) return;
	
	var ObjName = 'Div_' + WinID;
	if (LastFocusWinName !='' && document.all[LastFocusWinName] != null)
		document.all[LastFocusWinName].style.zIndex--;		

	document.all[ObjName].style.zIndex = 10;
	LastFocusWinName = ObjName;
}
//------------   Change an Image Source File
function Win_ChangeImageSrc(ImgName, Src)
{
	document.images[ImgName].src = Src;
}