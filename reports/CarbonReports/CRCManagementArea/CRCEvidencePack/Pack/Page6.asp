<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <title>Untitled Page</title>
        <script language="javascript" type="text/javascript">
// <!CDATA[

function showme()
{
    var StringConvert = document.getElementById("myform").outerHTML
    document.getElementById("textsubmit").innerText = StringConvert ;
    document.myform.submit();
}

function icomplete(what)
{
     alert(what);
}
 

// ]]>
</script>
</head>
<body>
    <br />
    <br />
    <table cellpadding="0" cellspacing="0" class="t6" style="border-top-style: solid;
        border-right-style: solid; border-left-style: solid; border-bottom-style: solid">
        <tr class="tr11">
            <td class="td749" style="width: 56px; text-align: center">
<input id="Button2" style="width: 80px" title="buton" type="button" value="Complete" onclick="javascript:icomplete('6.1.1');" /><br />              
                <font class="ft54"><strong>6.1.1</strong></font></td>
            <td class="td750" style="font-weight: bold; width: 292px">
                Has a copy of the information</td>
            <td class="td751" style="width: 262px">
                &nbsp;</td>
            <td class="td752" style="width: 145px">
                <font class="ft54"><strong>Document</strong></font></td>
            <td class="td591" style="font-weight: bold; width: 149px">
                <font class="ft54">Authorised</font></td>
            <td class="td753" style="font-weight: bold; width: 122px">
                <font class="ft54">Completion</font></td>
        </tr>
        <tr class="tr10" style="font-weight: bold">
            <td class="td754" style="width: 56px">
                &nbsp;</td>
            <td class="td755" style="width: 292px">
                supplied to the administrator for the</td>
            <td class="td756" style="width: 262px">
                &nbsp;</td>
            <td class="td757" style="width: 145px">
                <font class="ft54">reference</font></td>
            <td class="td596" style="width: 149px">
                <font class="ft54">signature</font></td>
            <td class="td758" style="width: 122px">
                <font class="ft54">date</font></td>
        </tr>
        <tr class="tr10">
            <td class="td754" style="width: 56px">
                &nbsp;</td>
            <td class="td755" style="width: 292px">
                first footprint report and the latest</td>
            <td class="td759" style="width: 262px">
                &nbsp;</td>
            <td class="td760" style="width: 145px">
                &nbsp;</td>
            <td class="td606" style="width: 149px">
                &nbsp;</td>
            <td class="td761" style="width: 122px">
                &nbsp;</td>
        </tr>
        <tr class="tr10">
            <td class="td754" style="width: 56px">
                &nbsp;</td>
            <td class="td755" style="width: 292px">
                footprint report (if different) and each</td>
            <td class="td759" style="width: 262px; text-align: center">
                &nbsp;<select id="Select1" style="width: 60px">
                    <option selected="selected" value="Yes">Yes</option>
                    <option value="No">No</option>
                </select></td>
            <td class="td760" style="width: 145px">
                &nbsp;</td>
            <td class="td606" style="width: 149px">
                &nbsp;</td>
            <td class="td761" style="width: 122px; text-align: center">
                &nbsp;<input id="Text1" style="width: 88px" type="text" /></td>
        </tr>
        <tr class="tr10">
            <td class="td754" style="width: 56px">
                &nbsp;</td>
            <td class="td755" style="width: 292px">
                previous footprint report been</td>
            <td class="td759" style="width: 262px">
                &nbsp;</td>
            <td class="td760" style="width: 145px">
                &nbsp;</td>
            <td class="td606" style="width: 149px">
                &nbsp;</td>
            <td class="td761" style="width: 122px">
                &nbsp;</td>
        </tr>
        <tr class="tr18">
            <td class="td762" style="width: 56px">
                &nbsp;</td>
            <td class="td763" style="width: 292px">
                <font class="ft73">attached to the evidence pack?</font></td>
            <td class="td764" style="width: 262px">
                &nbsp;</td>
            <td class="td765" style="width: 145px">
                &nbsp;</td>
            <td class="td766" style="width: 149px">
                &nbsp;</td>
            <td class="td767" style="width: 122px">
                &nbsp;</td>
        </tr>
    </table>
    <br />
    <br />
    <table cellpadding="0" cellspacing="0" class="t13" style="border-top-style: solid;
        border-right-style: solid; border-left-style: solid; border-bottom-style: solid">
        <tr class="tr11">
            <td class="td768" style="width: 55px; text-align: center">
<input id="Button2" style="width: 80px" title="buton" type="button" value="Complete" onclick="javascript:icomplete('6.1.2');" /><br />              
                <font class="ft54"><strong>6.1.2</strong></font></td>
            <td class="td769" style="font-weight: bold; width: 242px">
                <font class="ft54">Historic scheme coverage</font></td>
            <td class="td541" style="font-weight: bold; width: 131px">
            </td>
            <td class="td770" style="font-weight: bold; width: 137px">
            </td>
            <td class="td612" style="font-weight: bold; width: 154px">
            </td>
            <td class="td771" style="font-weight: bold; width: 124px">
            </td>
        </tr>
        <tr class="tr22" style="font-weight: bold">
            <td class="td519" style="width: 55px">
                &nbsp;</td>
            <td class="td772" style="width: 242px">
                &nbsp;</td>
            <td class="td701" style="width: 131px">
                &nbsp;</td>
            <td class="td773" style="width: 137px">
            </td>
            <td class="td617" style="width: 154px">
            </td>
            <td class="td774" style="width: 124px">
            </td>
        </tr>
        <tr class="tr2">
            <td class="td529" style="width: 55px">
                &nbsp;</td>
            <td class="td775" style="width: 242px">
                What percentage of your footprint</td>
            <td class="td684" style="width: 131px; text-align: center">
                <font class="ft67"><em>
                    <input id="Text2" style="width: 54px" type="text" /></em></font></td>
            <td class="td776" style="width: 137px; font-style: italic">
                &nbsp;</td>
            <td class="td777" style="width: 154px">
                &nbsp;</td>
            <td class="td778" style="width: 124px; text-align: center">
                &nbsp;<input id="Text4" style="width: 88px" type="text" /></td>
        </tr>
        <tr class="tr10">
            <td class="td529" style="width: 55px">
                &nbsp;</td>
            <td class="td775" style="width: 242px">
                emissions are regulated in Phase 1?</td>
            <td class="td697" style="width: 131px">
                &nbsp;</td>
            <td class="td779" style="width: 137px">
                &nbsp;</td>
            <td class="td620" style="width: 154px">
                &nbsp;</td>
            <td class="td780" style="width: 124px">
                &nbsp;</td>
        </tr>
        <tr class="tr19">
            <td class="td669" style="width: 55px">
                &nbsp;</td>
            <td class="td781" style="width: 242px">
                <font class="ft61">&nbsp;</font></td>
            <td class="td688" style="width: 131px">
                &nbsp;</td>
            <td class="td782" style="width: 137px">
                &nbsp;</td>
            <td class="td624" style="width: 154px">
                &nbsp;</td>
            <td class="td783" style="width: 124px">
                &nbsp;</td>
        </tr>
        <tr class="tr3">
            <td class="td529" style="width: 55px">
                &nbsp;</td>
            <td class="td775" style="width: 242px">
                What percentage of your footprint</td>
            <td class="td684" style="width: 131px">
                <font class="ft67"><em>&nbsp;</em></font></td>
            <td class="td779" style="width: 137px; font-style: italic">
                &nbsp;</td>
            <td class="td620" style="width: 154px">
                &nbsp;</td>
            <td class="td780" style="width: 124px">
                &nbsp;</td>
        </tr>
        <tr class="tr10">
            <td class="td529" style="width: 55px">
                &nbsp;</td>
            <td class="td775" style="width: 242px">
                emissions were regulated in</td>
            <td class="td684" style="width: 131px; text-align: center">
                <font class="ft67"><em>&nbsp;<input id="Text3" style="width: 54px" type="text" /></em></font></td>
            <td class="td779" style="width: 137px; font-style: italic">
                &nbsp;</td>
            <td class="td620" style="width: 154px">
                &nbsp;</td>
            <td class="td780" style="width: 124px; text-align: center">
                &nbsp;<input id="Text5" style="width: 88px" type="text" /></td>
        </tr>
        <tr class="tr10">
            <td class="td529" style="width: 55px; height: 19px">
                &nbsp;</td>
            <td class="td775" style="width: 242px; height: 19px">
                subsequent phase(s)?</td>
            <td class="td684" style="width: 131px; height: 19px">
                <font class="ft67"><em>&nbsp;</em></font></td>
            <td class="td779" style="width: 137px; font-style: italic; height: 19px">
                &nbsp;</td>
            <td class="td620" style="width: 154px; height: 19px">
                &nbsp;</td>
            <td class="td780" style="width: 124px; height: 19px">
                &nbsp;</td>
        </tr>
        <tr class="tr18">
            <td class="td784" style="width: 55px">
                &nbsp;</td>
            <td class="td785" style="width: 242px">
                &nbsp;</td>
            <td class="td547" style="width: 131px">
                <font class="ft68"><em>&nbsp;</em></font></td>
            <td class="td786" style="width: 137px; font-style: italic">
                &nbsp;</td>
            <td class="td640" style="width: 154px">
                &nbsp;</td>
            <td class="td787" style="width: 124px">
                &nbsp;</td>
        </tr>
        <tr class="tr2">
            <td class="td529" style="width: 55px; height: 19px">
                &nbsp;</td>
            <td class="td775" style="width: 242px; height: 19px">
                If increased between phases, please confirm why</td>
            <td class="td697" style="width: 131px; height: 19px">
                &nbsp;<textarea id="TextArea1" style="width: 313px; height: 181px"></textarea></td>
            <td class="td779" style="width: 137px; height: 19px">
                &nbsp;</td>
            <td class="td620" style="width: 154px; height: 19px">
                &nbsp;</td>
            <td class="td780" style="width: 124px; height: 19px; text-align: center">
                &nbsp;<input id="Text6" style="width: 88px" type="text" /></td>
        </tr>
        <tr class="tr22">
            <td class="td519" style="width: 55px">
                &nbsp;</td>
            <td class="td781" style="width: 242px">
                <font class="ft58">&nbsp;</font></td>
            <td class="td701" style="width: 131px">
                &nbsp;</td>
            <td class="td788" style="width: 137px">
                &nbsp;</td>
            <td class="td637" style="width: 154px">
                &nbsp;</td>
            <td class="td789" style="width: 124px">
                &nbsp;</td>
        </tr>
    </table>
    <br />
    <table cellpadding="0" cellspacing="0" class="t14" style="border-top-style: solid;
        border-right-style: solid; border-left-style: solid; border-bottom-style: solid">
        <tr class="tr11">
            <td class="td768" style="width: 52px; text-align: center">
<input id="Button2" style="width: 80px" title="buton" type="button" value="Complete" onclick="javascript:icomplete('6.2.1');" /><br />              
                6.2.1</td>
            <td class="td769" style="width: 245px">
                Total footprint data</td>
            <td class="td541" style="width: 321px">
            </td>
            <td class="td770" style="width: 138px">
            </td>
            <td class="td612" style="width: 149px">
            </td>
            <td class="td771" style="width: 128px">
            </td>
        </tr>
        <tr class="tr21">
            <td class="td790" style="width: 52px">
                &nbsp;</td>
            <td class="td791" style="width: 245px">
                &nbsp;</td>
            <td class="td792" style="width: 321px">
                &nbsp;</td>
            <td class="td773" style="width: 138px">
            </td>
            <td class="td617" style="width: 149px">
            </td>
            <td class="td774" style="width: 128px">
            </td>
        </tr>
        <tr class="tr2">
            <td class="td529" style="width: 52px">
                &nbsp;</td>
            <td class="td775" style="width: 245px">
                <font class="ft85">Has a full record of energy supplies</font></td>
            <td class="td684" style="width: 321px; text-align: center">
                <select id="Select2" style="width: 60px">
                    <option selected="selected" value="Yes">Yes</option>
                    <option value="No">No</option>
                </select>
            </td>
            <td class="td776" style="width: 138px; font-style: italic">
                &nbsp;</td>
            <td class="td777" style="width: 149px">
                &nbsp;</td>
            <td class="td778" style="width: 128px; text-align: center">
                &nbsp;<input id="Text8" style="width: 88px" type="text" /></td>
        </tr>
        <tr class="tr10">
            <td class="td529" style="width: 52px">
                &nbsp;</td>
            <td class="td775" style="width: 245px">
                <font class="ft85">by source been supplied for the</font></td>
            <td class="td697" style="width: 321px">
                &nbsp;</td>
            <td class="td779" style="width: 138px">
                &nbsp;</td>
            <td class="td620" style="width: 149px">
                &nbsp;</td>
            <td class="td780" style="width: 128px">
                &nbsp;</td>
        </tr>
        <tr class="tr22">
            <td class="td519" style="width: 52px">
                &nbsp;</td>
            <td class="td781" style="width: 245px">
                <font class="ft58">footprint year?</font></td>
            <td class="td701" style="width: 321px">
                &nbsp;</td>
            <td class="td788" style="width: 138px">
                &nbsp;</td>
            <td class="td637" style="width: 149px">
                &nbsp;</td>
            <td class="td789" style="width: 128px">
                &nbsp;</td>
        </tr>
        <tr class="tr2">
            <td class="td529" style="width: 52px">
            </td>
            <td class="td775" style="width: 245px">
            </td>
            <td class="td684" style="width: 321px">
            </td>
            <td class="td779" style="font-size: 6pt; width: 138px; font-style: italic">
            </td>
            <td class="td620" style="width: 149px">
            </td>
            <td class="td780" style="width: 128px">
            </td>
        </tr>
        <tr class="tr2">
            <td class="td529" style="width: 52px">
                &nbsp;</td>
            <td class="td775" style="width: 245px">
                <font class="ft85">Total emissions calculated using the</font></td>
            <td class="td684" style="width: 321px; text-align: center">
                <em><font class="ft72"><span style="font-size: 6pt">
                    <input id="Text7" style="width: 88px" type="text" />2</span></font></em></td>
            <td class="td779" style="font-size: 6pt; width: 138px; font-style: italic">
                &nbsp;</td>
            <td class="td620" style="width: 149px">
                &nbsp;</td>
            <td class="td780" style="width: 128px; text-align: center">
                &nbsp;<input id="Text9" style="width: 88px" type="text" /></td>
        </tr>
        <tr class="tr10">
            <td class="td529" style="width: 52px">
                &nbsp;</td>
            <td class="td775" style="width: 245px">
                <font class="ft85">supply data by source in the</font></td>
            <td class="td697" style="width: 321px">
                &nbsp;</td>
            <td class="td779" style="width: 138px">
                &nbsp;</td>
            <td class="td620" style="width: 149px">
                &nbsp;</td>
            <td class="td780" style="width: 128px">
                &nbsp;</td>
        </tr>
        <tr class="tr19">
            <td class="td669" style="width: 52px">
                &nbsp;</td>
            <td class="td781" style="width: 245px">
                <font class="ft61">footprint year.</font></td>
            <td class="td688" style="width: 321px">
                &nbsp;</td>
            <td class="td782" style="width: 138px">
                &nbsp;</td>
            <td class="td624" style="width: 149px">
                &nbsp;</td>
            <td class="td783" style="width: 128px">
                &nbsp;</td>
        </tr>
    </table>
    <br />
    <table cellpadding="0" cellspacing="0" class="t14" style="border-top-style: solid;
        border-right-style: solid; border-left-style: solid; border-bottom-style: solid">
        <tr class="tr2">
            <td class="td514" style="width: 47px; text-align: center">
<input id="Button2" style="width: 80px" title="buton" type="button" value="Complete" onclick="javascript:icomplete('6.3');" /><br />              
                6.3</td>
            <td class="td793" style="width: 251px">
                <font class="ft85">&nbsp;</font></td>
            <td class="td794" style="width: 318px">
                <em><font class="ft67"></font></em>
            </td>
            <td class="td770" style="width: 141px; font-style: italic">
            </td>
            <td class="td612" style="width: 147px">
            </td>
            <td class="td771" style="width: 129px">
            </td>
        </tr>
        <tr class="tr10">
            <td class="td529" style="width: 47px">
                &nbsp;</td>
            <td class="td795" style="width: 251px">
                <font class="ft85">&nbsp;</font></td>
            <td class="td796" style="width: 318px">
                &nbsp;</td>
            <td class="td773" style="width: 141px">
            </td>
            <td class="td617" style="width: 147px">
            </td>
            <td class="td774" style="width: 129px">
            </td>
        </tr>
        <tr class="tr10">
            <td class="td529" style="width: 47px">
                &nbsp;</td>
            <td class="td795" style="width: 251px">
                CCA emissions in footprint year other than CCA emissions from CCA facilities with
                a CCA exemption</td>
            <td class="td796" style="width: 318px; text-align: center">
                &nbsp;<input id="Text10" style="width: 88px" type="text" /></td>
            <td class="td779" style="width: 141px">
                &nbsp;</td>
            <td class="td620" style="width: 147px">
                &nbsp;</td>
            <td class="td780" style="width: 129px; text-align: center">
                &nbsp;<input id="Text21" style="width: 88px" type="text" /></td>
        </tr>
        <tr class="tr18">
            <td class="td784" style="width: 47px">
                &nbsp;</td>
            <td class="td797" style="width: 251px">
                <font class="ft73">&nbsp;</font></td>
            <td class="td798" style="width: 318px">
                &nbsp;</td>
            <td class="td786" style="width: 141px">
                &nbsp;</td>
            <td class="td640" style="width: 147px">
                &nbsp;</td>
            <td class="td787" style="width: 129px">
                &nbsp;</td>
        </tr>
        <tr class="tr18">
            <td class="td784" style="width: 47px">
            </td>
            <td class="td797" style="width: 251px">
            </td>
            <td class="td798" style="width: 318px">
            </td>
            <td class="td786" style="width: 141px">
            </td>
            <td class="td640" style="width: 147px">
            </td>
            <td class="td787" style="width: 129px">
            </td>
        </tr>
        <tr class="tr18">
            <td class="td784" style="width: 47px">
            </td>
            <td class="td797" style="width: 251px">
            </td>
            <td class="td798" style="width: 318px">
            </td>
            <td class="td786" style="width: 141px">
            </td>
            <td class="td640" style="width: 147px">
            </td>
            <td class="td787" style="width: 129px">
            </td>
        </tr>
    </table>
    <br />
    <table cellpadding="0" cellspacing="0" class="t12" style="border-top-style: solid;
        border-right-style: solid; border-left-style: solid; border-bottom-style: solid">
        <tr class="tr11">
            <td class="td799" style="width: 42px; height: 20px; text-align: center">
   <input id="Button2" style="width: 80px" title="buton" type="button" value="Complete" onclick="javascript:icomplete('6.4');" /><br />  
                6.4</td>
            <td class="td800" style="height: 20px">
                &nbsp;</td>
            <td class="td801" colspan="2" style="width: 249px; height: 20px">
                EU ETS emissions in footprint year</td>
            <td class="td713" style="width: 317px; height: 20px; text-align: center">
                <em><font class="ft67">
                    <input id="Text36" style="width: 88px" type="text" /></font></em></td>
            <td class="td802" style="width: 144px; font-style: italic; height: 20px">
            </td>
            <td class="td803" style="width: 148px; height: 20px">
            </td>
            <td class="td803" style="width: 127px; height: 20px; text-align: center">
                <input id="Text37" style="width: 88px" type="text" /></td>
        </tr>
    </table>
    <br />
    <table cellpadding="0" cellspacing="0" class="t6" style="border-top-style: solid;
        border-right-style: solid; border-left-style: solid; border-bottom-style: solid">
        <tr class="tr11">
            <td class="td815" style="width: 42px; text-align: center">
<input id="Button2" style="width: 80px" title="buton" type="button" value="Complete" onclick="javascript:icomplete('6.5');" /><br />              
                <font class="ft54"><strong>6.5</strong></font></td>
            <td class="td816" style="font-weight: bold; width: 254px">
                <font class="ft54">Electricity credits</font></td>
            <td class="td713" style="font-weight: bold; width: 317px">
            </td>
            <td class="td817" style="font-weight: bold; width: 146px">
            </td>
            <td class="td714" style="font-weight: bold; width: 148px">
            </td>
            <td class="td539" style="font-weight: bold; width: 129px">
            </td>
        </tr>
        <tr class="tr22" style="font-weight: bold">
            <td class="td715" style="width: 42px">
                &nbsp;</td>
            <td class="td818" style="width: 254px">
                &nbsp;</td>
            <td class="td808" style="width: 317px">
                &nbsp;</td>
            <td class="td819" style="width: 146px">
            </td>
            <td class="td718" style="width: 148px">
            </td>
            <td class="td545" style="width: 129px">
            </td>
        </tr>
        <tr class="tr2">
            <td class="td723" style="width: 42px">
                &nbsp;</td>
            <td class="td820" style="width: 254px">
                Electricity sources � export</td>
            <td class="td734" style="width: 317px; text-align: center">
                <font class="ft67"><em>
                    <select id="Select3" name="" style="width: 60px">
                        <option selected="selected" value="Yes">Yes</option>
                        <option value="N/A">N/A</option>
                    </select>
                </em></font>
            </td>
            <td class="td821" style="width: 146px; font-style: italic">
                &nbsp;</td>
            <td class="td726" style="width: 148px">
                &nbsp;</td>
            <td class="td822" style="width: 129px; text-align: center">
                &nbsp;<input id="Text13" style="width: 88px" type="text" /></td>
        </tr>
        <tr class="tr10">
            <td class="td723" style="width: 42px">
                &nbsp;</td>
            <td class="td820" style="width: 254px">
                meter identifiers with evidence</td>
            <td class="td725" style="width: 317px">
                &nbsp;</td>
            <td class="td821" style="width: 146px">
                &nbsp;</td>
            <td class="td726" style="width: 148px">
                &nbsp;</td>
            <td class="td822" style="width: 129px">
                &nbsp;</td>
        </tr>
        <tr class="tr19">
            <td class="td727" style="width: 42px">
                &nbsp;</td>
            <td class="td823" style="width: 254px">
                <font class="ft61">of electricity credit eligibility</font></td>
            <td class="td728" style="width: 317px">
                &nbsp;</td>
            <td class="td824" style="width: 146px">
                &nbsp;</td>
            <td class="td729" style="width: 148px">
                &nbsp;</td>
            <td class="td825" style="width: 129px">
                &nbsp;</td>
        </tr>
        <tr class="tr2">
            <td class="td723" style="width: 42px">
            </td>
            <td class="td820" style="width: 254px">
            </td>
            <td class="td734" style="width: 317px">
            </td>
            <td class="td821" style="width: 146px; font-style: italic">
            </td>
            <td class="td726" style="width: 148px">
            </td>
            <td class="td822" style="width: 129px">
            </td>
        </tr>
        <tr class="tr2">
            <td class="td723" style="width: 42px">
                &nbsp;</td>
            <td class="td820" style="width: 254px">
                Total generated (includes self-supply)</td>
            <td class="td734" style="width: 317px; text-align: center">
                <font class="ft67"><em>
                    <input id="Text18" style="width: 88px" type="text" /></em></font></td>
            <td class="td821" style="width: 146px; font-style: italic">
                &nbsp;</td>
            <td class="td726" style="width: 148px">
                &nbsp;</td>
            <td class="td822" style="width: 129px; text-align: center">
                &nbsp;<input id="Text14" style="width: 88px" type="text" /></td>
        </tr>
        <tr class="tr22">
            <td class="td715" style="width: 42px">
                &nbsp;</td>
            <td class="td823" style="width: 254px">
                <font class="ft58">&nbsp;</font></td>
            <td class="td808" style="width: 317px">
                &nbsp;</td>
            <td class="td826" style="width: 146px">
                &nbsp;</td>
            <td class="td827" style="width: 148px">
                &nbsp;</td>
            <td class="td828" style="width: 129px">
                &nbsp;</td>
        </tr>
        <tr class="tr2">
            <td class="td723" style="width: 42px">
            </td>
            <td class="td820" style="width: 254px">
            </td>
            <td class="td734" style="width: 317px">
            </td>
            <td class="td821" style="width: 146px; font-style: italic">
            </td>
            <td class="td726" style="width: 148px">
            </td>
            <td class="td822" style="width: 129px">
            </td>
        </tr>
        <tr class="tr2">
            <td class="td723" style="width: 42px">
                &nbsp;</td>
            <td class="td820" style="width: 254px">
                Total exported by meter</td>
            <td class="td734" style="width: 317px; text-align: center">
                <font class="ft67"><em>
                    <input id="Text19" style="width: 88px" type="text" /></em></font></td>
            <td class="td821" style="width: 146px; font-style: italic">
                &nbsp;</td>
            <td class="td726" style="width: 148px">
                &nbsp;</td>
            <td class="td822" style="width: 129px; text-align: center">
                &nbsp;<input id="Text15" style="width: 88px" type="text" /></td>
        </tr>
        <tr class="tr19">
            <td class="td727" style="width: 42px">
                &nbsp;</td>
            <td class="td823" style="width: 254px">
                <font class="ft61">identifier (grid/other users)</font></td>
            <td class="td728" style="width: 317px">
                &nbsp;</td>
            <td class="td824" style="width: 146px">
                &nbsp;</td>
            <td class="td729" style="width: 148px">
                &nbsp;</td>
            <td class="td825" style="width: 129px">
                &nbsp;</td>
        </tr>
        <tr class="tr2">
            <td class="td723" style="width: 42px">
            </td>
            <td class="td820" style="width: 254px">
            </td>
            <td class="td734" style="width: 317px">
            </td>
            <td class="td821" style="width: 146px; font-style: italic">
            </td>
            <td class="td726" style="width: 148px">
            </td>
            <td class="td822" style="width: 129px">
            </td>
        </tr>
        <tr class="tr2">
            <td class="td723" style="width: 42px">
                &nbsp;</td>
            <td class="td820" style="width: 254px">
                Grid average emissions factor</td>
            <td class="td734" style="width: 317px; text-align: center">
                <input id="Text20" style="width: 88px" type="text" /></td>
            <td class="td821" style="width: 146px; font-style: italic">
                &nbsp;</td>
            <td class="td726" style="width: 148px">
                &nbsp;</td>
            <td class="td822" style="width: 129px; text-align: center">
                &nbsp;<input id="Text16" style="width: 88px" type="text" /></td>
        </tr>
        <tr class="tr22">
            <td class="td715" style="width: 42px">
                &nbsp;</td>
            <td class="td823" style="width: 254px">
                <font class="ft58">(tCO</font><font class="ft92"><span style="font-size: 6pt">2</span></font><font
                    class="ft58">/MWh)</font></td>
            <td class="td808" style="width: 317px">
                &nbsp;</td>
            <td class="td826" style="width: 146px">
                &nbsp;</td>
            <td class="td827" style="width: 148px">
                &nbsp;</td>
            <td class="td828" style="width: 129px">
                &nbsp;</td>
        </tr>
        <tr class="tr2">
            <td class="td723" style="width: 42px">
            </td>
            <td class="td820" style="width: 254px">
            </td>
            <td class="td734" style="width: 317px">
            </td>
            <td class="td821" style="font-size: 6pt; width: 146px; font-style: italic">
            </td>
            <td class="td726" style="width: 148px">
            </td>
            <td class="td822" style="width: 129px">
            </td>
        </tr>
        <tr class="tr2">
            <td class="td723" style="width: 42px">
                &nbsp;</td>
            <td class="td820" style="width: 254px">
                Calculated emissions credit</td>
            <td class="td734" style="width: 317px; text-align: center">
                <em><font class="ft67">
                    <input id="Text35" style="width: 88px" type="text" /></font></em></td>
            <td class="td821" style="font-size: 6pt; width: 146px; font-style: italic">
                &nbsp;</td>
            <td class="td726" style="width: 148px">
                &nbsp;</td>
            <td class="td822" style="width: 129px; text-align: center">
                &nbsp;<input id="Text17" style="width: 88px" type="text" /></td>
        </tr>
        <tr class="tr22">
            <td class="td715" style="width: 42px">
                &nbsp;</td>
            <td class="td823" style="width: 254px">
                <font class="ft58">(generated x grid factor)</font></td>
            <td class="td808" style="width: 317px">
                &nbsp;</td>
            <td class="td826" style="width: 146px">
                &nbsp;</td>
            <td class="td827" style="width: 148px">
                &nbsp;</td>
            <td class="td828" style="width: 129px">
                &nbsp;</td>
        </tr>
    </table>
    <br />
    <table cellpadding="0" cellspacing="0" class="t6" style="border-top-style: solid;
        border-right-style: solid; border-left-style: solid; border-bottom-style: solid">
        <tr class="tr11">
            <td class="td514" style="width: 42px; text-align: center">
<input id="Button2" style="width: 80px" title="buton" type="button" value="Complete" onclick="javascript:icomplete('6.6');" /><br />              
                <font class="ft54"><strong>6.6</strong></font></td>
            <td class="td829" style="font-weight: bold; width: 252px">
                <font class="ft54">Residual measurement list</font></td>
            <td class="td830" style="font-weight: bold; width: 319px">
            </td>
            <td class="td751" style="font-weight: bold; width: 144px">
            </td>
            <td class="td831" style="font-weight: bold; width: 149px">
            </td>
            <td class="td517" style="font-weight: bold; width: 128px">
            </td>
        </tr>
        <tr class="tr19" style="font-weight: bold">
            <td class="td669" style="width: 42px">
                &nbsp;</td>
            <td class="td832" style="width: 252px">
                &nbsp;</td>
            <td class="td833" style="width: 319px">
                &nbsp;</td>
            <td class="td834" style="width: 144px">
            </td>
            <td class="td835" style="width: 149px">
            </td>
            <td class="td523" style="width: 128px">
            </td>
        </tr>
        <tr class="tr2">
            <td class="td529" style="width: 42px">
                &nbsp;</td>
            <td class="td836" style="width: 252px">
                Have you compiled a residual</td>
            <td class="td837" style="width: 319px; text-align: center">
                <font class="ft67"><em>
                    <select id="Select4" style="width: 60px">
                        <option selected="selected" value="Yes">Yes</option>
                        <option value="No">No</option>
                    </select>
                </em></font>
            </td>
            <td class="td759" style="width: 144px; font-style: italic">
                &nbsp;</td>
            <td class="td227" style="width: 149px">
                &nbsp;</td>
            <td class="td532" style="width: 128px; text-align: center">
                &nbsp;<input id="Text28" style="width: 88px" type="text" /></td>
        </tr>
        <tr class="tr10">
            <td class="td529" style="width: 42px">
                &nbsp;</td>
            <td class="td836" style="width: 252px">
                measurement list � if so</td>
            <td class="td838" style="width: 319px">
                &nbsp;</td>
            <td class="td759" style="width: 144px">
                &nbsp;</td>
            <td class="td227" style="width: 149px">
                &nbsp;</td>
            <td class="td532" style="width: 128px">
                &nbsp;</td>
        </tr>
        <tr class="tr10">
            <td class="td529" style="width: 42px">
                &nbsp;</td>
            <td class="td836" style="width: 252px">
                please supply this as a schedule</td>
            <td class="td838" style="width: 319px; text-align: center">
                &nbsp;<input id="Text11" style="width: 88px" type="text" /></td>
            <td class="td759" style="width: 144px">
                &nbsp;</td>
            <td class="td227" style="width: 149px">
                &nbsp;</td>
            <td class="td532" style="width: 128px; text-align: center">
                &nbsp;<input id="Text12" style="width: 88px" type="text" /></td>
        </tr>
        <tr class="tr19">
            <td class="td669" style="width: 42px">
                &nbsp;</td>
            <td class="td839" style="width: 252px">
                <font class="ft61">&nbsp;</font></td>
            <td class="td833" style="width: 319px">
                &nbsp;</td>
            <td class="td840" style="width: 144px">
                &nbsp;</td>
            <td class="td223" style="width: 149px">
                &nbsp;</td>
            <td class="td841" style="width: 128px">
                &nbsp;</td>
        </tr>
        <tr class="tr2">
            <td class="td529" style="width: 42px; height: 21px">
                &nbsp;</td>
            <td class="td836" style="width: 252px; height: 21px">
                Regulated emissions, as a</td>
            <td class="td837" style="width: 319px; height: 21px; text-align: center">
            </td>
            <td class="td759" style="width: 144px; height: 21px">
                &nbsp;</td>
            <td class="td227" style="width: 149px; height: 21px">
                &nbsp;</td>
            <td class="td532" style="width: 128px; height: 21px; text-align: center">
                &nbsp;</td>
        </tr>
        <tr class="tr10">
            <td class="td529" style="width: 42px">
                &nbsp;</td>
            <td class="td836" style="width: 252px">
                percentage of total footprint emissions</td>
            <td class="td838" style="width: 319px; text-align: center">
                &nbsp;<input id="Text24" style="width: 88px" type="text" /></td>
            <td class="td759" style="width: 144px">
                &nbsp;</td>
            <td class="td227" style="width: 149px">
                &nbsp;</td>
            <td class="td532" style="width: 128px; text-align: center">
                &nbsp;<input id="Text30" style="width: 88px" type="text" /></td>
        </tr>
        <tr class="tr19">
            <td class="td669" style="width: 42px">
                &nbsp;</td>
            <td class="td839" style="width: 252px">
                <font class="ft61"></font>
            </td>
            <td class="td833" style="width: 319px">
                &nbsp;</td>
            <td class="td840" style="width: 144px">
                &nbsp;</td>
            <td class="td223" style="width: 149px">
                &nbsp;</td>
            <td class="td841" style="width: 128px">
                &nbsp;</td>
        </tr>
        <tr class="tr2">
            <td class="td529" style="width: 42px; height: 57px">
                &nbsp;</td>
            <td class="td836" style="width: 252px; height: 57px">
                EU ETS emissions � regulated emissions as a percentage of total footprint emissions</td>
            <td class="td837" style="width: 319px; height: 57px; text-align: center">
                <input id="Text25" style="width: 88px" type="text" /></td>
            <td class="td759" style="width: 144px; height: 57px">
                &nbsp;</td>
            <td class="td227" style="width: 149px; height: 57px">
                &nbsp;</td>
            <td class="td532" style="width: 128px; height: 57px; text-align: center">
                <input id="Text31" style="width: 88px" type="text" /></td>
        </tr>
        <tr class="tr10">
            <td class="td529" style="width: 42px; height: 23px">
                &nbsp;</td>
            <td class="td836" style="width: 252px; height: 23px">
                &nbsp;</td>
            <td class="td838" style="width: 319px; height: 23px; text-align: center">
                &nbsp;</td>
            <td class="td759" style="width: 144px; height: 23px">
                &nbsp;</td>
            <td class="td227" style="width: 149px; height: 23px">
                &nbsp;</td>
            <td class="td532" style="width: 128px; height: 23px">
                &nbsp;</td>
        </tr>
        <tr class="tr2">
            <td class="td529" style="width: 42px; height: 19px">
                &nbsp;</td>
            <td class="td836" style="width: 252px; height: 19px">
                CCA emissions outside of EU</td>
            <td class="td837" style="width: 319px; height: 19px; text-align: center">
            </td>
            <td class="td759" style="width: 144px; height: 19px">
                &nbsp;</td>
            <td class="td227" style="width: 149px; height: 19px">
                &nbsp;</td>
            <td class="td532" style="width: 128px; height: 19px; text-align: center">
                &nbsp;</td>
        </tr>
        <tr class="tr10">
            <td class="td529" style="width: 42px">
                &nbsp;</td>
            <td class="td836" style="width: 252px">
                ETS as a percentage of total footprint emissions</td>
            <td class="td838" style="width: 319px; text-align: center">
                &nbsp;<input id="Text26" style="width: 88px" type="text" /></td>
            <td class="td759" style="width: 144px">
                &nbsp;</td>
            <td class="td227" style="width: 149px">
                &nbsp;</td>
            <td class="td532" style="width: 128px; text-align: center">
                &nbsp;<input id="Text32" style="width: 88px" type="text" /></td>
        </tr>
        <tr class="tr19">
            <td class="td669" style="width: 42px">
                &nbsp;</td>
            <td class="td839" style="width: 252px">
                <font class="ft61">&nbsp;</font></td>
            <td class="td833" style="width: 319px">
                &nbsp;</td>
            <td class="td840" style="width: 144px">
                &nbsp;</td>
            <td class="td223" style="width: 149px">
                &nbsp;</td>
            <td class="td841" style="width: 128px">
                &nbsp;</td>
        </tr>
        <tr class="tr2">
            <td class="td529" style="width: 42px">
                &nbsp;</td>
            <td class="td836" style="width: 252px">
                Core emissions outside of EU</td>
            <td class="td837" style="width: 319px; text-align: center">
            </td>
            <td class="td759" style="width: 144px">
                &nbsp;</td>
            <td class="td227" style="width: 149px">
                &nbsp;</td>
            <td class="td532" style="width: 128px; text-align: center">
                &nbsp;</td>
        </tr>
        <tr class="tr10">
            <td class="td529" style="width: 42px">
                &nbsp;</td>
            <td class="td836" style="width: 252px">
                ETS and CCAs as a percentage of total footprint emissions</td>
            <td class="td838" style="width: 319px; text-align: center">
                &nbsp;<input id="Text38" style="width: 88px" type="text" /></td>
            <td class="td759" style="width: 144px">
                &nbsp;</td>
            <td class="td227" style="width: 149px">
                &nbsp;</td>
            <td class="td532" style="width: 128px; text-align: center">
                &nbsp;<input id="Text39" style="width: 88px" type="text" /></td>
        </tr>
        <tr class="tr10">
            <td class="td529" style="width: 42px">
                &nbsp;</td>
            <td class="td836" style="width: 252px">
                &nbsp;</td>
            <td class="td838" style="width: 319px; text-align: center">
                &nbsp;</td>
            <td class="td759" style="width: 144px">
                &nbsp;</td>
            <td class="td227" style="width: 149px">
                &nbsp;</td>
            <td class="td532" style="width: 128px; text-align: center">
                &nbsp;</td>
        </tr>
        <tr class="tr2">
            <td class="td529" style="width: 42px">
            </td>
            <td class="td836" style="width: 252px">
            </td>
            <td class="td837" style="width: 319px">
            </td>
            <td class="td759" style="width: 144px">
            </td>
            <td class="td227" style="width: 149px">
            </td>
            <td class="td532" style="width: 128px">
            </td>
        </tr>
        <tr class="tr2">
            <td class="td529" style="width: 42px">
                &nbsp;</td>
            <td class="td836" style="width: 252px">
                Residual supplies as a</td>
            <td class="td837" style="width: 319px; text-align: center">
            </td>
            <td class="td759" style="width: 144px">
                &nbsp;</td>
            <td class="td227" style="width: 149px">
                &nbsp;</td>
            <td class="td532" style="width: 128px; text-align: center">
                &nbsp;</td>
        </tr>
        <tr class="tr10">
            <td class="td529" style="width: 42px; height: 24px">
                &nbsp;</td>
            <td class="td836" style="width: 252px; height: 24px">
                percentage of total footprint emissions</td>
            <td class="td838" style="width: 319px; height: 24px; text-align: center">
                &nbsp;<input id="Text27" style="width: 88px" type="text" /></td>
            <td class="td759" style="width: 144px; height: 24px">
                &nbsp;</td>
            <td class="td227" style="width: 149px; height: 24px">
                &nbsp;</td>
            <td class="td532" style="width: 128px; height: 24px; text-align: center">
                &nbsp;<input id="Text34" style="width: 88px" type="text" /></td>
        </tr>
        <tr class="tr19">
            <td class="td669" style="width: 42px">
                &nbsp;</td>
            <td class="td839" style="width: 252px">
                <font class="ft61">&nbsp;</font></td>
            <td class="td833" style="width: 319px">
                &nbsp;</td>
            <td class="td840" style="width: 144px">
                &nbsp;</td>
            <td class="td223" style="width: 149px">
                &nbsp;</td>
            <td class="td841" style="width: 128px">
                &nbsp;</td>
        </tr>
        <tr class="tr2">
            <td class="td529" style="width: 42px">
                &nbsp;</td>
            <td class="td836" style="width: 252px">
                Regulated emissions<font class="ft93"><span style="font-size: 6pt">3 </span></font>
                as a</td>
            <td class="td837" style="width: 319px; text-align: center">
            </td>
            <td class="td759" style="width: 144px">
                &nbsp;</td>
            <td class="td227" style="width: 149px">
                &nbsp;</td>
            <td class="td532" style="width: 128px; text-align: center">
                &nbsp;</td>
        </tr>
        <tr class="tr10">
            <td class="td529" style="width: 42px; height: 24px">
                &nbsp;</td>
            <td class="td836" style="width: 252px; height: 24px">
                percentage of total footprint emissions</td>
            <td class="td838" style="width: 319px; height: 24px; text-align: center">
                &nbsp;<input id="Text23" style="width: 88px" type="text" /></td>
            <td class="td759" style="width: 144px; height: 24px">
                &nbsp;</td>
            <td class="td227" style="width: 149px; height: 24px">
                &nbsp;</td>
            <td class="td532" style="width: 128px; height: 24px; text-align: center">
                &nbsp;<input id="Text29" style="width: 88px" type="text" /></td>
        </tr>
        <tr class="tr19">
            <td class="td669" style="width: 42px">
                &nbsp;</td>
            <td class="td839" style="width: 252px">
                <font class="ft61">&nbsp;</font></td>
            <td class="td833" style="width: 319px">
                &nbsp;</td>
            <td class="td840" style="width: 144px">
                &nbsp;</td>
            <td class="td223" style="width: 149px">
                &nbsp;</td>
            <td class="td841" style="width: 128px">
                &nbsp;</td>
        </tr>
    </table>
    <br />
    <br />

</body>
</html>
