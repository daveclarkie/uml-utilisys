ECONode = function (id, pid, dsc, w, h, c, bc, target, meta, mything)
{

	this.id = id;
	this.pid = pid;
	this.dsc = dsc;

	this.w = w;
	this.h = h;
	this.c = c;
	this.bc = bc;
	this.target = target;
	this.meta = meta;
	this.mything = mything;


	this.siblingIndex = 0;
	this.dbIndex = 0;
	
	this.XPosition = 0;
	this.YPosition = 0;
	this.prelim = 0;
	this.modifier = 0;
	this.leftNeighbor = null;
	this.rightNeighbor = null;
	this.nodeParent = null;	
	this.nodeChildren = [];
	
	this.isCollapsed = false;
	this.canCollapse = false;
	
	this.isSelected = false;
}


ECONode.prototype._getLevel = function ()
{
	alert("me 1");
	
	if (this.nodeParent.id == -1)
	{return 0;}
	else return this.nodeParent._getLevel() + 1;
}

ECONode.prototype._isAncestorCollapsed = function () {
	if (this.nodeParent.isCollapsed) { return true; }
	else 
	{
		if (this.nodeParent.id == -1) { return false; }
		else	{ return this.nodeParent._isAncestorCollapsed(); }
	}
}

ECONode.prototype._setAncestorsExpanded = function () {
	if (this.nodeParent.id == -1) { return; }
	else 
	{
		this.nodeParent.isCollapsed = false;
		return this.nodeParent._setAncestorsExpanded(); 
	}	
}

ECONode.prototype._getChildrenCount = function () {
	if (this.isCollapsed) return 0;
    if(this.nodeChildren == null)
        return 0;
    else
        return this.nodeChildren.length;
}

ECONode.prototype._getLeftSibling = function ()
{
if(this.leftNeighbor != null && this.leftNeighbor.nodeParent == this.nodeParent)
return this.leftNeighbor;
    else
        return null;	
}

ECONode.prototype._getRightSibling = function () {
    if(this.rightNeighbor != null && this.rightNeighbor.nodeParent == this.nodeParent)
        return this.rightNeighbor;
    else
        return null;	
}

ECONode.prototype._getChildAt = function (i) {
	return this.nodeChildren[i];
}

ECONode.prototype._getChildrenCenter = function (tree) {
    node = this._getFirstChild();
    node1 = this._getLastChild();
    return node.prelim + ((node1.prelim - node.prelim) + tree._getNodeSize(node1)) / 2;	
}

ECONode.prototype._getFirstChild = function () {
	return this._getChildAt(0);
}

ECONode.prototype._getLastChild = function () {
	return this._getChildAt(this._getChildrenCount() - 1);
}

ECONode.prototype._drawChildrenLinks = function (tree) 
{
	var s = [];
	var xa = 0, ya = 0, xb = 0, yb = 0, xc = 0, yc = 0, xd = 0, yd = 0;
	var node1 = null;
	switch(tree.config.iRootOrientation)
	{
		case ECOTree.RO_TOP:
			xa = this.XPosition + (this.w / 2);
			ya = this.YPosition + this.h;
// keep alert("ECOTree.RO_TOP:" +  ' xa ' + xa  + ' ya ' + ya);
break;
			
		case ECOTree.RO_BOTTOM:
			xa = this.XPosition + (this.w / 2);
			ya = this.YPosition;
//alert("ECOTree.RO_BOTTOM:");
break;
			
		case ECOTree.RO_RIGHT:
			xa = this.XPosition;
			ya = this.YPosition + (this.h / 2);		
//alert("ECOTree.RO_RIGHT:");
break;
			
		case ECOTree.RO_LEFT:
			xa = this.XPosition + this.w;
			ya = this.YPosition + (this.h / 2);		
//alert(" ECOTree.RO_LEFT:");
break;		
	}
	
	for (var k = 0; k < this.nodeChildren.length; k++)
	{
		node1 = this.nodeChildren[k];
				
		switch(tree.config.iRootOrientation)
		{
			case ECOTree.RO_TOP:
				xd = xc = node1.XPosition + (node1.w / 2);
				yd = node1.YPosition;
				xb = xa;
				switch (tree.config.iNodeJustification)
				{
					case ECOTree.NJ_TOP:
						yb = yc = yd - tree.config.iLevelSeparation / 2;
						
			 	//alert(  "yb " + yb)
						
						break;
					case ECOTree.NJ_BOTTOM:
						yb = yc = ya + tree.config.iLevelSeparation / 2;
						break;
					case ECOTree.NJ_CENTER:
						yb = yc = ya + (yd - ya) / 2;
						break;
				}
				break;
				
			case ECOTree.RO_BOTTOM:
				xd = xc = node1.XPosition + (node1.w / 2);
				yd = node1.YPosition + node1.h;
				xb = xa;
				switch (tree.config.iNodeJustification)
				{
					case ECOTree.NJ_TOP:
						yb = yc = yd + tree.config.iLevelSeparation / 2;
						
						alert("here as well")
						
						break;
					case ECOTree.NJ_BOTTOM:
						yb = yc = ya - tree.config.iLevelSeparation / 2;
						break;
					case ECOTree.NJ_CENTER:
						yb = yc = yd + (ya - yd) / 2;
						break;
				}				
				break;

			case ECOTree.RO_RIGHT:
				xd = node1.XPosition + node1.w;
				yd = yc = node1.YPosition + (node1.h / 2);	
				yb = ya;
				switch (tree.config.iNodeJustification)
				{
					case ECOTree.NJ_TOP:
						xb = xc = xd + tree.config.iLevelSeparation / 2;
						
						alert(" also here")
						
						break;
					case ECOTree.NJ_BOTTOM:
						xb = xc = xa - tree.config.iLevelSeparation / 2;
						break;
					case ECOTree.NJ_CENTER:
						xb = xc = xd + (xa - xd) / 2;
						break;
				}								
				break;		
				
			case ECOTree.RO_LEFT:
				xd = node1.XPosition;
				yd = yc = node1.YPosition + (node1.h / 2);		
				yb = ya;
				switch (tree.config.iNodeJustification)
				{
					case ECOTree.NJ_TOP:
						xb = xc = xd - tree.config.iLevelSeparation / 2;
						break;
					case ECOTree.NJ_BOTTOM:
						xb = xc = xa + tree.config.iLevelSeparation / 2;
						break;
					case ECOTree.NJ_CENTER:
						xb = xc = xa + (xd - xa) / 2;
						break;
				}								
				break;				
		}		
		
		
		switch(tree.render)
		{
			case "CANVAS":
				tree.ctx.save();
				tree.ctx.strokeStyle = tree.config.linkColor;
				tree.ctx.beginPath();			
				switch (tree.config.linkType)
				{
					case "M":						
						tree.ctx.moveTo(xa,ya);
						tree.ctx.lineTo(xb,yb);
						tree.ctx.lineTo(xc,yc);
						tree.ctx.lineTo(xd,yd);						
						break;
						
					case "B":
						tree.ctx.moveTo(xa,ya);
						tree.ctx.bezierCurveTo(xb,yb,xc,yc,xd,yd);	
						break;					
				}
				tree.ctx.stroke();
				tree.ctx.restore();
				break;
											
			case "VML":
                 // keep	alert(tree.config.linkType);
				switch (tree.config.linkType)
				{
					case "M":
						s.push('<v:polyline  points="');
						s.push(xa + ' ' + ya + ' ' + xb + ' ' + yb + ' ' + xc + ' ' + yc + ' ' + xd + ' ' + yd); 		
						s.push('" strokecolor="'+tree.config.linkColor+'"><v:fill on="false" /></v:polyline>');	

 

				
                               // here it is









					break;
					case "B":
						s.push('<v:curve from="');
						s.push(xa + ' ' + ya + '" control1="' + xb + ' ' + yb + '" control2="' + xc + ' ' + yc + '" to="' + xd + ' ' + yd); 		
						s.push('" strokecolor="'+tree.config.linkColor+'"><v:fill on="false" /></v:curve>');					
						break;					
				}
				break;
				
		}			
	}	
	
	return s.join('');
}

ECOTree = function (obj, elm) 
{
	this.config = {
		iMaxDepth : 10000,
		iLevelSeparation : 40,
		iSiblingSeparation : 20,
		iSubtreeSeparation : 20,
		iRootOrientation : ECOTree.RO_TOP,
		iNodeJustification : ECOTree.NJ_TOP,
		topXAdjustment : 0,
		topYAdjustment : 0,		
		render : "AUTO",
 		linkType : "M",
		//linkType : "B",

		linkColor : "green",
		nodeColor : "lightgray",
 //ECOTree.NF_FLAT
nodeFill : ECOTree.NF_FLAT,		
//nodeFill : ECOTree.NF_GRADIENT,
		nodeBorderColor : "lightgrey",
		nodeSelColor : "#FFFFCC",
		
		levelColors : ["white","white","white","white"],
		
		levelBorderColors : ["orange","orange","orange","orange"],
		
		colorStyle : ECOTree.CS_NODE,
		useTarget : false,
		searchMode : ECOTree.SM_DSC,
		selectMode : ECOTree.SL_MULTIPLE,
		defaultNodeWidth : 110,
		defaultNodeHeight : 80,
		defaultTarget : 'javascript:void(0);',
		expandedImage : './img/less.gif',
		collapsedImage : './img/plus.gif',
		transImage : './img/trans.gif'
	}
	
	this.version = "1.1";
	this.obj = obj;
	this.elm = document.getElementById(elm);
	this.self = this;
	this.render = (this.config.render == "AUTO" ) ? ECOTree._getAutoRenderMode() : this.config.render;
	this.ctx = null;
	this.canvasoffsetTop = 0;
	this.canvasoffsetLeft = 0;
	
	this.maxLevelHeight = [];
	this.maxLevelWidth = [];
	this.previousLevelNode = [];
	
	this.rootYOffset = 0;
	this.rootXOffset = 0;
	
	this.nDatabaseNodes = [];
	this.mapIDs = {};
	
	this.root = new ECONode(-1, null, null, 2, 2);
	this.iSelectedNode = -1;
	this.iLastSearch = 0;
	
}

//Constant values

//Tree orientation
ECOTree.RO_TOP = 0;
ECOTree.RO_BOTTOM = 1;
ECOTree.RO_RIGHT = 2;
ECOTree.RO_LEFT = 3;

//Level node alignment
ECOTree.NJ_TOP = 0;
ECOTree.NJ_CENTER = 1;
ECOTree.NJ_BOTTOM = 2;

//Node fill type
ECOTree.NF_GRADIENT = 0;
ECOTree.NF_FLAT = 1;

//Colorizing style
ECOTree.CS_NODE = 0;
ECOTree.CS_LEVEL = 1;

//Search method: Title, metadata or both
ECOTree.SM_DSC = 0;
ECOTree.SM_META = 1;
ECOTree.SM_BOTH = 2;

//Selection mode: single, multiple, no selection
ECOTree.SL_MULTIPLE = 0;
ECOTree.SL_SINGLE = 1;
ECOTree.SL_NONE = 2;


ECOTree._getAutoRenderMode = function() {
	var r = "VML";
	var is_ie6 = /msie 6\.0/i.test(navigator.userAgent);
	var is_ff = /Firefox/i.test(navigator.userAgent);	
	if (is_ff) r = "CANVAS";
	return r;
}

//CANVAS functions...
ECOTree._roundedRect = function (ctx,x,y,width,height,radius) 
{
alert("here 2");

ctx.beginPath();
  ctx.moveTo(x,y+radius);
  ctx.lineTo(x,y+height-radius);
  ctx.quadraticCurveTo(x,y+height,x+radius,y+height);
  ctx.lineTo(x+width-radius,y+height);
  ctx.quadraticCurveTo(x+width,y+height,x+width,y+height-radius);
  ctx.lineTo(x+width,y+radius);
  ctx.quadraticCurveTo(x+width,y,x+width-radius,y);
  ctx.lineTo(x+radius,y);
  ctx.quadraticCurveTo(x,y,x,y+radius);
  ctx.fill();
  ctx.stroke();
}

ECOTree._canvasNodeClickHandler = function (tree,target,nodeid) {
	if (target != nodeid) return;
	tree.selectNode(nodeid,true);
}

//Layout algorithm
ECOTree._firstWalk = function (tree, node, level)
{

	var leftSibling = null;
        node.XPosition = 0;
        node.YPosition = 0;
        node.prelim = 0;
        node.modifier = 0;
        node.leftNeighbor = null;
        node.rightNeighbor = null;
        tree._setLevelHeight(node, level);
        tree._setLevelWidth(node, level);
        tree._setNeighbors(node, level);

if(node._getChildrenCount() == 0 || level == tree.config.iMaxDepth)
        {
            leftSibling = node._getLeftSibling();
            if(leftSibling != null)
                  {
                     node.prelim = leftSibling.prelim + tree._getNodeSize(leftSibling) + tree.config.iSiblingSeparation;
                     }
            else
                   {  node.prelim = 0;  }
        } 
        else
        {
            var n = node._getChildrenCount();
            for(var i = 0; i < n; i++)
            {
                var iChild = node._getChildAt(i);
             //      alert( level+1);
                ECOTree._firstWalk(tree, iChild, level + 1);
            }

            var midPoint = node._getChildrenCenter(tree);
         //   alert( node._getChildrenCenter(tree));
         //    alert( tree._getNodeSize(node)/2);
         // alert(" before  midPoint  " +  midPoint);         
            midPoint -= tree._getNodeSize(node) / 2;
        //  alert(" after  midPoint  " +  midPoint);

leftSibling = node._getLeftSibling();
            if(leftSibling != null)
            {
                node.prelim = leftSibling.prelim + tree._getNodeSize(leftSibling) + tree.config.iSiblingSeparation;
                node.modifier = node.prelim - midPoint;
              //   alert(node.prelim + ' ~ ' + node.modifier);

                ECOTree._apportion(tree, node, level);
            } 
            else
            {            	
                node.prelim = midPoint;
            }
        }	
}

ECOTree._apportion = function (tree, node, level) 
{
	// keep alert("ECOTree._apportion "  + level);
	
        var firstChild = node._getFirstChild();
        var firstChildLeftNeighbor = firstChild.leftNeighbor;
        var j = 1;
        for(var k = tree.config.iMaxDepth - level; firstChild != null && firstChildLeftNeighbor != null && j <= k;)
        {
            var modifierSumRight = 0;
            var modifierSumLeft = 0;
            var rightAncestor = firstChild;
            
					
			var leftAncestor = firstChildLeftNeighbor;
            for(var l = 0; l < j; l++)
            {
                rightAncestor = rightAncestor.nodeParent;
                leftAncestor = leftAncestor.nodeParent;
                modifierSumRight += rightAncestor.modifier;
                modifierSumLeft += leftAncestor.modifier;
            }

            var totalGap = (firstChildLeftNeighbor.prelim + modifierSumLeft + tree._getNodeSize(firstChildLeftNeighbor) + tree.config.iSubtreeSeparation) - (firstChild.prelim + modifierSumRight);
            if(totalGap > 0)
            {
                var subtreeAux = node;
                var numSubtrees = 0;
                for(; subtreeAux != null && subtreeAux != leftAncestor; subtreeAux = subtreeAux._getLeftSibling())
                    numSubtrees++;

                if(subtreeAux != null)
                {
                    var subtreeMoveAux = node;
                    var singleGap = totalGap / numSubtrees;
                    for(; subtreeMoveAux != leftAncestor; subtreeMoveAux = subtreeMoveAux._getLeftSibling())
                    {
                        subtreeMoveAux.prelim += totalGap;
                        subtreeMoveAux.modifier += totalGap;
                        totalGap -= singleGap;
                    }

                }
            }
            j++;
            if(firstChild._getChildrenCount() == 0)
                firstChild = tree._getLeftmost(node, 0, j);
            else
                firstChild = firstChild._getFirstChild();
            if(firstChild != null)
                firstChildLeftNeighbor = firstChild.leftNeighbor;
        }
}

ECOTree._secondWalk = function (tree, node, level, X, Y) 
{
// keep alert(level);

if(level <= tree.config.iMaxDepth)
        {
            var xTmp = tree.rootXOffset + node.prelim + X;
            var yTmp = tree.rootYOffset + Y;
            var maxsizeTmp = 0;
            var nodesizeTmp = 0;
            var flag = false;
            
            switch(tree.config.iRootOrientation)
            {            
	            case ECOTree.RO_TOP:
	   //     alert("here 5");
			
			
			case ECOTree.RO_BOTTOM:	        	            	    	
	                maxsizeTmp = tree.maxLevelHeight[level];
	                nodesizeTmp = node.h;	                
	                break;

	            case ECOTree.RO_RIGHT:
	            case ECOTree.RO_LEFT:            
	                maxsizeTmp = tree.maxLevelWidth[level];
	                flag = true;
	                nodesizeTmp = node.w;
	                break;
            }
            
			
			switch(tree.config.iNodeJustification)
            {
	            case ECOTree.NJ_TOP:
	                node.XPosition = xTmp;
	                node.YPosition = yTmp;
	                break;
	
	            case ECOTree.NJ_CENTER:
	                node.XPosition = xTmp;
	                node.YPosition = yTmp + (maxsizeTmp - nodesizeTmp) / 2;
	                break;
	
	            case ECOTree.NJ_BOTTOM:
	                node.XPosition = xTmp;
	                node.YPosition = (yTmp + maxsizeTmp) - nodesizeTmp;
	                break;
            }
            if(flag)
            {
                var swapTmp = node.XPosition;
                node.XPosition = node.YPosition;
                node.YPosition = swapTmp;
            }
            switch(tree.config.iRootOrientation)
            {
	            case ECOTree.RO_BOTTOM:
	                node.YPosition = -node.YPosition - nodesizeTmp;
	                break;
	
	            case ECOTree.RO_RIGHT:
	                node.XPosition = -node.XPosition - nodesizeTmp;
	                break;
            }
            if(node._getChildrenCount() != 0)
                ECOTree._secondWalk(tree, node._getFirstChild(), level + 1, X + node.modifier, Y + maxsizeTmp + tree.config.iLevelSeparation);
            var rightSibling = node._getRightSibling();
            if(rightSibling != null)
                ECOTree._secondWalk(tree, rightSibling, level, X, Y);
        }	
}

ECOTree.prototype._positionTree = function ()
{	
// keep alert("here 3");

this.maxLevelHeight = [];
	this.maxLevelWidth = [];			
	this.previousLevelNode = [];		
	ECOTree._firstWalk(this.self, this.root, 0);
	
	switch(this.config.iRootOrientation)
	{            
	    case ECOTree.RO_TOP:
	    case ECOTree.RO_LEFT: 
	    		this.rootXOffset = this.config.topXAdjustment + this.root.XPosition;
	    		this.rootYOffset = this.config.topYAdjustment + this.root.YPosition;
	        break;    
	        
	    case ECOTree.RO_BOTTOM:	
	    case ECOTree.RO_RIGHT:             
	    		this.rootXOffset = this.config.topXAdjustment + this.root.XPosition;
	    		this.rootYOffset = this.config.topYAdjustment + this.root.YPosition;
	}	
	
	ECOTree._secondWalk(this.self, this.root, 0, 0, 0);	
}

ECOTree.prototype._setLevelHeight = function (node, level) 
{	
// alert("here 4");
if (this.maxLevelHeight[level] == null) 
		this.maxLevelHeight[level] = 0;
    if(this.maxLevelHeight[level] < node.h)
        this.maxLevelHeight[level] = node.h;	
}

ECOTree.prototype._setLevelWidth = function (node, level) {
//alert("here 6");

if (this.maxLevelWidth[level] == null) 
		this.maxLevelWidth[level] = 0;
    if(this.maxLevelWidth[level] < node.w)
        this.maxLevelWidth[level] = node.w;		
}

ECOTree.prototype._setNeighbors = function(node, level) {
//alert("here 7");
node.leftNeighbor = this.previousLevelNode[level];
    if(node.leftNeighbor != null)
        node.leftNeighbor.rightNeighbor = node;
    this.previousLevelNode[level] = node;	
}

ECOTree.prototype._getNodeSize = function (node) {
    switch(this.config.iRootOrientation)
    {
    case ECOTree.RO_TOP: 
      // alert("here 8");
  
  case ECOTree.RO_BOTTOM: 
        return node.w;

    case ECOTree.RO_RIGHT: 
    case ECOTree.RO_LEFT: 
        return node.h;
    }
    return 0;
}

ECOTree.prototype._getLeftmost = function (node, level, maxlevel)
{
//alert(level + ' - ' + maxlevel);

if(level >= maxlevel) return node;
    if(node._getChildrenCount() == 0) return null;
    
    var n = node._getChildrenCount();
    for(var i = 0; i < n; i++)
    {
        var iChild = node._getChildAt(i);
        var leftmostDescendant = this._getLeftmost(iChild, level + 1, maxlevel);
        if(leftmostDescendant != null)
            return leftmostDescendant;
    }

    return null;	
}


ECOTree.prototype._mouseOver = function(dbindex,flagtoggle)
{

alert("here");

}


ECOTree.prototype._selectNodeInt = function (dbindex, flagToggle) 
{
	//alert("here 9");
	
	//document.getElementByID("Temp").value=windows.status;
	
	
/*	
	if (this.config.selectMode == ECOTree.SL_SINGLE)
	{
		if ((this.iSelectedNode != dbindex) && (this.iSelectedNode != -1))
		{
			this.nDatabaseNodes[this.iSelectedNode].isSelected = false;
		}		
		this.iSelectedNode = (this.nDatabaseNodes[dbindex].isSelected && flagToggle) ? -1 : dbindex;
	}	
	this.nDatabaseNodes[dbindex].isSelected = (flagToggle) ? !this.nDatabaseNodes[dbindex].isSelected : true;
*/	
	
		
}



ECOTree.prototype._collapseAllInt = function (flag) {
	alert(" here 10");
	var node = null;
	for (var n = 0; n < this.nDatabaseNodes.length; n++)
	{ 
		node = this.nDatabaseNodes[n];
		if (node.canCollapse) node.isCollapsed = flag;
	}	
	this.UpdateTree();
}

ECOTree.prototype._selectAllInt = function (flag) {
	alert(" here 11");
	var node = null;
	for (var k = 0; k < this.nDatabaseNodes.length; k++)
	{ 
		node = this.nDatabaseNodes[k];
		node.isSelected = flag;
	}	
	this.iSelectedNode = -1;
	this.UpdateTree();
}

ECOTree.prototype._drawTree = function () {
	var s = [];
	var node = null;
	var color = "";
	var border = "";

var number_of_companies=0 
			
//rhona
//s.push('<DIV style="display:;z-index:101;position:absolute;top:10px;left:10px;" ID="ToolBar"><button onclick="javascript:zoomin()" >Zoom +</button><button onclick="javascript:zoomout()" >- Zoom</button></DIV>');
//s.push('<DIV style="display:;z-index:101;position:absolute;top:10px;left:10px;" ID="ToolBar">')
//s.push ('</DIV>');
 


//s.push('<DIV style="display:;z-index:100;position:absolute;top:10px;left:10px;" ID="TheData"></DIV>');


	for (var n = 0; n < this.nDatabaseNodes.length; n++)
	{ 
		node = this.nDatabaseNodes[n];
		
		//alert(this.config.colorStyle);
		
		
		switch (this.config.colorStyle) 
		{
			case ECOTree.CS_NODE:
			
			//alert( 'john ' + ECOTree.CS_NODE);
			
				color = node.c;
				border = node.bc;
				break;
			case ECOTree.CS_LEVEL:
				var iColor = node._getLevel() % this.config.levelColors.length;
				color = this.config.levelColors[iColor];
				iColor = node._getLevel() % this.config.levelBorderColors.length;
				border = this.config.levelBorderColors[iColor];
				break;
		}
		
		if (!node._isAncestorCollapsed())
		{
			switch (this.render)
			{
				case "CANVAS":
					//Canvas part...
					this.ctx.save();
					this.ctx.strokeStyle = border;
					switch (this.config.nodeFill) {
						case ECOTree.NF_GRADIENT:							
							var lgradient = this.ctx.createLinearGradient(node.XPosition,0,node.XPosition+node.w,0);
							lgradient.addColorStop(0.0,((node.isSelected)?this.config.nodeSelColor:color));
							lgradient.addColorStop(1.0,"#F5FFF5");
							this.ctx.fillStyle = lgradient;
							break;
							
						case ECOTree.NF_FLAT:
							this.ctx.fillStyle = ((node.isSelected)?this.config.nodeSelColor:color);
							break;
					}					
					
					ECOTree._roundedRect(this.ctx,node.XPosition,node.YPosition,node.w,node.h,5);
					this.ctx.restore();
					
					//HTML part...
					s.push('<div id="' + node.id + '" class="econode" style="{top:'+(node.YPosition+this.canvasoffsetTop)+'; left:'+(node.XPosition+this.canvasoffsetLeft)+'; width:'+node.w+'; height:'+node.h+';}" ');
					if (this.config.selectMode != ECOTree.SL_NONE)											
						//s.push('onclick="javascript:ECOTree._canvasNodeClickHandler('+this.obj+',event.target.id,\''+node.id+'\');" ');										
					s.push('>');
					s.push('<font face=Verdana size=1>');					
					if (node.canCollapse) {
						s.push('<a id="c' + node.id + '" href="javascript:'+this.obj+'.collapseNode(\''+node.id+'\', true);" >');
						s.push('<img border=0 src="'+((node.isCollapsed) ? this.config.collapsedImage : this.config.expandedImage)+'" >');							
						s.push('</a>');
						s.push('<img src="'+this.config.transImage+'" >');						
					}					
					if (node.target && this.config.useTarget)
					{
						alert('here ');
                                               s.push('<a id="t' + node.id + '" href="'+node.target+'">');
						s.push(node.dsc);
						s.push('</a>');
					}				
					else
					{	alert('here 2');					
						s.push(node.dsc);
					}
					s.push('</font>');
					s.push('</div>');		
					break;
					
				case "VML":
	//s.push('<v:roundrect id="' + node.id + '" strokecolor="'+border+'" arcsize="0.18"	');

 
 //style="width:100;height:100" 
 // main rhona
if (node.dsc.indexOf(": Meter (P")>0) 
{
var iOP="BC" + node.id

// s.push('<v:roundrect onmouseOver="meRollOver(this,1,' + "BC" + node.id + ');" onmouseout="meone(this,2,' + "BC" + node.id + ');"   style="background-color:#ffffc0;" id="'   + node.id + '" strokecolor="'+border+'" arcsize="0.18"	');
  

 s.push('<v:roundrect onClick="meRollOverClick(this,1,' + "BC" + node.id + ');"  style="background-color:#ffffc0;" id="'   + node.id + '" strokecolor="'+border+'" arcsize="0.18"	');
  


// ON Clic
// s.push('<v:roundrect onDblClick="meone(this,1,' + "BC" + node.id + ');" id="'   + node.id + '" strokecolor="'+border+'" arcsize="0.18"	');


}
else

{
 var iOP="DC" + node.id;
 var xOP="FF" + node.id;
 var xcomp="GG" + node.id;

   if (node.dsc.indexOf("Site:")>0) 
   {
        s.push('<v:roundrect style="z-index:500;"   onClick="meSite(this,1,' + "DC" + node.id + ');" id="DC'   + node.id + '" strokecolor="'+border+'" arcsize="0.18"	');
   }
    else
   {
   if (node.dsc.indexOf("Company:")>0) 
   {
         number_of_companies++  
         s.push('<v:roundrect style="z-index:600;"   onClick="meCompany(this,1,' + "EC" + node.id + ');" id="EC'   + node.id + '" strokecolor="'+border+'" arcsize="0.18"	');
   }
    else
   {
     if (node.dsc.indexOf("Organisation:")>0)  
       {

//alert(number_of_companies);

s.push('<v:roundrect style="z-index:700;" onClick="meOrganisation(this,1,' + "GG" + node.id + ');" id="GG'+node.id+'" strokecolor="'+border+'" arcsize="0.18"');
       }
         else
       {
           s.push('<v:roundrect style="background-color:#ffffc0;" id="'   + node.id + '" strokecolor="'+border+'" arcsize="0.18"	');
       } 
  


   }
        s.push('<v:roundrect style="background-color:#ffffc0;" id="'   + node.id + '" strokecolor="'+border+'" arcsize="0.18"	');
   }
}

var desh = node.dsc.split("<BR>",1);
var tesh = node.dsc.split("|");
var itesh = node.dsc.split("*");
var mesh = node.dsc.replace(desh,"");
var TheDates

mesh = node.dsc.replace("^" ,"'");


//alert(mesh);

//mesh = mesh.replace("(Z2)" , ' onClick="javascript:justclickme()"'); 
//mesh = mesh.replace("(Z1)" , ' style="background-color:yellow;"'); 
 

var str=mesh
var iLen=str.length;
var my=str.indexOf("(Z1)") + 4 ; 
var mi=str.indexOf("(Z2)")   ;
var NIG=str.substring(my,mi)
var ti = NIG.replace("B1K" ,"<BR>").replace("B1K" ,"<BR>").replace("B1K" ,"<BR>").replace("B1K" ,"<BR>").replace("B1K" ,"<BR>").replace("B1K" ,"<BR>").replace("B1K" ,"<BR>").replace("B1K" ,"<BR>").replace("B1K" ,"<BR>").replace("B1K" ,"<BR>").replace("B1K" ,"<BR>").replace("B1K" ,"<BR>");
var otesh = itesh[1] 

s.push(' onClick="javascript:sjohn(\''+ otesh+ ',' + node.id + '\')"  style="position:absolute; top:'+node.YPosition+'; left:'+node.XPosition+'; width:'+node.w+'; height:'+node.h+'" ');



 
					if (this.config.selectMode != ECOTree.SL_NONE)
						s.push('href="javascript:'+this.obj+'.selectNode(\''+node.id+'\', true);" ');										
					s.push('>');

					s.push('<v:textbox inset="0.5px,0.5px,0.5px,0.5px" ><font style="font-color:green;font-family:Arial;font-size:10px;">');

	//s.push('<a href="javascript:'+this.obj+'.collapseNode(\''+node.id+'\', true);" >');
	//s.push('<img border=1 height="15px" width="15px" src="'+((node.isCollapsed) ? this.config.collapsedImage : this.config.expandedImage)+'" >');
	//s.push('</a>');
	//s.push('<img src="'+this.config.transImage+'" >');

 

 
var pstr=itesh[1]
var iLen=pstr.length;
var pmy=pstr.indexOf("[[") + 2 ; 
var pmi=pstr.indexOf("]]")   ;
var pNIG=pstr.substring(pmy,pmi)

s.push('<input  size="5" id="' + "EE" + node.id + '" type="Hidden" value="' + pNIG + '"></a>');


// bubbles

if (ti.indexOf("|")>0) 
    {

//if (node.dsc.indexOf("Site:")>0) 
//	{
// 	s.push('<iframe style="display:;position:absolute;top:100px;left:30px;" id="'+ "FF" + node.id + '" src="blank.htm"></iframe>');
//	}
//else
//	{
//	  if (node.dsc.indexOf("Company:")>0)
//	{
//  	    s.push('<iframe style="display:;position:absolute;top:100px;left:30px;"  id="'+ "FF" + node.id + '" src="blank.htm"></iframe>');
//	}
//	  else
//	{
//	  var NAG="";
//
//	} 
//}




    }
else
    {


var KP = "BC" + node.id ;

 // alert(iOP);

 // s.push('<a href="#" onClick="meonetwo(' + iOP + ');">');
 // s.push(iOP + ' Details</a>'); 

// gigawatt totals
     s.push('<div ID="' + iOP + '" style="display:none;z-index:88;width:200px;height:200px;background-color:lightgray;color:blue;position:absolute;top:42;left:5;">' + ti + '"</div>') ;

// paul
//    alert(iOP);
//alert( document.getElementById(iOP).style. );
 
 }


					if (node.canCollapse)
					{
s.push('<a href="javascript:'+this.obj+'.collapseNode(\''+node.id+'\', true);" >');
//s.push('<img border=0 height="15px" width="15px" src="'+((node.isCollapsed) ? this.config.collapsedImage : 
s.push('<img border=0  height="10px" width="10px" src="'+((node.isCollapsed) ? this.config.collapsedImage : 
this.config.expandedImage)+'" >');
						s.push('</a>');
						s.push('<img src="'+this.config.transImage+'" >');						
					}	

					if (node.target && this.config.useTarget)
					{
						s.push('<a href="'+node.target+'">');
						//s.push(otesh);			
						s.push('</a>');	
					}				
					  else
					{						

						 //s.push(otesh);
					}
					
// rhona

var ttt=node.dsc ;
var tt1=ttt.indexOf("|") + 1 ; 
var tt2=ttt.indexOf("*")   ;

var pig=ttt.substring(tt1,tt2) ;
var pig_cca=0 ;
var pig_co2=0 ;
var pig_co22=0; 
var Percentagerule=0;

//alert(pig);

newArray = pig.split(':');
for(i=0; i<newArray.length; i++) 
{   
 if (i==0) // this will always be the GWh first
   {
      // alert(newArray[i]);
   }
    else
   {
      // CCA 
      if (i==1)  
      { 
         pig_cca=newArray[i] ;
          //alert(newArray[i]);
      }

      // CO2
      if (i==2)  
      { pig_co2=newArray[i]  ;  }


      if (i==4)
      {
         pig_co22=newArray[i]  ;
        // alert(pig_co22);
      }


      if (i==5)
      {
         Percentagerule=newArray[i]  ;
         // alert(Percentagerule);
      }




   }
}
 

var myDecimal = parseFloat(pig).toFixed(3);


//CCA 
//alert('(1)=' + tesh[0]); 
var pesh = tesh[0].substring(1,0);
var sesh = tesh[0].substring(2,1);


if (parseFloat(pig) > 6) 
 {
    tesh[0]="Y" ;

 // don't show a SGU red tag if an Organisation
 if (node.dsc.indexOf("Organisation")>0) 
  {
    pesh="N";
  }
 else
   {
      // 10 sept 2010 take out sgu for sites
       if (node.dsc.indexOf("Site:")>0) 
      {
          pesh="N";
      }
      else
      {
          pesh="Y";
      }
   }

    
 }


if (node.dsc.indexOf("Organisation")>0) 
{
  var icolor = "black"
  tesh[0]="N" ;
}
else
{
  var icolor = "white"
}


//var str="AB"
//document.write(str.substring(1,0)+" 1<br />");
//document.write(str.substring(2,1)+" 2<br />");
//alert('(1)=' + tesh[0]);
// alert(tesh[0].substring(2,0));
//   alert('(1)=' + tesh[0] + "\n" + '(2)=' + pesh + "\n" + '(3)=' + sesh);

var subtotal_co2=0

if (sesh=="Y")
{
var ytt1=ttt.indexOf("XY") + 1 ; 
var ytt2=ttt.indexOf("%")   ;
// var ypig=ttt.substring(tt1,tt2) number of gigawats
var ypig=ttt.substring(ytt1,ytt2);
var yvalue=ypig.replace("Y>","");
var kpig=ttt.substring(tt1,tt2); //30% so we need to calculate by the 70% remaining
var krr=((100-yvalue) / 100 ) * parseFloat(kpig); 
var rrk=krr.toFixed(3);
var johno =  String.fromCharCode(8679) ;
var sjohno = johno.fontsize(16);
var ijohno = sjohno.fontcolor("black");
// CCA 

  s.push( ijohno  );
  s.push(' <v:textbox style="position:absolute;top:0;left:15;width:170px;">');
  s.push('   <label style="background-color:blue;color:white;font-weight:bold;font-size:10px;">CCA=' + (100-yvalue) + '% </label>  '); 
  s.push('   <label style="background-color:darkred;color:yellow;font-weight:bold;font-size:10px;">' + rrk + '  GWh </label> '); 
  s.push(' </v:textbox>');
}


// am i an SGU or NOT
if (pesh=="Y")
{ 
     // Meters don't have a SGU tag
     if (node.dsc.indexOf("Meter (")>0) 
    	{
          // alert(node.dsc);
    	}
     else
	{
            s.push(' <v:textbox style="color:white;background-color:red;position:absolute;top:0;left:15;width:35px;">');
            s.push('    <center><label style="font-weight:bold;font-size:12px;">SGU</label></center> '); 
            s.push(' </v:textbox>');
 
            s.push(' <v:textbox style="color:white;position:absolute;top:0;left:60;width:110px;">');
            s.push(' <label style="font-weight:bold;font-size:12px;">' + parseFloat(pig).toFixed(3) +' GWh</label>'); 
            s.push(' </v:textbox>');

// IF NO CCA at all
if (parseFloat(pig_cca)==0)
{ }
else
{
            s.push(' <v:textbox style="color:' + icolor  + ';position:absolute;top:0;left:150;width:65px;">');
            s.push('   <label style="background-color:blue;color:white;font-weight:bold;font-size:10px;">CCA</label>  '); 
            s.push('   <label style="background-color:BLACK;color:yellow;font-weight:bold;font-size:10px;">' + parseFloat(pig_cca).toFixed(2)  + '</label>');
            s.push(' </v:textbox>');

            subtotal_co2=pig_cca * (.541 * (1000000/1000))
            s.push(' <v:textbox style="color:' + icolor  + ';position:absolute;top:15;left:150;width:65px;">');
            s.push('   <label style="background-color:blue;color:white;font-weight:bold;font-size:10px;">CO2</label>  '); 
            s.push('   <label style="background-color:BLACK;color:yellow;font-weight:bold;font-size:10px;">' + subtotal_co2.toFixed(2)  + '</label>'); 
            s.push(' </v:textbox>');

		// 25% ruling
		if (node.dsc.indexOf("Company:")>0) 
		{ 
 		 var muck= parseFloat(pig) * 1000 * .541
 		 var ireturn = (subtotal_co2.toFixed(2)/muck) * 100
     		   s.push(' <v:textbox style="color:' + icolor  + ';position:absolute;top:30;left:175;width:65px;">');
       		   //s.push('   <label style="background-color:blue;color:white;font-weight:bold;font-size:10px;">CCA</label>  '); 
         	   s.push('   <label style="background-color:BLACK;color:white;font-weight:bold;font-size:10px;">' + ireturn.toFixed(2) + '%' + '</label>'); 
           	   s.push(' </v:textbox>');
 
 		   if (ireturn > 25)
    		{
    		   s.push(' <v:textbox style="color:' + icolor  + ';position:absolute;top:55;left:54;width:65px;">');
    		   s.push('   <label ID=ZZ' + node.id + ' style="color:YELLOW;font-weight:bold;font-size:10px;">CCA Exempt</label>  '); 
     		   s.push(' </v:textbox>');
    		}
    		 else
    		{
    		   s.push(' <v:textbox style="color:' + icolor  + ';position:absolute;top:55;left:54;width:65px;">');
    		   s.push('   <label ID=ZZ' + node.id + ' style="color:DARKRED;font-weight:bold;font-size:10px;"></label>  '); 
    		   s.push(' </v:textbox>');
    		}
		}
// here
}




	}
}
 else
{




   //if (tesh[0]=="W")
    if (pesh=="W")
   {
            s.push(' <v:textbox style="color:black;background-color:yellow;position:absolute;top:0;left:15;width:35px;">');
            s.push('    <center><label style="font-weight:bold;font-size:12px;">SGU</label></center> '); 
            s.push(' </v:textbox>');

            s.push(' <v:textbox style="color:white;position:absolute;top:0;left:60;width:110px;">');
            s.push('   <label style="font-weight:bold;font-size:12px;">' + parseFloat(pig).toFixed(3)  +' GWh</label>'); 
            s.push(' </v:textbox>');

// IF NO CCA at all
if (parseFloat(pig_cca)==0)
{ }
else
{ 
             s.push(' <v:textbox style="color:' + icolor  + ';position:absolute;top:0;left:150;width:65px;">');
             s.push('   <label style="background-color:blue;color:white;font-weight:bold;font-size:10px;">CCA</label>  '); 
             s.push('   <label style="background-color:BLACK;color:yellow;font-weight:bold;font-size:10px;">' + parseFloat(pig_cca).toFixed(2)  + '</label>'); 
             s.push(' </v:textbox>');
 
             subtotal_co2=pig_cca * (.541 * (1000000/1000))
             s.push(' <v:textbox style="color:' + icolor  + ';position:absolute;top:15;left:150;width:65px;">');
             s.push('   <label style="background-color:blue;color:white;font-weight:bold;font-size:10px;">CO2</label>  '); 
             s.push('   <label style="background-color:BLACK;color:yellow;font-weight:bold;font-size:10px;">' + subtotal_co2.toFixed(2)  + '</label>');
             s.push(' </v:textbox>');

		// 25% ruling
		if (node.dsc.indexOf("Company:")>0) 
		{
		var muck= parseFloat(pig) * 1000 * .541
		var ireturn = (subtotal_co2.toFixed(2)/muck) * 100
            		 s.push(' <v:textbox style="color:' + icolor  + ';position:absolute;top:30;left:175;width:65px;">');
            		 //s.push('   <label style="background-color:blue;color:white;font-weight:bold;font-size:10px;">CCA</label>  '); 
            		 s.push('   <label style="background-color:BLACK;color:white;font-weight:bold;font-size:10px;">' + ireturn.toFixed(2) + '%' + '</label>'); 
            		 s.push(' </v:textbox>');

 		 if (ireturn > 25)
  		 {
   		  s.push(' <v:textbox style="color:' + icolor  + ';position:absolute;top:55;left:54;width:65px;">');
    		  s.push('   <label ID=ZZ' + node.id + ' style="color:YELLOW;font-weight:bold;font-size:10px;">CCA Exempt</label>  '); 
    		  s.push(' </v:textbox>');
    		 }
   		 else
   		 {
     		  s.push(' <v:textbox style="color:' + icolor  + ';position:absolute;top:55;left:54;width:65px;">');
     		  s.push('   <label ID=ZZ' + node.id + ' style="color:DARKRED;font-weight:bold;font-size:10px;"></label>  '); 
      		  s.push(' </v:textbox>');
    		 }
		}
// here
}




   }
    else
   {

// don't show gigawatt totals for meters
 if (node.dsc.indexOf("Meter (")>0) 
{ }

else

{



 if (parseFloat(pig)< 0 ) 
 {
   var getrid="";
 }
  else
 {


             s.push(' <v:textbox style="color:' + icolor  + ';position:absolute;top:0;left:60;width:110px;">');
             s.push('   <label style="font-weight:bold;font-size:12px;">' + parseFloat(pig).toFixed(3)  + ' GWh</label>'); 
             s.push(' </v:textbox>');

// IF NO CCA at all
if (parseFloat(pig_cca)==0)
{ }
else
{
             s.push(' <v:textbox style="color:' + icolor  + ';position:absolute;top:0;left:150;width:65px;">');
             s.push('   <label style="background-color:blue;color:white;font-weight:bold;font-size:10px;">CCA</label>  '); 
             s.push('   <label style="background-color:BLACK;color:yellow;font-weight:bold;font-size:10px;">' + parseFloat(pig_cca).toFixed(2)  + '</label>'); 
             s.push(' </v:textbox>');
 
             subtotal_co2=pig_cca * (.541 * (1000000/1000))
             s.push(' <v:textbox style="color:' + icolor  + ';position:absolute;top:15;left:150;width:65px;">');
             s.push('   <label style="background-color:blue;color:white;font-weight:bold;font-size:10px;">CO2</label>  '); 
             s.push('   <label style="background-color:BLACK;color:yellow;font-weight:bold;font-size:10px;">' + subtotal_co2.toFixed(2)  + '</label>');
             s.push(' </v:textbox>');

		// 25% ruling
 		if (node.dsc.indexOf("Company:")>0) 
 		{
			var muck= parseFloat(pig) * 1000 * .541
			var ireturn = (subtotal_co2.toFixed(2) / muck) * 100
       		        s.push(' <v:textbox style="color:' + icolor  + ';position:absolute;top:30;left:175;width:65px;">');
          		//s.push('   <label style="background-color:blue;color:white;font-weight:bold;font-size:10px;">CCA</label>  '); 
           		s.push('   <label style="background-color:BLACK;color:white;font-weight:bold;font-size:10px;">' + ireturn.toFixed(2) + '%'  + '</label>'); 
            		s.push(' </v:textbox>');
    		  if (ireturn > 25)
    		  {
    		        s.push(' <v:textbox style="color:' + icolor  + ';position:absolute;top:55;left:54;width:65px;">');
    		        s.push('   <label ID=ZZ' + node.id + ' style="color:YELLOW;font-weight:bold;font-size:10px;">CCA Exempt</label>  '); 
    		        s.push(' </v:textbox>');
    		  }
   		  else
    		  {
    		        s.push(' <v:textbox style="color:' + icolor  + ';position:absolute;top:55;left:54;width:65px;">');
     		        s.push('   <label ID=ZZ' + node.id + ' style="color:DARKRED;font-weight:bold;font-size:10px;"></label>  '); 
     		        s.push(' </v:textbox>');
     		  }
 		}
		// 25% ruling
}




 }


}



   }

}

 

// am i Exempt or NOT 
 if (node.dsc.indexOf("Organisation:")>0) 
 {
    if (parseFloat(pig)<1)
     {
       s.push(' <v:textbox style="color:' + icolor  + ';position:absolute;top:55;left:65;width:90px;">');
       s.push('   <label style="color:darkred;font-weight:bold;font-size:10px;">1 GWh Exemption</label>  '); 
       s.push(' </v:textbox>');
     }
 }





var thecolour = 'black' ;
 
if (node.dsc.indexOf(":")>0) 
{
   thecolour = 'black' ;
}
else
{
   thecolour = 'yellow' ;
}  
 
if (node.dsc.indexOf("Company:")>0) 
{
   thecolour = 'lightgray' ;
}

if (node.dsc.indexOf("Site:")>0) 
{
   thecolour = 'white' ;
}
 
s.push(' <v:textbox style="z-index:-1;color:'+ thecolour  +';font-size:9px;position:absolute;top:20;left:5;" >');



if (node.dsc.indexOf("Site:")>0) 
{
   otesh = otesh.replace("[[" , '<font class="harry">'); 
   otesh = otesh.replace("]]" , '</font>'); 
   var coBackColor="#25664f";
   var coColor="#ffffff";
    
}
 else
{
 if (node.dsc.indexOf("Company:")>0)
  {
    otesh = otesh.replace("[[" , '<font class="dick">'); 
    otesh = otesh.replace("]]" , '</font>'); 
    var coBackColor="#63a609";
    var coColor="#ffffff";
     
  }


}

 if (node.dsc.indexOf("Organisation:")>0)
  {
    otesh = otesh.replace("[[" , '<font class="tom">'); 
    otesh = otesh.replace("]]" , '</font>'); 
    var coBackColor="#ffffCO";
    var coColor="#000000";
     
  }


 if (node.dsc.indexOf("Meter (")>0)
  {
    otesh = otesh.replace("[[" , '<font class="dick">'); 
    otesh = otesh.replace("]]" , '</font>'); 
    var coBackColor="lightgrey";
    var coColor="#000000";
     
  }

//otesh = otesh.replace("[[" , '<font class="harry">'); 
//otesh = otesh.replace("]]" , '</font>'); 



otesh = otesh.replace("class=myTable" , 'class="myTable"'); 
otesh = otesh.replace("ID=TableID" , 'ID="TableID"');  
otesh = otesh.replace("Archived" ,"<font style='color:red;'>Archived</font>");
otesh = otesh.replace("Real-Time" ,"<font style='color:blue;'>Real-Time</font>"); 

//otesh = otesh.replace("GWh" ,"<font style='font-size:8px;'>GWh</font>"); 



if (node.dsc.indexOf("Meter (")>0)
{ 
   s.push('<BR>' + otesh);
}
else
{
   s.push('<BR><BR>' + otesh);
}
 


s.push(' </v:textbox>');


// The CO2


 //if (node.dsc.indexOf("Organisation:")>0 || node.dsc.indexOf("Meter (")>0 )
 // {
 //   coBackColor="Black" ;
 // }


s.push(' <v:textbox style="z-index:-1;background-color:black;color:white;font-size:11px;position:absolute;top:17;left:15;width:35px;" >');
s.push('<center> CO<sup>2</sup></center>'); 
s.push(' </v:textbox>');
 

s.push(' <v:textbox style="z-index:-1;background-color:' + coBackColor +';color:' + coColor +';font-size:11px;position:absolute;top:17;left:59;" >');
  var co = parseFloat(pig) * 1000 * .541 ;
s.push( co.toFixed(2) + '(T)' ); 
s.push(' </v:textbox>');

 




if (node.dsc.indexOf("Site:")>0) 
	{
 	   // s.push('<iframe frameborder="0" Scrolling="AUTO" style="display:none;position:absolute;top:42px;left:1px;" id="'+ "FF" + node.id + '" src="blank.htm"></iframe>');
	}
else
	{
	  if (node.dsc.indexOf("Company:")>0)
	{
  	  // s.push('<iframe frameborder="0" Scrolling="AUTO" style="display:none;position:absolute;top:42px;left:1px;" id="'+ "FF" + node.id + '" src="blank.htm"></iframe>');
	}
	  else
	{
	  var NAG="";
	} 
}




 //s.push(' <v:textbox inset="0.5px,0.5px,0.5px,0.5px" style="background-color:#ffffff;color:Blue;position:absolute;top:55;left:5;width:180px" >');

var utesh = itesh[0].replace("Y|","")
    utesh = utesh.replace("N|","")   

var bimagename ="default.gif"

if (node.dsc.indexOf("Organisation:")>0) 
{
    bimagename = "OraganisationDetails.gif"
}
if (node.dsc.indexOf("Company:")>0) 
{
    bimagename = "CompanyDetails.gif"
}
if (node.dsc.indexOf("Site:")>0) 
{
    bimagename = "SiteDetails.gif"
}
if (node.dsc.indexOf("Meter (Primary)")>0) 
{
    bimagename = "MeterDetails.gif"
}

if (node.dsc.indexOf("Meter (Ass")>0) 
{
    bimagename = "na.gif"
}

if (bimagename=="na.gif")
{

}
else
{
// s.push('<img onClick="javascript:displayWindow(event.clientX + 20,event.clientY + 20 )" src="' + bimagename + '" height="20px" width="20px" alt="view details" style="z-index:700;position:absolute;top:0px;left:170px"      ');
}



if (node.dsc.indexOf("Organisation:")>0) 
{
// s.push("Organisation"); 
 //s.push(' </v:textbox>');
    
}
else
{
 //s.push("Total: " + utesh); 
 //s.push(' </v:textbox>');
}


//s.push(mesh); 



					s.push('</font></v:textbox>');



																
					switch (this.config.nodeFill) 
                                         {
						case ECOTree.NF_GRADIENT:
							s.push('<v:fill type=gradient color2="'+((node.isSelected)?this.config.nodeSelColor:color)+'" color="#F5FFF5" angle=90 />');	
							break;
						case ECOTree.NF_FLAT:
							s.push('<v:fill type="solid" color="'+((node.isSelected)?this.config.nodeSelColor:color)+'" />');	
							break;
					}
					s.push('<v:shadow type="single" on="true" offset="4pt,3pt" color="silver" opacity="1.0" />');					
					s.push('<v:stroke filltype="frame" src="logo.gif" />');					

                                        s.push('</v:roundrect>');																									
					break;
			}	
			if (!node.isCollapsed)	s.push(node._drawChildrenLinks(this.self));
		}
	}


 return s.join('');
 
//alert("FINISHED I THINK");

// var labs,i ;
// var tots=0;
// var tots1=0;
// labs=document.getElementsByTagName('label');
// for(i=0;i<labs.length;i++)
//  {
//    if (labs[i].id=="")
//    {       }
//      else
//    {
//         tots++;
//           alert(labs[i].innerText)
//    }    
//   }



	
}

ECOTree.prototype.toString = function () {	
	var s = [];
	
	this._positionTree();
	
	//alert(this.render);
	
	switch (this.render)
	{
		case "CANVAS":
			s.push('<canvas id="ECOTreecanvas" width=2000 height=2000></canvas>');
			break;
			
		case "HTML":
			s.push('<div border="1" class="maindiv">');
			s.push(this._drawTree());
			s.push('</div>');
			break;
// the scale			
		case "VML":
//s.push('<v:group ID="myGroup" coordsize="10000, 10000" coordorigin="-50, -50" //style="overflow:auto;background-color:white;position:absolute;width=10000px;height=10000px;" >');

//s.push('john was heere');


 s.push('<v:group ID="myGroup" coordsize="10000, 10000" coordorigin="-50, -50"  style="zoom:1;overflow:no;background-color:#ffffc0;position:absolute;width=10000px;height=10000px;" >');			

 

			s.push(this._drawTree());
			s.push('</v:group>');
			break;
	}
	
	return s.join('');
}

// ECOTree API begins here...

ECOTree.prototype.UpdateTree = function () {	
	this.elm.innerHTML = this;
	if (this.render == "CANVAS") 
	{
		var canvas = document.getElementById("ECOTreecanvas");
		if (canvas && canvas.getContext)
		{
			this.canvasoffsetLeft = canvas.offsetLeft;
			this.canvasoffsetTop = canvas.offsetTop;
			this.ctx = canvas.getContext('2d');
			var h = this._drawTree();	
			var r = this.elm.ownerDocument.createRange();
			r.setStartBefore(this.elm);
			var parsedHTML = r.createContextualFragment(h);								
			//this.elm.parentNode.insertBefore(parsedHTML,this.elm)
			//this.elm.parentNode.appendChild(parsedHTML);
			this.elm.appendChild(parsedHTML);
			//this.elm.insertBefore(parsedHTML,this.elm.firstChild);
		}
	}
}

ECOTree.prototype.add = function (id, pid, dsc, w, h, c, bc, target, meta, mything)
{	

//alert(dsc); 

	var nw = w || this.config.defaultNodeWidth; //Width, height, colors, target and metadata defaults...
	var nh = h || this.config.defaultNodeHeight;
	var mything= (typeof mything != "undefined")	? mything : "";




        var color = c || this.config.nodeColor;
	var border = bc || this.config.nodeBorderColor;
	var tg = (this.config.useTarget) ? ((typeof target == "undefined") ? (this.config.defaultTarget) : target) : null;
	var metadata = (typeof meta != "undefined")	? meta : "";
	
	var pnode = null; //Search for parent node in database
	if (pid == -1) 
		{
			pnode = this.root;
		}
	else
		{
			for (var k = 0; k < this.nDatabaseNodes.length; k++)
			{
				if (this.nDatabaseNodes[k].id == pid)
				{
					pnode = this.nDatabaseNodes[k];
					break;
				}
			}	
		}
	
	var node = new ECONode(id, pid, dsc, nw, nh, color, border, tg, metadata, mything);	//New node creation...
	node.nodeParent = pnode;  //Set it's parent
	pnode.canCollapse = true; //It's obvious that now the parent can collapse	
	var i = this.nDatabaseNodes.length;	//Save it in database
	node.dbIndex = this.mapIDs[id] = i;	 
	this.nDatabaseNodes[i] = node;	
	var h = pnode.nodeChildren.length; //Add it as child of it's parent
	node.siblingIndex = h;
	pnode.nodeChildren[h] = node;
}

ECOTree.prototype.searchNodes = function (str)
{
alert("jon");

var node = null;
	var m = this.config.searchMode;
	var sm = (this.config.selectMode == ECOTree.SL_SINGLE);	 
	
	if (typeof str == "undefined") return;
	if (str == "") return;
	
	var found = false;
	var n = (sm) ? this.iLastSearch : 0;
	if (n == this.nDatabaseNodes.length) n = this.iLastSeach = 0;
	
	str = str.toLocaleUpperCase();
	
	for (; n < this.nDatabaseNodes.length; n++)
	{ 		
		node = this.nDatabaseNodes[n];				
		if (node.dsc.toLocaleUpperCase().indexOf(str) != -1 && ((m == ECOTree.SM_DSC) || (m == ECOTree.SM_BOTH))) { node._setAncestorsExpanded(); this._selectNodeInt(node.dbIndex, false); found = true; }
		if (node.meta.toLocaleUpperCase().indexOf(str) != -1 && ((m == ECOTree.SM_META) || (m == ECOTree.SM_BOTH))) { node._setAncestorsExpanded(); this._selectNodeInt(node.dbIndex, false); found = true; }
		if (sm && found) {this.iLastSearch = n + 1; break;}
	}	
	this.UpdateTree();	
}

ECOTree.prototype.selectAll = function () {
	if (this.config.selectMode != ECOTree.SL_MULTIPLE) return;
	this._selectAllInt(true);
}

ECOTree.prototype.unselectAll = function () {
	this._selectAllInt(false);
}

ECOTree.prototype.collapseAll = function () {
	this._collapseAllInt(true);
}

ECOTree.prototype.expandAll = function () {
	this._collapseAllInt(false);
}

ECOTree.prototype.collapseNode = function (nodeid, upd) {
	var dbindex = this.mapIDs[nodeid];
	this.nDatabaseNodes[dbindex].isCollapsed = !this.nDatabaseNodes[dbindex].isCollapsed;
	if (upd) this.UpdateTree();
}

ECOTree.prototype.selectNode = function (nodeid, upd) 
{		

//alert("here main");
var dbindex = this.mapIDs[nodeid];

//document.getElementById(elm)


//alert(this.nDatabaseNodes[dbindex].dsc);

        document.getElementById("Temp").value =this.nDatabaseNodes[dbindex].dsc + ',' + nodeid
//alert("done");

/*
	this._selectNodeInt(this.mapIDs[nodeid], true);
	if (upd) this.UpdateTree();
*/	
	
}

ECOTree.prototype.setNodeTitle = function (nodeid, title, upd) 
{

	var dbindex = this.mapIDs[nodeid];
	this.nDatabaseNodes[dbindex].dsc = title;
alert(title);

	if (upd) this.UpdateTree();
}

ECOTree.prototype.setNodeMetadata = function (nodeid, meta, upd) {
	var dbindex = this.mapIDs[nodeid];
	this.nDatabaseNodes[dbindex].meta = meta;
	if (upd) this.UpdateTree();
}

ECOTree.prototype.setNodeTarget = function (nodeid, target, upd) {
	var dbindex = this.mapIDs[nodeid];
	this.nDatabaseNodes[dbindex].target = target;
	if (upd) this.UpdateTree();	
}

ECOTree.prototype.setNodeColors = function (nodeid, color, border, upd) {
	var dbindex = this.mapIDs[nodeid];
	if (color) this.nDatabaseNodes[dbindex].c = color;
	if (border) this.nDatabaseNodes[dbindex].bc = border;
	if (upd) this.UpdateTree();	
}

ECOTree.prototype.getSelectedNodes = function ()
 {
	var node = null;
	var selection = [];
	var selnode = null;	

//alert("here main 2");

	
	for (var n=0; n<this.nDatabaseNodes.length; n++) {
		node = this.nDatabaseNodes[n];
		if (node.isSelected)
		{			
			selnode = {
				"id" : node.id,
				"dsc" : node.dsc,
				"meta" : node.meta
			}
			selection[selection.length] = selnode;
		}
	}
	return selection;
}

function john(xitem,iNodeid)
{
 var theitem = xitem;
 window.status=theitem +  iNodeid
 
 //alert(xitem);
}

function sjohn(xitem,iNodeid)
{

//alert( xitem);

//alert('john ' + '\n' + xitem);

var theitem = xitem;
document.getElementById("Temp").value=theitem + ',' + iNodeid + ',' + "me." ;
 

}
 

function justclickme(what,x,y)
{

//  var obj = document.getElementById('id_of_yours');
//  obj.style.top = x;
//  obj.style.left = y;


//  var offsetX = event.screenX - event.pageX; //(or clientX) 
//  var offsetY = event.screenY - event.pageY; //(or clientY)
//  alert(event.screenX);
    document.getElementById("TheData").style.top=event.screenY/2 ;
    document.getElementById("TheData").style.left=event.screenX/2;
    document.getElementById("TheData").innerText=what;

 

}

function clearitout()
{
  document.getElementById("TheData").innerText="";
}


function meone(what,i,tit)
{

    var Yop= "BC" + what.id ;
    var thy = document.getElementById(what.id).innerText;
    var yt= thy.indexOf(": Meter");
 if (yt>0) 
   {

//alert(document.getElementById(what.id).style.height);
// alert(document.getElementById(what.id).arcsize);


if (document.getElementById(what.id).style.height=="80px") 
{
   document.getElementById(what.id).style.height="220px";
   document.getElementById(what.id).style.zindex="200";
   document.getElementById(what.id).style.arcsize="0.40";
   //  document.getElementById(what.id).className="John";
   document.getElementById(Yop).style.display="";

}
else
{
   document.getElementById(what.id).style.height="80px";
   document.getElementById(what.id).style.zindex="0";
   document.getElementById(what.id).style.arcsize="0.18";
   // document.getElementById(what.id).className="Smith";
   document.getElementById(Yop).style.display="none";
}



////////////////////////////////////////////////////////


////////////////////////////////////////////////////////



//   if (i==1) 
//       { 
//         document.getElementById(what.id).style.height="220px";
//         document.getElementById(what.id).style.zindex="200";
//         document.getElementById(Yop).style.display="";
//       }
//   else
//       { 
//         document.getElementById(what.id).style.height="80px";
//         document.getElementById(what.id).style.zindex="0";
//         document.getElementById(Yop).style.display="none";
//       }
  

 }

}


/////////////////////////////////////////////////////
/////////////////////////////////////////////////////

 var IE = document.all?true:false

// If NS -- that is, !IE -- then set up for mouse capture
 if (!IE) document.captureEvents(Event.MOUSEMOVE)

// Set-up to use getMouseXY function onMouseMove
 document.onmousemove = getMouseXY;

// Temporary variables to hold mouse x-y pos.s
 var tempX = 0
 var tempY = 0



function getMouseXY(e) 
{
  if (IE) { // grab the x-y pos.s if browser is IE
    tempX = event.clientX + document.body.scrollLeft
    tempY = event.clientY + document.body.scrollTop
  } else {  // grab the x-y pos.s if browser is NS
    tempX = e.pageX
    tempY = e.pageY
  }  
  // catch possible negative values in NS4
  if (tempX < 0){tempX = 0}
  if (tempY < 0){tempY = 0}  
 

    //document.getElementById("TheData").innerText=event.clientX + "<BR>" + event.screenX;

 
  return true
}


 ///////////////////////////
////////////////////////////
//////////////////////////
function meonetwo(what)
{
alert(what);

    var Yop= "BC" + what.id ;
    var thy = document.getElementById(what.id).innerText;
    var yt= thy.indexOf(": Meter");
 if (yt>0) 
   {

if (document.getElementById(what.id).style.height=="80px") 
{
   document.getElementById(what.id).style.height="220px";
   document.getElementById(what.id).style.zindex="200";
   document.getElementById(what.id).style.arcsize="0.40";
   //  document.getElementById(what.id).className="John";
   document.getElementById(Yop).style.display="";

}
else
{
   document.getElementById(what.id).style.height="80px";
   document.getElementById(what.id).style.zindex="0";
   document.getElementById(what.id).style.arcsize="0.18";
   // document.getElementById(what.id).className="Smith";
   document.getElementById(Yop).style.display="none";
}
 
  

 }

}

function meSite(what,when,who)
{
document.getElementById("but1").style.display="none";
document.getElementById("but2").style.display="none";
document.getElementById("but3").style.display="none";
 
 document.getElementById("windowcontent").style.classname="sitedimming";
 document.getElementById("toti").style.classname="company";
 document.getElementById("tot").style.classname="company";


 document.getElementById("toti").style.display="none";
  if (document.getElementById("tot").style.display="")
  {
    document.getElementById("toti").style.display="none";
    document.getElementById("theidis").innerText =  document.getElementById(what.id).innerText ;
  }
  var Yop= "DC" + what.id ;
  var iYop= what.id.replace("DC","FF") ;
  var yYop= what.id.replace("DC","EE") ;
  var theitem=document.getElementById(yYop).value
// document.getElementById("TheD").src="https://portal.utilitymasters.co.uk/sqlconnection.asp?id=" + theitem + "&type=Site";

  document.getElementById("TheD").src="http://images.utilitymasters.co.uk/CRC/sqlconnection.asp?id=" + theitem + "&type=Site";

  document.getElementById("TheD").style.display="";
}
 

function meCompany(what,when,who)
{
document.getElementById("but1").style.display="none";
document.getElementById("but2").style.display="none";
document.getElementById("but3").style.display="none";

 document.getElementById("windowcontent").style.classname="companydimming";
 document.getElementById("toti").style.classname="site";
 document.getElementById("tot").style.classname="site";

 document.getElementById("toti").style.display="none";
  if (document.getElementById("tot").style.display="")
  {
    document.getElementById("toti").style.display="none";
    document.getElementById("theidis").innerText =  document.getElementById(what.id).innerText ;
  }
 var Yop= "EC" + what.id ;
 var iYop= what.id.replace("EC","FF") ;
 var yYop= what.id.replace("EC","EE") ;
 var theitem=document.getElementById(yYop).value
 document.getElementById("TheD").src="http://images.utilitymasters.co.uk/CRC/sqlconnection.asp?id=" + theitem + "&type=Company";
 document.getElementById("TheD").style.display="";
}




function meOrganisation(what,when,who)
{
document.getElementById("but1").style.display="none";
document.getElementById("but2").style.display="none";
document.getElementById("but3").style.display="none";

 document.getElementById("windowcontent").style.classname="companydimming";
 document.getElementById("toti").style.classname="site";
 document.getElementById("tot").style.classname="site";

 document.getElementById("toti").style.display="none";
  if (document.getElementById("tot").style.display="")
  {
    document.getElementById("toti").style.display="none";
    document.getElementById("theidis").innerText =  document.getElementById(what.id).innerText ;
  }
 var Yop= "GG" + what.id ;
 var iYop= what.id.replace("GG","FF") ;
 var yYop= what.id.replace("GG","EE") ;
 var theitem=document.getElementById(yYop).value
 document.getElementById("TheD").src="http://images.utilitymasters.co.uk/CRC/sqlconnection.asp?id=" + theitem + "&type=Organisation";
 document.getElementById("TheD").style.display="";
}



////////////////////////
function meRollOver(what,i,tit)
{
document.getElementById("but1").style.display="";
document.getElementById("but2").style.display="";
document.getElementById("but3").style.display="";

document.getElementById("TheD").style.display="none";

    var Yop= "BC" + what.id ;
    var iOP="DC" + what.id;
    var thy = document.getElementById(what.id).innerText;
    var Yok = document.getElementById(what.id).innerHtml;
    var yt= thy.indexOf(": Meter");

 if (yt>0) 
   {

    if (i==1) 
       { 
         document.getElementById(what.id).style.height="220px";
         document.getElementById(what.id).style.zindex="200";
         document.getElementById(Yop).style.display="";
         document.getElementById("toti").innerText=document.getElementById(Yop).innerText;

  //  alert(document.getElementById(Yop).outerHTML);

       }
   else
       { 
         document.getElementById(what.id).style.height="80px";
         document.getElementById(what.id).style.zindex="0";
         document.getElementById(Yop).style.display="none";
       }
  }

}


function meRollOverClick(what,i,tit)
{
document.getElementById("but1").style.display="";
document.getElementById("but2").style.display="";
document.getElementById("but3").style.display="";

    document.getElementById("TheD").style.display="none";
    var Yop= "BC" + what.id ;

var HAV = document.getElementById(Yop).innerHTML.replace('class=myTable','class="myTable"')  ;
var HAV1= HAV.replace('row1', '"row1"' );
var HAV2= HAV1.replace('row2', '"row2"' ); 
var HAV3= HAV2.replace('row3', '"row3"' );
var HAV4= HAV3.replace('row4', '"row4"' );
var HAV5= HAV4.replace('row5', '"row5"' );
var HAV6= HAV5.replace('row6', '"row6"' );
var HAV7= HAV6.replace('row7', '"row7"' );
var HAV8= HAV7.replace('row8', '"row8"' );
var HAV9= HAV8.replace('row9', '"row9"' );
var HAV10= HAV9.replace('row10', '"row10"' );
var HAV11= HAV10.replace('row11', '"row11"' );
var HAV12= HAV11.replace('row12', '"row12"' );

    document.getElementById("toti").innerHTML=HAV12;
    document.getElementById("toti").style.display="";

//alert( document.getElementById("toti").innerHTML );

}



///////////////////////////////////////////////////////
function zoomin(what)
{
// var value= document.getElementById("myGroup").style.zoom  ;
// document.getElementById("myGroup").style.zoom = document.getElementById("myGroup").style.zoom + .001
//alert(value);
//document.getElementById("myGroup").style.zoom = document.getElementById("myGroup").style.zoom + .00001 
//zoom(this.viewing.sx, this.viewing.sy) 

document.getElementById("myGroup").style.zoom=what;
document.getElementById("zoomo").style.zoom=1;
 


} 

function zoomout()
{
 var value= document.getElementById("myGroup").style.zoom  ;
 alert(value);
 document.getElementById("myGroup").style.zoom = document.getElementById("myGroup").style.zoom - .3

}  


           function displayWindow(what,when) 
          { 

             var w, h, l, t; 
             w = 400; 
             h = 200; 


             // l = screen.width/4;

        l = what;
    
//        t = screen.height/4; 
         t = when; 

            // no title"
              displayFloatingDiv('windowcontent', 'John\'s Name', w, h, l, t); 
 
            // with title 
            //displayFloatingDiv('windowcontent', 'Floating and Dimming Div', w, h, l, t); 
         }    

 
/////////////////////////////
/////////////////////////////
////////////////////////////
var isMozilla;
var objDiv = null;
var originalDivHTML = "";
var DivID = "";
var over = false;

 
function buildDimmerDiv()
{
   document.write('<div id="dimmer" class="dimmer"></div>');
// style="width:'+ window.screen.width + 'px; height:' + window.screen.height +'px"
}


 
function displayFloatingDiv(divId, title, width, height, left, top) 
{

    DivID = divId;

    document.getElementById('dimmer').style.visibility = "visible";

    document.getElementById(divId).style.width = "300px"; 
    //width + 'px';
    
    document.getElementById(divId).style.height = height + 'px';
    document.getElementById(divId).style.left = left + 'px';
    document.getElementById(divId).style.top = top + 'px';
	
	var addHeader;
	
	if (originalDivHTML == "")
	    originalDivHTML = document.getElementById(divId).innerHTML;
	
//	addHeader = '<table style="width:' + width + 'px" class="floatingHeader">' +
//	            '<tr><td ondblclick="void(0);" onmouseover="over=true;" onmouseout="over=false;" style="cursor:move;height:18px">' + title + '</td>' + 
//	            '<td style="width:18px" align="right"><a href="javascript:hiddenFloatingDiv(\'' + divId + '\');void(0);">' + 
//	            '<img alt="Close..." title="Close..." src="close.jpg" border="0"></a></td></tr></table>';
	

//	addHeader = '<table style="width:' + width + 'px" class="floatingHeader">' +
//	            '<tr><td ID="theidis" ondblclick="void(0);" onmouseover="mytit()" onmouseout="over=false;" style="cursor:move;height:18px">' + "<tag>" + "Title" + "</tag>" + '</td>' + 
//	            '<td style="width:18px" align="right"><a href="javascript:hiddenFloatingDiv(\'' + divId + '\');void(0);">' + 
//	            '<img alt="Close..." title="Close..." src="close.jpg" border="0"></a></td></tr></table>';


	addHeader = '<table style="width:' + 300 + 'px" class="floatingHeader">' +
	            '<tr><td ID="theidis" ondblclick="void(0);" onmouseover="mytit()" onmouseout="over=false;" style="cursor:move;height:18px">' + "<tag>" + "Title" + "</tag>" + '</td>' + 
	            '<td style="width:18px" align="right"><a href="javascript:hiddenFloatingDiv(\'' + divId + '\');void(0);">' + 
	            '<img alt="Close..." title="Close..." src="close.jpg" border="0"></a></td></tr></table>';
  	
	document.getElementById(divId).innerHTML = addHeader + originalDivHTML;
	
//alert(document.getElementById(divId).innerHTML);

        document.getElementById(divId).className = 'dimming';
	document.getElementById(divId).style.visibility = "visible";
}

function mytit()
{
  over=true;
}
 
function hiddenFloatingDiv(divId) 
{
	document.getElementById(divId).innerHTML = originalDivHTML;
	document.getElementById(divId).style.visibility='hidden';
	document.getElementById('dimmer').style.visibility = 'hidden';
	
	DivID = "";
}
 
function MouseDown(e) 
{

    if (over)
    {
        if (isMozilla) 
        {
            objDiv = document.getElementById(DivID);
            X = e.layerX;
            Y = e.layerY;
            return false;
        }
        else {
            objDiv = document.getElementById(DivID);
            objDiv = objDiv.style;
            X = event.offsetX;
            Y = event.offsetY;
        }
    }
}


 
function MouseMove(e) 
{
    if (objDiv) {
        if (isMozilla) {
            objDiv.style.top = (e.pageY-Y) + 'px';
            objDiv.style.left = (e.pageX-X) + 'px';
            return false;
        }
        else 
        {
            objDiv.pixelLeft = event.clientX-X + document.body.scrollLeft;
            objDiv.pixelTop = event.clientY-Y + document.body.scrollTop;
            return false;
        }
    }
}

 
function MouseUp() 
{
    objDiv = null;
}

 
function init()
{
    // check browser
    isMozilla = (document.all) ? 0 : 1;


    if (isMozilla) 
    {
        document.captureEvents(Event.MOUSEDOWN | Event.MOUSEMOVE | Event.MOUSEUP);
    }

    window.document.onmousedown = MouseDown;
    window.document.onmousemove = MouseMove;
    window.document.onmouseup = MouseUp;

    // add the div
    // used to dim the page
     	buildDimmerDiv();

}

// call init
init();

 

function TopAlignLatestNews(what,iType)
{
//var row1="";
//var row2="";
//var row3="";
//var row4="";
//var row5="";
//var row6="";
//var row7="";
//var row8="";
//var row9="";
//var row10="";
//var row11="";
//var row12="";

//var HAV = document.getElementById("toti").innerHTML.replace('class=myTable','class="myTable"')  ;
//var HAV1= HAV.replace('r1c1', '"r1c1"' );
//var HAV2= HAV1.replace('r2c1', '"r2c1"' ); 
//var HAV3= HAV2.replace('r3c1', '"r3c1"' );
//var HAV4= HAV3.replace('r4c1', '"r4c1"' );
//var HAV5= HAV4.replace('r5c1', '"r5c1"' );
//var HAV6= HAV5.replace('r6c1', '"r6c1"' );
//var HAV7= HAV6.replace('r7c1', '"r7c1"' );
//var HAV8= HAV7.replace('r8c1', '"r8c1"' );
//var HAV9= HAV8.replace('r9c1', '"r9c1"' );
//var HAV10= HAV9.replace('r10c1', '"r10c1"' );
//var HAV11= HAV10.replace('r11c1', '"r11c1"' );
//var HAV12= HAV11.replace('r12c1', '"r12c1"' );

//alert(HAV12);


//document.getElementById("toti").innerHTML= HAV12 ;

var LatestNewsTable = what ;
var NewType=iType;
var myRowIS=""
    try
    {

    alert(document.getElementById(LatestNewsTable).outerHTML);

        table = document.getElementById(LatestNewsTable);
           // alert(table.rows.length);
      

  for (var r = 0, n=table.rows.length;r<n;r++)
        {
               myRowIS="r" + r + "c1" ;
            for (var c = 0,m=table.rows[r].cells.length;c<m;c++)
            {
                //  alert("row = " + r + ", cell = " + c);
                // table.rows[r].cells[c].style.verticalAlign = "top";

                 if (c==1)
                 {
		   
                      //  table.rows[r].cells[c].innerText= parseFloat(table.rows[r].cells[c].innerText) * 1000
		      
 				//alert( table.rows[r].cells[c].innerText)
                       
		 
                      // document.getElementById(myRowIS).innerText= parseFloat(table.rows[r].cells[c].innerText) * 1000;

                 }


 
            }
        }

          

    }
    catch(er)
    {
          // Non-critical error
          // alert(er.description);
    }
}

 



