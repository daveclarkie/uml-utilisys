<%
Dim TheID 
         TheID=request.QueryString("id")  

Dim TheNet
         TheNet=request.QueryString("net")  

Dim Current_Phase
         Current_Phase=SESSION("phase")  

Dim ReportPhase
         ReportPhase=Request.QueryString("RP")  
If ReportPhase="" then
           Current_Phase=SESSION("phase")  
Else
           Current_Phase=Request.QueryString("RP")  
End If


Dim OnOff 
         if TheNet="true" then
               vis="Display:none;" 
         else
             vis="Display:;"  
        end if


 
 

 




         
%> 


<HTML>
<%
Public dComm  
Public rComm  
 
Public Function OpenConnection()
  Set dComm = CreateObject("ADODB.Connection")
      dComm.Open "Provider=SQLOLEDB.1;Data Source=uk-ed0-sqlcl-01.MCEG.local;Initial Catalog=UML_CMS;Integrated Security=SSPI;"
End Function

Public Function OpenRecordset(nSQL)
 Set rComm = CreateObject("ADODB.recordset")
     rComm.ActiveConnection = dComm
     rComm.CursorLocation = 3 ' 2
     rComm.CursorType = 3 '1
     rComm.LockType = 1 '3
     rComm.Source = nSQL
     rComm.Open
End Function

Public Function CloseConnection()
  Set dComm = Nothing
  Set rComm = Nothing
End Function


'=========================================================================================
strsql = "SELECT * FROM dbo.tblCRC_CCA_Page99 WHERE virtualID = " & TheID
strsql = strsql & " AND TagID='9.9'" & " AND phase=" & "'" & Current_Phase & "'"

Call OpenConnection
Call OpenRecordset(strsql)
if rComm.EOF = true then
   continue=false
   f1_Date=""
   f2_Date=""
   f3_Date=""
    
   f1=""
   f2=""
   f3=""
   f4="" 
   f5=""
   f6=""
 
   Response.Write "<script language=""javascript"" type=""text/javascript"">" & vbnewline
   Response.write "var VirtualID=" & TheID  & vbnewline
   Response.write "var EditID=" & "0"  & vbnewline
   Response.write "var TagId=" & "'" & "9.9" & "'" & vbnewline
   Response.Write "</script>" &  vbnewline
else
   continue=true
    F1_Date=trim(rcomm("F1_Date"))
    F2_Date=trim(rcomm("F2_Date"))
    F3_Date=trim(rcomm("F3_Date"))
    
    F1=rcomm("F1")
    F2=rcomm("F2")
    F3=rcomm("F3")
    F4=rcomm("F4")
    F5=rcomm("F5")
    F6=rcomm("F6")
    
      
   Response.Write "<script language=""javascript"" type=""text/javascript"">" & vbnewline
   Response.write "var VirtualID=" & TheID  & vbnewline
   Response.write "var EditID=" & rComm("ID") & vbnewline
   Response.write "var TagId=" & "'" & "9.9" & "'" & vbnewline
   Response.Write "</script>" &  vbnewline
end if
'=========================================================================================
 %>
<head>
    <title>Page 8.9</title>
    <LINK REL=StyleSheet HREF="CCA_stylesheet.css" TYPE="text/css" >
<script type="text/javascript" src="mree.js"></script>
<script language="javascript" type="text/javascript">
// <!CDATA[

 function icomplete(what)
{
 var total=0
 document.getElementById("mylabel").innerText="";
    total = total + validate("F1_Date");
    total = total + validate("F2_Date"); 
    total = total + validate("F3_Date"); 
    validateall(3,total,VirtualID,"9.8");
}


function NotApplicable()
{
 var d = new Date() 
 var dy =     PadDigits(  d.getFullYear()  , 4);
 var dm =    PadDigits(     d.getMonth()  + 1 , 2);
 var dd =      PadDigits(   d.getDate() , 2);

 document.getElementById("F1_Date").value=dd + "/" + dm + "/" + dy;
 document.getElementById("F1_Date").style.backgroundColor="white"; 
 document.getElementById("F2_Date").value=dd + "/" + dm + "/" + dy;
 document.getElementById("F2_Date").style.backgroundColor="white"; 
 document.getElementById("F3_Date").value=dd + "/" + dm + "/" + dy;
 document.getElementById("F3_Date").style.backgroundColor="white"; 
}

function updateme()
{
     document.getElementById("myform").action = "evidenceupdate.asp?virtualid=" + VirtualID + "&base=tblCRC_CCA_Page99" + "&tag=" + TagId + "&editid="  + EditID ;
     document.getElementById("myform").submit();
}


// ]]>
</script>
</head>
<body>
 <form name="myform"  action="evidenceupdate.asp" method="post" target="myframe"> 
     <div  id="labelbanner">
        Domestic exclusions  (9.9)
    </div> 

 <br />
<!-----  R E T U R N ---->
 
 <button class="backbutton" onclick="window.location.href=<%="'page1.asp?id=" & TheID & "&net=" & TheNet  & "&phase=" & Current_Phase  & "'" %>" title="Return back main menu" ><< Back</button>

<%if TheNet="false" then %>
 <input class="completebutton" title="Assign these questions with a Completed status Tag " type="button" value="Complete" onclick="javascript:icomplete('9.9');" />
 <input class="completebutton" title="Not Applicable" type="button" value="N/A" onclick="javascript:NotApplicable('9.9');" />
<%end if%>
<!-----  R E T U R N ---->
<br />
 
<label class="messagelabel" id="myLabel"></label>
    <table  >
        <tr  >
            <td class="One" style="height: 21px; width: 237px;"   >
                Have domestic deductions been made?<br />
                <select class="Two" name="F1" id="F1">
                  	   <%if continue=true then %>
					   <% if rComm("F1")="Yes" then %>
					       <option selected="selected" value="Yes">Yes</option> 
					       <option value="No">No</option>
					   <% Else %>
					       <option value="Yes">Yes</option>
					       <option selected="selected" value="No">No</option> 
					   <% End If %>
					   <% else %>
<option selected value=""></option>
<option value="Yes">Yes</option>
<option value="No">No</option>
					   <% End If %>
                </select>
                <br />
                <textarea id="F4" rows="6" cols="31" class="three"  name="F4" ><%=F4%></textarea></td>
            <td style="width: 76px; height: 21px"  >
            <input id="F1_Date"  style="<%=vis%>"  type="text" class="four" name="F1_Date" value="<%=F1_Date%>" /></td>
        </tr>
     
     
    
    
        <tr>
            <td style="height: 26px; width: 237px;" class="One"   >
                Have domestic exclusions been estimated?
<SELECT class="Two" name="F2" id="F2"> 
	                <%if continue=true then %>
					   <% if rComm("F2")="Yes" then %>
					       <option selected="selected" value="Yes">Yes</option> 
					       <option value="No">No</option>
					   <% Else %>
					       <option value="Yes">Yes</option>
					       <option selected="selected" value="No">No</option> 
					   <% End If %>
					<% else %>
<option selected value=""></option>
<option value="Yes">Yes</option>
<option value="No">No</option>
					<% End If %>
 </SELECT>
                <br />

                <textarea id="F5" rows="6" cols="31"  class="three" name="F5" ><%=F5%></textarea></td>
            <td style="height: 26px; width: 76px;"  >
               <input id="F2_Date"  style="<%=vis%>"  type="text" class="four" name="F2_Date" value="<%=F2_Date%>" /></td>
        </tr>
        
        
        
        <tr>
            <td style="height: 26px; width: 237px;" class="One"   >
                Evidence (including emissions calculations) for deductions attached in schedule<br />
            <SELECT class="Two" name="F3" id="F3"> 
	              <%if continue=true then %>
					   <% if rComm("F3")="Yes" then %>
					       <option selected="selected" value="Yes">Yes</option> 
					       <option value="No">No</option>
					   <% Else %>
					       <option value="Yes">Yes</option>
					       <option selected="selected" value="No">No</option> 
					   <% End If %>
					<% else %>
<option selected value=""></option>
<option value="Yes">Yes</option>
<option value="No">No</option>
					<% End If %>
 </SELECT>
                <br />
                <textarea id="F6" rows="6" cols="31" class="three"  name="F6" ><%=F6%>  </textarea></td>
            <td style="height: 26px; width: 76px;"  >
               <input id="F3_Date"  style="<%=vis%>"  type="text" class="four" name="F3_Date" value="<%=F3_Date%>" /></td>
        </tr>
        
    </table>
    
  <br />
  <%if TheNet="false" then %>
 <INPUT type="button" value="Submit" id="button1" name="button1" onclick="javascript:updateme();" class="backbutton"/>
<%end if %>

 
<iframe id="myframe" name="myframe" src="blank2.htm" frameborder="no" scrolling="no"/>
 
</form>
</body>
</html>
