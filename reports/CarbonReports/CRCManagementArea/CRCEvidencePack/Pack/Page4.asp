<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <title>Untitled Page</title>
<script language="javascript" type="text/javascript">
// <!CDATA[

function showme()
{
    var StringConvert = document.getElementById("myform").outerHTML
    document.getElementById("textsubmit").innerText = StringConvert ;
    document.myform.submit();
}

function icomplete(what)
{
     alert(what);
}
 

// ]]>
</script>
</head>
<body>
    <br />
  
    <table cellpadding="0" cellspacing="0" class="t9" style="width: 748px; border-top-style: solid;
        border-right-style: solid; border-left-style: solid; border-bottom-style: solid">
        <tr class="tr11">
            <td class="td514">
<input id="Button2" style="width: 80px" title="buton" type="button" value="Complete" onclick="javascript:icomplete('4.1');" /><br />               
                <font class="ft54"><strong>4.1</strong></font></td>
            <td class="td515" style="font-weight: bold; width: 207px">
                <font class="ft54">Nominated director (or equivalent)</font></td>
            <td class="td516" style="font-weight: bold; width: 137px">
                <font class="ft54">Information</font></td>
            <td class="td329" style="font-weight: bold; width: 113px; text-align: left;">
                <font class="ft54">Document reference</font></td>
            <td class="td517" style="font-weight: bold; width: 118px; text-align: left;">
                <font class="ft54">Authorised signature</font></td>
            <td class="td518" style="font-weight: bold; text-align: left;">
                <font class="ft54">Completion date</font></td>
        </tr>
        <tr class="tr22" style="font-weight: bold">
            <td class="td519">
                &nbsp;</td>
            <td class="td520" style="width: 207px">
                <font class="ft56">&nbsp;</font></td>
            <td class="td521" style="width: 137px">
                &nbsp;</td>
            <td class="td522" style="width: 113px">
                <font class="ft56"></font>
            </td>
            <td class="td523" style="width: 118px">
                <font class="ft56">&nbsp;</font></td>
            <td class="td524">
                <font class="ft56">&nbsp;</font></td>
        </tr>
        <tr class="tr10">
            <td class="td525">
                &nbsp;</td>
            <td class="td520" style="width: 207px">
                Name</td>
            <td class="td526" style="width: 137px">
                &nbsp;</td>
            <td class="td364" style="width: 113px">
                &nbsp;</td>
            <td class="td527" style="width: 118px">
                &nbsp;</td>
            <td class="td528">
                &nbsp;</td>
        </tr>
        <tr class="tr2">
            <td class="td529" style="height: 19px">
                &nbsp;</td>
            <td class="td530" style="width: 207px; height: 19px">
                <font class="ft85">Evidence of position in</font></td>
            <td class="td531" style="width: 137px; height: 19px">
                <font class="ft67"><em>Schedule attached</em></font></td>
            <td class="td345" style="width: 113px; font-style: italic; height: 19px">
                &nbsp;</td>
            <td class="td532" style="width: 118px; height: 19px">
                &nbsp;</td>
            <td class="td533" style="height: 19px">
                &nbsp;</td>
        </tr>
        <tr class="tr22">
            <td class="td519">
                &nbsp;</td>
            <td class="td520" style="width: 207px">
                <font class="ft58">organisation</font></td>
            <td class="td521" style="width: 137px">
                &nbsp;</td>
            <td class="td352" style="width: 113px">
                &nbsp;</td>
            <td class="td534" style="width: 118px">
                &nbsp;</td>
            <td class="td535">
                &nbsp;</td>
        </tr>
        <tr class="tr10">
            <td class="td525">
                &nbsp;</td>
            <td class="td520" style="width: 207px">
                Evidence of appointment</td>
            <td class="td536" style="width: 137px">
                <font class="ft64"><em>Schedule attached</em></font></td>
            <td class="td364" style="width: 113px; font-style: italic">
                &nbsp;</td>
            <td class="td527" style="width: 118px">
                &nbsp;</td>
            <td class="td528">
                &nbsp;</td>
        </tr>
        <tr class="tr13">
            <td class="td525">
                &nbsp;</td>
            <td class="td520" style="width: 207px">
                Date effective from</td>
            <td class="td526" style="width: 137px; text-align: center;">
                &nbsp;<input id="Text6" style="width: 86px" type="text" /></td>
            <td class="td364" style="width: 113px">
                &nbsp;</td>
            <td class="td527" style="width: 118px">
                &nbsp;</td>
            <td class="td528">
                &nbsp;</td>
        </tr>
        <tr class="tr10">
            <td class="td525">
                &nbsp;</td>
            <td class="td520" style="width: 207px">
                Contact address</td>
            <td class="td526" style="width: 137px">
                &nbsp;</td>
            <td class="td364" style="width: 113px">
                &nbsp;</td>
            <td class="td527" style="width: 118px">
                &nbsp;</td>
            <td class="td528">
                &nbsp;</td>
        </tr>
        <tr class="tr10">
            <td class="td525">
                &nbsp;</td>
            <td class="td520" style="width: 207px">
                Contact telephone number</td>
            <td class="td526" style="width: 137px">
                &nbsp;</td>
            <td class="td364" style="width: 113px">
                &nbsp;</td>
            <td class="td527" style="width: 118px">
                &nbsp;</td>
            <td class="td528">
                &nbsp;</td>
        </tr>
        <tr class="tr13">
            <td class="td525">
                &nbsp;</td>
            <td class="td520" style="width: 207px">
                Contact email</td>
            <td class="td526" style="width: 137px">
                &nbsp;</td>
            <td class="td364" style="width: 113px">
                &nbsp;</td>
            <td class="td527" style="width: 118px">
                &nbsp;</td>
            <td class="td528">
                &nbsp;</td>
        </tr>
    </table>
    <br />
   
    <table cellpadding="0" cellspacing="0" class="t9" style="border-top-style: solid;
        border-right-style: solid; border-left-style: solid; border-bottom-style: solid">
        <tr class="tr2">
            <td class="td537" style="height: 19px">
<input id="Button2" style="width: 80px" title="buton" type="button" value="Complete" onclick="javascript:icomplete('4.2');" /><br />               
                <font class="ft54"><strong>4.2</strong></font></td>
            <td class="td538" style="font-weight: bold; width: 202px; height: 19px">
                <font class="ft54">Other officers</font></td>
            <td class="td260" style="font-weight: bold; width: 136px; height: 19px">
                <font class="ft54">Information</font></td>
            <td class="td539" style="font-weight: bold; width: 119px; height: 19px; text-align: left;">
                <font class="ft54">Document reference</font></td>
            <td class="td540" style="font-weight: bold; width: 119px; height: 19px; text-align: left;">
                <font class="ft54">Authorised signature</font></td>
            <td class="td541" style="font-weight: bold; width: 156px; height: 19px; text-align: left;">
                <font class="ft54">Completion date</font></td>
        </tr>
        <tr class="tr22" style="font-weight: bold">
            <td class="td542">
                &nbsp;</td>
            <td class="td543" style="width: 202px">
                &nbsp;</td>
            <td class="td544" style="width: 136px">
                &nbsp;</td>
            <td class="td545" style="width: 119px">
                <font class="ft56">&nbsp;</font></td>
            <td class="td546" style="width: 119px">
                <font class="ft56">&nbsp;</font></td>
            <td class="td547" style="width: 156px">
                <font class="ft56">&nbsp;</font></td>
        </tr>
        <tr class="tr13">
            <td class="td548">
                &nbsp;</td>
            <td class="td549" style="width: 202px">
                <font class="ft60"><strong>Primary contact</strong></font></td>
            <td class="td550" style="font-weight: bold; width: 136px">
                &nbsp;</td>
            <td class="td551" style="width: 119px">
                &nbsp;</td>
            <td class="td552" style="width: 119px">
                &nbsp;</td>
            <td class="td553" style="width: 156px">
                &nbsp;</td>
        </tr>
        <tr class="tr13">
            <td class="td548">
                &nbsp;</td>
            <td class="td549" style="width: 202px">
                Primary contact</td>
            <td class="td554" style="width: 136px">
                <font class="ft64"><em>Contact details</em></font></td>
            <td class="td551" style="width: 119px; font-style: italic">
                &nbsp;</td>
            <td class="td552" style="width: 119px">
                &nbsp;</td>
            <td class="td553" style="width: 156px">
                &nbsp;</td>
        </tr>
        <tr class="tr10">
            <td class="td548">
                &nbsp;</td>
            <td class="td549" style="width: 202px">
                Secondary contact</td>
            <td class="td554" style="width: 136px">
                <font class="ft64"><em>Contact details</em></font></td>
            <td class="td551" style="width: 119px; font-style: italic">
                &nbsp;</td>
            <td class="td552" style="width: 119px">
                &nbsp;</td>
            <td class="td553" style="width: 156px">
                &nbsp;</td>
        </tr>
        <tr class="tr13">
            <td class="td548">
                &nbsp;</td>
            <td class="td549" style="width: 202px">
                Account representative 1</td>
            <td class="td554" style="width: 136px">
                <font class="ft64"><em>Contact details</em></font></td>
            <td class="td551" style="width: 119px; font-style: italic">
                &nbsp;</td>
            <td class="td552" style="width: 119px">
                &nbsp;</td>
            <td class="td553" style="width: 156px">
                &nbsp;</td>
        </tr>
        <tr class="tr13">
            <td class="td548">
                &nbsp;</td>
            <td class="td549" style="width: 202px">
                Account representative 2</td>
            <td class="td554" style="width: 136px">
                <font class="ft64"><em>Contact details</em></font></td>
            <td class="td551" style="width: 119px; font-style: italic">
                &nbsp;</td>
            <td class="td552" style="width: 119px">
                &nbsp;</td>
            <td class="td553" style="width: 156px">
                &nbsp;</td>
        </tr>
        <tr class="tr10">
            <td class="td548">
                &nbsp;</td>
            <td class="td549" style="width: 202px">
                <font class="ft58">Account representative 3</font></td>
            <td class="td554" style="width: 136px">
                <font class="ft70"><em>Contact details</em></font></td>
            <td class="td551" style="width: 119px; font-style: italic">
                &nbsp;</td>
            <td class="td552" style="width: 119px">
                &nbsp;</td>
            <td class="td553" style="width: 156px">
                &nbsp;</td>
        </tr>
    </table>
    <br />

   
    <table id="TABLE1" cellpadding="0" cellspacing="0" class="t8" style="border-top-style: solid;
        border-right-style: solid; border-left-style: solid; text-align: left; border-bottom-style: solid" onclick="return TABLE1_onclick()">
        <tr class="tr11">
            <td class="td568" style="height: 38px">
<input id="Button2" style="width: 80px" title="buton" type="button" value="Complete" onclick="javascript:icomplete('4.3');" /><br />               
                <font class="ft54"><strong>4.3</strong></font></td>
            <td class="td569" style="font-weight: bold; width: 203px; height: 38px;">
                <font class="ft54">Related organisation</font></td>
            <td class="td435" style="font-weight: bold; width: 160px; height: 38px;">
                <font class="ft54">Information</font></td>
            <td class="td570" style="font-weight: bold; width: 117px; height: 38px;">
                <font class="ft54">Document reference</font></td>
            <td class="td435" style="font-weight: bold; width: 119px; height: 38px;">
                <font class="ft54">Authorised signature</font></td>
            <td class="td571" style="font-weight: bold; width: 157px; height: 38px;">
                <font class="ft54">Completion date</font></td>
        </tr>
        <tr class="tr2">
            <td class="td577">
                &nbsp;</td>
            <td class="td578" style="width: 203px; text-align: left;">
                Are you responsible for CRC
                energy supplies for a related organisation?
                Identify organisations concerned</td>
            <td class="td439" style="width: 160px; text-align: center">
                <font class="ft67"><em>
                    <select id="Select1" style="width: 53px">
                        <option selected="selected" value="Yes">Yes</option>
                        <option value="No">No</option>
                    </select>
                </em></font>
            </td>
            <td class="td579" style="width: 117px; font-style: italic">
                &nbsp;</td>
            <td class="td448" style="width: 119px">
                &nbsp;</td>
            <td class="td580" style="width: 157px; text-align: center;">
                <input id="Text1" style="width: 86px" type="text" />&nbsp;</td>
        </tr>
        <tr class="tr10">
            <td class="td577">
                &nbsp;</td>
            <td class="td578" style="width: 203px; text-align: left;">
                &nbsp;</td>
            <td class="td448" style="width: 160px">
                &nbsp;</td>
            <td class="td579" style="width: 117px">
                &nbsp;</td>
            <td class="td448" style="width: 119px">
                &nbsp;</td>
            <td class="td580" style="width: 157px">
                &nbsp;</td>
        </tr>
        <tr class="tr2">
            <td class="td577">
                &nbsp;</td>
            <td class="td578" style="width: 203px; text-align: left;">
                How is this accounted for/reported to you?</td>
            <td class="td439" style="width: 160px">
                <font class="ft67"><em>Link &nbsp;</em></font></td>
            <td class="td579" style="width: 117px; font-style: italic">
                &nbsp;</td>
            <td class="td448" style="width: 119px">
                &nbsp;</td>
            <td class="td580" style="width: 157px; text-align: center;">
                <input id="Text2" style="width: 86px" type="text" />&nbsp;</td>
        </tr>
    </table>
    <br />
    
    <table cellpadding="0" cellspacing="0" class="t6" style="border-top-style: solid;
        border-right-style: solid; border-left-style: solid; border-bottom-style: solid; text-align: left;" id="TABLE2" onclick="return TABLE2_onclick()">
        <tr class="tr11">
            <td class="td588" style="height: 38px; width: 20px;">
<input id="Button2" style="width: 80px" title="buton" type="button" value="Complete" onclick="javascript:icomplete('4.4');" /><br />              
                <font class="ft54"><strong>4.4</strong></font></td>
            <td class="td589" style="font-weight: bold; width: 271px; height: 38px">
                <font class="ft54">Action</font></td>
            <td class="td590" style="font-weight: bold; width: 265px; height: 38px">
                <font class="ft54">Information</font></td>
            <td class="td591" style="font-weight: bold; width: 108px; height: 38px; text-align: center;">
                <font class="ft54">Document 
                    <br />
                    reference</font></td>
            <td class="td591" style="font-weight: bold; width: 116px; height: 38px; text-align: center;">
                <font class="ft54">Authorised signature</font></td>
            <td class="td592" style="font-weight: bold; width: 161px; height: 38px; text-align: center;">
                <font class="ft54">Completion date</font></td>
        </tr>
        <tr class="tr21" style="font-weight: bold">
            <td class="td593" style="width: 20px">
                &nbsp;</td>
            <td class="td594" style="width: 271px">
                &nbsp;</td>
            <td class="td595" style="width: 265px">
                &nbsp;</td>
            <td class="td596" style="width: 108px">
                <font class="ft87">&nbsp;</font></td>
            <td class="td596" style="width: 116px">
                <font class="ft87">&nbsp;</font></td>
            <td class="td597" style="width: 161px">
                &nbsp;</td>
        </tr>
        <tr class="tr2">
            <td class="td436" style="width: 20px">
                &nbsp;</td>
            <td class="td598" style="width: 271px; text-align: left;">
                Do you have any CRC procedures?</td>
            <td class="td599" style="width: 265px; text-align: center;">
                <font class="ft67"><em><select id="Select2" style="width: 53px">
                    <option selected="selected" value="Yes">Yes</option>
                    <option value="No">No</option>
                </select>
                </em></font></td>
            <td class="td600" style="width: 108px; font-style: italic">
                &nbsp;</td>
            <td class="td600" style="width: 116px">
                &nbsp;</td>
            <td class="td601" style="width: 161px; text-align: center;">
                &nbsp;<input id="Text3" style="width: 86px" type="text" /></td>
        </tr>
        <tr class="tr19">
            <td class="td441" style="width: 20px">
                &nbsp;</td>
            <td class="td602" style="width: 271px">
                <font class="ft61">&nbsp;</font></td>
            <td class="td603" style="width: 265px">
                &nbsp;</td>
            <td class="td604" style="width: 108px">
                &nbsp;</td>
            <td class="td604" style="width: 116px">
                &nbsp;</td>
            <td class="td605" style="width: 161px">
                &nbsp;</td>
        </tr>
        <tr class="tr2">
            <td class="td436" style="width: 20px">
                &nbsp;</td>
            <td class="td598" style="width: 271px">
                If yes, are any of these
                integrated into ISO9001
                or ISO14001management standards?</td>
            <td class="td599" style="width: 265px; text-align: center;">
                <font class="ft67"><em><select id="Select3" style="width: 53px">
                    <option selected="selected" value="Yes">Yes</option>
                    <option value="No">No</option>
                </select>
                    <br />
                    <textarea id="TextArea1" style="width: 243px; height: 141px"></textarea></em></font></td>
            <td class="td606" style="width: 108px; font-style: italic">
                &nbsp;</td>
            <td class="td606" style="width: 116px">
                &nbsp;</td>
            <td class="td601" style="width: 161px; text-align: center;"><input id="Text4" style="width: 86px" type="text" />&nbsp;</td>
        </tr>
        <tr class="tr10">
            <td class="td436" style="width: 20px; height: 19px">
                &nbsp;</td>
            <td class="td598" style="width: 271px; height: 19px;">
                &nbsp;</td>
            <td class="td599" style="width: 265px; height: 19px;">
                <font class="ft67"><em></em></font></td>
            <td class="td606" style="width: 108px; font-style: italic; height: 19px;">
                &nbsp;</td>
            <td class="td606" style="width: 116px; height: 19px;">
                &nbsp;</td>
            <td class="td601" style="width: 161px; height: 19px;">
                &nbsp;</td>
        </tr>
        <tr class="tr2">
            <td class="td436" style="width: 20px">
                &nbsp;</td>
            <td class="td598" style="width: 271px">
                If yes, date CRC
                procedures were last reviewed</td>
            <td class="td599" style="width: 265px; text-align: center;">
                <font class="ft67"><em>&nbsp;<input id="Text5" style="width: 86px" type="text" /></em></font></td>
            <td class="td606" style="width: 108px; font-style: italic">
                &nbsp;</td>
            <td class="td606" style="width: 116px">
                &nbsp;</td>
            <td class="td601" style="width: 161px">
                &nbsp;</td>
        </tr>
        <tr class="tr19">
            <td class="td441" style="width: 20px">
                &nbsp;</td>
            <td class="td602" style="width: 271px">
                <font class="ft61"></font></td>
            <td class="td603" style="width: 265px">
                &nbsp;</td>
            <td class="td604" style="width: 108px">
                &nbsp;</td>
            <td class="td604" style="width: 116px">
                &nbsp;</td>
            <td class="td605" style="width: 161px">
                &nbsp;</td>
        </tr>
        <tr class="tr2">
            <td class="td436" style="width: 20px">
                &nbsp;</td>
            <td class="td598" style="width: 271px">
                If yes, are procedures
                attached as an annex to the evidence pack?</td>
            <td class="td599" style="width: 265px; text-align: center;">
                <font class="ft67"><em><select id="Select4" style="width: 53px">
                    <option selected="selected" value="Yes">Yes</option>
                    <option value="No">No</option>
                </select>
                </em></font></td>
            <td class="td606" style="width: 108px; font-style: italic">
                &nbsp;</td>
            <td class="td606" style="width: 116px">
                &nbsp;</td>
            <td class="td601" style="width: 161px">
                &nbsp;</td>
        </tr>
    </table>

</body>
</html>
