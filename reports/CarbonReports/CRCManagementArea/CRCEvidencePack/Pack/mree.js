function isValidDate(sText) 
{ 
    var reDate = /(?:0[1-9]|[12][0-9]|3[01])\/(?:0[1-9]|1[0-2])\/(?:19|20\d{2})/; 
    return reDate.test(sText); 
} 
    
function validate(what) 
{ 
        var oInput1 = document.getElementById(what); 
        if (isValidDate(oInput1.value)) 
         { 
              oInput1.style.backgroundColor="white"; 
              return 1
         }
              else 
         { 
              oInput1.style.backgroundColor="red"; 
              oInput1.focus();
	          document.getElementById("mylabel").innerText="Not all Completion dates have been filled in Correctly !!!";
              return 0;
         } 
}

 
function validateall(total,current,virtualid,tag)
{
      if (total==current)
        {
           window.location.href="masterevidence_update.asp?virtualid=" + virtualid + "&tag=" + tag 
        }
}
 

function PadDigits(n, totalDigits) 
    { 
        n = n.toString(); 
        var pd = ''; 
        if (totalDigits > n.length) 
        { 
            for (i=0; i < (totalDigits-n.length); i++) 
            { 
                pd += '0'; 
            } 
        } 
           return pd + n.toString(); 
    } 
 
