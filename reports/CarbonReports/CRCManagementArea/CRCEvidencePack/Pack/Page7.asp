<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <title>Untitled Page</title>
    <script language="javascript" type="text/javascript">
// <!CDATA[

function showme()
{
    var StringConvert = document.getElementById("myform").outerHTML
    document.getElementById("textsubmit").innerText = StringConvert ;
    document.myform.submit();
}

function icomplete(what)
{
     alert(what);
}
 

// ]]>
</script>
</head>
<body>
    <br />
    <table id="TABLE1" cellpadding="0" cellspacing="0" class="t8" onclick="return TABLE1_onclick()" style="border-top-style: solid; border-right-style: solid; border-left-style: solid; border-bottom-style: solid">
        <tr class="tr11">
            <td class="td588" style="width: 50px">
<input id="Button2" style="width: 80px" title="buton" type="button" value="Complete" onclick="javascript:icomplete('7.1');" /><br />              
                <font class="ft54"><strong>7.1</strong></font></td>
            <td class="td845" style="font-weight: bold; width: 271px;">
                <font class="ft54">Summary of records</font></td>
            <td class="td490" style="font-weight: bold; width: 162px;">
                <font class="ft54">Information</font></td>
            <td class="td259" style="font-weight: bold; width: 163px;">
                <font class="ft54">Document</font></td>
            <td class="td846" style="font-weight: bold; width: 141px;">
                <font class="ft54">Authorised</font></td>
            <td class="td847" style="font-weight: bold; width: 129px;">
                <font class="ft54">Completion</font></td>
        </tr>
        <tr class="tr22" style="font-weight: bold">
            <td class="td848" style="width: 50px">
                &nbsp;</td>
            <td class="td849" style="width: 271px">
                &nbsp;</td>
            <td class="td513" style="width: 162px">
                &nbsp;</td>
            <td class="td273" style="width: 163px">
                <font class="ft56">reference</font></td>
            <td class="td850" style="width: 141px">
                <font class="ft56">signature</font></td>
            <td class="td300" style="width: 129px">
                <font class="ft56">date</font></td>
        </tr>
        <tr class="tr2">
            <td class="td436" style="width: 50px">
                &nbsp;</td>
            <td class="td851" style="width: 271px">
                Headline energy data for the most</td>
            <td class="td496" style="width: 162px">
                <font class="ft67"><em>&nbsp;</em></font></td>
            <td class="td70" style="font-style: italic; width: 163px;">
                &nbsp;</td>
            <td class="td821" style="width: 141px">
                &nbsp;</td>
            <td class="td312" style="width: 129px">
                &nbsp;</td>
        </tr>
        <tr class="tr10">
            <td class="td436" style="width: 50px">
                &nbsp;</td>
            <td class="td851" style="width: 271px">
                recent annual report has been</td>
            <td class="td496" style="width: 162px; text-align: center">
                <font class="ft67"><em>&nbsp;<select id="Select1" style="width: 63px">
                    <option selected="selected" value="Yes">Yes</option>
                    <option value="No">No</option>
                </select></em></font></td>
            <td class="td70" style="font-style: italic; width: 163px;">
                &nbsp;</td>
            <td class="td821" style="width: 141px">
                &nbsp;</td>
            <td class="td312" style="width: 129px; text-align: center">
                &nbsp;<input id="Text1" style="width: 90px" type="text" /></td>
        </tr>
        <tr class="tr10">
            <td class="td436" style="width: 50px">
                &nbsp;</td>
            <td class="td851" style="width: 271px">
                summarised in Section 2 of this document</td>
            <td class="td182" style="width: 162px">
                &nbsp;</td>
            <td class="td70" style="width: 163px">
                &nbsp;</td>
            <td class="td821" style="width: 141px">
                &nbsp;</td>
            <td class="td312" style="width: 129px">
                &nbsp;</td>
        </tr>
        <tr class="tr19">
            <td class="td441" style="width: 50px">
                &nbsp;</td>
            <td class="td852" style="width: 271px">
                <font class="ft61">&nbsp;</font></td>
            <td class="td195" style="width: 162px">
                &nbsp;</td>
            <td class="td168" style="width: 163px">
                &nbsp;</td>
            <td class="td824" style="width: 141px">
                &nbsp;</td>
            <td class="td319" style="width: 129px">
                &nbsp;</td>
        </tr>
        <tr class="tr2">
            <td class="td436" style="width: 50px">
            </td>
            <td class="td851" style="width: 271px">
            </td>
            <td class="td496" style="width: 162px">
            </td>
            <td class="td70" style="width: 163px; font-style: italic">
            </td>
            <td class="td821" style="width: 141px">
            </td>
            <td class="td312" style="width: 129px">
            </td>
        </tr>
        <tr class="tr2">
            <td class="td436" style="width: 50px">
                &nbsp;</td>
            <td class="td851" style="width: 271px">
                The most recent annual report has</td>
            <td class="td496" style="width: 162px">
                <font class="ft67"><em>&nbsp;</em></font></td>
            <td class="td70" style="font-style: italic; width: 163px;">
                &nbsp;</td>
            <td class="td821" style="width: 141px">
                &nbsp;</td>
            <td class="td312" style="width: 129px">
                &nbsp;</td>
        </tr>
        <tr class="tr10">
            <td class="td436" style="width: 50px">
                &nbsp;</td>
            <td class="td851" style="width: 271px">
                been attached as a schedule to this</td>
            <td class="td496" style="width: 162px; text-align: center">
                <font class="ft67"><em>&nbsp;<select id="Select2" style="width: 63px">
                    <option selected="selected" value="Yes">Yes</option>
                    <option value="No">No</option>
                </select></em></font></td>
            <td class="td70" style="font-style: italic; width: 163px;">
                &nbsp;</td>
            <td class="td821" style="width: 141px">
                &nbsp;</td>
            <td class="td312" style="width: 129px; text-align: center">
                &nbsp;<input id="Text2" style="width: 90px" type="text" /></td>
        </tr>
        <tr class="tr19">
            <td class="td441" style="width: 50px">
                &nbsp;</td>
            <td class="td852" style="width: 271px">
                <font class="ft61">section</font></td>
            <td class="td195" style="width: 162px">
                &nbsp;</td>
            <td class="td168" style="width: 163px">
                &nbsp;</td>
            <td class="td824" style="width: 141px">
                &nbsp;</td>
            <td class="td319" style="width: 129px">
                &nbsp;</td>
        </tr>
    </table>
    <br />
    <table cellpadding="0" cellspacing="0" class="t15" style="border-top-style: solid;
        border-right-style: solid; border-left-style: solid; border-bottom-style: solid" id="TABLE3" onclick="return TABLE3_onclick()">
        <tr class="tr11">
            <td class="td679" style="width: 48px; text-align: center; height: 19px;">
<input id="Button2" style="width: 80px" title="buton" type="button" value="Complete" onclick="javascript:icomplete('7.2.1');" /><br />              
                7.2.1</td>
            <td class="td853" colspan="2" style="height: 19px">
                &nbsp;</td>
            <td class="td854" style="width: 266px; height: 19px;">
                Data for the annual reporting</td>
            <td class="td855" style="height: 19px">
                &nbsp;</td>
            <td class="td856" style="height: 19px">
                &nbsp;</td>
            <td class="td857" style="width: 148px; height: 19px;">
                </td>
            <td class="td858" style="height: 19px">
                &nbsp;</td>
            <td class="td859" style="width: 3px; height: 19px;">
                &nbsp;</td>
            <td class="td860" style="width: 150px; height: 19px;">
                </td>
            <td class="td861" style="height: 19px">
                &nbsp;</td>
            <td class="td862" style="width: 133px; height: 19px;">
                </td>
            <td class="td863" style="height: 19px">
                &nbsp;</td>
            <td class="td864" style="width: 131px; height: 19px;">
                </td>
        </tr>
        <tr class="tr22">
            <td class="td698" style="width: 48px">
                &nbsp;</td>
            <td class="td865" colspan="2">
                &nbsp;</td>
            <td class="td866" style="width: 266px">
                <font class="ft56">year</font></td>
            <td class="td867">
                &nbsp;</td>
            <td class="td868">
                &nbsp;</td>
            <td class="td869" style="width: 148px">
                &nbsp;</td>
            <td class="td870">
                &nbsp;</td>
            <td class="td871" style="width: 3px">
                &nbsp;</td>
            <td class="td872" style="width: 150px">
                <font class="ft56"></font></td>
            <td class="td873">
                &nbsp;</td>
            <td class="td874" style="width: 133px">
                <font class="ft56"></font></td>
            <td class="td875">
                &nbsp;</td>
            <td class="td876" style="width: 131px">
                <font class="ft56"></font></td>
        </tr>
        <tr class="tr2">
            <td class="td682" style="width: 48px">
                &nbsp;</td>
            <td class="td877" colspan="2">
                &nbsp;</td>
            <td class="td878" style="width: 266px">
                Total relevant energy supply,</td>
            <td class="td879">
                &nbsp;</td>
            <td class="td647">
                &nbsp;</td>
            <td class="td880" style="width: 148px">
                &nbsp;</td>
            <td class="td881">
                &nbsp;</td>
            <td class="td882" style="width: 3px">
                &nbsp;</td>
            <td class="td344" style="width: 150px">
                &nbsp;</td>
            <td class="td883">
                &nbsp;</td>
            <td class="td70" style="width: 133px">
                &nbsp;</td>
            <td class="td884">
                &nbsp;</td>
            <td class="td304" style="width: 131px">
                &nbsp;</td>
        </tr>
        <tr class="tr22">
            <td class="td698" style="width: 48px">
                &nbsp;</td>
            <td class="td865" colspan="2">
                &nbsp;</td>
            <td class="td866" style="width: 266px">
                <font class="ft56">comprising:</font></td>
            <td class="td867">
                &nbsp;</td>
            <td class="td868">
                &nbsp;</td>
            <td class="td869" style="width: 148px">
                &nbsp;</td>
            <td class="td870">
                &nbsp;</td>
            <td class="td871" style="width: 3px">
                &nbsp;</td>
            <td class="td351" style="width: 150px">
                &nbsp;</td>
            <td class="td873">
                &nbsp;</td>
            <td class="td885" style="width: 133px">
                &nbsp;</td>
            <td class="td875">
                &nbsp;</td>
            <td class="td322" style="width: 131px">
                &nbsp;</td>
        </tr>
        <tr class="tr10">
            <td class="td689" style="width: 48px">
                &nbsp;</td>
            <td class="td886" colspan="2">
                &nbsp;</td>
            <td class="td866" style="width: 266px">
                <font class="ft59">Electricity</font></td>
            <td class="td887">
                &nbsp;</td>
            <td class="td664">
                &nbsp;</td>
            <td class="td888" style="width: 148px; text-align: center;">
                <font class="ft64"><em><input id="Text27" style="width: 90px" type="text" /></em></font></td>
            <td class="td889" style="font-style: italic">
                &nbsp;</td>
            <td class="td890" style="width: 3px">
                &nbsp;</td>
            <td class="td891" style="width: 150px">
                &nbsp;</td>
            <td class="td892">
                &nbsp;</td>
            <td class="td269" style="width: 133px">
                &nbsp;</td>
            <td class="td893">
                &nbsp;</td>
            <td class="td722" style="width: 131px; text-align: center">
                &nbsp;<input id="Text3" style="width: 90px" type="text" /></td>
        </tr>
        <tr class="tr10">
            <td class="td689" style="width: 48px; height: 19px">
                &nbsp;</td>
            <td class="td886" colspan="2" style="height: 19px">
                &nbsp;</td>
            <td class="td866" style="width: 266px; height: 19px">
                <font class="ft58">Natural gas</font></td>
            <td class="td887" style="height: 19px">
                &nbsp;</td>
            <td class="td664" style="height: 19px">
                &nbsp;</td>
            <td class="td888" style="width: 148px; height: 19px; text-align: center;">
                <font class="ft70"><em><input id="Text28" style="width: 90px" type="text" /></em></font></td>
            <td class="td889" style="font-style: italic; height: 19px">
                &nbsp;</td>
            <td class="td890" style="width: 3px; height: 19px">
                &nbsp;</td>
            <td class="td891" style="width: 150px; height: 19px">
                &nbsp;</td>
            <td class="td892" style="height: 19px">
                &nbsp;</td>
            <td class="td269" style="width: 133px; height: 19px">
                &nbsp;</td>
            <td class="td893" style="height: 19px">
                &nbsp;</td>
            <td class="td722" style="width: 131px; height: 19px; text-align: center">
                &nbsp;<input id="Text4" style="width: 90px" type="text" /></td>
        </tr>
        <tr class="tr13">
            <td class="td693" style="width: 48px">
                &nbsp;</td>
            <td class="td894" colspan="2">
                &nbsp;</td>
            <td class="td866" style="width: 266px">
                <font class="ft59">Fuel 1</font></td>
            <td class="td895">
                &nbsp;</td>
            <td class="td896">
                &nbsp;</td>
            <td class="td888" style="width: 148px; text-align: center;">
                <font class="ft64"><em><input id="Text29" style="width: 90px" type="text" /></em></font></td>
            <td class="td897" style="font-style: italic">
                &nbsp;</td>
            <td class="td898" style="width: 3px">
                &nbsp;</td>
            <td class="td363" style="width: 150px">
                &nbsp;</td>
            <td class="td899">
                &nbsp;</td>
            <td class="td900" style="width: 133px">
                &nbsp;</td>
            <td class="td901">
                &nbsp;</td>
            <td class="td733" style="width: 131px; text-align: center">
                &nbsp;<input id="Text5" style="width: 90px" type="text" /></td>
        </tr>
        <tr class="tr10">
            <td class="td689" style="width: 48px">
                &nbsp;</td>
            <td class="td886" colspan="2">
                &nbsp;</td>
            <td class="td866" style="width: 266px">
                <font class="ft59">Fuel 2</font></td>
            <td class="td887">
                &nbsp;</td>
            <td class="td664">
                &nbsp;</td>
            <td class="td888" style="width: 148px; text-align: center;">
                <font class="ft64"><em><input id="Text30" style="width: 90px" type="text" /></em></font></td>
            <td class="td889" style="font-style: italic">
                &nbsp;</td>
            <td class="td890" style="width: 3px">
                &nbsp;</td>
            <td class="td891" style="width: 150px">
                &nbsp;</td>
            <td class="td892">
                &nbsp;</td>
            <td class="td269" style="width: 133px">
                &nbsp;</td>
            <td class="td893">
                &nbsp;</td>
            <td class="td722" style="width: 131px; text-align: center">
                &nbsp;<input id="Text6" style="width: 90px" type="text" /></td>
        </tr>
        <tr class="tr10">
            <td class="td689" style="width: 48px">
                &nbsp;</td>
            <td class="td886" colspan="2">
                &nbsp;</td>
            <td class="td866" style="width: 266px">
                <font class="ft58">Renewable fuel 1</font></td>
            <td class="td887">
                &nbsp;</td>
            <td class="td664">
                &nbsp;</td>
            <td class="td902" style="width: 148px">
                &nbsp;</td>
            <td class="td889">
                &nbsp;</td>
            <td class="td890" style="width: 3px">
                &nbsp;</td>
            <td class="td891" style="width: 150px">
                &nbsp;</td>
            <td class="td892">
                &nbsp;</td>
            <td class="td269" style="width: 133px">
                &nbsp;</td>
            <td class="td893">
                &nbsp;</td>
            <td class="td722" style="width: 131px">
                &nbsp;</td>
        </tr>
        <tr class="tr13">
            <td class="td693" style="width: 48px">
                &nbsp;</td>
            <td class="td894" colspan="2">
                &nbsp;</td>
            <td class="td866" style="width: 266px">
                <font class="ft59">Renewable fuel 2</font></td>
            <td class="td895">
                &nbsp;</td>
            <td class="td896">
                &nbsp;</td>
            <td class="td903" style="width: 148px">
                &nbsp;</td>
            <td class="td897">
                &nbsp;</td>
            <td class="td898" style="width: 3px">
                &nbsp;</td>
            <td class="td363" style="width: 150px">
                &nbsp;</td>
            <td class="td899">
                &nbsp;</td>
            <td class="td900" style="width: 133px">
                &nbsp;</td>
            <td class="td901">
                &nbsp;</td>
            <td class="td733" style="width: 131px">
                &nbsp;</td>
        </tr>
        <tr class="tr10">
            <td class="td689" style="width: 48px">
                &nbsp;</td>
            <td class="td886" colspan="2">
                &nbsp;</td>
            <td class="td866" style="width: 266px">
            </td>
            <td class="td887">
                &nbsp;</td>
            <td class="td664">
                &nbsp;</td>
            <td class="td902" style="width: 148px">
                &nbsp;</td>
            <td class="td889">
                &nbsp;</td>
            <td class="td890" style="width: 3px">
                &nbsp;</td>
            <td class="td891" style="width: 150px">
                &nbsp;</td>
            <td class="td892">
                &nbsp;</td>
            <td class="td269" style="width: 133px">
                &nbsp;</td>
            <td class="td893">
                &nbsp;</td>
            <td class="td722" style="width: 131px">
                &nbsp;</td>
        </tr>
        <tr class="tr10">
            <td class="td689" style="width: 48px">
                &nbsp;</td>
            <td class="td886" colspan="2">
                &nbsp;</td>
            <td class="td866" style="width: 266px">
                <font class="ft56">EGCs (refer to section 7.2.10)</font></td>
            <td class="td887">
                &nbsp;</td>
            <td class="td664">
                &nbsp;</td>
            <td class="td902" style="width: 148px">
                &nbsp;</td>
            <td class="td889">
                &nbsp;</td>
            <td class="td890" style="width: 3px">
                &nbsp;</td>
            <td class="td891" style="width: 150px">
                &nbsp;</td>
            <td class="td892">
                &nbsp;</td>
            <td class="td269" style="width: 133px">
                &nbsp;</td>
            <td class="td893">
                &nbsp;</td>
            <td class="td722" style="width: 131px">
                &nbsp;</td>
        </tr>
        <tr class="tr2">
            <td class="td682" style="width: 48px">
                &nbsp;</td>
            <td class="td877" colspan="2">
                &nbsp;</td>
            <td class="td878" style="width: 266px">
                Renewable electricity</td>
            <td class="td879">
                &nbsp;</td>
            <td class="td647">
                &nbsp;</td>
            <td class="td880" style="width: 148px">
                &nbsp;</td>
            <td class="td881">
                &nbsp;</td>
            <td class="td882" style="width: 3px">
                &nbsp;</td>
            <td class="td344" style="width: 150px">
                &nbsp;</td>
            <td class="td883">
                &nbsp;</td>
            <td class="td70" style="width: 133px">
                &nbsp;</td>
            <td class="td884">
                &nbsp;</td>
            <td class="td304" style="width: 131px">
                &nbsp;</td>
        </tr>
        <tr class="tr10">
            <td class="td682" style="width: 48px">
                &nbsp;</td>
            <td class="td877" colspan="2">
                &nbsp;</td>
            <td class="td878" style="width: 266px">
                generated (refer to section 7.2.11)</td>
            <td class="td879">
                &nbsp;</td>
            <td class="td647">
                &nbsp;</td>
            <td class="td880" style="width: 148px">
                &nbsp;</td>
            <td class="td881">
                &nbsp;</td>
            <td class="td882" style="width: 3px">
                &nbsp;</td>
            <td class="td344" style="width: 150px">
                &nbsp;</td>
            <td class="td883">
                &nbsp;</td>
            <td class="td70" style="width: 133px">
                &nbsp;</td>
            <td class="td884">
                &nbsp;</td>
            <td class="td304" style="width: 131px">
                &nbsp;</td>
        </tr>
        <tr class="tr19">
            <td class="td685" style="width: 48px">
                &nbsp;</td>
            <td class="td904" colspan="2">
                &nbsp;</td>
            <td class="td905" style="width: 266px">
                <font class="ft57">&nbsp;</font></td>
            <td class="td906">
                &nbsp;</td>
            <td class="td651">
                &nbsp;</td>
            <td class="td907" style="width: 148px">
                &nbsp;</td>
            <td class="td908">
                &nbsp;</td>
            <td class="td909" style="width: 3px">
                &nbsp;</td>
            <td class="td357" style="width: 150px">
                &nbsp;</td>
            <td class="td910">
                &nbsp;</td>
            <td class="td168" style="width: 133px">
                &nbsp;</td>
            <td class="td911">
                &nbsp;</td>
            <td class="td318" style="width: 131px">
                &nbsp;</td>
        </tr>
        <tr class="tr3">
            <td class="td682" style="width: 48px">
                &nbsp;</td>
            <td class="td877" colspan="2">
                &nbsp;</td>
            <td class="td878" style="width: 266px">
                <font class="ft85">Electricity from renewablesources</font></td>
            <td class="td879">
                &nbsp;</td>
            <td class="td647">
                &nbsp;</td>
            <td class="td912" style="width: 148px; text-align: center;">
                <font class="ft67"><em><input id="Text31" style="width: 90px" type="text" /></em></font></td>
            <td class="td881" style="font-style: italic">
                &nbsp;</td>
            <td class="td882" style="width: 3px">
                &nbsp;</td>
            <td class="td344" style="width: 150px">
                &nbsp;</td>
            <td class="td883">
                &nbsp;</td>
            <td class="td70" style="width: 133px">
                &nbsp;</td>
            <td class="td884">
                &nbsp;</td>
            <td class="td304" style="width: 131px; text-align: center">
                &nbsp;<input id="Text7" style="width: 90px" type="text" /></td>
        </tr>
        <tr class="tr19">
            <td class="td685" style="width: 48px">
                &nbsp;</td>
            <td class="td904" colspan="2">
                &nbsp;</td>
            <td class="td866" style="width: 266px">
                <font class="ft61">&nbsp;</font></td>
            <td class="td906">
                &nbsp;</td>
            <td class="td651">
                &nbsp;</td>
            <td class="td907" style="width: 148px">
                &nbsp;</td>
            <td class="td908">
                &nbsp;</td>
            <td class="td909" style="width: 3px">
                &nbsp;</td>
            <td class="td357" style="width: 150px">
                &nbsp;</td>
            <td class="td910">
                &nbsp;</td>
            <td class="td168" style="width: 133px">
                &nbsp;</td>
            <td class="td911">
                &nbsp;</td>
            <td class="td318" style="width: 131px">
                &nbsp;</td>
        </tr>
        <tr class="tr10">
            <td class="td689" style="width: 48px">
                &nbsp;</td>
            <td class="td886" colspan="2">
                &nbsp;</td>
            <td class="td866" style="width: 266px">
                <font class="ft58">ROCs/FITs issued/received</font></td>
            <td class="td887">
                &nbsp;</td>
            <td class="td664">
                &nbsp;</td>
            <td class="td888" style="width: 148px; text-align: center;">
                <font class="ft70"><em><input id="Text32" style="width: 90px" type="text" /></em></font></td>
            <td class="td889" style="font-style: italic">
                &nbsp;</td>
            <td class="td890" style="width: 3px">
                &nbsp;</td>
            <td class="td891" style="width: 150px">
                &nbsp;</td>
            <td class="td892">
                &nbsp;</td>
            <td class="td269" style="width: 133px">
                &nbsp;</td>
            <td class="td893">
                &nbsp;</td>
            <td class="td722" style="width: 131px; text-align: center">
                &nbsp;<input id="Text8" style="width: 90px" type="text" /></td>
        </tr>
        <tr class="tr2">
            <td class="td682" style="width: 48px">
                &nbsp;</td>
            <td class="td877" colspan="2">
                &nbsp;</td>
            <td class="td878" style="width: 266px">
                <font class="ft85">Electricity covered by ROCs or</font></td>
            <td class="td879">
                &nbsp;</td>
            <td class="td647">
                &nbsp;</td>
            <td class="td912" style="width: 148px; text-align: center;">
                <font class="ft67"><em><input id="Text33" style="width: 90px" type="text" /></em></font></td>
            <td class="td881" style="font-style: italic">
                &nbsp;</td>
            <td class="td882" style="width: 3px">
                &nbsp;</td>
            <td class="td344" style="width: 150px">
                &nbsp;</td>
            <td class="td883">
                &nbsp;</td>
            <td class="td70" style="width: 133px">
                &nbsp;</td>
            <td class="td884">
                &nbsp;</td>
            <td class="td304" style="width: 131px; text-align: center">
                &nbsp;<input id="Text9" style="width: 90px" type="text" /></td>
        </tr>
        <tr class="tr22">
            <td class="td698" style="width: 48px">
                &nbsp;</td>
            <td class="td865" colspan="2">
                &nbsp;</td>
            <td class="td866" style="width: 266px">
                <font class="ft58">FIT</font><font class="ft89"><span style="font-size: 6pt">4</span></font></td>
            <td class="td867" style="font-size: 6pt">
                &nbsp;</td>
            <td class="td868">
                &nbsp;</td>
            <td class="td869" style="width: 148px">
                &nbsp;</td>
            <td class="td870">
                &nbsp;</td>
            <td class="td871" style="width: 3px">
                &nbsp;</td>
            <td class="td351" style="width: 150px">
                &nbsp;</td>
            <td class="td873">
                &nbsp;</td>
            <td class="td885" style="width: 133px">
                &nbsp;</td>
            <td class="td875">
                &nbsp;</td>
            <td class="td322" style="width: 131px">
                &nbsp;</td>
        </tr>
        <tr class="tr10">
            <td class="td920" style="width: 48px">
                &nbsp;</td>
            <td class="td921" colspan="3">
                &nbsp;</td>
            <td class="td887">
                &nbsp;</td>
            <td class="td922" colspan="2">
                &nbsp;</td>
            <td class="td923" colspan="2">
                &nbsp;</td>
            <td class="td924" style="width: 150px">
                &nbsp;</td>
            <td class="td925">
                &nbsp;</td>
            <td class="td926" style="width: 133px">
                &nbsp;</td>
            <td class="td927">
                &nbsp;</td>
            <td class="td928" style="width: 131px">
                &nbsp;</td>
        </tr>
        <tr class="tr3">
            <td class="td929" style="width: 48px; text-align: center">
<input id="Button2" style="width: 80px" title="buton" type="button" value="Complete" onclick="javascript:icomplete('7.2.2');" /><br />              
                7.2.2</td>
            <td class="td877" colspan="2">
                &nbsp;</td>
            <td class="td930" style="width: 266px">
                <font class="ft85">Copies of all relevant supplier</font></td>
            <td class="td931" colspan="3">
                <font class="ft67"><em></em></font></td>
            <td class="td932" colspan="3" style="font-style: italic">
                </td>
            <td class="td883">
                &nbsp;</td>
            <td class="td933" style="width: 133px">
                </td>
            <td class="td884">
                &nbsp;</td>
            <td class="td304" style="width: 131px">
                </td>
        </tr>
        <tr class="tr10">
            <td class="td682" style="width: 48px">
                &nbsp;</td>
            <td class="td877" colspan="2">
                &nbsp;</td>
            <td class="td930" style="width: 266px">
                <font class="ft85">invoices in the period have</font></td>
            <td class="td879">
                &nbsp;</td>
            <td class="td580" colspan="2">
                &nbsp;</td>
            <td class="td932" colspan="3">
                </td>
            <td class="td883">
                &nbsp;</td>
            <td class="td933" style="width: 133px">
                </td>
            <td class="td884">
                &nbsp;</td>
            <td class="td304" style="width: 131px">
                </td>
        </tr>
        <tr class="tr10">
            <td class="td682" style="width: 48px">
                &nbsp;</td>
            <td class="td877" colspan="2">
                &nbsp;</td>
            <td class="td930" style="width: 266px">
                <font class="ft85">been collated and are available</font></td>
            <td class="td879">
                &nbsp;</td>
            <td class="td580" colspan="2">
                &nbsp;</td>
            <td class="td934" colspan="2">
                &nbsp;</td>
            <td class="td344" style="width: 150px">
                &nbsp;</td>
            <td class="td883">
                &nbsp;</td>
            <td class="td70" style="width: 133px">
                &nbsp;</td>
            <td class="td884">
                &nbsp;</td>
            <td class="td304" style="width: 131px">
                &nbsp;</td>
        </tr>
        <tr class="tr10">
            <td class="td682" style="width: 48px">
                &nbsp;</td>
            <td class="td877" colspan="2">
                &nbsp;</td>
            <td class="td930" style="width: 266px">
                <font class="ft85">for inspection, for example,</font></td>
            <td class="td879">
                &nbsp;</td>
            <td class="td580" colspan="2" style="text-align: center">
                &nbsp;<select id="Select21" style="width: 71px">
                    <option selected="selected" value="Yes">Yes</option>
                    <option value="No">No</option>
                    <option value="N/A">N/A</option>
                </select></td>
            <td class="td934" colspan="2">
                &nbsp;</td>
            <td class="td344" style="width: 150px">
                &nbsp;</td>
            <td class="td883">
                &nbsp;</td>
            <td class="td70" style="width: 133px">
                &nbsp;</td>
            <td class="td884">
                &nbsp;</td>
            <td class="td304" style="width: 131px; text-align: center">
                &nbsp;<input id="Text10" style="width: 90px" type="text" /></td>
        </tr>
        <tr class="tr10">
            <td class="td682" style="width: 48px">
                &nbsp;</td>
            <td class="td877" colspan="2">
                &nbsp;</td>
            <td class="td930" style="width: 266px">
                <font class="ft85">electricity, natural gas and bulk</font></td>
            <td class="td879">
                &nbsp;</td>
            <td class="td580" colspan="2">
                &nbsp;</td>
            <td class="td934" colspan="2">
                &nbsp;</td>
            <td class="td344" style="width: 150px">
                &nbsp;</td>
            <td class="td883">
                &nbsp;</td>
            <td class="td70" style="width: 133px">
                &nbsp;</td>
            <td class="td884">
                &nbsp;</td>
            <td class="td304" style="width: 131px">
                &nbsp;</td>
        </tr>
        <tr class="tr15">
            <td class="td935" style="width: 48px">
                &nbsp;</td>
            <td class="td936" colspan="2">
                &nbsp;</td>
            <td class="td937" style="width: 266px">
                <font class="ft82">fuels</font></td>
            <td class="td938">
                &nbsp;</td>
            <td class="td939" colspan="2">
                &nbsp;</td>
            <td class="td940" colspan="2">
                &nbsp;</td>
            <td class="td941" style="width: 150px">
                &nbsp;</td>
            <td class="td942">
                &nbsp;</td>
            <td class="td200" style="width: 133px">
                &nbsp;</td>
            <td class="td943">
                &nbsp;</td>
            <td class="td457" style="width: 131px">
                &nbsp;</td>
        </tr>
        <tr class="tr13">
            <td class="td944" style="width: 48px">
                &nbsp;</td>
            <td class="td945" colspan="3">
                &nbsp;</td>
            <td class="td895">
                &nbsp;</td>
            <td class="td946" colspan="2">
                &nbsp;</td>
            <td class="td947" colspan="2">
                &nbsp;</td>
            <td class="td948" style="width: 150px">
                &nbsp;</td>
            <td class="td949">
                &nbsp;</td>
            <td class="td950" style="width: 133px">
                &nbsp;</td>
            <td class="td951">
                &nbsp;</td>
            <td class="td286" style="width: 131px">
                &nbsp;</td>
        </tr>
        <tr class="tr2">
            <td class="td929" style="width: 48px; text-align: center">
<input id="Button2" style="width: 80px" title="buton" type="button" value="Complete" onclick="javascript:icomplete('7.2.3');" /><br />              
                7.2.3</td>
            <td class="td952">
                &nbsp;</td>
            <td class="td953" colspan="2">
                <font class="ft85">Supplier annual statements have</font></td>
            <td class="td954">
                &nbsp;</td>
            <td class="td955" colspan="2" style="text-align: center">
                <font class="ft67"><em></em></font></td>
            <td class="td956" style="font-style: italic">
                &nbsp;</td>
            <td class="td957" style="width: 3px">
                &nbsp;</td>
            <td class="td958" style="width: 150px">
                </td>
            <td class="td959">
                &nbsp;</td>
            <td class="td960" style="width: 133px">
                </td>
            <td class="td884">
                &nbsp;</td>
            <td class="td961" style="width: 131px">
                </td>
        </tr>
        <tr class="tr10">
            <td class="td682" style="width: 48px">
                &nbsp;</td>
            <td class="td952">
                &nbsp;</td>
            <td class="td953" colspan="2">
                <font class="ft85">been collated for the CRC year</font></td>
            <td class="td954">
                &nbsp;</td>
            <td class="td915" colspan="2">
                &nbsp;</td>
            <td class="td962" colspan="2">
                &nbsp;</td>
            <td class="td958" style="width: 150px">
                </td>
            <td class="td959">
                &nbsp;</td>
            <td class="td960" style="width: 133px">
                </td>
            <td class="td884">
                &nbsp;</td>
            <td class="td961" style="width: 131px">
                </td>
        </tr>
        <tr class="tr19">
            <td class="td685" style="width: 48px">
                &nbsp;</td>
            <td class="td963">
                &nbsp;</td>
            <td class="td964" colspan="2">
                <font class="ft61">and are available for inspection</font></td>
            <td class="td965">
                &nbsp;</td>
            <td class="td966" colspan="2" style="text-align: center">
                &nbsp;<select id="Select20" style="width: 71px">
                    <option selected="selected" value="Yes">Yes</option>
                    <option value="No">No</option>
                    <option value="N/A">N/A</option>
                </select></td>
            <td class="td967" colspan="2">
                &nbsp;</td>
            <td class="td968" style="width: 150px">
                &nbsp;</td>
            <td class="td969">
                &nbsp;</td>
            <td class="td970" style="width: 133px">
                &nbsp;</td>
            <td class="td911">
                &nbsp;</td>
            <td class="td318" style="width: 131px; text-align: center">
                &nbsp;<input id="Text11" style="width: 90px" type="text" /></td>
        </tr>
        <tr class="tr21">
            <td class="td972" style="width: 48px">
                &nbsp;</td>
            <td class="td973" colspan="3">
                &nbsp;</td>
            <td class="td974">
                &nbsp;</td>
            <td class="td975" colspan="2">
                &nbsp;</td>
            <td class="td976" colspan="2">
                &nbsp;</td>
            <td class="td977" style="width: 150px">
                &nbsp;</td>
            <td class="td978">
                &nbsp;</td>
            <td class="td979" style="width: 133px">
                &nbsp;</td>
            <td class="td980">
                &nbsp;</td>
            <td class="td981" style="width: 131px">
                &nbsp;</td>
        </tr>
        <tr class="tr21">
            <td class="td972" style="width: 48px">
            </td>
            <td class="td973" colspan="3">
            </td>
            <td class="td974">
            </td>
            <td class="td975" colspan="2">
            </td>
            <td class="td976" colspan="2">
            </td>
            <td class="td977" style="width: 150px">
            </td>
            <td class="td978">
            </td>
            <td class="td979" style="width: 133px">
            </td>
            <td class="td980">
            </td>
            <td class="td981" style="width: 131px">
            </td>
        </tr>
        <tr class="tr21">
            <td class="td972" style="width: 48px">
            </td>
            <td class="td973" colspan="3">
            </td>
            <td class="td974">
            </td>
            <td class="td975" colspan="2">
            </td>
            <td class="td976" colspan="2">
            </td>
            <td class="td977" style="width: 150px">
            </td>
            <td class="td978">
            </td>
            <td class="td979" style="width: 133px">
            </td>
            <td class="td980">
            </td>
            <td class="td981" style="width: 131px">
            </td>
        </tr>
        <tr class="tr2">
            <td class="td929" style="width: 48px; text-align: center">
<input id="Button2" style="width: 80px" title="buton" type="button" value="Complete" onclick="javascript:icomplete('7.2.4');" /><br />              
                7.2.4</td>
            <td class="td952">
                &nbsp;</td>
            <td class="td953" colspan="2">
                <font class="ft85">Estimates have been based on</font></td>
            <td class="td954">
                &nbsp;</td>
            <td class="td955" colspan="2" style="text-align: center">
                <font class="ft67"><em>
                    <select id="Select19" style="width: 71px">
                        <option selected="selected" value="Yes">Yes</option>
                        <option value="No">No</option>
                        <option value="N/A">N/A</option>
                    </select>
                </em></font></td>
            <td class="td956" style="font-style: italic">
                &nbsp;</td>
            <td class="td957" style="width: 3px">
                &nbsp;</td>
            <td class="td958" style="width: 150px">
                </td>
            <td class="td959">
                &nbsp;</td>
            <td class="td960" style="width: 133px">
                </td>
            <td class="td884">
                &nbsp;</td>
            <td class="td961" style="width: 131px">
                </td>
        </tr>
        <tr class="tr10">
            <td class="td682" style="width: 48px">
                &nbsp;</td>
            <td class="td952">
                &nbsp;</td>
            <td class="td953" colspan="2">
                <font class="ft85">calculations and the basis for</font></td>
            <td class="td954">
                &nbsp;</td>
            <td class="td915" colspan="2">
                &nbsp;</td>
            <td class="td962" colspan="2">
                &nbsp;</td>
            <td class="td958" style="width: 150px">
                </td>
            <td class="td959">
                &nbsp;</td>
            <td class="td960" style="width: 133px">
                </td>
            <td class="td884">
                &nbsp;</td>
            <td class="td961" style="width: 131px">
                </td>
        </tr>
        <tr class="tr22">
            <td class="td698" style="width: 48px">
                &nbsp;</td>
            <td class="td982">
                &nbsp;</td>
            <td class="td964" colspan="2">
                <font class="ft58">these is supplied as a schedule</font></td>
            <td class="td983">
                &nbsp;</td>
            <td class="td984" colspan="2">
                &nbsp;</td>
            <td class="td985" colspan="2">
                &nbsp;</td>
            <td class="td986" style="width: 150px">
                &nbsp;</td>
            <td class="td987">
                &nbsp;</td>
            <td class="td988" style="width: 133px">
                &nbsp;</td>
            <td class="td875">
                &nbsp;</td>
            <td class="td322" style="width: 131px">
                &nbsp;</td>
        </tr>
        <tr class="tr32">
            <td class="td706" style="width: 48px">
                &nbsp;</td>
            <td class="td989" colspan="6">
                <font class="ft93"><span style="font-size: 6pt">4 </span></font><font class="ft85">Check
                    ROC banding for ROCs issued.</font></td>
            <td class="td956">
                &nbsp;</td>
            <td class="td882" style="width: 3px">
                &nbsp;</td>
            <td class="td917" style="width: 150px">
                &nbsp;</td>
            <td class="td666">
                &nbsp;</td>
            <td class="td918" style="width: 133px">
                &nbsp;</td>
            <td class="td919">
                &nbsp;</td>
            <td class="td278" style="width: 131px; text-align: center">
                &nbsp;<input id="Text12" style="width: 90px" type="text" /></td>
        </tr>
    </table>
    <br />
    <table cellpadding="0" cellspacing="0" class="t16" style="border-top-style: solid;
        border-right-style: solid; border-left-style: solid; border-bottom-style: solid">
        <tr class="tr11">
            <td class="td992" style="width: 49px; text-align: center">
<input id="Button2" style="width: 80px" title="buton" type="button" value="Complete" onclick="javascript:icomplete('7.2.6');" /><br />              
                <font class="ft54"><strong>7.2.6</strong></font></td>
            <td class="td993" style="font-weight: bold; width: 272px">
                In-house meter read (specify</td>
            <td class="td994" style="width: 155px">
                <font class="ft67"><em></em></font></td>
            <td class="td995" style="width: 178px; font-style: italic">
                <font class="ft67"></font></td>
        </tr>
        <tr class="tr10" style="font-style: italic">
            <td class="td996" style="width: 49px">
                &nbsp;</td>
            <td class="td993" style="width: 272px">
                manual system or automatic,</td>
            <td class="td994" style="width: 155px">
                <font class="ft67"></font></td>
            <td class="td995" style="width: 178px">
                <font class="ft67"></font></td>
        </tr>
        <tr class="tr10">
            <td class="td996" style="width: 49px">
                &nbsp;</td>
            <td class="td993" style="width: 272px">
                for example, EMS) data has</td>
            <td class="td997" style="width: 155px">
                &nbsp;</td>
            <td class="td995" style="width: 178px">
                <font class="ft67"><em></em></font></td>
        </tr>
        <tr class="tr10" style="font-style: italic">
            <td class="td996" style="width: 49px">
                &nbsp;</td>
            <td class="td993" style="width: 272px">
                been corroborated against</td>
            <td class="td997" style="width: 155px; text-align: center;">
                &nbsp;<select id="Select17" style="width: 71px">
                    <option selected="selected" value="Yes">Yes</option>
                    <option value="No">No</option>
                    <option value="N/A">N/A</option>
                </select></td>
            <td class="td998" style="width: 178px; text-align: center;">
                &nbsp;<select id="Select18" style="width: 71px">
                    <option selected="selected" value="Manual">Manual </option>
                    <option value="Automatic">Automatic</option>
                </select></td>
        </tr>
        <tr class="tr10">
            <td class="td996" style="width: 49px">
                &nbsp;</td>
            <td class="td993" style="width: 272px">
                supplier invoices for the</td>
            <td class="td997" style="width: 155px">
                &nbsp;</td>
            <td class="td998" style="width: 178px">
                &nbsp;</td>
        </tr>
        <tr class="tr10">
            <td class="td996" style="width: 49px; height: 19px">
                &nbsp;</td>
            <td class="td993" style="width: 272px; height: 19px">
                scheme year</td>
            <td class="td997" style="width: 155px; height: 19px">
                &nbsp;</td>
            <td class="td998" style="width: 178px; height: 19px">
                &nbsp;</td>
        </tr>
    </table>
    <br />
    <table cellpadding="0" cellspacing="0" class="t18" style="border-top-style: solid;
        border-right-style: solid; border-left-style: solid; border-bottom-style: solid">
        <tr class="tr11">
            <td class="td1008" style="width: 49px; text-align: center">
<input id="Button2" style="width: 80px" title="buton" type="button" value="Complete" onclick="javascript:icomplete('7.2.7');" /><br />              
                7.2.7</td>
            <td class="td1009" style="width: 273px">
                <font class="ft85">The emissions factors used are</font></td>
            <td class="td375" style="width: 172px; text-align: center;">
                <font class="ft67"><em>
                    <select id="Select16" style="width: 71px">
                        <option selected="selected" value="Yes">Yes</option>
                        <option value="No">No</option>
                        <option value="N/A">N/A</option>
                    </select>
                </em></font></td>
            <td class="td463" style="width: 154px; font-style: italic">
                </td>
            <td class="td1010" style="width: 143px">
                </td>
            <td class="td211" style="width: 129px">
                </td>
        </tr>
        <tr class="tr10">
            <td class="td1011" style="width: 49px">
                &nbsp;</td>
            <td class="td1012" style="width: 273px">
                <font class="ft85">those established by the CRC.</font></td>
            <td class="td384" style="width: 172px">
                &nbsp;</td>
            <td class="td1013" style="width: 154px">
                </td>
            <td class="td1014" style="width: 143px">
                </td>
            <td class="td225" style="width: 129px">
                </td>
        </tr>
        <tr class="tr10">
            <td class="td1011" style="width: 49px">
                &nbsp;</td>
            <td class="td1012" style="width: 273px">
                <font class="ft85">List of factors used is supplied as</font></td>
            <td class="td384" style="width: 172px">
                &nbsp;</td>
            <td class="td475" style="width: 154px">
                &nbsp;</td>
            <td class="td216" style="width: 143px">
                &nbsp;</td>
            <td class="td216" style="width: 129px">
                &nbsp;</td>
        </tr>
        <tr class="tr19">
            <td class="td1015" style="width: 49px">
                &nbsp;</td>
            <td class="td1016" style="width: 273px">
                <font class="ft61">a schedule</font></td>
            <td class="td745" style="width: 172px">
                &nbsp;</td>
            <td class="td480" style="width: 154px">
                &nbsp;</td>
            <td class="td221" style="width: 143px">
                &nbsp;</td>
            <td class="td221" style="width: 129px; text-align: center">
                &nbsp;<input id="Text13" style="width: 90px" type="text" /></td>
        </tr>
    </table>
    <br />
    <table cellpadding="0" cellspacing="0" class="t14" style="border-top-style: solid;
        border-right-style: solid; border-left-style: solid; border-bottom-style: solid">
        <tr class="tr11">
            <td class="td486" style="width: 47px; text-align: center">
<input id="Button2" style="width: 80px" title="buton" type="button" value="Complete" onclick="javascript:icomplete('7.2.8');" /><br />              
                7.2.8</td>
            <td class="td1017" style="width: 274px">
                <font class="ft85">Basis for calculations to</font></td>
            <td class="td740" style="width: 173px; text-align: center;">
                <font class="ft67"><em>
                    <select id="Select15" style="width: 71px">
                        <option selected="selected" value="Yes">Yes</option>
                        <option value="No">No</option>
                        <option value="N/A">N/A</option>
                    </select>
                </em></font></td>
            <td class="td1018" style="width: 153px; font-style: italic">
                </td>
            <td class="td1019" style="width: 143px">
                </td>
            <td class="td1019" style="width: 128px">
                </td>
        </tr>
        <tr class="tr10">
            <td class="td492" style="width: 47px">
                &nbsp;</td>
            <td class="td1020" style="width: 274px">
                <font class="ft85">separate excluded use(s)</font></td>
            <td class="td66" style="width: 173px">
                &nbsp;</td>
            <td class="td932" style="width: 153px">
                </td>
            <td class="td1021" style="width: 143px">
                </td>
            <td class="td1022" style="width: 128px">
                &nbsp;</td>
        </tr>
        <tr class="tr10">
            <td class="td492" style="width: 47px">
                &nbsp;</td>
            <td class="td1020" style="width: 274px">
                <font class="ft85">has been supplied as a</font></td>
            <td class="td66" style="width: 173px">
                &nbsp;</td>
            <td class="td761" style="width: 153px">
                &nbsp;</td>
            <td class="td1022" style="width: 143px">
                &nbsp;</td>
            <td class="td1022" style="width: 128px">
                &nbsp;</td>
        </tr>
        <tr class="tr18">
            <td class="td506" style="width: 47px">
                &nbsp;</td>
            <td class="td1023" style="width: 274px">
                <font class="ft73">schedule</font></td>
            <td class="td142" style="width: 173px">
                &nbsp;</td>
            <td class="td767" style="width: 153px">
                &nbsp;</td>
            <td class="td1024" style="width: 143px">
                &nbsp;</td>
            <td class="td1024" style="width: 128px; text-align: center">
                &nbsp;<input id="Text14" style="width: 90px" type="text" /></td>
        </tr>
    </table>
    <br />
    <table cellpadding="0" cellspacing="0" class="t14" style="border-top-style: solid;
        border-right-style: solid; border-left-style: solid; border-bottom-style: solid">
        <tr class="tr11">
            <td class="td431" style="width: 47px; text-align: center">
<input id="Button2" style="width: 80px" title="buton" type="button" value="Complete" onclick="javascript:icomplete('7.2.9');" /><br />              
                7.2.9</td>
            <td class="td1025" style="width: 275px">
                <font class="ft85">Evidence for non-CRC energy</font></td>
            <td class="td1026" style="width: 172px; text-align: center;">
                <font class="ft67"><em>
                    <select id="Select14" style="width: 71px">
                        <option selected="selected" value="Yes">Yes</option>
                        <option value="No">No</option>
                        <option value="N/A">N/A</option>
                    </select>
                </em></font></td>
            <td class="td1027" style="width: 153px; font-style: italic">
                </td>
            <td class="td1028" style="width: 143px">
                </td>
            <td class="td1029" style="width: 128px">
                </td>
        </tr>
        <tr class="tr10">
            <td class="td436" style="width: 47px">
                &nbsp;</td>
            <td class="td1030" style="width: 275px">
                <font class="ft85">sources has been included in the</font></td>
            <td class="td1031" style="width: 172px">
                &nbsp;</td>
            <td class="td1032" style="width: 153px">
                </td>
            <td class="td1033" style="width: 143px">
                </td>
            <td class="td960" style="width: 128px">
                </td>
        </tr>
        <tr class="tr10">
            <td class="td436" style="width: 47px">
                &nbsp;</td>
            <td class="td1030" style="width: 275px">
                <font class="ft85">evidence pack as part of the</font></td>
            <td class="td1031" style="width: 172px">
                &nbsp;</td>
            <td class="td1034" style="width: 153px">
                &nbsp;</td>
            <td class="td1035" style="width: 143px">
                &nbsp;</td>
            <td class="td1036" style="width: 128px">
                &nbsp;</td>
        </tr>
        <tr class="tr19">
            <td class="td441" style="width: 47px">
                &nbsp;</td>
            <td class="td1037" style="width: 275px">
                <font class="ft61">footprint report</font></td>
            <td class="td1038" style="width: 172px">
                &nbsp;</td>
            <td class="td1039" style="width: 153px">
                &nbsp;</td>
            <td class="td1040" style="width: 143px">
                &nbsp;</td>
            <td class="td970" style="width: 128px; text-align: center">
                &nbsp;<input id="Text15" style="width: 90px" type="text" /></td>
        </tr>
    </table>
    <br />
    <table id="TABLE2" cellpadding="0" cellspacing="0" class="t12" style="border-top-style: solid;
        border-right-style: solid; border-left-style: solid; height: 141px; border-bottom-style: solid" onclick="return TABLE2_onclick()">
        <tr class="tr11">
            <td class="td1041" style="width: 46px; text-align: center">
<input id="Button2" style="width: 80px" title="buton" type="button" value="Complete" onclick="javascript:icomplete('7.2.10');" /><br />              
                7.2.10</td>
            <td class="td1042">
                &nbsp;</td>
            <td class="td1025" style="width: 269px">
                <font class="ft85">Calculation giving basis for EGC</font></td>
            <td class="td1026" style="width: 175px; text-align: center;">
                <font class="ft67"><em>
                    <select id="Select13" style="width: 71px">
                        <option selected="selected" value="Yes">Yes</option>
                        <option value="No">No</option>
                        <option value="N/A">N/A</option>
                    </select>
                </em></font></td>
            <td class="td1027" style="width: 152px; font-style: italic">
                </td>
            <td class="td1028" style="width: 142px">
                </td>
            <td class="td1043">
                &nbsp;</td>
            <td class="td1044" style="width: 126px">
                </td>
        </tr>
        <tr class="tr22">
            <td class="td1045" style="width: 46px">
                &nbsp;</td>
            <td class="td983">
                &nbsp;</td>
            <td class="td1037" style="width: 269px">
                <font class="ft58">has been supplied as a schedule</font></td>
            <td class="td1046" style="width: 175px">
                &nbsp;</td>
            <td class="td1047" style="width: 152px">
                <font class="ft56"></font></td>
            <td class="td1048" style="width: 142px">
                <font class="ft56"></font></td>
            <td class="td1049">
                &nbsp;</td>
            <td class="td1050" style="width: 126px">
                <font class="ft56"></font></td>
        </tr>
        <tr class="tr10">
            <td class="td1055" style="width: 46px">
                &nbsp;</td>
            <td class="td1056" colspan="2">
                &nbsp;</td>
            <td class="td1057" style="width: 175px">
                &nbsp;</td>
            <td class="td1058" style="width: 152px">
                &nbsp;</td>
            <td class="td1059" style="width: 142px">
                &nbsp;</td>
            <td class="td890">
                &nbsp;</td>
            <td class="td928" style="width: 126px; text-align: center">
                &nbsp;<input id="Text16" style="width: 90px" type="text" /></td>
        </tr>
        <tr class="tr10">
            <td class="td1055" style="width: 46px">
            </td>
            <td class="td1056" colspan="2">
            </td>
            <td class="td1057" style="width: 175px">
            </td>
            <td class="td1058" style="width: 152px">
            </td>
            <td class="td1059" style="width: 142px">
            </td>
            <td class="td890">
            </td>
            <td class="td928" style="width: 126px">
            </td>
        </tr>
        <tr class="tr10">
            <td class="td1055" style="width: 46px">
            </td>
            <td class="td1056" colspan="2">
            </td>
            <td class="td1057" style="width: 175px">
            </td>
            <td class="td1058" style="width: 152px">
            </td>
            <td class="td1059" style="width: 142px">
            </td>
            <td class="td890">
            </td>
            <td class="td928" style="width: 126px">
            </td>
        </tr>
        <tr class="tr2">
            <td class="td1060" style="width: 46px; text-align: center">
<input id="Button2" style="width: 80px" title="buton" type="button" value="Complete" onclick="javascript:icomplete('7.2.11');" /><br />              
                7.2.11</td>
            <td class="td954">
                &nbsp;</td>
            <td class="td1030" style="width: 269px">
                <font class="ft85">Evidence that renewable energy</font></td>
            <td class="td1061" style="width: 175px; text-align: center;">
                <font class="ft67"><em>
                    <select id="Select10" style="width: 71px">
                        <option selected="selected" value="Yes">Yes</option>
                        <option value="No">No</option>
                        <option value="N/A">N/A</option>
                    </select>
                </em></font></td>
            <td class="td1032" style="width: 152px; font-style: italic">
                </td>
            <td class="td1033" style="width: 142px">
                </td>
            <td class="td1062">
                &nbsp;</td>
            <td class="td961" style="width: 126px">
                </td>
        </tr>
        <tr class="tr10">
            <td class="td1063" style="width: 46px">
                &nbsp;</td>
            <td class="td954">
                &nbsp;</td>
            <td class="td1030" style="width: 269px">
                <font class="ft85">has been generated that claims</font></td>
            <td class="td1031" style="width: 175px">
                &nbsp;</td>
            <td class="td1032" style="width: 152px">
                </td>
            <td class="td1033" style="width: 142px">
                </td>
            <td class="td1062">
                &nbsp;</td>
            <td class="td961" style="width: 126px; text-align: center;">
                <input id="Text17" style="width: 90px" type="text" /></td>
        </tr>
    </table>
    <br />
    <table cellpadding="0" cellspacing="0" class="t19" style="border-top-style: solid;
        border-right-style: solid; border-left-style: solid; border-bottom-style: solid">
        <tr class="tr11">
            <td class="td1066" style="width: 42px; text-align: center">
<input id="Button2" style="width: 80px" title="buton" type="button" value="Complete" onclick="javascript:icomplete('7.3.1');" /><br />              
                7.3.1</td>
            <td class="td1067" style="width: 279px">
                <font class="ft85">Evidence of the percentage of</font></td>
            <td class="td1068" style="width: 177px">
                <font class="ft67"><em></em></font></td>
            <td class="td1069" style="width: 151px; font-style: italic">
                </td>
            <td class="td1029" style="width: 145px">
                </td>
            <td class="td1028" style="width: 128px">
                </td>
        </tr>
        <tr class="tr10">
            <td class="td472" style="width: 42px">
                &nbsp;</td>
            <td class="td1070" style="width: 279px">
                <font class="ft85">your electricity and gas supplies</font></td>
            <td class="td1071" style="width: 177px">
                &nbsp;</td>
            <td class="td1072" style="width: 151px">
                </td>
            <td class="td960" style="width: 145px">
                </td>
            <td class="td1033" style="width: 128px">
                </td>
        </tr>
        <tr class="tr10">
            <td class="td1073" style="width: 42px">
            </td>
            <td class="td1070" style="width: 279px">
                <font class="ft85">measured by AMR, HH metering</font></td>
            <td class="td1071" style="width: 177px">
                &nbsp;</td>
            <td class="td1074" style="width: 151px">
                &nbsp;</td>
            <td class="td1036" style="width: 145px">
                &nbsp;</td>
            <td class="td1035" style="width: 128px">
                &nbsp;</td>
        </tr>
        <tr class="tr10">
            <td class="td1073" style="width: 42px">
            </td>
            <td class="td1070" style="width: 279px">
                <font class="ft85">and dynamic unmetered supply</font></td>
            <td class="td1071" style="width: 177px; text-align: center;">
                &nbsp;<select id="Select11" style="width: 71px">
                    <option selected="selected" value="Yes">Yes</option>
                    <option value="No">No</option>
                    <option value="N/A">N/A</option>
                </select></td>
            <td class="td1074" style="width: 151px">
                &nbsp;</td>
            <td class="td1036" style="width: 145px">
                &nbsp;</td>
            <td class="td1035" style="width: 128px; text-align: center;">
                &nbsp;<input id="Text18" style="width: 90px" type="text" /></td>
        </tr>
        <tr class="tr18">
            <td class="td1075" style="width: 42px">
                <font class="ft77"></font>
            </td>
            <td class="td1076" style="width: 279px">
                <font class="ft73">beyond the legal minimum</font></td>
            <td class="td1077" style="width: 177px">
                &nbsp;</td>
            <td class="td1078" style="width: 151px">
                &nbsp;</td>
            <td class="td1079" style="width: 145px">
                &nbsp;</td>
            <td class="td1080" style="width: 128px">
                &nbsp;</td>
        </tr>
        <tr class="tr3">
            <td class="td472" style="width: 42px">
            </td>
            <td class="td1070" style="width: 279px">
            </td>
            <td class="td1081" style="width: 177px">
            </td>
            <td class="td1072" style="width: 151px; font-style: italic">
            </td>
            <td class="td960" style="width: 145px">
            </td>
            <td class="td1033" style="width: 128px">
            </td>
        </tr>
        <tr class="tr3">
            <td class="td472" style="width: 42px">
                &nbsp;</td>
            <td class="td1070" style="width: 279px">
                <font class="ft85">Evidence of the percentage of</font></td>
            <td class="td1081" style="width: 177px; text-align: center;">
                <font class="ft67"><em></em></font></td>
            <td class="td1072" style="width: 151px; font-style: italic">
                </td>
            <td class="td960" style="width: 145px">
                </td>
            <td class="td1033" style="width: 128px">
                </td>
        </tr>
        <tr class="tr10">
            <td class="td472" style="width: 42px">
                &nbsp;</td>
            <td class="td1070" style="width: 279px">
                <font class="ft85">CRC emissions covered by the</font></td>
            <td class="td1071" style="width: 177px">
                &nbsp;</td>
            <td class="td1072" style="width: 151px">
                </td>
            <td class="td960" style="width: 145px">
                </td>
            <td class="td1033" style="width: 128px">
                </td>
        </tr>
        <tr class="tr10">
            <td class="td472" style="width: 42px">
                &nbsp;</td>
            <td class="td1070" style="width: 279px">
                <font class="ft85">CTS or equivalent has been</font></td>
            <td class="td1071" style="width: 177px">
                &nbsp;</td>
            <td class="td1074" style="width: 151px">
                &nbsp;</td>
            <td class="td1036" style="width: 145px">
                &nbsp;</td>
            <td class="td1035" style="width: 128px">
                &nbsp;</td>
        </tr>
        <tr class="tr18">
            <td class="td1082" style="width: 42px">
                &nbsp;</td>
            <td class="td1076" style="width: 279px">
                <font class="ft73">supplied as a schedule</font></td>
            <td class="td1077" style="width: 177px; text-align: center;">
                &nbsp;<select id="Select12" style="width: 71px">
                    <option selected="selected" value="Yes">Yes</option>
                    <option value="No">No</option>
                    <option value="N/A">N/A</option>
                </select></td>
            <td class="td1078" style="width: 151px">
                &nbsp;</td>
            <td class="td1079" style="width: 145px">
                &nbsp;</td>
            <td class="td1080" style="width: 128px; text-align: center;">
                &nbsp;<input id="Text19" style="width: 90px" type="text" /></td>
        </tr>
    </table>
    <br />
    <table cellpadding="0" cellspacing="0" class="t20" style="border-top-style: solid;
        border-right-style: solid; border-left-style: solid; border-bottom-style: solid">
        <tr class="tr11">
            <td class="td431" style="width: 44px; height: 19px; text-align: center">
<input id="Button2" style="width: 80px" title="buton" type="button" value="Complete" onclick="javascript:icomplete('7.3.2');" /><br />              
                7.3.2</td>
            <td class="td1025" style="width: 277px; height: 19px">
                <font class="ft85">Evidence for the reported annual</font></td>
            <td class="td1026" style="width: 178px; height: 19px">
                <font class="ft67"><em></em></font></td>
            <td class="td1027" style="width: 152px; font-style: italic; height: 19px">
                </td>
            <td class="td1028" style="width: 144px; height: 19px">
                </td>
            <td class="td1029" style="width: 126px; height: 19px">
                </td>
        </tr>
        <tr class="tr10">
            <td class="td436" style="width: 44px">
                &nbsp;</td>
            <td class="td1030" style="width: 277px">
                <font class="ft85">turnover (generally private sector)</font></td>
            <td class="td1031" style="width: 178px">
                &nbsp;</td>
            <td class="td1032" style="width: 152px">
                </td>
            <td class="td1033" style="width: 144px">
                </td>
            <td class="td960" style="width: 126px">
                </td>
        </tr>
        <tr class="tr10">
            <td class="td436" style="width: 44px">
                &nbsp;</td>
            <td class="td1030" style="width: 277px">
                <font class="ft85">or revenue expenditure (relevant</font></td>
            <td class="td1031" style="width: 178px">
                &nbsp;</td>
            <td class="td1034" style="width: 152px">
                &nbsp;</td>
            <td class="td1035" style="width: 144px">
                &nbsp;</td>
            <td class="td1036" style="width: 126px">
                &nbsp;</td>
        </tr>
        <tr class="tr10">
            <td class="td436" style="width: 44px">
                &nbsp;</td>
            <td class="td1030" style="width: 277px">
                <font class="ft85">for most public bodies) has been</font></td>
            <td class="td1031" style="width: 178px">
                &nbsp;</td>
            <td class="td1034" style="width: 152px">
                &nbsp;</td>
            <td class="td1035" style="width: 144px">
                &nbsp;</td>
            <td class="td1036" style="width: 126px">
                &nbsp;</td>
        </tr>
        <tr class="tr10">
            <td class="td436" style="width: 44px">
                &nbsp;</td>
            <td class="td1030" style="width: 277px">
                <font class="ft85">supplied as a schedule such as</font></td>
            <td class="td1031" style="width: 178px">
                &nbsp;</td>
            <td class="td1034" style="width: 152px">
                &nbsp;</td>
            <td class="td1035" style="width: 144px">
                &nbsp;</td>
            <td class="td1036" style="width: 126px">
                &nbsp;</td>
        </tr>
        <tr class="tr18">
            <td class="td449" style="width: 44px">
                &nbsp;</td>
            <td class="td1037" style="width: 277px">
                <font class="ft73">annual accounts</font></td>
            <td class="td1083" style="width: 178px; text-align: center;">
                &nbsp;<select id="Select9" style="width: 71px">
                    <option selected="selected" value="Yes">Yes</option>
                    <option value="No">No</option>
                    <option value="N/A">N/A</option>
                </select></td>
            <td class="td1084" style="width: 152px">
                &nbsp;</td>
            <td class="td1080" style="width: 144px">
                &nbsp;</td>
            <td class="td1079" style="width: 126px; text-align: center;">
                &nbsp;<input id="Text20" style="width: 90px" type="text" /></td>
        </tr>
    </table>
    <br />
    <table cellpadding="0" cellspacing="0" class="t6" style="border-top-style: solid;
        border-right-style: solid; border-left-style: solid; border-bottom-style: solid">
        <tr class="tr11">
            <td class="td431" style="width: 45px; text-align: center">
<input id="Button2" style="width: 80px" title="buton" type="button" value="Complete" onclick="javascript:icomplete('7.4.1');" /><br />              
                <font class="ft54"><strong>7.4.1</strong></font></td>
            <td class="td1025" style="font-weight: bold; width: 275px">
                Have you included evidence that</td>
            <td class="td1026" style="width: 180px">
                <font class="ft67"><em></em></font></td>
            <td class="td1027" style="width: 154px; font-style: italic">
                <font class="ft54"><strong></strong></font></td>
            <td class="td1028" style="font-weight: bold; width: 140px">
                <font class="ft54"></font></td>
            <td class="td1029" style="font-weight: bold; width: 128px">
                <font class="ft54"></font></td>
        </tr>
        <tr class="tr10" style="font-weight: bold">
            <td class="td436" style="width: 45px">
                &nbsp;</td>
            <td class="td1030" style="width: 275px">
                your CRC organisation discloses</td>
            <td class="td1061" style="width: 180px">
                <font class="ft67"><em></em></font></td>
            <td class="td1032" style="width: 154px; font-style: italic">
                <font class="ft54"></font></td>
            <td class="td1033" style="width: 140px">
                <font class="ft54"></font></td>
            <td class="td960" style="width: 128px">
                <font class="ft54"></font></td>
        </tr>
        <tr class="tr10">
            <td class="td436" style="width: 45px">
                &nbsp;</td>
            <td class="td1030" style="width: 275px">
                long-term carbon emission</td>
            <td class="td1031" style="width: 180px; text-align: center;">
                &nbsp;</td>
            <td class="td1034" style="width: 154px">
                &nbsp;</td>
            <td class="td1035" style="width: 140px">
                &nbsp;</td>
            <td class="td1036" style="width: 128px">
                &nbsp;</td>
        </tr>
        <tr class="tr10">
            <td class="td436" style="width: 45px">
                &nbsp;</td>
            <td class="td1030" style="width: 275px">
                reduction targets in its annual</td>
            <td class="td1031" style="width: 180px">
                &nbsp;</td>
            <td class="td1034" style="width: 154px">
                &nbsp;</td>
            <td class="td1035" style="width: 140px">
                &nbsp;</td>
            <td class="td1036" style="width: 128px">
                &nbsp;</td>
        </tr>
        <tr class="tr10">
            <td class="td436" style="width: 45px">
                &nbsp;</td>
            <td class="td1030" style="width: 275px">
                reporting in respect of its CRC energy supplies?</td>
            <td class="td1031" style="width: 180px; text-align: center;">
                &nbsp;<select id="Select5" style="width: 71px">
                    <option selected="selected" value="Yes">Yes</option>
                    <option value="No">No</option>
                    <option value="N/A">N/A</option>
                </select></td>
            <td class="td1034" style="width: 154px">
                &nbsp;</td>
            <td class="td1035" style="width: 140px">
                &nbsp;</td>
            <td class="td1036" style="width: 128px; text-align: center;">
                &nbsp;<input id="Text21" style="width: 90px" type="text" /></td>
        </tr>
        <tr class="tr18">
            <td class="td449" style="width: 45px">
                &nbsp;</td>
            <td class="td1037" style="width: 275px">
                <font class="ft73">&nbsp;</font></td>
            <td class="td1083" style="width: 180px">
                &nbsp;</td>
            <td class="td1084" style="width: 154px">
                &nbsp;</td>
            <td class="td1080" style="width: 140px">
                &nbsp;</td>
            <td class="td1079" style="width: 128px">
                &nbsp;</td>
        </tr>
        <tr class="tr3">
            <td class="td436" style="width: 45px">
                &nbsp;</td>
            <td class="td1030" style="width: 275px">
                Have you included evidence that</td>
            <td class="td1031" style="width: 180px">
                &nbsp;</td>
            <td class="td1034" style="width: 154px">
                &nbsp;</td>
            <td class="td1035" style="width: 140px">
                &nbsp;</td>
            <td class="td1036" style="width: 128px">
                &nbsp;</td>
        </tr>
        <tr class="tr10">
            <td class="td436" style="width: 45px">
                &nbsp;</td>
            <td class="td1030" style="width: 275px">
                your CRC organisation discloses</td>
            <td class="td1031" style="width: 180px">
                &nbsp;</td>
            <td class="td1034" style="width: 154px">
                &nbsp;</td>
            <td class="td1035" style="width: 140px">
                &nbsp;</td>
            <td class="td1036" style="width: 128px">
                &nbsp;</td>
        </tr>
        <tr class="tr10">
            <td class="td436" style="width: 45px">
                &nbsp;</td>
            <td class="td1030" style="width: 275px">
                carbon emissions performance</td>
            <td class="td1031" style="width: 180px; text-align: center;">
                &nbsp;</td>
            <td class="td1034" style="width: 154px">
                &nbsp;</td>
            <td class="td1035" style="width: 140px">
                &nbsp;</td>
            <td class="td1036" style="width: 128px">
                &nbsp;</td>
        </tr>
        <tr class="tr10">
            <td class="td436" style="width: 45px">
                &nbsp;</td>
            <td class="td1030" style="width: 275px">
                against these targets, in its annual</td>
            <td class="td1031" style="width: 180px">
                &nbsp;</td>
            <td class="td1034" style="width: 154px">
                &nbsp;</td>
            <td class="td1035" style="width: 140px">
                &nbsp;</td>
            <td class="td1036" style="width: 128px">
                &nbsp;</td>
        </tr>
        <tr class="tr10">
            <td class="td436" style="width: 45px">
                &nbsp;</td>
            <td class="td1030" style="width: 275px">
                reporting in respect of the majority</td>
            <td class="td1031" style="width: 180px">
                &nbsp;</td>
            <td class="td1034" style="width: 154px">
                &nbsp;</td>
            <td class="td1035" style="width: 140px">
                &nbsp;</td>
            <td class="td1036" style="width: 128px">
                &nbsp;</td>
        </tr>
        <tr class="tr10">
            <td class="td436" style="width: 45px">
                &nbsp;</td>
            <td class="td1030" style="width: 275px">
                (greater than 50 per cent) of its CRC energy supplies?</td>
            <td class="td1031" style="width: 180px; text-align: center;">
                &nbsp;<select id="Select6" style="width: 71px">
                    <option selected="selected" value="Yes">Yes</option>
                    <option value="No">No</option>
                    <option value="N/A">N/A</option>
                </select></td>
            <td class="td1034" style="width: 154px">
                &nbsp;</td>
            <td class="td1035" style="width: 140px">
                &nbsp;</td>
            <td class="td1036" style="width: 128px; text-align: center;">
                &nbsp;<input id="Text22" style="width: 90px" type="text" /></td>
        </tr>
        <tr class="tr15">
            <td class="td455" style="width: 45px">
                &nbsp;</td>
            <td class="td1037" style="width: 275px">
                <font class="ft82">&nbsp;</font></td>
            <td class="td1085" style="width: 180px">
                &nbsp;</td>
            <td class="td1086" style="width: 154px">
                &nbsp;</td>
            <td class="td1087" style="width: 140px">
                &nbsp;</td>
            <td class="td1088" style="width: 128px">
                &nbsp;</td>
        </tr>
        <tr class="tr2">
            <td class="td436" style="width: 45px">
            </td>
            <td class="td1030" style="width: 275px">
            </td>
            <td class="td1031" style="width: 180px">
            </td>
            <td class="td1034" style="width: 154px">
            </td>
            <td class="td1035" style="width: 140px">
            </td>
            <td class="td1036" style="width: 128px">
            </td>
        </tr>
        <tr class="tr2">
            <td class="td436" style="width: 45px">
                &nbsp;</td>
            <td class="td1030" style="width: 275px">
                Have you included evidence that</td>
            <td class="td1031" style="width: 180px">
                &nbsp;</td>
            <td class="td1034" style="width: 154px">
                &nbsp;</td>
            <td class="td1035" style="width: 140px">
                &nbsp;</td>
            <td class="td1036" style="width: 128px">
                &nbsp;</td>
        </tr>
        <tr class="tr10">
            <td class="td436" style="width: 45px">
                &nbsp;</td>
            <td class="td1030" style="width: 275px">
                your CRC organisation names a</td>
            <td class="td1031" style="width: 180px">
                &nbsp;</td>
            <td class="td1034" style="width: 154px">
                &nbsp;</td>
            <td class="td1035" style="width: 140px">
                &nbsp;</td>
            <td class="td1036" style="width: 128px">
                &nbsp;</td>
        </tr>
        <tr class="tr10">
            <td class="td436" style="width: 45px">
                &nbsp;</td>
            <td class="td1030" style="width: 275px">
                director or person with managerial</td>
            <td class="td1031" style="width: 180px">
                &nbsp;</td>
            <td class="td1034" style="width: 154px">
                &nbsp;</td>
            <td class="td1035" style="width: 140px">
                &nbsp;</td>
            <td class="td1036" style="width: 128px">
                &nbsp;</td>
        </tr>
        <tr class="tr10">
            <td class="td436" style="width: 45px">
                &nbsp;</td>
            <td class="td1030" style="width: 275px">
                responsibility with responsibility</td>
            <td class="td1031" style="width: 180px; text-align: center;">
                &nbsp;</td>
            <td class="td1034" style="width: 154px">
                &nbsp;</td>
            <td class="td1035" style="width: 140px">
                &nbsp;</td>
            <td class="td1036" style="width: 128px">
                &nbsp;</td>
        </tr>
        <tr class="tr10">
            <td class="td436" style="width: 45px">
                &nbsp;</td>
            <td class="td1030" style="width: 275px">
                for overseeing carbon</td>
            <td class="td1031" style="width: 180px">
                &nbsp;</td>
            <td class="td1034" style="width: 154px">
                &nbsp;</td>
            <td class="td1035" style="width: 140px">
                &nbsp;</td>
            <td class="td1036" style="width: 128px">
                &nbsp;</td>
        </tr>
        <tr class="tr10">
            <td class="td436" style="width: 45px">
                &nbsp;</td>
            <td class="td1030" style="width: 275px">
                performance, in respect of the</td>
            <td class="td1031" style="width: 180px">
                &nbsp;</td>
            <td class="td1034" style="width: 154px">
                &nbsp;</td>
            <td class="td1035" style="width: 140px">
                &nbsp;</td>
            <td class="td1036" style="width: 128px">
                &nbsp;</td>
        </tr>
        <tr class="tr10">
            <td class="td436" style="width: 45px">
                &nbsp;</td>
            <td class="td1030" style="width: 275px">
                majority of its CRC energy supplies, in its annual reporting?</td>
            <td class="td1031" style="width: 180px; text-align: center;">
                &nbsp;<select id="Select7" style="width: 71px">
                    <option selected="selected" value="Yes">Yes</option>
                    <option value="No">No</option>
                    <option value="N/A">N/A</option>
                </select></td>
            <td class="td1034" style="width: 154px">
                &nbsp;</td>
            <td class="td1035" style="width: 140px">
                &nbsp;</td>
            <td class="td1036" style="width: 128px; text-align: center;">
                &nbsp;<input id="Text23" style="width: 90px" type="text" /></td>
        </tr>
        <tr class="tr6">
            <td class="td1089" style="width: 45px">
                &nbsp;</td>
            <td class="td1037" style="width: 275px">
                <font class="ft88">&nbsp;</font></td>
            <td class="td1090" style="width: 180px">
                &nbsp;</td>
            <td class="td1091" style="width: 154px">
                &nbsp;</td>
            <td class="td1092" style="width: 140px">
                &nbsp;</td>
            <td class="td1093" style="width: 128px">
                &nbsp;</td>
        </tr>
        <tr class="tr2">
            <td class="td436" style="width: 45px">
            </td>
            <td class="td1030" style="width: 275px">
            </td>
            <td class="td1031" style="width: 180px">
            </td>
            <td class="td1034" style="width: 154px">
            </td>
            <td class="td1035" style="width: 140px">
            </td>
            <td class="td1036" style="width: 128px">
            </td>
        </tr>
        <tr class="tr2">
            <td class="td436" style="width: 45px">
                &nbsp;</td>
            <td class="td1030" style="width: 275px">
                Have you included evidence that</td>
            <td class="td1031" style="width: 180px">
                &nbsp;</td>
            <td class="td1034" style="width: 154px">
                &nbsp;</td>
            <td class="td1035" style="width: 140px">
                &nbsp;</td>
            <td class="td1036" style="width: 128px">
                &nbsp;</td>
        </tr>
        <tr class="tr10">
            <td class="td436" style="width: 45px">
                &nbsp;</td>
            <td class="td1030" style="width: 275px">
                your organisation actively</td>
            <td class="td1031" style="width: 180px; text-align: center;">
                &nbsp;</td>
            <td class="td1034" style="width: 154px">
                &nbsp;</td>
            <td class="td1035" style="width: 140px">
                &nbsp;</td>
            <td class="td1036" style="width: 128px">
                &nbsp;</td>
        </tr>
        <tr class="tr10">
            <td class="td436" style="width: 45px">
                &nbsp;</td>
            <td class="td1030" style="width: 275px">
                engages employees to reduce energy supplies?</td>
            <td class="td1031" style="width: 180px; text-align: center;">
                &nbsp;<select id="Select8" style="width: 71px">
                    <option selected="selected" value="Yes">Yes</option>
                    <option value="No">No</option>
                    <option value="N/A">N/A</option>
                </select></td>
            <td class="td1034" style="width: 154px">
                &nbsp;</td>
            <td class="td1035" style="width: 140px">
                &nbsp;</td>
            <td class="td1036" style="width: 128px; text-align: center;">
                &nbsp;<input id="Text24" style="width: 90px" type="text" /></td>
        </tr>
        <tr class="tr19">
            <td class="td441" style="width: 45px">
                &nbsp;</td>
            <td class="td1037" style="width: 275px">
                <font class="ft61">&nbsp;</font></td>
            <td class="td1038" style="width: 180px">
                &nbsp;</td>
            <td class="td1039" style="width: 154px">
                &nbsp;</td>
            <td class="td1040" style="width: 140px">
                &nbsp;</td>
            <td class="td970" style="width: 128px">
                &nbsp;</td>
        </tr>
    </table>
    <br />
    <table cellpadding="0" cellspacing="0" class="t14" style="border-top-style: solid;
        border-right-style: solid; border-left-style: solid; border-bottom-style: solid">
        <tr class="tr11">
            <td class="td815" style="width: 44px; text-align: center">
<input id="Button2" style="width: 80px" title="buton" type="button" value="Complete" onclick="javascript:icomplete('7.5');" /><br />              
                7.5</td>
            <td class="td816" style="width: 277px">
                Annual reports in phase</td>
            <td class="td713" style="width: 179px">
                </td>
            <td class="td817" style="width: 154px">
                &nbsp;</td>
            <td class="td714" style="width: 138px">
                &nbsp;</td>
            <td class="td539" style="width: 131px">
                </td>
        </tr>
        <tr class="tr22">
            <td class="td715" style="width: 44px">
                &nbsp;</td>
            <td class="td818" style="width: 277px">
                &nbsp;</td>
            <td class="td717" style="width: 179px">
                <font class="ft56">&nbsp;</font></td>
            <td class="td819" style="width: 154px">
                <font class="ft56"></font></td>
            <td class="td718" style="width: 138px">
                <font class="ft56"></font></td>
            <td class="td545" style="width: 131px">
                <font class="ft56"></font></td>
        </tr>
        <tr class="tr2">
            <td class="td723" style="width: 44px">
                &nbsp;</td>
            <td class="td820" style="width: 277px">
                <font class="ft85">Annual report year 1 �</font></td>
            <td class="td734" style="width: 179px">
                <font class="ft55"><span style="font-family: Wingdings"></span></font>
            </td>
            <td class="td821" style="width: 154px; font-family: Wingdings">
                &nbsp;</td>
            <td class="td726" style="width: 138px">
                &nbsp;</td>
            <td class="td822" style="width: 131px">
                &nbsp;</td>
        </tr>
        <tr class="tr19">
            <td class="td727" style="width: 44px">
                &nbsp;</td>
            <td class="td823" style="width: 277px">
                <font class="ft61">summary, archived data</font></td>
            <td class="td728" style="width: 179px; text-align: center;">
                &nbsp;<select id="Select3" style="width: 98px">
                    <option selected="selected" value="Confirmed">Confirmed</option>
                    <option value="Unconfirmed">Unconfirmed</option>
                </select></td>
            <td class="td824" style="width: 154px">
                &nbsp;</td>
            <td class="td729" style="width: 138px">
                &nbsp;</td>
            <td class="td825" style="width: 131px; text-align: center;">
                &nbsp;<input id="Text25" style="width: 90px" type="text" /></td>
        </tr>
        <tr class="tr3">
            <td class="td723" style="width: 44px">
                &nbsp;</td>
            <td class="td820" style="width: 277px">
                <font class="ft85">Annual report year 2 �</font></td>
            <td class="td725" style="width: 179px">
                &nbsp;</td>
            <td class="td821" style="width: 154px">
                &nbsp;</td>
            <td class="td726" style="width: 138px">
                &nbsp;</td>
            <td class="td822" style="width: 131px">
                &nbsp;</td>
        </tr>
        <tr class="tr19">
            <td class="td727" style="width: 44px">
                &nbsp;</td>
            <td class="td823" style="width: 277px">
                <font class="ft61">summary, archived data</font></td>
            <td class="td728" style="width: 179px; text-align: center;">
                &nbsp;<select id="Select4" style="width: 98px">
                    <option selected="selected" value="Confirmed">Confirmed</option>
                    <option value="Unconfirmed">Unconfirmed</option>
                </select></td>
            <td class="td824" style="width: 154px">
                &nbsp;</td>
            <td class="td729" style="width: 138px">
                &nbsp;</td>
            <td class="td825" style="width: 131px; text-align: center;">
                &nbsp;<input id="Text26" style="width: 90px" type="text" /></td>
        </tr>
        <tr class="tr13">
            <td class="td730" style="width: 44px">
                &nbsp;</td>
            <td class="td1094" style="width: 277px">
                &nbsp;</td>
            <td class="td731" style="width: 179px">
                &nbsp;</td>
            <td class="td1095" style="width: 154px">
                &nbsp;</td>
            <td class="td732" style="width: 138px">
                &nbsp;</td>
            <td class="td551" style="width: 131px">
                &nbsp;</td>
        </tr>
    </table>
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />

</body>
</html>
