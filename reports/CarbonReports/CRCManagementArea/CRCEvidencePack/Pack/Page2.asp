<%
Public dComm  
Public rComm  


Public Function OpenConnection()
	  Set dComm = CreateObject("ADODB.Connection")
	      dComm.Open "Provider=SQLOLEDB.1;Data Source=uk-ed0-sqlcl-01.MCEG.local;Initial Catalog=UML_CMS;Integrated Security=SSPI;"
End Function

Public Function OpenRecordset(nSQL)
 Set rComm = CreateObject("ADODB.recordset")
     rComm.ActiveConnection = dComm
     rComm.CursorLocation = 3 ' 2
     rComm.CursorType = 3 '1
     rComm.LockType = 1 '3
     rComm.Source = nSQL
     rComm.Open
End Function

Public Function CloseConnection()
  Set dComm = Nothing
  Set rComm = Nothing
End Function

strsql = "SELECT * FROM dbo.tblCRC_CCA_Page21 WHERE virtualID = " & session("VirtualID")
strsql = strsql & " AND TagID='2.1'"

Call OpenConnection
Call OpenRecordset()

Response.write rcomm("F1_Date") & "<BR>"


Call CloseConnection

%>


<form name="myform"  action="readxml.asp" method="post"> 

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <title>Untitled Page</title>
<script language="javascript" type="text/javascript">
// <!CDATA[

function showme()
{
    var StringConvert = document.getElementById("myform").outerHTML
    document.getElementById("textsubmit").innerText = StringConvert ;
    document.myform.submit();
}

function icomplete(what)
{
     alert(what);
}
 

// ]]>
</script>
</head>
<body>
    <br />
    <br />
 
    <table cellpadding="0" cellspacing="0" class="t7" style="border-top-style: solid;
        border-right-style: solid; border-left-style: solid; border-bottom-style: solid">
        <tr>
            <td class="td288" style="width: 55px; height: 38px; text-align: center">
<input id="Button2" style="width: 80px" title="buton" type="button" value="Complete" onclick="javascript:icomplete('2.1');" /><br />
                <font class="ft54">2.1<br />
                    </font></td>
            <td class="td289" colspan="2" style="height: 38px">
                <font class="ft54">Qualification (2008)</font></td>
            <td class="td290" style="width: 167px; height: 38px; text-align: center">
                <font class="ft54">Information</font></td>
            <td class="td291" style="width: 159px; height: 38px; text-align: center">
                <font class="ft54">Document<br />
                    reference</font></td>
            <td class="td292" style="width: 120px; height: 38px; text-align: center">
                <font class="ft54">Authorised signature</font></td>
            <td class="td293" style="width: 130px; height: 38px; text-align: center">
                <font class="ft54">Completion<br />
                    date</font></td>
        </tr>
        <tr>
            <td class="td301" style="width: 55px; height: 19px">
                &nbsp;</td>
            <td class="td289" colspan="2" style="height: 19px">
                Qualified as a full participant</td>
            <td class="td290" style="width: 167px; height: 19px; text-align: center">
                <select id="Select1" style="width: 51px">
                    <option selected="selected" value="Yes">Yes</option>
                    <option value="No">No</option>
                </select>
            </td>
            <td class="td291" style="width: 159px; height: 19px"><input id="Text13" style="width: 89px" type="text" /></td>
            <td class="td292" style="width: 120px; height: 19px">
                <font class="ft67">&nbsp;<input id="Text8" style="width: 89px" type="text" /></font></td>
            <td class="td293" style="width: 130px; height: 19px; text-align: center">
                <input id="Text3" style="width: 89px" type="text" /></td>
        </tr>
        <tr>
            <td class="td301" style="width: 55px">
                &nbsp;</td>
            <td class="td289" colspan="2">
                Qualifies for exemption based<br />
                on CCA emissions coverage</td>
            <td class="td290" style="width: 167px; text-align: center">
                <font class="ft67">&nbsp;<select id="Select3" style="width: 53px">
                    <option selected="selected" value="Yes">Yes</option>
                    <option value="No">No</option>
                </select></font></td>
            <td class="td311" style="width: 159px">
                &nbsp;<input id="Text14" style="width: 89px" type="text" /></td>
            <td class="td304" style="width: 120px">
                &nbsp;<input id="Text9" style="width: 89px" type="text" /></td>
            <td class="td312" style="width: 130px; text-align: center">
                &nbsp;<input id="Text4" style="width: 89px" type="text" /></td>
        </tr>
        <tr>
            <td class="td301" style="width: 55px; height: 19px">
                &nbsp;</td>
            <td class="td289" colspan="2" style="height: 19px">
                Required to make an information disclosure only</td>
            <td class="td290" style="width: 167px; height: 19px; text-align: center">
                <select id="Select2" style="width: 53px">
                    <option selected="selected" value="Yes">Yes</option>
                    <option value="No">No</option>
                </select>
            </td>
            <td class="td311" style="width: 159px; height: 19px">
                &nbsp;<input id="Text15" style="width: 89px" type="text" /></td>
            <td class="td304" style="width: 120px; height: 19px">
                &nbsp;<input id="Text10" style="width: 89px" type="text" /></td>
            <td class="td312" style="width: 130px; height: 19px; text-align: center">
                &nbsp;<input id="Text5" style="width: 89px" type="text" /></td>
        </tr>
        <tr>
            <td class="td294" style="width: 55px">
                &nbsp;</td>
            <td class="td320" colspan="2">
                <font class="ft58">&nbsp;</font></td>
            <td class="td297" style="width: 167px">
                &nbsp;</td>
            <td class="td321" style="width: 159px">
                &nbsp;</td>
            <td class="td322" style="width: 120px">
                &nbsp;</td>
            <td class="td323" style="width: 130px">
                &nbsp;</td>
        </tr>
        <tr>
            <td class="td301" style="width: 55px">
                &nbsp;</td>
            <td class="td289" colspan="2">
                Settled HHM electricity supply</td>
            <td class="td290" style="width: 167px; text-align: center">
                <input id="Text1" style="width: 89px" type="text" /></td>
            <td class="td311" style="width: 159px">
                &nbsp;<input id="Text16" style="width: 89px" type="text" /></td>
            <td class="td304" style="width: 120px">
                &nbsp;<input id="Text11" style="width: 89px" type="text" /></td>
            <td class="td312" style="width: 130px; text-align: center">
                &nbsp;<input id="Text6" style="width: 89px" type="text" /></td>
        </tr>
        <tr>
            <td class="td294" style="width: 55px">
                &nbsp;</td>
            <td class="td295" style="width: 91px">
                &nbsp;</td>
            <td class="td296" style="width: 199px">
                &nbsp;</td>
            <td class="td316" style="width: 167px">
                <font class="ft70">&nbsp;</font></td>
            <td class="td321" style="width: 159px">
                &nbsp;</td>
            <td class="td322" style="width: 120px">
                &nbsp;</td>
            <td class="td323" style="width: 130px">
                &nbsp;</td>
        </tr>
        <tr>
            <td class="td301" style="width: 55px">
                &nbsp;</td>
            <td class="td289" colspan="2">
                HHM electricity supplied from
                <br />
                non-settlement meters</td>
            <td class="td290" style="width: 167px; text-align: center">
                <input id="Text2" style="width: 92px" type="text" /></td>
            <td class="td311" style="width: 159px">
                &nbsp;<input id="Text17" style="width: 89px" type="text" /></td>
            <td class="td304" style="width: 120px">
                &nbsp;<input id="Text12" style="width: 89px" type="text" /></td>
            <td class="td312" style="width: 130px; text-align: center">
                &nbsp;<input id="Text7" style="width: 89px" type="text" /></td>
        </tr>
        <tr>
            <td class="td294" style="width: 55px">
                &nbsp;</td>
            <td class="td320" colspan="2">
                <font class="ft58">&nbsp;</font></td>
            <td class="td316" style="width: 167px">
                <font class="ft70">&nbsp;</font></td>
            <td class="td321" style="width: 159px">
                &nbsp;</td>
            <td class="td322" style="width: 120px">
                &nbsp;</td>
            <td class="td323" style="width: 130px">
                &nbsp;</td>
        </tr>
    </table>
 
    <table cellpadding="0" cellspacing="0" class="t7" style="border-top-style: solid;
        border-right-style: solid; border-left-style: solid; height: 100px; border-bottom-style: solid">
        <tr>
            <td class="td276" style="width: 55px; height: 4px; text-align: center">
<input id="Button3" style="width: 80px" title="buton" type="button" value="Complete" onclick="javascript:icomplete('2.2');" /><br />
                <font class="ft65">2.2<br />
                    </font></td>
            <td class="td324" style="width: 218px; height: 4px">
                <font class="ft65">Registration</font></td>
            <td class="td282" style="width: 142px; height: 4px">
                &nbsp;</td>
            <td class="td207" style="width: 181px; height: 4px">
                &nbsp;</td>
            <td class="td278" style="width: 130px; height: 4px">
                &nbsp;</td>
            <td class="td279" style="width: 143px; height: 4px">
                &nbsp;</td>
        </tr>
        <tr>
            <td class="td289" colspan="2" style="height: 1px">
            </td>
            <td class="td290" style="width: 142px; height: 1px; text-align: center">
                <font class="ft54">Information</font></td>
            <td class="td291" style="width: 181px; height: 1px; text-align: center">
                <font class="ft54">Document<br />
                    reference</font></td>
            <td class="td292" style="width: 130px; height: 1px; text-align: center">
                <font class="ft54">Authorised<br />
                    signature</font></td>
            <td class="td293" style="width: 143px; height: 1px; text-align: center">
                <font class="ft54">Completion<br />
                    date</font></td>
        </tr>
        <tr>
            <td class="td289" colspan="2" style="height: 22px">
                Registration summary print-out
                <br />
                from CRC Registry included?</td>
            <td class="td290" style="width: 142px; height: 22px; text-align: center">
                <font class="ft67">&nbsp;<select id="Select4" style="width: 61px">
                    <option selected="selected" value="Yes">Yes</option>
                    <option value="No">No</option>
                </select></font></td>
            <td class="td291" style="width: 181px; height: 22px; text-align: center">
                <input id="Text18" style="width: 131px" type="text" /></td>
            <td class="td292" style="width: 130px; height: 22px; text-align: center">
                <input id="Text19" style="width: 110px" type="text" /></td>
            <td class="td293" style="width: 143px; height: 22px; text-align: center">
                <input id="Text20" style="width: 100px" type="text" /></td>
        </tr>
    </table>
 
 
    <table cellpadding="0" cellspacing="0" class="t7" style="border-top-style: solid;
        border-right-style: solid; border-left-style: solid; border-bottom-style: solid">
        <tr class="tr11">
            <td class="td325" style="width: 56px; height: 19px; text-align: center">
 <input id="Button4" style="width: 80px" title="buton" type="button" value="Complete" onclick="javascript:icomplete('2.3');" /><br />            
                <font class="ft54"><strong>2.3<br />
                    </strong></font></td>
            <td class="td326" style="font-weight: bold; width: 216px; height: 19px; text-align: center">
                <font class="ft54">Summary � footprint report</font></td>
            <td class="td327" colspan="2" style="font-weight: bold; height: 19px; text-align: center">
                <font class="ft54">Supply and emissions (kWh and t<font class="ft10">CO</font><font
                    class="ft71"><span style="font-size: 8pt">2</span></font><font class="ft56">)</font></font></td>
            <td class="td328" style="font-weight: bold; width: 114px; height: 19px; text-align: center">
                <font class="ft54">Document reference</font></td>
            <td class="td329" style="font-weight: bold; width: 164px; height: 19px; text-align: center">
                <font class="ft54">Authorised<br />
                    signature</font></td>
            <td class="td330" style="font-weight: bold; width: 241px; height: 19px; text-align: center">
                <font class="ft54">Completion<br />
                    date</font></td>
        </tr>
        <tr class="tr2">
            <td class="td331" style="width: 56px">
                &nbsp;</td>
            <td class="td332" style="width: 216px">
                Total core supply of electricity</td>
            <td class="td342" style="width: 222px">
                <input id="Text21" type="text" /></td>
            <td class="td343" style="width: 234px; font-style: italic">
                <input id="Text22" style="width: 154px" type="text" /></td>
            <td class="td344" style="font-size: 6pt; width: 114px; font-style: italic">
                &nbsp;<input id="Text33" style="width: 89px" type="text" /></td>
            <td class="td345" style="width: 164px">
                &nbsp;<input id="Text76" style="width: 89px" type="text" /></td>
            <td class="td346" style="width: 241px; text-align: center">
                &nbsp;<input id="Text23" style="width: 107px" type="text" /></td>
        </tr>
        <tr class="tr22">
            <td class="td347" style="width: 56px">
                &nbsp;</td>
            <td class="td348" style="width: 216px">
                <font class="ft58">&nbsp;</font></td>
            <td class="td349" style="width: 222px">
                &nbsp;</td>
            <td class="td350" style="width: 234px">
                &nbsp;</td>
            <td class="td351" style="width: 114px">
                &nbsp;</td>
            <td class="td352" style="width: 164px">
                &nbsp;</td>
            <td class="td353" style="width: 241px">
                &nbsp;</td>
        </tr>
        <tr class="tr2">
            <td class="td331" style="width: 56px">
                &nbsp;</td>
            <td class="td332" style="width: 216px">
                Total of any electricity generating credits (EGCs)</td>
            <td class="td342" style="width: 222px">
                <input id="Text24" type="text" /></td>
            <td class="td343" style="width: 234px; font-style: italic">
                <input id="Text25" style="width: 154px" type="text" /></td>
            <td class="td344" style="font-size: 6pt; width: 114px; font-style: italic">
                &nbsp;<input id="Text34" style="width: 89px" type="text" /></td>
            <td class="td345" style="width: 164px">
                &nbsp;<input id="Text77" style="width: 89px" type="text" /></td>
            <td class="td346" style="width: 241px; text-align: center">
                &nbsp;<input id="Text26" style="width: 107px" type="text" /></td>
        </tr>
        <tr class="tr19">
            <td class="td354" style="width: 56px; height: 19px;">
                &nbsp;</td>
            <td class="td348" style="width: 216px; height: 19px;">
                <font class="ft61">&nbsp;</font></td>
            <td class="td355" style="width: 222px; height: 19px;">
                &nbsp;</td>
            <td class="td356" style="width: 234px; height: 19px;">
                &nbsp;</td>
            <td class="td357" style="width: 114px; height: 19px;">
                &nbsp;</td>
            <td class="td358" style="width: 164px; height: 19px;">
                &nbsp;</td>
            <td class="td359" style="width: 241px; height: 19px;">
                &nbsp;</td>
        </tr>
        <tr class="tr13">
            <td class="td360" style="width: 56px">
                &nbsp;</td>
            <td class="td348" style="width: 216px">
                <font class="ft59">Total core supply of gas</font></td>
            <td class="td361" style="width: 222px">
                <input id="Text27" type="text" /></td>
            <td class="td362" style="width: 234px; font-style: italic">
                <input id="Text28" style="width: 159px" type="text" /></td>
            <td class="td363" style="font-size: 6pt; width: 114px; font-style: italic">
                &nbsp;<input id="Text35" style="width: 89px" type="text" /></td>
            <td class="td364" style="width: 164px">
                &nbsp;<input id="Text78" style="width: 89px" type="text" /></td>
            <td class="td365" style="width: 241px; text-align: center">
                &nbsp;<input id="Text29" style="width: 107px" type="text" /></td>
        </tr>
        <tr class="tr2">
            <td class="td331" style="width: 56px; height: 19px">
                &nbsp;</td>
            <td class="td332" style="width: 216px; height: 19px">
                Total other energy supply<br />
                (please specify type(s))<br />
                with a new row per fuel<br />
                type</td>
            <td class="td342" style="width: 222px; height: 19px">
                <font class="ft67"><em>
                    <input id="Text30" type="text" /></em></font></td>
            <td class="td343" style="width: 234px; font-style: italic; height: 19px">
                <input id="Text31" style="width: 157px" type="text" /></td>
            <td class="td344" style="font-size: 6pt; width: 114px; font-style: italic; height: 19px">
                &nbsp;<input id="Text36" style="width: 89px" type="text" /></td>
            <td class="td345" style="width: 164px; height: 19px">
                &nbsp;<input id="Text79" style="width: 89px" type="text" /></td>
            <td class="td346" style="width: 241px; height: 19px; text-align: center">
                &nbsp;<input id="Text32" style="width: 107px" type="text" /></td>
        </tr>
        <tr class="tr10">
            <td class="td331" style="width: 56px">
                &nbsp;</td>
            <td class="td332" style="width: 216px">
                &nbsp;<input id="Button1" style="width: 70px" type="button" value="New run" /></td>
            <td class="td367" style="width: 222px">
                &nbsp;</td>
            <td class="td366" style="width: 234px">
                &nbsp;</td>
            <td class="td344" style="width: 114px">
                &nbsp;</td>
            <td class="td345" style="width: 164px">
                &nbsp;</td>
            <td class="td346" style="width: 241px">
                &nbsp;</td>
        </tr>
        <tr class="tr18">
            <td class="td368" style="width: 56px">
                &nbsp;</td>
            <td class="td348" style="width: 216px">
                <font class="ft73"></font>
            </td>
            <td class="td369" style="width: 222px">
                &nbsp;</td>
            <td class="td370" style="width: 234px">
                &nbsp;</td>
            <td class="td371" style="width: 114px">
                &nbsp;</td>
            <td class="td372" style="width: 164px">
                &nbsp;</td>
            <td class="td373" style="width: 241px">
                &nbsp;</td>
        </tr>
    </table>
                    
    <table cellpadding="0" cellspacing="0" class="t8" style="border-top-style: solid;
        border-right-style: solid; border-left-style: solid; border-bottom-style: solid">
        <tr class="tr11">
            <td class="td374" style="width: 55px; height: 57px; text-align: center">
 <input id="Button5" style="width: 80px" title="buton" type="button" value="Complete" onclick="javascript:icomplete('2.4');" /><br />            
                <font class="ft54"><strong>2.4<br />
                    </strong></font></td>
            <td class="td375" style="font-weight: bold; width: 190px; height: 57px">
                <font class="ft54">Summary � annual report</font></td>
            <td class="td376" style="font-weight: bold; width: 154px; height: 57px">
                Current year end [insert] (tCO<font class="ft76"><span style="font-size: 6pt">2</span></font><font
                    class="ft54">)</font></td>
            <td class="td377" style="width: 163px; height: 57px">
                Previous year end [insert] (tCO<font class="ft78"><span style="font-size: 6pt">2</span></font><font
                    class="ft77">)</font></td>
            <td class="td376" style="width: 118px; height: 57px">
                Estimated?<br />
                (Y/N)</td>
            <td class="td377" style="width: 158px; height: 57px">
                <font class="ft54"><strong>Document<br />
                    reference</strong></font></td>
            <td class="td329" style="font-weight: bold; width: 135px; height: 57px">
                <font class="ft54">Authorised signature</font></td>
            <td class="td378" style="font-weight: bold; width: 125px; height: 57px">
                <font class="ft54">Completion<br />
                    date</font></td>
        </tr>
        <tr class="tr13">
            <td class="td392" style="width: 55px">
                &nbsp;</td>
            <td class="td393" style="width: 190px">
                <font class="ft59">Electricity</font></td>
            <td class="td394" style="width: 154px; text-align: center">
                &nbsp;<input id="Text37" style="width: 122px" type="text" /></td>
            <td class="td395" style="width: 163px; text-align: center">
                &nbsp;<input id="Text38" style="width: 122px" type="text" /></td>
            <td class="td394" style="width: 118px">
                &nbsp;<select id="Select5" style="width: 55px">
                    <option selected="selected" value="Yes">Yes</option>
                    <option value="No">No</option>
                </select></td>
            <td class="td395" style="width: 158px">
                &nbsp;<input id="Text80" style="width: 89px" type="text" /></td>
            <td class="td364" style="width: 135px">
                &nbsp;<input id="Text92" style="width: 89px" type="text" /></td>
            <td class="td396" style="width: 125px; text-align: center">
                &nbsp;<input id="Text39" style="width: 91px" type="text" /></td>
        </tr>
        <tr class="tr10">
            <td class="td397" style="width: 55px">
                &nbsp;</td>
            <td class="td393" style="width: 190px">
                <font class="ft59">EGCs</font></td>
            <td class="td398" style="width: 154px; text-align: center">
                &nbsp;<input id="Text40" style="width: 122px" type="text" /></td>
            <td class="td399" style="width: 163px">
                &nbsp;</td>
            <td class="td398" style="width: 118px">
                &nbsp;<select id="Select6" style="width: 55px">
                    <option selected="selected" value="Yes">Yes</option>
                    <option value="No">No</option>
                </select></td>
            <td class="td399" style="width: 158px">
                &nbsp;<input id="Text81" style="width: 89px" type="text" /></td>
            <td class="td400" style="width: 135px">
                &nbsp;<input id="Text98" style="width: 89px" type="text" /></td>
            <td class="td401" style="width: 125px; text-align: center">
                &nbsp;<input id="Text41" style="width: 91px" type="text" /></td>
        </tr>
        <tr class="tr2">
            <td class="td379" style="width: 55px">
                &nbsp;</td>
            <td class="td380" style="width: 190px">
                ROC/FIT</td>
            <td class="td385" style="width: 154px; text-align: center">
                &nbsp;<input id="Text42" style="width: 122px" type="text" /></td>
            <td class="td386" style="width: 163px">
                &nbsp;</td>
            <td class="td385" style="width: 118px">
                &nbsp;<select id="Select7" style="width: 55px">
                    <option selected="selected" value="Yes">Yes</option>
                    <option value="No">No</option>
                </select></td>
            <td class="td386" style="width: 158px">
                &nbsp;<input id="Text82" style="width: 89px" type="text" /></td>
            <td class="td345" style="width: 135px">
                &nbsp;<input id="Text99" style="width: 89px" type="text" /></td>
            <td class="td384" style="width: 125px; text-align: center">
                &nbsp;<input id="Text43" style="width: 91px" type="text" /></td>
        </tr>
        <tr class="tr22">
            <td class="td402" style="width: 55px">
                &nbsp;</td>
            <td class="td393" style="width: 190px">
                <font class="ft58">output</font></td>
            <td class="td403" style="width: 154px; text-align: center">
                &nbsp;<input id="Text44" style="width: 122px" type="text" /></td>
            <td class="td404" style="width: 163px">
                &nbsp;</td>
            <td class="td403" style="width: 118px">
                &nbsp;<select id="Select8" style="width: 55px">
                    <option selected="selected" value="Yes">Yes</option>
                    <option value="No">No</option>
                </select></td>
            <td class="td404" style="width: 158px">
                &nbsp;<input id="Text83" style="width: 89px" type="text" /></td>
            <td class="td352" style="width: 135px">
                &nbsp;<input id="Text100" style="width: 89px" type="text" /></td>
            <td class="td405" style="width: 125px; text-align: center">
                &nbsp;<input id="Text45" style="width: 91px" type="text" /></td>
        </tr>
        <tr class="tr10">
            <td class="td397" style="width: 55px">
                &nbsp;</td>
            <td class="td393" style="width: 190px">
                <font class="ft59">Natural gas</font></td>
            <td class="td398" style="width: 154px; text-align: center">
                &nbsp;<input id="Text46" style="width: 122px" type="text" /></td>
            <td class="td399" style="width: 163px">
                &nbsp;</td>
            <td class="td398" style="width: 118px">
                &nbsp;<select id="Select9" style="width: 55px">
                    <option selected="selected" value="Yes">Yes</option>
                    <option value="No">No</option>
                </select></td>
            <td class="td399" style="width: 158px">
                &nbsp;<input id="Text84" style="width: 89px" type="text" /></td>
            <td class="td400" style="width: 135px">
                &nbsp;<input id="Text101" style="width: 89px" type="text" /></td>
            <td class="td401" style="width: 125px; text-align: center">
                &nbsp;<input id="Text47" style="width: 91px" type="text" /></td>
        </tr>
        <tr class="tr10">
            <td class="td397" style="width: 55px">
                &nbsp;</td>
            <td class="td393" style="width: 190px">
                <font class="ft58">Fuel 1</font></td>
            <td class="td398" style="width: 154px; text-align: center">
                &nbsp;<input id="Text48" style="width: 122px" type="text" /></td>
            <td class="td399" style="width: 163px">
                &nbsp;</td>
            <td class="td398" style="width: 118px">
                &nbsp;<select id="Select10" style="width: 55px">
                    <option selected="selected" value="Yes">Yes</option>
                    <option value="No">No</option>
                </select></td>
            <td class="td399" style="width: 158px">
                &nbsp;<input id="Text85" style="width: 89px" type="text" /></td>
            <td class="td400" style="width: 135px">
                &nbsp;<input id="Text102" style="width: 89px" type="text" /></td>
            <td class="td401" style="width: 125px; text-align: center">
                &nbsp;<input id="Text49" style="width: 91px" type="text" /></td>
        </tr>
        <tr class="tr13">
            <td class="td392" style="width: 55px; height: 14px">
                &nbsp;</td>
            <td class="td393" style="width: 190px; height: 14px">
                <font class="ft59">Fuel 2</font></td>
            <td class="td394" style="width: 154px; height: 14px; text-align: center">
                &nbsp;<input id="Text50" style="width: 122px" type="text" /></td>
            <td class="td395" style="width: 163px; height: 14px">
                &nbsp;</td>
            <td class="td394" style="width: 118px; height: 14px">
                &nbsp;<select id="Select11" style="width: 55px">
                    <option selected="selected" value="Yes">Yes</option>
                    <option value="No">No</option>
                </select></td>
            <td class="td395" style="width: 158px; height: 14px">
                &nbsp;<input id="Text86" style="width: 89px" type="text" /></td>
            <td class="td364" style="width: 135px; height: 14px">
                &nbsp;<input id="Text103" style="width: 89px" type="text" /></td>
            <td class="td396" style="width: 125px; height: 14px; text-align: center">
                &nbsp;<input id="Text51" style="width: 91px" type="text" /></td>
        </tr>
        <tr class="tr10">
            <td class="td397" style="width: 55px">
                &nbsp;</td>
            <td class="td393" style="width: 190px">
                <font class="ft59">Fuel 3</font></td>
            <td class="td398" style="width: 154px; text-align: center">
                &nbsp;<input id="Text52" style="width: 122px" type="text" /></td>
            <td class="td399" style="width: 163px">
                &nbsp;</td>
            <td class="td398" style="width: 118px">
                &nbsp;<select id="Select12" style="width: 55px">
                    <option selected="selected" value="Yes">Yes</option>
                    <option value="No">No</option>
                </select></td>
            <td class="td399" style="width: 158px">
                &nbsp;<input id="Text87" style="width: 89px" type="text" /></td>
            <td class="td400" style="width: 135px">
                &nbsp;<input id="Text104" style="width: 89px" type="text" /></td>
            <td class="td401" style="width: 125px; text-align: center">
                &nbsp;<input id="Text53" style="width: 91px" type="text" /></td>
        </tr>
        <tr class="tr10">
            <td class="td397" style="width: 55px">
                &nbsp;</td>
            <td class="td393" style="width: 190px">
                <font class="ft58">Fuel 4</font></td>
            <td class="td398" style="width: 154px; text-align: center">
                &nbsp;<input id="Text54" style="width: 122px" type="text" /></td>
            <td class="td399" style="width: 163px">
                &nbsp;</td>
            <td class="td398" style="width: 118px">
                &nbsp;<select id="Select13" style="width: 55px">
                    <option selected="selected" value="Yes">Yes</option>
                    <option value="No">No</option>
                </select></td>
            <td class="td399" style="width: 158px">
                &nbsp;<input id="Text116" style="width: 89px" type="text" /></td>
            <td class="td400" style="width: 135px">
                &nbsp;<input id="Text105" style="width: 89px" type="text" /></td>
            <td class="td401" style="width: 125px; text-align: center">
                &nbsp;<input id="Text55" style="width: 91px" type="text" /></td>
        </tr>
        <tr class="tr13">
            <td class="td392" style="width: 55px">
            </td>
            <td class="td393" style="width: 190px">
                &nbsp;</td>
            <td class="td394" style="font-weight: bold; width: 154px; text-align: center">
            </td>
            <td class="td395" style="width: 163px">
            </td>
            <td class="td394" style="width: 118px">
            </td>
            <td class="td395" style="width: 158px">
            </td>
            <td class="td364" style="width: 135px">
            </td>
            <td class="td396" style="width: 125px; text-align: center">
            </td>
        </tr>
        <tr class="tr13">
            <td class="td392" style="width: 55px">
                &nbsp;</td>
            <td class="td393" style="width: 190px">
                <font class="ft60"><strong>Total</strong></font></td>
            <td class="td394" style="font-weight: bold; width: 154px; text-align: center">
                &nbsp;<input id="Text56" style="width: 122px" type="text" /></td>
            <td class="td395" style="width: 163px">
                &nbsp;</td>
            <td class="td394" style="width: 118px">
                &nbsp;<select id="Select14" style="width: 55px">
                    <option selected="selected" value="Yes">Yes</option>
                    <option value="No">No</option>
                </select></td>
            <td class="td395" style="width: 158px">
                &nbsp;<input id="Text88" style="width: 89px" type="text" /></td>
            <td class="td364" style="width: 135px">
                &nbsp;<input id="Text106" style="width: 89px" type="text" /></td>
            <td class="td396" style="width: 125px; text-align: center">
                &nbsp;<input id="Text57" style="width: 91px" type="text" /></td>
        </tr>
        <tr class="tr2">
            <td class="td379" style="width: 55px">
                &nbsp;</td>
            <td class="td380" style="width: 190px">
                Significant group undertaking 1 total</td>
            <td class="td385" style="width: 154px; text-align: center">
                &nbsp;<input id="Text58" style="width: 122px" type="text" /></td>
            <td class="td386" style="width: 163px">
                &nbsp;</td>
            <td class="td385" style="width: 118px">
                &nbsp;<select id="Select15" style="width: 55px">
                    <option selected="selected" value="Yes">Yes</option>
                    <option value="No">No</option>
                </select></td>
            <td class="td386" style="width: 158px">
                &nbsp;<input id="Text89" style="width: 89px" type="text" /></td>
            <td class="td345" style="width: 135px">
                &nbsp;<input id="Text107" style="width: 89px" type="text" /></td>
            <td class="td384" style="width: 125px; text-align: center">
                &nbsp;<input id="Text59" style="width: 91px" type="text" /></td>
        </tr>
        <tr class="tr2">
            <td class="td379" style="width: 55px">
            </td>
            <td class="td380" style="width: 190px">
                &nbsp;</td>
            <td class="td385" style="width: 154px; text-align: center">
            </td>
            <td class="td386" style="width: 163px">
            </td>
            <td class="td385" style="width: 118px">
            </td>
            <td class="td386" style="width: 158px">
            </td>
            <td class="td345" style="width: 135px">
            </td>
            <td class="td384" style="width: 125px; text-align: center">
            </td>
        </tr>
        <tr class="tr2">
            <td class="td379" style="width: 55px">
                &nbsp;</td>
            <td class="td380" style="width: 190px">
                Significant group undertaking 2 total</td>
            <td class="td385" style="width: 154px; text-align: center">
                &nbsp;<input id="Text66" style="width: 122px" type="text" /></td>
            <td class="td386" style="width: 163px">
                &nbsp;</td>
            <td class="td385" style="width: 118px">
                &nbsp;<select id="Select19" style="width: 55px">
                    <option selected="selected" value="Yes">Yes</option>
                    <option value="No">No</option>
                </select></td>
            <td class="td386" style="width: 158px">
                &nbsp;<input id="Text93" style="width: 89px" type="text" /></td>
            <td class="td345" style="width: 135px">
                &nbsp;<input id="Text111" style="width: 89px" type="text" /></td>
            <td class="td384" style="width: 125px; text-align: center">
                &nbsp;<input id="Text67" style="width: 91px" type="text" /></td>
        </tr>
        <tr class="tr13">
            <td class="td392" style="width: 55px">
                &nbsp;</td>
            <td class="td393" style="width: 190px">
                <font class="ft59"></font>
            </td>
            <td class="td394" style="width: 154px; text-align: center">
                &nbsp;</td>
            <td class="td395" style="width: 163px">
                &nbsp;</td>
            <td class="td394" style="width: 118px">
                &nbsp;</td>
            <td class="td395" style="width: 158px">
                &nbsp;</td>
            <td class="td364" style="width: 135px">
                &nbsp;</td>
            <td class="td396" style="width: 125px; text-align: center">
                &nbsp;</td>
        </tr>
    </table>


 <INPUT type="button" value="Submit" id=button6 name=button1 onclick="javascript:showme();">
 <TEXTAREA style="display:none;" rows=20 cols=80 id="textsubmit" name="textsubmit">
 </TEXTAREA>

 
</body>
</html>
</form>