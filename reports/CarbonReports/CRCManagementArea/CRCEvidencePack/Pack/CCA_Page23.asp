<%
Dim TheID 
         TheID=request.QueryString("id")  

Dim TheNet
         TheNet=request.QueryString("net")  
        
Dim Current_Phase
         Current_Phase=SESSION("phase")  


Dim ReportPhase
         ReportPhase=Request.QueryString("RP")  
If ReportPhase="" then
           Current_Phase=SESSION("phase")  
Else
           Current_Phase=Request.QueryString("RP")  
End If


Dim OnOff 
         if TheNet="true" then
               vis="Display:none;" 
         else
             vis="Display:;"  
        end if


 
         
%> 

 <html>  
 <%
Public dComm  
Public rComm  
 
Public Function OpenConnection()
  Set dComm = CreateObject("ADODB.Connection")
      dComm.Open "Provider=SQLOLEDB.1;Data Source=uk-ed0-sqlcl-01.MCEG.local;Initial Catalog=UML_CMS;Integrated Security=SSPI;"
End Function

Public Function OpenRecordset(nSQL)
 Set rComm = CreateObject("ADODB.recordset")
     rComm.ActiveConnection = dComm
     rComm.CursorLocation = 3 ' 2
     rComm.CursorType = 3 '1
     rComm.LockType = 1 '3
     rComm.Source = nSQL
     rComm.Open
End Function

Public Function CloseConnection()
  Set dComm = Nothing
  Set rComm = Nothing
End Function


'=========================================================================================
strsql = " SELECT * FROM dbo.tblCRC_CCA_Page23 WHERE virtualID = " & TheID
strsql = strsql & " AND TagID='2.3'" & " AND phase=" & "'" & Current_Phase & "'"

Call OpenConnection
Call OpenRecordset(strsql)
if rComm.EOF = true then
   continue=false
    f1_date=""
    f2_date=""
    f3_date=""
    f4_date=""
    f1=""
    f2=""
    f3=""
    f4=""
    f5=""
    f6=""
    f7=""
    f8=""
   
   Response.Write "<script language=""javascript"" type=""text/javascript"">" & vbnewline
   Response.write "var VirtualID=" & TheID  & vbnewline
   Response.write "var EditID=" & "0"  & vbnewline
   Response.write "var TagId=" & "'" & "2.3" & "'" & vbnewline
   Response.Write "</script>" &  vbnewline
else
   continue=true
   f1_date=trim(rcomm("f1_date"))
   f2_date=trim(rcomm("f2_date"))
   f3_date=trim(rcomm("f3_date"))
   f4_date=trim(rcomm("f4_date"))
    f1=rcomm("f1")
    f2=rcomm("f2")
    f3=rcomm("f3")
    f4=rcomm("f4")
    f5=rcomm("f5")
    f6=rcomm("f6")
    f7=rcomm("f7")
    f8=rcomm("f8")  
   
   Response.Write "<script language=""javascript"" type=""text/javascript"">" & vbnewline
   Response.write "var VirtualID=" & TheID & vbnewline
   Response.write "var EditID=" & rComm("ID") & vbnewline
   Response.write "var TagId=" & "'" & "2.3" & "'" & vbnewline
   Response.Write "</script>" &  vbnewline
end if
 
  
'=========================================================================================

 %>
 
<head>
    <title>Page One</title>
<LINK REL=StyleSheet HREF="CCA_stylesheet.css" TYPE="text/css" >
<script type="text/javascript" src="mree.js"></script>
<script language="javascript" type="text/javascript">

// <!CDATA[
function icomplete(what)
{
   var total=0
       document.getElementById("mylabel").innerText="";
        total = total + validate("F1_Date");
        total = total + validate("F2_Date"); 
        total = total + validate("F3_Date"); 
        total = total + validate("F4_Date"); 

        validateall(4,total,VirtualID,"2.3")
}


function NotApplicable()
{
 var d = new Date() 
 var dy =     PadDigits(  d.getFullYear()  , 4)
 var dm =    PadDigits(     d.getMonth() +1 , 2)
 var dd =      PadDigits(   d.getDate() , 2)

 document.getElementById("F1_Date").value=dd + "/" + dm + "/" + dy;
 document.getElementById("F1_Date").style.backgroundColor="white"; 
 document.getElementById("F2_Date").value=dd + "/" + dm + "/" + dy;
 document.getElementById("F2_Date").style.backgroundColor="white"; 
 document.getElementById("F3_Date").value=dd + "/" + dm + "/" + dy;
 document.getElementById("F3_Date").style.backgroundColor="white"; 
 document.getElementById("F4_Date").value=dd + "/" + dm + "/" + dy;
 document.getElementById("F4_Date").style.backgroundColor="white"; 
 
  // updateme() ;
  
}


function updateme()
{
  document.getElementById("myform").action = "evidenceupdate.asp?virtualid=" + VirtualID + "&base=tblCRC_CCA_Page23" + "&tag=" + TagId + "&editid="  + EditID ;
  document.getElementById("myform").submit();
}


// ]]>
</script>
</head>

<body>
 <form name="myform"  action="evidenceupdate.asp" method="post" target="myframe"> 
    <div  id="labelbanner">
         Footprint report  (2.3) 
    </div>
    
 <br />
<!-----  R E T U R N ---->
 <button class="backbutton" onclick="window.location.href=<%="'page1.asp?id=" & TheID & "&net=" & TheNet  & "&phase=" & Current_Phase  & "'" %>" title="Return back main menu" ><< Back</button>


<%if TheNet="false" then %>
 <input class="completebutton" title="Assign these questions with a Completed status Tag " type="button" value="Complete" onclick="javascript:icomplete('2.3');" />
 <input class="completebutton" title="Not Applicable" type="button" value="N/A" onclick="javascript:NotApplicable('2.3');" />
<%end if%>
<!-----  R E T U R N ---->
<br />

<label class="messagelabel" id="myLabel"></label>
  
  
  
    <table  >
    
    <tr>
    <td  ></td>
     <td  class="One">
         Supply</td>
     <td  class="One">
         Emissions 
        <br />
         (kWh and tCO2)
         </td>
     <td  ></td>  
    </tr>
    
        
        <tr  >
            <td class="One" >
                Total core supply of electricity</td>
            <td  >  <input id="F1" type="text" name="F1" class="Two"  value="<%=F1%>"/></td>
            <td  >  <input id="F5"   type="text" name="F5" class="Two"  value="<%=F5%>" /></td>
            <td  >  <input id="F1_Date" type="text" name="F1_Date"  style="<%=vis%>"  class="Four"  value="<%=F1_Date%>"/></td>
        </tr>
        <tr  >
            <td class="One">
                Total of any electricity generating credits (EGCs)</td>
            <td  > <input id="F2" type="text" name="F2" class="Two"   value="<%=F2%>"/></td>
            <td  > <input id="F6"  type="text" name="F6" class="Two"  value="<%=F6%>"/></td>
            <td  > <input id="F2_Date"   type="text" name="F2_Date"  style="<%=vis%>"  class="Four"  value="<%=F2_Date%>"/></td>
        </tr>
        <tr  >
            <td class="One">
            Total core supply of gas </td>
            <td  > <input id="F3" type="text" name="F3" class="Two"  value="<%=F3%>"/></td>
            <td > <input id="F7" type="text"  name="F7" class="Two"  value="<%=F7%>"/></td>
            <td > <input id="F3_Date"  type="text" name="F3_Date"  style="<%=vis%>"  class="Four"  value="<%=F3_Date%>" /></td>
        </tr>
        <tr  >
            <td  class="One">
                Total other energy supply 
                (please specify type(s))
                <br /> 
                with a new row per fuel 
                type
                </td>
                
            <td > <input id="F4" type="text" name="F4" class="Two"  value="<%=F4%>" /> </td>
            <td > <input id="F8"  type="text" name="F8" class="Two"  value="<%=F8%>" /></td>
            <td ><input id="F4_Date"  type="text" name="F4_Date"  style="<%=vis%>"  class="Four"  value="<%=F4_Date%>" /></td>
        </tr>
    </table>

 
<%if TheNet="false" then %>
 <INPUT type="button" value="Submit" id="button1" name="button1" onclick="javascript:updateme();" class="backbutton"/>
<%end if %>


<iframe id="myframe" name="myframe" src="blank2.htm" frameborder="no" scrolling="no"/>
 
 </form>
</body>
</html>
<%
 

 Call CloseConnection
 %> 