<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <title>Untitled Page</title>
    <script language="javascript" type="text/javascript">
// <!CDATA[

function showme()
{
    var StringConvert = document.getElementById("myform").outerHTML
    document.getElementById("textsubmit").innerText = StringConvert ;
    document.myform.submit();
}

function icomplete(what)
{
     alert(what);
}
 

// ]]>
</script>
</head>
<body>
    <br />
    <br />
  
    <table cellpadding="0" cellspacing="0" class="t11" style="border-top-style: solid;
        border-right-style: solid; border-left-style: solid; border-bottom-style: solid">
        <tr class="tr11">
            <td class="td486" colspan="2" style="text-align: center">
 <input id="Button2" style="width: 80px" title="buton" type="button" value="Complete" onclick="javascript:icomplete('5.1.1');" /><br />              
                <font class="ft54"><strong>5.1.1</strong></font></td>
            <td class="td608" colspan="2" style="font-weight: bold">
                <font class="ft54">Electricity sources cross
                    <br />
                    referenced to meter
                    <br />
                    number and location</font></td>
            <td class="td609" style="font-weight: bold; width: 206px">
                <font class="ft54">Tick</font><font class="ft54"> to confirm if details included</font></td>
            <td class="td610" style="width: 125px; text-align: center">
                <font class="ft54">Document
                    <br />
                    reference</font></td>
            <td class="td611" style="width: 137px; text-align: center">
                <font class="ft54">Authorised<br />
                    signature</font></td>
            <td class="td612" style="width: 140px; text-align: center">
                <font class="ft54">Completion<br />
                    date</font></td>
        </tr>
        <tr class="tr10">
            <td class="td625" colspan="2">
                &nbsp;</td>
            <td class="td621" colspan="2">
                <font class="ft59">List of meters</font></td>
            <td class="td626" style="width: 206px; text-align: center">
                &nbsp;<select id="Select1" style="width: 84px">
                    <option selected="selected" value="Confirmed">Confirmed</option>
                    <option value="Unconfirmed">Unconfirmed</option>
                </select></td>
            <td class="td627" style="width: 125px">
                &nbsp;</td>
            <td class="td628" style="width: 137px">
                &nbsp;</td>
            <td class="td629" style="width: 140px; text-align: center">
                &nbsp;<input id="Text1" style="width: 79px" type="text" /></td>
        </tr>
        <tr class="tr2">
            <td class="td492" colspan="2">
                &nbsp;</td>
            <td class="td613" colspan="2">
                Meter identification number</td>
            <td class="td366" style="width: 206px; text-align: center">
                &nbsp;<select id="Select3" style="width: 84px">
                    <option selected="selected" value="Confirmed">Confirmed</option>
                    <option value="Unconfirmed">Unconfirmed</option>
                </select></td>
            <td class="td618" style="width: 125px">
                &nbsp;</td>
            <td class="td619" style="width: 137px">
                &nbsp;</td>
            <td class="td620" style="width: 140px; text-align: center">
                &nbsp;<input id="Text2" style="width: 79px" type="text" /></td>
        </tr>
        <tr class="tr19">
            <td class="td498" colspan="2">
                &nbsp;</td>
            <td class="td621" colspan="2">
                <font class="ft61">(MPAN/MPRN)</font></td>
            <td class="td356" style="width: 206px; text-align: center">
                &nbsp;<select id="Select4" style="width: 84px">
                    <option selected="selected" value="Confirmed">Confirmed</option>
                    <option value="Unconfirmed">Unconfirmed</option>
                </select></td>
            <td class="td622" style="width: 125px">
                &nbsp;</td>
            <td class="td623" style="width: 137px">
                &nbsp;</td>
            <td class="td624" style="width: 140px; text-align: center">
                &nbsp;<input id="Text3" style="width: 79px" type="text" /></td>
        </tr>
        <tr class="tr10">
            <td class="td625" colspan="2">
                &nbsp;</td>
            <td class="td621" colspan="2">
                <font class="ft58">Non-utility meter ID</font></td>
            <td class="td626" style="width: 206px; text-align: center">
                &nbsp;<select id="Select5" style="width: 84px">
                    <option selected="selected" value="Confirmed">Confirmed</option>
                    <option value="Unconfirmed">Unconfirmed</option>
                </select></td>
            <td class="td627" style="width: 125px">
                &nbsp;</td>
            <td class="td628" style="width: 137px">
                &nbsp;</td>
            <td class="td629" style="width: 140px; text-align: center">
                &nbsp;<input id="Text4" style="width: 79px" type="text" /></td>
        </tr>
        <tr class="tr13">
            <td class="td630" colspan="2">
                &nbsp;</td>
            <td class="td621" colspan="2">
                <font class="ft59">UMS ID: MSID</font></td>
            <td class="td631" style="width: 206px; text-align: center">
                &nbsp;<select id="Select6" style="width: 84px">
                    <option selected="selected" value="Confirmed">Confirmed</option>
                    <option value="Unconfirmed">Unconfirmed</option>
                </select></td>
            <td class="td632" style="width: 125px">
                &nbsp;</td>
            <td class="td633" style="width: 137px">
                &nbsp;</td>
            <td class="td634" style="width: 140px; text-align: center">
                &nbsp;<input id="Text5" style="width: 79px" type="text" /></td>
        </tr>
        <tr class="tr2">
            <td class="td492" colspan="2">
                &nbsp;</td>
            <td class="td613" colspan="2">
                Location identifier (including SGU)</td>
            <td class="td366" style="width: 206px; text-align: center">
                &nbsp;<select id="Select7" style="width: 84px">
                    <option selected="selected" value="Confirmed">Confirmed</option>
                    <option value="Unconfirmed">Unconfirmed</option>
                </select></td>
            <td class="td618" style="width: 125px">
                &nbsp;</td>
            <td class="td619" style="width: 137px">
                &nbsp;</td>
            <td class="td620" style="width: 140px; text-align: center">
                &nbsp;<input id="Text6" style="width: 79px" type="text" /></td>
        </tr>
        <tr class="tr22">
            <td class="td510" colspan="2" style="height: 19px">
                &nbsp;</td>
            <td class="td621" colspan="2" style="height: 19px">
                <font class="ft58">&nbsp;</font></td>
            <td class="td350" style="width: 206px; height: 19px">
                &nbsp;</td>
            <td class="td635" style="width: 125px; height: 19px">
                &nbsp;</td>
            <td class="td636" style="width: 137px; height: 19px">
                &nbsp;</td>
            <td class="td637" style="width: 140px; height: 19px">
                &nbsp;</td>
        </tr>
        <tr class="tr2">
            <td class="td492" colspan="2" style="height: 57px">
                &nbsp;</td>
            <td class="td613" colspan="2" style="height: 57px">
                Please confirm if there are any
                <br />
                CCA exemptions or exclusions (covered
                <br />
                exclusions (covered
                <br />
                separately by Table 5.1.6)</td>
            <td class="td614" style="width: 206px; height: 57px; text-align: center">
                <font class="ft67"><em>
                    <select id="Select2" style="width: 58px">
                        <option selected="selected" value="Yes">Yes</option>
                        <option value="No">No</option>
                    </select>
                </em></font>
            </td>
            <td class="td618" style="width: 125px; font-style: italic; height: 57px">
                &nbsp;</td>
            <td class="td619" style="width: 137px; height: 57px">
                &nbsp;</td>
            <td class="td620" style="width: 140px; height: 57px; text-align: center">
                &nbsp;<input id="Text7" style="width: 79px" type="text" /></td>
        </tr>
        <tr class="tr3">
            <td class="td641" colspan="2">
                &nbsp;</td>
            <td class="td642" colspan="2">
                &nbsp;</td>
            <td class="td643" style="width: 206px">
                &nbsp;</td>
            <td class="td644" style="width: 125px">
                &nbsp;</td>
            <td class="td643" style="width: 137px">
                &nbsp;</td>
            <td class="td645" style="width: 140px">
                &nbsp;</td>
        </tr>
        <tr class="tr3">
        
            <td class="td646" colspan="2" style="text-align: center">
    <input id="Button2" style="width: 80px" title="buton" type="button" value="Complete" onclick="javascript:icomplete('5.1.2');" /><br />  
                <font class="ft54"><strong>5.1.2</strong></font></td>
            <td class="td647" style="font-weight: bold">
                &nbsp;</td>
            <td class="td648" style="width: 313px">
                <font class="ft54"><strong>Meter details by
                    <br />
                    electricity supply</strong></font></td>
            <td class="td614" style="font-weight: bold; width: 206px">
                <font class="ft54">Tick<font class="ft54"> to confirm if details included</font></font></td>
            <td class="td615" style="width: 125px; text-align: center;">
                <font class="ft54">Document
                    <br />
                    reference</font></td>
            <td class="td616" style="width: 137px; text-align: center;">
                <font class="ft54">Authorised<br />
                    signature</font></td>
            <td class="td617" style="width: 140px; text-align: center;">
                <font class="ft54">Completion<br />
                    date</font></td>
        </tr>
        <tr class="tr10">
            <td class="td649" colspan="2">
                &nbsp;</td>
            <td class="td647">
                &nbsp;</td>
            <td class="td648" style="width: 313px">
                <font class="ft54"><strong>&nbsp;</strong></font></td>
            <td class="td614" style="font-weight: bold; width: 206px">
                <font class="ft54">&nbsp;</font></td>
            <td class="td615" style="font-weight: bold; width: 125px">
                <font class="ft54">&nbsp;</font></td>
            <td class="td616" style="font-weight: bold; width: 137px">
                <font class="ft54">&nbsp;</font></td>
            <td class="td617" style="font-weight: bold; width: 140px">
                <font class="ft54">&nbsp;</font></td>
        </tr>
        <tr class="tr19" style="font-weight: bold">
            <td class="td650" colspan="2">
                &nbsp;</td>
            <td class="td651">
                &nbsp;</td>
            <td class="td652" style="width: 313px">
                &nbsp;</td>
            <td class="td653" style="width: 206px">
                <font class="ft57">&nbsp;</font></td>
            <td class="td622" style="width: 125px">
                &nbsp;</td>
            <td class="td623" style="width: 137px">
                &nbsp;</td>
            <td class="td624" style="width: 140px">
                &nbsp;</td>
        </tr>
        <tr class="tr2">
            <td class="td649" colspan="2">
                &nbsp;</td>
            <td class="td647">
                &nbsp;</td>
            <td class="td648" style="width: 313px">
                List utility and non-utility meter ID by meter details (UMS covered by following
                table)</td>
            <td class="td366" style="width: 206px; text-align: center">
                &nbsp;<select id="Select11" style="width: 84px">
                    <option selected="selected" value="Confirmed">Confirmed</option>
                    <option value="Unconfirmed">Unconfirmed</option>
                </select></td>
            <td class="td618" style="width: 125px">
                &nbsp;</td>
            <td class="td619" style="width: 137px">
                &nbsp;</td>
            <td class="td620" style="width: 140px; text-align: center">
                &nbsp;<input id="Text9" style="width: 79px" type="text" /></td>
        </tr>
        <tr class="tr3">
            <td class="td649" colspan="2">
                &nbsp;</td>
            <td class="td647">
                &nbsp;</td>
            <td class="td648" style="width: 313px">
                Meter type, manual, 100 kVA, AMR, 
                <br />
                other (please specify)</td>
            <td class="td366" style="width: 206px; text-align: center">
                &nbsp;<select id="Select12" style="width: 84px">
                    <option selected="selected" value="Confirmed">Confirmed</option>
                    <option value="Unconfirmed">Unconfirmed</option>
                </select></td>
            <td class="td618" style="width: 125px">
                &nbsp;</td>
            <td class="td619" style="width: 137px">
                &nbsp;</td>
            <td class="td620" style="width: 140px; text-align: center">
                &nbsp;<input id="Text10" style="width: 79px" type="text" /></td>
        </tr>
        <tr class="tr18">
            <td class="td661" colspan="2">
                &nbsp;</td>
            <td class="td662">
                &nbsp;</td>
            <td class="td656" style="width: 313px">
                <font class="ft73">&nbsp;</font></td>
            <td class="td370" style="width: 206px">
                &nbsp;</td>
            <td class="td638" style="width: 125px">
                &nbsp;</td>
            <td class="td639" style="width: 137px">
                &nbsp;</td>
            <td class="td640" style="width: 140px">
                &nbsp;</td>
        </tr>
        <tr class="tr3">
            <td class="td649" colspan="2" style="height: 19px">
                &nbsp;</td>
            <td class="td647" style="height: 19px">
                &nbsp;</td>
            <td class="td648" style="width: 313px; height: 19px">
                Meter accuracy (design value)</td>
            <td class="td366" style="width: 206px; height: 19px; text-align: center">
                &nbsp;<select id="Select13" style="width: 84px">
                    <option selected="selected" value="Confirmed">Confirmed</option>
                    <option value="Unconfirmed">Unconfirmed</option>
                </select></td>
            <td class="td618" style="width: 125px; height: 19px">
                &nbsp;</td>
            <td class="td619" style="width: 137px; height: 19px">
                &nbsp;</td>
            <td class="td620" style="width: 140px; height: 19px; text-align: center">
                &nbsp;<input id="Text11" style="width: 79px" type="text" /></td>
        </tr>
        <tr class="tr19">
            <td class="td650" colspan="2">
                &nbsp;</td>
            <td class="td651">
                &nbsp;</td>
            <td class="td656" style="width: 313px">
                <font class="ft61">&nbsp;</font></td>
            <td class="td356" style="width: 206px">
                &nbsp;</td>
            <td class="td622" style="width: 125px">
                &nbsp;</td>
            <td class="td623" style="width: 137px">
                &nbsp;</td>
            <td class="td624" style="width: 140px">
                &nbsp;</td>
        </tr>
        <tr class="tr10">
            <td class="td663" colspan="2" style="height: 19px">
                &nbsp;</td>
            <td class="td664" style="height: 19px">
                &nbsp;</td>
            <td class="td656" style="width: 313px; height: 19px">
                <font class="ft58">Meter used for EGCs</font></td>
            <td class="td626" style="width: 206px; height: 19px; text-align: center">
                &nbsp;<select id="Select14" style="width: 84px">
                    <option selected="selected" value="Confirmed">Confirmed</option>
                    <option value="Unconfirmed">Unconfirmed</option>
                </select></td>
            <td class="td627" style="width: 125px; height: 19px">
                &nbsp;</td>
            <td class="td628" style="width: 137px; height: 19px">
                &nbsp;</td>
            <td class="td629" style="width: 140px; height: 19px; text-align: center">
                &nbsp;<input id="Text12" style="width: 79px" type="text" /></td>
        </tr>
        <tr class="tr3">
            <td class="td649" colspan="2">
                &nbsp;</td>
            <td class="td647">
                &nbsp;</td>
            <td class="td648" style="width: 313px">
                Meter used for supply to another<br />
                organisation (other than tenants)</td>
            <td class="td366" style="width: 206px; text-align: center">
                &nbsp;<select id="Select15" style="width: 84px">
                    <option selected="selected" value="Confirmed">Confirmed</option>
                    <option value="Unconfirmed">Unconfirmed</option>
                </select></td>
            <td class="td618" style="width: 125px">
                &nbsp;</td>
            <td class="td619" style="width: 137px">
                &nbsp;</td>
            <td class="td620" style="width: 140px; text-align: center">
                &nbsp;<input id="Text13" style="width: 79px" type="text" /></td>
        </tr>
        <tr class="tr18">
            <td class="td661" colspan="2">
                &nbsp;</td>
            <td class="td662">
                &nbsp;</td>
            <td class="td656" style="width: 313px">
                <font class="ft73">&nbsp;</font></td>
            <td class="td370" style="width: 206px">
                &nbsp;</td>
            <td class="td638" style="width: 125px">
                &nbsp;</td>
            <td class="td639" style="width: 137px">
                &nbsp;</td>
            <td class="td640" style="width: 140px">
                &nbsp;</td>
        </tr>
        <tr class="tr1">
            <td class="td641" colspan="2">
                &nbsp;</td>
            <td class="td642" colspan="2">
                &nbsp;</td>
            <td class="td643" style="width: 206px">
                &nbsp;</td>
            <td class="td644" style="width: 125px">
                &nbsp;</td>
            <td class="td643" style="width: 137px">
                &nbsp;</td>
            <td class="td645" style="width: 140px">
                &nbsp;</td>
        </tr>
        <tr class="tr2">
            <td class="td665" style="width: 45px; height: 38px; text-align: center;">
<input id="Button2" style="width: 80px" title="buton" type="button" value="Complete" onclick="javascript:icomplete('5.1.3');" /><br />              
                <font class="ft54"><strong>5.1.3</strong></font></td>
            <td class="td666" style="font-weight: bold; height: 38px; width: 7px;">
                &nbsp;</td>
            <td class="td667" colspan="2" style="height: 38px">
                <font class="ft54"><strong>Electricity source by
                    <br />
                    un-metered supply (UMS)</strong></font></td>
            <td class="td614" style="font-weight: bold; width: 206px; height: 38px">
                <font class="ft54">Tick<font class="ft54"> to confirm if details included</font></font></td>
            <td class="td615" style="width: 125px; height: 38px; text-align: center;">
                <font class="ft54">Document
                    <br />
                    reference</font></td>
            <td class="td616" style="width: 137px; height: 38px; text-align: center;">
                <font class="ft54">Authorised<br />
                    signature</font></td>
            <td class="td617" style="width: 140px; height: 38px; text-align: center;">
                <font class="ft54">Completion<br />
                    date</font></td>
        </tr>
        <tr class="tr2">
            <td class="td529" style="width: 45px">
                &nbsp;</td>
            <td class="td666" style="width: 7px">
                &nbsp;</td>
            <td class="td667" colspan="2">
                List UMS ID by meter details Calculation methodology</td>
            <td class="td366" style="width: 206px; text-align: center">
                &nbsp;<input id="Text15" style="width: 79px" type="text" /></td>
            <td class="td618" style="width: 125px">
                &nbsp;</td>
            <td class="td619" style="width: 137px">
                &nbsp;</td>
            <td class="td620" style="width: 140px; text-align: center">
                &nbsp;<input id="Text16" style="width: 79px" type="text" /></td>
        </tr>
        <tr class="tr19">
            <td class="td669" style="width: 45px">
                &nbsp;</td>
            <td class="td670" style="width: 7px">
                &nbsp;</td>
            <td class="td671" colspan="2">
                <font class="ft61">&nbsp;</font></td>
            <td class="td356" style="width: 206px">
                &nbsp;</td>
            <td class="td622" style="width: 125px">
                &nbsp;</td>
            <td class="td623" style="width: 137px">
                &nbsp;</td>
            <td class="td624" style="width: 140px">
                &nbsp;</td>
        </tr>
        <tr class="tr3">
            <td class="td529" style="width: 45px">
                &nbsp;</td>
            <td class="td666" style="width: 7px">
                &nbsp;</td>
            <td class="td667" colspan="2">
                Local authorities should indicate when<br />
                their street lighting schedule should be done
                <br />
                annually (dynamic supply)</td>
            <td class="td614" style="width: 206px; text-align: center">
                <input id="Text8" style="width: 79px" type="text" /></td>
            <td class="td618" style="width: 125px; font-style: italic">
                &nbsp;</td>
            <td class="td619" style="width: 137px">
                &nbsp;</td>
            <td class="td620" style="width: 140px; text-align: center">
                &nbsp;<input id="Text14" style="width: 79px" type="text" /></td>
        </tr>
    </table>
    <br />
    <table cellpadding="0" cellspacing="0" class="t6" style="border-top-style: solid;
        border-right-style: solid; border-left-style: solid; border-bottom-style: solid">
        <tr class="tr11">
            <td class="td679" style="width: 40px; text-align: center">
<input id="Button2" style="width: 80px" title="buton" type="button" value="Complete" onclick="javascript:icomplete('5.1.4');" /><br />              
                <font class="ft54"><strong>5.1.4</strong></font></td>
            <td class="td680" style="font-weight: bold">
                &nbsp;</td>
            <td class="td433" style="width: 331px">
                <font class="ft54"><strong>Gas sources cross</strong></font></td>
            <td class="td609" style="font-weight: bold; width: 209px">
                <font class="ft54">Tick<font class="ft54"> to confirm if details included</font></font></td>
            <td class="td681" style="width: 157px; text-align: center;">
                <font class="ft54">Document
                    <br />
                    reference</font></td>
            <td class="td609" style="width: 177px; text-align: center;">
                <font class="ft54">Authorised<br />
                    signature</font></td>
            <td class="td541" style="width: 261px; text-align: center;">
                <font class="ft54">Completion<br />
                    date</font></td>
        </tr>
        <tr class="tr10">
            <td class="td682" style="width: 40px">
                &nbsp;</td>
            <td class="td26">
                &nbsp;</td>
            <td class="td438" style="width: 331px">
                <font class="ft54"><strong>referenced to meter</strong></font></td>
            <td class="td614" style="font-weight: bold; width: 209px">
                <font class="ft54">&nbsp;</font></td>
            <td class="td683" style="font-weight: bold; width: 157px">
                <font class="ft54">&nbsp;</font></td>
            <td class="td614" style="font-weight: bold; width: 177px">
                <font class="ft54">&nbsp;</font></td>
            <td class="td684" style="font-weight: bold; width: 261px">
                <font class="ft54">&nbsp;</font></td>
        </tr>
        <tr class="tr19" style="font-weight: bold">
            <td class="td685" style="width: 40px">
                &nbsp;</td>
            <td class="td686">
                &nbsp;</td>
            <td class="td443" style="width: 331px">
                <font class="ft57">number and location</font></td>
            <td class="td653" style="width: 209px">
                <font class="ft57">&nbsp;</font></td>
            <td class="td687" style="width: 157px">
                &nbsp;</td>
            <td class="td356" style="width: 177px">
                &nbsp;</td>
            <td class="td688" style="width: 261px">
                &nbsp;</td>
        </tr>
        <tr class="tr10">
            <td class="td689" style="width: 40px">
                &nbsp;</td>
            <td class="td690">
                &nbsp;</td>
            <td class="td443" style="width: 331px">
                <font class="ft58">List of meters</font></td>
            <td class="td626" style="width: 209px; text-align: center">
                &nbsp;<select id="Select16" style="width: 84px">
                    <option selected="selected" value="Confirmed">Confirmed</option>
                    <option value="Unconfirmed">Unconfirmed</option>
                </select></td>
            <td class="td691" style="width: 157px">
                &nbsp;</td>
            <td class="td626" style="width: 177px">
                &nbsp;</td>
            <td class="td692" style="width: 261px; text-align: center">
                &nbsp;<input id="Text17" style="width: 79px" type="text" /></td>
        </tr>
        <tr class="tr13">
            <td class="td693" style="width: 40px">
                &nbsp;</td>
            <td class="td694">
                &nbsp;</td>
            <td class="td443" style="width: 331px">
                <font class="ft59">Utility meter MPRN</font></td>
            <td class="td631" style="width: 209px; text-align: center">
                &nbsp;<select id="Select17" style="width: 84px">
                    <option selected="selected" value="Confirmed">Confirmed</option>
                    <option value="Unconfirmed">Unconfirmed</option>
                </select></td>
            <td class="td695" style="width: 157px">
                &nbsp;</td>
            <td class="td631" style="width: 177px">
                &nbsp;</td>
            <td class="td553" style="width: 261px; text-align: center">
                &nbsp;<input id="Text18" style="width: 79px" type="text" /></td>
        </tr>
        <tr class="tr10">
            <td class="td689" style="width: 40px">
                &nbsp;</td>
            <td class="td690">
                &nbsp;</td>
            <td class="td443" style="width: 331px">
                <font class="ft59">Non-utility meter ID</font></td>
            <td class="td626" style="width: 209px; text-align: center">
                &nbsp;<select id="Select18" style="width: 84px">
                    <option selected="selected" value="Confirmed">Confirmed</option>
                    <option value="Unconfirmed">Unconfirmed</option>
                </select></td>
            <td class="td691" style="width: 157px">
                &nbsp;</td>
            <td class="td626" style="width: 177px">
                &nbsp;</td>
            <td class="td692" style="width: 261px; text-align: center">
                &nbsp;<input id="Text19" style="width: 79px" type="text" /></td>
        </tr>
        <tr class="tr2">
            <td class="td682" style="height: 19px; width: 40px;">
                &nbsp;</td>
            <td class="td26" style="height: 19px">
                &nbsp;</td>
            <td class="td438" style="width: 331px; height: 19px">
                Meter type (please specify<font class="ft89"><span style="font-size: 6pt">2</span></font><font
                    class="ft58">)</font></td>
            <td class="td366" style="width: 209px; height: 19px; text-align: center">
                &nbsp;<select id="Select19" style="width: 84px">
                    <option selected="selected" value="Confirmed">Confirmed</option>
                    <option value="Unconfirmed">Unconfirmed</option>
                </select></td>
            <td class="td696" style="width: 157px; height: 19px">
                &nbsp;</td>
            <td class="td366" style="width: 177px; height: 19px">
                &nbsp;</td>
            <td class="td697" style="width: 261px; height: 19px; text-align: center">
                &nbsp;<input id="Text20" style="width: 79px" type="text" /></td>
        </tr>
        <tr class="tr22">
            <td class="td698" style="width: 40px">
                &nbsp;</td>
            <td class="td699">
                &nbsp;</td>
            <td class="td443" style="width: 331px">
                <font class="ft58">&nbsp;</font></td>
            <td class="td350" style="width: 209px">
                &nbsp;</td>
            <td class="td700" style="width: 157px">
                &nbsp;</td>
            <td class="td350" style="width: 177px">
                &nbsp;</td>
            <td class="td701" style="width: 261px">
                &nbsp;</td>
        </tr>
        <tr class="tr2">
            <td class="td682" style="width: 40px">
                &nbsp;</td>
            <td class="td26">
                &nbsp;</td>
            <td class="td438" style="width: 331px">
                Meter accuracy (design value)</td>
            <td class="td366" style="width: 209px; text-align: center">
                &nbsp;<select id="Select20" style="width: 84px">
                    <option selected="selected" value="Confirmed">Confirmed</option>
                    <option value="Unconfirmed">Unconfirmed</option>
                </select></td>
            <td class="td696" style="width: 157px">
                &nbsp;</td>
            <td class="td366" style="width: 177px">
                &nbsp;</td>
            <td class="td697" style="width: 261px; text-align: center">
                &nbsp;<input id="Text21" style="width: 79px" type="text" /></td>
        </tr>
        <tr class="tr19">
            <td class="td685" style="width: 40px">
                &nbsp;</td>
            <td class="td686">
                &nbsp;</td>
            <td class="td443" style="width: 331px">
                <font class="ft61">&nbsp;</font></td>
            <td class="td356" style="width: 209px">
                &nbsp;</td>
            <td class="td687" style="width: 157px">
                &nbsp;</td>
            <td class="td356" style="width: 177px">
                &nbsp;</td>
            <td class="td688" style="width: 261px">
                &nbsp;</td>
        </tr>
        <tr class="tr2">
            <td class="td682" style="width: 40px">
                &nbsp;</td>
            <td class="td26">
                &nbsp;</td>
            <td class="td438" style="width: 331px">
                Location identifier (including SGU)</td>
            <td class="td366" style="width: 209px; text-align: center">
                &nbsp;<select id="Select21" style="width: 84px">
                    <option selected="selected" value="Confirmed">Confirmed</option>
                    <option value="Unconfirmed">Unconfirmed</option>
                </select></td>
            <td class="td696" style="width: 157px">
                &nbsp;</td>
            <td class="td366" style="width: 177px">
                &nbsp;</td>
            <td class="td697" style="width: 261px; text-align: center">
                &nbsp;<input id="Text22" style="width: 79px" type="text" /></td>
        </tr>
        <tr class="tr22">
            <td class="td698" style="height: 19px; width: 40px;">
                &nbsp;</td>
            <td class="td699" style="height: 19px">
                &nbsp;</td>
            <td class="td443" style="width: 331px; height: 19px">
                <font class="ft58">&nbsp;</font></td>
            <td class="td350" style="width: 209px; height: 19px">
                &nbsp;</td>
            <td class="td700" style="width: 157px; height: 19px">
                &nbsp;</td>
            <td class="td350" style="width: 177px; height: 19px">
                &nbsp;</td>
            <td class="td701" style="width: 261px; height: 19px">
                &nbsp;</td>
        </tr>
        <tr class="tr2">
            <td class="td682" style="width: 40px">
                &nbsp;</td>
            <td class="td26">
                &nbsp;</td>
            <td class="td438" style="width: 331px">
                Please confirm if there<br />
                are any CCA or EU<br />
                ETS exclusions<br />
                (covered separately by Table 5.1.6)</td>
            <td class="td614" style="width: 209px; text-align: center">
                <font class="ft67"><em>
                    <select id="Select8" style="width: 58px">
                        <option selected="selected" value="Yes">Yes</option>
                        <option value="No">No</option>
                    </select>
                </em></font>
            </td>
            <td class="td696" style="width: 157px; font-style: italic">
                &nbsp;</td>
            <td class="td366" style="width: 177px">
                &nbsp;</td>
            <td class="td697" style="width: 261px; text-align: center">
                &nbsp;<input id="Text23" style="width: 79px" type="text" /></td>
        </tr>
    </table>
    <br />
    <table id="TABLE1" cellpadding="0" cellspacing="0" class="t6" style="border-top-style: solid;
        border-right-style: solid; border-left-style: solid; border-bottom-style: solid; text-align: center;" onclick="return TABLE1_onclick()">
        <tr class="tr11">
            <td class="td711" style="width: 41px; text-align: center;">
<input id="Button2" style="width: 80px" title="buton" type="button" value="Complete" onclick="javascript:icomplete('5.1.5');" /><br />              
                <font class="ft54"><strong>5.1.5</strong></font></td>
            <td class="td712" style="font-weight: bold; width: 335px; text-align: left;">
                <font class="ft54">Other sources cross</font></td>
            <td class="td713" style="font-weight: bold; width: 208px">
                Tick<font class="ft54"> to confirm if details included</font></td>
            <td class="td714" style="width: 157px">
                <font class="ft54">Document
                    <br />
                    reference</font></td>
            <td class="td714" style="width: 176px; text-align: center;">
                <font class="ft54">Authorised<br />
                    signature</font></td>
            <td class="td434" style="width: 263px; text-align: center;">
                <font class="ft54">Completion<br />
                    date</font></td>
        </tr>
        <tr class="tr22">
            <td class="td715" style="width: 41px">
                &nbsp;</td>
            <td class="td716" style="width: 335px; text-align: left;">
                <font class="ft56"><strong>referenced to location List of supplies</strong></font></td>
            <td class="td717" style="font-weight: bold; width: 208px">
                <font class="ft56">&nbsp;</font></td>
            <td class="td718" style="font-weight: bold; width: 157px">
                <font class="ft56">&nbsp;</font></td>
            <td class="td718" style="font-weight: bold; width: 176px">
                <font class="ft56">&nbsp;</font></td>
            <td class="td299" style="font-weight: bold; width: 263px">
                <font class="ft56">&nbsp;</font></td>
        </tr>
        <tr class="tr10" style="font-weight: bold">
            <td class="td719" style="width: 41px">
                &nbsp;</td>
            <td class="td716" style="width: 335px; text-align: left;">
                <font class="ft59">&nbsp;</font></td>
            <td class="td720" style="width: 208px">
                &nbsp;</td>
            <td class="td721" style="width: 157px">
                &nbsp;</td>
            <td class="td721" style="width: 176px">
                &nbsp;</td>
            <td class="td722" style="width: 263px">
                &nbsp;</td>
        </tr>
        <tr class="tr10">
            <td class="td719" style="width: 41px; height: 16px">
                &nbsp;</td>
            <td class="td716" style="width: 335px; height: 16px; text-align: left;">
                <font class="ft58">Type of fuel being used</font></td>
            <td class="td720" style="width: 208px; height: 16px; text-align: center">
                &nbsp;<input id="Text24" style="width: 171px" type="text" /></td>
            <td class="td721" style="width: 157px; height: 16px">
                &nbsp;</td>
            <td class="td721" style="width: 176px; height: 16px">
                &nbsp;</td>
            <td class="td722" style="width: 263px; height: 16px; text-align: center;">
                &nbsp;<input id="Text27" style="width: 79px" type="text" /></td>
        </tr>
        <tr class="tr3">
            <td class="td723" style="width: 41px">
                &nbsp;</td>
            <td class="td724" style="width: 335px; text-align: left;">
                Type of usage (as lead,backup, ignition etc)</td>
            <td class="td725" style="width: 208px; text-align: center">
                &nbsp;<input id="Text25" style="width: 171px" type="text" /></td>
            <td class="td726" style="width: 157px">
                &nbsp;</td>
            <td class="td726" style="width: 176px">
                &nbsp;</td>
            <td class="td304" style="width: 263px; text-align: center;">
                &nbsp;<input id="Text28" style="width: 79px" type="text" /></td>
        </tr>
        <tr class="tr19">
            <td class="td727" style="width: 41px; height: 19px">
                &nbsp;</td>
            <td class="td716" style="width: 335px; height: 19px">
                <font class="ft61">&nbsp;</font></td>
            <td class="td728" style="width: 208px; height: 19px">
                &nbsp;</td>
            <td class="td729" style="width: 157px; height: 19px">
                &nbsp;</td>
            <td class="td729" style="width: 176px; height: 19px">
                &nbsp;</td>
            <td class="td318" style="width: 263px; height: 19px">
                &nbsp;</td>
        </tr>
        <tr class="tr2">
            <td class="td723" style="width: 41px">
                &nbsp;</td>
            <td class="td724" style="width: 335px; text-align: left;">
                Measurement method (for
                example invoices,weighbridge, estimation)</td>
            <td class="td725" style="width: 208px; text-align: center;">
                &nbsp;<input id="Text26" style="width: 171px" type="text" /></td>
            <td class="td726" style="width: 157px">
                &nbsp;</td>
            <td class="td726" style="width: 176px">
                &nbsp;</td>
            <td class="td304" style="width: 263px; text-align: center;">
                &nbsp;<input id="Text29" style="width: 79px" type="text" /></td>
        </tr>
        <tr class="tr10">
            <td class="td723" style="width: 41px">
                &nbsp;</td>
            <td class="td724" style="width: 335px">
                &nbsp;</td>
            <td class="td725" style="width: 208px">
                &nbsp;</td>
            <td class="td726" style="width: 157px">
                &nbsp;</td>
            <td class="td726" style="width: 176px">
                &nbsp;</td>
            <td class="td304" style="width: 263px">
                &nbsp;</td>
        </tr>
        <tr class="tr13">
            <td class="td730" style="width: 41px">
                &nbsp;</td>
            <td class="td716" style="width: 335px; text-align: left;">
                <font class="ft59">Suppliers used Location identifier (including SGU)</font></td>
            <td class="td731" style="width: 208px">
                &nbsp;<input id="Text30" style="width: 171px" type="text" /></td>
            <td class="td732" style="width: 157px">
                &nbsp;</td>
            <td class="td732" style="width: 176px">
                &nbsp;</td>
            <td class="td733" style="width: 263px">
                &nbsp;<input id="Text31" style="width: 79px" type="text" /></td>
        </tr>
        <tr class="tr2">
            <td class="td723" style="width: 41px; height: 19px;">
                &nbsp;</td>
            <td class="td724" style="width: 335px; height: 19px; text-align: left;">
                &nbsp;</td>
            <td class="td725" style="width: 208px; height: 19px;">
                &nbsp;</td>
            <td class="td726" style="width: 157px; height: 19px;">
                &nbsp;</td>
            <td class="td726" style="width: 176px; height: 19px;">
                &nbsp;</td>
            <td class="td304" style="width: 263px; height: 19px;">
                &nbsp;</td>
        </tr>
        <tr class="tr13">
            <td class="td730" style="width: 41px">
                &nbsp;</td>
            <td class="td716" style="width: 335px; text-align: left;">
                <font class="ft59">ROC/FIT claimed (if relevant)</font></td>
            <td class="td717" style="width: 208px; text-align: center">
                <font class="ft64"><em>
                    <select id="Select10" name="N/A" style="width: 58px">
                        <option selected="selected" value="Yes">Yes</option>
                        <option value="No">No</option>
                        <option value="N/A">N/A</option>
                    </select>
                </em></font>
            </td>
            <td class="td732" style="width: 157px; font-style: italic">
                &nbsp;</td>
            <td class="td732" style="width: 176px">
                &nbsp;</td>
            <td class="td733" style="width: 263px">
                &nbsp;<input id="Text32" style="width: 79px" type="text" /></td>
        </tr>
        <tr class="tr2">
            <td class="td723" style="width: 41px">
                &nbsp;</td>
            <td class="td724" style="width: 335px; text-align: left;">
                Please confirm if there are any CCA or EU ETS exclusions (covered separately by
                Table 5.1.6)</td>
            <td class="td734" style="width: 208px; text-align: center">
                <font class="ft67"><em>
                    <select id="Select9" style="width: 58px">
                        <option selected="selected" value="Yes">Yes</option>
                        <option value="No">No</option>
                    </select>
                </em></font>
            </td>
            <td class="td726" style="width: 157px; font-style: italic">
                &nbsp;</td>
            <td class="td726" style="width: 176px">
                &nbsp;</td>
            <td class="td304" style="width: 263px">
                &nbsp;<input id="Text33" style="width: 79px" type="text" /></td>
        </tr>
        <tr class="tr10">
            <td class="td723" style="width: 41px">
                &nbsp;</td>
            <td class="td724" style="width: 335px; text-align: left;">
                &nbsp;</td>
            <td class="td725" style="width: 208px">
                &nbsp;</td>
            <td class="td726" style="width: 157px">
                &nbsp;</td>
            <td class="td726" style="width: 176px">
                &nbsp;</td>
            <td class="td304" style="width: 263px">
                &nbsp;</td>
        </tr>
    </table>
    <br />
    <br />
    <table cellpadding="0" cellspacing="0" class="t12" style="width: 1180px; border-top-style: solid; border-right-style: solid; border-left-style: solid; border-bottom-style: solid;">
        <tr class="tr2">
            <td class="td486" style="width: 44px; height: 38px">
            <center>
   <input id="Button2" style="width: 80px" title="buton" type="button" value="Complete" onclick="javascript:icomplete('5.1.6');" /><br />  
                5.1.6
                </center>
                </td>
            <td class="td589" style="height: 38px">
                EU ETS and CCA
                <br />
                exemptions/exclusions<br />
                by site and source
                <br />
            </td>
            <td class="td738" style="height: 38px; text-align: center">
                Tick to confirm
                <br />
                when completed</td>
            <td class="td375" style="height: 38px; text-align: center">
                Document<br />
                when completed</td>
            <td class="td739" style="height: 38px; text-align: center">
                Authorised<br />
                signature</td>
            <td class="td740" style="height: 38px; text-align: center">
                Completion<br />
                date</td>
        </tr>
        <tr class="tr10">
            <td class="td492" style="width: 44px">
                &nbsp;</td>
            <td class="td598">
                &nbsp;Site, CCA or EU ETS<br />
                facility address and postcode</td>
            <td class="td741" style="text-align: center">
                &nbsp;<select id="Select24" style="width: 84px">
                    <option selected="selected" value="Confirmed">Confirmed</option>
                    <option value="Unconfirmed">Unconfirmed</option>
                </select></td>
            <td class="td380">
                &nbsp;</td>
            <td class="td742">
                &nbsp;</td>
            <td class="td743" style="text-align: center">
                &nbsp;<input id="Text34" style="width: 79px" type="text" /></td>
        </tr>
        <tr class="tr19">
            <td class="td498" style="width: 44px">
                &nbsp;</td>
            <td class="td602">
                <font class="ft61">&nbsp;</font></td>
            <td class="td744">
                &nbsp;</td>
            <td class="td745">
                &nbsp;</td>
            <td class="td171">
                &nbsp;</td>
            <td class="td171">
                &nbsp;</td>
        </tr>
        <tr class="tr2">
            <td class="td492" style="width: 44px">
                &nbsp;</td>
            <td class="td598">
                <font class="ft85">Group member (CCA sources)</font></td>
            <td class="td746" style="text-align: center">
                &nbsp;<select id="Select23" style="width: 84px">
                    <option selected="selected" value="Confirmed">Confirmed</option>
                    <option value="Unconfirmed">Unconfirmed</option>
                </select></td>
            <td class="td384">
                &nbsp;</td>
            <td class="td66">
                &nbsp;</td>
            <td class="td66" style="text-align: center">
                &nbsp;<input id="Text35" style="width: 79px" type="text" /></td>
        </tr>
        <tr class="tr19">
            <td class="td498" style="width: 44px">
                &nbsp;</td>
            <td class="td602">
                <font class="ft61">&nbsp;</font></td>
            <td class="td744">
                &nbsp;</td>
            <td class="td745">
                &nbsp;</td>
            <td class="td171">
                &nbsp;</td>
            <td class="td171">
                &nbsp;</td>
        </tr>
        <tr class="tr2">
            <td class="td492" style="width: 44px">
                &nbsp;</td>
            <td class="td598">
                <font class="ft85">Type of supply electricity,
                    <br />
                    natural gas, oil, bulk<br />
                    supplies/other (please specify)</font></td>
            <td class="td746" style="text-align: center">
                &nbsp;<textarea id="TextArea1" style="width: 269px; height: 114px"></textarea></td>
            <td class="td384">
                &nbsp;</td>
            <td class="td66">
                &nbsp;</td>
            <td class="td66" style="text-align: center">
                &nbsp;<input id="Text36" style="width: 79px" type="text" /></td>
        </tr>
        <tr class="tr19">
            <td class="td498" style="width: 44px">
                &nbsp;</td>
            <td class="td602">
                <font class="ft61">&nbsp;</font></td>
            <td class="td744">
                &nbsp;</td>
            <td class="td745">
                &nbsp;</td>
            <td class="td171">
                &nbsp;</td>
            <td class="td171">
                &nbsp;</td>
        </tr>
        <tr class="tr10">
            <td class="td625" style="width: 44px">
                &nbsp;</td>
            <td class="td602">
                <font class="ft58">Meter identification details</font></td>
            <td class="td747" style="text-align: center">
                &nbsp;<select id="Select22" style="width: 84px">
                    <option selected="selected" value="Confirmed">Confirmed</option>
                    <option value="Unconfirmed">Unconfirmed</option>
                </select></td>
            <td class="td401">
                &nbsp;</td>
            <td class="td748">
                &nbsp;</td>
            <td class="td748" style="text-align: center">
                &nbsp;<input id="Text37" style="width: 79px" type="text" /></td>
        </tr>
    </table>
    <br />

</body>
</html>
