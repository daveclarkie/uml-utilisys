<html>  
 <%
response.buffer=true
 
Public dComm  
Public rComm  
 
Public Function OpenConnection()
  Set dComm = CreateObject("ADODB.Connection")
      dComm.Open "Provider=SQLOLEDB.1;Data Source=uk-ed0-sqlcl-01.MCEG.local;Initial Catalog=UML_CMS;Integrated Security=SSPI;"
End Function

Public Function OpenRecordset(nSQL)
 Set rComm = CreateObject("ADODB.recordset")
     rComm.ActiveConnection = dComm
     rComm.CursorLocation = 3 ' 2
     rComm.CursorType = 3 '1
     rComm.LockType = 1 '3
     rComm.Source = nSQL
     rComm.Open
End Function

Public Function CloseConnection()
  Set dComm = Nothing
  Set rComm = Nothing
End Function


'=========================================================================================
strsql = "SELECT * FROM dbo.tblCRC_CCA_Page21 WHERE virtualID = " & session("VirtualID")
strsql = strsql & " AND TagID='2.1'"

Call OpenConnection
Call OpenRecordset(strsql)
if rComm.EOF = true then
   continue=false
   f1_date=""
   f2_date=""
   f3_date=""
   f4=""
   f4_date=""
   f5=""
   f5_date=""
   
   Response.Write "<script language=""javascript"" type=""text/javascript"">" & vbnewline
   Response.write "var VirtualID=" & session("VirtualID")  & vbnewline
   Response.write "var EditID=" & "0"  & vbnewline
   Response.write "var TagId=" & "'" & "2.1" & "'" & vbnewline
   Response.Write "</script>" &  vbnewline
else
   continue=true
   f1_date=trim(rcomm("f1_date"))
   f2_date=trim(rcomm("f2_date"))
   f3_date=trim(rcomm("f3_date"))
   f4=rcomm("f4")
   f4_date=trim(rcomm("f4_date"))
   f5=rcomm("f5")
   f5_date=trim(rcomm("f5_date"))
   
   Response.Write "<script language=""javascript"" type=""text/javascript"">" & vbnewline
   Response.write "var VirtualID=" & session("VirtualID")  & vbnewline
   Response.write "var EditID=" & rComm("ID") & vbnewline
   Response.write "var TagId=" & "'" & "2.1" & "'" & vbnewline
   Response.Write "</script>" &  vbnewline
end if
 
  
'=========================================================================================

 %>
 
<head>
    <title>Page One</title>
<LINK REL=StyleSheet HREF="CCA_stylesheet.css" TYPE="text/css" >
<script type="text/javascript" src="mree.js"></script>
<script language="javascript" type="text/javascript">

// <!CDATA[
function icomplete(what)
{
   var total=0
       document.getElementById("mylabel").innerText="";
        total = total + validate("F1_Date");
        total = total + validate("F2_Date"); 
        total = total + validate("F3_Date"); 
        total = total + validate("F4_Date"); 
        total = total + validate("F5_Date"); 

        validateall(5,total,VirtualID,"2.1")
}


function NotApplicable()
{
 var d = new Date() 
 var dy =     PadDigits(  d.getFullYear()  , 4)
 var dm =    PadDigits(     d.getMonth()  , 2)
 var dd =      PadDigits(   d.getDate() , 2)

 document.getElementById("F1_Date").value=dd + "/" + dm + "/" + dy;
 document.getElementById("F1_Date").style.backgroundColor="white"; 
 document.getElementById("F2_Date").value=dd + "/" + dm + "/" + dy;
 document.getElementById("F2_Date").style.backgroundColor="white"; 
 document.getElementById("F3_Date").value=dd + "/" + dm + "/" + dy;
 document.getElementById("F3_Date").style.backgroundColor="white"; 
 document.getElementById("F4_Date").value=dd + "/" + dm + "/" + dy;
 document.getElementById("F4_Date").style.backgroundColor="white"; 
 document.getElementById("F5_Date").value=dd + "/" + dm + "/" + dy;
 document.getElementById("F5_Date").style.backgroundColor="white"; 

  //updateme() ;
}


function updateme()
{
  document.getElementById("myform").action = "evidenceupdate.asp?virtualid=" + VirtualID + "&base=tblCRC_CCA_Page21" + "&tag=" + TagId + "&editid="  + EditID ;
  document.getElementById("myform").submit();
}


// ]]>
</script>
</head>

<body>
 <form name="myform"  action="evidenceupdate.asp" method="post" target="myframe"> 
    <div  id="labelbanner">
        Documentation upload&nbsp;</div>
    
 <br />
 <button class="backbutton" onclick="window.location.href=<%="'page1.asp?id=" & session("VirtualID")& "'" %>" title="Return back main menu" ><< Back</button>
  <br />

<label class="messagelabel" id="myLabel"></label>
    <table >
        <tr>
            <td   class="One"  >
               Description</td>
            <td    >
                <input name="F4"  type="text"  value="default" class="Three" /></td>
            <td>
                  <select name="F1" class="Two" >
					       <option value="pdf">Adobe PDF</option> 
					       <option value="doc">Word document (2003)</option>
					       <option value="docx">Word document (2003 > 2010)</option> 
					       <option value="xls">Excel document (2003)</option>
					       <option value="xlsx">Excel document (2003 > 2010)</option> 
                </select>
             </td>           
            
            <td>
                 <select name="F1" class="Two" >
                            <Option>2.1</Option>
                            <Option> 2.2</Option>
                            <Option>2.3</Option>
                            <Option>2.4</Option>
                            <Option>3.1</Option>
                            <Option>3.2.1</Option>
                            <Option>3.2.2</Option>
                            <Option>3.2.3</Option>
                            <Option>4.1</Option>
                            <Option>4.2</Option>
                            <Option>4.3</Option>
                            <Option>4.4</Option>
                            <Option>5.1.1</Option>
                            <Option>5.1.2</Option>
                            <Option>5.1.3</Option>
                            <Option>5.1.4</Option>
                            <Option>5.1.5</Option>
                            <Option>5.1.6</Option>
                            <Option>6.1.1</Option>
                            <Option>6.1.2</Option>
                            <Option>6.2.1</Option>
                            <Option>6.3</Option>
                            <Option>6.4</Option>
                            <Option>6.5</Option>
                            <Option>6.6</Option>
                            <Option>7.1</Option>
                            <Option>7.2.1</Option>
                            <Option>7.2.2</Option>
                            <Option>7.2.3</Option>
                            <Option>7.2.4</Option>
                            <Option>7.2.6</Option>
                            <Option>7.2.7</Option>
                            <Option>7.2.8</Option>
                            <Option>7.2.9</Option>
                            <Option>7.2.10</Option>
                            <Option>7.2.11</Option>
                            <Option>7.3.1</Option>
                            <Option>7.3.2</Option>
                            <Option>7.4.1</Option>
                            <Option>7.5</Option>
                            <Option>8.1.1</Option>
                            <Option>8.1.2</Option>
                            <Option>8.2.1</Option>
                            <Option>8.2.2</Option>
                            <Option>8.3</Option>
                            <Option>8.4</Option>
                            <Option>8.5</Option>
                            <Option>8.6</Option>
                            <Option>8.7</Option>
                            <Option>8.8</Option>
                            <Option>8.9</Option>
                            <Option>9.1</Option>
                            <Option>9.2a</Option>
                            <Option>9.2b</Option>
                            <Option>9.3</Option>
                            <Option>9.4a</Option>
                            <Option>9.4b</Option>
                            <Option>9.5</Option>
                            <Option>9.6.1</Option>
                            <Option>9.7</Option>
                            <Option>9.8</Option>
                            <Option>9.9</Option>
                            <Option>9.10.1</Option>
                </select>
            
            </td> 
                  
        </tr>
        <tr>
            <td class="One" >  Location</td>
            <td class="backbutton">
             <INPUT id="File1" language="javascript"  type=file size=30 name="File1" class="backbutton"/> 
             </td>
        </tr>
        
          <tr>
          <td colspan="2">
             <button class="backbutton" onclick="window.location.href=<%="'page1.asp?id=" & session("VirtualID")& "'" %>" title="Return back main menu" >Update</button>
          </td>
        </tr>
        
        
    </table>

 


<iframe id="myframe" name="myframe" frameborder="no" scrolling="no"/>
 
 </form>
</body>
</html>
<%
 

 Call CloseConnection
 %> 