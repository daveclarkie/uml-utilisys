 <%
Dim TheID 
         TheID=request.QueryString("id")  

Dim TheNet
         TheNet=request.QueryString("net") 

Dim Current_Phase
         Current_Phase=SESSION("phase")  


Dim ReportPhase
         ReportPhase=Request.QueryString("RP")  
If ReportPhase="" then
           Current_Phase=SESSION("phase")  
Else
           Current_Phase=Request.QueryString("RP")  
End If



Dim OnOff 
         if TheNet="true" then
               vis="Display:none;" 
         else
             vis="Display:;"  
        end if


  

          
%> 


 <html>  
 <%
Public dComm  
Public rComm  
 
Public Function OpenConnection()
  Set dComm = CreateObject("ADODB.Connection")
      dComm.Open "Provider=SQLOLEDB.1;Data Source=uk-ed0-sqlcl-01.MCEG.local;Initial Catalog=UML_CMS;Integrated Security=SSPI;"
End Function

Public Function OpenRecordset(nSQL)
 Set rComm = CreateObject("ADODB.recordset")
     rComm.ActiveConnection = dComm
     rComm.CursorLocation = 3 ' 2
     rComm.CursorType = 3 '1
     rComm.LockType = 1 '3
     rComm.Source = nSQL
     rComm.Open
End Function

Public Function CloseConnection()
  Set dComm = Nothing
  Set rComm = Nothing
End Function


'=========================================================================================
strsql = "SELECT * FROM dbo.tblCRC_CCA_Page44 WHERE virtualID = " & TheID
strsql = strsql & " AND TagID='4.4'" & " AND phase=" & "'" & Current_Phase & "'"

Call OpenConnection
Call OpenRecordset(strsql)
if rComm.EOF = true then
   continue=false
   f1_date=""
   f2_date=""
   f4_date=""
   f5_date=""
    
    f1=""
    f2=""
    f3="" 
    f4=""
    f5=""
   
   
   Response.Write "<script language=""javascript"" type=""text/javascript"">" & vbnewline
   Response.write "var VirtualID=" & TheID & vbnewline
   Response.write "var EditID=" & "0"  & vbnewline
   Response.write "var TagId=" & "'" & "4.4" & "'" & vbnewline
   Response.Write "</script>" &  vbnewline
else
   continue=true
   f1_date=trim(rcomm("f1_date"))
   f2_date=trim(rcomm("f2_date"))
   f4_date=trim(rcomm("f4_date"))
   f5_date=trim(rcomm("f5_date"))

   f1=rcomm("f1")
   f2=rcomm("f2")
   f3=rcomm("f3")
   f4=rcomm("f4")
   f5=rcomm("f5")
   
   Response.Write "<script language=""javascript"" type=""text/javascript"">" & vbnewline
   Response.write "var VirtualID=" & TheID  & vbnewline
   Response.write "var EditID=" & rComm("ID") & vbnewline
   Response.write "var TagId=" & "'" & "4.4" & "'" & vbnewline
   Response.Write "</script>" &  vbnewline
end if
 
  
'=========================================================================================

 %>
 
<head>
    <title>Page One</title>
<LINK REL=StyleSheet HREF="CCA_stylesheet.css" TYPE="text/css" >
<script type="text/javascript" src="mree.js"></script>
<script language="javascript" type="text/javascript">

// <!CDATA[
function icomplete(what)
{
   var total=0
       document.getElementById("mylabel").innerText="";
        total = total + validate("F1_Date");
        total = total + validate("F2_Date"); 
        total = total + validate("F4_Date"); 
        total = total + validate("F5_Date"); 
        validateall(4,total,VirtualID,"4.4")
}


function NotApplicable()
{
 var d = new Date() 
 var dy =     PadDigits(  d.getFullYear()  , 4)
 var dm =    PadDigits(     d.getMonth()  +1, 2)
 var dd =      PadDigits(   d.getDate() , 2)

document.getElementById("F1_Date").value=dd + "/" + dm + "/" + dy;
document.getElementById("F1_Date").style.backgroundColor="white"; 
document.getElementById("F2_Date").value=dd + "/" + dm + "/" + dy;
document.getElementById("F2_Date").style.backgroundColor="white"; 
document.getElementById("F4_Date").value=dd + "/" + dm + "/" + dy;
document.getElementById("F4_Date").style.backgroundColor="white"; 
document.getElementById("F5_Date").value=dd + "/" + dm + "/" + dy;
document.getElementById("F5_Date").style.backgroundColor="white"; 

  //updateme() ;
}


function updateme()
{
  document.getElementById("myform").action = "evidenceupdate.asp?virtualid=" + VirtualID + "&base=tblCRC_CCA_Page44" + "&tag=" + TagId + "&editid="  + EditID ;
  document.getElementById("myform").submit();
}


// ]]>
</script>
</head>

<body>
 <form name="myform"  action="evidenceupdate.asp" method="post" target="myframe"> 
    <div  id="labelbanner">
        CRC written procedures  (4.4) 
    </div>
    
 <br />
<!-----  R E T U R N ---->
<button class="backbutton" onclick="window.location.href=<%="'page1.asp?id=" & TheID & "&net=" & TheNet  & "&phase=" & Current_Phase  & "'" %>" title="Return back main menu" ><< Back</button>

<%if TheNet="false" then %>
 <input class="completebutton" title="Assign these questions with a Completed status Tag " type="button" value="Complete" onclick="javascript:icomplete('4.4');" />
 <input class="completebutton" title="Not Applicable" type="button" value="N/A" onclick="javascript:NotApplicable('4.4');" />
<%end if%>
<!-----  R E T U R N ---->
 
<br />

<label class="messagelabel" id="myLabel"></label>
    <table >
        <tr>
            <td  class="One">
               Do you have any CRC procedures?
               </td>
            <td  >
                <select name="F1" class="Two" id="F1" >
					<%if continue=true then %>
					   <% if rComm("F1")="Yes" then %>
					       <option selected="selected" value="Yes">Yes</option> 
					       <option value="No">No</option>
					   <% Else %>
					       <option value="Yes">Yes</option>
					       <option selected="selected" value="No">No</option> 
					   <% End If %>
					<% else %>
<option selected value=""></option>
<option value="Yes">Yes</option>
<option value="No">No</option> 
					<% End If %>
                </select>
            </td>
            <td style="width: 53px"  >
            <input    type="text"  name="F1_Date"  style="<%=vis%>" value="<%=F1_Date%>" class="four" id="F1_Date"  /></td>
        </tr>
        
        <tr>
            <td  class="One" colspan="2">
               If yes, are any of these integrated into ISO9001 
              <br />
               or ISO14001management standards?<br />
               
                  <select name="F2" class="Two" id="F2">
                	<%if continue=true then %>
					   <% if rComm("F2")="Yes" then %>
					       <option selected="selected" value="Yes">Yes</option> 
					       <option value="No">No</option>
					   <% Else %>
					       <option value="Yes">Yes</option>
					       <option selected="selected" value="No">No</option> 
					   <% End If %>
					<% else %>
<option selected value=""></option>
<option value="Yes">Yes</option>
<option value="No">No</option> 
					<% End If %>
                </select>
                <br />
                
            <textarea id="F3" rows="6" cols="31" name="F3" class="three"><%=F3%>  </textarea>
                
                </td>
            <td style="width: 53px"  >
                <input id="F2_Date"   type="text"  style="<%=vis%>"  name="F2_Date"  value="<%=F2_Date%>"  class="four"  /></td>
        </tr>
        
        
        
        <tr>
            <td   class="One"  >If yes, date CRC procedures were last reviewed</td>
            <td  >
                <INPUT id="F4" class="four" type=text value="<%=F4%>" name="F4" />
            </td>
            <td   >
                 <input id="F4_Date"   type="text"  style="<%=vis%>" name="F4_Date"  value="<%=F4_Date%>" class="four"  /></td>
        </tr>
        <tr>
            <td   class="One"  >If yes, are procedures attached as an annex
            <br />
             to the evidence pack?</td>
            <td    >
                <select class="Two" name="F5" id="F5">
                	<%if continue=true then %>
					   <% if rComm("F5")="Yes" then %>
					       <option selected="selected" value="Yes">Yes</option> 
					       <option value="No">No</option>
					   <% Else %>
					       <option value="Yes">Yes</option>
					       <option selected="selected" value="No">No</option> 
					   <% End If %>
					<% else %>
<option selected value=""></option>
<option value="Yes">Yes</option>
<option value="No">No</option> 
					<% End If %>
                </select>
            </td>
            <td   >
                  <input id="F5_Date"   type="text" style="<%=vis%>" name="F5_Date"  value="<%=F5_Date%>" class="four"  /></td>
        </tr>
    </table>


<%if TheNet="false" then %>
 <INPUT type="button" value="Submit" id="button1" name="button1" onclick="javascript:updateme();" class="backbutton"/>
<%end if %>


<iframe id="myframe" name="myframe" src="blank2.htm" frameborder="no" scrolling="no"/>
 
 </form>
</body>
</html>
<%
 

 Call CloseConnection
 %> 