<!--#include file="uploadform.cls"-->
<HTML>
<HEAD>
<TITLE>Upload Control Test Form</TITLE>
<META NAME="Author" CONTENT="Daniel Morales Lira">
<script language="javascript" type="text/javascript">
<!--
 

// -->
</script>
</HEAD>

<BODY BGCOLOR="#FFFFFF" TEXT="#000000" LINK="#FF0000" VLINK="#800000" ALINK="#FF00FF" BACKGROUND="?">
<%

	NumberOfFiles = request.QueryString("files")
	if NumberOfFiles = "" then NumberOfFiles = 1

	set FormData = new UploadForm '//Initialize the Class'
	FormData.UploadPath = "c:\inetpub\wwwroot\test"
	if request.TotalBytes > 0 then '<
		FormData.BinData = request.BinaryRead(request.TotalBytes) '//Store All data into Class'
		FormData.Transform '//Activate data inside Class'
		FormData.SaveAll "" '//Save all the files in the form'
		'//BELOW ARE SOME OF THE METHODS AND PROPERTIES OF THE CLASS'
	%>
		<b>TextData:</b>&nbsp;<%=FormData.TextData%><br>
		<b>DataArray:</b>&nbsp;<%=join(FormData.DataArray,"@")%><br>
		<b>FieldCount:</b>&nbsp;<%=FormData.FieldCount%><br>
		<b>FileCount:</b>&nbsp;<%=FormData.FileCount%><br>
		<b>FieldData(File1):</b>&nbsp;<%=FormData.FieldData("file1")%><br>
		<b>FieldData(likes):</b>&nbsp;<%=FormData.FieldData("likes")%><br>
		<b>Filename(File1):</b>&nbsp;<%=FormData.FileName("file1",true)%><br>
		<b>FileData(File1):</b>&nbsp;<%=FormData.FileData("file1")%><br>
		<b>FileArray:</b>&nbsp;<%=join(FormData.FileArray,"@")%><br>
		<b>FieldArray:</b>&nbsp;<%=join(FormData.FieldArray,"@")%><br>
	<%end if%>
<center>
	<form method="post" enctype="multipart/form-data">
		<table border="0">
			<tr><td>Full Name</td><td><input type="text" name="name" size="30"></td></tr>
			<tr>
				<td>Country</td>
				<td>
					<select name="country">
						<option value="">Select your Country</option>
						<option value="Mexico">Mexico</option>
						<option value="United States">United States</option>
						<option value="Canada">Canada</option>
					</select>
				</td>
			</tr>
			<tr>
				<td>Gender</td>
				<td>
					<input type="radio" name="gender" value="Male">Male<br>
					<input type="radio" name="gender" value="Female">Female
				</td>
			</tr>
			<tr>
				<td>I like</td>
				<td>
					<input type="checkbox" name="likes" value="Music">Music<br>
					<input type="checkbox" name="likes" value="Movies">Movies<br>
					<input type="checkbox" name="likes" value="Cars">Cars<br>
					<input type="checkbox" name="likes" value="Books">Books<br>
					<input type="checkbox" name="likes" value="Computers">Computers<br>
				</td>
			</tr>
			<tr><td>Description of yourself</td><td><textarea name="description" cols="35" rows="5"></textarea></td></tr>
			<tr>
				<td>Number of Files to Upload</td>
				<td>
					<input type="text" id="numberoffiles" size="3" value="<%=NumberOfFiles%>">
					<input type="button" onclick="document.location='uploadtest.asp?files=' + document.getElementById('numberoffiles').value" value="Reload Form">
				</td>
			</tr>
			<%for i = 1 to NumberOfFiles%>
			<tr><td>Add your picture</td><td><input type="file" name="file<%=i%>" size="30" id="File1" language="javascript" onclick="return File1_onclick()"></td></tr>
			<%next%>
			<tr><td colspan="2"><center><input type="submit" name="submit" value="Save your info"></center></td></tr>
		</table>
	</form>
</center>
</BODY>
</HTML>
