<%

' Constants
adLongVarChar = 201
ForWriting = 2

' Global variables
Dim g_requestData
Dim g_requestDataStr

' Global Functions
Function BinaryToString(binData)
	Dim RST
	Dim LenBinary
	Dim retStr
	
	Set RST = CreateObject("ADODB.Recordset")
	lenBinary = LenB(binData)	
	if lenBinary > 0 Then
		RST.Fields.Append "CBin", adLongVarChar, lenBinary
		RST.Open
		RST.AddNew
		RST("CBin").AppendChunk binData
		RST.Update
		retStr = RST("CBin")
	End if
	Set RST = Nothing
	BinaryToString = retStr
End Function

' File upload extra functions
Function GetBoundryIndicator()
	Dim strBoundry
	Dim currentIndex
	Dim beginPos
	Dim endPos
	
	strBoundry = Request.ServerVariables ("HTTP_CONTENT_TYPE")
	currentIndex = InStr(1, strBoundry, "boundary=")
	beginPos = currentIndex + 8
	endPos = Len(strBoundry)	
	strBoundry = "--" & Right(strBoundry, endPos - beginPos)
	
	GetBoundryIndicator = strBoundry
End Function

Function GetRequestData()
	Dim requestData
	Dim requestDataStr
	
	requestData = Request.BinaryRead(Request.TotalBytes)
	requestDataStr = BinaryToString(requestData)	
	
	GetRequestData = requestDataStr
End Function

' Set global data
g_requestDataStr = GetRequestData()

' File upload functions
Function GetFileNamePath(elementName)
	Dim currentIndex
	Dim beginPos
	Dim endPos
	
	currentIndex = InStr(1, g_requestDataStr, "name=" & Chr(34) & elementName & Chr(34))
	beginPos = currentIndex + 7 + Len(elementName)
	
	currentIndex = InStr(beginPos, g_requestDataStr, "filename=")
	beginPos = currentIndex + 10
	
	currentIndex = InStr(beginPos, g_requestDataStr, Chr(34))
	endPos = currentIndex
	
	GetFileNamePath = Mid(g_requestDataStr, beginPos, endPos - beginPos)
End Function

Function GetFileName(elementName)
	Dim fileName
	Dim retStr
	Dim currentIndex
	
	fileName = GetFileNamePath(elementName)
	currentIndex = InStrRev(fileName, Chr(92))
	retStr = Mid(fileName, currentIndex + 1)
	
	GetFileName = retStr
End Function

Function GetFileType(elementName)
	Dim currentIndex
	Dim beginPos
	Dim endPos
	
	currentIndex = InStr(1, g_requestDataStr, "name=" & Chr(34) & elementName & Chr(34))
	beginPos = currentIndex + 7 + Len(elementName)
	
	currentIndex = InStr(beginPos, g_requestDataStr, "Content-Type:")
	beginPos = currentIndex + 14
	
	currentIndex = InStr(beginPos, g_requestDataStr, chr(13) & chr(10))
	endPos = currentIndex
	
	GetFileType = Mid(g_requestDataStr, beginPos, endPos - beginPos)	
End Function

Function GetFile(elementName)
	Dim currentIndex
	Dim beginPos
	Dim endPos
	
	currentIndex = InStr(1, g_requestDataStr, "name=" & Chr(34) & elementName & Chr(34))
	beginPos = currentIndex
	
	currentIndex = InStr(beginPos, g_requestDataStr, "Content-Type:")
	beginPos = currentIndex + 14
	
	currentIndex = InStr(beginPos, g_requestDataStr, chr(13) & chr(10))
	beginPos = currentIndex + 4
	
	currentIndex = InStr(beginPos, g_requestDataStr, GetBoundryIndicator())
	endPos = currentIndex
	
	GetFile = Mid(g_requestDataStr, beginPos, endPos - beginPos)	
End Function

' Extra global functions
Sub SaveFileToSystem(fileData, filePath)
	Dim fsObj
	Dim fileObj
	
	Set fsObj = CreateObject("Scripting.FileSystemObject")
	Set fileObj = fsObj.OpenTextFile(filePath, ForWriting, True)
	
	fileObj.Write fileData
	fileObj.Close
	
	Set fsObj = Nothing
	Set fileObj = Nothing
End Sub

Sub CreateServerFolder(path)
	Dim fsObj
	Dim fdr
	
	Set fsObj = CreateObject("Scripting.FileSystemObject")
	Set fdr = fsObj.CreateFolder(path)
	
	Set fsObj = Nothing
	Set fdr = Nothing
End Sub

%>
