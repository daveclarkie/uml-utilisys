<%
Dim TheID 
         TheID=request.QueryString("id")  

Dim TheNet
         TheNet=request.QueryString("net")  

Dim Current_Phase
         Current_Phase=SESSION("phase")  



Dim ReportPhase
         ReportPhase=Request.QueryString("RP")  
If ReportPhase="" then
           Current_Phase=SESSION("phase")  
Else
           Current_Phase=Request.QueryString("RP")  
End If



 Dim OnOff 
         if TheNet="true" then
               vis="Display:none;" 
         else
             vis="Display:;"  
        end if


 
 

%> 


<HTML>
<%
Public dComm  
Public rComm  
 
Public Function OpenConnection()
  Set dComm = CreateObject("ADODB.Connection")
      dComm.Open "Provider=SQLOLEDB.1;Data Source=uk-ed0-sqlcl-01.MCEG.local;Initial Catalog=UML_CMS;Integrated Security=SSPI;"
End Function

Public Function OpenRecordset(nSQL)
 Set rComm = CreateObject("ADODB.recordset")
     rComm.ActiveConnection = dComm
     rComm.CursorLocation = 3 ' 2
     rComm.CursorType = 3 '1
     rComm.LockType = 1 '3
     rComm.Source = nSQL
     rComm.Open
End Function

Public Function CloseConnection()
  Set dComm = Nothing
  Set rComm = Nothing
End Function


'=========================================================================================
strsql = "SELECT * FROM dbo.tblCRC_CCA_Page812 WHERE virtualID = " & TheID
strsql = strsql & " AND TagID='8.1.2'" & " AND phase=" & "'" & Current_Phase & "'"

Call OpenConnection
Call OpenRecordset(strsql)
if rComm.EOF = true then
   continue=false
   f1_Date=""
   f2_Date=""
   f3_Date=""
   f4_Date=""
   f1=""
   f2=""
   f3=""
   f4=""
   
   Response.Write "<script language=""javascript"" type=""text/javascript"">" & vbnewline
   Response.write "var VirtualID=" & TheID  & vbnewline
   Response.write "var EditID=" & "0"  & vbnewline
   Response.write "var TagId=" & "'" & "8.1.2" & "'" & vbnewline
   Response.Write "</script>" &  vbnewline
else
   continue=true
    F1_Date=trim(rcomm("F1_Date"))
    F2_Date=trim(rcomm("F2_Date"))
    F3_Date=trim(rcomm("F3_Date"))
    F4_Date=trim(rcomm("F4_Date"))

    F1=rcomm("F1")
    F2=rcomm("F2")
    F3=rcomm("F3")
    F4=rcomm("F4")
   
   Response.Write "<script language=""javascript"" type=""text/javascript"">" & vbnewline
   Response.write "var VirtualID=" & TheID  & vbnewline
   Response.write "var EditID=" & rComm("ID") & vbnewline
   Response.write "var TagId=" & "'" & "8.12" & "'" & vbnewline
   Response.Write "</script>" &  vbnewline
end if
'=========================================================================================
 %>
<head>
    <title>Page 8.1.2</title>
    <LINK REL=StyleSheet HREF="CCA_stylesheet.css" TYPE="text/css" >
<script type="text/javascript" src="mree.js"></script>
<script language="javascript" type="text/javascript">
// <!CDATA[

 function icomplete(what)
{
 var total=0
 document.getElementById("mylabel").innerText="";
    total = total + validate("F1_Date");
    total = total + validate("F2_Date"); 
    total = total + validate("F3_Date"); 
    total = total + validate("F4_Date"); 
    validateall(4,total,VirtualID,"8.1.2");
}


function NotApplicable()
{
 var d = new Date() 
 var dy =     PadDigits(  d.getFullYear()  , 4);
 var dm =    PadDigits(     d.getMonth() +1 , 2);
 var dd =      PadDigits(   d.getDate() , 2);

 document.getElementById("F1_Date").value=dd + "/" + dm + "/" + dy;
 document.getElementById("F1_Date").style.backgroundColor="white"; 
 document.getElementById("F2_Date").value=dd + "/" + dm + "/" + dy;
 document.getElementById("F2_Date").style.backgroundColor="white"; 
 document.getElementById("F3_Date").value=dd + "/" + dm + "/" + dy;
 document.getElementById("F3_Date").style.backgroundColor="white"; 
 document.getElementById("F4_Date").value=dd + "/" + dm + "/" + dy;
 document.getElementById("F4_Date").style.backgroundColor="white"; 
}

function updateme()
{
     document.getElementById("myform").action = "evidenceupdate.asp?virtualid=" + VirtualID + "&base=tblCRC_CCA_Page812" + "&tag=" + TagId + "&editid="  + EditID ;
     document.getElementById("myform").submit();
}


// ]]>
</script>
</head>
<body>
 <form name="myform"  action="evidenceupdate.asp" method="post" target="myframe"> 
     <div  id="labelbanner">
        Change of contacts/responsibilities for Officers (8.1.2)
    </div> 
 
 
 <br />
<!-----  R E T U R N ---->
  

 <button class="backbutton" onclick="window.location.href=<%="'page1.asp?id=" & TheID & "&net=" & TheNet  & "&phase=" & Current_Phase  & "'" %>" title="Return back main menu" ><< Back</button>

<%if TheNet="false" then %>
 <input class="completebutton" title="Assign these questions with a Completed status Tag " type="button" value="Complete" onclick="javascript:icomplete('8.1.2');" />
 <input class="completebutton" title="Not Applicable" type="button" value="N/A" onclick="javascript:NotApplicable('8.1.2');" />
<%end if%>
<!-----  R E T U R N ---->
<br />
 
<label class="messagelabel" id="myLabel"></label>


    <table  >
        <tr  >
            <td class="One" style="height: 21px"   >
                Change in primary,secondary or
                <br />
                 accountrepresentative contacts<br />
                <input id="F1"  type="text" style="width: 205px"  value="<%=F1%>" class="Two" name="F1" /></td>
            <td style="width: 76px; height: 21px"  ><input id="F1_Date" style="<%=vis%>"  type="text" class="four" name="F1_Date" value="<%=F1_Date%>" /></td>
        </tr>
        <tr >
            <td style="height: 26px" class="One"   >
                Date effective from<br />
                <input id="F2"  value="<%=F2%>" type="text" style="width: 205px" class="four" name="F2" /></td>
            <td style="height: 26px; width: 76px;"  >
              <input id="F2_Date"  type="text" class="four" style="<%=vis%>"  name="F2_Date" value="<%=F2_Date%>" /></td>
        </tr>
        <tr  >
            <td style="height: 26px" class="One"   >
                Name of new primary,secondary
                <br />
                 or other contactand CRC role<br />
                <textarea id="F3" rows="6" cols="31" class="three"  name="F3" ><%=F3%>  </textarea></td>
            <td style="height: 26px; width: 76px;"  >
               <input id="F3_Date"  type="text" class="four" style="<%=vis%>" name="F3_Date" value="<%=F3_Date%>" /></td>
        </tr>
        <tr >
            <td class="One"   >
                Evidence for transfer of responsibilities<br />
                <textarea id="F4"  rows="6" cols="31" name="F4" class="three"><%=F4%>  </textarea></td>
          
            <td style="width: 76px"  >
                 <input id="F4_Date"   type="text" class="four" style="<%=vis%>" name="F4_Date" value="<%=F4_Date%>"/></td>
        </tr>
    </table>
    
<!---   B R EA K One ------>
<br />


<!---   B R EA K Two ------>


<!---   B R EA K Three ------>
<br />

    <br />
<%if TheNet="false" then %>
 <INPUT type="button" value="Submit" id="button1" name="button1" onclick="javascript:updateme();" class="backbutton"/>
<%end if %>

<iframe id="myframe" name="myframe" src="blank2.htm" frameborder="no" scrolling="no"/>
</form>
</body>
</html>
