<%
Class UploadForm
	Public UploadPath '//Path to upload files to'
	Public BinData '//Store all binary date in this property'
	Public TextData '//Use this to get the complete Forms data after using the Transform method'

	Public DataArray
	Private adLongVarChar
	Private ForWriting

	Private Boundry '//Field Delimiter'

	'//INITIALIZE CLASS'
	Private Sub Class_Initialize()
		adLongVarChar = 201
		ForWriting = 2
	End Sub
	'//TERMINATE CLASS'
	Private Sub Class_Terminate()
	End Sub

	'//GET BOUNDARY CODE'
	Private Sub GetBoundry
		Boundry = Request.ServerVariables ("HTTP_CONTENT_TYPE")
		lngBoundryPos = instr(1,Boundry,"boundary=") + 8
		Boundry = "--" & right(Boundry,len(Boundry)-lngBoundryPos)
	End Sub

	'// GENERATE DATA ARRAY'
	Private Sub GetDataArray()
		locData = ""
		If not isEmpty(TextData) then
			lngCurrentBegin = instr(1,TextData,Boundry)
			lngCurrentEnd = instr(lngCurrentBegin + 1,TextData,Boundry) - 1
			do while lngCurrentEnd > 0 '<
				strData = mid(TextData,lngCurrentBegin, (lngCurrentEnd - lngCurrentBegin) + 1)
				locData = locData & strData & "/@@@/"
				lngCurrentBegin = lngCurrentEnd
				lngCurrentEnd = instr(lngCurrentBegin + 9 ,TextData,Boundry) - 1
			loop
			locData = left(locData,len(locData)-5)
			DataArray = split(locData,"/@@@/")
		End If
		'//OLD CODE//'if not isEmpty(TextData) then
		'//OLD CODE//'	strToSplit = TextData
		'//OLD CODE//'	strToSplit = Right(strToSplit,len(strToSplit)-len(Boundry))
		'//OLD CODE//'	strToSplit = Left(strToSplit,len(strToSplit)-(len(Boundry)+4))
	 	'//OLD CODE//'	GetDataArray = split(strToSplit, Boundry)
	 	'//OLD CODE//'end if
	End Sub

	'//SAVE RAW DATA TO A GIVEN FILE'
	Public Sub SaveRawData(xPath,xFilename)
		if xPath = "" then xPath = UploadPath
		Set fso = CreateObject("Scripting.FileSystemObject")
		Set f = fso.CreateTextFile(xPath & "\" & xFilename, True)
		f.Write cStr(TextData)
		Set f = nothing
		Set fso = nothing
	End Sub

	'//SAVE ALL THE FILES IN THE FORM TO THE SPECIFIED PATH'
	Public Sub SaveAll(xPath)
		if xPath = "" then xPath = UploadPath
		Set fso = CreateObject("Scripting.FileSystemObject")
		FileList = FileArray()
		for i = 0 to ubound(FileList)
			newName = FileName(FileList(i),false)
			if not (newName = "") then
				Set f = fso.OpenTextFile(xPath & "\" & newName, ForWriting, True)
				f.Write FileData(FileList(i))
				Set f = nothing
			end if
		next
		Set fso = nothing
	End Sub

	'//SAVE SELECTED FILE TO SELECTED PATH'
	Public Sub Save(FieldName,xPath,newFileName)
		if xPath = "" then xPath = UploadPath
		if newFileName = "" then newFileName = FileName(FieldName)
		Set fso = CreateObject("Scripting.FileSystemObject")
		Set f = fso.OpenTextFile(xPath & "\" & newFileName, ForWriting, True)
		f.Write FileData(FieldName)
		Set f = nothing
		Set fso = nothing
	End Sub


	'//TRANSFORM THE DATA IN THE FORM FOR INSIDE CLASS HANDLING'
	Public Sub Transform()
		Set RST = CreateObject("ADODB.Recordset")
		LenBinary = LenB(BinData)
		if LenBinary > 0 Then '<
			GetBoundry
			RST.Fields.Append "myBinary", adLongVarChar, LenBinary
			RST.Open
				RST.AddNew
					RST("myBinary").AppendChunk BinData
				RST.Update
			TextData = RST("myBinary")
		End if
		set RST = Nothing
		GetDataArray
	End Sub

	'//COUNT THE NUMBER OF FILES IN THE FORM'
	Public Function FileCount()
		for i = 0 to Ubound(DataArray)
			if instr(DataArray(i),"filename=") > 0 then '<
				FileCount = FileCount + 1
			end if
		next
	End Function

	'//COUNT THE NUMBER OF FIELDS IN THE FORM, INCLUDING FILE FIELDS'
	Public Function FieldCount()
		for i = 0 to ubound(DataArray)
			if instr(DataArray(i),"name=") > 0 then '<
				FieldCount = FieldCount + 1
			end if
		next
	End Function

	'// RETURN THE FILE DATA FOR A GIVEN FILE FIELDNAME'
	Public Function FileData(FieldName)
		strFieldData = FieldData(FieldName)
		lastChar = len(strFieldData) - 1
		lngBeginFieldData = instr(1,strFieldData,vbcrlf & vbcrlf)+4
		FileData = mid(strFieldData,lngBeginFieldData,lastChar-lngBeginFieldData)
	End Function

	'//RETURN THE DATA FOR A GIVEN FIELDNAME'
	Public Function FieldData(FieldName)
		FieldData = ""
		for i = 0 to Ubound(DataArray)
			curField = DataArray(i)
			lastChar = len(curField)
			lngNamePos = instr(1,curField,"name=" & chr(34) & FieldName & chr(34))
			if lngNamePos > 0 then '<
				strToLookFor = chr(34) & "; filename=" & chr(34) 'COMMENT"
				chkForFile = instr(curField, strToLookFor)
				if chkForFile = 0 then
					lngBeginFieldData = instr(lngNamePos,curField,vbcrlf & vbcrlf)+4
				else
					lngBeginFieldData = instr(lngNamePos,curField,strToLookFor)+3
				end if
				lngEndFieldData = lastChar-1
				FieldData = FieldData & mid(curField,lngBeginFieldData,lngEndFieldData-lngBeginFieldData) & ","
			end if

			'//OLD CODE//'strToLookFor = chr(13) & chr(10) & "Content-Disposition: form-data; name=" & chr(34) & FieldName & chr(34) '"
			'//OLD CODE//'StartData = instr(curfield,strToLookFor)
			'//OLD CODE//'if StartData > 0 then '<
			'//OLD CODE//'	if instr(curfield,"filename=" & chr(34)) > 0 and instr(curfield,"Content-Type: ") > 0 then '<
			'//OLD CODE//'		buff = 3
			'//OLD CODE//'		buff2 = 1
			'//OLD CODE//'	else
			'//OLD CODE//'		buff = 5
			'//OLD CODE//'		buff2 = 6
			'//OLD CODE//'	end if
			'//OLD CODE//'	FieldData = FieldData & mid(curfield,len(strToLookFor)+buff,len(curfield)-len(strToLookFor)-buff2) & ","
			'//OLD CODE//'end if
		next
		FieldData = left(FieldData,len(FieldData)-1)
	End Function

	'//RETURN THE FILENAME FOR A GIVEN FILE FIELDNAME, showFullPath = [TRUE/FALSE]'
	Public Function FileName(FieldName,showFullPath)
		strFieldData = FieldData(FieldName)
		FileName = Mid(strFieldData,11,instr(11,strFieldData,chr(34))-11)
		if not showFullPath then
			FileName = right(FileName,len(FileName)-instrrev(FileName,"\"))
		end if
	End Function

	'//RETURNS AN ARRAY OF ALL THE FIELDNAMES IN THE FORM'
	Public Function FieldArray()
		FieldArray = ""
		for i = 0 to ubound(DataArray)
			curData = DataArray(i)
			startName = instr(curData,"name=" & chr(34))
			if startName > 0 then '<
				FieldArray = FieldArray & mid(curData,startName+6,instr(startName+6,curData,chr(34)) - (startName+6)) & "/@@@/"
			end if
		next
		FieldArray = split(left(FieldArray,len(FieldArray)-5),"/@@@/")
	End Function

	'//RETURNS AN ARRAY OF ALL THE FILE FIELDNAMES IN THE FORM'
	Public Function FileArray()
		FileArray = ""
		for i = 0 to ubound(DataArray)
			curData = DataArray(i)
			if instr(curData,"filename=") > 0 then '<
				startName = instr(curData,"name=" & chr(34))
				if startName > 0 then '<
					FileArray = FileArray & mid(curData,startName+6,instr(startName+6,curData,chr(34)) - (startName+6)) & "/@@@/"
				end if
			end if
		next
		FileArray = split(left(FileArray,len(FileArray)-5),"/@@@/")
	End Function

End Class
%>
