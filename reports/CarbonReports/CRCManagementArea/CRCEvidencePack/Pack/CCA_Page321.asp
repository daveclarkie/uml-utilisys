
<%
Dim TheID 
         TheID=request.QueryString("id")  

Dim TheNet
         TheNet=request.QueryString("net")  

Dim Current_Phase
         Current_Phase=SESSION("phase")  


Dim ReportPhase
         ReportPhase=Request.QueryString("RP")  
If ReportPhase="" then
           Current_Phase=SESSION("phase")  
Else
           Current_Phase=Request.QueryString("RP")  
End If


Dim OnOff 
         if TheNet="true" then
               vis="Display:none;" 
         else
             vis="Display:;"  
        end if


 
         
%> 

 <html>  
 <%
Public dComm  
Public rComm  
 
Public Function OpenConnection()
  Set dComm = CreateObject("ADODB.Connection")
      dComm.Open "Provider=SQLOLEDB.1;Data Source=uk-ed0-sqlcl-01.MCEG.local;Initial Catalog=UML_CMS;Integrated Security=SSPI;"
End Function

Public Function OpenRecordset(nSQL)
 Set rComm = CreateObject("ADODB.recordset")
     rComm.ActiveConnection = dComm
     rComm.CursorLocation = 3 ' 2
     rComm.CursorType = 3 '1
     rComm.LockType = 1 '3
     rComm.Source = nSQL
     rComm.Open
End Function

Public Function CloseConnection()
  Set dComm = Nothing
  Set rComm = Nothing
End Function


'=========================================================================================
strsql = "SELECT * FROM dbo.tblCRC_CCA_Page321 WHERE virtualID = " & TheID
strsql = strsql & " AND TagID='3.2.1'" & " AND phase=" & "'" & Current_Phase & "'"

Call OpenConnection
Call OpenRecordset(strsql)
if rComm.EOF = true then
   continue=false
    f1_date=""
    f2_date=""
    f3_date=""
    f4_date=""
    f1=""
    f2=""
    f3="" 
    f4="" 
   
   Response.Write "<script language=""javascript"" type=""text/javascript"">" & vbnewline
   Response.write "var VirtualID=" & TheID & vbnewline
   Response.write "var EditID=" & "0"  & vbnewline
   Response.write "var TagId=" & "'" & "3.2.1" & "'" & vbnewline
   Response.Write "</script>" &  vbnewline
else
   continue=true
   f1_date=trim(rcomm("f1_date"))
   f2_date=trim(rcomm("f2_date"))
   f3_date=trim(rcomm("f3_date"))
    f4_date=trim(rcomm("f4_date"))
    f1=rcomm("f1")
    f2=rcomm("f2")
    f3=rcomm("f3")
   f4=rcomm("f4")
      
   Response.Write "<script language=""javascript"" type=""text/javascript"">" & vbnewline
   Response.write "var VirtualID=" & TheID & vbnewline
   Response.write "var EditID=" & rComm("ID") & vbnewline
   Response.write "var TagId=" & "'" & "3.2.1" & "'" & vbnewline
   Response.Write "</script>" &  vbnewline
end if
 
  
'=========================================================================================

 %>
 
<head>
    <title>Page One</title>
<LINK REL=StyleSheet HREF="CCA_stylesheet.css" TYPE="text/css" >
<script type="text/javascript" src="mree.js"></script>
<script language="javascript" type="text/javascript">

// <!CDATA[
function icomplete(what)
{
   var total=0
       document.getElementById("mylabel").innerText="";
        total = total + validate("F1_Date");
        total = total + validate("F2_Date"); 
        total = total + validate("F3_Date"); 
        total = total + validate("F3_Date"); 
        validateall(4,total,VirtualID,"3.2.1")
}


function NotApplicable()
{
 var d = new Date() 
 var dy =     PadDigits(  d.getFullYear()  , 4)
 var dm =    PadDigits(     d.getMonth() +1 , 2)
 var dd =      PadDigits(   d.getDate() , 2)

 document.getElementById("F1_Date").value=dd + "/" + dm + "/" + dy;
 document.getElementById("F1_Date").style.backgroundColor="white"; 
 document.getElementById("F2_Date").value=dd + "/" + dm + "/" + dy;
 document.getElementById("F2_Date").style.backgroundColor="white"; 
 document.getElementById("F3_Date").value=dd + "/" + dm + "/" + dy;
 document.getElementById("F3_Date").style.backgroundColor="white"; 
 document.getElementById("F4_Date").value=dd + "/" + dm + "/" + dy;
 document.getElementById("F4_Date").style.backgroundColor="white"; 

  //updateme() ;
}


function updateme()
{
  document.getElementById("myform").action = "evidenceupdate.asp?virtualid=" + VirtualID + "&base=tblCRC_CCA_Page321" + "&tag=" + TagId + "&editid="  + EditID ;
  document.getElementById("myform").submit();
}


// ]]>
</script>
</head>

<body>
 <form name="myform"  action="evidenceupdate.asp" method="post" target="myframe"> 
    <div  id="labelbanner">
       Description of structure and relationships  (3.2.1) 
    </div>
    
 <br />
<!-----  R E T U R N ---->
<button class="backbutton" onclick="window.location.href=<%="'page1.asp?id=" & TheID & "&net=" & TheNet  & "&phase=" & Current_Phase  & "'" %>" title="Return back main menu" ><< Back</button>


<%if TheNet="false" then %>
 <input class="completebutton" title="Assign these questions with a Completed status Tag " type="button" value="Complete" onclick="javascript:icomplete('3.2.1');" />
 <input class="completebutton" title="Not Applicable" type="button" value="N/A" onclick="javascript:NotApplicable('3.2.1');" />
<%end if%>
<!-----  R E T U R N ---->
 

<br />

<label class="messagelabel" id="myLabel"></label>
    <table >
        <tr>
            <td  class="One">
                The parent organisation type has been defined and justification is available</td>
            <td  >
                <select name="F1" class="Two" >
					<%if continue=true then %>
					   <% if rComm("F1")="Yes" then %>
					       <option selected value="Yes">Yes</option> 
					       <option value="No">No</option>
					   <% Else %>
					       <option value="Yes">Yes</option>
					       <option Selected value="No">No</option> 
					   <% End If %>
					<% else %>
<option selected value=""></option>
<option value="Yes">Yes</option>
<option value="No">No</option> 
					<% End If %>
                </select>
            </td>
            <td style="width: 53px"  >
            <input    type="text"  name="F1_Date"  style="<%=vis%>"  value="<%=F1_Date%>" class="four"  /></td>
        </tr>
        
        <tr>
            <td  class="One">
                    There is evidence that the primary member is part of the organisational group</td>
            <td >
               
                  <select name="F2" class="Two">
                	<%if continue=true then %>
					   <% if rComm("F2")="Yes" then %>
					       <option selected value="Yes">Yes</option> 
					       <option value="No">No</option>
					   <% Else %>
					       <option value="Yes">Yes</option>
					       <option Selected value="No">No</option> 
					   <% End If %>
					<% else %>
<option selected value=""></option>
<option value="Yes">Yes</option>
<option value="No">No</option> 
					<% End If %>
                </select>
                </td>
            <td style="width: 53px"  >
                <input id="Text4"   type="text" name="F2_Date"  style="<%=vis%>"  value="<%=F2_Date%>"  class="four"  /></td>
        </tr>
        
        
 
        <tr>
            <td   class="One"  >
                 There is justification for extent of the organisation or organisational group</td>
            <td  >
                 <select name="F3" class="Two">
                	<%if continue=true then %>
					   <% if rComm("F3")="Yes" then %>
					       <option selected value="Yes">Yes</option> 
					       <option value="No">No</option>
					   <% Else %>
					       <option value="Yes">Yes</option>
					       <option Selected value="No">No</option> 
					   <% End If %>
					<% else %>
<option selected value=""></option>
<option value="Yes">Yes</option>
<option value="No">No</option> 
					<% End If %>
                </select>
            </td>
            <td   >
                 <input id="Text5"   type="text" name="F3_Date" style="<%=vis%>"   value="<%=F3_Date%>" class="four"  /></td>
        </tr>
       
      
              <tr>
            <td   class="One"  >
                 If an overseas organisation with no UK subsidiaries, there 
                    is evidence of relationship to UK activities</td>
            <td  >
                 <select name="F4" class="Two">
                	<%if continue=true then %>
					   <% if rComm("F4")="Yes" then %>
					       <option selected value="Yes">Yes</option> 
					       <option value="No">No</option>
					   <% Else %>
					       <option value="Yes">Yes</option>
					       <option Selected value="No">No</option> 
					   <% End If %>
					<% else %>
<option selected value=""></option>
<option value="Yes">Yes</option>
<option value="No">No</option> 
					<% End If %>
                </select>
            </td>
            <td   >
                 <input id="Text1"   type="text" name="F4_Date"  style="<%=vis%>" value="<%=F4_Date%>" class="four"  /></td>
        </tr>
      
      
       
    </table>

<%if TheNet="false" then %>
 <INPUT type="button" value="Submit" id="button1" name="button1" onclick="javascript:updateme();" class="backbutton"/>
<%end if %> 

<iframe id="myframe" name="myframe" src="blank2.htm" frameborder="no" scrolling="no"/>
 
 </form>
</body>
</html>
<%
 

 Call CloseConnection
 %> 