<!--  <html> 

   <head>
        <LINK REL=StyleSheet HREF="CCA_stylesheet.css" TYPE="text/css" >
  </head>

  <body> -->
  <%
   Response.Expires = 0
   Response.Buffer = TRUE
   Response.Clear
 
  
Public dComm  
Public rComm  

Public Function OpenConnection()
	  Set dComm = CreateObject("ADODB.Connection")
	      dComm.Open "Provider=SQLOLEDB.1;Data Source=uk-ed0-sqlcl-01.MCEG.local;Initial Catalog=UML_CMS;Integrated Security=SSPI;"
End Function

Public Function OpenRecordset(nSQL)
 Set rComm = CreateObject("ADODB.recordset")
     rComm.ActiveConnection = dComm
     rComm.CursorLocation = 3 ' 2
     rComm.CursorType = 3 '1
     rComm.LockType = 1 '3
     rComm.Source = nSQL
     rComm.Open
End Function

Public Function CloseConnection()
  Set dComm = Nothing
  Set rComm = Nothing
End Function
 
function DateConverter(what)
	if trim(what)="" then
	   DateConverter="Null"
	else
       DateConverter="'" & what & "'"
	end if
end function 


function CheckDates(what)
OK=false 
nnsplit=split(what,",")
 for each ele in nnSPLIT
     if trim(ele)= "" then
        OK=true
        exit for
     end if
 next
 CheckDates=OK
end function

function xy(what)
  xy = replace(what,"'","''")
end function



Call OpenConnection

 'response.Write "Base name = " & Request.QueryString("Base")  & "<BR>"
 
select case Request.QueryString("Base") 




'============================================================================================
case "tblCRC_CCA_Page21"
 if  Request.QueryString("editid")=0 then
	strsql = ""
	strsql = "INSERT INTO dbo.tblCRC_CCA_Page21 "
	strsql = strsql  & " (phase,VirtualID,TagID,F1,F1_Date,F2,F2_Date,F3,F3_Date,F4,F4_Date,F5,F5_Date)"
    strsql = strsql  & " VALUES("
	strsql = strsql  & "'" & SESSION("phase") & "'" &  ","
	strsql = strsql  & Request.QueryString("virtualID") & ","   
	strsql = strsql  & "'" & Request.QueryString("tag") & "'" & ","
		
	strsql = strsql  & "'" & Request("F1") & "'" & ","
	strsql = strsql  & DateConverter(Request("F1_Date")) & ","
	 
	strsql = strsql  & "'" & Request("F2") & "'" & ","
	strsql = strsql  & DateConverter(Request("F2_Date")) & ","

	strsql = strsql  & "'" & Request("F3") & "'" & ","
	strsql = strsql  & DateConverter(Request("F3_Date")) & ","

	strsql = strsql  & "'" & xy(Request("F4")) & "'" & ","
	strsql = strsql  & DateConverter(Request("F4_Date")) & ","

	strsql = strsql  & "'" & xy(Request("F5")) & "'" & ","
	strsql = strsql  & DateConverter(Request("F5_Date")) 
	
    strsql = strsql  & ")"
 
 else
 
    strsql = ""
	strsql = "UPDATE dbo.tblCRC_CCA_Page21 "
	strsql = strsql  & " SET F1=" & "'" & trim(Request("F1")) & "'"  & ","
	strsql = strsql  & " F1_Date ="  & DateConverter(Request("F1_Date")) & ","
	 
	strsql = strsql  & " F2=" & "'" & Request("F2") & "'" & ","
	strsql = strsql  & " F2_Date="  & DateConverter(Request("F2_Date")) & ","

    strsql = strsql  & " F3=" & "'" & Request("F3") & "'" & ","
	strsql = strsql  & " F3_Date="  & DateConverter(Request("F3_Date")) & ","

	strsql = strsql  & " F4=" & "'" & xy(Request("F4")) & "'" & ","
	strsql = strsql  & " F4_Date=" & DateConverter(Request("F4_Date")) & ","

    strsql = strsql  & " F5=" & "'" & xy(Request("F5")) & "'" & ","
	strsql = strsql  & " F5_Date=" & DateConverter(Request("F5_Date"))  
	
	strsql = strsql  & " WHERE ID=" & Request.QueryString("editid")
	
	' Response.Write strsql & "<BR>"
   
 end if
 
 call OpenRecordset(StrSQL)
 
 STR=Request("F1_Date")& "," & Request("F2_Date")& "," & Request("F3_Date")& "," & Request("F4_Date")& "," & Request("F5_Date")  
 if CheckDates(STR)=True then
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='N'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 else
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='Y'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 end if
    
'============================================================================================
case "tblCRC_CCA_Page22"             
if  Request.QueryString("editid")=0 then
	strsql = ""
	strsql = "INSERT INTO dbo.tblCRC_CCA_Page22 "
	strsql = strsql  & " (phase,VirtualID,TagID,F1,F1_Date)"
    strsql = strsql  & " VALUES("
    strsql = strsql  & "'" & SESSION("phase") & "'" &  ","
	strsql = strsql  & Request.QueryString("virtualID") & ","
	strsql = strsql  & "'" & Request.QueryString("tag") & "'" & ","
	strsql = strsql  & "'" & Request("F1") & "'" & ","
	strsql = strsql  & DateConverter(Request("F1_Date"))  
    strsql = strsql  & ")"
 else
    strsql = ""
	strsql = "UPDATE dbo.tblCRC_CCA_Page22 "
	strsql = strsql  & " SET F1=" & "'" & trim(Request("F1")) & "'"  & ","
	strsql = strsql  & " F1_Date ="  & DateConverter(Request("F1_Date")) 
	strsql = strsql  & " WHERE ID=" & Request.QueryString("editid")
 end if
 
 call OpenRecordset(StrSQL)
 
 STR=Request("F1_Date")  
 if CheckDates(STR)=True then
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='N'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 else
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='Y'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 end if
'============================================================================================
case "tblCRC_CCA_Page23"
if  Request.QueryString("editid")=0 then
	strsql = ""
	strsql = "INSERT INTO dbo.tblCRC_CCA_Page23 "
	strsql = strsql  & " (phase,VirtualID,TagID,F1,F1_Date,F2,F2_Date,F3,F3_Date,F4,F4_Date,F5,F6,F7,F8 )"
    strsql = strsql  & " VALUES("
    strsql = strsql  & "'" & SESSION("phase") & "'" &  ","
	strsql = strsql  & Request.QueryString("virtualID") & ","
	strsql = strsql  & "'" & Request.QueryString("tag") & "'" & ","
	strsql = strsql  & "'" & Request("F1") & "'" & ","
	strsql = strsql  & DateConverter(Request("F1_Date"))  & ","
    strsql = strsql  & "'" & Request("F2") & "'" & ","
    strsql = strsql  & DateConverter(Request("F2_Date"))  & ","
    strsql = strsql  & "'" & Request("F3") & "'" & ","
    strsql = strsql  & DateConverter(Request("F3_Date"))  & ","
    strsql = strsql  & "'" & Request("F4") & "'" & ","
    strsql = strsql  & DateConverter(Request("F4_Date"))  & ","
    strsql = strsql  & "'" & Request("F5") & "'" & ","
    strsql = strsql  & "'" & Request("F6") & "'" & ","
    strsql = strsql  & "'" & Request("F7") & "'" & ","
    strsql = strsql  & "'" & Request("F8") & "'"  
    strsql = strsql  & ")"
 else
    strsql = ""
	strsql = "UPDATE dbo.tblCRC_CCA_Page23 "
	strsql = strsql  & " SET F1=" & "'" & trim(Request("F1")) & "'"  & ","
	strsql = strsql  & " F1_Date ="  & DateConverter(Request("F1_Date")) & ","
    strsql = strsql  & " F2=" & "'" & trim(Request("F2")) & "'"  & ","
    strsql = strsql  & " F2_Date ="  & DateConverter(Request("F2_Date")) & ","
    strsql = strsql  & " F3="  & "'" & trim(Request("F3")) & "'"  & ","
    strsql = strsql  & " F3_Date ="  & DateConverter(Request("F3_Date")) & ","
    strsql = strsql  & " F4=" & "'" & trim(Request("F4")) & "'"  & ","
    strsql = strsql  & " F4_Date ="  & DateConverter(Request("F4_Date")) & ","
    strsql = strsql  & " F5=" & "'" & trim(Request("F5")) & "'"  & ","
    strsql = strsql  & " F6=" & "'" & trim(Request("F6")) & "'"  & ","
    strsql = strsql  & " F7=" & "'" & trim(Request("F7")) & "'"  & ","
    strsql = strsql  & " F8=" & "'" & trim(Request("F8")) & "'"  
	strsql = strsql  & " WHERE ID=" & Request.QueryString("editid")
 end if
 
 call OpenRecordset(StrSQL)
 
 STR=Request("F1_Date")& "," & Request("F2_Date")& "," & Request("F3_Date")& "," & Request("F4_Date")   
 if CheckDates(STR)=True then
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='N'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 else
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='Y'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 end if

'=============================================================================================
case "tblCRC_CCA_Page31" 
if  Request.QueryString("editid")=0 then
	strsql = ""
	strsql = "INSERT INTO dbo.tblCRC_CCA_Page31 "
	strsql = strsql  & " (Phase,VirtualID,TagID,F1,F1_Date,F2,F2_Date,F3,F3_Date)"
    strsql = strsql  & " VALUES("
    	strsql = strsql  & "'" & SESSION("phase") & "'" &  ","
	strsql = strsql  & Request.QueryString("virtualID") & ","
	strsql = strsql  & "'" & Request.QueryString("tag") & "'" & ","
	strsql = strsql  & "'" & Request("F1") & "'" & ","
	strsql = strsql  & DateConverter(Request("F1_Date")) & ","
	strsql = strsql  & "'" & Request("F2") & "'" & ","
	strsql = strsql  & DateConverter(Request("F2_Date")) & ","
	strsql = strsql  & "'" & Request("F3") & "'" & ","
	strsql = strsql  & DateConverter(Request("F3_Date"))  
    strsql = strsql  & ")"
 else
    strsql = ""
	strsql = "UPDATE dbo.tblCRC_CCA_Page31 "
	strsql = strsql  & " SET F1=" & "'" & trim(Request("F1")) & "'"  & ","
	strsql = strsql  & " F1_Date ="  & DateConverter(Request("F1_Date")) & ","
	strsql = strsql  & " F2=" & "'" & Request("F2") & "'" & ","
	strsql = strsql  & " F2_Date="  & DateConverter(Request("F2_Date")) & ","
    strsql = strsql  & " F3=" & "'" & Request("F3") & "'" & ","
	strsql = strsql  & " F3_Date="  & DateConverter(Request("F3_Date")) 
	strsql = strsql  & " WHERE ID=" & Request.QueryString("editid")
 end if
 
 call OpenRecordset(StrSQL)
 
 STR=Request("F1_Date")& "," & Request("F2_Date")& "," & Request("F3_Date") 
 if CheckDates(STR)=True then
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='N'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 else
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='Y'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 end if

'============================================================================================
case "tblCRC_CCA_Page321"

if  Request.QueryString("editid")=0 then
	strsql = ""
	strsql = "INSERT INTO dbo.tblCRC_CCA_Page321 "
	strsql = strsql  & " (Phase,VirtualID,TagID,F1,F1_Date,F2,F2_Date,F3,F3_Date,F4,F4_Date)"
    strsql = strsql  & " VALUES("
    	strsql = strsql  & "'" & SESSION("phase") & "'" &  ","
	strsql = strsql  & Request.QueryString("virtualID") & ","
	strsql = strsql  & "'" & Request.QueryString("tag") & "'" & ","
	strsql = strsql  & "'" & Request("F1") & "'" & ","
	strsql = strsql  & DateConverter(Request("F1_Date")) & ","
	strsql = strsql  & "'" & Request("F2") & "'" & ","
	strsql = strsql  & DateConverter(Request("F2_Date")) & ","
	strsql = strsql  & "'" & Request("F3") & "'" & ","
	strsql = strsql  & DateConverter(Request("F3_Date"))  & ","
	strsql = strsql  & "'" & Request("F4") & "'" & ","
	strsql = strsql  & DateConverter(Request("F4_Date"))  
    strsql = strsql  & ")"
 else
    strsql = ""
	strsql = "UPDATE dbo.tblCRC_CCA_Page321 "
	strsql = strsql  & " SET F1=" & "'" & trim(Request("F1")) & "'"  & ","
	strsql = strsql  & " F1_Date ="  & DateConverter(Request("F1_Date")) & ","
	strsql = strsql  & " F2=" & "'" & Request("F2") & "'" & ","
	strsql = strsql  & " F2_Date="  & DateConverter(Request("F2_Date")) & ","
    strsql = strsql  & " F3=" & "'" & Request("F3") & "'" & ","
	strsql = strsql  & " F3_Date="  & DateConverter(Request("F3_Date")) & ","
    strsql = strsql  & " F4=" & "'" & Request("F4") & "'" & ","
	strsql = strsql  & " F4_Date="  & DateConverter(Request("F4_Date")) 
	strsql = strsql  & " WHERE ID=" & Request.QueryString("editid")
 end if
 
 call OpenRecordset(StrSQL)
 
 STR=Request("F1_Date") & "," & Request("F2_Date") & "," & Request("F3_Date") & "," & Request("F4_Date") 
 if CheckDates(STR)=True then
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='N'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 else
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='Y'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 end if

'============================================================================================
case "tblCRC_CCA_Page322"
if  Request.QueryString("editid")=0 then
	strsql = ""
	strsql = "INSERT INTO dbo.tblCRC_CCA_Page322 "
	strsql = strsql  & " (Phase,VirtualID,TagID,F1,F1_Date,F2,F2_Date,F3,F3_Date,F4,F4_Date,F5,F6)"
    strsql = strsql  & " VALUES("
    strsql = strsql  & "'" & SESSION("phase") & "'" &  ","
	strsql = strsql  & Request.QueryString("virtualID") & ","
	strsql = strsql  & "'" & Request.QueryString("tag") & "'" & ","
	strsql = strsql  & "'" & Request("F1") & "'" & ","
	strsql = strsql  & DateConverter(Request("F1_Date")) & ","
	strsql = strsql  & "'" & Request("F2") & "'" & ","
	strsql = strsql  & DateConverter(Request("F2_Date")) & ","
	strsql = strsql  & "'" & Request("F3") & "'" & ","
	strsql = strsql  & DateConverter(Request("F3_Date"))  & ","
	strsql = strsql  & "'" & Request("F4") & "'" & ","
	strsql = strsql  & DateConverter(Request("F4_Date"))  & ","
    strsql = strsql  & "'" & Request("F5") & "'" & ","
    strsql = strsql  & "'" & Request("F6") & "'"  
    strsql = strsql  & ")"
 else
    strsql = ""
	strsql = "UPDATE dbo.tblCRC_CCA_Page322 "
	strsql = strsql  & " SET F1=" & "'" & trim(Request("F1")) & "'"  & ","
	strsql = strsql  & " F1_Date ="  & DateConverter(Request("F1_Date")) & ","
	strsql = strsql  & " F2=" & "'" & Request("F2") & "'" & ","
	strsql = strsql  & " F2_Date="  & DateConverter(Request("F2_Date")) & ","
    strsql = strsql  & " F3=" & "'" & Request("F3") & "'" & ","
	strsql = strsql  & " F3_Date="  & DateConverter(Request("F3_Date")) & ","
    strsql = strsql  & " F4=" & "'" & Request("F4") & "'" & ","
	strsql = strsql  & " F4_Date="  & DateConverter(Request("F4_Date")) & ","
    strsql = strsql  & " F5=" & "'" & Request("F5") & "'" & ","
    strsql = strsql  & " F6=" & "'" & Request("F6") & "'" 
	strsql = strsql  & " WHERE ID=" & Request.QueryString("editid")
 end if
 
 call OpenRecordset(StrSQL)
 
 STR=Request("F1_Date") & "," & Request("F2_Date") & "," & Request("F3_Date") & "," & Request("F4_Date") 
 if CheckDates(STR)=True then
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='N'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 else
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='Y'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
  call OpenRecordset(StrSQL)
 end if


'============================================================================================
case "tblCRC_CCA_Page323"
if  Request.QueryString("editid")=0 then
	strsql = ""
	strsql = "INSERT INTO dbo.tblCRC_CCA_Page323 "
	strsql = strsql  & " (Phase,VirtualID,TagID,F1,F1_Date,F2,F2_Date,F3,F3_Date,F4,F4_Date,F5)"
    strsql = strsql  & " VALUES("
    strsql = strsql  & "'" & SESSION("phase") & "'" &  ","
	strsql = strsql  & Request.QueryString("virtualID") & ","
	strsql = strsql  & "'" & Request.QueryString("tag") & "'" & ","
	strsql = strsql  & "'" & Request("F1") & "'" & ","
	strsql = strsql  & DateConverter(Request("F1_Date")) & ","
	strsql = strsql  & "'" & Request("F2") & "'" & ","
	strsql = strsql  & DateConverter(Request("F2_Date")) & ","
	strsql = strsql  & "'" & Request("F3") & "'" & ","
	strsql = strsql  & DateConverter(Request("F3_Date"))  & ","
	strsql = strsql  & "'" & Request("F4") & "'" & ","
	strsql = strsql  & DateConverter(Request("F4_Date"))  & ","
    strsql = strsql  & "'" & Request("F5") & "'"  
    strsql = strsql  & ")"
 else
    strsql = ""
	strsql = "UPDATE dbo.tblCRC_CCA_Page323 "
	strsql = strsql  & " SET F1=" & "'" & trim(Request("F1")) & "'"  & ","
	strsql = strsql  & " F1_Date ="  & DateConverter(Request("F1_Date")) & ","
	strsql = strsql  & " F2=" & "'" & Request("F2") & "'" & ","
	strsql = strsql  & " F2_Date="  & DateConverter(Request("F2_Date")) & ","
    strsql = strsql  & " F3=" & "'" & Request("F3") & "'" & ","
	strsql = strsql  & " F3_Date="  & DateConverter(Request("F3_Date")) & ","
    strsql = strsql  & " F4=" & "'" & Request("F4") & "'" & ","
	strsql = strsql  & " F4_Date="  & DateConverter(Request("F4_Date")) & ","
    strsql = strsql  & " F5=" & "'" & Request("F5") & "'" 
	strsql = strsql  & " WHERE ID=" & Request.QueryString("editid")
 end if
 call OpenRecordset(StrSQL)
 
 STR=Request("F1_Date") & "," & Request("F2_Date") & "," & Request("F3_Date") & "," & Request("F4_Date") 
 if CheckDates(STR)=True then
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='N'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 else
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='Y'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
  call OpenRecordset(StrSQL)
 end if

'============================================================================================
case "tblCRC_CCA_Page41"
if  Request.QueryString("editid")=0 then
	strsql = ""
	strsql = "INSERT INTO dbo.tblCRC_CCA_Page41"
	strsql = strsql  & "  (Phase,VirtualID,TagID,F1,F1_Date,F2,F2_Date,F3,F3_Date,F4,F4_Date,F5,F5_Date,F6,F6_Date,F7,F7_Date,F8,F8_Date)"
    strsql = strsql  & "  VALUES("
    strsql = strsql  & "'" & SESSION("phase") & "'" &  ","
	strsql = strsql  & Request.QueryString("virtualID") & ","
	strsql = strsql  & "'" & Request.QueryString("tag") & "'" & ","
	strsql = strsql  & "'" & xy(Request("F1")) & "'" & ","
	strsql = strsql  & DateConverter(Request("F1_Date")) & ","
	strsql = strsql  & "'" & xy(Request("F2")) & "'" & ","
	strsql = strsql  & DateConverter(Request("F2_Date")) & ","
	strsql = strsql  & "'" & xy(Request("F3")) & "'" & ","
	strsql = strsql  & DateConverter(Request("F3_Date"))  & ","
	strsql = strsql  & "'" & xy(Request("F4")) & "'" & ","
	strsql = strsql  & DateConverter(Request("F4_Date"))  & ","
    strsql = strsql  & "'" & xy(Request("F5")) & "'" & ","
    strsql = strsql  & DateConverter(Request("F5_Date"))  & ","
    strsql = strsql  & "'" & xy(Request("F6")) & "'" & ","
    strsql = strsql  & DateConverter(Request("F6_Date"))  & ","
    strsql = strsql  & "'" & xy(Request("F7")) & "'" & ","
    strsql = strsql  & DateConverter(Request("F7_Date"))  & ","
    strsql = strsql  & "'" & xy(Request("F8")) & "'" & ","
    strsql = strsql  & DateConverter(Request("F8_Date"))  
    strsql = strsql  & ")"
 else
    strsql = ""
    strsql = "UPDATE dbo.tblCRC_CCA_Page41"
    strsql = strsql  & "  SET F1=" & "'" & xy( trim(Request("F1"))) & "'"  & ","
    strsql = strsql  & " F1_Date ="  & DateConverter(Request("F1_Date")) & ","
    strsql = strsql  & " F2=" & "'" & xy(Request("F2")) & "'" & ","
    strsql = strsql  & " F2_Date="  & DateConverter(Request("F2_Date")) & ","
    strsql = strsql  & " F3=" & "'" & xy(Request("F3")) & "'" & ","
    strsql = strsql  & " F3_Date="  & DateConverter(Request("F3_Date")) & ","
    strsql = strsql  & " F4=" & "'" & xy(Request("F4")) & "'" & ","
    strsql = strsql  & " F4_Date="  & DateConverter(Request("F4_Date")) & ","
    strsql = strsql  & " F5=" & "'" & xy(Request("F5")) & "'" & ","
    strsql = strsql  & " F5_Date ="  & DateConverter(Request("F5_Date")) & ","
    strsql = strsql  & " F6=" & "'" & xy(Request("F6")) & "'" & ","
    strsql = strsql  & " F6_Date ="  & DateConverter(Request("F6_Date")) & ","
    strsql = strsql  & " F7=" & "'" & xy(Request("F7")) & "'" & ","
    strsql = strsql  & " F7_Date ="  & DateConverter(Request("F7_Date")) & ","
    strsql = strsql  & " F8=" & "'" & xy(Request("F8")) & "'" & ","
    strsql = strsql  & " F8_Date ="  & DateConverter(Request("F8_Date"))  
	strsql = strsql  & " WHERE ID=" & Request.QueryString("editid")
 end if
 call OpenRecordset(StrSQL)
 
 STR=Request("F1_Date") & "," & Request("F2_Date") & "," & Request("F3_Date") & "," & Request("F4_Date") & "," & Request("F5_Date") & "," & Request("F6_Date") & "," & Request("F7_Date") & "," & Request("F8_Date") 
 if CheckDates(STR)=True then
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='N'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 else
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='Y'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 end if
'=============================================================================================
case "tblCRC_CCA_Page42"
if  Request.QueryString("editid")=0 then
	strsql = ""
	strsql = "INSERT INTO dbo.tblCRC_CCA_Page42"
	strsql = strsql  & "  (Phase,VirtualID,TagID,F1,F1_Date,F2,F2_Date,F3,F3_Date,F4,F4_Date,F5,F5_Date,F6,F6_Date,F7,F7_Date,F8,F8_Date , F9,F9_Date,F10,F10_Date,F11,F11_Date,F12,F12_Date,F13,F13_Date,F14,F14_Date,F15,F15_Date,F16,F16_Date , F17,F17_Date,F18,F18_Date,F19,F19_Date,F20,F20_Date,F21,F21_Date,F22,F22_Date,F23,F23_Date,F24,F24_Date , F25,F25_Date , F26,F26_Date,F27,F27_Date,F28,F28_Date,F29,F29_Date,F30,F30_Date,F31,F31_Date,F32,F32_Date)"
    strsql = strsql  & "  VALUES("
	strsql = strsql  & "'" & SESSION("phase") & "'" &  ","    
	strsql = strsql  & Request.QueryString("virtualID") & ","
	strsql = strsql  & "'" & Request.QueryString("tag") & "'" & ","
	strsql = strsql  & "'" & Request("F1") & "'" & ","
	strsql = strsql  & DateConverter(Request("F1_Date")) & ","
	strsql = strsql  & "'" & Request("F2") & "'" & ","
	strsql = strsql  & DateConverter(Request("F2_Date")) & ","
	strsql = strsql  & "'" & Request("F3") & "'" & ","
	strsql = strsql  & DateConverter(Request("F3_Date"))  & ","
	strsql = strsql  & "'" & Request("F4") & "'" & ","
	strsql = strsql  & DateConverter(Request("F4_Date"))  & ","
    strsql = strsql  & "'" & Request("F5") & "'" & ","
    strsql = strsql  & DateConverter(Request("F5_Date"))  & ","
    strsql = strsql  & "'" & Request("F6") & "'" & ","
    strsql = strsql  & DateConverter(Request("F6_Date"))  & ","
    strsql = strsql  & "'" & Request("F7") & "'" & ","
    strsql = strsql  & DateConverter(Request("F7_Date"))  & ","
    strsql = strsql  & "'" & Request("F8") & "'" & ","
    strsql = strsql  & DateConverter(Request("F8_Date"))  & ","
    
    strsql = strsql  & "'" & Request("F9") & "'" & ","
    strsql = strsql  & DateConverter(Request("F9_Date")) & ","
    strsql = strsql  & "'" & Request("F10") & "'" & ","
    strsql = strsql  & DateConverter(Request("F10_Date")) & ","
    strsql = strsql  & "'" & Request("F11") & "'" & ","
    strsql = strsql  & DateConverter(Request("F11_Date"))  & ","
    strsql = strsql  & "'" & Request("F12") & "'" & ","
    strsql = strsql  & DateConverter(Request("F12_Date"))  & ","
    strsql = strsql  & "'" & Request("F13") & "'" & ","
    strsql = strsql  & DateConverter(Request("F13_Date"))  & ","
    strsql = strsql  & "'" & Request("F14") & "'" & ","
    strsql = strsql  & DateConverter(Request("F14_Date"))  & ","
    strsql = strsql  & "'" & Request("F15") & "'" & ","
    strsql = strsql  & DateConverter(Request("F15_Date"))  & ","
    strsql = strsql  & "'" & Request("F16") & "'" & ","
    strsql = strsql  & DateConverter(Request("F16_Date"))  & ","
    
    
    strsql = strsql  & "'" & Request("F17") & "'" & ","
	strsql = strsql  & DateConverter(Request("F17_Date")) & ","
	strsql = strsql  & "'" & Request("F18") & "'" & ","
	strsql = strsql  & DateConverter(Request("F18_Date")) & ","
	strsql = strsql  & "'" & Request("F19") & "'" & ","
	strsql = strsql  & DateConverter(Request("F19_Date"))  & ","
	strsql = strsql  & "'" & Request("F20") & "'" & ","
	strsql = strsql  & DateConverter(Request("F20_Date"))  & ","
    strsql = strsql  & "'" & Request("F21") & "'" & ","
    strsql = strsql  & DateConverter(Request("F21_Date"))  & ","
    strsql = strsql  & "'" & Request("F22") & "'" & ","
    strsql = strsql  & DateConverter(Request("F22_Date"))  & ","
    strsql = strsql  & "'" & Request("F23") & "'" & ","
    strsql = strsql  & DateConverter(Request("F23_Date"))  & ","
    strsql = strsql  & "'" & Request("F24") & "'" & ","
    strsql = strsql  & DateConverter(Request("F24_Date"))  & ","
    
    
    	strsql = strsql  & "'" & Request("F25") & "'" & ","
	strsql = strsql  & DateConverter(Request("F25_Date")) & ","
	strsql = strsql  & "'" & Request("F26") & "'" & ","
	strsql = strsql  & DateConverter(Request("F26_Date")) & ","
	strsql = strsql  & "'" & Request("F27") & "'" & ","
	strsql = strsql  & DateConverter(Request("F27_Date"))  & ","
	strsql = strsql  & "'" & Request("F28") & "'" & ","
	strsql = strsql  & DateConverter(Request("F28_Date"))  & ","
    strsql = strsql  & "'" & Request("F29") & "'" & ","
    strsql = strsql  & DateConverter(Request("F29_Date"))  & ","
    strsql = strsql  & "'" & Request("F30") & "'" & ","
    strsql = strsql  & DateConverter(Request("F30_Date"))  & ","
    strsql = strsql  & "'" & Request("F31") & "'" & ","
    strsql = strsql  & DateConverter(Request("F31_Date"))  & ","
    strsql = strsql  & "'" & Request("F32") & "'" & ","
    strsql = strsql  & DateConverter(Request("F32_Date"))  
    strsql = strsql  & ")"
 else
    strsql = ""
    strsql = "UPDATE dbo.tblCRC_CCA_Page42"
    strsql = strsql  & "  SET F1=" & "'" & trim(Request("F1")) & "'"  & ","
    strsql = strsql  & " F1_Date ="  & DateConverter(Request("F1_Date")) & ","
    strsql = strsql  & " F2=" & "'" & Request("F2") & "'" & ","
    strsql = strsql  & " F2_Date="  & DateConverter(Request("F2_Date")) & ","
    strsql = strsql  & " F3=" & "'" & Request("F3") & "'" & ","
    strsql = strsql  & " F3_Date="  & DateConverter(Request("F3_Date")) & ","
    strsql = strsql  & " F4=" & "'" & Request("F4") & "'" & ","
    strsql = strsql  & " F4_Date="  & DateConverter(Request("F4_Date")) & ","
    strsql = strsql  & " F5=" & "'" & Request("F5") & "'" & ","
    strsql = strsql  & " F5_Date ="  & DateConverter(Request("F5_Date")) & ","
    strsql = strsql  & " F6=" & "'" & Request("F6") & "'" & ","
    strsql = strsql  & " F6_Date ="  & DateConverter(Request("F6_Date")) & ","
    strsql = strsql  & " F7=" & "'" & Request("F7") & "'" & ","
    strsql = strsql  & " F7_Date ="  & DateConverter(Request("F7_Date")) & ","
    strsql = strsql  & " F8=" & "'" & Request("F8") & "'" & ","
    strsql = strsql  & " F8_Date ="  & DateConverter(Request("F8_Date"))   & ","
    
    strsql = strsql  & " F9=" & "'" & trim(Request("F9")) & "'"  & ","
    strsql = strsql  & " F9_Date ="  & DateConverter(Request("F9_Date")) & ","
    strsql = strsql  & " F10=" & "'" & Request("F10") & "'" & ","
    strsql = strsql  & " F10_Date="  & DateConverter(Request("F10_Date")) & ","
    strsql = strsql  & " F11=" & "'" & Request("F11") & "'" & ","
    strsql = strsql  & " F11_Date="  & DateConverter(Request("F11_Date")) & ","
    strsql = strsql  & " F12=" & "'" & Request("F12") & "'" & ","
    strsql = strsql  & " F12_Date="  & DateConverter(Request("F12_Date")) & ","
    strsql = strsql  & " F13=" & "'" & Request("F13") & "'" & ","
    strsql = strsql  & " F13_Date ="  & DateConverter(Request("F13_Date")) & ","
    strsql = strsql  & " F14=" & "'" & Request("F14") & "'" & ","
    strsql = strsql  & " F14_Date ="  & DateConverter(Request("F14_Date")) & ","
    strsql = strsql  & " F15=" & "'" & Request("F15") & "'" & ","
    strsql = strsql  & " F15_Date ="  & DateConverter(Request("F15_Date")) & ","
    strsql = strsql  & " F16=" & "'" & Request("F16") & "'" & ","
    strsql = strsql  & " F16_Date ="  & DateConverter(Request("F16_Date")) & ","
    
    strsql = strsql  & " F17=" & "'" & trim(Request("F17")) & "'"  & ","
    strsql = strsql  & " F17_Date ="  & DateConverter(Request("F17_Date")) & ","
    strsql = strsql  & " F18=" & "'" & Request("F18") & "'" & ","
    strsql = strsql  & " F18_Date="  & DateConverter(Request("F18_Date")) & ","
    strsql = strsql  & " F19=" & "'" & Request("F19") & "'" & ","
    strsql = strsql  & " F19_Date="  & DateConverter(Request("F19_Date")) & ","
    strsql = strsql  & " F20=" & "'" & Request("F20") & "'" & ","
    strsql = strsql  & " F20_Date="  & DateConverter(Request("F20Date")) & ","
    strsql = strsql  & " F21=" & "'" & Request("F21") & "'" & ","
    strsql = strsql  & " F21_Date ="  & DateConverter(Request("F21_Date")) & ","
    strsql = strsql  & " F22=" & "'" & Request("F22") & "'" & ","
    strsql = strsql  & " F22_Date ="  & DateConverter(Request("F22_Date")) & ","
    strsql = strsql  & " F23=" & "'" & Request("F23") & "'" & ","
    strsql = strsql  & " F23_Date ="  & DateConverter(Request("F23_Date")) & ","
    strsql = strsql  & " F24=" & "'" & Request("F24") & "'" & ","
    strsql = strsql  & " F24_Date ="  & DateConverter(Request("F24_Date")) & ","
    
    strsql = strsql  & " F25=" & "'" & trim(Request("F25")) & "'"  & ","
    strsql = strsql  & " F25_Date ="  & DateConverter(Request("F25_Date")) & ","
    strsql = strsql  & " F26=" & "'" & Request("F26") & "'" & ","
    strsql = strsql  & " F26_Date="  & DateConverter(Request("F26_Date")) & ","
    strsql = strsql  & " F27=" & "'" & Request("F27") & "'" & ","
    strsql = strsql  & " F27_Date="  & DateConverter(Request("F27_Date")) & ","
    strsql = strsql  & " F28=" & "'" & Request("F28") & "'" & ","
    strsql = strsql  & " F28_Date="  & DateConverter(Request("F28_Date")) & ","
    strsql = strsql  & " F29=" & "'" & Request("F29") & "'" & ","
    strsql = strsql  & " F29_Date ="  & DateConverter(Request("F29_Date")) & ","
    strsql = strsql  & " F30=" & "'" & Request("F30") & "'" & ","
    strsql = strsql  & " F30_Date ="  & DateConverter(Request("F30_Date")) & ","
    strsql = strsql  & " F31=" & "'" & Request("F31") & "'" & ","
    strsql = strsql  & " F31_Date ="  & DateConverter(Request("F31_Date")) & ","
    strsql = strsql  & " F32=" & "'" & Request("F32") & "'" & ","
    strsql = strsql  & " F32_Date ="  & DateConverter(Request("F32_Date"))  
	strsql = strsql  & " WHERE ID=" & Request.QueryString("editid")
 end if
   call OpenRecordset(StrSQL)


 STR=Request("F1_Date") & "," & Request("F2_Date") & "," & Request("F3_Date") & "," & Request("F4_Date") & "," & Request("F5_Date") & "," & Request("F6_Date") & "," & Request("F7_Date") & "," & Request("F8_Date")  & "," &  Request("F9_Date") & "," & Request("F10_Date") & "," & Request("F11_Date") & "," & Request("F12_Date") & "," & Request("F13_Date") & "," & Request("F14_Date") & "," & Request("F15_Date") & "," & Request("F16_Date")   & "," &  Request("F17_Date") & "," & Request("F18_Date") & "," & Request("F19_Date") & "," & Request("F20_Date") & "," & Request("F21_Date") & "," & Request("F22_Date") & "," & Request("F23_Date") & "," & Request("F24_Date")   & "," & Request("F25_Date") & "," & Request("F26_Date") & "," & Request("F27_Date") & "," & Request("F28_Date") & "," & Request("F29_Date")   & "," &  Request("F30_Date") & "," & Request("F31_Date") & "," & Request("F32_Date") 
 if CheckDates(STR)=True then
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='N'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 else
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='Y'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
   call OpenRecordset(StrSQL)
 end if
'============================================================================================
case "tblCRC_CCA_Page43"
if  Request.QueryString("editid")=0 then
	strsql = ""
	strsql = "INSERT INTO dbo.tblCRC_CCA_Page43"
	strsql = strsql  & "  (Phase,VirtualID,TagID,F1,F1_Date)"
    strsql = strsql  & "  VALUES("
	strsql = strsql  & "'" & SESSION("phase") & "'" &  ","    
	strsql = strsql  & Request.QueryString("virtualID") & ","
	strsql = strsql  & "'" & Request.QueryString("tag") & "'" & ","
	strsql = strsql  & "'" & Request("F1") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F1_Date"))  
    strsql = strsql  & ")"
 else
    strsql = ""
    strsql = "UPDATE dbo.tblCRC_CCA_Page43"
    strsql = strsql  & "  SET F1=" & "'" & trim(Request("F1")) & "'"  & ","
    strsql = strsql  & " F1_Date ="  & DateConverter(Request("F1_Date"))  
	strsql = strsql  & " WHERE ID=" & Request.QueryString("editid")
 end if
 call OpenRecordset(StrSQL)
 
 STR=Request("F1_Date")  
 if CheckDates(STR)=True then
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='N'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 else
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='Y'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 end if
'============================================================================================
case "tblCRC_CCA_Page44" 
if  Request.QueryString("editid")=0 then
	strsql = ""
	strsql = "INSERT INTO dbo.tblCRC_CCA_Page44"
	strsql = strsql  & "  (Phase,VirtualID,TagID,F1,F1_Date,F2,F2_Date,F3,F4,F4_Date,F5,F5_Date)"
    strsql = strsql  & "  VALUES("
	strsql = strsql  & "'" & SESSION("phase") & "'" &  ","    
	strsql = strsql  & Request.QueryString("virtualID") & ","
	strsql = strsql  & "'" & Request.QueryString("tag") & "'" & ","
	strsql = strsql  & "'" & Request("F1") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F1_Date"))  & ","
	strsql = strsql  & "'" & Request("F2") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F2_Date"))  & ","
    strsql = strsql  & "'" & Request("F3") & "'"  & ","
	strsql = strsql  & "'" & Request("F4") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F4_Date"))   & ","
	strsql = strsql  & "'" & Request("F5") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F5_Date"))  
    strsql = strsql  & ")"
 else
    strsql = ""
    strsql = "UPDATE dbo.tblCRC_CCA_Page44"
    strsql = strsql  & "  SET F1=" & "'" & trim(Request("F1")) & "'"  & ","
    strsql = strsql  & " F1_Date ="  & DateConverter(Request("F1_Date"))  & ","
    strsql = strsql  & " F2= "  & "'" & Request("F2") & "'"   & ","
    strsql = strsql  & " F2_Date ="  & DateConverter(Request("F2_Date"))  & ","
    strsql = strsql  & " F3= "  & "'" & Request("F3") & "'"   & ","
    strsql = strsql  & " F4="  & "'" & Request("F4") & "'"   & ","
    strsql = strsql  & " F4_Date ="  & DateConverter(Request("F4_Date"))  & "," 
    strsql = strsql  & " F5="  & "'" & Request("F5") & "'"   & ","
    strsql = strsql  & " F5_Date ="  & DateConverter(Request("F5_Date"))   
	strsql = strsql  & " WHERE ID=" & Request.QueryString("editid")
 end if
'response.Write  strsql
call OpenRecordset(StrSQL)
 
 STR=Request("F1_Date")  & "," & Request("F2_Date") & "," & Request("F4_Date") & "," & Request("F5_Date")
 if CheckDates(STR)=True then
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='N'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 else
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='Y'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 end if

'============================================================================================
case "tblCRC_CCA_Page511"
if  Request.QueryString("editid")=0 then
	strsql = ""
	strsql = "INSERT INTO dbo.tblCRC_CCA_Page511"
	strsql = strsql  & "  (Phase,VirtualID,TagID,F1,F1_Date,F2,F2_Date,F3,F3_Date,F4,F4_Date,F5,F5_Date,F6,F6_Date,F7,F7_Date)"
    strsql = strsql  & "  VALUES("
	strsql = strsql  & "'" & SESSION("phase") & "'" &  ","    
	strsql = strsql  & Request.QueryString("virtualID") & ","
	strsql = strsql  & "'" & Request.QueryString("tag") & "'" & ","
	strsql = strsql  & "'" & Request("F1") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F1_Date"))  & ","
	strsql = strsql  & "'" & Request("F2") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F2_Date"))  & ","
    strsql = strsql  & "'" & Request("F3") & "'"  & ","
    strsql = strsql  & DateConverter(Request("F3_Date"))   & ","       	
	strsql = strsql  & "'" & Request("F4") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F4_Date"))   & ","
	strsql = strsql  & "'" & Request("F5") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F5_Date"))  & ","
	strsql = strsql  & "'" & Request("F6") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F6_Date"))  & ","
	strsql = strsql  & "'" & Request("F7") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F7_Date"))  
    strsql = strsql  & ")"
 else
    strsql = ""
    strsql = "UPDATE dbo.tblCRC_CCA_Page511"
    strsql = strsql  & "  SET F1=" & "'" & trim(Request("F1")) & "'"  & ","
    strsql = strsql  & " F1_Date ="  & DateConverter(Request("F1_Date"))  & ","
    strsql = strsql  & " F2= "  & "'" & Request("F2") & "'"   & ","
    strsql = strsql  & " F2_Date ="  & DateConverter(Request("F2_Date"))  & ","
    strsql = strsql  & " F3= "  & "'" & Request("F3") & "'"   & ","
    strsql = strsql  & " F3_Date ="  & DateConverter(Request("F3_Date"))  & "," 
    strsql = strsql  & " F4="  & "'" & Request("F4") & "'"   & ","
    strsql = strsql  & " F4_Date ="  & DateConverter(Request("F4_Date"))  & "," 
    strsql = strsql  & " F5="  & "'" & Request("F5") & "'"   & ","
    strsql = strsql  & " F5_Date ="  & DateConverter(Request("F5_Date"))   & "," 
    strsql = strsql  & " F6="  & "'" & Request("F6") & "'"   & ","
    strsql = strsql  & " F6_Date ="  & DateConverter(Request("F6_Date"))   & "," 
    strsql = strsql  & " F7="  & "'" & Request("F7") & "'"   & ","
    strsql = strsql  & " F7_Date ="  & DateConverter(Request("F7_Date"))       
	strsql = strsql  & " WHERE ID=" & Request.QueryString("editid")
 end if
'response.Write  strsql
call OpenRecordset(StrSQL)
 
 STR=Request("F1_Date")  & "," & Request("F2_Date") & "," & Request("F3_Date") & "," & Request("F4_Date") & "," & Request("F5_Date") & "," & Request("F6_Date") & "," & Request("F7_Date")
 if CheckDates(STR)=True then
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='N'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 else
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='Y'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 end if
'============================================================================================
case "tblCRC_CCA_Page512"
if  Request.QueryString("editid")=0 then
	strsql = ""
	strsql = "INSERT INTO dbo.tblCRC_CCA_Page512"
	strsql = strsql  & "  (Phase,VirtualID,TagID,F1,F1_Date,F2,F2_Date,F3,F3_Date,F4,F4_Date,F5,F5_Date )"
    strsql = strsql  & "  VALUES("
	strsql = strsql  & "'" & SESSION("phase") & "'" &  ","    
	strsql = strsql  & Request.QueryString("virtualID") & ","
	strsql = strsql  & "'" & Request.QueryString("tag") & "'" & ","
	strsql = strsql  & "'" & Request("F1") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F1_Date"))  & ","
	strsql = strsql  & "'" & Request("F2") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F2_Date"))  & ","
    strsql = strsql  & "'" & Request("F3") & "'"  & ","
    strsql = strsql  & DateConverter(Request("F3_Date"))   & ","       	
	strsql = strsql  & "'" & Request("F4") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F4_Date"))   & ","
	strsql = strsql  & "'" & Request("F5") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F5_Date"))   
    strsql = strsql  & ")"
 else
    strsql = ""
    strsql = "UPDATE dbo.tblCRC_CCA_Page512"
    strsql = strsql  & "  SET F1=" & "'" & trim(Request("F1")) & "'"  & ","
    strsql = strsql  & " F1_Date ="  & DateConverter(Request("F1_Date"))  & ","
    strsql = strsql  & " F2= "  & "'" & Request("F2") & "'"   & ","
    strsql = strsql  & " F2_Date ="  & DateConverter(Request("F2_Date"))  & ","
    strsql = strsql  & " F3= "  & "'" & Request("F3") & "'"   & ","
    strsql = strsql  & " F3_Date ="  & DateConverter(Request("F3_Date"))  & "," 
    strsql = strsql  & " F4="  & "'" & Request("F4") & "'"   & ","
    strsql = strsql  & " F4_Date ="  & DateConverter(Request("F4_Date"))  & "," 
    strsql = strsql  & " F5="  & "'" & Request("F5") & "'"   & ","
    strsql = strsql  & " F5_Date ="  & DateConverter(Request("F5_Date"))   
	strsql = strsql  & " WHERE ID=" & Request.QueryString("editid")
 end if
'response.Write  strsql
call OpenRecordset(StrSQL)
 
 STR=Request("F1_Date")  & "," & Request("F2_Date") & "," & Request("F3_Date") & "," & Request("F4_Date") & "," & Request("F5_Date")  
 if CheckDates(STR)=True then
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='N'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 else
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='Y'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 end if
'============================================================================================
case "tblCRC_CCA_Page513"
if  Request.QueryString("editid")=0 then
	strsql = ""
	strsql = "INSERT INTO dbo.tblCRC_CCA_Page513"
	strsql = strsql  & "  (Phase,VirtualID,TagID,F1,F1_Date,F2,F2_Date)"
    strsql = strsql  & "  VALUES("
	strsql = strsql  & "'" & SESSION("phase") & "'" &  ","    
	strsql = strsql  & Request.QueryString("virtualID") & ","
	strsql = strsql  & "'" & Request.QueryString("tag") & "'" & ","
	strsql = strsql  & "'" & Request("F1") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F1_Date"))  & ","
	strsql = strsql  & "'" & Request("F2") & "'"   & ","
 	strsql = strsql  & DateConverter(Request("F2_Date"))  
    strsql = strsql  & ")"
 else
    strsql = ""
    strsql = "UPDATE dbo.tblCRC_CCA_Page513"
    strsql = strsql  & "  SET F1=" & "'" & trim(Request("F1")) & "'"  & ","
    strsql = strsql  & " F1_Date ="  & DateConverter(Request("F1_Date"))  & ","
    strsql = strsql  & " F2= "  & "'" & Request("F2") & "'"   & ","
    strsql = strsql  & " F2_Date ="  & DateConverter(Request("F2_Date"))   
	strsql = strsql  & " WHERE ID=" & Request.QueryString("editid")
 end if
'response.Write  strsql
call OpenRecordset(StrSQL)
 
 STR=Request("F1_Date")  & "," & Request("F2_Date")  
 if CheckDates(STR)=True then
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='N'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 else
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='Y'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 end if

'============================================================================================
case "tblCRC_CCA_Page514"
if  Request.QueryString("editid")=0 then
	strsql = ""
	strsql = "INSERT INTO dbo.tblCRC_CCA_Page514"
	strsql = strsql  & "  (Phase,VirtualID,TagID,F1,F1_Date,F2,F2_Date,F3,F3_Date,F4,F4_Date,F5,F5_Date,F6,F6_Date,F7,F7_Date)"
    strsql = strsql  & "  VALUES("
	strsql = strsql  & "'" & SESSION("phase") & "'" &  ","    
	strsql = strsql  & Request.QueryString("virtualID") & ","
	strsql = strsql  & "'" & Request.QueryString("tag") & "'" & ","
	strsql = strsql  & "'" & Request("F1") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F1_Date"))  & ","
	strsql = strsql  & "'" & Request("F2") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F2_Date"))  & ","
    strsql = strsql  & "'" & Request("F3") & "'"  & ","
    strsql = strsql  & DateConverter(Request("F3_Date"))   & ","       	
	strsql = strsql  & "'" & Request("F4") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F4_Date"))   & ","
	strsql = strsql  & "'" & Request("F5") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F5_Date"))  & ","
	strsql = strsql  & "'" & Request("F6") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F6_Date"))  & ","
	strsql = strsql  & "'" & Request("F7") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F7_Date"))  
    strsql = strsql  & ")"
 else
    strsql = ""
    strsql = "UPDATE dbo.tblCRC_CCA_Page514"
    strsql = strsql  & "  SET F1=" & "'" & trim(Request("F1")) & "'"  & ","
    strsql = strsql  & " F1_Date ="  & DateConverter(Request("F1_Date"))  & ","
    strsql = strsql  & " F2= "  & "'" & Request("F2") & "'"   & ","
    strsql = strsql  & " F2_Date ="  & DateConverter(Request("F2_Date"))  & ","
    strsql = strsql  & " F3= "  & "'" & Request("F3") & "'"   & ","
    strsql = strsql  & " F3_Date ="  & DateConverter(Request("F3_Date"))  & "," 
    strsql = strsql  & " F4="  & "'" & Request("F4") & "'"   & ","
    strsql = strsql  & " F4_Date ="  & DateConverter(Request("F4_Date"))  & "," 
    strsql = strsql  & " F5="  & "'" & Request("F5") & "'"   & ","
    strsql = strsql  & " F5_Date ="  & DateConverter(Request("F5_Date"))   & "," 
    strsql = strsql  & " F6="  & "'" & Request("F6") & "'"   & ","
    strsql = strsql  & " F6_Date ="  & DateConverter(Request("F6_Date"))   & "," 
    strsql = strsql  & " F7="  & "'" & Request("F7") & "'"   & ","
    strsql = strsql  & " F7_Date ="  & DateConverter(Request("F7_Date"))       
	strsql = strsql  & " WHERE ID=" & Request.QueryString("editid")
 end if
'response.Write  strsql
call OpenRecordset(StrSQL)
 
 STR=Request("F1_Date")  & "," & Request("F2_Date") & "," & Request("F3_Date") & "," & Request("F4_Date") & "," & Request("F5_Date") & "," & Request("F6_Date") & "," & Request("F7_Date")
 if CheckDates(STR)=True then
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='N'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 else
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='Y'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 end if

'============================================================================================
case "tblCRC_CCA_Page515"
if  Request.QueryString("editid")=0 then
	strsql = ""
	strsql = "INSERT INTO dbo.tblCRC_CCA_Page515"
	strsql = strsql  & "  (Phase,VirtualID,TagID,F1,F1_Date,F2,F2_Date,F3,F3_Date,F4,F4_Date,F5,F5_Date,F6,F6_Date )"
    strsql = strsql  & "  VALUES("
	strsql = strsql  & "'" & SESSION("phase") & "'" &  ","    
	strsql = strsql  & Request.QueryString("virtualID") & ","
	strsql = strsql  & "'" & Request.QueryString("tag") & "'" & ","
	strsql = strsql  & "'" & Request("F1") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F1_Date"))  & ","
	strsql = strsql  & "'" & Request("F2") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F2_Date"))  & ","
    strsql = strsql  & "'" & Request("F3") & "'"  & ","
    strsql = strsql  & DateConverter(Request("F3_Date"))   & ","       	
	strsql = strsql  & "'" & Request("F4") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F4_Date"))   & ","
	strsql = strsql  & "'" & Request("F5") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F5_Date"))  & ","
	strsql = strsql  & "'" & Request("F6") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F6_Date"))   
 
    strsql = strsql  & ")"
 else
    strsql = ""
    strsql = "UPDATE dbo.tblCRC_CCA_Page515"
    strsql = strsql  & "  SET F1=" & "'" & trim(Request("F1")) & "'"  & ","
    strsql = strsql  & " F1_Date ="  & DateConverter(Request("F1_Date"))  & ","
    strsql = strsql  & " F2= "  & "'" & Request("F2") & "'"   & ","
    strsql = strsql  & " F2_Date ="  & DateConverter(Request("F2_Date"))  & ","
    strsql = strsql  & " F3= "  & "'" & Request("F3") & "'"   & ","
    strsql = strsql  & " F3_Date ="  & DateConverter(Request("F3_Date"))  & "," 
    strsql = strsql  & " F4="  & "'" & Request("F4") & "'"   & ","
    strsql = strsql  & " F4_Date ="  & DateConverter(Request("F4_Date"))  & "," 
    strsql = strsql  & " F5="  & "'" & Request("F5") & "'"   & ","
    strsql = strsql  & " F5_Date ="  & DateConverter(Request("F5_Date"))   & "," 
    strsql = strsql  & " F6="  & "'" & Request("F6") & "'"   & ","
    strsql = strsql  & " F6_Date ="  & DateConverter(Request("F6_Date")) 
       
	strsql = strsql  & " WHERE ID=" & Request.QueryString("editid")
 end if
'response.Write  strsql
call OpenRecordset(StrSQL)
 
 STR=Request("F1_Date")  & "," & Request("F2_Date") & "," & Request("F3_Date") & "," & Request("F4_Date") & "," & Request("F5_Date") & "," & Request("F6_Date")   
 if CheckDates(STR)=True then
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='N'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 else
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='Y'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 end if

'============================================================================================
case "tblCRC_CCA_Page516"
if  Request.QueryString("editid")=0 then
	strsql = ""
	strsql = "INSERT INTO dbo.tblCRC_CCA_Page516"
	strsql = strsql  & "  (Phase,VirtualID,TagID,F1,F1_Date,F2,F2_Date,F3,F3_Date,F4,F4_Date  )"
    strsql = strsql  & "  VALUES("
	strsql = strsql  & "'" & SESSION("phase") & "'" &  ","    
	strsql = strsql  & Request.QueryString("virtualID") & ","
	strsql = strsql  & "'" & Request.QueryString("tag") & "'" & ","
	strsql = strsql  & "'" & Request("F1") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F1_Date"))  & ","
	strsql = strsql  & "'" & Request("F2") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F2_Date"))  & ","
    strsql = strsql  & "'" & Request("F3") & "'"  & ","
    strsql = strsql  & DateConverter(Request("F3_Date"))   & ","       	
	strsql = strsql  & "'" & Request("F4") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F4_Date"))  
    strsql = strsql  & ")"
 else
    strsql = ""
    strsql = "UPDATE dbo.tblCRC_CCA_Page516"
    strsql = strsql  & "  SET F1=" & "'" & trim(Request("F1")) & "'"  & ","
    strsql = strsql  & " F1_Date ="  & DateConverter(Request("F1_Date"))  & ","
    strsql = strsql  & " F2= "  & "'" & Request("F2") & "'"   & ","
    strsql = strsql  & " F2_Date ="  & DateConverter(Request("F2_Date"))  & ","
    strsql = strsql  & " F3= "  & "'" & Request("F3") & "'"   & ","
    strsql = strsql  & " F3_Date ="  & DateConverter(Request("F3_Date"))  & "," 
    strsql = strsql  & " F4="  & "'" & Request("F4") & "'"   & ","
    strsql = strsql  & " F4_Date ="  & DateConverter(Request("F4_Date"))   
	strsql = strsql  & " WHERE ID=" & Request.QueryString("editid")
 end if
'response.Write  strsql
call OpenRecordset(StrSQL)
 
 STR=Request("F1_Date")  & "," & Request("F2_Date") & "," & Request("F3_Date") & "," & Request("F4_Date")  
 if CheckDates(STR)=True then
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='N'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 else
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='Y'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 end if

'============================================================================================
case "tblCRC_CCA_Page611"
if  Request.QueryString("editid")=0 then
	strsql = ""
	strsql = "INSERT INTO dbo.tblCRC_CCA_Page611"
	strsql = strsql  & "  (Phase,VirtualID,TagID,F1,F1_Date)"
    strsql = strsql  & "  VALUES("
	strsql = strsql  & "'" & SESSION("phase") & "'" &  ","    
	strsql = strsql  & Request.QueryString("virtualID") & ","
	strsql = strsql  & "'" & Request.QueryString("tag") & "'" & ","
	strsql = strsql  & "'" & Request("F1") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F1_Date"))   
    strsql = strsql  & ")"
 else
    strsql = ""
    strsql = "UPDATE dbo.tblCRC_CCA_Page611"
    strsql = strsql  & "  SET F1=" & "'" & trim(Request("F1")) & "'"  & ","
    strsql = strsql  & " F1_Date ="  & DateConverter(Request("F1_Date"))  
	strsql = strsql  & " WHERE ID=" & Request.QueryString("editid")
 end if
 
call OpenRecordset(StrSQL)
 
 STR=Request("F1_Date")   
 if CheckDates(STR)=True then
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='N'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 else
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='Y'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 end if


'============================================================================================
case "tblCRC_CCA_Page612" 
if  Request.QueryString("editid")=0 then
	strsql = ""
	strsql = "INSERT INTO dbo.tblCRC_CCA_Page612"
	strsql = strsql  & "  (Phase,VirtualID,TagID,F1,F1_Date,F2,F2_Date,F3,F3_Date)"
    strsql = strsql  & "  VALUES("
	strsql = strsql  & "'" & SESSION("phase") & "'" &  ","    
	strsql = strsql  & Request.QueryString("virtualID") & ","
	strsql = strsql  & "'" & Request.QueryString("tag") & "'" & ","
	strsql = strsql  & "'" & Request("F1") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F1_Date"))   & ","
    strsql = strsql  & "'" & Request("F2") & "'"  & ","
    strsql = strsql  & DateConverter(Request("F2_Date"))   & ","
    strsql = strsql  & "'" & Request("F3") & "'"  & ","
    strsql = strsql  & DateConverter(Request("F3_Date"))   
    strsql = strsql  & ")"
 else
    strsql = ""
    strsql = "UPDATE dbo.tblCRC_CCA_Page612"
    strsql = strsql  & "  SET F1=" & "'" & trim(Request("F1")) & "'"  & ","
    strsql = strsql  & " F1_Date ="  & DateConverter(Request("F1_Date"))  & ","
    strsql = strsql  & " F2="  &"'" & trim(Request("F2")) & "'"  & ","
    strsql = strsql  & " F2_Date ="  & DateConverter(Request("F2_Date"))  & ","
    strsql = strsql  & " F3="  &"'" & trim(Request("F3")) & "'"  & ","
    strsql = strsql  & " F3_Date ="  & DateConverter(Request("F3_Date"))  
	strsql = strsql  & " WHERE ID=" & Request.QueryString("editid")
 end if
 
call OpenRecordset(StrSQL)
 
 STR=Request("F1_Date")  & "," & Request("F2_Date")  & "," & Request("F3_Date")   
 if CheckDates(STR)=True then
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='N'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 else
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='Y'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 end if


'============================================================================================
case "tblCRC_CCA_Page621" 
if  Request.QueryString("editid")=0 then
	strsql = ""
	strsql = "INSERT INTO dbo.tblCRC_CCA_Page621"
	strsql = strsql  & "  (Phase,VirtualID,TagID,F1,F1_Date,F2,F2_Date )"
    strsql = strsql  & "  VALUES("
	strsql = strsql  & "'" & SESSION("phase") & "'" &  ","    
	strsql = strsql  & Request.QueryString("virtualID") & ","
	strsql = strsql  & "'" & Request.QueryString("tag") & "'" & ","
	strsql = strsql  & "'" & Request("F1") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F1_Date"))   & ","
    strsql = strsql  & "'" & Request("F2") & "'"  & ","
    strsql = strsql  & DateConverter(Request("F2_Date"))   
    strsql = strsql  & ")"
 else
    strsql = ""
    strsql = "UPDATE dbo.tblCRC_CCA_Page621"
    strsql = strsql  & "  SET F1=" & "'" & trim(Request("F1")) & "'"  & ","
    strsql = strsql  & " F1_Date ="  & DateConverter(Request("F1_Date"))  & ","
    strsql = strsql  & " F2="  &"'" & trim(Request("F2")) & "'"  & ","
    strsql = strsql  & " F2_Date ="  & DateConverter(Request("F2_Date")) 
	strsql = strsql  & " WHERE ID=" & Request.QueryString("editid")
 end if
 
call OpenRecordset(StrSQL)
 
 STR=Request("F1_Date")  & "," & Request("F2_Date")   
 if CheckDates(STR)=True then
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='N'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 else
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='Y'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 end if

'============================================================================================
case "tblCRC_CCA_Page63" 
if  Request.QueryString("editid")=0 then
	strsql = ""
	strsql = "INSERT INTO dbo.tblCRC_CCA_Page63"
	strsql = strsql  & "  (Phase,VirtualID,TagID,F1,F1_Date  )"
    strsql = strsql  & "  VALUES("
	strsql = strsql  & "'" & SESSION("phase") & "'" &  ","    
	strsql = strsql  & Request.QueryString("virtualID") & ","
	strsql = strsql  & "'" & Request.QueryString("tag") & "'" & ","
	strsql = strsql  & "'" & Request("F1") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F1_Date"))    
    strsql = strsql  & ")"
 else
    strsql = ""
    strsql = "UPDATE dbo.tblCRC_CCA_Page63"
    strsql = strsql  & "  SET F1=" & "'" & trim(Request("F1")) & "'"  & ","
    strsql = strsql  & " F1_Date ="  & DateConverter(Request("F1_Date"))   
	strsql = strsql  & " WHERE ID=" & Request.QueryString("editid")
 end if
 
call OpenRecordset(StrSQL)
 
 STR=Request("F1_Date") 
 if CheckDates(STR)=True then
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='N'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 else
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='Y'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 end if

'============================================================================================
case "tblCRC_CCA_Page64" 
if  Request.QueryString("editid")=0 then
	strsql = ""
	strsql = "INSERT INTO dbo.tblCRC_CCA_Page64"
	strsql = strsql  & "  (Phase,VirtualID,TagID,F1,F1_Date  )"
    strsql = strsql  & "  VALUES("
	strsql = strsql  & "'" & SESSION("phase") & "'" &  ","    
	strsql = strsql  & Request.QueryString("virtualID") & ","
	strsql = strsql  & "'" & Request.QueryString("tag") & "'" & ","
	strsql = strsql  & "'" & Request("F1") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F1_Date"))    
    strsql = strsql  & ")"
 else
    strsql = ""
    strsql = "UPDATE dbo.tblCRC_CCA_Page64"
    strsql = strsql  & "  SET F1=" & "'" & trim(Request("F1")) & "'"  & ","
    strsql = strsql  & " F1_Date ="  & DateConverter(Request("F1_Date"))   
	strsql = strsql  & " WHERE ID=" & Request.QueryString("editid")
 end if
 
call OpenRecordset(StrSQL)
 
 STR=Request("F1_Date") 
 if CheckDates(STR)=True then
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='N'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 else
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='Y'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 end if


'============================================================================================
case "tblCRC_CCA_Page65" 
if  Request.QueryString("editid")=0 then
	strsql = ""
	strsql = "INSERT INTO dbo.tblCRC_CCA_Page65"
	strsql = strsql  & "  (Phase,VirtualID,TagID,F1,F1_Date,F2,F2_Date,F3,F3_Date,F4,F4_Date,F5,F5_Date  )"
    strsql = strsql  & "  VALUES("
	strsql = strsql  & "'" & SESSION("phase") & "'" &  ","    
	strsql = strsql  & Request.QueryString("virtualID") & ","
	strsql = strsql  & "'" & Request.QueryString("tag") & "'" & ","
	strsql = strsql  & "'" & Request("F1") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F1_Date"))    & ","
    strsql = strsql  & "'" & Request("F2") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F2_Date"))    & ","
	strsql = strsql  & "'" & Request("F3") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F3_Date"))   & "," 
	strsql = strsql  & "'" & Request("F4") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F4_Date"))    & ","
	strsql = strsql  & "'" & Request("F5") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F5_Date"))    
    strsql = strsql  & ")"
 else
    strsql = ""
    strsql = "UPDATE dbo.tblCRC_CCA_Page65"
    strsql = strsql  & "  SET F1=" & "'" & trim(Request("F1")) & "'"  & ","
    strsql = strsql  & " F1_Date ="  & DateConverter(Request("F1_Date"))  & ","
    strsql = strsql  & " F2=" & "'" & Request("F2") & "'"  & ","
    strsql = strsql  & " F2_Date ="  & DateConverter(Request("F2_Date"))  & ","
    strsql = strsql  & " F3=" &"'" & Request("F3") & "'"  & ","
    strsql = strsql  & " F3_Date ="  & DateConverter(Request("F3_Date"))  & ","
    strsql = strsql  & " F4=" & "'" & Request("F4") & "'"  & ","
    strsql = strsql  & " F4_Date ="  & DateConverter(Request("F4_Date"))  & ","
    strsql = strsql  & " F5=" &"'" & Request("F5") & "'"  & ","
    strsql = strsql  & " F5_Date ="  & DateConverter(Request("F5_Date"))  
	strsql = strsql  & " WHERE ID=" & Request.QueryString("editid")
 end if

call OpenRecordset(StrSQL)
 
 STR=Request("F1_Date") & "," & Request("F2_Date") & "," & Request("F3_Date") & "," & Request("F4_Date") & "," & Request("F5_Date")
 
 if CheckDates(STR)=True then
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='N'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 else
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='Y'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 end if
'============================================================================================
case "tblCRC_CCA_Page66" 
if  Request.QueryString("editid")=0 then
	strsql = ""
	strsql = "INSERT INTO dbo.tblCRC_CCA_Page66"
	strsql = strsql  & "  (Phase,VirtualID,TagID,F1,F1_Date,F2,F2_Date,F3,F3_Date,F4,F4_Date,F5,F5_Date,F6,F6_Date,F7,F7_Date,F8,F8_Date   )"
    strsql = strsql  & "  VALUES("
	strsql = strsql  & "'" & SESSION("phase") & "'" &  ","    
	strsql = strsql  & Request.QueryString("virtualID") & ","
	strsql = strsql  & "'" & Request.QueryString("tag") & "'" & ","
	strsql = strsql  & "'" & Request("F1") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F1_Date"))    & ","
    strsql = strsql  & "'" & Request("F2") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F2_Date"))    & ","
	strsql = strsql  & "'" & Request("F3") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F3_Date"))   & "," 
	strsql = strsql  & "'" & Request("F4") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F4_Date"))    & ","
	strsql = strsql  & "'" & Request("F5") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F5_Date"))    & ","
	strsql = strsql  & "'" & Request("F6") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F6_Date"))    & ","
	strsql = strsql  & "'" & Request("F7") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F7_Date"))   & ","
	strsql = strsql  & "'" & Request("F8") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F8_Date"))   
    strsql = strsql  & ")"
 else
    strsql = ""
    strsql = "UPDATE dbo.tblCRC_CCA_Page66"
    strsql = strsql  & "  SET F1=" & "'" & trim(Request("F1")) & "'"  & ","
    strsql = strsql  & " F1_Date ="  & DateConverter(Request("F1_Date"))  & ","
    strsql = strsql  & " F2=" & "'" & Request("F2") & "'"  & ","
    strsql = strsql  & " F2_Date ="  & DateConverter(Request("F2_Date"))  & ","
    strsql = strsql  & " F3=" &"'" & Request("F3") & "'"  & ","
    strsql = strsql  & " F3_Date ="  & DateConverter(Request("F3_Date"))  & ","
    strsql = strsql  & " F4=" & "'" & Request("F4") & "'"  & ","
    strsql = strsql  & " F4_Date ="  & DateConverter(Request("F4_Date"))  & ","
    strsql = strsql  & " F5=" &"'" & Request("F5") & "'"  & ","
    strsql = strsql  & " F5_Date ="  & DateConverter(Request("F5_Date"))  & ","
    strsql = strsql  & " F6=" & "'" & Request("F6") & "'"  & ","
    strsql = strsql  & " F6_Date ="  & DateConverter(Request("F6_Date"))  & ","
    strsql = strsql  & " F7=" &"'" & Request("F7") & "'"  & ","
    strsql = strsql  & " F7_Date ="  & DateConverter(Request("F7_Date"))  & ","
    strsql = strsql  & " F8=" &"'" & Request("F8") & "'"  & ","
    strsql = strsql  & " F8_Date ="  & DateConverter(Request("F8_Date"))   
	strsql = strsql  & " WHERE ID=" & Request.QueryString("editid")
 end if

call OpenRecordset(StrSQL)
 
 STR=Request("F1_Date") & "," & Request("F2_Date") & "," & Request("F3_Date") & "," & Request("F4_Date") & "," & Request("F5_Date") & "," & Request("F6_Date") & "," & Request("F7_Date") & "," & Request("F8_Date")
 if CheckDates(STR)=True then
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='N'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 else
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='Y'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 end if
'============================================================================================
case "tblCRC_CCA_Page71" 
if  Request.QueryString("editid")=0 then
	strsql = ""
	strsql = "INSERT INTO dbo.tblCRC_CCA_Page71"
	strsql = strsql  & "  (Phase,VirtualID,TagID,F1,F1_Date,F2,F2_Date)"
    strsql = strsql  & "  VALUES("
	strsql = strsql  & "'" & SESSION("phase") & "'" &  ","    
	strsql = strsql  & Request.QueryString("virtualID") & ","
	strsql = strsql  & "'" & Request.QueryString("tag") & "'" & ","
	strsql = strsql  & "'" & Request("F1") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F1_Date"))    & ","
    strsql = strsql  & "'" & Request("F2") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F2_Date"))   
    strsql = strsql  & ")"
 else
    strsql = ""
    strsql = "UPDATE dbo.tblCRC_CCA_Page71"
    strsql = strsql  & "  SET F1=" & "'" & trim(Request("F1")) & "'"  & ","
    strsql = strsql  & " F1_Date ="  & DateConverter(Request("F1_Date"))  & ","
    strsql = strsql  & " F2=" & "'" & Request("F2") & "'"  & ","
    strsql = strsql  & " F2_Date ="  & DateConverter(Request("F2_Date")) 
	strsql = strsql  & " WHERE ID=" & Request.QueryString("editid")
 end if

call OpenRecordset(StrSQL)
 
 STR=Request("F1_Date") & "," & Request("F2_Date") 
 if CheckDates(STR)=True then
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='N'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 else
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='Y'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 end if
'==========================================================
case "tblCRC_CCA_Page721" 
if  Request.QueryString("editid")=0 then
	strsql = ""
	strsql = "INSERT INTO dbo.tblCRC_CCA_Page721"
	strsql = strsql  & "  (Phase,VirtualID,TagID,F1,F1_Date,F2,F2_Date,F3,F3_Date,F4,F4_Date)"
    strsql = strsql  & "  VALUES("
	strsql = strsql  & "'" & SESSION("phase") & "'" &  ","    
	strsql = strsql  & Request.QueryString("virtualID") & ","
	strsql = strsql  & "'" & Request.QueryString("tag") & "'" & ","
	strsql = strsql  & "'" & Request("F1") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F1_Date"))  & ","
	strsql = strsql  & "'" & Request("F2") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F2_Date"))  & ","
	strsql = strsql  & "'" & Request("F3") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F3_Date"))  & ","
	strsql = strsql  & "'" & Request("F4") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F4_Date"))  
	strsql = strsql  & ")"
 else
    strsql = ""
    strsql = "UPDATE dbo.tblCRC_CCA_Page721"
    strsql = strsql  & "  SET F1=" & "'" & trim(Request("F1")) & "'"  & ","
    strsql = strsql  & " F1_Date ="  & DateConverter(Request("F1_Date"))   & ","
    strsql = strsql  & " F2=" & "'" & Request("F2") & "'"  & ","
    strsql = strsql  & " F2_Date ="  & DateConverter(Request("F2_Date")) & ","
    strsql = strsql  & " F3=" & "'" & Request("F3") & "'"  & ","
    strsql = strsql  & " F3_Date ="  & DateConverter(Request("F3_Date")) & ","
    strsql = strsql  & " F4=" & "'" & Request("F4") & "'"   & ","
    strsql = strsql  & " F4_Date ="  & DateConverter(Request("F4_Date"))  
	strsql = strsql  & " WHERE ID=" & Request.QueryString("editid")
 end if

 call OpenRecordset(StrSQL)
 
 STR=Request("F1_Date")  & "," & Request("F2_Date") & "," & Request("F3_Date")   & "," & Request("F4_Date")  
 if CheckDates(STR)=True then
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='N'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 else
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='Y'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 end if

'============================================================================================
case "tblCRC_CCA_Page7210" 
if  Request.QueryString("editid")=0 then
	strsql = ""
	strsql = "INSERT INTO dbo.tblCRC_CCA_Page7210"
	strsql = strsql  & "  (Phase,VirtualID,TagID,F1,F1_Date)"
    strsql = strsql  & "  VALUES("
	strsql = strsql  & "'" & SESSION("phase") & "'" &  ","    
	strsql = strsql  & Request.QueryString("virtualID") & ","
	strsql = strsql  & "'" & Request.QueryString("tag") & "'" & ","
	strsql = strsql  & "'" & Request("F1") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F1_Date")) 
    strsql = strsql  & ")"
 else
    strsql = ""
    strsql = "UPDATE dbo.tblCRC_CCA_Page7210"
    strsql = strsql  & "  SET F1=" & "'" & trim(Request("F1")) & "'"  & ","
    strsql = strsql  & " F1_Date ="  & DateConverter(Request("F1_Date")) 
	strsql = strsql  & " WHERE ID=" & Request.QueryString("editid")
 end if

call OpenRecordset(StrSQL)
 
 STR=Request("F1_Date")  
 if CheckDates(STR)=True then
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='N'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 else
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='Y'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 end if

'============================================================================================
case "tblCRC_CCA_Page7211" 
if  Request.QueryString("editid")=0 then
	strsql = ""
	strsql = "INSERT INTO dbo.tblCRC_CCA_Page7211"
	strsql = strsql  & "  (Phase,VirtualID,TagID,F1,F1_Date)"
    strsql = strsql  & "  VALUES("
	strsql = strsql  & "'" & SESSION("phase") & "'" &  ","    
	strsql = strsql  & Request.QueryString("virtualID") & ","
	strsql = strsql  & "'" & Request.QueryString("tag") & "'" & ","
	strsql = strsql  & "'" & Request("F1") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F1_Date")) 
    strsql = strsql  & ")"
 else
    strsql = ""
    strsql = "UPDATE dbo.tblCRC_CCA_Page7211"
    strsql = strsql  & "  SET F1=" & "'" & trim(Request("F1")) & "'"  & ","
    strsql = strsql  & " F1_Date ="  & DateConverter(Request("F1_Date")) 
	strsql = strsql  & " WHERE ID=" & Request.QueryString("editid")
 end if

call OpenRecordset(StrSQL)
 
 STR=Request("F1_Date")  
 if CheckDates(STR)=True then
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='N'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 else
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='Y'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 end if

'============================================================================================
case "tblCRC_CCA_Page722" 
if  Request.QueryString("editid")=0 then
	strsql = ""
	strsql = "INSERT INTO dbo.tblCRC_CCA_Page722"
	strsql = strsql  & "  (Phase,VirtualID,TagID,F1,F1_Date)"
    strsql = strsql  & "  VALUES("
	strsql = strsql  & "'" & SESSION("phase") & "'" &  ","    
	strsql = strsql  & Request.QueryString("virtualID") & ","
	strsql = strsql  & "'" & Request.QueryString("tag") & "'" & ","
	strsql = strsql  & "'" & Request("F1") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F1_Date")) 
    strsql = strsql  & ")"
 else
    strsql = ""
    strsql = "UPDATE dbo.tblCRC_CCA_Page722"
    strsql = strsql  & "  SET F1=" & "'" & trim(Request("F1")) & "'"  & ","
    strsql = strsql  & " F1_Date ="  & DateConverter(Request("F1_Date")) 
	strsql = strsql  & " WHERE ID=" & Request.QueryString("editid")
 end if

call OpenRecordset(StrSQL)
 
 STR=Request("F1_Date")  
 if CheckDates(STR)=True then
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='N'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 else
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='Y'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 end if

'============================================================================================
case "tblCRC_CCA_Page723" 
if  Request.QueryString("editid")=0 then
	strsql = ""
	strsql = "INSERT INTO dbo.tblCRC_CCA_Page723"
	strsql = strsql  & "  (Phase,VirtualID,TagID,F1,F1_Date)"
    strsql = strsql  & "  VALUES("
	strsql = strsql  & "'" & SESSION("phase") & "'" &  ","    
	strsql = strsql  & Request.QueryString("virtualID") & ","
	strsql = strsql  & "'" & Request.QueryString("tag") & "'" & ","
	strsql = strsql  & "'" & Request("F1") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F1_Date")) 
    strsql = strsql  & ")"
 else
    strsql = ""
    strsql = "UPDATE dbo.tblCRC_CCA_Page723"
    strsql = strsql  & "  SET F1=" & "'" & trim(Request("F1")) & "'"  & ","
    strsql = strsql  & " F1_Date ="  & DateConverter(Request("F1_Date")) 
	strsql = strsql  & " WHERE ID=" & Request.QueryString("editid")
 end if

call OpenRecordset(StrSQL)
 
 STR=Request("F1_Date")  
 if CheckDates(STR)=True then
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='N'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 else
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='Y'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 end if

'============================================================================================
case "tblCRC_CCA_Page724" 
if  Request.QueryString("editid")=0 then
	strsql = ""
	strsql = "INSERT INTO dbo.tblCRC_CCA_Page724"
	strsql = strsql  & "  (Phase,VirtualID,TagID,F1,F1_Date)"
    strsql = strsql  & "  VALUES("
	strsql = strsql  & "'" & SESSION("phase") & "'" &  ","    
	strsql = strsql  & Request.QueryString("virtualID") & ","
	strsql = strsql  & "'" & Request.QueryString("tag") & "'" & ","
	strsql = strsql  & "'" & Request("F1") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F1_Date")) 
    strsql = strsql  & ")"
 else
    strsql = ""
    strsql = "UPDATE dbo.tblCRC_CCA_Page724"
    strsql = strsql  & "  SET F1=" & "'" & trim(Request("F1")) & "'"  & ","
    strsql = strsql  & " F1_Date ="  & DateConverter(Request("F1_Date")) 
	strsql = strsql  & " WHERE ID=" & Request.QueryString("editid")
 end if

call OpenRecordset(StrSQL)
 
 STR=Request("F1_Date")  
 if CheckDates(STR)=True then
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='N'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 else
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='Y'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 end if
'============================================================================================
case "tblCRC_CCA_Page726" 
if  Request.QueryString("editid")=0 then
	strsql = ""
	strsql = "INSERT INTO dbo.tblCRC_CCA_Page726"
	strsql = strsql  & "  (Phase,VirtualID,TagID,F1,F1_Date,F2,F3)"
    strsql = strsql  & "  VALUES("
	strsql = strsql  & "'" & SESSION("phase") & "'" &  ","    
	strsql = strsql  & Request.QueryString("virtualID") & ","
	strsql = strsql  & "'" & Request.QueryString("tag") & "'" & ","
	strsql = strsql  & "'" & Request("F1") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F1_Date")) & ","
  	strsql = strsql  & "'" & Request("F2") & "'"  & ","
    strsql = strsql  & "'" & Request("F3") & "'"  
    strsql = strsql  & ")"
 else
    strsql = ""
    strsql = "UPDATE dbo.tblCRC_CCA_Page726"
    strsql = strsql  & " SET F1=" & "'" & trim(Request("F1")) & "'"  & ","
    strsql = strsql  & " F1_Date ="  & DateConverter(Request("F1_Date")) & ","
    strsql = strsql  & " F2 ="  & "'" & trim(Request("F2")) & "'" & ","
    strsql = strsql  & " F3 ="  & "'" & trim(Request("F3")) & "'" 
	strsql = strsql  & " WHERE ID=" & Request.QueryString("editid")
 end if

call OpenRecordset(StrSQL)
 
 STR=Request("F1_Date")  
 if CheckDates(STR)=True then
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='N'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 else
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='Y'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 end if


'============================================================================================
case "tblCRC_CCA_Page727" 
if  Request.QueryString("editid")=0 then
	strsql = ""
	strsql = "INSERT INTO dbo.tblCRC_CCA_Page727"
	strsql = strsql  & "  (Phase,VirtualID,TagID,F1,F1_Date)"
    strsql = strsql  & "  VALUES("
	strsql = strsql  & "'" & SESSION("phase") & "'" &  ","    
	strsql = strsql  & Request.QueryString("virtualID") & ","
	strsql = strsql  & "'" & Request.QueryString("tag") & "'" & ","
	strsql = strsql  & "'" & Request("F1") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F1_Date")) 
    strsql = strsql  & ")"
 else
    strsql = ""
    strsql = "UPDATE dbo.tblCRC_CCA_Page727"
    strsql = strsql  & "  SET F1=" & "'" & trim(Request("F1")) & "'"  & ","
    strsql = strsql  & " F1_Date ="  & DateConverter(Request("F1_Date")) 
	strsql = strsql  & " WHERE ID=" & Request.QueryString("editid")
 end if

call OpenRecordset(StrSQL)
 
 STR=Request("F1_Date")  
 if CheckDates(STR)=True then
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='N'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 else
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='Y'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 end if

'============================================================================================
case "tblCRC_CCA_Page728" 
if  Request.QueryString("editid")=0 then
	strsql = ""
	strsql = "INSERT INTO dbo.tblCRC_CCA_Page728"
	strsql = strsql  & "  (Phase,VirtualID,TagID,F1,F1_Date)"
    strsql = strsql  & "  VALUES("
	strsql = strsql  & "'" & SESSION("phase") & "'" &  ","    
	strsql = strsql  & Request.QueryString("virtualID") & ","
	strsql = strsql  & "'" & Request.QueryString("tag") & "'" & ","
	strsql = strsql  & "'" & Request("F1") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F1_Date")) 
    strsql = strsql  & ")"
 else
    strsql = ""
    strsql = "UPDATE dbo.tblCRC_CCA_Page728"
    strsql = strsql  & "  SET F1=" & "'" & trim(Request("F1")) & "'"  & ","
    strsql = strsql  & " F1_Date ="  & DateConverter(Request("F1_Date")) 
	strsql = strsql  & " WHERE ID=" & Request.QueryString("editid")
 end if

call OpenRecordset(StrSQL)
 
 STR=Request("F1_Date")  
 if CheckDates(STR)=True then
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='N'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 else
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='Y'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 end if

'============================================================================================
case "tblCRC_CCA_Page729" 
if  Request.QueryString("editid")=0 then
	strsql = ""
	strsql = "INSERT INTO dbo.tblCRC_CCA_Page729"
	strsql = strsql  & "  (Phase,VirtualID,TagID,F1,F1_Date)"
    strsql = strsql  & "  VALUES("
    strsql = strsql  & "'" & SESSION("phase") & "'" &  ","
	strsql = strsql  & Request.QueryString("virtualID") & ","
	strsql = strsql  & "'" & Request.QueryString("tag") & "'" & ","
	strsql = strsql  & "'" & Request("F1") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F1_Date")) 
    strsql = strsql  & ")"
 else
    strsql = ""
    strsql = "UPDATE dbo.tblCRC_CCA_Page729"
    strsql = strsql  & "  SET F1=" & "'" & trim(Request("F1")) & "'"  & ","
    strsql = strsql  & " F1_Date ="  & DateConverter(Request("F1_Date")) 
	strsql = strsql  & " WHERE ID=" & Request.QueryString("editid")
 end if

call OpenRecordset(StrSQL)
 
 STR=Request("F1_Date")  
 if CheckDates(STR)=True then
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='N'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 else
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='Y'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 end if

'============================================================================================
case "tblCRC_CCA_Page731" 
if  Request.QueryString("editid")=0 then
	strsql = ""
	strsql = "INSERT INTO dbo.tblCRC_CCA_Page731"
	strsql = strsql  & "  (Phase,VirtualID,TagID,F1,F1_Date,F2,F2_Date)"
    strsql = strsql  & "  VALUES("
	strsql = strsql  & "'" & SESSION("phase") & "'" &  ","    
	strsql = strsql  & Request.QueryString("virtualID") & ","
	strsql = strsql  & "'" & Request.QueryString("tag") & "'" & ","
	strsql = strsql  & "'" & Request("F1") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F1_Date")) & ","
	strsql = strsql  & "'" & Request("F2") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F2_Date")) 
	strsql = strsql  & ")"
 else
    strsql = ""
    strsql = "UPDATE dbo.tblCRC_CCA_Page731"
    strsql = strsql  & "  SET F1=" & "'" & trim(Request("F1")) & "'"  & ","
    strsql = strsql  & " F1_Date ="  & DateConverter(Request("F1_Date")) & ","
    strsql = strsql  & " F2="  & "'" & trim(Request("F2")) & "'" & ","
    strsql = strsql  & " F2_Date ="  & DateConverter(Request("F2_Date"))  
	strsql = strsql  & " WHERE ID=" & Request.QueryString("editid")
 end if

call OpenRecordset(StrSQL)
 
 STR=Request("F1_Date")  
 if CheckDates(STR)=True then
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='N'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 else
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='Y'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 end if
'============================================================================================
case "tblCRC_CCA_Page732" 
if  Request.QueryString("editid")=0 then
	strsql = ""
	strsql = "INSERT INTO dbo.tblCRC_CCA_Page732"
	strsql = strsql  & "  (Phase,VirtualID,TagID,F1,F1_Date,F2,F2_Date)"
    strsql = strsql  & "  VALUES("
	strsql = strsql  & "'" & SESSION("phase") & "'" &  ","    
	strsql = strsql  & Request.QueryString("virtualID") & ","
	strsql = strsql  & "'" & Request.QueryString("tag") & "'" & ","
	strsql = strsql  & "'" & Request("F1") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F1_Date"))  & ","
	strsql = strsql  & "'" & Request("F2") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F2_Date"))  
	strsql = strsql  & ")"
 else
    strsql = ""
    strsql = "UPDATE dbo.tblCRC_CCA_Page732"
    strsql = strsql  & "  SET F1=" & "'" & trim(Request("F1")) & "'"  & ","
    strsql = strsql  & " F1_Date ="  & DateConverter(Request("F1_Date"))   & ","
    strsql = strsql  & " F2=" & "'" & Request("F2") & "'"  & ","
    strsql = strsql  & " F2_Date ="  & DateConverter(Request("F2_Date"))  
	strsql = strsql  & " WHERE ID=" & Request.QueryString("editid")
 end if

call OpenRecordset(StrSQL)
 
 STR=Request("F1_Date")  
 if CheckDates(STR)=True then
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='N'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 else
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='Y'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 end if

'============================================================================================
case "tblCRC_CCA_Page741" 
if  Request.QueryString("editid")=0 then
	strsql = ""
	strsql = "INSERT INTO dbo.tblCRC_CCA_Page741"
	strsql = strsql  & "  (Phase,VirtualID,TagID,F1,F1_Date,F2,F2_Date,F3,F3_Date,F4,F4_Date)"
    strsql = strsql  & "  VALUES("
	strsql = strsql  & "'" & SESSION("phase") & "'" &  ","    
	strsql = strsql  & Request.QueryString("virtualID") & ","
	strsql = strsql  & "'" & Request.QueryString("tag") & "'" & ","
	strsql = strsql  & "'" & Request("F1") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F1_Date"))  & ","
	strsql = strsql  & "'" & Request("F2") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F2_Date"))  & ","
	strsql = strsql  & "'" & Request("F3") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F3_Date"))  & ","
	strsql = strsql  & "'" & Request("F4") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F4_Date"))  
	strsql = strsql  & ")"
 else
    strsql = ""
    strsql = "UPDATE dbo.tblCRC_CCA_Page741"
    strsql = strsql  & "  SET F1=" & "'" & trim(Request("F1")) & "'"  & ","
    strsql = strsql  & " F1_Date ="  & DateConverter(Request("F1_Date"))   & ","
    strsql = strsql  & " F2=" & "'" & Request("F2") & "'"  & ","
    strsql = strsql  & " F2_Date ="  & DateConverter(Request("F2_Date"))  & ","
    strsql = strsql  & " F3=" & "'" & Request("F3") & "'"  & ","
    strsql = strsql  & " F3_Date ="  & DateConverter(Request("F3_Date"))   & ","
    strsql = strsql  & " F4=" & "'" & Request("F4") & "'"  & ","
    strsql = strsql  & " F4_Date ="  & DateConverter(Request("F4_Date"))      
	strsql = strsql  & " WHERE ID=" & Request.QueryString("editid")
 end if

call OpenRecordset(StrSQL)
 
 STR=Request("F1_Date")  
 if CheckDates(STR)=True then
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='N'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 else
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='Y'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 end if
'============================================================================================
case "tblCRC_CCA_Page75"
if  Request.QueryString("editid")=0 then
	strsql = ""
	strsql = "INSERT INTO dbo.tblCRC_CCA_Page75"
	strsql = strsql  & "  (Phase,VirtualID,TagID,F1,F1_Date,F2,F2_Date)"
    strsql = strsql  & "  VALUES("
	strsql = strsql  & "'" & SESSION("phase") & "'" &  ","    
	strsql = strsql  & Request.QueryString("virtualID") & ","
	strsql = strsql  & "'" & Request.QueryString("tag") & "'" & ","
	strsql = strsql  & "'" & Request("F1") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F1_Date"))  & ","
	strsql = strsql  & "'" & Request("F2") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F2_Date"))  
	strsql = strsql  & ")"
 else
    strsql = ""
    strsql = "UPDATE dbo.tblCRC_CCA_Page75"
    strsql = strsql  & "  SET F1=" & "'" & trim(Request("F1")) & "'"  & ","
    strsql = strsql  & " F1_Date ="  & DateConverter(Request("F1_Date"))   & ","
    strsql = strsql  & " F2=" & "'" & Request("F2") & "'"  & ","
    strsql = strsql  & " F2_Date ="  & DateConverter(Request("F2_Date"))  
	strsql = strsql  & " WHERE ID=" & Request.QueryString("editid")
 end if

call OpenRecordset(StrSQL)
 
 STR=Request("F1_Date")  
 if CheckDates(STR)=True then
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='N'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 else
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='Y'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 end if
'============================================================================================
case "tblCRC_CCA_Page811" 
if  Request.QueryString("editid")=0 then
	strsql = ""
	strsql = "INSERT INTO dbo.tblCRC_CCA_Page811"
	strsql = strsql  & "  (Phase,VirtualID,TagID,F1,F1_Date,F2,F2_Date,F3,F3_Date,F4,F4_Date)"
    strsql = strsql  & "  VALUES("
	strsql = strsql  & "'" & SESSION("phase") & "'" &  ","    
	strsql = strsql  & Request.QueryString("virtualID") & ","
	strsql = strsql  & "'" & Request.QueryString("tag") & "'" & ","
	strsql = strsql  & "'" & Request("F1") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F1_Date"))  & ","
	strsql = strsql  & "'" & Request("F2") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F2_Date"))  & ","
	strsql = strsql  & "'" & Request("F3") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F3_Date"))  & ","
	strsql = strsql  & "'" & Request("F4") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F4_Date"))  
	strsql = strsql  & ")"
 else
    strsql = ""
    strsql = "UPDATE dbo.tblCRC_CCA_Page811"
    strsql = strsql  & "  SET F1=" & "'" & trim(Request("F1")) & "'"  & ","
    strsql = strsql  & " F1_Date ="  & DateConverter(Request("F1_Date"))   & ","
    strsql = strsql  & " F2=" & "'" & Request("F2") & "'"  & ","
    strsql = strsql  & " F2_Date ="  & DateConverter(Request("F2_Date")) & ","
    strsql = strsql  & " F3=" & "'" & Request("F3") & "'"  & ","
    strsql = strsql  & " F3_Date ="  & DateConverter(Request("F3_Date")) & ","
    strsql = strsql  & " F4=" & "'" & Request("F4") & "'"   & ","
    strsql = strsql  & " F4_Date ="  & DateConverter(Request("F4_Date"))  
	strsql = strsql  & " WHERE ID=" & Request.QueryString("editid")
 end if

 call OpenRecordset(StrSQL)
 
 STR=Request("F1_Date")  & "," & Request("F2_Date") & "," & Request("F3_Date")   & "," & Request("F4_Date")  
 if CheckDates(STR)=True then
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='N'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 else
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='Y'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 end if
'============================================================================================
case "tblCRC_CCA_Page812" 
if  Request.QueryString("editid")=0 then
	strsql = ""
	strsql = "INSERT INTO dbo.tblCRC_CCA_Page812"
	strsql = strsql  & "  (Phase,VirtualID,TagID,F1,F1_Date,F2,F2_Date,F3,F3_Date,F4,F4_Date)"
    strsql = strsql  & "  VALUES("
	strsql = strsql  & "'" & SESSION("phase") & "'" &  ","    
	strsql = strsql  & Request.QueryString("virtualID") & ","
	strsql = strsql  & "'" & Request.QueryString("tag") & "'" & ","
	strsql = strsql  & "'" & Request("F1") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F1_Date"))  & ","
	strsql = strsql  & "'" & Request("F2") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F2_Date"))  & ","
	strsql = strsql  & "'" & Request("F3") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F3_Date"))  & ","
	strsql = strsql  & "'" & Request("F4") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F4_Date"))  
	strsql = strsql  & ")"
 else
    strsql = ""
    strsql = "UPDATE dbo.tblCRC_CCA_Page812"
    strsql = strsql  & "  SET F1=" & "'" & trim(Request("F1")) & "'"  & ","
    strsql = strsql  & " F1_Date ="  & DateConverter(Request("F1_Date"))   & ","
    strsql = strsql  & " F2=" & "'" & Request("F2") & "'"  & ","
    strsql = strsql  & " F2_Date ="  & DateConverter(Request("F2_Date")) & ","
    strsql = strsql  & " F3=" & "'" & Request("F3") & "'"  & ","
    strsql = strsql  & " F3_Date ="  & DateConverter(Request("F3_Date")) & ","
    strsql = strsql  & " F4=" & "'" & Request("F4") & "'"   & ","
    strsql = strsql  & " F4_Date ="  & DateConverter(Request("F4_Date"))  
	strsql = strsql  & " WHERE ID=" & Request.QueryString("editid")
 end if

 call OpenRecordset(StrSQL)
 
 STR=Request("F1_Date")  & "," & Request("F2_Date") & "," & Request("F3_Date")   & "," & Request("F4_Date")  
 if CheckDates(STR)=True then
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='N'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 else
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='Y'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 end if
'============================================================================================
case "tblCRC_CCA_Page821" 
if  Request.QueryString("editid")=0 then
	strsql = ""
	strsql = "INSERT INTO dbo.tblCRC_CCA_Page821"
	strsql = strsql  & "  (Phase,VirtualID,TagID,F1,F1_Date,F2,F2_Date,F3,F3_Date,F4,F4_Date)"
    strsql = strsql  & "  VALUES("
	strsql = strsql  & "'" & SESSION("phase") & "'" &  ","    
	strsql = strsql  & Request.QueryString("virtualID") & ","
	strsql = strsql  & "'" & Request.QueryString("tag") & "'" & ","
	strsql = strsql  & "'" & Request("F1") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F1_Date"))  & ","
	strsql = strsql  & "'" & Request("F2") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F2_Date"))  & ","
	strsql = strsql  & "'" & Request("F3") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F3_Date"))  & ","
	strsql = strsql  & "'" & Request("F4") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F4_Date"))  
	strsql = strsql  & ")"
 else
    strsql = ""
    strsql = "UPDATE dbo.tblCRC_CCA_Page821"
    strsql = strsql  & "  SET F1=" & "'" & trim(Request("F1")) & "'"  & ","
    strsql = strsql  & " F1_Date ="  & DateConverter(Request("F1_Date"))   & ","
    strsql = strsql  & " F2=" & "'" & Request("F2") & "'"  & ","
    strsql = strsql  & " F2_Date ="  & DateConverter(Request("F2_Date")) & ","
    strsql = strsql  & " F3=" & "'" & Request("F3") & "'"  & ","
    strsql = strsql  & " F3_Date ="  & DateConverter(Request("F3_Date")) & ","
    strsql = strsql  & " F4=" & "'" & Request("F4") & "'"   & ","
    strsql = strsql  & " F4_Date ="  & DateConverter(Request("F4_Date"))  
	strsql = strsql  & " WHERE ID=" & Request.QueryString("editid")
 end if

 call OpenRecordset(StrSQL)
 
 STR=Request("F1_Date")  & "," & Request("F2_Date") & "," & Request("F3_Date")   & "," & Request("F4_Date")  
 if CheckDates(STR)=True then
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='N'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 else
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='Y'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 end if

'============================================================================================
case "tblCRC_CCA_Page822" 
if  Request.QueryString("editid")=0 then
	strsql = ""
	strsql = "INSERT INTO dbo.tblCRC_CCA_Page822"
	strsql = strsql  & "  (Phase,VirtualID,TagID,F1,F1_Date,F2,F2_Date,F3,F3_Date,F4,F4_Date,F5,F5_Date,F6,F6_Date)"
    strsql = strsql  & "  VALUES("
	strsql = strsql  & "'" & SESSION("phase") & "'" &  ","    
	strsql = strsql  & Request.QueryString("virtualID") & ","
	strsql = strsql  & "'" & Request.QueryString("tag") & "'" & ","
	strsql = strsql  & "'" & Request("F1") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F1_Date"))  & ","
	strsql = strsql  & "'" & Request("F2") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F2_Date"))  & ","
	strsql = strsql  & "'" & Request("F3") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F3_Date"))  & ","
	strsql = strsql  & "'" & Request("F4") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F4_Date"))  & ","
    strsql = strsql  & "'" & Request("F5") & "'"  & ","
    strsql = strsql  & DateConverter(Request("F5_Date"))  & ","
    strsql = strsql  & "'" & Request("F6") & "'"  & ","
    strsql = strsql  & DateConverter(Request("F6_Date"))  
	strsql = strsql  & ")"
 else
    strsql = ""
    strsql = "UPDATE dbo.tblCRC_CCA_Page822"
    strsql = strsql  & "  SET F1=" & "'" & trim(Request("F1")) & "'"  & ","
    strsql = strsql  & " F1_Date ="  & DateConverter(Request("F1_Date"))   & ","
    strsql = strsql  & " F2=" & "'" & Request("F2") & "'"  & ","
    strsql = strsql  & " F2_Date ="  & DateConverter(Request("F2_Date")) & ","
    strsql = strsql  & " F3=" & "'" & Request("F3") & "'"  & ","
    strsql = strsql  & " F3_Date ="  & DateConverter(Request("F3_Date")) & ","
    strsql = strsql  & " F4=" & "'" & Request("F4") & "'"   & ","
    strsql = strsql  & " F4_Date ="  & DateConverter(Request("F4_Date"))  & ","
    strsql = strsql  & " F5=" & "'" & Request("F5") & "'"   & ","
    strsql = strsql  & " F5_Date ="  & DateConverter(Request("F5_Date"))    & ","
    strsql = strsql  & " F6=" & "'" & Request("F6") & "'"   & ","
    strsql = strsql  & " F6_Date ="  & DateConverter(Request("F6_Date"))  
	strsql = strsql  & " WHERE ID=" & Request.QueryString("editid")
 end if

 call OpenRecordset(StrSQL)
 
 STR=Request("F1_Date")  & "," & Request("F2_Date") & "," & Request("F3_Date")   & "," & Request("F4_Date")  & "," & Request("F5_Date")   & "," & Request("F6_Date")  
 if CheckDates(STR)=True then
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='N'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 else
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='Y'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 end if


'============================================================================================
case "tblCRC_CCA_Page83" 
if  Request.QueryString("editid")=0 then
	strsql = ""
	strsql = "INSERT INTO dbo.tblCRC_CCA_Page83"
	strsql = strsql  & "  (Phase,VirtualID,TagID,F1,F1_Date,F2,F2_Date,F3,F3_Date,F4,F4_Date)"
    strsql = strsql  & "  VALUES("
    	strsql = strsql  & "'" & SESSION("phase") & "'" &  ","
	strsql = strsql  & Request.QueryString("virtualID") & ","
	strsql = strsql  & "'" & Request.QueryString("tag") & "'" & ","
	strsql = strsql  & "'" & Request("F1") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F1_Date"))  & ","
	strsql = strsql  & "'" & Request("F2") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F2_Date"))  & ","
	strsql = strsql  & "'" & Request("F3") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F3_Date"))  & ","
	strsql = strsql  & "'" & Request("F4") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F4_Date"))  
	strsql = strsql  & ")"
 else
    strsql = ""
    strsql = "UPDATE dbo.tblCRC_CCA_Page83"
    strsql = strsql  & "  SET F1=" & "'" & trim(Request("F1")) & "'"  & ","
    strsql = strsql  & " F1_Date ="  & DateConverter(Request("F1_Date"))   & ","
    strsql = strsql  & " F2=" & "'" & Request("F2") & "'"  & ","
    strsql = strsql  & " F2_Date ="  & DateConverter(Request("F2_Date")) & ","
    strsql = strsql  & " F3=" & "'" & Request("F3") & "'"  & ","
    strsql = strsql  & " F3_Date ="  & DateConverter(Request("F3_Date")) & ","
    strsql = strsql  & " F4=" & "'" & Request("F4") & "'"   & ","
    strsql = strsql  & " F4_Date ="  & DateConverter(Request("F4_Date"))  
	strsql = strsql  & " WHERE ID=" & Request.QueryString("editid")
 end if

 call OpenRecordset(StrSQL)
 
 STR=Request("F1_Date")  & "," & Request("F2_Date") & "," & Request("F3_Date")   & "," & Request("F4_Date")  
 if CheckDates(STR)=True then
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='N'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 else
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='Y'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 end if

'============================================================================================
case "tblCRC_CCA_Page84" 
if  Request.QueryString("editid")=0 then
	strsql = ""
	strsql = "INSERT INTO dbo.tblCRC_CCA_Page84"
	strsql = strsql  & "  (Phase,,VirtualID,TagID,F1,F1_Date,F2,F2_Date,F3,F3_Date,F4,F4_Date)"
    strsql = strsql  & "  VALUES("
	strsql = strsql  & "'" & SESSION("phase") & "'" &  ","    
	strsql = strsql  & Request.QueryString("virtualID") & ","
	strsql = strsql  & "'" & Request.QueryString("tag") & "'" & ","
	strsql = strsql  & "'" & Request("F1") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F1_Date"))  & ","
	strsql = strsql  & "'" & Request("F2") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F2_Date"))  & ","
	strsql = strsql  & "'" & Request("F3") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F3_Date"))  & ","
	strsql = strsql  & "'" & Request("F4") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F4_Date"))  
	strsql = strsql  & ")"
 else
    strsql = ""
    strsql = "UPDATE dbo.tblCRC_CCA_Page84"
    strsql = strsql  & "  SET F1=" & "'" & trim(Request("F1")) & "'"  & ","
    strsql = strsql  & " F1_Date ="  & DateConverter(Request("F1_Date"))   & ","
    strsql = strsql  & " F2=" & "'" & Request("F2") & "'"  & ","
    strsql = strsql  & " F2_Date ="  & DateConverter(Request("F2_Date")) & ","
    strsql = strsql  & " F3=" & "'" & Request("F3") & "'"  & ","
    strsql = strsql  & " F3_Date ="  & DateConverter(Request("F3_Date")) & ","
    strsql = strsql  & " F4=" & "'" & Request("F4") & "'"   & ","
    strsql = strsql  & " F4_Date ="  & DateConverter(Request("F4_Date"))  
	strsql = strsql  & " WHERE ID=" & Request.QueryString("editid")
 end if

 call OpenRecordset(StrSQL)
 
 STR=Request("F1_Date")  & "," & Request("F2_Date") & "," & Request("F3_Date")   & "," & Request("F4_Date")  
 if CheckDates(STR)=True then
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='N'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 else
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='Y'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 end if
'==============================================================
'========================================
case "tblCRC_CCA_Page85" 
if  Request.QueryString("editid")=0 then
	strsql = ""
	strsql = "INSERT INTO dbo.tblCRC_CCA_Page85"
	strsql = strsql  & "  (Phase,VirtualID,TagID,F1,F1_Date,F2,F2_Date)"
    strsql = strsql  & "  VALUES("
    strsql = strsql  & "'" & SESSION("phase") & "'" &  ","
	strsql = strsql  & Request.QueryString("virtualID") & ","
	strsql = strsql  & "'" & Request.QueryString("tag") & "'" & ","
	strsql = strsql  & "'" & Request("F1") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F1_Date"))    & ","
	strsql = strsql  & "'" & Request("F2") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F2_Date"))    
    strsql = strsql  & ")"
 else
    strsql = ""
    strsql = "UPDATE dbo.tblCRC_CCA_Page85"
    strsql = strsql  & "  SET F1=" & "'" & trim(Request("F1")) & "'"  & ","
    strsql = strsql  & " F1_Date ="  & DateConverter(Request("F1_Date"))  & ","
    strsql = strsql  & " F2=" & "'" & Request("F2") & "'"  & ","
    strsql = strsql  & " F2_Date ="  & DateConverter(Request("F2_Date"))   
	strsql = strsql  & " WHERE ID=" & Request.QueryString("editid")
 end if

call OpenRecordset(StrSQL)
 
 STR=Request("F1_Date") & "," & Request("F2_Date")  
 if CheckDates(STR)=True then
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='N'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 else
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='Y'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 end if


'============================================================================================
case "tblCRC_CCA_Page86" 
if  Request.QueryString("editid")=0 then
	strsql = ""
	strsql = "INSERT INTO dbo.tblCRC_CCA_Page86"
	strsql = strsql  & "  (Phase,VirtualID,TagID,F1,F1_Date,F2,F2_Date)"
    strsql = strsql  & "  VALUES("
	strsql = strsql  & "'" & SESSION("phase") & "'" &  ","    
	strsql = strsql  & Request.QueryString("virtualID") & ","
	strsql = strsql  & "'" & Request.QueryString("tag") & "'" & ","
	strsql = strsql  & "'" & Request("F1") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F1_Date"))  & ","
	strsql = strsql  & "'" & Request("F2") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F2_Date"))   
	strsql = strsql  & ")"
 else
    strsql = ""
    strsql = "UPDATE dbo.tblCRC_CCA_Page86"
    strsql = strsql  & "  SET F1=" & "'" & trim(Request("F1")) & "'"  & ","
    strsql = strsql  & " F1_Date ="  & DateConverter(Request("F1_Date"))   & ","
    strsql = strsql  & " F2=" & "'" & Request("F2") & "'"  & ","
    strsql = strsql  & " F2_Date ="  & DateConverter(Request("F2_Date"))  
	strsql = strsql  & " WHERE ID=" & Request.QueryString("editid")
 end if

 call OpenRecordset(StrSQL)
 
 STR=Request("F1_Date")  & "," & Request("F2_Date")  
 if CheckDates(STR)=True then
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='N'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 else
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='Y'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 end if

'============================================================================================
case "tblCRC_CCA_Page87" 
if  Request.QueryString("editid")=0 then
	strsql = ""
	strsql = "INSERT INTO dbo.tblCRC_CCA_Page87"
	strsql = strsql  & "  (Phase,VirtualID,TagID,F1,F1_Date,F2,F2_Date)"
    strsql = strsql  & "  VALUES("
    strsql = strsql  & "'" & SESSION("phase") & "'" &  ","
	strsql = strsql  & Request.QueryString("virtualID") & ","
	strsql = strsql  & "'" & Request.QueryString("tag") & "'" & ","
	strsql = strsql  & "'" & Request("F1") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F1_Date"))  & ","
	strsql = strsql  & "'" & Request("F2") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F2_Date"))   
	strsql = strsql  & ")"
 else
    strsql = ""
    strsql = "UPDATE dbo.tblCRC_CCA_Page87"
    strsql = strsql  & "  SET F1=" & "'" & trim(Request("F1")) & "'"  & ","
    strsql = strsql  & " F1_Date ="  & DateConverter(Request("F1_Date"))   & ","
    strsql = strsql  & " F2=" & "'" & Request("F2") & "'"  & ","
    strsql = strsql  & " F2_Date ="  & DateConverter(Request("F2_Date"))  
	strsql = strsql  & " WHERE ID=" & Request.QueryString("editid")
 end if

 call OpenRecordset(StrSQL)
 
 STR=Request("F1_Date")  & "," & Request("F2_Date")  
 if CheckDates(STR)=True then
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='N'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 else
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='Y'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 end if

'============================================================================================
case "tblCRC_CCA_Page88" 
if  Request.QueryString("editid")=0 then
	strsql = ""
	strsql = "INSERT INTO dbo.tblCRC_CCA_Page88"
	strsql = strsql  & "  (Phase,VirtualID,TagID,F1,F1_Date)"
    strsql = strsql  & "  VALUES("
    strsql = strsql  & "'" & SESSION("phase") & "'" &  ","    
	strsql = strsql  & Request.QueryString("virtualID") & ","
	strsql = strsql  & "'" & Request.QueryString("tag") & "'" & ","
	strsql = strsql  & "'" & Request("F1") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F1_Date"))   
  	strsql = strsql  & ")"
 else
    strsql = ""
    strsql = "UPDATE dbo.tblCRC_CCA_Page88"
    strsql = strsql  & "  SET F1=" & "'" & trim(Request("F1")) & "'"  & ","
    strsql = strsql  & " F1_Date ="  & DateConverter(Request("F1_Date"))  
 	strsql = strsql  & " WHERE ID=" & Request.QueryString("editid")
 end if

 call OpenRecordset(strsql)
 
 STR=Request("F1_Date")   
 if CheckDates(STR)=True then
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='N'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 else
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='Y'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 end if


'============================================================================================
case "tblCRC_CCA_Page89" 
if  Request.QueryString("editid")=0 then
	strsql = ""
	strsql = "INSERT INTO dbo.tblCRC_CCA_Page89"
	strsql = strsql  & "  (Phase,VirtualID,TagID,F1,F1_Date,F2,F2_Date,F3,F3_Date,F4,F4_Date,F5,F5_Date)"
    strsql = strsql  & "  VALUES("
    strsql = strsql  & "'" & SESSION("phase") & "'" &  ","
	strsql = strsql  & Request.QueryString("virtualID") & ","
	strsql = strsql  & "'" & Request.QueryString("tag") & "'" & ","
	strsql = strsql  & "'" & Request("F1") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F1_Date"))  & ","
	strsql = strsql  & "'" & Request("F2") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F2_Date"))  & ","
	strsql = strsql  & "'" & Request("F3") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F3_Date"))  & ","
	strsql = strsql  & "'" & Request("F4") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F4_Date"))  & ","
	strsql = strsql  & "'" & Request("F5") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F5_Date"))  	
	strsql = strsql  & ")"
 else
    strsql = ""
    strsql = "UPDATE dbo.tblCRC_CCA_Page89"
    strsql = strsql  & "  SET F1=" & "'" & trim(Request("F1")) & "'"  & ","
    strsql = strsql  & " F1_Date ="  & DateConverter(Request("F1_Date"))   & ","
    strsql = strsql  & " F2=" & "'" & Request("F2") & "'"  & ","
    strsql = strsql  & " F2_Date ="  & DateConverter(Request("F2_Date")) & ","
    strsql = strsql  & " F3=" & "'" & Request("F3") & "'"  & ","
    strsql = strsql  & " F3_Date ="  & DateConverter(Request("F3_Date")) & ","
    strsql = strsql  & " F4=" & "'" & Request("F4") & "'"   & ","
    strsql = strsql  & " F4_Date ="  & DateConverter(Request("F4_Date"))  & ","
    strsql = strsql  & " F5=" & "'" & Request("F5") & "'"   & ","
    strsql = strsql  & " F5_Date ="  & DateConverter(Request("F5_Date"))      
	strsql = strsql  & " WHERE ID=" & Request.QueryString("editid")
 end if

 call OpenRecordset(StrSQL)
 
 STR=Request("F1_Date")  & "," & Request("F2_Date") & "," & Request("F3_Date")   & "," & Request("F4_Date")   & "," & Request("F5_Date") 
 if CheckDates(STR)=True then
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='N'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 else
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='Y'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 end if

'============================================================================================
case "tblCRC_CCA_Page9101" 
if  Request.QueryString("editid")=0 then
	strsql = ""
	strsql = "INSERT INTO dbo.tblCRC_CCA_Page9101"
	strsql = strsql  & "  (Phase,VirtualID,TagID,F1,F1_Date,F2,F2_Date)"
    strsql = strsql  & "  VALUES("
    strsql = strsql  & "'" & SESSION("phase") & "'" &  ","
	strsql = strsql  & Request.QueryString("virtualID") & ","
	strsql = strsql  & "'" & Request.QueryString("tag") & "'" & ","
	strsql = strsql  & "'" & Request("F1") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F1_Date"))    & ","
    strsql = strsql  & "'" & Request("F2") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F2_Date"))   
    strsql = strsql  & ")"
 else
    strsql = ""
    strsql = "UPDATE dbo.tblCRC_CCA_Page9101"
    strsql = strsql  & "  SET F1=" & "'" & trim(Request("F1")) & "'"  & ","
    strsql = strsql  & " F1_Date ="  & DateConverter(Request("F1_Date"))  & ","
    strsql = strsql  & " F2=" & "'" & Request("F2") & "'"  & ","
    strsql = strsql  & " F2_Date ="  & DateConverter(Request("F2_Date")) 
	strsql = strsql  & " WHERE ID=" & Request.QueryString("editid")
 end if

call OpenRecordset(StrSQL)
 
 STR=Request("F1_Date") & "," & Request("F2_Date") 
 if CheckDates(STR)=True then
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='N'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 else
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='Y'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 end if

'============================================================================================
case "tblCRC_CCA_Page95" 
if  Request.QueryString("editid")=0 then
	strsql = ""
	strsql = "INSERT INTO dbo.tblCRC_CCA_Page95"
	strsql = strsql  & "  (Phase,VirtualID,TagID,F1,F1_Date,F2,F2_Date)"
    strsql = strsql  & "  VALUES("
    strsql = strsql  & "'" & SESSION("phase") & "'" &  ","
	strsql = strsql  & Request.QueryString("virtualID") & ","
	strsql = strsql  & "'" & Request.QueryString("tag") & "'" & ","
	strsql = strsql  & "'" & Request("F1") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F1_Date"))    & ","
    strsql = strsql  & "'" & Request("F2") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F2_Date"))   
    strsql = strsql  & ")"
 else
    strsql = ""
    strsql = "UPDATE dbo.tblCRC_CCA_Page95"
    strsql = strsql  & "  SET F1=" & "'" & trim(Request("F1")) & "'"  & ","
    strsql = strsql  & " F1_Date ="  & DateConverter(Request("F1_Date"))  & ","
    strsql = strsql  & " F2=" & "'" & Request("F2") & "'"  & ","
    strsql = strsql  & " F2_Date ="  & DateConverter(Request("F2_Date")) 
	strsql = strsql  & " WHERE ID=" & Request.QueryString("editid")
 end if

call OpenRecordset(StrSQL)
 
 STR=Request("F1_Date") & "," & Request("F2_Date") 
 if CheckDates(STR)=True then
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='N'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 else
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='Y'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 end if

'============================================================================================
case "tblCRC_CCA_Page961" 
if  Request.QueryString("editid")=0 then
	strsql = ""
	strsql = "INSERT INTO dbo.tblCRC_CCA_Page961"
	strsql = strsql  & "  (Phase,VirtualID,TagID,F1,F1_Date,F2,F2_Date)"
    strsql = strsql  & "  VALUES("
    strsql = strsql  & "'" & SESSION("phase") & "'" &  ","
	strsql = strsql  & Request.QueryString("virtualID") & ","
	strsql = strsql  & "'" & Request.QueryString("tag") & "'" & ","
	strsql = strsql  & "'" & Request("F1") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F1_Date"))    & ","
    strsql = strsql  & "'" & Request("F2") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F2_Date"))   
    strsql = strsql  & ")"
 else
    strsql = ""
    strsql = "UPDATE dbo.tblCRC_CCA_Page961"
    strsql = strsql  & "  SET F1=" & "'" & trim(Request("F1")) & "'"  & ","
    strsql = strsql  & " F1_Date ="  & DateConverter(Request("F1_Date"))  & ","
    strsql = strsql  & " F2=" & "'" & Request("F2") & "'"  & ","
    strsql = strsql  & " F2_Date ="  & DateConverter(Request("F2_Date")) 
	strsql = strsql  & " WHERE ID=" & Request.QueryString("editid")
 end if

call OpenRecordset(StrSQL)
 
 STR=Request("F1_Date") & "," & Request("F2_Date") 
 if CheckDates(STR)=True then
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='N'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 else
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='Y'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 end if
'============================================================================================
case "tblCRC_CCA_Page97" 
if  Request.QueryString("editid")=0 then
	strsql = ""
	strsql = "INSERT INTO dbo.tblCRC_CCA_Page97"
	strsql = strsql  & "  (Phase,VirtualID,TagID,F1,F1_Date,F2,F2_Date)"
    strsql = strsql  & "  VALUES("
    strsql = strsql  & "'" & SESSION("phase") & "'" &  ","
	strsql = strsql  & Request.QueryString("virtualID") & ","
	strsql = strsql  & "'" & Request.QueryString("tag") & "'" & ","
	strsql = strsql  & "'" & Request("F1") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F1_Date"))    & ","
    strsql = strsql  & "'" & Request("F2") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F2_Date"))   
    strsql = strsql  & ")"
 else
    strsql = ""
    strsql = "UPDATE dbo.tblCRC_CCA_Page97"
    strsql = strsql  & "  SET F1=" & "'" & trim(Request("F1")) & "'"  & ","
    strsql = strsql  & " F1_Date ="  & DateConverter(Request("F1_Date"))  & ","
    strsql = strsql  & " F2=" & "'" & Request("F2") & "'"  & ","
    strsql = strsql  & " F2_Date ="  & DateConverter(Request("F2_Date")) 
	strsql = strsql  & " WHERE ID=" & Request.QueryString("editid")
 end if

call OpenRecordset(StrSQL)
 
 STR=Request("F1_Date") & "," & Request("F2_Date") 
 if CheckDates(STR)=True then
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='N'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 else
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='Y'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 end if
'============================================================================================
case "tblCRC_CCA_Page98" 
if  Request.QueryString("editid")=0 then
	strsql = ""
	strsql = "INSERT INTO dbo.tblCRC_CCA_Page98"
	strsql = strsql  & "  (Phase,VirtualID,TagID,F1,F1_Date,F2,F2_Date,F3,F3_Date,F4,F5,F6)"
    strsql = strsql  & "  VALUES("
    strsql = strsql  & "'" & SESSION("phase") & "'" &  ","
	strsql = strsql  & Request.QueryString("virtualID") & ","
	strsql = strsql  & "'" & Request.QueryString("tag") & "'" & ","
	strsql = strsql  & "'" & Request("F1") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F1_Date"))    & ","
    strsql = strsql  & "'" & Request("F2") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F2_Date"))   & ","
	strsql = strsql  & "'" & Request("F3") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F3_Date"))    & ","
    strsql = strsql  & "'" & Request("F4") & "'"  & ","
    strsql = strsql  & "'" & Request("F5") & "'"  & ","
    strsql = strsql  & "'" & Request("F6") & "'"   
    strsql = strsql  & ")"
 else
    strsql = ""
    strsql = "UPDATE dbo.tblCRC_CCA_Page98"
    strsql = strsql  & "  SET F1=" & "'" & trim(Request("F1")) & "'"  & ","
    strsql = strsql  & " F1_Date ="  & DateConverter(Request("F1_Date"))  & ","
    strsql = strsql  & " F2=" & "'" & Request("F2") & "'"  & ","
    strsql = strsql  & " F2_Date ="  & DateConverter(Request("F2_Date"))  & ","
    strsql = strsql  & " F3=" & "'" & Request("F3") & "'"  & ","
    strsql = strsql  & " F3_Date ="  & DateConverter(Request("F3_Date"))  & ","    
    strsql = strsql  & " F4=" & "'" & Request("F4") & "'"  & ","
    strsql = strsql  & " F5=" & "'" & Request("F5") & "'"  & ","
    strsql = strsql  & " F6=" & "'" & Request("F6") & "'"   
	strsql = strsql  & " WHERE ID=" & Request.QueryString("editid")
 end if

call OpenRecordset(StrSQL)
 
 STR=Request("F1_Date") & "," & Request("F2_Date") & "," & Request("F3_Date") 
 if CheckDates(STR)=True then
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='N'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 else
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='Y'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 end if
'============================================================================================
case "tblCRC_CCA_Page99" 
if  Request.QueryString("editid")=0 then
	strsql = ""
	strsql = "INSERT INTO dbo.tblCRC_CCA_Page99"
	strsql = strsql  & "  (Phase,VirtualID,TagID,F1,F1_Date,F2,F2_Date,F3,F3_Date,F4,F5,F6)"
    strsql = strsql  & "  VALUES("
    strsql = strsql  & "'" & SESSION("phase") & "'" &  ","
	strsql = strsql  & Request.QueryString("virtualID") & ","
	strsql = strsql  & "'" & Request.QueryString("tag") & "'" & ","
	strsql = strsql  & "'" & Request("F1") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F1_Date"))    & ","
    strsql = strsql  & "'" & Request("F2") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F2_Date"))   & ","
	strsql = strsql  & "'" & Request("F3") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F3_Date"))    & ","
    strsql = strsql  & "'" & Request("F4") & "'"  & ","
    strsql = strsql  & "'" & Request("F5") & "'"  & ","
    strsql = strsql  & "'" & Request("F6") & "'"   
    strsql = strsql  & ")"
 else
    strsql = ""
    strsql = "UPDATE dbo.tblCRC_CCA_Page99"
    strsql = strsql  & "  SET F1=" & "'" & trim(Request("F1")) & "'"  & ","
    strsql = strsql  & " F1_Date ="  & DateConverter(Request("F1_Date"))  & ","
    strsql = strsql  & " F2=" & "'" & Request("F2") & "'"  & ","
    strsql = strsql  & " F2_Date ="  & DateConverter(Request("F2_Date"))  & ","
    strsql = strsql  & " F3=" & "'" & Request("F3") & "'"  & ","
    strsql = strsql  & " F3_Date ="  & DateConverter(Request("F3_Date"))  & ","    
    strsql = strsql  & " F4=" & "'" & Request("F4") & "'"  & ","
    strsql = strsql  & " F5=" & "'" & Request("F5") & "'"  & ","
    strsql = strsql  & " F6=" & "'" & Request("F6") & "'"   
	strsql = strsql  & " WHERE ID=" & Request.QueryString("editid")
 end if

call OpenRecordset(StrSQL)
 
 STR=Request("F1_Date") & "," & Request("F2_Date") & "," & Request("F3_Date") 
 if CheckDates(STR)=True then
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='N'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 else
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='Y'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 end if
'============================================================================================
'============================================================================================
case "tblCRC_CCA_Page91" 
if  Request.QueryString("editid")=0 then
	strsql = ""
	strsql = "INSERT INTO dbo.tblCRC_CCA_Page91"
	strsql = strsql  & "  (Phase,VirtualID,TagID,F1,F1_Date,F2,F2_Date)"
    strsql = strsql  & "  VALUES("
    strsql = strsql  & "'" & SESSION("phase") & "'" &  ","
	strsql = strsql  & Request.QueryString("virtualID") & ","
	strsql = strsql  & "'" & Request.QueryString("tag") & "'" & ","
	strsql = strsql  & "'" & Request("F1") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F1_Date"))    & ","
	strsql = strsql  & "'" & Request("F2") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F2_Date"))    
    strsql = strsql  & ")"
 else
    strsql = ""
    strsql = "UPDATE dbo.tblCRC_CCA_Page91"
    strsql = strsql  & "  SET F1=" & "'" & trim(Request("F1")) & "'"  & ","
    strsql = strsql  & " F1_Date ="  & DateConverter(Request("F1_Date"))  & ","
    strsql = strsql  & " F2=" & "'" & Request("F2") & "'"  & ","
    strsql = strsql  & " F2_Date ="  & DateConverter(Request("F2_Date"))   
	strsql = strsql  & " WHERE ID=" & Request.QueryString("editid")
 end if

call OpenRecordset(StrSQL)
 
 STR=Request("F1_Date") & "," & Request("F2_Date")  
 if CheckDates(STR)=True then
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='N'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 else
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='Y'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 end if
'========================================
case "tblCRC_CCA_Page92a" 
if  Request.QueryString("editid")=0 then
	strsql = ""
	strsql = "INSERT INTO dbo.tblCRC_CCA_Page92a"
	strsql = strsql  & "  (Phase,VirtualID,TagID,F1,F1_Date,F2,F2_Date)"
    strsql = strsql  & "  VALUES("
    strsql = strsql  & "'" & SESSION("phase") & "'" &  ","
	strsql = strsql  & Request.QueryString("virtualID") & ","
	strsql = strsql  & "'" & Request.QueryString("tag") & "'" & ","
	strsql = strsql  & "'" & Request("F1") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F1_Date"))    & ","
	strsql = strsql  & "'" & Request("F2") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F2_Date"))    
    strsql = strsql  & ")"
 else
    strsql = ""
    strsql = "UPDATE dbo.tblCRC_CCA_Page92a"
    strsql = strsql  & "  SET F1=" & "'" & trim(Request("F1")) & "'"  & ","
    strsql = strsql  & " F1_Date ="  & DateConverter(Request("F1_Date"))  & ","
    strsql = strsql  & " F2=" & "'" & Request("F2") & "'"  & ","
    strsql = strsql  & " F2_Date ="  & DateConverter(Request("F2_Date"))   
	strsql = strsql  & " WHERE ID=" & Request.QueryString("editid")
 end if

call OpenRecordset(StrSQL)
 
 STR=Request("F1_Date") & "," & Request("F2_Date")  
 if CheckDates(STR)=True then
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='N'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 else
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='Y'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 end if
'========================================
'========================================
case "tblCRC_CCA_Page93" 
if  Request.QueryString("editid")=0 then
	strsql = ""
	strsql = "INSERT INTO dbo.tblCRC_CCA_Page93"
	strsql = strsql  & "  (Phase,VirtualID,TagID,F1,F1_Date,F2,F2_Date)"
    strsql = strsql  & "  VALUES("
    strsql = strsql  & "'" & SESSION("phase") & "'" &  ","
	strsql = strsql  & Request.QueryString("virtualID") & ","
	strsql = strsql  & "'" & Request.QueryString("tag") & "'" & ","
	strsql = strsql  & "'" & Request("F1") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F1_Date"))    & ","
	strsql = strsql  & "'" & Request("F2") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F2_Date"))    
    strsql = strsql  & ")"
 else
    strsql = ""
    strsql = "UPDATE dbo.tblCRC_CCA_Page93"
    strsql = strsql  & "  SET F1=" & "'" & trim(Request("F1")) & "'"  & ","
    strsql = strsql  & " F1_Date ="  & DateConverter(Request("F1_Date"))  & ","
    strsql = strsql  & " F2=" & "'" & Request("F2") & "'"  & ","
    strsql = strsql  & " F2_Date ="  & DateConverter(Request("F2_Date"))   
	strsql = strsql  & " WHERE ID=" & Request.QueryString("editid")
 end if

call OpenRecordset(StrSQL)
 
 STR=Request("F1_Date") & "," & Request("F2_Date")  
 if CheckDates(STR)=True then
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='N'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 else
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='Y'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 end if
'========================================
case "tblCRC_CCA_Page94a" 
if  Request.QueryString("editid")=0 then
	strsql = ""
	strsql = "INSERT INTO dbo.tblCRC_CCA_Page94a"
	strsql = strsql  & "  (Phase,VirtualID,TagID,F1,F1_Date,F2,F2_Date)"
    strsql = strsql  & "  VALUES("
    strsql = strsql  & "'" & SESSION("phase") & "'" &  ","
	strsql = strsql  & Request.QueryString("virtualID") & ","
	strsql = strsql  & "'" & Request.QueryString("tag") & "'" & ","
	strsql = strsql  & "'" & Request("F1") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F1_Date"))    & ","
	strsql = strsql  & "'" & Request("F2") & "'"  & ","
	strsql = strsql  & DateConverter(Request("F2_Date"))    
    strsql = strsql  & ")"
 else
    strsql = ""
    strsql = "UPDATE dbo.tblCRC_CCA_Page94a"
    strsql = strsql  & "  SET F1=" & "'" & trim(Request("F1")) & "'"  & ","
    strsql = strsql  & " F1_Date ="  & DateConverter(Request("F1_Date"))  & ","
    strsql = strsql  & " F2=" & "'" & Request("F2") & "'"  & ","
    strsql = strsql  & " F2_Date ="  & DateConverter(Request("F2_Date"))   
	strsql = strsql  & " WHERE ID=" & Request.QueryString("editid")
 end if

call OpenRecordset(StrSQL)
 
 STR=Request("F1_Date") & "," & Request("F2_Date")  
 if CheckDates(STR)=True then
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='N'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 else
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='Y'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 end if
'============================================================================================
case "tblCRC_CCA_Page92b"
if  Request.QueryString("editid")=0 then
	strsql = ""
	strsql = "INSERT INTO dbo.tblCRC_CCA_Page92b "
	strsql = strsql  & " (phase,VirtualID,TagID,F1,F1_Date,F2,F2_Date,F3,F3_Date,F4,F5,F6)"
    strsql = strsql  & " VALUES("
    strsql = strsql  & "'" & SESSION("phase") & "'" &  ","
	strsql = strsql  & Request.QueryString("virtualID") & ","
	strsql = strsql  & "'" & Request.QueryString("tag") & "'" & ","
	strsql = strsql  & "'" & Request("F1") & "'" & ","
	strsql = strsql  & DateConverter(Request("F1_Date"))  & ","
    strsql = strsql  & "'" & Request("F2") & "'" & ","
    strsql = strsql  & DateConverter(Request("F2_Date"))  & ","
    strsql = strsql  & "'" & Request("F3") & "'" & ","
    strsql = strsql  & DateConverter(Request("F3_Date"))  & ","
    strsql = strsql  & "'" & Request("F4") & "'" & ","
    strsql = strsql  & "'" & Request("F5") & "'" & ","
    strsql = strsql  & "'" & Request("F6") & "'"  
    strsql = strsql  & ")"
 else
    strsql = ""
	strsql = "UPDATE dbo.tblCRC_CCA_Page92b "
	strsql = strsql  & " SET F1=" & "'" & trim(Request("F1")) & "'"  & ","
	strsql = strsql  & " F1_Date ="  & DateConverter(Request("F1_Date")) & ","
    strsql = strsql  & " F2=" & "'" & trim(Request("F2")) & "'"  & ","
    strsql = strsql  & " F2_Date ="  & DateConverter(Request("F2_Date")) & ","
    strsql = strsql  & " F3="  & "'" & trim(Request("F3")) & "'"  & ","
    strsql = strsql  & " F3_Date ="  & DateConverter(Request("F3_Date")) & ","
    strsql = strsql  & " F4=" & "'" & trim(Request("F4")) & "'"  & ","
    strsql = strsql  & " F5=" & "'" & trim(Request("F5")) & "'"  & ","
    strsql = strsql  & " F6=" & "'" & trim(Request("F6")) & "'"  
	strsql = strsql  & " WHERE ID=" & Request.QueryString("editid")
 end if
 
 call OpenRecordset(StrSQL)
 
 STR=Request("F1_Date")& "," & Request("F2_Date")& "," & Request("F3_Date")& "," & Request("F4_Date")   
 if CheckDates(STR)=True then
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='N'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 else
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='Y'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 end if
'====================================
'============================================================================================
case "tblCRC_CCA_Page94b"
if  Request.QueryString("editid")=0 then
	strsql = ""
	strsql = "INSERT INTO dbo.tblCRC_CCA_Page94b "
	strsql = strsql  & " (phase,VirtualID,TagID,F1,F1_Date,F2,F2_Date,F3,F3_Date,F4,F5,F6)"
    strsql = strsql  & " VALUES("
    strsql = strsql  & "'" & SESSION("phase") & "'" &  ","
	strsql = strsql  & Request.QueryString("virtualID") & ","
	strsql = strsql  & "'" & Request.QueryString("tag") & "'" & ","
	strsql = strsql  & "'" & Request("F1") & "'" & ","
	strsql = strsql  & DateConverter(Request("F1_Date"))  & ","
    strsql = strsql  & "'" & Request("F2") & "'" & ","
    strsql = strsql  & DateConverter(Request("F2_Date"))  & ","
    strsql = strsql  & "'" & Request("F3") & "'" & ","
    strsql = strsql  & DateConverter(Request("F3_Date"))  & ","
    strsql = strsql  & "'" & Request("F4") & "'" & ","
    strsql = strsql  & "'" & Request("F5") & "'" & ","
    strsql = strsql  & "'" & Request("F6") & "'"  
    strsql = strsql  & ")"
 else
    strsql = ""
	strsql = "UPDATE dbo.tblCRC_CCA_Page94b "
	strsql = strsql  & " SET F1=" & "'" & trim(Request("F1")) & "'"  & ","
	strsql = strsql  & " F1_Date ="  & DateConverter(Request("F1_Date")) & ","
    strsql = strsql  & " F2=" & "'" & trim(Request("F2")) & "'"  & ","
    strsql = strsql  & " F2_Date ="  & DateConverter(Request("F2_Date")) & ","
    strsql = strsql  & " F3="  & "'" & trim(Request("F3")) & "'"  & ","
    strsql = strsql  & " F3_Date ="  & DateConverter(Request("F3_Date")) & ","
    strsql = strsql  & " F4=" & "'" & trim(Request("F4")) & "'"  & ","
    strsql = strsql  & " F5=" & "'" & trim(Request("F5")) & "'"  & ","
    strsql = strsql  & " F6=" & "'" & trim(Request("F6")) & "'"  
	strsql = strsql  & " WHERE ID=" & Request.QueryString("editid")
 end if
 
 call OpenRecordset(StrSQL)
 
 STR=Request("F1_Date")& "," & Request("F2_Date")& "," & Request("F3_Date")& "," & Request("F4_Date")   
 if CheckDates(STR)=True then
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='N'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 else
    strsql = " UPDATE dbo.tblCRC_CCA_Master_Evidence "
    strsql = strsql & " SET iCompleted ='Y'"
    strsql = strsql & " WHERE virtualID =" & trim(Request.QueryString("virtualID"))
    strsql = strsql & " AND Tag=" & "'" & trim(Request.QueryString("Tag")) & "'"
    call OpenRecordset(StrSQL)
 end if





'=======================






 
end select

Call CloseConnection

' evidenceupdate.asp?virtualid

  %>
   <html> 
   <head>
        <LINK REL=StyleSheet HREF="CCA_stylesheet.css" TYPE="text/css" >
  </head>
  <body> 
      <span class= "UpdateComment">Items have been Updated Successully </span>
  </body>
  </html>
  <%
 
 response.write("<script language=""javascript"" type=""text/javascript"">")
 response.Write("parent.window.location.href=" & chr(34) & "page1.asp?id="  &  session("VirtualID") & "&phase=" & SESSION("phase")  & chr(34))  
 response.write("</script>")

  Response.End
  %>
 
 