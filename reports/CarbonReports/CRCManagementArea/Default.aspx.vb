
Partial Class reports_CarbonReport_CRCManagementArea_Default
    Inherits System.Web.UI.Page

    Private Sub LoadReports()

        'Me.ulBox.InnerHtml = ""

        Dim strSQL As String = "EXEC spReports_ListReports " & MySession.UserID & ", 16"

        Using connection As New SqlConnection(Core.ConnectionString)
            connection.Open()
            Dim command As New SqlCommand(strSQL, connection)
            command.CommandType = CommandType.Text
            Dim reader As SqlDataReader = command.ExecuteReader()

            While reader.Read

                Dim strUrl As String
                If reader("intReportDisabled") = 0 Then
                    strUrl = reader("varOnlineURL").ToString & Request.QueryString("crccustid")
                Else
                    strUrl = ""
                End If

                Me.lnkReports41.HRef = strUrl

                'Me.ulBox.InnerHtml &= "<li><a id='lnkReports" & reader("intReportPK") & "' class='" & reader("varCSS") & "' runat='server' title='" & reader("varReportName") & "' href='" & strUrl & "' ></a></li>"



            End While

        End Using



    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IIf(Core.data_select_value("select bitAllowMyReports from tblUsers where intUserPK = " & MySession.UserID) = "True", True, False) Then
            Response.Redirect(Core.data_select_value("select RawHomeURL from vwUsers where ID = " & MySession.UserID), True)
        End If

        If Request.QueryString("crccustid") Is Nothing Then
            Dim strSQL As String = "EXEC spPortal_TREEVIEW_CRCTopLevel " & MySession.UserID
            Dim myConnection As New OleDb.OleDbConnection(Core.CurrentConnectionString)

            Dim myCommand As New OleDb.OleDbCommand(strSQL, myConnection)
            myCommand.Connection.Open()
            myCommand.CommandTimeout = 180
            Dim Reader As OleDb.OleDbDataReader = myCommand.ExecuteReader(CommandBehavior.CloseConnection)

            If Reader.HasRows Then
                Reader.Read()

                Response.Redirect("default.aspx?crccustid=" & Reader("OrganisationID") & "", True)

                Reader.Close()
            End If
        End If


        ' Check you have rights to see the organisation
        Dim strCheckSQL As String = "SELECT COUNT(*) FROM UML_CMS.dbo.tblCRC_Company INNER JOIN (SELECT DISTINCT OrganisationID, Used, TypeOf FROM UML_CMS.dbo.tbl_Customer_BOM WHERE TypeOf = 'C' AND isNumeric(Used) = 1)tblA ON Used = CustID INNER JOIN UML_CMS.dbo.tbl_Virtual_Customer ON VirtualID = OrganisationID WHERE PRO_Custid IN (SELECT ID FROM [portal.utilitymasters.co.uk].dbo.vwUsersCompanies where [User ID] = " & MySession.UserID & ") AND OrganisationID = " & Request.QueryString("crccustid")

        Dim intCheckUserOrg As Integer = Core.data_select_value(strCheckSQL)

        If intCheckUserOrg = 0 Then
            Response.Redirect("~/reports/carbonreports/default.aspx", True)
        End If

        Me.lblCRCOrganisationName.Text = "Organisation Selected: " & Core.data_select_value("SELECT Product FROM UML_CMS.dbo.tbl_Virtual_Customer WHERE VirtualID = " & Request.QueryString("crccustid"))

        LoadReports()
    End Sub

End Class
