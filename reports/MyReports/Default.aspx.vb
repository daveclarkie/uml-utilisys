﻿
Partial Class reports_MyReports_Default
    Inherits System.Web.UI.Page


    Private Sub LoadReports()

        Me.ulBox.InnerHtml = ""

        Dim strSQL As String = "SELECT TOP 6 a.*, b.varCSSClassName, b.intReportPK FROM uvw_Analysis_ReportUsageByUser a INNER JOIN dbo.tlkpReports b ON a.varPagePath = b.varOnlineURL WHERE intUserPK = " & MySession.UserID & " ORDER BY intCount desc"

        Using connection As New SqlConnection(Core.ConnectionString)
            connection.Open()
            Dim command As New SqlCommand(strSQL, connection)
            command.CommandType = CommandType.Text
            Dim reader As SqlDataReader = command.ExecuteReader()

            While reader.Read

                Dim strUrl As String

                strUrl = reader("varPagePath").ToString

                Me.ulBox.InnerHtml &= "<li><a id='lnkReports" & reader("intReportPK") & "' class='" & reader("varCSSClassName") & "' runat='server' title='" & reader("varReportName") & "' href='" & strUrl & "' ></a>"
                Me.ulBox.InnerHtml &= "viewed " & reader("intCount") & " times"

                Me.ulBox.InnerHtml &= "</li>"



            End While

        End Using

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Request.QueryString("method") = "delete" Then

            Core.data_execute_nonquery("DELETE FROM [portal.utilitymasters.co.uk].dbo.tblUsersMyReports WHERE intUserFK = " & MySession.UserID & " AND intReportFK = " & Request.QueryString("report"))
            Response.Redirect("Default.aspx")
        ElseIf Request.QueryString("method") = "add" Then

            If Core.data_select_value("SELECT COUNT(*) FROM [portal.utilitymasters.co.uk].dbo.tblUsersMyReports WHERE intUserFK = " & MySession.UserID & " AND intReportFK = " & Request.QueryString("report")) = 0 Then
                Core.data_execute_nonquery("INSERT INTO [portal.utilitymasters.co.uk].dbo.tblUsersMyReports (intUserFK, intReportFK) VALUES (" & MySession.UserID & ", " & Request.QueryString("report") & ")")
            End If
            Response.Redirect("Default.aspx")
            End If

            LoadReports()
    End Sub
End Class
