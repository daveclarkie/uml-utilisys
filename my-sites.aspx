<%@ Page Language="VB" MasterPageFile="~/landing.master" AutoEventWireup="false" CodeFile="my-sites.aspx.vb" Inherits="my_sites" title="My Sites" %>

<asp:content contentplaceholderid="content_right" runat="server">

    <div class="content_back general">
        <ul class="box_links_reports">
            <li><a id="lnkMySites1" class="mySites_mapview" runat="server" title="Map View" href="mysites/map/default.aspx"></a></li>
            <li><a id="lnkMySites2" class="mySites_search"  runat="server" title="Search" href="mysites/search/default.aspx"></a></li>
            <li><a id="lnkMySites3" class="mySites_supplydetails"  runat="server" title="Supply Details" href="mysites/supplydetails/default.aspx"></a></li>
            
            
            
        </ul>
    </div>
    
</asp:content>
