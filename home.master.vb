Imports Security

Partial Class _homemaster
    Inherits System.Web.UI.MasterPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IIf(Core.data_select_value("select bitAllowAdmin from tblUsers where intUserPK = " & MySession.UserID) = "True", True, False) Then
            lnkAdmin.CssClass = "adminnone"
            lnkAdmin.NavigateUrl = ""
            lnkAdmin.ToolTip = ""
        Else
            lnkAdmin.CssClass = "admin"
        End If

        'Hide unused menus
        If Not IIf(Core.data_select_value("select bitAllowMyReports from tblUsers where intUserPK = " & MySession.UserID) = "True", True, False) Then
            lnkMyReports.CssClass = "reportsnone"
            lnkMyReports.NavigateUrl = ""
            lnkMyReports.ToolTip = ""
        Else
            lnkMyReports.CssClass = "reports"
        End If


        If Not IIf(Core.data_select_value("select bitAllowMySites from tblUsers where intUserPK = " & MySession.UserID) = "True", True, False) Then
            lnkMySites.CssClass = "sitesnone"
            lnkMySites.NavigateUrl = ""
            lnkMySites.ToolTip = ""
        Else
            lnkMySites.CssClass = "sites"
        End If


        If Not IIf(Core.data_select_value("select bitAllowMyApps from tblUsers where intUserPK = " & MySession.UserID) = "True", True, False) Then
            lnkMyApps.CssClass = "appsnone"
            lnkMyApps.NavigateUrl = ""
            lnkMyApps.ToolTip = ""
        Else
            lnkMyApps.CssClass = "apps"
        End If


        If Not IIf(Core.data_select_value("select bitAllowMyHelp from tblUsers where intUserPK = " & MySession.UserID) = "True", True, False) Then
            lnkHelp.CssClass = "helpnone"
            lnkHelp.NavigateUrl = ""
            lnkHelp.ToolTip = ""
        Else
            lnkHelp.CssClass = "help"
        End If



        'select correct menu item
        Dim strURL As String = Core.CheckForNullString(Me.Page.Request.Url.PathAndQuery.ToString)

        'sets the menu item to green when on the relevant page
        If strURL.Contains("/my-sites.aspx") Then Me.lnkMySites.Style.Add("background", "transparent url('" & Me.ResolveUrl("~/assets/Schneider/menu-item-my-sites-hover.jpg") & "') no-repeat")
        If strURL.Contains("/mysites/") Then Me.lnkMySites.Style.Add("background", "transparent url('" & Me.ResolveUrl("~/assets/Schneider/menu-item-my-sites-hover.jpg") & "') no-repeat")
        If strURL.Contains("/my-reports.aspx") Then Me.lnkMyReports.Style.Add("background", "transparent url('" & Me.ResolveUrl("~/assets/Schneider/menu-item-my-reports-hover.jpg") & "') no-repeat")
        If strURL.Contains("/reports/") Then Me.lnkMyReports.Style.Add("background", "transparent url('" & Me.ResolveUrl("~/assets/Schneider/menu-item-my-reports-hover.jpg") & "') no-repeat")
        If strURL.Contains("/my-account.aspx") Then Me.lnkMyAccount.Style.Add("background", "transparent url('" & Me.ResolveUrl("~/assets/Schneider/menu-item-my-account-hover.jpg") & "') no-repeat")
        If strURL.Contains("/myaccount/") Then Me.lnkMyAccount.Style.Add("background", "transparent url('" & Me.ResolveUrl("~/assets/Schneider/menu-item-my-account-hover.jpg") & "') no-repeat")
        If strURL.Contains("/my-apps.aspx") Then Me.lnkMyApps.Style.Add("background", "transparent url('" & Me.ResolveUrl("~/assets/Schneider/menu-item-my-apps-hover.jpg") & "') no-repeat")
        If strURL.Contains("/myapps/") Then Me.lnkMyApps.Style.Add("background", "transparent url('" & Me.ResolveUrl("~/assets/Schneider/menu-item-my-apps-hover.jpg") & "') no-repeat")
        If strURL.Contains("/users.aspx") Then Me.lnkAdmin.Style.Add("background", "transparent url('" & Me.ResolveUrl("~/assets/Schneider/menu-item-admin-hover.jpg") & "') no-repeat")
        If strURL.Contains("/user.aspx") Then Me.lnkAdmin.Style.Add("background", "transparent url('" & Me.ResolveUrl("~/assets/Schneider/menu-item-admin-hover.jpg") & "') no-repeat")
        If strURL.Contains("/maintenance.aspx") Then Me.lnkAdmin.Style.Add("background", "transparent url('" & Me.ResolveUrl("~/assets/Schneider/menu-item-admin-hover.jpg") & "') no-repeat")
        If strURL.Contains("/my-help.aspx") Then Me.lnkHelp.Style.Add("background", "transparent url('" & Me.ResolveUrl("~/assets/Schneider/menu-item-my-help-hover.jpg") & "') no-repeat")

        Me.lblCurrrentUser.Text = "Logged in as: " & Core.data_select_value(" SELECT varEmail FROM [portal.utilitymasters.co.uk].dbo.tblUsers where intUserPK = " & MySession.UserID) & " : " & MySession.UserID

        If MySession.UserID = 1 Then
            Response.Redirect("~/login.aspx?action=usererror")
        End If

        Dim strMarqueeCode As String = ""
        Dim strMarqueeText As String = ""
        Dim sMyScreenPath As String

        sMyScreenPath = System.Web.HttpContext.Current.Request.Url.AbsolutePath

        Me.currentpage.Text = Nothing
        Dim strCustomPage As String = Core.data_select_value("SELECT varDefaultPage FROM tblUsers where intUserPK = " & MySession.UserID)

        'If MySession.UserID <> 77 Then
        Core.data_execute_nonquery("INSERT INTO [portal.utilitymasters.co.uk].dbo.tblUserActivities (intUserFK, varPagePath) VALUES (" & MySession.UserID & ", '" & sMyScreenPath & "')")
        'End If
        'strMarqueeText = "Please find the UTILIsys Portal Terms and Conditions <a href='https://images.utilitymasters.co.uk/portal documents/UTILIsys Terms and Conditions.pdf' target='_blank'>HERE</a>"
        strMarqueeText = "Welcome to UTILISYS " & Core.data_select_value(" SELECT varForename + ' ' + varSurname FROM [portal.utilitymasters.co.uk].dbo.tblUsers where intUserPK = " & MySession.UserID) & ", you logged in on: " & Core.data_select_value("SELECT TOP 1 UML_EXTData.dbo.FormatDateTime(dtmCreated, 'LONGDATE') FROM tblUsersLoginAttempts WHERE intUserFK = " & MySession.UserID & " ORDER BY dtmCreated desc") & " at " & Core.data_select_value("SELECT TOP 1 UML_EXTData.dbo.FormatDateTime(dtmCreated, 'HH:MM:SS 24') FROM tblUsersLoginAttempts WHERE intUserFK = " & MySession.UserID & " ORDER BY dtmCreated desc")
        'strMarqueeText = System.Web.HttpContext.Current.Request.Url.ToString()

        'strMarqueeCode = "<MARQUEE>" + strMarqueeText + "</MARQUEE>"
        strMarqueeCode = "<asp:label id='lblInformation' runat='server'>" + strMarqueeText + "</asp:label>"
        marquee_1.Text = psDecrypt(psEncrypt(strMarqueeCode))



        Dim strPassthrough As String = Core.data_select_value("SELECT varEmail + '&&' + varPassword FROM tblUsers where intUserPK = " & MySession.UserID)

        'Me.lnkHelp.NavigateUrl = "#" ' "http://utilihelp.utilitymasters.co.uk/login.aspx?passthrough=" + psEncrypt(strPassthrough.Replace(" ", "+"))

    End Sub


End Class

