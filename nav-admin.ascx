<%@ Control Language="VB" AutoEventWireup="false" CodeFile="nav-admin.ascx.vb" Inherits="nav_admin" %>

<div id="left_nav_admin" class="left_nav">
    <div>
        <asp:hyperlink rel="nofollow" runat="server" id="lnkAdmin" navigateurl="~/users.aspx" cssclass="nav_home" tooltip="Administration area" />
        <h2 class="nav_heading"><asp:hyperlink runat="server" id="lnkAdminHome" navigateurl="~/users.aspx" tooltip="Administration area">Administration</asp:hyperlink></h2>
        <ul class="nav">
            <li><asp:hyperlink runat="server" id="lnkUsers" navigateurl="~/users.aspx" tooltip="Administer users">Users</asp:hyperlink></li>
            <li><asp:hyperlink runat="server" id="lnkMaintenance" navigateurl="~/maintenance.aspx" tooltip="Maintenance">Report Maintenance</asp:hyperlink></li>
            <li><asp:hyperlink runat="server" id="lnkLoginAnalysis" navigateurl="~/myadmin/loginanalysis.aspx" tooltip="Login Analysis">Login Analysis</asp:hyperlink></li>
            <li><asp:hyperlink runat="server" id="Hyperlink1" navigateurl="~/myapps/CustMeterSetup/CustomerMeterSetup.aspx" tooltip="Administration for customer meters">Customer Meter(s) Setup</asp:hyperlink></li>
      
      
        </ul>
    </div>
</div>

